<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'content_media_block';
$className = 'content_media_block';
$new_form = get_field('new_form');
$new_form = $new_form ? ' new-form' : ' ';
//$className .= ' $new-form';
$className .= $new_form;
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];}

if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/content_media_block/screenshot.png" >';
    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$image_right_or_left = get_field('image_right_or_left');
$have_svg_animation = get_field('have_svg_animation');
$title = get_field('title');
$description = get_field('description');
$description_style = get_field('description_style');
$image = get_field('image');
$cta_link = get_field('cta_link');
$main_title = get_field('main_title');
$title_style = get_field('title_style');
$padding_content = get_field ('padding_content');
$increase_padding = get_field ('increase_padding');
?>
<!-- region propellerhealth Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <?php if ($main_title) { ?>
        <h2 class="content-media-title headline-2 iv-st-from-bottom"> <?= $main_title ?> </h2>
    <?php } ?>
    <div class="content-media-cards <?= $image_right_or_left? 'content-media-cards-reverse':"" ?><?= $increase_padding ? ' increase' : "" ?>">
        <div class="content-media-image">
            <?php if ($image) { ?>
                <picture class="aspect-ratio iv-st-from-bottom">
                    <?= get_acf_image( $image, 'img-494-600' ) ?>
                </picture>

            <?php } ?>
        </div>
        <?php if ($have_svg_animation) { ?>
            <div class="have-svg">
                <svg class="dots-animation-i-e site-dots" fill="none">
                    <path class="dot-line-animation" d="" stroke="#90E2C8" stroke-width="3" stroke-linecap="round" stroke-dasharray="1 10 1 10"/>
                </svg>
            </div>
        <?php } ?>
        <div class="content-media-text<?= $padding_content ? ' no-padding ' : ""?>">
            <?php if ($title) { ?>
                <h3 class="headline-3 = content-media-text-title iv-st-from-bottom <?= $title_style ?>">
                    <?= $title ?>
                </h3>
            <?php } ?>
            <?php if ($description) { ?>
                <div class="paragraph content-media-text-description iv-st-from-bottom <?= $description_style ?>">
                    <?= $description  ?>
                </div>
            <?php } ?>
            <?php if ($cta_link) { ?>
                <a  role="button" class="cta-button white-cta content-media-link iv-st-from-bottom"
                    target="<?= $cta_link['target']?>"  href="<?= $cta_link['url'] ?> "> <?= $cta_link['title'] ?>
                </a>
            <?php } ?>
        </div>
    </div>
</div>
</section>
<!-- endregion propellerhealth Block -->


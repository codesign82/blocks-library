<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
$versions = get_field('trending_products_versions');

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'trending_products_block';
$className = 'trending_products_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if ($versions) {
    $className .= ' ' . $versions;
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/trending_products_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_title = get_field('main_title');
$trending_product_repeater = get_field('trending_product_repeater');
$trending_products_looking_repeater = get_field('trending_products_looking_repeater');
?>
<!-- region samrt_dv Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<?php if ($versions) { ?>
    <div class="slides-bg">
        <svg preserveAspectRatio="none" viewBox="0 0 100 100">
            <path d="M -12.4 20 L 9.3 0 L 90.7 0 L 112.4 20 L 0 20 Z"></path>
        </svg>
    </div>
<?php } ?>
<div class="content-wrapper">
    <div class="container">
        <div class="trending-wrapper" data-version="<?= $versions ?>">
            <div class="left-content">
                <?php if ($main_title) { ?>
                    <div class="headline-2 left-title word-up"><?= $main_title ?></div>
                <?php } ?>
            </div>

            <div class="right-content">
                <div class="swiper-button swiper-button-next arrow-left round-arrow">
                    <svg width="27" height="16" viewBox="0 0 27 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.814378 7.34953C0.423853 7.74006 0.423853 8.37322 0.814378 8.76375L7.17834 15.1277C7.56886 15.5182 8.20203 15.5182 8.59255 15.1277C8.98308 14.7372 8.98308 14.104 8.59255 13.7135L2.9357 8.05664L8.59255 2.39979C8.98308 2.00926 8.98308 1.3761 8.59255 0.985573C8.20203 0.595048 7.56886 0.595048 7.17834 0.985573L0.814378 7.34953ZM1.52148 9.05664H26.3666V7.05664H1.52148V9.05664Z"
                              fill="#313545"/>
                    </svg>

                </div>
                <?php if (have_rows('trending_product_repeater')) { ?>
                    <div class="swiper mySwiper ">
                        <div class="swiper-wrapper">

                            <?php while (have_rows('trending_product_repeater')) {
                                the_row();
                                $title = get_sub_field('title');
                                $small_text = get_sub_field('small_text');
                                ?>
                                <div class="swiper-slide">
                                    <div class="text-wrapper">
                                        <?php if ($title) { ?>
                                            <div class="headline-5 title">
                                                <?= $title ?>
                                            </div>
                                        <?php } ?>
                                        <?php if ($small_text) { ?>
                                            <div class="small-text">
                                                <?= $small_text ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>

                    </div>
                <?php } ?>
                <?php if (have_rows('trending_products_looking_repeater')) { ?>
                    <div class="swiper mySwiper">
                        <div class="swiper-wrapper">

                            <?php while (have_rows('trending_products_looking_repeater')) {
                                the_row();
                                $date = get_sub_field('date');
                                $description = get_sub_field('description');
                                ?>
                                <div class="swiper-slide">
                                    <div class="text-wrapper">
                                        <?php if ($date) { ?>
                                            <div class="date-text">
                                                <?= $date ?>
                                            </div>
                                        <?php } ?>
                                        <?php if ($description) { ?>
                                            <div class="description-text">
                                                <?= $description ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>

                    </div>
                <?php } ?>
                <div class="swiper-button swiper-button-prev arrow-right round-arrow">
                    <svg width="27" height="16" viewBox="0 0 27 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M26.1856 8.65047C26.5761 8.25994 26.5761 7.62678 26.1856 7.23625L19.8217 0.872292C19.4311 0.481767 18.798 0.481767 18.4074 0.872292C18.0169 1.26282 18.0169 1.89598 18.4074 2.28651L24.0643 7.94336L18.4074 13.6002C18.0169 13.9907 18.0169 14.6239 18.4074 15.0144C18.798 15.405 19.4311 15.405 19.8217 15.0144L26.1856 8.65047ZM25.4785 6.94336L0.633448 6.94336V8.94336L25.4785 8.94336V6.94336Z"
                              fill="#313545"/>
                    </svg>

                </div>
            </div>

        </div>

    </div>
</div>
</section>


<!-- endregion samrt_dv Block -->

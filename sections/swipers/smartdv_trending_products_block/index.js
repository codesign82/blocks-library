import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";

import {debounce} from "../../../scripts/functions/debounce";

import '../../../scripts/sitesSizer/propellerhealth';


import Swiper, {Navigation, Pagination} from "swiper";


const blockScript = async (container = document) => {
    const blocks = container.querySelectorAll('.trending_products_block');
    for (let block of blocks) {

        const swiperControllers = block.querySelector('.swiper-arrows-container');
        const slides = block.querySelectorAll('.swiper-slide');
        const breakpoint = window.matchMedia('(min-width:600px)');
        const version = block.querySelector(".trending-wrapper").dataset.version;
        const swiperChecker = breakpoint.matches ? {
            autoHeight: false,
        } : {
            autoHeight: true,
        };
        const desktopSlopValue = 8.333334;
        const tabletSlopValue = 5.04;
        const mobileSlopValue = (parseFloat(window.getComputedStyle(block.querySelector(".container"), null).getPropertyValue('padding-left')) / window.innerWidth) * 100;
        ;
        let pathSlopValue = desktopSlopValue;
        const fixPaths = debounce(() => {
            pathSlopValue = window.innerWidth >= 992
                ? desktopSlopValue
                : window.innerWidth >= 600
                    ? tabletSlopValue
                    : mobileSlopValue;
            const path = block.querySelector('.slides-bg path')
            path.setAttribute('d', `M 0 100 L ${pathSlopValue} 0 L ${100 - pathSlopValue} 0 L 100 100 Z`);
        }, 300);

        fixPaths();
        window.addEventListener('resize', fixPaths)
        const swiper = new Swiper(block.querySelector(".mySwiper"), {
            observer: true,
            observerParents: true,
            spaceBetween: '5%',
            modules: [Navigation, Pagination],
            navigation: {
                nextEl: block.querySelector(".swiper-button-next"),
                prevEl: block.querySelector(".swiper-button-prev"),
            },
            ...swiperChecker,
            breakpoints: {
                300:
                    {
                        slidesPerView: version === "theme1" ? 1.2 : 1.18,
                        // slidesPerView: 1,
                        spaceBetween: version === "theme1" ? 33 : 18,

                    },
                600:
                    {
                        // centeredSlides: true,
                        slidesPerView: version === "theme1" ? 1 : 1.5,
                        // slidesPerView: 1,
                        slidesOffsetBefore: 0,
                        spaceBetween: 18,
                    },
                991:
                    {
                        // centeredSlides: true,
                        slidesPerView: version === "theme1" ? 1 : 2.5,
                        // slidesPerView: 1,
                        slidesOffsetBefore: 0,
                        spaceBetween: 18,
                    }
            },
        });

        animations(block);
        imageLazyLoading(block);
    }
};
windowOnLoad(blockScript);



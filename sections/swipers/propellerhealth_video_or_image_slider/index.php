<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'video_or_image_slider';
$className = 'video_or_image_slider';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/video_or_image_slider/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_title = get_field('main_title');
$main_description = get_field('main_description');
$title = get_field('title');
$date_type = get_field('date_type');
$description = get_field('description');
$choose_you_media = get_field('choose_you_media');
$video_options = get_field('video_options');
$video_type = @$video_options['video_type'];
$video_url = @$video_options['video_url'];
$video_file = @$video_options['video_file'];
$youtube_url = @$video_options['youtube_url'];
$vimeo_url = @$video_options['vimeo_url'];
$vimeo_embed = @$video_options['embed'];
$video_final = '';
if ($video_type === 'video_url') {
    $video_final = $video_url;
} elseif ($video_type === 'youtube') {
    $video_final = $youtube_url;
} elseif ($video_type === 'vimeo') {
    $video_final = $vimeo_url;
} elseif ($video_type === 'embed') {
    $video_final = $vimeo_embed;
} else {
    $video_final = $video_file;
}
?>
<!-- region propellerhealth Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="video-or-image-slider-cards-wrapper">
        <div class="video-or-image-slider-first-content iv-st-from-bottom">
            <?php if ($main_title) { ?>
                <h4 class="video-or-image-slider-first-content-title headline-4">
                    <?= $main_title ?>
                </h4>
            <?php } ?>
            <?php if ($main_description) { ?>
                <div class="video-or-image-slider-first-content-description headline-5">
                    <?= $main_description ?>
                </div>
            <?php } ?>
        </div>
        <div class="video-or-image-slider  <?= have_rows('gallery') ? " iv-st-from-bottom" :  " " ?>">
            <?php if (have_rows('gallery')) { ?>
                <div class="swiper">
                    <div class="swiper-wrapper">
                        <?php while (have_rows('gallery')) {
                            the_row();
                            $image = get_sub_field('image');
                            $caption = get_sub_field('caption');
                            ?>
                            <?php if ($choose_you_media === 'images' && $image) { ?>
                                <div class="swiper-slide">

                                    <div class="figure-style">
                                        <?php if ($image) { ?>
                                            <div class="aspect-ratio">
                                                <?= get_acf_image( $image, 'img-800-463') ?>
                                            </div>
                                        <?php } ?>
                                        <?php if ($caption && $image) { ?>
                                            <figcaption class="headline-6 caption">
                                                <?= $caption ?>
                                            </figcaption>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            <?php } else { ?>
                <?php if ($video_final) { ?>
                    <div class="aspect-ratio">
                        <?php if ($video_type === 'video_url' || $video_type === 'video_file') { ?>
                            <video playsinline controls loop
                                   src="<?= $video_final ?>"
                                   class="video-player"
                                   data-video-type="<?= $video_type ?>"></video>
                        <?php } else { ?>
                            <div class="video-iframe video-wrapper">
                                <div class="video-player"
                                     data-video-url="<?= $video_final ?>"
                                     data-video-type="<?= $video_type ?>"></div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <div class="video-or-image-slider-last-content iv-st-from-bottom">
            <?php if ($title) { ?>
                <h3 class="video-or-image-slider-last-content-title headline-5">
                    <?= $title ?>
                </h3>
            <?php } ?>
            <?php if ($date_type) { ?>
                <h5 class="video-or-image-slider-last-content-date headline-5">
                    <?= $date_type ?>
                </h5>
            <?php } ?>
            <?php if ($description) { ?>
                <div class="video-or-image-slider-last-content-description headline-5">
                    <?= $description ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
</section>


<!-- endregion propellerhealth Block -->

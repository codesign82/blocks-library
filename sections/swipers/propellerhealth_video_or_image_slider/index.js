import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/propellerhealth';
import {Swiper} from "swiper";
import {vimeoPlayer} from "../../../scripts/general/vimeo-player";
import {youtubePlayer} from "../../../scripts/general/youtube-player";

const blockScript = async (container = document) => {
  // const block = container.querySelector('.customer_stories_block');

  const swiper = new Swiper('.swiper', {
    speed: 400,
    spaceBetween: 10,
  });

  const videoPlayer = block.querySelector('.video-player');
  let player;
  if (videoPlayer) {
    const videoType = videoPlayer.dataset.videoType;
    const videoURL = videoPlayer.dataset.videoUrl;
    if (videoType === 'vimeo') {
      player = vimeoPlayer(videoPlayer, videoURL);
    } else if (videoType === 'youtube') {
      player = youtubePlayer(videoPlayer, videoURL, {
        annotations: false,
        modestBranding: false,
        related: false
      });
    } else {
      player = videoPlayer;
    }
  }

  animations(block);
  imageLazyLoading(block);
  
};
windowOnLoad(blockScript);



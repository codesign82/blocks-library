<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'customers_slider_block';
$className = 'customers_slider_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/customers_slider_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
?>
<!-- region CyberHill's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="customers-slider-wrapper">
    <?php if ($title) { ?>
      <div class="headline-3 iv-st-from-left"><?= $title ?></div>
    <?php } ?>
    <div class="swiper swiper-container iv-st-from-right">
      <?php
      if (have_rows('grid_slides')) {
        ?>
        <div class="swiper-wrapper">
          <?php
          while (have_rows('grid_slides')) {
            the_row();
            ?>
            <div class="swiper-slide  ">
              <?php
              if (have_rows('grid_slides_column')) {
                ?>
                <?php
                while (have_rows('grid_slides_column')) {
                  the_row();
                  $image = get_sub_field('image');
                  $is_svg = get_sub_field('is_svg');
                  $svg = get_sub_field('svg');
                  $select_content = get_sub_field('select_content');
                  $text = get_sub_field('text');
                  $media_is_svg = get_sub_field('media_is_svg');
                  $media_is_svg = get_sub_field('media_is_svg');
                  $hover_state_image = get_sub_field('hover_state_image');
                  $hover_state_svg = get_sub_field('hover_state_svg');
                  ?>
                  <div class="perspective-flip">
                    <div class="cards flip <?php if ($text || $hover_state_image || $hover_state_svg) {?><?='has-flip'?><?php }?>">
                      <div class="flip-card flip-front">
                        <div class="content">
                          <?php if ($is_svg) : ?>
                            <?= $svg ?>
                          <?php else : ?>
                            <picture>
                              <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>"/>
                            </picture>
                          <?php endif; ?>
                        </div>
                      </div>
                      <div class="flip-back flip-card">
                        <div class="content">
                          <?php if ($select_content === 'text') { ?>
                            <h5 class="headline-5"><?= $text ?></h5>
                          <?php } ?>
                          <?php if ($select_content === 'media') { ?>
                            <?php if ($media_is_svg) : ?>
                              <?= $hover_state_svg ?>
                            <?php else : ?>
                              <picture>
                                <img src="<?= $hover_state_image['url'] ?>" alt="<?= $hover_state_image['alt'] ?>"/>
                              </picture>
                            <?php endif; ?>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php
                } ?>
                <?php
              }
              ?>
            </div>
            <?php
          } ?>
        </div>
        <?php
      }
      ?>
      <!-- Add Pagination -->
      <div class="swiper-pagination iv-st-from-bottom"></div>
    </div>
  </div>
</div>
</section>
<!-- endregion CyberHill's Block -->

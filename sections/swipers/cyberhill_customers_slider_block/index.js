import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import Swiper, {Pagination, Autoplay} from "swiper";
import '../../../scripts/sitesSizer/cyberhill';

Swiper.use([Pagination, Autoplay]);
const blockScript = async (container = document) => {
  const block = container.querySelector('.customers_slider_block');
  
  
  // add block code here
  
  
  let swiper = new Swiper(block.querySelector('.swiper'), {
    slidesPerView: 2,
    spaceBetween: 16,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false,
    },
    breakpoints: {
      600: {
        slidesPerView: 2,
        spaceBetween: 12,
      },
      1440: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
    },
    
    
    
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    
  });
  
  
  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



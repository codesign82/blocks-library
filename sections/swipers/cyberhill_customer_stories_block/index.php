<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'customer_stories_block';
$className = 'customer_stories_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/customer_stories_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$section_title = get_field('section_title');
$view_all = get_field('view_all');
$manual_entry = get_field('manual_entry');
?>
<!-- region CyberHill's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="customer">
    <?php
    if (!$manual_entry) {
      $case_studies = get_field('case_studies');
      $case_studies_count = count($case_studies);
      if ($case_studies): ?>
        <div class="swiper swiper-container">
          <div class="swiper-wrapper">
            <?php foreach ($case_studies as $case_study):
              $post_id = $case_study;
              $thumbnail_id = get_post_thumbnail_id($post_id);
              $alt = get_post_meta(get_post_thumbnail_id($post_id), '_wp_attachment_image_alt', true);
              ?>
              <div class="swiper-slide">
                <div class="content">
                  <div class="right-side">
                    <?php if ($thumbnail_id) { ?>
                      <picture class="aspect-ratio">
                        <a href="<?php the_permalink($post_id); ?>">
                          <img src="<?= get_the_post_thumbnail_url($post_id) ?>" alt="<?= $alt ?>">
                        </a>
                      </picture>
                    <?php } ?>
                  </div>
                  <div class="left-side">
                    <?php if ($section_title) { ?>
                      <div class="title-with-shape title-with-shape-blue">
                        <?= $section_title ?>
                      </div>
                    <?php } ?>
                    <a href="<?php the_permalink($post_id); ?>">
                      <h3 class="headline-3 current-slide-title"><?= get_the_title($post_id) ?></h3>
                    </a>
                    <div class="paragraph"><?= wp_trim_words(get_the_excerpt($post_id), 20) ?></div>
                    <?php if ($case_studies_count > 1) { ?>
                      <div class="next">
                        <div class="small-text-14">NEXT</div>
                        <div class="paragraph-14 next-slide-title"><?= get_the_title($post_id) ?></div>
                      </div>
                    <?php } ?>

                  </div>
                </div>
              </div>
            <?php endforeach; ?>
            <div class="moves">
              <?php if ($case_studies_count > 1) { ?>
                <div class="swiper-scrollbar"></div>
                <div class="s-btn">
                  <div class="card-arrow">
                    <button aria-label="Next Slide" class="swiper-button-prev ">
                      <svg width="48" height="48" viewBox="0 0 48 48" fill="none"
                           xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M20.0347 30.2058L14.2681 24.6074H36V23.4843H14.1737L20.0347 17.7942L19.217 17L12.8247 23.206L12.8176 23.1991L12 23.9933L12.0069 24L12 24.0067L12.8176 24.8009L12.8247 24.794L19.217 31L20.0347 30.2058Z"
                              fill="#1D4070"/>
                      </svg>
                    </button>
                    <button class="swiper-button-next">
                      <svg width="48" height="48" viewBox="0 0 48 48" fill="none"
                           xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M27.9653 17.7942L33.7319 23.3926H12V24.5157H33.8263L27.9653 30.2058L28.783 31L35.1753 24.794L35.1824 24.8009L36 24.0067L35.9931 24L36 23.9933L35.1824 23.1991L35.1753 23.206L28.783 17L27.9653 17.7942Z"
                              fill="#1D4070"/>
                      </svg>
                    </button>
                  </div>
                  <div class="swiper-pagination"></div>
                </div>
              <?php } ?>
            </div>
          </div>
        </div>
      <?php endif; ?>
    <?php } else {
      if (have_rows('manual_entry_repeater')) { ?>
        <div class="swiper swiper-container">
          <div class="swiper-wrapper">
            <?php $row_count = count(get_field('manual_entry_repeater')); ?>
            <?php while (have_rows('manual_entry_repeater')) {
              the_row();
              $image = get_sub_field('image');
              $title = get_sub_field('title');
              $description = get_sub_field('description');
              $subtitle = get_sub_field('subtitle');
              $link = get_sub_field('link');
              ?>
              <div class="swiper-slide">
                <div class="content">
                  <div class="right-side">
                    <?php if ($image) { ?>
                      <picture class="aspect-ratio">
                        <?php if ($link){ ?><a href="<?= $link ?>"><?php } ?>
                          <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                          <?php if ($link){ ?></a><?php } ?>
                      </picture>
                    <?php } ?>
                  </div>
                  <div class="left-side">
                    <?php if ($section_title) { ?>
                      <div class="title-with-shape title-with-shape-blue">
                        <?= $section_title ?>
                      </div>
                    <?php } ?>
                    <?php if ($link){ ?><a href="<?= $link ?>"><?php } ?>
                      <h3 class="headline-3 current-slide-title"><?= $title ?></h3>
                      <?php if ($link){ ?></a><?php } ?>
                    <div class="paragraph"><?= $description ?></div>
                    <?php if ($row_count > 1) { ?>
                      <div class="next">
                        <div class="small-text-14">NEXT</div>
                        <div class="paragraph-14 next-slide-title"><?= $title ?></div>
                      </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            <?php } ?>
            <?php if ($row_count > 1) { ?>
              <div class="moves">
                <div class="swiper-scrollbar"></div>
                <div class="s-btn">
                  <div class="card-arrow">
                    <button aria-label="Next Slide" class="swiper-button-prev ">
                      <svg width="48" height="48" viewBox="0 0 48 48" fill="none"
                           xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M20.0347 30.2058L14.2681 24.6074H36V23.4843H14.1737L20.0347 17.7942L19.217 17L12.8247 23.206L12.8176 23.1991L12 23.9933L12.0069 24L12 24.0067L12.8176 24.8009L12.8247 24.794L19.217 31L20.0347 30.2058Z"
                              fill="#1D4070"/>
                      </svg>
                    </button>
                    <button class="swiper-button-next">
                      <svg width="48" height="48" viewBox="0 0 48 48" fill="none"
                           xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M27.9653 17.7942L33.7319 23.3926H12V24.5157H33.8263L27.9653 30.2058L28.783 31L35.1753 24.794L35.1824 24.8009L36 24.0067L35.9931 24L36 23.9933L35.1824 23.1991L35.1753 23.206L28.783 17L27.9653 17.7942Z"
                              fill="#1D4070"/>
                      </svg>
                    </button>
                  </div>
                  <div class="swiper-pagination"></div>
                </div>
              </div>
            <?php } ?>
          </div>
        </div>
      <?php } ?>
    <?php } ?>
  </div>
</div>
</section>
<!-- endregion CyberHill's Block -->

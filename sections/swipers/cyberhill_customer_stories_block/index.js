import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/cyberhill';
import Swiper, {Pagination, Scrollbar, EffectFade} from "swiper";

const getNewIndex = (change, oldValue, len) => (((oldValue + change) % len) + len) % len;
Swiper.use([Pagination, Scrollbar, EffectFade])
const blockScript = async (container = document) => {
  const block = container.querySelector('.customer_stories_block');
  
  const nextSlideButton = block.querySelector('.swiper-button-next');
  const prevSlideButton = block.querySelector('.swiper-button-prev');
  let swiper = new Swiper(block.querySelector('.swiper'), {
    // loop: true,
    slidesPerView: 1,
    // grabCursor: true,
    // autoHeight:true,
    allowTouchMove: false,
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
    
    scrollbar: {el: '.swiper-scrollbar', draggable: true},
    pagination: {el: '.swiper-pagination', clickable: true, type: 'fraction'}
  });
  
  function changeButtonActive(button) {
    setTimeout(
        function () {
          button.classList.remove('add-active')
        }, 300);
  }
  
  nextSlideButton.addEventListener('click', () => {
    // console.log(swiper);
    // swiper.slideTo(swiper.activeIndex === swiper.slides.length - 1 ? 0 : swiper.activeIndex + 1);
    swiper.slideTo(getNewIndex(1, swiper.activeIndex, swiper.slides.length));
    nextSlideButton.classList.add('add-active')
    changeButtonActive(nextSlideButton);
    // swiper.slideTo((swiper.activeIndex + 1) % swiper.slides.length);
  })
  
  prevSlideButton.addEventListener('click', () => {
    // console.log(swiper);
    prevSlideButton.classList.add('add-active')
    changeButtonActive(prevSlideButton);
    swiper.slideTo(getNewIndex(-1, swiper.activeIndex, swiper.slides.length));
    
    // swiper.slideTo(swiper.activeIndex === 0 ? swiper.slides.length - 1 : swiper.activeIndex - 1);
  })
  
  const slides = block.querySelectorAll('.swiper-slide');
  for (let slide of slides) {
    const title = slide.querySelector('.next-slide-title');
    const nextTitle = slide.nextElementSibling.querySelector('.current-slide-title')?.innerHTML;
    if (nextTitle) {
      title.innerHTML = nextTitle;
    } else {
      const nextTitle = slides[0].querySelector('.current-slide-title')?.innerHTML;
      title.innerHTML = nextTitle;
    }
  }
  
  animations(block);
  imageLazyLoading(block);
  
};
windowOnLoad(blockScript);



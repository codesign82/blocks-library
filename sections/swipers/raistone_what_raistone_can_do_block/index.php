<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'what_raistone_can_do_block';
$className = 'what_raistone_can_do_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/what_raistone_can_do_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$subtitle = get_field('subtitle');
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<div class="container">
    <div class="what-can-do-wrapper">
        <div class="step-content">
            <?php if ($title) { ?>
                <div class="what-can-do-title">
                    <p
                            class="paragraph paragraph-m-paragraph what-can-do-sub-title iv-st-from-bottom"><?= $title ?></p>
                    <?php if ($subtitle) { ?>
                        <h2 class="headline-2 main-title iv-st-from-bottom"><?= $subtitle ?></h2>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php if (have_rows('vertical_sliders')) { ?>
                <div class="what-can-do-info">
                    <ul class="steps-dots-wrapper step-bullets iv-st-from-bottom">
                        <li class="progress"></li>
                        <?php while (have_rows('vertical_sliders')) {
                            the_row();
                            $step_title = get_sub_field('step_title');
                            ?>
                            <?php if ($step_title) { ?>
                                <li class="step-dot dot-link ">
                                    <?= $step_title ?>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
        </div>
        <?php if (have_rows('vertical_sliders')) { ?>
            <div class="steps-wrapper">
                <?php while (have_rows('vertical_sliders')) {
                    the_row();
                    $svg_or_image = get_sub_field('svg_or_image');
                    $svg = get_sub_field('svg');
                    $image = get_sub_field('image');
                    $background_color = get_sub_field('background_color');
                    $step_title = get_sub_field('step_title');
                    $description = get_sub_field('description');
                    $link = get_sub_field('link');
                    ?>
                    <div class="step">
                        <div class="info-content">
                            <?php if ($step_title) { ?>
                                <h5 class="headline-3 info-title iv-st-from-bottom"><?= $step_title ?></h5>
                            <?php } ?>
                            <?php if ($description) { ?>
                                <div class="paragraph paragraph-l-paragraph info-description iv-st-from-bottom"><?= $description ?></div>
                            <?php } ?>
                            <?php if ($link) { ?>
                                <a class="btn-arrow btn-raistone iv-st-from-bottom" href="<?= $link['url'] ?>"
                                   target="<?= $link['target'] ?>"><?= $link['title'] ?>
                                    <svg width="9" height="14" viewBox="0 0 9 14" fill="none">
                                        <path d="M1.03906 13L7.03906 7L1.03906 1" stroke="#0A0A0A"
                                              stroke-width="2" stroke-linecap="round"
                                              stroke-linejoin="round"/>
                                    </svg>
                                </a>
                            <?php } ?>
                        </div>
                        <?php if ($svg_or_image == 'image') { ?>
                            <div class="step-illustration iv-st-from-bottom"
                                 style="background-color: <?= $background_color ?>">
                                <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                            </div>
                        <?php } ?>
                        <?php if ($svg_or_image == 'svg') { ?>
                            <div class="step-illustration iv-st-from-bottom"
                                 style="background-color: <?= $background_color ?>">
                                <?= $svg ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>
</section>


<!-- endregion raistone's Block -->

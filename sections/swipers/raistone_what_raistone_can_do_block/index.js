import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/raistone';

import {gsap} from "gsap"
import {ScrollTrigger} from "gsap/ScrollTrigger";
import DrawSVGPlugin from "gsap/DrawSVGPlugin";

gsap.registerPlugin(ScrollTrigger, DrawSVGPlugin)

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.what_raistone_can_do_block');

  const steps = block.querySelectorAll('.step');
  const dots = block.querySelectorAll('.step-dot');

  const stepHeight = 500;
  const height = steps.length * stepHeight;

  for (let i = 0; i < dots.length; i++) {
    const dot = dots[i];
    dot.addEventListener('click', () => {
      window.scrollBy(0, block.getBoundingClientRect().top + i * stepHeight);
    })
  }

  const stepsWrapper = block.querySelector('.what-can-do-wrapper');
  ScrollTrigger.matchMedia({
    '(min-width:991px)': () => {
      let currentStep = 0;
      ScrollTrigger.create({
        trigger: stepsWrapper,
        start: 'top top',
        end: '+=' + height,
        pin: true,
        pinSpacing: true,
        scrub: true,
        animation: gsap.timeline().fromTo(block.querySelector('.what-can-do-wrapper .progress'), {
          opacity: 1,
          scaleY: 0,
          transformOrigin: 'top'
        }, {
          scaleY: 1,
          ease: 'linear',
          duration: steps.length - 1
        }).set({}, {}, steps.length),
        onUpdate(self) {
          if (self.progress === 1) return
          dots[currentStep].classList.remove('active');
          steps[currentStep].classList.remove('active');

          currentStep = ~~(self.progress * steps.length);
          dots[currentStep].classList.add('active');
          steps[currentStep].classList.add('active');
        }
      })

      return () => {
        for (let dot of dots) {
          dot.classList.remove('active');
        }
        for (let step of steps) {
          step.classList.remove('active');
        }
      }
    },
  })


  //// Animation

  const stepTwoArrow = block.querySelectorAll(".step-two-arrow")
  const chartWrapper = block.querySelectorAll(".chart-wrapper rect")
  const chartLine = block.querySelectorAll(".chart-line")
  const stepOneCardLines = block.querySelectorAll(".lines-wrapper path")
  const smallChartBackground = block.querySelector(".small-chart-background")
  const smallChartLine = block.querySelector(".small-chart-line")
  const smallChartCircle = block.querySelector(".small-chart-circle")
  const arrowLine = block.querySelector(".arrow-line")
  const arrowHead = block.querySelector(".arrow-head")
  const stepFourLine = block.querySelector(".step-four-line")
  const stepFourLogo = block.querySelector(".step-four-logo")
  const phoneContent = block.querySelector(".phone-content")
  const phoneCard = block.querySelector(".small-card-phone")
  const investmentBtn = block.querySelector(".investment-btn")
  const greenShape = block.querySelector(".green-line")
  const mobileShape = block.querySelector(".mobile-shape")

  const stepThreeLines = [...block.querySelectorAll(".step-three-svg rect")].reverse()
  const smallCard = block.querySelector(".small-card")
  const blueShape = block.querySelector(".blue-shape")
  const stepThreeLogo = block.querySelector(".raistone-logo")
  gsap.timeline({
    scrollTrigger: {
      trigger: steps[0],
      start: () => `top+=${0 - 20} top`,
      end: () => `top+=${stepHeight + 20} top`,
      toggleActions: 'restart none restart none',
    },
  })
      .from(chartLine, {
        scaleY: 0,
        transformOrigin: "bottom center",
        stagger: .2,
        duration: 1
      })
      .from(arrowLine, {
        drawSVG: 0
      }, "<50%")
      .from(arrowHead, {
        drawSVG: 0
      }, "<50%")
      .from(stepOneCardLines, {
        scaleX: 0,
        transformOrigin: "left center",
        stagger: .1,
        opacity: 0,
        yoyo: true
      }, 0)

  gsap.timeline({
    scrollTrigger: {
      trigger: steps[0],
      start: () => `top+=${stepHeight - 20} top`,
      end: () => `top+=${2 * stepHeight + 20} top`,
      toggleActions: 'restart none restart none',
    }
  })
      .fromTo(stepTwoArrow, {
        yPercent: 200,
      }, {
        yPercent: -200,
        stagger: .5,
        duration: 2,
      })
      .set(stepTwoArrow, {
        opacity: 0,
      })
      .fromTo(stepTwoArrow, {
        yPercent: 200,
      }, {
        opacity: 1,
        yPercent: 0,
        stagger: .5,
        duration: 2,
      }, "<80%")
      .from(chartWrapper, {
        scaleY: 0,
        transformOrigin: "bottom center",
        stagger: .05,
        ease: "power4",
      }, 0)


  gsap.timeline({
    scrollTrigger: {
      trigger: steps[0],
      start: () => `top+=${2 * stepHeight - 20} top`,
      end: () => `top+=${3 * stepHeight + 20} top`,
      toggleActions: 'restart none restart none',
    }
  })
      .from(stepThreeLogo, {
        scale: .3,
        opacity: 0,
        transformOrigin: "center center",
      })
      .from(stepThreeLines, {
        opacity: 0,
        scale: 1.1,
        transformOrigin: "center center",
        stagger: .03,
        ease: "power4"
      }, "<50%")
      .from(smallCard, {
        yPercent: 2,
        xPercent: -2,
        yoyo: true,
        duration: 1,
        ease: "linear"
      }, 0)
      .fromTo(blueShape, {
        rotate: -15
      }, {
        rotate: 0,
        yoyo: true,
        duration: 2,
        ease: "power3.inOut",
        transformOrigin: "top left"
      }, 0)

  gsap.timeline({
    scrollTrigger: {
      trigger: steps[0],
      start: () => `top+=${3 * stepHeight - 20} top`,
      end: () => `top+=${4 * stepHeight + 20} top`,
      toggleActions: 'restart none restart none',
    }, defaults: {
      duration: 1.5,
      ease: "power3"
    }
  })
      .from(stepFourLogo, {
        opacity: 0,
        scale: .7,
        transformOrigin: "center",
        duration: .5
      })
      .from(phoneContent, {
        opacity: 0,
        scale: 1.1,
        transformOrigin: "center",
        duration: .5
      }, "<50%")
      .from(phoneCard, {
        opacity: 0,
        scale: .9,
        transformOrigin: "center",
        duration: .5
      }, "<")
      .from(investmentBtn, {
        opacity: 0,
        scale: .7,
        transformOrigin: "center",
        duration: .5
      }, "<50%")
      .from(greenShape, {
        scaleX: 0,
        transformOrigin: "center left",
        duration: 1
      }, "<30%")
      .fromTo(smallChartBackground, {
        clipPath: "polygon(0% 0%, 0% 0%, 0% 100%, 0% 100%)"
      }, {
        clipPath: "polygon(0% 0%, 100% 0%, 100% 100%, 0% 100%)",
      }, "<30%")
      .from(smallChartLine, {
        drawSVG: 0,

      }, "<")
      .from(stepFourLine, {
        drawSVG: 0
      }, "<")
      .from(smallChartCircle, {
        scale: 0,
        transformOrigin: "center center",
        duration: .5,
        ease: "back"
      })


  gsap.fromTo(mobileShape, {
    xPercent: -5,
    yPercent: -5
  }, {
    xPercent: 5,
    yPercent: 5,
    yoyo: true,
    repeat: 2,
    duration: 2,
    ease: "power1.inOut"
  })

  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


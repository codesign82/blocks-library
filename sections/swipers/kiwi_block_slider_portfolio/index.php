<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'block-slider-portfolio d-flex flex-wrap flex-md-nowrap align-items-center align-items-xl-start ';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/block-slider-portfolio/screenshot1.png" >';
  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$button = get_field('button');

?>
<!-- region Kiwi's Block -->
<?php general_settings_for_blocks($id, $className); ?>
<div class="portfolio-images iv-st-from-bottom">
  <div class="swiper-container">
    <div class="swiper-wrapper">
      <?php
      while (have_rows('portfolio_images')) {
        the_row();
        $image = get_sub_field('image');
        ?>
        <div class="swiper-slide">
          <?php if ($image) { ?>
            <img alt="<?=$image['alt']?>" <?=acf_img($image['id'],'500px','medium')?>>
          <?php } ?>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<div class="portfolio-details circle-plus-line">
  <?php if ($title) { ?>
    <div class="wysiwyg-block title real-line-up">
      <?=$title?>
    </div>
  <?php } ?>
  <?php if ($button) { ?>
    <a class="btn iv-st-from-bottom" href="<?=$button['url']?>" target="<?=$button['target']?>">
      <div class="btn-border"></div>
      <div class="btn-bg"></div>
      <div class="btn-text"><?=$button['title']?></div>
    </a>
  <?php } ?>
  
  <div class="swiper-btns iv-st-from-left">
    <div class="swiper-button-next swiper-button">
      <div class="swiper-btn-bg"></div>
      <div class="swiper-btn-border"></div>
      <svg height="7" viewBox="0 0 13 7" width="13">
        <path d="M6.5 0L13 7H0z"/>
      </svg>
    </div>
    <div class="swiper-button-prev swiper-button">
      <div class="swiper-btn-bg"></div>
      <div class="swiper-btn-border"></div>
      <svg height="7" viewBox="0 0 13 7" width="13">
        <path d="M6.5 7L0 0h13z"/>
      </svg>
    </div>
  </div>
</div>
</section>
<!-- endregion Kiwi's Block -->

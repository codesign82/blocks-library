import './index.html';
import './style.scss';
// import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/wrap';
import Draggable from 'gsap/Draggable';
import InertiaPlugin from 'gsap/InertiaPlugin';
import Swiper, {EffectFade, Navigation} from 'swiper';
// import 'swiper/modules/effect-fade/effect-fade.scss';




Swiper.use([Navigation, EffectFade]);
const blockScript = async (container = document) => {
  const block = container.querySelector('.block-slider-circle-vertical-2');
  
  
  const blocks = container.querySelectorAll('.block-slider-portfolio');
  for (let block of blocks) {
    const imagesLoading = [];
    const slideImages = Array.from(block.querySelectorAll('.portfolio-images .swiper-slide img'));
    for (const slideImage of slideImages) {
      imagesLoading.push(new Promise(resolve => slideImage.complete ? resolve() : (slideImage.onload = resolve)));
    }
    const slider = new Swiper(block.querySelector('.portfolio-images .swiper-container'), {
      spaceBetween: 30,
      effect: 'fade',
      fadeEffect: {
        crossFade: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
    
    Promise.allSettled(imagesLoading).then(() => {
      console.log('sliderPortfolio');
      slider.update();
    });
  }
 



};
windowOnLoad(blockScript);




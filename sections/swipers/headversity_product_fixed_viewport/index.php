<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'product_fixed_viewport';

$className = 'product_fixed_viewport';

if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}

//if ($is_desktop) {
//  $className .= 'desktop';
//}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/product_fixed_viewport/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('pfv_title');
$top_button = get_field('top_button');


?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="pin-element">
        <?php if ($title) { ?>
            <h2 move-to-here
                class="headline-3 block-title  iv-st-from-bottom"> <?= $title ?>
                <?php if ($top_button) { ?>
                    <a class="btn top_button  iv-st-from-bottom"
                       href="<?= $top_button['url'] ?>"><?= $top_button['title'] ?></a>
                <?php } ?>

            </h2>
        <?php } ?>
        <div class="block-content">
            <div class="image-wrapper ">
                <?php if (have_rows('slides')) {
                    while (have_rows('slides')) {
                        the_row();
                        $left_image = get_sub_field('left_image');
                        $right_image = get_sub_field('right_image');
                        $rounded_left = get_sub_field('is_rounded_left');
                        $rounded_right = get_sub_field('is_rounded_right');
                        $orbs_color = get_sub_field('orbs_color');
                        $main_image = get_sub_field('main_image');
//          $rounded_left  = @$image_section['is_rounded_left'];
//          $rounded_right = @$image_section['is_rounded_right'];
                        $is_desktop = get_sub_field('is_desktop');
                        $orbs_color = get_sub_field('orbs_color');
                        ?>

                        <?php if ($main_image) { ?>
                            <div class="mobile-wrapper <?= get_row_index() === 1 ? 'active' : '' ?> <?= $is_desktop ? 'desktop' : "" ?> ">
                                <picture class=" pic-2 aspect-ratio">
                                    <?= get_acf_image($main_image, 'img-400-277') ?>
                                </picture>
                                <div class="ellipse ellipse-yellow  "
                                     style="background: radial-gradient(50% 50% at 50% 50%, <?= $orbs_color ?> 0%, rgba(255, 255, 255, 0) 100%);"
                                ></div>
                            </div>
                        <?php } ?>

                        <div class="text-screen-case <?= get_row_index() === 1 ? 'active' : '' ?> <?= $is_desktop ? 'desktop' : "" ?>">
                            <?php if ($left_image) { ?>
                                <picture
                                        class=" pic-1 small-card <?= $rounded_left ? "rounded" : "" ?>">
                                    <?php if ($rounded_left) { ?>
                                        <?= get_acf_image($left_image, 'img-173-173') ?>
                                    <?php } else { ?>
                                        <?= get_acf_image($left_image, 'img-205-84') ?>
                                    <?php } ?>
                                </picture>
                            <?php } ?>
                            <?php if ($right_image) { ?>
                                <picture
                                        class="small-card pic-3 <?= $rounded_right ? "rounded" : "" ?>">
                                    <?php if ($rounded_left) { ?>
                                        <?= get_acf_image($right_image, 'img-173-173') ?>
                                    <?php } else { ?>
                                        <?= get_acf_image($right_image, 'img-205-84') ?>
                                    <?php } ?>
                                </picture>
                            <?php } ?>
                        </div>
                    <?php }
                } ?>
                <div class="yellow-ring"></div>
            </div>
            <div class="text-overflow">
                <?php if (have_rows('slides')) {
                    while (have_rows('slides')) {
                        the_row();
                        $text_title = get_sub_field('text_title');
                        $text_description = get_sub_field('text_description');
                        $cta_button = get_sub_field('cta_button');
                        ?>
                        <div class="text-wrapper  <?= get_row_index() === 1 ? 'active' : '' ?>">
                            <h2
                                    class="section-title headline-4"><?= $text_title ?></h2>
                            <div
                                    class="section-desc paragraph"><?= $text_description ?></div>
                            <?php if ($cta_button) { ?>
                                <a class="btn"
                                   href="<?= $cta_button['url'] ?>"><?= $cta_button['title'] ?></a>
                            <?php } ?>
                        </div>
                    <?php }
                } ?>
            </div>
            <?php if (have_rows('slides')) { ?>
                <ul class="steps-dots-wrapper step-bullets">
                    <?php while (have_rows('slides')) {
                        the_row();
                        ?>
                        <li class="step-dot dot-link <?= get_row_index() === 1 ? 'active' : '' ?> "
                            role="radio" tabindex="0" aria-label="<?= __('slide', 'headversity') ?>">
                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>
        </div>
    </div>
</div>
</section>

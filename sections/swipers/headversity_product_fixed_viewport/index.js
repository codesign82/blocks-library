import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/propellerhealth';

import {gsap} from "gsap"
import {ScrollTrigger} from "gsap/ScrollTrigger";


gsap.registerPlugin(ScrollTrigger)
/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
    const blocks = container.querySelectorAll('.product_fixed_viewport');

    for (const block of blocks) {


        // const headerHeight = document.querySelector(".header-wrapper").clientHeight
        const steps = block.querySelectorAll('.text-screen-case');
        const stepsChildren = block.querySelectorAll('.text-screen-case picture');
        const ellipse = block.querySelectorAll('.ellipse');
        const textWrapper = block.querySelectorAll('.text-wrapper');
        const mobileWrapper = block.querySelectorAll('.mobile-wrapper');
        let textBlocks = block.querySelectorAll('.text-wrapper');
        const stepHeight = 500;
        const height = steps.length * stepHeight;
        const dots = block.querySelectorAll('.step-dot');
        const stepsWrapper = block.querySelector('.pin-element');

        let currentStep = 0;
        const stepsAnimation = gsap.timeline();
        for (let step of steps) {
            const pictures = step.querySelectorAll('picture')
            stepsAnimation.add(
                gsap.timeline({defaults: {ease: "power1"}})
                    .fromTo(pictures[0], {x: -10, y: 10}, {x: 0, y: 0})
                    .fromTo(pictures[1], {x: 10, y: 10}, {x: 0, y: 0}, 0)
            )
        }

        const scrollTrigger = ScrollTrigger.create({
            trigger: stepsWrapper,
            start: `center center`,
            end: '+=' + height,
            pin: true,
            pinSpacing: true,
            scrub: .25,
            animation: stepsAnimation,
            onUpdate(self) {
                if (self.progress === 1 || currentStep === ~~(self.progress * steps.length)) return
                dots[currentStep].classList.remove('active');
                ellipse[currentStep].classList.remove('active');
                textWrapper[currentStep].classList.remove('active');
                steps[currentStep].classList.remove('active');
                mobileWrapper[currentStep]?.classList.remove('active');
                currentStep = ~~(self.progress * steps.length);
                dots[currentStep].classList.add('active');
                ellipse[currentStep].classList.add('active');
                textWrapper[currentStep].classList.add('active');
                steps[currentStep].classList.add('active');
                mobileWrapper[currentStep]?.classList.add('active');
            }
        })

        for (let i = 0; i < dots.length; i++) {
            const dot = dots[i];
            dot.addEventListener('click', () => {
                window.scrollTo(0, scrollTrigger.start + i * stepHeight * 1.05)
            })
        }

        animations(block);
        imageLazyLoading(block);
    }
};
windowOnLoad(blockScript);


<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'testimonials_slider_block';
$className = 'testimonials_slider_block no-side-border';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/testimonials_slider_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$section_title = get_field('section_title');
?>
<!-- region CyberHill's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="testimonial-wrapper">
    <?php if ($section_title) { ?>
      <div class="title-with-shape title-with-shape-blue headline-6 iv-st-from-bottom">
        <?= $section_title ?>
      </div>
    <?php } ?>

    <?php
    $testimonials = get_field('testimonials');
    if ($testimonials): ?>
      <div class="swiper swiper-container">
        <div class="swiper-wrapper">
          <?php foreach ($testimonials as $testimonial):
            $image = get_field('image', $testimonial);
            $name = get_field('name', $testimonial);
            $job_title = get_field('job_title', $testimonial);
            $quote = get_field('quote', $testimonial);
            ?>
            <div class="swiper-slide iv-st-from-bottom">
              <div class="content">
                <div class="quote-icon-and-text">
                  <div class="headline-2 quote-text" data-text="”">
                    <?= $quote ?>
                  </div>
                </div>
                <div class="picture-and-information">
                  <?php if ($image) { ?>
                    <picture class="profile-image">
                      <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>"/>
                    </picture>
                  <?php } ?>
                  <?php if ($name) { ?>
                    <div class="author-info"
                      <?php if (!$image) { ?>
                      style="margin-left: 2.3rem"
                      <?php } ?>>
                      <div class="small-text-14 name"><?= $name ?></div>
                      <div class="paragraph-14 postion"><?= $job_title ?></div>
                    </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    <?php endif; ?>

    <div class="swiper-buttons iv-st-from-bottom">
      <div class="swiper-button swiper-button-prev">
        <svg width="24" height="14" viewBox="0 0 24 14" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M8.03467 13.2058L2.26815 7.60735L24 7.60735V6.48433L2.17373 6.48433L8.03467 0.794195L7.21706 0L0.824749 6.20602L0.817617 6.1991L1.90735e-06 6.99329L0.00692368 7.00001L1.90735e-06 7.00673L0.817617 7.80093L0.824749 7.794L7.21706 14L8.03467 13.2058Z" fill="#82FA81"/>
        </svg>
      </div>
      <div class="swiper-button swiper-button-next">
        <svg width="25" height="15" viewBox="0 0 25 15" fill="none"
             xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd"
                d="M16.2524 1.29418L21.9363 6.89262L0.515991 6.89262V8.01564L22.0294 8.01564L16.2524 13.7058L17.0583 14.5L23.359 8.29398L23.366 8.3009L24.1719 7.50671L24.1651 7.49999L24.1719 7.49327L23.366 6.69907L23.359 6.706L17.0583 0.499981L16.2524 1.29418Z"
                fill="#82FA81"/>
        </svg>
      </div>
    </div>
    <!-- Add Pagination -->
    <div class="swiper-paginations swiper-paginations-white iv-st-from-bottom"></div>
  </div>
</div>

</section>


<!-- endregion CyberHill's Block -->

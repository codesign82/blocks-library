import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/cyberhill';
import Swiper, {Pagination, Navigation} from "swiper";

Swiper.use([Pagination, Navigation]);
const blockScript = async (container = document) => {
  const block = container.querySelector('.testimonials_slider_block');
  
  const quotes = block.querySelectorAll('.quote-text p');
  console.log(quotes)
  for (let quote of quotes) {
    let quoteString = quote.textContent;
    let dateSpan = document.createElement('span')
    quote.innerHTML += "<span>”</span>";
  }
  
  // add block code here
  let swiper = new Swiper(block.querySelector('.swiper'), {
    loop: true,
    autoHeight: true,
    pagination: {
      el: ".swiper-paginations",
      clickable: true,
    },
    navigation: {
      nextEl: block.querySelector('.swiper-button-next'),
      prevEl: block.querySelector('.swiper-button-prev')
    },
    
  });
  
  
  
  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



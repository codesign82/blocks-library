import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {debounce} from "../../../scripts/functions/debounce";
import '../../../scripts/sitesSizer/propellerhealth';

import {Swiper, Navigation, Pagination} from "swiper";

import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger)



const blockScript = async (container = document) => {
  const block = container.querySelector('.testimonials_block');

  const swiperControllers = block.querySelector('.swiper-arrows-container');
  const slides = block.querySelectorAll('.swiper-slide');
  const breakpoint = window.matchMedia('(min-width:600px)');
  const swiperChecker = breakpoint.matches ? {
    autoHeight: false,
  } : {
    autoHeight: true,
  };

  const swiper = new Swiper(block.querySelector(".mySwiper"), {
    observer: true,
    centeredSlides: true,
    observerParents: true,
    spaceBetween: '5%',
    modules: [Navigation, Pagination],
    navigation: {
      nextEl: block.querySelector(".swiper-button-next"),
      prevEl: block.querySelector(".swiper-button-prev"),
    },
    pagination: {
      el: block.querySelector('.swiper-pagination'),
      type: 'bullets',
      clickable: true,

    },
    ...swiperChecker,
    breakpoints: {
      300:
          {
            slidesPerView: 1.12,
            spaceBetween: 18,

          },
      600:
          {
            // centeredSlides: true,
            slidesPerView: 1.58,
            spaceBetween: 53,
            slidesOffsetBefore: 0,
          }
    },
  });
  const desktopSlopValue = 8.333334;
  const tabletSlopValue = 5.04;
  const mobileSlopValue = (parseFloat(window.getComputedStyle(block.querySelector(".container"), null).getPropertyValue('padding-left')) / window.innerWidth) * 100;
  ;
  let pathSlopValue = desktopSlopValue;
  const fixPaths = debounce(() => {
    pathSlopValue = window.innerWidth >= 992
        ? desktopSlopValue
        : window.innerWidth >= 600
            ? tabletSlopValue
            : mobileSlopValue;
    const path = block.querySelector('.slides-bg path')
    path.setAttribute('d', `M 0 100 L ${pathSlopValue} 0 L ${100 - pathSlopValue} 0 L 100 100 Z`);
  }, 300);

  fixPaths();
  window.addEventListener('resize', fixPaths)

  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



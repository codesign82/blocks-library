<?php
$post_id = @$args['post_id'] ?: get_the_ID();
$description = get_field('description', $post_id);
$company_logo = get_field('company_logo', $post_id);
$company_name = get_field('company_name', $post_id);
?>
<div class="swiper mySwiper iv-st-from-right">
  <div class="swiper-wrapper ">
      <div class="swiper-slide">
        <div class="right-content">
          <div class="card">
            <svg width="32" height="26" viewBox="0 0 32 26" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M14.1616 4.49985L12.9136 0.563847C5.71356 2.48384 0.913557 9.39585 0.913557 17.9398C0.913557 22.9318 3.21756 25.8118 7.05756 25.8118C10.3216 25.8118 12.7216 23.4118 12.7216 20.0518C12.7216 16.9798 10.2256 14.6758 7.05756 14.6758H6.38556C6.76956 9.87585 9.74556 5.84385 14.1616 4.49985ZM24.6256 14.6758H23.9536C24.3376 9.87585 27.3136 5.84385 31.7296 4.49985L30.4816 0.563847C23.2816 2.48384 18.4816 9.39585 18.4816 17.9398C18.4816 22.9318 20.7856 25.8118 24.6256 25.8118C27.8896 25.8118 30.2896 23.4118 30.2896 20.0518C30.2896 16.9798 27.7936 14.6758 24.6256 14.6758Z" fill="#086875"/>
            </svg>
            <?php if ($description) { ?>
              <div class="right-description">
                <?= $description ?>
              </div>
            <?php } ?>
            <div class="line"></div>
            <div class="company-wrapper">
              <?php if ($company_logo) { ?>
                <picture class="company-logo">
                  <img src="<?= $company_logo['url'] ?>" alt="<?= $company_logo['alt'] ?>">
                </picture>
              <?php } ?>
              <?php if ($company_name) { ?>
                <h6 class="paragraph company-name">
                  <?=$company_name ?>
                </h6>
              <?php } ?>
            </div>
          </div>
        </div>

      </div>
  </div>
</div>

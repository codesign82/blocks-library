<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'testimonials_block';
$className = 'testimonials_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/testimonials_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_title = get_field('main_title');
$main_description = get_field('main_description');
$automatically_or_manual = get_field('automatically_or_manual');
$testimonials = get_field('testimonials');
$query_options = get_field('query_options');
$order = @$query_options['order'];
$posts_per_page = @$query_options['number_of_posts'];
$args = array(
    'post_type' => 'testimonials',
    'posts_per_page' => $posts_per_page,
    'order' => $order,
    'post_status' => 'publish'
);
// The Query
$the_query = new WP_Query($args);
$have_posts = $the_query->have_posts();


?>
<!-- region samrt_dv Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="slides-bg">
    <svg preserveAspectRatio="none" viewBox="0 0 100 100">
        <path d="M -12.4 20 L 9.3 0 L 90.7 0 L 112.4 20 L 0 20 Z"></path>
    </svg>
</div>

<div class="container">
    <div class="testimonials-wrapper">
        <div class="left-content">
            <?php if ($main_title) { ?>
                <div class="headline-2 left-title word-up">
                    <?= $main_title ?>
                </div>
            <?php } ?>
            <?php if ($main_description) { ?>
                <div class="paragraph left-description iv-st-from-bottom">
                    <?= $main_description ?>
                </div>
            <?php } ?>
            <div class="swiper-arrows-container">
                <svg class="swiper-button swiper-button-next arrow-left same-hover iv-st-from-bottom" width="63"
                     height="63" viewBox="0 0 63 63" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="31.5" cy="31.5" r="30.5" stroke="#BFD730" stroke-width="2"/>
                    <path d="M18.8163 30.3495C18.4258 30.7401 18.4258 31.3732 18.8163 31.7637L25.1803 38.1277C25.5708 38.5182 26.204 38.5182 26.5945 38.1277C26.985 37.7372 26.985 37.104 26.5945 36.7135L20.9377 31.0566L26.5945 25.3998C26.985 25.0093 26.985 24.3761 26.5945 23.9856C26.204 23.595 25.5708 23.595 25.1803 23.9856L18.8163 30.3495ZM19.5234 32.0566H44.3685V30.0566H19.5234V32.0566Z"
                          fill="#313545"/>
                </svg>
                <svg class="swiper-button swiper-button-prev arrow-right same-hover iv-st-from-bottom" width="63"
                     height="63" viewBox="0 0 63 63" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="31.5" cy="31.5" r="30.5" transform="rotate(-180 31.5 31.5)" stroke="#BFD730"
                            stroke-width="2"/>
                    <path d="M44.1837 32.6505C44.5742 32.2599 44.5742 31.6268 44.1837 31.2363L37.8197 24.8723C37.4292 24.4818 36.796 24.4818 36.4055 24.8723C36.015 25.2628 36.015 25.896 36.4055 26.2865L42.0623 31.9434L36.4055 37.6002C36.015 37.9907 36.015 38.6239 36.4055 39.0144C36.796 39.405 37.4292 39.405 37.8197 39.0144L44.1837 32.6505ZM43.4766 30.9434L18.6315 30.9434V32.9434H43.4766V30.9434Z"
                          fill="#313545"/>
                </svg>
            </div>
        </div>
        <?php if ($automatically_or_manual) { ?>
            <?php if ($testimonials) { ?>
                <div class="swiper mySwiper iv-st-from-right">
                    <div class="swiper-wrapper ">
                        <?php foreach ($testimonials as $testimonial):
                            $description = get_field('description', $testimonial);
                            $company_logo = get_field('company_logo', $testimonial);
                            $company_name = get_field('company_name', $testimonial);
                            ?>
                            <div class="swiper-slide">
                                <div class="right-content">
                                    <div class="card">
                                        <svg width="32" height="26" viewBox="0 0 32 26" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path d="M14.1616 4.49985L12.9136 0.563847C5.71356 2.48384 0.913557 9.39585 0.913557 17.9398C0.913557 22.9318 3.21756 25.8118 7.05756 25.8118C10.3216 25.8118 12.7216 23.4118 12.7216 20.0518C12.7216 16.9798 10.2256 14.6758 7.05756 14.6758H6.38556C6.76956 9.87585 9.74556 5.84385 14.1616 4.49985ZM24.6256 14.6758H23.9536C24.3376 9.87585 27.3136 5.84385 31.7296 4.49985L30.4816 0.563847C23.2816 2.48384 18.4816 9.39585 18.4816 17.9398C18.4816 22.9318 20.7856 25.8118 24.6256 25.8118C27.8896 25.8118 30.2896 23.4118 30.2896 20.0518C30.2896 16.9798 27.7936 14.6758 24.6256 14.6758Z"
                                                  fill="#086875"/>
                                        </svg>

                                        <?php if ($description) { ?>
                                            <div class="right-description">
                                                <?= $description ?>
                                            </div>
                                        <?php } ?>
                                        <div class="line"></div>
                                        <div class="company-wrapper">
                                            <?php if ($company_logo) { ?>
                                                <picture class="company-logo">
                                                    <?= get_acf_image($company_logo, 'img-43-43') ?>
                                                </picture>
                                            <?php } ?>
                                            <?php if ($company_name) { ?>
                                                <div class="paragraph company-name">
                                                    <?= $company_name ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php } ?>
        <?php } else { ?>
            <div class="swiper mySwiper iv-st-from-right">
                <div class="swiper-wrapper">
                    <?php while ($the_query->have_posts()) {
                        $the_query->the_post();
                        $description = get_field('description', get_the_ID());
                        $company_logo = get_field('company_logo', get_the_ID());
                        $company_name = get_field('company_name', get_the_ID());
                        ?>
                        <div class="swiper-slide">
                            <div class="right-content">
                                <div class="card">
                                    <svg width="6" height="5" viewBox="0 0 6 5" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path d="M2.4959 0.127991L1.7919 3.08799L1.6639 2.39999C1.9199 2.39999 2.1279 2.47999 2.2879 2.63999C2.4479 2.78932 2.5279 2.99199 2.5279 3.24799C2.5279 3.50399 2.4479 3.71199 2.2879 3.87199C2.1279 4.03199 1.93057 4.11199 1.6959 4.11199C1.4399 4.11199 1.2319 4.03199 1.0719 3.87199C0.922567 3.70132 0.8479 3.49332 0.8479 3.24799C0.8479 3.15199 0.853233 3.06666 0.8639 2.99199C0.874567 2.91732 0.8959 2.82666 0.9279 2.71999C0.9599 2.61332 1.00257 2.48532 1.0559 2.33599L1.7279 0.127991H2.4959ZM5.2159 0.127991L4.5119 3.08799L4.3839 2.39999C4.6399 2.39999 4.8479 2.47999 5.0079 2.63999C5.1679 2.78932 5.2479 2.99199 5.2479 3.24799C5.2479 3.50399 5.1679 3.71199 5.0079 3.87199C4.8479 4.03199 4.65057 4.11199 4.4159 4.11199C4.1599 4.11199 3.9519 4.03199 3.7919 3.87199C3.64257 3.70132 3.5679 3.49332 3.5679 3.24799C3.5679 3.15199 3.57323 3.06666 3.5839 2.99199C3.59457 2.91732 3.6159 2.82666 3.6479 2.71999C3.6799 2.61332 3.72257 2.48532 3.7759 2.33599L4.4479 0.127991H5.2159Z"
                                              fill="#086875"/>
                                    </svg>
                                    <?php if ($description) { ?>
                                        <div class="right-description">
                                            <?= $description ?>
                                        </div>
                                    <?php } ?>
                                    <div class="line"></div>
                                    <div class="company-wrapper">
                                        <?php if ($company_logo) { ?>
                                            <picture class="company-logo">
                                                <img src="<?= $company_logo['url'] ?>"
                                                     alt="<?= $company_logo['alt'] ?>">
                                            </picture>
                                        <?php } ?>
                                        <?php if ($company_name) { ?>
                                            <div class="paragraph company-name">
                                                <?= $company_name ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <?php
                        ?>
                    <?php }
                    wp_reset_postdata(); ?>
                </div>
            </div>
            <?php $args['paged'] = 2;
            $args['posts_per_page'] = $posts_per_page; ?>
        <?php } ?>
    </div>
</div>
</section>


<!-- endregion samrt_dv Block -->

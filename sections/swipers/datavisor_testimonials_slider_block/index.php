<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'testimonials_slider_block';
$className = 'testimonials_slider_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/takeoff-code/gutenberg-blocks/testimonials_slider_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$manually_or_automatically = get_field('manually_or_automatically');

?>
<!-- region datavisor's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<?php if ($manually_or_automatically === 'automatically') { ?>
    <?php
    $query_options = get_field('query_options');
    $number_of_posts = @$query_options['number_of_posts'];
    $post_type = 'testimonials';
    $order = @$query_options['order'];
    $args = array(
        'post_type' => 'testimonials',
        'posts_per_page' => $number_of_posts,
        'order' => $order,
        'post_status' => 'publish',
    );
    $the_query = new WP_Query($args);
// The Loop
    $have_posts = $the_query->have_posts();
    if ($have_posts) {
        $i = 0;
        ?>
        <div class="testimonial-wrapper">
            <div class="swiper">
                <div class="swiper-wrapper">
                    <?php while ($the_query->have_posts()) {
                        $the_query->the_post();
                        $post_id = get_the_ID();
                        $image = get_field('picture', $post_id);
                        $logo_image = get_field('logo_image', $post_id);
                        $description = get_field('description', $post_id);
                        $name = get_field('name', $post_id);
                        $position = get_field('position', $post_id);
                        $company = get_field('company', $post_id);
                        ?>
                        <div class="test-wrapper swiper-slide">
                            <div class="card-wrapper">
                                <picture class="card-image">
                                    <img src="<?= $image ?>" alt="<?= __("Featured image for $name", 'datavisor') ?>">
                                </picture>
                                <div class="slide-content-wrapper">
                                    <picture class="logo">
                                        <img src="<?= $logo_image['url'] ?>" alt="<?= $logo_image['alt'] ?>">
                                    </picture>
                                    <div class="description"><?= $description ?></div>
                                    <div class="name"><?= $name ?></div>
                                    <div class="position-wrapper">
                                        <div><?= $position . ',' ?></div>
                                        <div><?= $company ?></div>
                                    </div>
                                    <div class="swiper-pagination">
                                        <?php
                                        for ($x = 0; $x < $the_query->found_posts; $x++) { ?>
                                            <span data-slide="<?= $x ?>"
                                                  class="swiper-pagination-bullet<?= $x === $i ? ' swiper-pagination-bullet-active' : '' ?>"></span>
                                        <?php }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $i++;
                    } ?>
                </div>

            </div>
        </div>
    <?php } else { ?>
        <div class="no-posts">No Posts Available</div>
    <?php }
    /* Restore original Post Data */
    wp_reset_postdata(); ?>

<?php } else { ?>
    <?php
    $testimonials = get_field('posts');
    if ($testimonials) {
        $i = 0;
        ?>
        <div class="">
            <div class="swiper">
                <div class="swiper-wrapper">
                    <?php foreach ($testimonials as $testimonial) {
                        $image = get_field('picture', $testimonial);
                        $logo_image = get_field('logo_image', $testimonial);
                        $description = get_field('description', $testimonial);
                        $name = get_field('name', $testimonial);
                        $position = get_field('position', $testimonial);
                        $company = get_field('company', $testimonial);
                        ?>
                        <div class="test-wrapper swiper-slide">
                            <div class="card-wrapper">
                                <?php if ($image) { ?>
                                    <picture class="card-image">
                                        <img src="<?= $image ?>"
                                             alt="<?= __("Featured image for $name", 'datavisor') ?>">
                                    </picture>
                                <?php } ?>
                                <div class="slide-content-wrapper">
                                    <?php if ($logo_image) { ?>
                                        <picture class="logo">
                                            <img src="<?= $logo_image['url'] ?>" alt="<?= $logo_image['alt'] ?>">
                                        </picture>
                                    <?php } ?>
                                    <?php if ($description) { ?>
                                        <div class="description"><?= $description ?></div>
                                    <?php } ?>
                                    <?php if ($name) { ?>
                                        <div class="name"><?= $name ?></div>
                                    <?php } ?>
                                    <div class="position-wrapper">
                                        <?php if ($position) { ?>
                                            <div><?= $position ?></div>
                                        <?php } ?>
                                        <?php if ($company) { ?>
                                            <div class="position-company"><?= $company ?></div>
                                        <?php } ?>
                                    </div>
                                    <div class="swiper-pagination">
                                        <?php
                                        for ($x = 0; $x < count($testimonials); $x++) { ?>
                                            <span data-slide="<?= $x ?>"
                                                  class="swiper-pagination-bullet<?= $x === $i ? ' swiper-pagination-bullet-active' : '' ?>"></span>
                                        <?php }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $i++;
                        ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>

</section>


<!-- endregion datavisor's Block -->

import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/propellerhealth';
import {Swiper} from "swiper";

const blockScript = async (container = document) => {
  const block = container.querySelector('.testimonials_slider_block');




  //region swiper for testimonials
  const testSwiper = new Swiper('.testimonials_slider_block .swiper', {

    // Optional parameters
    loop: true,
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
    threshold: 10,
    // If we need pagination
    // pagination: {
    //     el: '.swiper-pagination',
    //     clickable: true,
    // },

  });
  for (const slide of testSwiper.slides) {
    slide.addEventListener('click', e => {
      const slideTo = e.target.closest('.swiper-pagination-bullet');
      slideTo && testSwiper.slideToLoop(+slideTo.dataset.slide)
    })
  }


//endregion
  
  animations(block);
  imageLazyLoading(block);
  
};
windowOnLoad(blockScript);



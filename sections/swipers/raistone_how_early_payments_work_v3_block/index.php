<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'how_early_payments_work_v3_block';
$className = 'how_early_payments_work_v3_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/how_early_payments_work_v3_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$sub_title = get_field('small_title');
$main_title = get_field('main_title');
$main_description = get_field('description');
$toggle_button = get_field('toggle_button');
$timeline = get_field('timeline');
$timeline_text = @$timeline['timeline_text'];
$first_timeline_button_text = @$timeline['first_timeline_button_text'];
$second_timeline_button_text = @$timeline['second_timeline_button_text'];
$first_timeline = $timeline['first_timeline'];
$second_timeline = $timeline['second_timeline'];
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="content-wrapper">
        <?php if ($sub_title) { ?>
            <h6 class="headline-6 content-wrapper-sub-title"><?= $sub_title ?></h6>
        <?php } ?>
        <?php if ($main_title) { ?>
            <h3 class="headline-2 content-wrapper-title"><?= $main_title ?></h3>
        <?php } ?>
        <?php if ($main_description) { ?>
            <div
                    class="paragraph paragraph-xl-paragraph content-wrapper-description">
                <?= $main_description ?>
            </div>
        <?php } ?>
        <?php if ($first_timeline_button_text || $second_timeline_button_text) { ?>
            <ul class="toggle-action">
                <?php if ($first_timeline_button_text) { ?>
                    <li class="headline-6 toggle-btn tab-1 active-btn"><?= $first_timeline_button_text ?></li>
                <?php } ?>
                <?php if ($second_timeline_button_text) { ?>
                    <li class="headline-6 toggle-btn tab-2"><?= $second_timeline_button_text ?></li>
                <?php } ?>
            </ul>
        <?php } ?>
        <?php if ($timeline_text) { ?>
            <div class="headline-6 small-title">
                <?= $timeline_text ?>
            </div>
        <?php } ?>
    </div>
    <?php if (have_rows('timeline')): ?>
        <?php if ($first_timeline) { ?>
            <div class="tab-content tab-content-1 tab-content-active">
                <div class="how-early-wrapper how-early-wrapper-first">
                    <div class="swiper mySwiper first-timeline-swiper">
                        <div class="swiper-wrapper">
                            <?php
                            while (have_rows('timeline')) : the_row();
                                if (have_rows('first_timeline')): while (have_rows('first_timeline')) : the_row();
                                    $main_icon = get_sub_field('main_icon');
                                    $title = get_sub_field('title');
                                    $description = get_sub_field('description');
                                    $subtext = get_sub_field('subtext');
                                    $day = get_sub_field('day');
                                    $day_icon = get_sub_field('day_icon');
                                    $day_text = get_sub_field('day_text');
                                    ?>
                                    <div class="swiper-slide">
                                        <div class="what-can-do-wrapper">
                                            <div class="background"></div>
                                            <div class="step-illustration">
                                                <svg class="raistone-logo main-logo" width="1187" height="1188"
                                                     viewBox="0 0 1187 1188" fill="none">
                                                    <g class="logo-shapes" opacity="0.1">
                                                        <path
                                                                d="M0 593.434C0 832.077 141.001 1037.91 344.169 1132.1V54.8992C141.001 149.086 0 354.79 0 593.434Z"
                                                                fill="#FBBF49"/>
                                                        <path
                                                                d="M805.002 563.933C837.144 549.646 868.095 532.846 897.459 513.532C1005.66 442.363 1058.3 361.536 1058.3 266.555C1058.3 250.152 1056.58 234.145 1053.27 218.536C944.415 85.0596 778.944 0 593.368 0C558.316 0 524.058 3.17485 490.726 8.99541V406.117C521.148 389.316 556.2 379.792 593.368 379.792C701.169 379.792 790.452 460.089 805.002 563.933Z"
                                                                fill="#FBBF49"/>
                                                        <path
                                                                d="M490.726 1178.01C524.058 1183.83 558.316 1187 593.368 1187C704.873 1187 808.97 1156.18 898.121 1102.74L490.726 821.761V1178.01Z"
                                                                fill="#FBBF49"/>
                                                        <path
                                                                d="M981.451 598.329C917.828 638.015 851.693 668.838 783.441 691.062C759.764 736.832 720.083 773.079 671.804 792.128L1003.54 1022.17C1116.5 914.095 1187 761.967 1187 593.435C1187 534.435 1178.27 477.553 1162.26 423.845C1125.36 487.871 1065.05 546.209 981.451 598.329Z"
                                                                fill="#FBBF49"/>
                                                    </g>
                                                </svg>
                                                <div class="step-wrapper">
                                                    <div class="left-content">
                                                        <?php if ($main_icon) { ?>
                                                            <?= $main_icon ?>
                                                        <?php } ?>
                                                        <?php if ($day_text) { ?>
                                                            <div class="headline-6 left-title"><?= $day . ' ' . $day_text ?></div>
                                                        <?php } ?>
                                                        <?php if ($title) { ?>
                                                            <p class="headline-2 left-text"><?= $title ?></p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="right-content">
                                                        <?php if ($description) { ?>
                                                            <div class="description paragraph-l-paragraph">
                                                                <?= $description ?>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if ($subtext) { ?>
                                                            <div class="example-text">
                                                                <?= $subtext ?>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                <?php
                                endwhile; endif;
                            endwhile; ?>
                        </div>
                        <div class="swiper-button-next">

                            <svg class="right-arrow" width="46" height="79" viewBox="0 0 46 79" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M6 6L39.5 39.5L6 73" stroke="#521437" stroke-width="11.1667"
                                      stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </div>
                        <div class="swiper-button-prev">
                            <svg class="left-arrow" width="46" height="79" viewBox="0 0 46 79" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M39.5 73L6 39.5L39.5 6" stroke="#521437" stroke-width="11.1667"
                                      stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </div>
                    </div>
                    <div class="icons-wrapper">
                        <?php
                        while (have_rows('timeline')) : the_row();

                            if (have_rows('first_timeline')): while (have_rows('first_timeline')) : the_row();
                                $main_icon = get_sub_field('main_icon');
                                $title = get_sub_field('title');
                                $description = get_sub_field('description');
                                $subtext = get_sub_field('subtext');
                                $day = get_sub_field('day');
                                $day_icon = get_sub_field('day_icon');
                                $day_text = get_sub_field('day_text');
                                $row_index = get_row_index();
                                ?>
                                <div class="step-title-circle first-step <?= $row_index === 1 ? 'active-step' : '' ?>">
                                    <?php if ($day_icon) { ?>
                                        <div class="icon-wrapper">
                                            <?= $day_icon ?>
                                        </div>
                                    <?php } ?>
                                    <div class="step-title paragraph paragraph-s-paragraph">
                                        <?php if ($day) { ?>
                                            <strong><?= $day ?> </strong>
                                        <?php } ?>
                                        <?php if ($day_text) { ?>
                                            <span><?= $day_text ?></span>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php
                            endwhile; endif;
                        endwhile; ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if ($second_timeline) { ?>
            <div class="tab-content tab-content-2">
                <div class="how-early-wrapper how-early-wrapper-second">
                    <div class="swiper mySwiper second-timeline-swiper">
                        <div class="swiper-wrapper">
                            <?php
                            while (have_rows('timeline')) : the_row();
                                if (have_rows('second_timeline')): while (have_rows('second_timeline')) : the_row();
                                    $main_icon = get_sub_field('main_icon');
                                    $title = get_sub_field('title');
                                    $description = get_sub_field('description');
                                    $subtext = get_sub_field('subtext');
                                    $day = get_sub_field('day');
                                    $day_icon = get_sub_field('day_icon');
                                    $day_text = get_sub_field('day_text');
                                    ?>
                                    <div class="swiper-slide">
                                        <div class="what-can-do-wrapper">
                                            <div class="background"></div>
                                            <div class="step-illustration">
                                                <svg class="raistone-logo main-logo" width="1187" height="1188"
                                                     viewBox="0 0 1187 1188" fill="none">
                                                    <g class="logo-shapes" opacity="0.1">
                                                        <path
                                                                d="M0 593.434C0 832.077 141.001 1037.91 344.169 1132.1V54.8992C141.001 149.086 0 354.79 0 593.434Z"
                                                                fill="#FBBF49"/>
                                                        <path
                                                                d="M805.002 563.933C837.144 549.646 868.095 532.846 897.459 513.532C1005.66 442.363 1058.3 361.536 1058.3 266.555C1058.3 250.152 1056.58 234.145 1053.27 218.536C944.415 85.0596 778.944 0 593.368 0C558.316 0 524.058 3.17485 490.726 8.99541V406.117C521.148 389.316 556.2 379.792 593.368 379.792C701.169 379.792 790.452 460.089 805.002 563.933Z"
                                                                fill="#FBBF49"/>
                                                        <path
                                                                d="M490.726 1178.01C524.058 1183.83 558.316 1187 593.368 1187C704.873 1187 808.97 1156.18 898.121 1102.74L490.726 821.761V1178.01Z"
                                                                fill="#FBBF49"/>
                                                        <path
                                                                d="M981.451 598.329C917.828 638.015 851.693 668.838 783.441 691.062C759.764 736.832 720.083 773.079 671.804 792.128L1003.54 1022.17C1116.5 914.095 1187 761.967 1187 593.435C1187 534.435 1178.27 477.553 1162.26 423.845C1125.36 487.871 1065.05 546.209 981.451 598.329Z"
                                                                fill="#FBBF49"/>
                                                    </g>
                                                </svg>
                                                <div class="step-wrapper">
                                                    <div class="left-content">
                                                        <?php if ($main_icon) { ?>
                                                            <?= $main_icon ?>
                                                        <?php } ?>
                                                        <?php if ($day_text) { ?>
                                                            <div class="headline-6 left-title"><?= $day . ' ' . $day_text ?></div>
                                                        <?php } ?>
                                                        <?php if ($title) { ?>
                                                            <p class="headline-2 left-text"><?= $title ?></p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="right-content">
                                                        <?php if ($description) { ?>
                                                            <ul>
                                                                <li class="paragraph-l-paragraph">
                                                                    <?= $description ?>
                                                                </li>

                                                            </ul>
                                                        <?php } ?>
                                                        <?php if ($subtext) { ?>
                                                            <div class="example-text">
                                                                <?= $subtext ?>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                endwhile; endif;
                            endwhile; ?>
                        </div>
                        <div class="swiper-button-next">

                            <svg class="right-arrow" width="46" height="79" viewBox="0 0 46 79" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M6 6L39.5 39.5L6 73" stroke="#521437" stroke-width="11.1667"
                                      stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </div>
                        <div class="swiper-button-prev">
                            <svg class="left-arrow" width="46" height="79" viewBox="0 0 46 79" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M39.5 73L6 39.5L39.5 6" stroke="#521437" stroke-width="11.1667"
                                      stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </div>
                    </div>
                    <div class="icons-wrapper">
                        <?php
                        while (have_rows('timeline')) : the_row();

                            if (have_rows('second_timeline')): while (have_rows('second_timeline')) : the_row();
                                $main_icon = get_sub_field('main_icon');
                                $title = get_sub_field('title');
                                $description = get_sub_field('description');
                                $subtext = get_sub_field('subtext');
                                $day = get_sub_field('day');
                                $day_icon = get_sub_field('day_icon');
                                $day_text = get_sub_field('day_text');
                                $row_index = get_row_index();
                                ?>
                                <div class="step-title-circle first-step <?= $row_index === 1 ? 'active-step' : '' ?>">
                                    <?php if ($day_icon) { ?>
                                        <div class="icon-wrapper">
                                            <?= $day_icon ?>
                                        </div>
                                    <?php } ?>
                                    <div class="step-title paragraph paragraph-s-paragraph">
                                        <?php if ($day) { ?>
                                            <strong><?= $day ?> </strong>
                                        <?php } ?>
                                        <?php if ($day_text) { ?>
                                            <span><?= $day_text ?></span>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php
                            endwhile; endif;
                        endwhile; ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php endif; ?>

    <?php if (have_rows('timeline')): ?>
        <?php if ($first_timeline) { ?>
            <div class="tab-content  tab-mobile-content-1 tab-content-active">
                <div class="how-early-wrapper-mobile">
                    <div class="what-can-do-wrapper">
                        <div class="step-illustration-wrapper">
                            <svg class="raistone-logo main-logo" width="1187" height="1188"
                                 viewBox="0 0 1187 1188" fill="none">
                                <g class="logo-shapes" opacity="0.1">
                                    <path
                                            d="M0 593.434C0 832.077 141.001 1037.91 344.169 1132.1V54.8992C141.001 149.086 0 354.79 0 593.434Z"
                                            fill="#FBBF49"/>
                                    <path
                                            d="M805.002 563.933C837.144 549.646 868.095 532.846 897.459 513.532C1005.66 442.363 1058.3 361.536 1058.3 266.555C1058.3 250.152 1056.58 234.145 1053.27 218.536C944.415 85.0596 778.944 0 593.368 0C558.316 0 524.058 3.17485 490.726 8.99541V406.117C521.148 389.316 556.2 379.792 593.368 379.792C701.169 379.792 790.452 460.089 805.002 563.933Z"
                                            fill="#FBBF49"/>
                                    <path
                                            d="M490.726 1178.01C524.058 1183.83 558.316 1187 593.368 1187C704.873 1187 808.97 1156.18 898.121 1102.74L490.726 821.761V1178.01Z"
                                            fill="#FBBF49"/>
                                    <path
                                            d="M981.451 598.329C917.828 638.015 851.693 668.838 783.441 691.062C759.764 736.832 720.083 773.079 671.804 792.128L1003.54 1022.17C1116.5 914.095 1187 761.967 1187 593.435C1187 534.435 1178.27 477.553 1162.26 423.845C1125.36 487.871 1065.05 546.209 981.451 598.329Z"
                                            fill="#FBBF49"/>
                                </g>
                            </svg>
                            <div class="icons-wrapper">
                                <?php
                                while (have_rows('timeline')) : the_row();
                                    if (have_rows('first_timeline')): while (have_rows('first_timeline')) : the_row();
                                        $main_icon = get_sub_field('main_icon');
                                        $left_title = get_sub_field('title');
                                        $right_description = get_sub_field('description');
                                        $sub_text = get_sub_field('subtext');
                                        $day_icon = get_sub_field('day_icon');
                                        $day = get_sub_field('day');
                                        $day_subtext = get_sub_field('day_text');
                                        ?>
                                        <div class="steps-wrapper">
                                            <div class="step-title-circle first-step active-step">
                                                <?php if ($day_icon) { ?>
                                                    <div class="icon-wrapper">
                                                        <?= $day_icon ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <div class="step-wrapper">
                                                <div class="step-title paragraph paragraph-s-paragraph">
                                                    <?php if ($day) { ?>
                                                        <strong><?= $day ?></strong>
                                                    <?php } ?>
                                                    <?php if ($day_subtext) { ?>
                                                        <span>  <?= $day_subtext ?></span>
                                                    <?php } ?>
                                                </div>

                                                <div class="left-content">
                                                    <?php if ($left_title) { ?>

                                                        <p class="headline-2 left-text"><?= $left_title ?></p>
                                                    <?php } ?>
                                                </div>
                                                <div class="right-content">
                                                    <?php if ($right_description) { ?>
                                                        <div class="description paragraph-l-paragraph">
                                                            <?= $right_description ?>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($sub_text) { ?>
                                                        <div class="example-text">
                                                            <?= $sub_text ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                    endwhile; endif;
                                endwhile; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if ($second_timeline) { ?>
            <div class="tab-content tab-mobile-content-2">
                <div class="how-early-wrapper-mobile">
                    <div class="what-can-do-wrapper">
                        <div class="step-illustration-wrapper">
                            <svg class="raistone-logo main-logo" width="1187" height="1188"
                                 viewBox="0 0 1187 1188" fill="none">
                                <g class="logo-shapes" opacity="0.1">
                                    <path
                                            d="M0 593.434C0 832.077 141.001 1037.91 344.169 1132.1V54.8992C141.001 149.086 0 354.79 0 593.434Z"
                                            fill="#FBBF49"/>
                                    <path
                                            d="M805.002 563.933C837.144 549.646 868.095 532.846 897.459 513.532C1005.66 442.363 1058.3 361.536 1058.3 266.555C1058.3 250.152 1056.58 234.145 1053.27 218.536C944.415 85.0596 778.944 0 593.368 0C558.316 0 524.058 3.17485 490.726 8.99541V406.117C521.148 389.316 556.2 379.792 593.368 379.792C701.169 379.792 790.452 460.089 805.002 563.933Z"
                                            fill="#FBBF49"/>
                                    <path
                                            d="M490.726 1178.01C524.058 1183.83 558.316 1187 593.368 1187C704.873 1187 808.97 1156.18 898.121 1102.74L490.726 821.761V1178.01Z"
                                            fill="#FBBF49"/>
                                    <path
                                            d="M981.451 598.329C917.828 638.015 851.693 668.838 783.441 691.062C759.764 736.832 720.083 773.079 671.804 792.128L1003.54 1022.17C1116.5 914.095 1187 761.967 1187 593.435C1187 534.435 1178.27 477.553 1162.26 423.845C1125.36 487.871 1065.05 546.209 981.451 598.329Z"
                                            fill="#FBBF49"/>
                                </g>
                            </svg>
                            <div class="icons-wrapper">
                                <?php
                                while (have_rows('timeline')) : the_row();
                                    if (have_rows('second_timeline')): while (have_rows('second_timeline')) : the_row();
                                        $main_icon = get_sub_field('main_icon');
                                        $left_title = get_sub_field('title');
                                        $right_description = get_sub_field('description');
                                        $sub_text = get_sub_field('subtext');
                                        $day_icon = get_sub_field('day_icon');
                                        $day = get_sub_field('day');
                                        $day_subtext = get_sub_field('day_text');
                                        ?>
                                        <div class="steps-wrapper">
                                            <div class="step-title-circle first-step active-step">
                                                <?php if ($day_icon) { ?>
                                                    <div class="icon-wrapper">
                                                        <?= $day_icon ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <div class="step-wrapper">
                                                <div class="step-title paragraph paragraph-s-paragraph">
                                                    <?php if ($day) { ?>
                                                        <strong><?= $day ?></strong>
                                                    <?php } ?>
                                                    <?php if ($day_subtext) { ?>
                                                        <span>  <?= $day_subtext ?></span>
                                                    <?php } ?>
                                                </div>

                                                <div class="left-content">
                                                    <?php if ($left_title) { ?>

                                                        <p class="headline-2 left-text"><?= $left_title ?></p>
                                                    <?php } ?>
                                                </div>
                                                <div class="right-content">
                                                    <?php if ($right_description) { ?>
                                                        <div class="description paragraph-l-paragraph">
                                                            <?= $right_description ?>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($sub_text) { ?>
                                                        <div class="example-text">
                                                            <?= $sub_text ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                    endwhile; endif;
                                endwhile; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php endif; ?>
</div>
</section>


<!-- endregion raistone's Block -->

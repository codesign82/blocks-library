<?php
$post_id = @$args['post_id'];
$post_type = @$args['post_type'];
$thumbnail_id = get_post_thumbnail_id();
$alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);
$image = get_field('image', $post_id);
$quotation = get_field('quotation', $post_id);
$job_title = get_field('job_title', $post_id);
$title = get_field('name', $post_id);
?>

<div class="supplier-wrapper iv-st-from-bottom">
  <div class="left-supplier-content">
    <?php if ($image) { ?>
      <a href="<?= get_the_permalink($post_id) ?>" class="supplier-img aspect-ratio">
        <img src="<?=$image['url'] ?>" alt="<?= $image['alt'] ?>">
      </a>
    <?php } ?>
  </div>
  <div class="right-supplier-content">
    <?php if ($quotation) { ?>
      <div class="paragraph img-description">“<?= $quotation ?>”</div>
    <?php } ?>
    <div class="name-and-job">
      <?php if ($title) { ?>
        <a href="<?= get_the_permalink($post_id) ?>" class="text name"><?= $title ?></a>
      <?php } ?>
      <?php if ($job_title) { ?>
        <span class="text job"><?= $job_title ?></span>
      <?php } ?>
    </div>
  </div>
</div>

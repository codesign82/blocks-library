import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/raistone';

import 'swiper/swiper-bundle.css';
import Swiper, {Navigation, Pagination,EffectFade} from 'swiper'

Swiper.use([Pagination, Navigation, EffectFade]);
import 'swiper/css';

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.how_early_payments_work_v3_block');


  let currentActiveIcon = 0;
  // Change Active Between Btn

  const toggleBtn = block.querySelectorAll(".toggle-btn");
  for (let toggleBtnElement of toggleBtn) {
    toggleBtnElement?.addEventListener('click', () => {
      for (let toggleBtnElement2 of toggleBtn) {
        toggleBtnElement2.classList.remove('active-btn')
      }
      toggleBtnElement.classList.add('active-btn');
    })
  }
  const tab1 = block.querySelector('.tab-1');
  const tab2 = block.querySelector('.tab-2');
  const tabContent1 = block.querySelector('.tab-content-1');
  const tabContent2 = block.querySelector('.tab-content-2');
  const tabContentMobile1 = block.querySelector('.tab-mobile-content-1');
  const tabContentMobile2 = block.querySelector('.tab-mobile-content-2');

  function tabController(activeTab) {
    if (activeTab === 1) {
      tab2?.classList.remove('active-btn');
      tab1?.classList.add('active-btn');
      tabContent2?.classList.remove('tab-content-active');
      tabContent1?.classList.add('tab-content-active');
      tabContentMobile2?.classList.remove('tab-content-active');
      tabContentMobile1?.classList.add('tab-content-active');
    } else {
      tab1?.classList.remove('active-btn');
      tab2?.classList.add('active-btn');
      tabContent1?.classList.remove('tab-content-active');
      tabContent2?.classList.add('tab-content-active');
      tabContentMobile1?.classList.remove('tab-content-active');
      tabContentMobile2?.classList.add('tab-content-active');
    }
  }

  tab1?.addEventListener('click', () => tabController(1))
  tab2?.addEventListener('click', () => tabController(2))

  const firstTimeline = block.querySelector('.first-timeline-swiper');
  const secondTimeline = block.querySelector('.second-timeline-swiper');
  if (firstTimeline) {
    const icons = block.querySelectorAll(".how-early-wrapper-first .step-title-circle");
    let firstSwiper = new Swiper(firstTimeline, {
      pagination: {
        el: firstTimeline.querySelector('.swiper-pagination'),
        clickable: true,
      },
      navigation: {
        nextEl: firstTimeline.querySelector('.swiper-button-next'),
        prevEl: firstTimeline.querySelector('.swiper-button-prev'),
      },

      slidesPerView: '1',
      spaceBetween: 25,
      allowTouchMove: true,
      observer: true,
      observeParents: true,
      on: {
        slideChange: function () {
          icons[currentActiveIcon].classList.remove("active-step");
          currentActiveIcon = firstSwiper.realIndex;
          icons[currentActiveIcon].classList.add("active-step");

        },
      },

      effect: 'fade',
      fadeEffect: {
        crossFade: true
      },
    });
    icons.forEach((icon, index) => {
      icon.addEventListener("click", () => {
        firstSwiper.slideTo(index)
      })
    })
  }
  if (secondTimeline) {
    const icons2 = block.querySelectorAll(".how-early-wrapper-second .step-title-circle");
    let currentActiveIcon2 = 0;
    let secondSwiper = new Swiper(secondTimeline, {
      pagination: {
        el: secondTimeline.querySelector('.swiper-pagination'),
        clickable: true,
      },
      navigation: {
        nextEl: secondTimeline.querySelector('.swiper-button-next'),
        prevEl: secondTimeline.querySelector('.swiper-button-prev'),
      },
      slidesPerView: '1',
      spaceBetween: 25,
      allowTouchMove: true,
      observer: true,
      observeParents: true,
      on: {
        slideChange: function () {
          icons2[currentActiveIcon2].classList.remove("active-step");
          currentActiveIcon2 = secondSwiper.realIndex;
          icons2[currentActiveIcon2].classList.add("active-step");
        },
      },

      effect: 'fade',
      fadeEffect: {
        crossFade: true
      },
    });
    icons2.forEach((icon2, index) => {
      icon2.addEventListener("click", () => {
        secondSwiper.slideTo(index)
      })
    })
  }

  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


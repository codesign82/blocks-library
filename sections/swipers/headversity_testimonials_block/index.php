<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'testimonials_block';
$className = 'testimonials_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/testimonials_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$has_title = get_field('has_title');
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<div class="container">
    <?php if ($has_title && $title) { ?>
        <h2 move-to-here class="title headline-3 iv-st-from-bottom">
            <?= $title ?>
        </h2>
    <?php } ?>
    <div class="content-wrapper">
        <?php
        $testimonials = get_field('select_testimonial');
        if ($testimonials) { ?>
            <div class="swiper mySwiper">
                <div class="swiper-wrapper ">
                    <?php foreach ($testimonials as $testimonial) {
                        $alt = get_post_meta(get_post_thumbnail_id($testimonial), '_wp_attachment_image_alt', true);
                        $quote = get_field('quote', $testimonial);
                        $logo = get_field('logo', $testimonial);
                        $name = get_field('name', $testimonial);
                        $company = get_field('company', $testimonial);
                        $position = get_field('position', $testimonial);
                        $case_study_linking = get_field('case_study_linking', $testimonial);
                        ?>
                        <div class="swiper-slide iv-st-from-bottom">
                            <div class="testimonial-card <?= !has_post_thumbnail($testimonial) ? 'has-no-img' : '' ?> ">
                                <?php if (has_post_thumbnail($testimonial)) { ?>
                                    <div class="card-left">
                                        <picture class="img-wrapper aspect-ratio">
                                            <img src="<?php thumbnail_url($testimonial); ?>"
                                                 alt="<?= $alt ?>">
                                        </picture>
                                    </div>
                                <?php } ?>
                                <div class="card-right">
                                    <?php if ($quote) { ?>
                                        <div class="right-content paragraph ">
                                            <svg class='quotations' width='53' height='46' role="img"
                                                 viewBox='0 0 53 46' fill='none'
                                                 xmlns='http://www.w3.org/2000/svg'>
                                                <path
                                                        d='M0.199219 26.2L12.3992 0H22.5992L14.1992 24H21.3992V45.2H0.199219V26.2ZM29.7992 26.2L41.9992 0H52.1992L43.7992 24H50.9992V45.2H29.7992V26.2Z'
                                                        fill='#005DB3'/>
                                            </svg>
                                            <?= $quote ?>
                                        </div>
                                    <?php } ?>
                                    <div class="card-footer upper-line">
                                        <?php if ($logo) { ?>
                                            <picture class="card-footer-img-wrapper ">
                                                <img src="<?= $logo['url'] ?>"
                                                     alt="<?= $logo['alt'] ?>">
                                            </picture>
                                        <?php } ?>
                                        <div class="card-footer-content">
                                            <?php if ($name) { ?>
                                                <h3
                                                        class="card-footer-content-title headline-6"><?= $name ?>
                                                </h3>
                                            <?php }
                                            if ($company || $position) { ?>
                                                <div class="card-footer-content-content paragraph">
                                                    <?= $company ?>
                                                    <?php if ($position) {
                                                        echo "<br>";
                                                    } ?>
                                                    <?= $position ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if ($case_study_linking) { ?>
                                <a href="<?= get_the_permalink($case_study_linking) ?>"
                                   class="link cta-link"><?= __('View Case Study', 'headversity') ?>
                                    <svg class='arrow' viewBox='0 0 29.5 22.1' role="application">
                                        <path class='arrow-head'
                                              d='M29.1,12.1a1.7,1.7,0,0,0,0-2.1L19.5.4a1.7,1.7,0,0,0-2.1,0,1.6,1.6,0,0,0,0,2.2L25.9,11l-8.5,8.5a1.6,1.6,0,0,0,0,2.2,1.7,1.7,0,0,0,2.1,0Z'
                                              fill='#f90'/>
                                        <path class='arrow-line' d='M0,12.5H28v-3H0Z' fill='#f90'/>
                                    </svg>
                                </a>
                            <?php } ?>
                        </div>
                    <?php } ?>

                </div>
            </div>
            <div class="swiper-arrows-container">
                <button class="swiper-button swiper-button-prev  arrow-left"
                        aria-label="<?= __('slider', 'headversity') ?>">
                    <svg width="24" height="37" viewBox="0 0 24 37" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M21.1171 34.4222L3.90214 18.3194L20.8714 2.41621"
                              stroke="#FF9900" stroke-width="4" stroke-linecap="round"/>
                    </svg>
                </button>
                <div class='swiper-pagination' role="button" aria-label="slider"></div>
                <button class="swiper-button swiper-button-next  arrow-right"
                        aria-label="<?= __('slider', 'headversity') ?>">
                    <svg width="23" height="37" viewBox="0 0 23 37" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M2.7774 34.4222L19.9924 18.3194L3.02308 2.41621"
                              stroke="#FF9900" stroke-width="4" stroke-linecap="round"/>
                    </svg>
                </button>
            </div>
        <?php } ?>
    </div>
</div>
</section>


<!-- endregion headversity's Block -->

import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/propellerhealth';

import Swiper, {Navigation, Pagination} from "swiper";
import {debounce} from "lodash/function";

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {

  const block = container.querySelector('.testimonials_block');

  // add block code here
  const swiperControllers = block.querySelector('.swiper-arrows-container');
  const slides = block.querySelectorAll('.swiper-slide');
  const breakpoint = window.matchMedia('(min-width:600px)');
  const swiperChecker = breakpoint.matches ? {
    autoHeight: true,
  } : {
    autoHeight: true,
  };

  const swiper = new Swiper(block.querySelector(".mySwiper"), {
    observer: true,
    // autoHeight:true,
    // height:true,
    observerParents: true,
    spaceBetween: '5%',
    modules: [Navigation, Pagination],
    navigation: {
      nextEl: block.querySelector(".swiper-button-next"),
      prevEl: block.querySelector(".swiper-button-prev"),
    },
    pagination: {
      el: block.querySelector('.swiper-pagination'),
      type: 'bullets',
      clickable: true,

    },
    ...swiperChecker,
    breakpoints: {
      300:
          {
            slidesPerView: 1.02,
            spaceBetween: 11,
            slidesOffsetBefore: -20,

          },
      600:
          {
            centeredSlides: true,
            slidesPerView: 1,
            slidesOffsetBefore: 0,
          }
    },
  });
  //   on: {
  //     init: ({activeIndex}) => {
  //       swiperControllers.style.top = (slides[activeIndex].children[0].clientHeight / 2 + 30) + 'px';
  //     },
  //     slideChange: ({activeIndex}) => {
  //       swiperControllers.style.top = (slides[activeIndex].children[0].clientHeight / 2 + 30) + 'px';
  //     }
  //   }
  // });
  //
  // const swiperControllerTopCalculator = debounce(() => {
  //   swiperControllers.style.top = (block.querySelector('.swiper-slide.swiper-slide-active')?.children[0].clientHeight / 2 + 30) + 'px';
  // }, 300)
  //
  // window.addEventListener('resize', swiperControllerTopCalculator);

  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


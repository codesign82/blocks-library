import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/moving';
import {ScrollTrigger} from "gsap/ScrollTrigger";


gsap.registerPlugin(ScrollTrigger);

const blockScript = async (container = document) => {
  const block = container.querySelector('.text_and_phone_screens_block');

  const initFadeSlider = () => {

    let phoneCover = block.querySelector('.phone-screens');
    let slides = phoneCover.querySelectorAll('.phone-screen');
    let textBlocks = block.querySelectorAll('.text-screen-case');
    gsap.set(slides, {xPercent: -50, yPercent: -49.5})

    let lastContent;

    function getCurrentSection() {
      let newContent;
      // Find the current section
      textBlocks.forEach(textBlock => {
        if (textBlock.getBoundingClientRect().top < window.innerHeight * .5) {
          newContent = textBlock.content;
        }
      });

      // If the current section is different than that last, animate in
      if (newContent
          && (lastContent == null
              || !newContent.isSameNode(lastContent))) {
        // Fade out last section
        if (lastContent) {
          lastContent.leave();
        }

        // Animate in new section
        newContent.enter();

        lastContent = newContent;
      }

    }
    ScrollTrigger.saveStyles(phoneCover)
    ScrollTrigger.matchMedia({
      '(min-width: 600px)': () => {
// Set up our content behaviors
        lastContent=null;

        for (let i = 0; i < textBlocks.length; i++) {
          textBlocks[i].content = slides[i];
          textBlocks[i].content.enter = function () {
            gsap.set(textBlocks[i].content, {
              opacity: 1,
            });
          }
          textBlocks[i].content.leave = function () {
            gsap.set(textBlocks[i].content, {opacity: 0});
          }
        }

        ScrollTrigger.create({
          trigger: block,
          pin: phoneCover,
          pinSpacing: false,
          start: 'top top',
          end: 'bottom bottom',
          onUpdate: getCurrentSection,
        })
        ScrollTrigger.create({
          trigger: block,
          start: 'top 20%',
          end: 'bottom 80%',
          scrub: true,
          invalidateOnRefresh:true,
          onToggle({isActive}) {
            gsap.set(slides, {willChange: isActive ? 'opacity' : 'auto'})
            gsap.set(phoneCover, {willChange: isActive ? 'width,max-width,height,max-height,position,top,left' : 'auto'})
          }
        })
        // Handle the updated position



        textBlocks[0].content.enter()

      },
      '(max-width: 599.98px)': () => {
        lastContent=null;

        for (let i = 0; i < textBlocks.length; i++) {
          textBlocks[i].content = slides[i];
          textBlocks[i].content.enter = function () {
            gsap.set(textBlocks[i].content, {
              opacity: 1,
            });
          }
          textBlocks[i].content.leave = function () {
            gsap.set(textBlocks[i].content, {opacity: 0});
          }
        }

        ScrollTrigger.create({
          trigger: phoneCover,
          pin: true,
          pinSpacing: false,
          start: 'bottom 100%',
          endTrigger: block.querySelector('.text-content-wrapper'),
          end: 'bottom 100%',
          onUpdate: getCurrentSection,
        })

        textBlocks[0].content.enter()

      },

    })
  }

  initFadeSlider();

  animations(block);
  imageLazyLoading(block);
  
};
windowOnLoad(blockScript);



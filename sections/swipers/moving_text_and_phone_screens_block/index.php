<?php
// Create id attribute allowing for custom "anchor" value.
$id = 'text_and_phone_screens_block';
// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'text_and_phone_screens_block';
$className = 'text_and_phone_screens_block';
if ( ! empty( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
    $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/text_and_phone_screens_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/

$text_with_gradient = get_field( 'text_with_gradient' );
$first_text         = @$text_with_gradient['first_text'];
$gradient_text      = @$text_with_gradient['gradient_text'];
$last_text          = @$text_with_gradient['last_text'];
$main_title         = get_field( 'main_title' );
?>
<!-- region MOVING's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
    <div class="text_and_phone_screens_block_wrapper">
        <div class="text-content">
            <h1 class="text-content-title headline-1 text-uppercase ">
                <?php if ( $first_text ) { ?>
                    <?= $first_text ?>
                <?php } ?>
                <?php if ( $gradient_text ) { ?>
                    <span class="red-gradiant"><?= $gradient_text ?></span>
                <?php } ?>
                <?php if ( $last_text ) { ?>
                    <?= $last_text ?>
                <?php } ?>
            </h1>
            <div class="text-content-wrapper section-border section-border-light">
                <?php if ( $main_title ) { ?>
                    <p class="text-content-description title-circle-wrapper paragraph ">
                        <svg class="title-circle" width="34" height="34" viewBox="0 0 34 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="17" cy="17" r="15" stroke="#DEDFE3" stroke-width="3"/>
                        </svg>
                        <?= $main_title ?></p>
                <?php } ?>

                <?php
                if ( have_rows( 'text_and_phone_screen' ) ) {
                    ?>
                    <?php
                    while ( have_rows( 'text_and_phone_screen' ) ) {
                        the_row();
                        $title              = get_sub_field( 'title' );
                        $description        = get_sub_field( 'description' );
                        $phone_screen_shoot = get_sub_field( 'phone_screen_shoot' );
                        ?>
                        <div class="text-screen-case">
                            <?php if ( $title ) { ?>
                                <h6 class="text-screen-case-title headline-4 title-circle-wrapper ">
                                    <svg class="subtitle-circle" width="64" height="61" viewBox="0 0 64 61" fill="none">
                                        <path d="M2 0V19C2 35.5685 15.4417 49 32.0102 49C37.8539 49 42.161 49 41.9938 49" stroke="currentColor" stroke-width="3"/>
                                        <circle cx="52" cy="49" r="10" stroke="currentColor" stroke-width="3"/>
                                        <circle cx="52" cy="49" r="4" fill="currentColor"/>
                                    </svg>
                                    <?= $title ?></h6>
                            <?php } ?>
                            <?php if ( $description ) { ?>
                                <div class="text-screen-case-description paragraph "><?= $description ?></div>
                            <?php } ?>
                        </div>
                        <?php
                    } ?>
                    <?php
                }
                ?>
            </div>
            <div class="full-lines">
                <div class="line line-width"></div>
                <div class="line line-height"></div>
            </div>
        </div>
        <div class="phone-screens">
            <!--      <img class="phone-case" src="--><?//= get_template_directory_uri() . '/front-end/src/images/phone-case.png' ?><!--" alt="phone-frame">-->
            <?php
            if ( have_rows( 'text_and_phone_screen' ) ) {
                ?>
                <?php
                while ( have_rows( 'text_and_phone_screen' ) ) {
                    the_row();
                    $phone_screen_shoot = get_sub_field( 'phone_screen_shoot' );
                    ?>
                    <?php if ( $phone_screen_shoot ) { ?>
                        <picture class="phone-screen phone-screen-1">
                            <img src="<?= $phone_screen_shoot['url'] ?>" alt="<?= $phone_screen_shoot['alt'] ?>">
                        </picture>
                    <?php } ?>
                    <?php
                } ?>
                <?php
            }
            ?>
            <img class="phone-screens-blur" src="<?=get_template_directory_uri() ?>/front-end/src/images/blur-phone-block.jpg" />
            <!--      <svg class="phone-screens-blur" width="992" height="893" viewBox="0 0 992 893" fill="none">-->
            <!--        <g opacity="0.6">-->
            <!--          <g opacity="0.15" filter="url(#filter0_f5000)">-->
            <!--            <ellipse cx="388.5" cy="362.5" rx="252.5" ry="226" fill="#EA4AAA"/>-->
            <!--          </g>-->
            <!--          <g opacity="0.15" filter="url(#filter1_f6000)">-->
            <!--            <ellipse cx="604" cy="480.5" rx="308" ry="276" fill="#ED4E50"/>-->
            <!--          </g>-->
            <!--          <g opacity="0.15" filter="url(#filter2_f7000)">-->
            <!--            <ellipse cx="361.5" cy="568" rx="200.5" ry="179.5" fill="#EC4D63"/>-->
            <!--          </g>-->
            <!--        </g>-->
            <!--        <defs>-->
            <!--          <filter id="filter0_f5000" x="0.085907" y="0.585907" width="776.828" height="723.828" filterUnits="userSpaceOnUse"-->
            <!--                  color-interpolation-filters="sRGB">-->
            <!--            <feFlood flood-opacity="0" result="BackgroundImageFix"/>-->
            <!--            <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>-->
            <!--            <feGaussianBlur stdDeviation="67.957" result="effect1_foregroundBlur"/>-->
            <!--          </filter>-->
            <!--          <filter id="filter1_f6000" x="160.086" y="68.5859" width="887.828" height="823.828" filterUnits="userSpaceOnUse"-->
            <!--                  color-interpolation-filters="sRGB">-->
            <!--            <feFlood flood-opacity="0" result="BackgroundImageFix"/>-->
            <!--            <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>-->
            <!--            <feGaussianBlur stdDeviation="67.957" result="effect1_foregroundBlur"/>-->
            <!--          </filter>-->
            <!--          <filter id="filter2_f7000" x="25.0859" y="252.586" width="672.828" height="630.828" filterUnits="userSpaceOnUse"-->
            <!--                  color-interpolation-filters="sRGB">-->
            <!--            <feFlood flood-opacity="0" result="BackgroundImageFix"/>-->
            <!--            <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>-->
            <!--            <feGaussianBlur stdDeviation="67.957" result="effect1_foregroundBlur"/>-->
            <!--          </filter>-->
            <!--        </defs>-->
            <!--      </svg>-->
        </div>
    </div>
</div>
</section>


<!-- endregion MOVING's Block -->

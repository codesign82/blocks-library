import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import Swiper from "swiper";
import '../../../scripts/sitesSizer/zineone';

/**
 *
 * @param container {Document}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
const block = container.querySelector('.success_stories_block');
  
  new Swiper(block.querySelector('.swiper-container'), {
    slidesPerView: 1.1,
    spaceBetween: 24,
    
    breakpoints: {
      // when window width is >= 991px
      600: {
        slidesPerView: 1.2,
      },
      991: {
        spaceBetween: 40,
        slidesPerView: 1.52,
      },
    }
  });
  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);

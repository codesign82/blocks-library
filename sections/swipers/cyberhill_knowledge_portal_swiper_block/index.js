import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import Swiper, {Navigation} from "swiper";
// import '../../../scripts/sitesSizer/cyberhill';

Swiper.use([Navigation]);
const blockScript = async (container = document) => {
  const block = container.querySelector('.knowledge_portal_swiper_block');
  
  // add block code here
  let slides = block.querySelectorAll('.swiper-slide');
  new Swiper(block.querySelector('.mySwiper'), {
    slidesPerView: 1.14,
    loop: true,
    spaceBetween: 16,
    navigation: {
      nextEl: block.querySelector('.swiper-button-next'),
      prevEl: block.querySelector('.swiper-button-prev')
    },
    breakpoints: {
      600: {
        slidesPerView: 2,
        spaceBetween: 24,
        
      },
      992: {
        slidesPerView: 2.85,
        spaceBetween: 24,
      }
    }
    
  })
  
  
  
  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



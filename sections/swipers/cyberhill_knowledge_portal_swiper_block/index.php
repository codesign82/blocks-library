<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'knowledge_portal_swiper_block';
$className = 'knowledge_portal_swiper_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/knowledge_portal_swiper_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$convertor = get_field('choose_from_theme_option');
$title = get_field('title', 'options');
$sub_title = get_field('sub_title', 'options');
$cta_button = get_field('cta_button', 'options');
$section_title = get_field('section_title');
$section_subtitle = get_field('section_subtitle');
$view_all = get_field('view_all');


?>
<!-- region CyberHill's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="knowledge-portal-wrapper">
    <?php if ($title || $section_title) { ?>
      <div class="title-with-shape title-with-shape-blue iv-st-from-left">
        <?= $convertor ? $title : $section_title ?>
      </div>
    <?php } ?>

    <?php if ($sub_title && $convertor) : ?>
      <h4 class="headline-4 iv-st-from-bottom">
        <?= $sub_title ?>
      </h4>
    <?php elseif ($section_subtitle) : ?>
      <h4 class="headline-4 iv-st-from-bottom">
        <?= $section_subtitle ?>
      </h4>
    <?php endif; ?>
    <?php
    $knowledge_portal = '';
    !$convertor ? $knowledge_portal = get_field('knowledge_portal') : $knowledge_portal = get_field('knowledge_portal', 'options');
    if ($knowledge_portal): ?>
      <div class="swiper mySwiper has-layer iv-st-from-bottom">
        <div class="swiper-wrapper">
          <?php foreach ($knowledge_portal as $knowledge_slider_item):
            get_template_part('template-parts/knowledge-portal-card', '', array('id' => $knowledge_slider_item));
          endforeach; ?>
          <!--        <div class="swiper-pagination"></div>-->
        </div>
      </div>
    <?php endif; ?>
    <div class="all-and-swiper-button iv-st-from-right">
      <?php if ($view_all && !$convertor) { ?>
        <a href="<?= $view_all['url'] ?>" target="<?= $view_all['target'] ?>" class="primary-button"><?= $view_all['title'] ?></a>
      <?php } else { ?>
        <?php if ($cta_button) { ?>
          <a href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>" class="primary-button"><?= $cta_button['title'] ?></a>
        <?php } ?>
      <?php } ?>
      <div class="swiper-buttons">
        <div class="swiper-button swiper-button-prev ">
          <svg viewBox="0 0 24 14" fill="none"
               xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd"
                  d="M8.03467 13.2058L2.26815 7.60735L24 7.60735V6.48433L2.17373 6.48433L8.03467 0.794195L7.21706 0L0.824749 6.20602L0.817617 6.1991L1.90735e-06 6.99329L0.00692368 7.00001L1.90735e-06 7.00673L0.817617 7.80093L0.824749 7.794L7.21706 14L8.03467 13.2058Z"
                  fill="#1D4070"/>
          </svg>

        </div>
        <div class="swiper-button swiper-button-next ">
          <svg viewBox="0 0 24 14" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M15.9653 0.794176L21.7319 6.39265L-7.62939e-06 6.39265L-7.62939e-06 7.51567L21.8263 7.51567L15.9653 13.2058L16.7829 14L23.1753 7.79398L23.1824 7.8009L24 7.00671L23.9931 6.99999L24 6.99327L23.1824 6.19907L23.1753 6.206L16.7829 -1.90735e-05L15.9653 0.794176Z" fill="#1D4070"/>
          </svg>

        </div>
      </div>
    </div>
  </div>
</div>
</section>


<!-- endregion CyberHill's Block -->

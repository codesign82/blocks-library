<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'finance_and_technology_block';
$className = 'finance_and_technology_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/finance_and_technology_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$first_avatar = get_field('first_avatar');
$second_avatar = get_field('second_avatar');
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<svg class="line line-1" width="642" height="700" viewBox="0 0 642 700" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g opacity="0.5">
        <path d="M437.053 691.768L437.053 363.389C437.053 242.692 339.208 144.848 218.511 144.848V144.848C97.8141 144.848 -0.0302994 242.692 -0.0303047 363.389L-0.0303194 699.671"
              stroke="#C1DA9B"/>
        <path d="M640.663 697.177L640.663 258.125C640.663 115.938 525.397 0.67187 383.209 0.671864V0.671864C241.022 0.671858 125.756 115.938 125.756 258.126L125.756 697.177"
              stroke="#C1DA9B"/>
    </g>
</svg>
<svg class="line line-2" width="886" height="984" viewBox="0 0 886 984" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g opacity="0.5">
        <path d="M604.072 972.561L604.072 505.287C604.072 338.718 469.042 203.687 302.473 203.687V203.687C135.904 203.687 0.873699 338.718 0.873692 505.287L0.873671 983.672"
              stroke="#C1DA9B"/>
        <path d="M885.065 980.164L885.065 356.3C885.065 160.073 725.992 0.999993 529.765 0.999984V0.999984C333.539 0.999976 174.465 160.073 174.465 356.3L174.465 980.164"
              stroke="#C1DA9B"/>
    </g>
</svg>

<svg class="dash-line dash-line-1" viewBox="0 0 463.89 136.96">
    <defs>
        <clipPath id="a-fiance-block" transform="translate(-3.61 -31.11)">
            <path d="M0,0V222.16H467.46V0ZM308,88.69A32.33,32.33,0,1,1,340.2,56.36,32.25,32.25,0,0,1,308,88.69Z"
                  fill="none"/>
        </clipPath>
        <mask id="theMask5" maskUnits="userSpaceOnUse">
            <path class="dash-line dots-animation-i-e dash-line-2" d="M466,107.74C422,81,136.39-22.07,5.11,166.57"
                  transform="translate(-3.61 -31.11)" fill="none"
                  stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
        </mask>

    </defs>
    <g id="maskReveal5" mask="url(#theMask5)" clip-path="url(#a-fiance-block)">
        <path class="dash-line dash-line-2" d="M466,107.74C422,81,136.39-22.07,5.11,166.57"
              transform="translate(-3.61 -31.11)" fill="none"
              stroke="#521437" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"
              stroke-dasharray="15 14"/>
    </g>
    <circle class="circle-animation-i-e" cx="302.22" cy="25.25" r="23.75" fill="none" stroke="#521437"
            stroke-linecap="round" stroke-linejoin="round"
            stroke-width="3"/>
</svg>
<svg class="dash-line dash-line-2" viewBox="0 0 461.64 132.08">
    <defs>
        <mask id="theMask6" maskUnits="userSpaceOnUse">
            <path class="dots-animation-i-e" d="M1.5,22.22s79.39,113.1,240.81,107.13S457.5,31.46,460.14.5"
                  transform="translate(0 1)" fill="none"
                  stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>

        </mask>

    </defs>
    <g id="maskReveal6" mask="url(#theMask6)">
        <path d="M1.5,22.22s79.39,113.1,240.81,107.13S457.5,31.46,460.14.5" transform="translate(0 1)" fill="none"
              stroke="#521437" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"
              stroke-dasharray="26 17"/>
    </g>
</svg>

<?php if ($first_avatar) { ?>
    <picture class="avatar avatar-1 image-wrapper">
        <img src="<?= $first_avatar['url'] ?>" alt="<?= $first_avatar['alt'] ?>">
    </picture>
<?php } ?>
<?php if ($second_avatar) { ?>
    <picture class="avatar avatar-2 image-wrapper">
        <img src="<?= $second_avatar['url'] ?>" alt="<?= $second_avatar['alt'] ?>">
    </picture>
<?php } ?>

<div class="title-and-description">
    <?php if ($title) { ?>
        <h2 class="headline-2 title iv-st-from-bottom"><?= $title ?></h2>
    <?php } ?>
    <?php if ($description) { ?>
        <div class="paragraph paragraph-xl-paragraph description iv-st-from-bottom"><?= $description ?></div>
    <?php } ?>
</div>

<?php
$index = 0;
if (have_rows('cards')) { ?>
    <div class="swiper">
        <div class="swiper-wrapper boxes">
            <?php while (have_rows('cards')) {
                the_row();
                $background_color = get_sub_field('background_color');
                $is_image = get_sub_field('is_image');
                $svg = get_sub_field('svg');
                $image = get_sub_field('image');
                $card_image = get_sub_field('card_image');
                $title = get_sub_field('title');
                $title_color = get_sub_field('title_color');
                $description = get_sub_field('description');
                $content_only_logo = get_sub_field('content_only_logo');
                ?>
                <div class="swiper-slide <?= $content_only_logo ? 'content-only-logo' : '' ?> box"
                     style="background-color: <?= $background_color ?>">
                    <?php if ($content_only_logo) { ?>
                        <?php if ($card_image) { ?>
                            <div class="only-logo">
                                <picture class="card-image">
                                    <img src="<?= $card_image['url'] ?>" alt="<?= $card_image['alt'] ?>">
                                </picture>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <?php if (!$is_image) { ?>
                            <div class="icon">
                                <?= $svg ?>
                            </div>
                        <?php } else { ?>
                            <picture class="icon">
                                <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                            </picture>
                        <?php } ?>
                        <?php if ($title) { ?>
                            <div class="headline-2 price" style="color: <?= $title_color ?>"><?= $title ?></div>
                        <?php } ?>
                        <?php if ($description) { ?>
                            <div class="paragraph paragraph-xl-paragraph box-description">
                                <?= $description ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
                <?php $index++;
            } ?>
        </div>
    </div>
<?php } ?>


</section>


<!-- endregion raistone's Block -->

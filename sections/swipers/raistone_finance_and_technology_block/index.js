import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/raistone';

import Swiper, {Autoplay} from "swiper";

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.finance_and_technology_block');


  const slider = block.querySelector(".swiper")

  const swiper = new Swiper(slider, {
    modules: [Autoplay],
    slidesPerView: 1.2,
    loop: true,
    spaceBetween: 20,
    allowTouchMove: false,
    speed: 6000,
    breakpoints: {
      '600': {
        slidesPerView: 1.3,
        spaceBetween: 40,
        speed: 7000,
      },
      '992': {
        slidesPerView: 2.75,
        spaceBetween: 56,
        speed: 10000,
      }
    },
    autoplay: {
      delay: 0,
      disableOnInteraction: false,
      pauseOnMouseEnter: false,
      stopOnLastSlide: false,
      waitForTransition: true,
    },
    on: {
      init(self) {
        self.autoplay.stop()
        setTimeout(() => {
          self.autoplay.start()
        }, 1000)
      }
    }
  });
  swiper.autoplay.stop()


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


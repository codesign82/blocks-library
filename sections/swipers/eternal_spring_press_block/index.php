<?php
// Create id attribute allowing for custom "anchor" value.
$id = 'screenings';


// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'press_block';
$className = 'press_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/press_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title       = get_field( 'title' );
$description = get_field( 'description' );

?>
<!-- region Eternal_spring's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <div class="press-wrapper">
    <h2 class="headline-2 dark press-head iv-st-from-bottom"><?=$title ?></h2>
    <div class="press-group">
      <?php if ( have_rows( 'press_cards' ) ) { ?>
        <div class="swiper-container">
          <div class="swiper-button swiper-button-prev">
            <svg xmlns="http://www.w3.org/2000/svg" width="28" height="56"
                 viewBox="0 0 28 56">
              <g transform="rotate(-180 14 28)">
                <path fill="#423c2e" d="M.425.538L28 28.115.425 55.69z"/>
              </g>
            </svg>
          </div>
          <div class="swiper-wrapper">
            <?php $index = 0;
            while ( have_rows( 'press_cards' ) ) {
              the_row();
              $text  = get_sub_field( 'text' );
              $sub_text = get_sub_field( 'sub_text' );
              ?>
              <div class="swiper-slide ">
                <div class="press-img-and-text iv-st-from-bottom">
                  <img src="<?= get_template_directory_uri() . '/front-end/src/images/press-img-' . $index % 2 . '.svg' ?>" alt="">

                  <?php if ( $text ) { ?>
                    <h6 class="paragraph paragraph-18 text-paragraph">
                      <?= $text ?>
                    </h6>
                  <?php } ?>
                  <?php if ( $sub_text ) { ?>
                    <h5 class="paragraph paragraph-14 text-paragraph1">
                      <?= $sub_text ?>
                    </h5>
                  <?php } ?>
                </div>
              </div>
              <?php $index ++;
            } ?>
          </div>
        </div>
        <div class="swiper-button swiper-button-next">
          <svg xmlns="http://www.w3.org/2000/svg" width="28" height="56"
               viewBox="0 0 28 56">
            <path fill="#211e17" d="M0 .538l27.576 27.577L0 55.69z"/>
          </svg>
        </div>
        <div class="swiper-pagination"></div>
      <?php } ?>
    </div>
  </div>
</div>
</section>

<!-- endregion Eternal_spring's Block -->

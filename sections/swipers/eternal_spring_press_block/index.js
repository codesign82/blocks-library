import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import Swiper, {Pagination, Navigation,Grid } from "swiper";
import 'swiper/scss/grid';

import {gsap} from "gsap";
import '../../../scripts/sitesSizer/swc';




const blockScript = async (container = document) => {
const block = container.querySelector('.press_block');
  
  let swiper = new Swiper(block.querySelector(".swiper-container"), {
    modules:[Pagination, Navigation,Grid],
    navigation: {
      nextEl: block.querySelector('.swiper-button-next'),
      prevEl: block.querySelector('.swiper-button-prev')
    },
    slidesPerView: 1.1,
    spaceBetween: 10,
    slidesPerGroup: 1,
    grid: {
      rows: 3,
      fill: 'row',
    },
    allowTouchMove: true,
    pagination: {
      el: block.querySelector('.swiper-pagination'),
      clickable: true,
    },
    breakpoints: {
      600: {
        slidesPerView: 3,
        spaceBetween: 10,
        slidesPerGroup: 2,
        grid: {
          rows: 2,
          fill: 'row',
        },
      },
    },
  });
  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);

<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'block-slider-team';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/block-slider-team/screenshot1.png" >';
  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
?>
<!-- region Kiwi's Block -->
<?php general_settings_for_blocks($id, $className); ?>
<div class="container block-slider-team-wrapper">
  <?php if ($title) { ?>
    <div class="title wysiwyg-block real-line-up">
      <?=$title?>
    </div>
  <?php } ?>
  <div class="team-swiper">
    <div class="swiper-by-name iv-st-from-left">
      <div class="swiper-btns">
        <div class="swiper-button-next swiper-button">
          <div class="swiper-btn-bg"></div>
          <div class="swiper-btn-border"></div>
          <svg height="7" viewBox="0 0 13 7" width="13">
            <path d="M6.5 7L0 0h13z"/>
          </svg>
        </div>
        <div class="swiper-button-prev swiper-button">
          <div class="swiper-btn-bg"></div>
          <div class="swiper-btn-border"></div>
          <svg height="7" viewBox="0 0 13 7" width="13">
            <path d="M6.5 0L13 7H0z"/>
          </svg>
        </div>
      </div>
      <div class="swiper-container">
        <div class="swiper-wrapper">
          <?php
          while (have_rows('person_details')) {
            the_row();
            $person_name = get_sub_field('person_name');
            $job_title = get_sub_field('job_title');
            ?>
            <div class="swiper-slide">
              <?php if ($person_name) { ?>
                <h3 class="headline-3 name"><?=$person_name?></h3>
              <?php } ?>
              <?php if ($job_title) { ?>
                <span class="job-title"><?=$job_title?></span>
              <?php } ?>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
    <div class="swiper-by-image iv-st-from-right">
      <div class="swiper-container">
        <div class="swiper-wrapper">
          <?php
          while (have_rows('person_details')) {
            the_row();
            $person_image = get_sub_field('person_image');
            $person_description = get_sub_field('person_description');
            ?>
            <div class="swiper-slide">
              <?php if ($person_image) { ?>
                <picture class="aspect-ratio">
                  <img alt="<?=$person_image['alt']?>" src="<?=$person_image['url']?>">
                </picture>
              <?php } ?>
              <?php if ($person_description) { ?>
                <p class="paragraph size19"><?=$person_description?></p>
              <?php } ?>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
</section>

<!-- endregion Kiwi's Block -->

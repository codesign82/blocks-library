import './index.html';
import './style.scss';
// import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/wrap';
import Swiper, {Controller, EffectFade, Navigation} from 'swiper';
// import 'swiper/components/effect-fade/effect-fade.scss';



const blockScript = async (container = document) => {
  Swiper.use([Navigation, EffectFade, Controller]);
  const blocks = container.querySelectorAll('.block-slider-team');
  for (let block of blocks) {
    const imagesLoading = [];
    const slideImages = Array.from(block.querySelectorAll('.swiper-by-image .swiper-slide img'));
    for (const slideImage of slideImages) {
      imagesLoading.push(new Promise(resolve => slideImage.complete ? resolve() : (slideImage.onload = resolve)));
    }
    const imageSwiper = new Swiper(block.querySelector('.swiper-by-image .swiper-container'), {
      slidesPerView: 1,
      effect: 'fade',
      grabCursor: true,
      loop: true,
      loopedSlides: 9,
      updateOnImagesReady:true,
      fadeEffect: {
        crossFade: true,
      },
    });
    const nameSwiper = new Swiper(block.querySelector('.swiper-by-name .swiper-container'), {
      direction: 'vertical',
      slidesPerView: 4,
      slideToClickedSlide: true,
      grabCursor: true,
      loop: true,
      loopedSlides: 9,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
    imageSwiper.controller.control = nameSwiper;
    nameSwiper.controller.control = imageSwiper;
    Promise.allSettled(imagesLoading).then(() => {
      console.log('sliderTeam');
      setTimeout(function (){
        nameSwiper.update();
        imageSwiper.update();
        nameSwiper.slideTo(0,0);
        // imageSwiper.slideTo(0,0);
      },500)
    });
  }
  
  
}
windowOnLoad(blockScript);




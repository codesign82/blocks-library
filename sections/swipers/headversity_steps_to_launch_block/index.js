import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/propellerhealth';

import Swiper, {Navigation, Pagination} from 'swiper';

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {

  const block = container.querySelector('.steps_to_launch_block');

  let currentActiveIcon = 0;
  const iconsWrapper = block.querySelector(".step-numbers")
  const iconsWrapperRect = iconsWrapper.getBoundingClientRect()
  const icons = block.querySelectorAll(".step-number")
  const pagination = block.querySelector(".myswiper-wrapper")
  const swiperSlide = block.querySelectorAll(".swiper-slide")
  let swiper = new Swiper(block.querySelector('.mySwiper'), {
    modules: [Navigation, Pagination],
    pagination: {
      el: block.querySelector('.swiper-pagination'),
      clickable: true,
    },
    navigation: {
      nextEl: block.querySelector(".swiper-button-next"),
      prevEl: block.querySelector(".swiper-button-prev"),
    },

    slidesPerView: 1,
    spaceBetween: 30,
    allowTouchMove: true,
    observer: true,
    observeParents: true,
    on: {
      slideChange: slideChange,
      init: slideChange,
    },

    effect: 'fade',
    fadeEffect: {
      crossFade: true

    },
  });

  function slideChange(swiper) {
    icons[currentActiveIcon].classList.remove("active-step");
    currentActiveIcon = swiper.realIndex;
    icons[currentActiveIcon].classList.add("active-step");
  }

  icons.forEach((icon, index) => {
    icon.addEventListener("click", () => {
      swiper.slideTo(index)
    })
  })


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


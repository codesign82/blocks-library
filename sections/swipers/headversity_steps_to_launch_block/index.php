<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'steps_to_launch_block';
$className = 'steps_to_launch_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/steps_to_launch_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$bold_text = get_field('bold_text');
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="steps-wrapper iv-st-from-bottom">
        <?php if ($title) { ?>
            <h2 move-to-here class="headline-3 main-title "><?= $title ?></h2>
        <?php } ?>
        <div class="description-wrapper ">
            <div class="left-description ">
                <?php if ($description) { ?>
                    <div class="paragraph left-desc">
                        <?= $description ?>
                    </div>
                <?php } ?>
            </div>
            <div class="right-description">
                <?php if ($bold_text) { ?>
                    <div class="paragraph right-desc ">
                        <?= $bold_text ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="swiper mySwiper iv-st-from-bottom">
        <?php if (have_rows('steps_repeater')) { ?>
        <div class="swiper-wrapper">
            <?php while (have_rows('steps_repeater')) {
                the_row();
                $image = get_sub_field('image');
                ?>
                <div class="swiper-slide">
                    <?php if ($image) { ?>
                        <picture class="step-img aspect-ratio">
                            <?= get_acf_image($image, 'img-1104-536') ?>
                        </picture>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php } ?>
        </div>
    </div>
    <?php if (have_rows('steps_repeater')) { ?>

        <div class="step-numbers ">
            <?php $index = 1;
            while (have_rows('steps_repeater')) {
                the_row();
                $title = get_sub_field('title');
                $active_step = get_row_index() === 1 ? 'active-step' : ''
                ?>
                <button class="step-number <?= $active_step ?>" aria-label="<?= __('step' . $index, 'headversity') ?>">
                    <div class="number">
                        <?= $index ?>
                    </div>
                    <h2 class="step-number-text">
                        <?= __('step', 'headversity') ?> <?= $index ?>
                    </h2>
                    <?php if ($title) { ?>
                        <div class="step-description">
                            <?= $title ?>
                        </div>
                    <?php } ?>
                </button>
                <?php $index++;
            } ?>
        </div>

    <?php } ?>
    <div class='swiper-arrows-container'>
        <button class='swiper-button swiper-button-prev  arrow-left' aria-label="<?= __('slider', 'headversity') ?>">
            <svg width='24' height='37' viewBox='0 0 24 37' fill='none'
                 xmlns='http://www.w3.org/2000/svg'>
                <path d='M21.1171 34.4222L3.90214 18.3194L20.8714 2.41621'
                      stroke='#FF9900' stroke-width='4' stroke-linecap='round'/>
            </svg>
        </button>
        <div class='swiper-pagination' role="button" aria-label="slider"></div>
        <button class='swiper-button swiper-button-next  arrow-right' aria-label="<?= __('slider', 'headversity') ?>">
            <svg width='23' height='37' viewBox='0 0 23 37' fill='none'
                 xmlns='http://www.w3.org/2000/svg'>
                <path d='M2.7774 34.4222L19.9924 18.3194L3.02308 2.41621'
                      stroke='#FF9900' stroke-width='4' stroke-linecap='round'/>
            </svg>
        </button>
    </div>
</div>
</section>


<!-- endregion headversity's Block -->

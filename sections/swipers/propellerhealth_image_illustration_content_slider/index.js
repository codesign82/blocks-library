import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/propellerhealth';
import {ScrollTrigger} from "gsap/ScrollTrigger";
import DrawSVGPlugin from "gsap/DrawSVGPlugin";

gsap.registerPlugin(ScrollTrigger, DrawSVGPlugin)

const blockScript = async (container = document) => {
  const block = container.querySelector('.image_illustration_content_slider');


  const steps = block.querySelectorAll('.slide-wrapper');
  const stepHeight = 500;
  const height = steps.length * stepHeight;
  const dots = block.querySelectorAll('.step-dot');
  const stepsWrapper = block.querySelector('.pin-element');
  let currentStep = 0;
  const headerHeight = 60


  const scrollTrigger = ScrollTrigger.create({
    trigger: stepsWrapper,
    start: `center center`,
    end: '+=' + height,
    pin: true,
    pinSpacing: true,
    scrub: true,
    onUpdate(self) {

      if (self.progress === 1) return
      const nextStep = ~~(self.progress * steps.length);
      const prevStep = ~~(-self.progress * steps.length);
      if (currentStep !== nextStep) {
        dots[currentStep].classList.remove('active');
        if (self.direction === 1) {
          steps[currentStep].classList.remove('active')
          steps[nextStep].classList.add('active')
        }
        if (self.direction === -1) {
          steps[currentStep].classList.remove('active')
          steps[nextStep].classList.add('active')
        }
      }
      currentStep = nextStep;
      dots[currentStep].classList.add('active');

    }
  })


  for (let i = 0; i < dots.length; i++) {
    const dot = dots[i];
    dot.addEventListener('click', () => {
      window.scrollTo(0, scrollTrigger.start + i * stepHeight * 1.05)
    })
  }

  
  animations(block);
  imageLazyLoading(block);
  
};
windowOnLoad(blockScript);



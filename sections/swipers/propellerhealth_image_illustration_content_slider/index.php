<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'image_illustration_content_slider';
$className = 'image_illustration_content_slider';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/image_illustration_content_slider/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
?>
<!-- region propellerhealth Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<h2 class="headline-3 block-title iv-st-from-bottom"><?= $title ?></h2>


<div class="pin-element">
    <svg class="dots-animation-i-e site-dots" fill="none">
        <path class="dot-line-animation" d="" stroke="#90E2C8" stroke-width="3" stroke-linecap="round" stroke-dasharray="1 10 1 10"/>
    </svg>
    <div class="container">

        <div class="content-wrapper">
            <div class="slides-wrapper">
                <?php
                $index = 0;
                $slider_titles = get_field("slider_titles");
                while (have_rows('slider_titles')) {
                    the_row();
                    $slide_title = get_sub_field('slide_title');
                    $description = get_sub_field('description');
                    $image = get_sub_field('image');

                    ?>
                    <div class="slide-wrapper <?= $index++ ? '' : 'active' ?>">
                        <div class="moving-text-wrapper iv-st-from-bottom">
                            <div class="slide-numbers-wrapper">
                                <span class="counting-number"><?= get_row_index() ?></span>
                                <span
                                        class="slides-numbers">/<?= count($slider_titles) ?></span>
                            </div>

                            <div class="text-wrapper">
                                <h5 class="headline-3 title"><?= $slide_title ?></h5>
                                <div
                                        class="repeater-text wysiwyg-block"><?= $description ?></div>
                            </div>
                        </div>
                        <picture class="illustration-wrapper iv-st-from-bottom">
                            <?= get_acf_image( $image, 'img-392-463' ) ?>
                        </picture>
                    </div>
                <?php } ?>
            </div>
            <div class="moving-shape slide-shape"></div>
            <?php if (have_rows('slider_titles')) { ?>
                <ul class="steps-dots-wrapper step-bullets">
                    <?php while (have_rows('slider_titles')) {
                        the_row();
                        ?>
                        <li tabindex="0"
                            class="step-dot dot-link <?= get_row_index() === 1 ? 'active' : '' ?> "
                            aria-label="swipe to anther slide">
                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>
        </div>
    </div>

</div>
</section>


<!-- endregion propellerhealth Block -->

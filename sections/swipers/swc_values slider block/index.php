<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'values_slider_block';
$className = 'values_slider_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
    '/template-parts/blocks/values_slider_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$cover_background = get_field('cover_background');
$title = get_field('title');
?>
<!-- region SWC's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="values-slider-wrapper">
  <div class="values-slider-title iv-st-from-bottom-f">
    <?php if ($title) { ?>
      <h2 class="headline-1"><?=$title?></h2>
    <?php } ?>
    <div class="swiper-button-prev swiper-button">
      <svg width="13" height="20" viewBox="0 0 13 20" fill="none">
        <path
          d="M12.6835 2.35L10.3335 0L0.333496 10L10.3335 20L12.6835 17.65L5.05016 10L12.6835 2.35Z"
          fill="currentColor"/>
      </svg>
    </div>
    <div class="swiper-button-next swiper-button">
      <svg width="13" height="20" viewBox="0 0 13 20" fill="none">
        <path
          d="M0.316504 2.35L2.6665 0L12.6665 10L2.6665 20L0.316504 17.65L7.94984 10L0.316504 2.35Z"
          fill="currentColor"/>
      </svg>
    </div>
  </div>
  <div class="swiper-container iv-st-from-bottom-f">
    <div class="swiper-wrapper">
      <?php
      if (have_rows('flip_cards')) {
        while (have_rows('flip_cards')) {
          the_row();
          $logo_icon = acf_icon(get_sub_field('logo_icon'));
          $off_hover_title = get_sub_field('off_hover_title');
          $subtitle = get_sub_field('subtitle');
          $off_hover_dots_icon = acf_icon(get_sub_field('off_hover_dots_icon'));
          $hover_state_title = get_sub_field('hover_state_title');
          $description = get_sub_field('description');
          $hover_state_dots_icon = acf_icon(get_sub_field('hover_state_dots_icon'));
          ?>
          <div class="swiper-slide">
            <div class="square-flip">
              <div class="square square-box">
                <div class="square-container square-container-box">
                  <?php if ($logo_icon) { ?>
                    <div class="logo-icon"><?=$logo_icon?></div>
                  <?php } ?>
                  <div class="details">
                    <?php if ($off_hover_title) { ?>
                      <p class="headline-5"><?=$off_hover_title?></p>
                    <?php } ?>
                    <?php if ($subtitle) { ?>
                      <h6 class="subtitle"><?=$subtitle?></h6>
                    <?php } ?>
                    <div class="bullets">
                      <svg width="56" height="16" viewBox="0 0 56 16"
                           fill="none"
                      >
                        <path
                          d="M34.9242 7.86116C34.7814 3.67263 30.9777 0.30459 26.7915 0.67199C22.9873 1.00595 20.0392 4.32258 20.0143 8.02835C19.9871 12.1149 23.5211 15.74 27.8081 15.5519C31.8812 15.3734 35.0593 11.8219 34.9242 7.86116ZM27.6386 12.71C24.6617 12.8517 22.0389 10.1123 22.3867 7.35991C22.6578 5.20901 24.677 3.57766 26.7915 3.34735C29.2792 3.07609 32.0286 4.72918 32.3827 7.35991C32.7516 10.0997 30.3449 12.5808 27.6386 12.71ZM24.4356 7.84918C24.4248 6.65453 25.2026 5.32698 26.4919 5.00841C28.0337 4.62744 29.7075 5.83832 29.9877 7.44335C30.2658 9.03621 29.1225 10.7422 27.5201 10.8929C25.8401 11.0506 24.449 9.4258 24.4356 7.84918ZM10.4508 7.36044C10.5713 6.20966 11.6875 5.04218 13.105 5.10423C14.4639 5.16329 15.4584 6.3242 15.5551 7.56566C15.6475 8.74149 14.9275 9.90718 13.9217 10.2323C12.3354 10.7448 10.2695 9.08869 10.4508 7.36044ZM41.866 10.1106C43.0645 10.1655 44.0595 9.23006 44.251 8.15027C44.4921 6.7901 43.4278 5.37743 42.0825 5.31897C40.6496 5.25633 39.4684 6.7555 39.6974 8.15027C39.8609 9.14117 40.7331 10.0587 41.866 10.1106ZM1.85928 9.50277C2.71081 9.47338 3.45499 8.79948 3.56234 7.94753C3.68843 6.94733 2.90165 5.96843 1.85928 5.92583C0.894864 5.88622 0.0429079 6.66192 0.00158807 7.63656C-0.0431396 8.68234 0.863768 9.53728 1.85928 9.50277ZM52.7264 5.97256C51.9322 6.17102 51.299 6.97985 51.3785 7.86822C51.4679 8.8633 52.4197 9.60282 53.4002 9.49296C54.3043 9.39175 55.0167 8.59238 55.0174 7.73276C55.019 6.61206 53.8109 5.70164 52.7264 5.97256Z"
                          fill="#000"/>
                      </svg>
                    </div>
                  </div>
                </div>
              </div>
              <div class="square2 square-box">
                <div class="square-container2 square-container-box">
                  <?php if ($hover_state_title) { ?>
                    <h3 class="headline-3"><?=$hover_state_title?></h3>
                  <?php } ?>
                  <?php if ($description) { ?>
                    <div class="paragraph"><?=$description?></div>
                  <?php } ?>
                  <div class="bullets">
                    <svg width="56" height="16" viewBox="0 0 56 16"
                         fill="none"
                    >
                      <path
                        d="M34.9242 7.86116C34.7814 3.67263 30.9777 0.30459 26.7915 0.67199C22.9873 1.00595 20.0392 4.32258 20.0143 8.02835C19.9871 12.1149 23.5211 15.74 27.8081 15.5519C31.8812 15.3734 35.0593 11.8219 34.9242 7.86116ZM27.6386 12.71C24.6617 12.8517 22.0389 10.1123 22.3867 7.35991C22.6578 5.20901 24.677 3.57766 26.7915 3.34735C29.2792 3.07609 32.0286 4.72918 32.3827 7.35991C32.7516 10.0997 30.3449 12.5808 27.6386 12.71ZM24.4356 7.84918C24.4248 6.65453 25.2026 5.32698 26.4919 5.00841C28.0337 4.62744 29.7075 5.83832 29.9877 7.44335C30.2658 9.03621 29.1225 10.7422 27.5201 10.8929C25.8401 11.0506 24.449 9.4258 24.4356 7.84918ZM10.4508 7.36044C10.5713 6.20966 11.6875 5.04218 13.105 5.10423C14.4639 5.16329 15.4584 6.3242 15.5551 7.56566C15.6475 8.74149 14.9275 9.90718 13.9217 10.2323C12.3354 10.7448 10.2695 9.08869 10.4508 7.36044ZM41.866 10.1106C43.0645 10.1655 44.0595 9.23006 44.251 8.15027C44.4921 6.7901 43.4278 5.37743 42.0825 5.31897C40.6496 5.25633 39.4684 6.7555 39.6974 8.15027C39.8609 9.14117 40.7331 10.0587 41.866 10.1106ZM1.85928 9.50277C2.71081 9.47338 3.45499 8.79948 3.56234 7.94753C3.68843 6.94733 2.90165 5.96843 1.85928 5.92583C0.894864 5.88622 0.0429079 6.66192 0.00158807 7.63656C-0.0431396 8.68234 0.863768 9.53728 1.85928 9.50277ZM52.7264 5.97256C51.9322 6.17102 51.299 6.97985 51.3785 7.86822C51.4679 8.8633 52.4197 9.60282 53.4002 9.49296C54.3043 9.39175 55.0167 8.59238 55.0174 7.73276C55.019 6.61206 53.8109 5.70164 52.7264 5.97256Z"
                        fill="#FFEE92"/>
                    </svg>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php
        }
      }
      ?>
    </div>
  </div>
</div>
</section>

<!-- endregion SWC's Block -->

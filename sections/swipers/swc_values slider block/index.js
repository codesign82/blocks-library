import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import {Swiper, Navigation} from 'swiper';
import '../../../scripts/sitesSizer/swc';

/**
 *
 * @param container {Document}
 * @returns {Promise<void>}
 */
Swiper.use([Navigation]);
const blockScript = async (container = document) => {
const block = container.querySelector('.values_slider_block');
  

  // add block code here
  let slides = block.querySelectorAll('.swiper-slide');
  new Swiper(block.querySelector('.swiper-container'), {
    slidesPerView: 'auto', //4.7
    spaceBetween: 0,
    loop: true,
    loopedSlides: slides.length,
    slideToClickedSlide: true,
    navigation: {
      nextEl: block.querySelector('.swiper-button-next'),
      prevEl: block.querySelector('.swiper-button-prev')
    },
    breakpoints: {
      600: {
        slideToClickedSlide: false,
      }
    }
  })
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);

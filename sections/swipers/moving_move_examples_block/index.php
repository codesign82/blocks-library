<?php
// Create id attribute allowing for custom "anchor" value.
$id = 'move_examples_block';

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'move_examples_block';
$className = 'move_examples_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/move_examples_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$text_with_gradient = get_field('text_with_gradient');
$first_text = @$text_with_gradient['first_text'];
$gradient_text = @$text_with_gradient['gradient_text'];
$last_text = @$text_with_gradient['last_text'];


$move_examples_slider = get_field('move_examples_slider');
$skd_group_text = @$move_examples_slider['skd_group_text'];
$whitelabel_group_text = @$move_examples_slider['whitelabel_group_text'];
$first_tab_text = @$move_examples_slider['first_tab_text'];
$second_tab_text = @$move_examples_slider['second_tab_text'];
$third_tab_text = @$move_examples_slider['third_tab_text'];
$fourth_tab_text = @$move_examples_slider['fourth_tab_text'];


?>
<!-- region MOVING's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container g-container-remove-sm-0">
    <div class="circle-with-line">
        <div class="line-div"></div>
        <div class="circle-div"></div>
        <!--        <svg width="34" height="34" viewBox="0 0 34 34" fill="none" xmlns="http://www.w3.org/2000/svg">-->
        <!--          <circle cx="17" cy="17" r="15" stroke="#DEDFE3" stroke-width="3"/>-->
        <!--        </svg>-->
    </div>
    <h2 class="move_examples_block_title headline-1 text-uppercase a7a-test-from0-bottom">
        <?php if ($first_text) { ?>
            <?= $first_text ?>
        <?php } ?>
        <?php if ($gradient_text) { ?>
            <span class="green-gradiant"><?= $gradient_text ?></span>
        <?php } ?>
        <?php if ($last_text) { ?>
            <?= $last_text ?>
        <?php } ?>
    </h2>
    <!--  region prev ver of tabs controller-->
    <div class="tabs-controller hide-scrollbar g-container-set-sm">
        <div class="tab a7a-test-from0-bottom">
            <?php if ($skd_group_text) { ?>
                <p class="tab-title"><?= $skd_group_text ?></p>
            <?php } ?>
            <div class="tab-buttons">

                <?php if ($first_tab_text) { ?>
                    <button class="tab-button" aria-label="Tab Controller"><?= $first_tab_text ?></button>
                <?php } ?>
                <?php if ($second_tab_text) { ?>
                    <button class="tab-button" aria-label="Tab Controller"><?= $second_tab_text ?></button>
                <?php } ?>
            </div>
        </div>
        <div class="tab a7a-test-from0-bottom">
            <?php if ($whitelabel_group_text) { ?>
                <p class="tab-title"><?= $whitelabel_group_text ?></p>
            <?php } ?>
            <div class="tab-buttons">

                <?php if ($third_tab_text) { ?>
                    <button class="tab-button" aria-label="Tab Controller"><?= $third_tab_text ?></button>
                <?php } ?>
                <?php if ($fourth_tab_text) { ?>
                    <button class="tab-button" aria-label="Tab Controller"><?= $fourth_tab_text ?></button>
                <?php } ?>
            </div>
        </div>
    </div>
    <!--  endregion prev ver of tabs controller-->
    <!--      <div class="swiper-pagination"></div>-->
</div>

<!--  name of group-->
<?php if (have_rows('move_examples_slider')) : ?>

    <?php while (have_rows('move_examples_slider')) : the_row();

// name of repeat
        if (have_rows('sliders')) : ?>

            <div class="swiper-container move_examples_block_slider a7a-test-from0-bottom">
                <div class="swiper-wrapper">
                    <?php
                    while (have_rows('sliders')) : the_row();
                        $title = get_sub_field('title');
                        $description = get_sub_field('description');
                        $image = get_sub_field('image');
                        ?>
                        <div class="swiper-slide" data-title="SDK">
                            <div class="move-example-slider">
                                <div class="move-example-slider-details">
                                    <?php if ($title) { ?>
                                        <h5 class="move-example-slider-title headline-5"><?= $title ?></h5>
                                    <?php } ?>
                                    <?php if ($description) { ?>
                                        <div class="move-example-slider-description paragraph paragraph-20">
                                            <?= $description ?>
                                        </div>
                                    <?php } ?>
                                    <div class="move-example-slider-details-info">

                                        <?php
                                        if (have_rows('numbers_and_text')) {
                                            ?>
                                            <?php
                                            while (have_rows('numbers_and_text')) {
                                                the_row();
                                                $number = get_sub_field('number');
                                                $text = get_sub_field('text');
                                                ?>
                                                <div class="move-example-slider-details-info-wrapper">
                                                    <?php if ($number) { ?>
                                                        <p class="move-example-slider-details-info-number"><?= $number ?></p>
                                                    <?php } ?>
                                                    <?php if ($text) { ?>
                                                        <p class="move-example-slider-details-info-downloads"><?= $text ?></p>
                                                    <?php } ?>
                                                </div>

                                                <?php
                                            } ?>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?php if ($image) { ?>
                                    <div class="move-example-slider-image">
                                        <picture class="aspect-ratio">
                                            <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                                        </picture>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif; ?>

    <?php endwhile; ?>
<?php endif; ?>


</section>


<!-- endregion MOVING's Block -->

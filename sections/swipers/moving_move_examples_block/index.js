import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/moving';
import Swiper from "swiper";

const blockScript = async (container = document) => {
  const block = container.querySelector('.move_examples_block');


  // add block code here
  const tabButtons = block.querySelectorAll('.tab-button');
  const examplesSlider = new Swiper(block.querySelector('.move_examples_block_slider'), {
    // slidesPerView: 2.08,
    slidesPerView: 1,
    // slidesPerView: 1,
    spaceBetween: 0,
    centeredSlides: true,
    breakpoints: {
      600: {
        slidesPerView: 1.5,
        initialSlide: 1,
      },
      992: {
        slidesPerView: 2.14,
        initialSlide: 1,
      },
      3000: {
        slidesPerView: 3,
        initialSlide: 1,
      }
    },
    on: {
      slideChange: ({activeIndex}) => {
        tabButtons.forEach((tabButton) => {
          tabButton.classList.remove('tab-button-active');
        });
        tabButtons[activeIndex].classList.add('tab-button-active');
      }
    }
  })

  // tabs slide to
  // region set active tab
  tabButtons[examplesSlider.activeIndex].classList.add('tab-button-active');
  // endregion set active tab
  tabButtons.forEach((tabButton, i) => {
    tabButton.addEventListener('click', function (e) {
      examplesSlider.slideTo(i);
      tabButtons.forEach((tabButton) => {
        tabButton.classList.remove('tab-button-active');
      });
      tabButton.classList.add('tab-button-active');
    });
  });
  
  animations(block);
  imageLazyLoading(block);
  
};
windowOnLoad(blockScript);



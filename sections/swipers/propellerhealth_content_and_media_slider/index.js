import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/propellerhealth';
import Swiper, {Pagination, Controller, EffectFade} from "swiper";



const blockScript = async (container = document) => {
  const block = container.querySelector('.content_and_media_slider');

  const swiper = new Swiper(block.querySelector('.left-content.swiper'), {
    loop: true,
    modules: [Pagination, Controller],
    spaceBetween: 25,
    pagination: {
      el: block.querySelector('.swiper-pagination'),
      type: 'bullets',
      clickable: true,
    },
    breakpoints: {

      600: {
        spaceBetween: 25,
      },
      992: {
        spaceBetween: 42,
      }
    }
  });

  const swiper2 = new Swiper(block.querySelector('.right-content.swiper'), {
    modules: [Controller, EffectFade],
    effect: "fade",
    speed: 500,
    loop: true,
    fadeEffect: {
      crossFade: true
    },

  });
  swiper.controller.control = swiper2;
  swiper2.controller.control = swiper;
  
  animations(block);
  imageLazyLoading(block);
  
};
windowOnLoad(blockScript);



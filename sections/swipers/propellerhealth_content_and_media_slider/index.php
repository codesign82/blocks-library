<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'content_and_media_slider';
$className = 'content_and_media_slider';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/content_and_media_slider/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
?>
<!-- region propellerhealth Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="content-wrapper">
        <?php if (have_rows('sliders')) { ?>
            <div class="swiper mySwiper left-content">
                <div class="swiper-wrapper">
                    <?php while (have_rows('sliders')) {
                        the_row();
                        $image = get_sub_field('image');
                        ?>
                        <div class="swiper-slide">
                            <?php if ($image) { ?>
                                <picture class="image-wrapper aspect-ratio">
                                    <?= get_acf_image( $image, 'img-494-600' ) ?>
                                </picture>
                            <?php } ?>
                        </div>
                    <?php } ?>

                </div>
                <div class="swiper-pagination"></div>
            </div>
            <div class="swiper mySwiper right-content">
                <div class="swiper-wrapper">
                    <?php while (have_rows('sliders')) {
                        the_row();
                        $title = get_sub_field('title');
                        $description = get_sub_field('description');
                        $link = get_sub_field('link');
                        ?>
                        <div class="swiper-slide">
                            <div class="text-content">
                                <h3 class="headline-3 title"><?= $title ?></h3>
                                <div class="paragraph wysiwyg-block description"><?= $description ?></div>
                                <a role="button" class="cta-button swiper-btn" tabindex="0" href="<?= $link["url"] ?>"><?= $link["title"] ?></a>
                            </div>
                        </div>
                    <?php } ?>

                </div>
                <div class="swiper-pagination"></div>
            </div>
        <?php } ?>
    </div>
</div>
</section>


<!-- endregion propellerhealth Block -->

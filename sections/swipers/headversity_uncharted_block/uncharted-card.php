<?php
$post_id = @$args['post_id'] ?: get_the_ID();
$slide_index = @$args['slide_index'] ?: 0;
$post_index = @$args['post_index'] ?: 0;
$thumbnail_id = get_post_thumbnail_id();
$alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);
$terms = get_the_terms($post_id, 'uncharted_category');
$episode_title = get_field('episode_title', $post_id);
$episode_number = get_field('episode_number', $post_id);
$video_type = get_field('video_type', $post_id);
$video_url = get_field('video_url', $post_id);
$video_file = get_field('video_file', $post_id);
$youtube = get_field('youtube', $post_id);
$final_video = $video_type === 'url' ? $video_url : $video_file;
if ($video_type === 'youtube') {
  $final_video = $youtube;
}
$video_type_final = $video_type === 'youtube' ? 'youtube' : 'video';
$episode_number_final = $episode_number ?: get_post_meta($post_id, 'custom_episode_number', true);
?>
<div class="col iv-st-from-bottom uncharted-card" id="post-id-<?= $post_id; ?>">
  <div class="card media-card" data-video-url="<?= $final_video ?>" data-video-type="<?= $video_type_final ?>">
    <div class="card-image aspect-ratio ">
      <img src="<?php thumbnail_url($post_id); ?>" alt="<?= $alt ?: 'Image not found' ?>">
      <svg class="play-icon" data-slide-index="<?= $slide_index ?>" data-slide-number="<?=$post_index ?>" width="76" height="76" viewBox="0 0 76 76" fill="none" aria-label="<?= __('open video', 'headversity') ?> "
           xmlns="http://www.w3.org/2000/svg">
        <circle cx="38" cy="38" r="38" fill="white"/>
        <path d="M55.8691 34.7311L26.9434 51.4314V18.0308L55.8691 34.7311Z"
              fill="#FF9900"/>
        <path d="M30.1562 22.924L56.082 37.8923L30.1562 52.8605V22.924Z"
              stroke="#0A1B57" stroke-width="2"/>
      </svg>
    </div>
    <?php if (!empty($terms) && is_array($terms)) { ?>
      <div class="category-title paragraph color-transition">
        <?php foreach ($terms as $term) { ?>
          <a class="category-link" href="<?= get_term_link($term->slug, 'uncharted_category') ?>"
             aria-label="<?= __('go to uncharted category', 'headversity') ?> ">
            <?= $term->name ?>
          </a>
        <?php } ?>
      </div>
    <?php } ?>

    <a href="<?= get_the_permalink($post_id) ?>" class="card-title headline-5"
       aria-label="<?= __('go to post page', 'headversity') ?> "><?= get_the_title($post_id); ?></a>
    <?php if ($episode_title) { ?>
      <div class="date paragraph">
        <?= $episode_title . ' ' . $episode_number_final ?>
        <span class="date-separator">|</span>
        <span class="video-duration">00:00</span>
      </div>
    <?php } ?>
  </div>
</div>

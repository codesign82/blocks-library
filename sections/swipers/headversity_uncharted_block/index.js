import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/propellerhealth';


import Swiper, {Navigation, Controller} from "swiper";
import {swiperActivator} from "../../../scripts/general/swiper-activator";

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.uncharted_block');


  const mediaCards = block.querySelectorAll('.media-card');
  const mediaThumbs = block.querySelectorAll('.bottom-section .swiper-slide');
  const allVideosLoaded = [];

  // region function to get youtube ID
  function getId(url) {
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    const match = url.match(regExp);

    return (match && match[2].length === 11)
        ? match[2]
        : null;
  }

  // endregion function to get youtube ID

  // region get video duration

  for (let i = 0; i < mediaCards.length; i++) {
    let mediaCard = mediaCards[i];
    let mediaThumb = mediaThumbs[i];
    const videoURL = mediaCard.dataset.videoUrl;
    const videoType = mediaCard.dataset.videoType;
    const videoDuration = mediaCard?.querySelector('.video-duration');
    const thumbVideoDuration = mediaThumb?.querySelector('.episode-time');
    if (videoURL && videoType !== 'youtube') {
      const videoLoaded = new Promise(resolve => {
        // const episodeTime = mediaCard?.querySelector('.episode-time');
        let dummyVideo = document.createElement('video');

        // Define the URL of the MP3 audio file
        dummyVideo.src = videoURL;

        // Once the metadata has been loaded, display the duration in the console

        dummyVideo?.addEventListener('loadedmetadata', function () {
          // Obtain the duration in seconds of the audio file (with milliseconds as well, a float value)
          const duration = dummyVideo.duration;

          if (duration < 60) {
            videoDuration.innerHTML = Math.round(duration) + ' Sec.';
            thumbVideoDuration.innerHTML = Math.round(duration) + ' Sec.';
          } else {
            videoDuration.innerHTML = Math.floor(duration / 60) + ' Min.';
            thumbVideoDuration.innerHTML = Math.floor(duration / 60) + ' Min.';
          }
          dummyVideo.remove();
          resolve();
        }, false);
      })
      allVideosLoaded.push(videoLoaded)
    } else {
      videoDuration.closest('.date')?.classList.add('no-date');
      thumbVideoDuration.style.opacity = 0;
    }
  }


  // endregion get video duration

  const slides = block.querySelectorAll('.swiper-slide');

  const swiperChecker = slides.length > 3 ? {
    loop: true,
  } : {
    loop: false,
  };

  // region video modal
  // const playerIcon = block.querySelector(".player-icon-wrapper");
  const modal = block.querySelector(".modal");
  const exit = block.querySelector(".exit");

  // playerIcon?.addEventListener("click", videoToggling);

  exit?.addEventListener("click", hide);
  modal?.addEventListener("click", hide);

  // modal?.addEventListener("click", hide);

  function hide(e) {
    if (e.target.closest('.modal-content') && !e.target.closest('.exit')) return;
    document.documentElement.classList.remove('modal-opened');
    modal.classList.remove("show");
    const activeSlide = block.querySelector('.modal .swiper-slide-active');
    const activeVideo = activeSlide?.querySelector('.video');
    if (activeVideo.dataset.videoType === 'youtube') {
      const videoId = getId(activeVideo.src);
      activeVideo.src = 'https://www.youtube.com/embed/' + videoId;
    } else {
      activeVideo?.pause();
    }
  }

  window.addEventListener('keydown', function (event) {
    if (event.key === 'Escape') {
      hide(event);
    }
  })

  await Promise.all(allVideosLoaded)

  let mainSwiper;
  swiperActivator('(min-width:600px)', () => {
    mainSwiper = new Swiper(block.querySelector('.main-swiper'), {
      modules: [Navigation],
      slidesPerView: 1,
      spaceBetween: 20,
      navigation: {
        nextEl: block.querySelector('.swiper-button-next'),
        prevEl: block.querySelector('.swiper-button-prev')
      },
      breakpoints: {
        600: {
          slidesPerView: 2.5,
          spaceBetween: 20
        },
        992: {
          slidesPerView: 3,
          spaceBetween: 34
        }
      },
      ...swiperChecker,
    });
  }, () => mainSwiper?.destroy?.())

  const galleryThumbs = new Swiper(block.querySelector('.mySwiper'), {
    // initialSlide:1,
    spaceBetween: 10,
    slidesPerView: 2,
    // centeredSlides:true,
    slideToClickedSlide: true,
    loop: true,
    loopedSlides: 5, //looped slides should be the same
    modules: [Controller],
    breakpoints: {
      600: {
        spaceBetween: 10,
        slidesPerView: 3,
      },
      992: {
        slidesPerView: 3,
        spaceBetween: 112,
      }
    }
  });
  const galleryTop = new Swiper(block.querySelector('.mySwiper2'), {
    // initialSlide: 1,
    spaceBetween: 10,
    centeredSlides: true,
    loop: true,
    loopedSlides: 5, //looped slides should be the same
    modules: [Controller],
  });

  galleryThumbs.controller.control = galleryTop;
  galleryTop.controller.control = galleryThumbs;
  // endregion video modal

  const playVideo = block.querySelectorAll(".play-icon");

  for (let playVideoElement of playVideo) {
    playVideoElement?.addEventListener("click", () => {
      modal.classList.add("show");
      document.documentElement.classList.add('modal-opened');
      const slideIndex = +playVideoElement.dataset.slideIndex;
      galleryThumbs.slideToLoop(slideIndex, 0);
      const activeSlide = block.querySelector('.modal .swiper-slide-active');
      const activeVideo = activeSlide?.querySelector('.video');
      if (activeVideo.dataset.videoType === 'youtube') {
        const videoId = getId(activeVideo.src);
        activeVideo.src = 'https://www.youtube.com/embed/' + videoId + '?autoplay=1';
      } else {
        activeVideo?.play();
      }
      // galleryTop.slideToLoop(slideIndex,0);
    });
  }


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


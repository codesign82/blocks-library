<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'uncharted_block';
$className = 'uncharted_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/uncharted_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$uncharted = get_field('uncharted');
$show_posts_manually_or_automatically = get_field('show_posts_manually_or_automatically');
// Args
$query_options = get_field('query_options');
$categories = @$query_options['categories'];
$number_of_posts = @$query_options['posts_per_page'];
$order = @$query_options['order'];
$args = array(
    'post_type' => 'uncharted',
    'posts_per_page' => $number_of_posts,
    'order' => $order,
    'post_status' => 'publish',
    'tax_query' => $categories ? array(
        array(
            'taxonomy' => 'uncharted_category',
            'field' => 'term_id',
            'terms' => $categories
        )
    ) : array()
);
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="resources-content-wrapper">
        <div class="content-wrapper">
            <div class="top-content">
                <div class="left-content">
                    <?php if ($title) { ?>
                        <h2 move-to-here class="headline-4 title"><?= $title ?></h2>
                    <?php } ?>
                    <?php if ($description) { ?>
                        <div class="paragraph description"><?= $description ?></div>
                    <?php } ?>
                </div>
            </div>
            <?php if ($show_posts_manually_or_automatically === 'manually') { ?>
                <div class="resources-cards">
                    <?php if ($uncharted) { ?>
                        <?php if (count($uncharted) > 3) { ?>
                            <button class="swiper-button swiper-button-prev arrow-left"
                                    aria-label="<?= __('slider', 'headversity') ?>">
                                <svg width="24" height="37" viewBox="0 0 24 37" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M21.1171 34.4222L3.90214 18.3194L20.8714 2.41621" stroke="#FF9900"
                                          stroke-width="4" stroke-linecap="round"/>
                                </svg>
                            </button>
                        <?php } ?>
                        <div class="main-swiper swiper">
                            <div class="swiper-wrapper">
                                <?php
                                $slide_index = 0;
                                foreach ($uncharted as $post):
                                    $args_card = array('post_id' => $post, 'slide_index' => $slide_index);
                                    setup_postdata($post); ?>
                                    <div class="swiper-slide">
                                        <?php get_template_part('template-parts/uncharted-card', '', $args_card) ?>
                                    </div>
                                    <?php
                                    $slide_index++;
                                endforeach;
                                wp_reset_postdata();
                                ?>
                            </div>
                        </div>
                        <?php if (count($uncharted) > 3) { ?>
                            <button class="swiper-button swiper-button-next arrow-right"
                                    aria-label="<?= __('slider', 'headversity') ?>">
                                <svg width="23" height="37" viewBox="0 0 23 37" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M2.7774 34.4222L19.9924 18.3194L3.02308 2.41621" stroke="#FF9900"
                                          stroke-width="4" stroke-linecap="round"/>
                                </svg>
                            </button>
                        <?php } ?>
                    <?php } ?>
                </div>
            <?php } else { ?>
                <div class="resources-cards">
                    <!--  args -->
                    <?php
                    $the_query = new WP_Query($args); ?>
                    <?php if ($the_query->post_count > 3) { ?>
                        <button class="swiper-button swiper-button-prev arrow-left"
                                aria-label="<?= __('slider', 'headversity') ?>">
                            <svg width="24" height="37" viewBox="0 0 24 37" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M21.1171 34.4222L3.90214 18.3194L20.8714 2.41621" stroke="#FF9900"
                                      stroke-width="4" stroke-linecap="round"/>
                            </svg>
                        </button>
                    <?php } ?>
                    <div class="main-swiper swiper">
                        <div class="swiper-wrapper">
                            <?php
                            // The Loop
                            $slide_index = 0;
                            if ($the_query->have_posts()) { ?>
                                <?php while ($the_query->have_posts()) {
                                    $the_query->the_post();
                                    ?>
                                    <div class="swiper-slide">
                                        <?php get_template_part("template-parts/uncharted-card", '', array('post_id' => get_the_ID(), 'slide_index' => $slide_index)); ?>
                                    </div>
                                    <?php $slide_index++;
                                } ?>
                            <?php }
                            wp_reset_postdata(); ?>
                        </div>
                    </div>
                    <?php if ($the_query->post_count > 3) { ?>
                        <button class="swiper-button swiper-button-next arrow-right"
                                aria-label="<?= __('slider', 'headversity') ?>">
                            <svg width="23" height="37" viewBox="0 0 23 37" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M2.7774 34.4222L19.9924 18.3194L3.02308 2.41621" stroke="#FF9900"
                                      stroke-width="4" stroke-linecap="round"/>
                            </svg>
                        </button>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="modal" aria-modal="true" role="dialog">
    <div class="modal-content">
        <div class="swiper mySwiper2">
            <div class="swiper-wrapper">
                <?php if ($show_posts_manually_or_automatically === 'manually') {
                    if ($uncharted):
                        $slide_index = 0;
                        foreach ($uncharted as $post):
                            $episode_title = get_field('episode_title', $post);
                            $episode_number = get_field('episode_number', $post);
                            $video_type = get_field('video_type', $post);
                            $video_url = get_field('video_url', $post);
                            $video_file = get_field('video_file', $post);
                            $youtube = get_field('youtube', $post);
                            $final_video = $video_type === 'url' ? $video_url : $video_file;
                            setup_postdata($post); ?>
                            <div class="swiper-slide modal-video aspect-ratio" aria-modal="true" role="dialog">
                                <?php if ($video_type !== 'youtube') { ?>
                                    <video poster="<?php thumbnail_url($post) ?>"
                                           data-src="<?= $final_video ?>"
                                           type="video/mp4" playsinline controls
                                           class="video" data-video-type="<?= $video_type ?>">
                                    </video>
                                <?php } else {
                                    ?>
                                    <iframe class="video" data-video-type="<?= $video_type ?>" width="560" height="315"
                                            src="<?= generateVideoEmbedUrl($youtube) ?>" title="YouTube video player"
                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen></iframe>
                                    <?php
                                } ?>
                            </div>
                        <?php endforeach;
                        wp_reset_postdata();
                    endif; ?>
                <?php } else {
                    // The Query
                    $the_query = new WP_Query($args);
                    // The Loop
                    if ($the_query->have_posts()) { ?>
                        <?php while ($the_query->have_posts()) {
                            $the_query->the_post();
                            $thumb_id = get_the_ID();
                            $episode_title = get_field('episode_title', $thumb_id);
                            $episode_number = get_field('episode_number', $thumb_id);
                            $video_type = get_field('video_type', $thumb_id);
                            $video_url = get_field('video_url', $thumb_id);
                            $video_file = get_field('video_file', $thumb_id);
                            $youtube = get_field('youtube', $thumb_id);
                            $final_video = $video_type === 'url' ? $video_url : $video_file;
                            // DO Not Delete Video Type For JS
                            $video_type_final = $video_type === 'youtube' ? 'youtube' : 'video';
                            ?>
                            <div class="swiper-slide modal-video aspect-ratio" aria-modal="true" role="dialog">
                                <?php if ($video_type !== 'youtube') { ?>
                                    <video poster="<?php thumbnail_url($thumb_id); ?>"
                                           data-src="<?= $final_video ?>"
                                           type="video/mp4" playsinline controls
                                           class="video" data-video-type="<?= $video_type_final ?>">
                                    </video>
                                <?php } else {
                                    ?>
                                    <iframe class="video" data-video-type="<?= $video_type_final ?>" width="560"
                                            height="315" src="<?= generateVideoEmbedUrl($youtube) ?>"
                                            title="YouTube video player"
                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen></iframe>
                                    <?php
                                } ?>
                            </div>
                        <?php } ?>
                    <?php }
                    wp_reset_postdata(); ?>
                <?php } ?>
            </div>
        </div>
        <div class="bottom-section">
            <div class="episodes-section-title headline-5">
                <?= __('Next episodes:', 'headversity') ?>
            </div>
            <div class="swiper mySwiper">
                <div class="swiper-wrapper episodes-wrapper">
                    <?php if ($show_posts_manually_or_automatically === 'manually') {
                        if ($uncharted):
                            $slide_index = 0;
                            foreach ($uncharted as $post):
                                $alt = get_post_meta(get_post_thumbnail_id($post), '_wp_attachment_image_alt', true);
                                $episode_title = get_field('episode_title', $post);
                                $episode_number = get_field('episode_number', $post);
                                $episode_number_final = $episode_number ?: get_post_meta($post, 'custom_episode_number', true);
                                setup_postdata($post); ?>
                                <div class="swiper-slide episode episode-thumb">
                                    <picture class="poster-img">
                                        <img src="<?php thumbnail_url($post); ?>"
                                             alt="<?= $alt ?: 'Image not found' ?>">
                                    </picture>
                                    <div class="episode-desc">
                                        <div class="episode-title"><?= $episode_title . ' ' . $episode_number_final ?></div>
                                        <div class="episode-time">00:00</div>
                                    </div>
                                </div>

                            <?php endforeach;
                            wp_reset_postdata();
                        endif; ?>
                    <?php } else {
                        // The Query
                        $the_query = new WP_Query($args);
                        // The Loop
                        if ($the_query->have_posts()) { ?>
                            <?php while ($the_query->have_posts()) {
                                $the_query->the_post();
                                $alt = get_post_meta(get_the_ID(), '_wp_attachment_image_alt', true);
                                $episode_title = get_field('episode_title', get_the_ID());
                                $episode_number = get_field('episode_number', get_the_ID());
                                $episode_number_final = $episode_number ?: get_post_meta(get_the_ID(), 'custom_episode_number', true);
                                ?>
                                <div class="swiper-slide episode episode-thumb">
                                    <picture class="poster-img">
                                        <img src="<?php thumbnail_url(get_the_ID()); ?>"
                                             alt="<?= $alt ?: 'Image not found' ?>">
                                    </picture>
                                    <div class="episode-desc">
                                        <div class="episode-title"><?= $episode_title . ' ' . $episode_number_final ?></div>
                                        <div class="episode-time">00:00</div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php }
                        wp_reset_postdata(); ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <svg class="exit" width="39" height="39" viewBox="0 0 39 39" fill="none" role="application"
             xmlns="http://www.w3.org/2000/svg">
            <line x1="9.90039" y1="28.2841" x2="28.2852" y2="9.8993"
                  stroke="#FF9900" stroke-width="3" stroke-linecap="round"/>
            <path d="M28.9922 28.2841L10.6074 9.89928" stroke="#FF9900"
                  stroke-width="3" stroke-linecap="round"/>
        </svg>
    </div>
</div>
</section>


<!-- endregion headversity's Block -->

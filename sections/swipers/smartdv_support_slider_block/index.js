import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {debounce} from "../../../scripts/functions/debounce";
import '../../../scripts/sitesSizer/propellerhealth';

import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger)



const blockScript = async (container = document) => {
  const block = container.querySelector('.support_slider_block');


  const steps = block.querySelectorAll('.slider');
  const stepsPaths = [...block.querySelectorAll('.slider-bg path')];
  const stepHeight = 500;
  const height = steps.length * stepHeight;
  const dots = block.querySelectorAll('.step-dot');
  const stepsWrapper = block.querySelector('.pin-element');
  let currentStep = 0;
  let nextStep = 0;
  const desktopSlopValue = 8.333334;
  const tabletSlopValue = 5.04;
  const mobileSlopValue = (parseFloat(window.getComputedStyle(block.querySelector(".container"), null).getPropertyValue('padding-left')) / window.innerWidth) * 100;
  let pathSlopValue = desktopSlopValue;
  let init = false;

  const fixPaths = debounce((scrollTrigger) => {
    if (!init) {
      init = true;
      currentStep = ~~(scrollTrigger.progress * steps.length);
      currentStep === steps.length && currentStep--;
      steps[currentStep].classList.add('active');
      dots[currentStep].classList.add('active');
      for (let i = 1; i <= currentStep; i++) {
        gsap.to(steps[i].querySelector(".slider-bg path"), {
          attr: {
            d: 'M 0 100 L 0 0 L 100 0 L 100 100 Z'
          }
        })
      }
    }

    pathSlopValue = window.innerWidth >= 992
        ? desktopSlopValue
        : window.innerWidth >= 600
            ? tabletSlopValue
            : mobileSlopValue;
    const paths = block.querySelectorAll('.slider.active~.slider .slider-bg path, .slides-bg path');
    for (let path of paths) {
      path.setAttribute('d', `M 0 100 L ${pathSlopValue} 0 L ${100 - pathSlopValue} 0 L 100 100 Z`);
    }
  }, 300);

  const scrollTrigger = ScrollTrigger.create({
    trigger: stepsWrapper,
    start: `center center`,
    end: '+=' + height,
    pin: true,
    pinSpacing: true,
    scrub: true,
    onRefresh: fixPaths,
    onUpdate(self) {
      nextStep = ~~(self.progress * steps.length);
      if (self.progress === 1 || nextStep === currentStep || (currentStep === 0 && self.direction === -1)) return
      dots[currentStep].classList.remove('active');
      dots[nextStep].classList.add('active');

      steps[currentStep].classList.remove('active');
      steps[nextStep].classList.add('active');

      self.direction === -1 && gsap.to(stepsPaths.slice(nextStep + 1, currentStep + 1).reverse(), {
        attr: {
          d: `M 0 100 L ${pathSlopValue} 0 L ${100 - pathSlopValue} 0 L 100 100 Z`
        },
      })
      self.direction === 1 && gsap.to(stepsPaths.slice(currentStep + 1, nextStep + 1), {
        attr: {
          d: 'M 0 100 L 0 0 L 100 0 L 100 100 Z'
        },
      })
      currentStep = nextStep;
    }
  })


  for (let i = 0; i < dots.length; i++) {
    const dot = dots[i];
    dot.addEventListener('click', () => {
      window.scrollTo(0, scrollTrigger.start + i * stepHeight * 1.05)
    })
  }


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



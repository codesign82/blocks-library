<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'support_slider_block';
$className = 'support_slider_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/support_slider_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$mainTitle = get_field('main_title');
$mainDescription = get_field('main_description');
?>
<!-- region samrt_dv Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="pin-element">
    <div class="container">

        <div class="text-wrapper">
            <?php if ($mainTitle) { ?>
                <div class="headline-2 main-title word-up"><?= $mainTitle ?></div>
            <?php } ?>
            <?php if ($mainDescription) { ?>
                <p
                        class="paragraph main-description iv-st-from-bottom"><?= $mainDescription ?></p>
            <?php } ?>
        </div>
        <div class="sliders-dots">

            <?php if (have_rows('sliders')) { ?>

                <div class="sliders-wrapper">
                    <ul class="steps-dots-wrapper step-bullets">
                        <?php while (have_rows('sliders')) {
                            the_row();
                            ?>
                            <li tabindex="0"
                                class="step-dot dot-link <?= get_row_index() === 1 ? '' : '' ?> "
                                aria-label="swipe to anther slide">
                            </li>
                        <?php } ?>
                    </ul>

                    <?php
                    $index = 0;
                    while (have_rows('sliders')) {
                        the_row();
                        $icon = get_sub_field('icon');
                        $image = get_sub_field('image');
                        $title = get_sub_field('title');
                        $description = get_sub_field('description');
                        $backgroundColor = get_sub_field('bottom_color');
                        ?>
                        <div
                                class="slider <?= $index === 0 ? '' : '' ?>"
                                style="color: <?= $backgroundColor ?>; --index:<?= $index++ ?>">
                            <div class="slider-bg">
                                <svg preserveAspectRatio="none" viewBox="0 0 100 100">
                                    <path d="M 0 100 L 0 0 L 100 0 L 100 100 Z"></path>
                                </svg>
                            </div>

                            <div class="content-wrapper">
                                <?php if ($icon) { ?>
                                    <div class="icon">
                                        <?= $icon ?>
                                    </div>
                                <?php } ?>
                                <?php if ($image) { ?>
                                    <div class="icon">
                                        <img src="<?= $image["url"] ?>"
                                             alt="<?= __(' Slider icon ', 'smart_dv') ?>">
                                    </div>
                                <?php } ?>
                                <div class="slider-text">
                                    <?php if ($title) { ?>
                                        <div class="headline-3 title"><?= $title ?></div>
                                    <?php } ?>
                                    <?php if ($description) { ?>
                                        <div class="paragraph description"><?= $description ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>


                </div>

            <?php } ?>

        </div>
        <div class="slides-bg">
            <svg preserveAspectRatio="none" viewBox="0 0 100 100">
                <path d="M -12.4 20 L 9.3 0 L 90.7 0 L 112.4 20 L 0 20 Z"></path>
            </svg>
        </div>

    </div>
</div>
</section>


<!-- endregion samrt_dv Block -->

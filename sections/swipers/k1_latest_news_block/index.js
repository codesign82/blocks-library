import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/propellerhealth';
import Swiper, {Navigation} from 'swiper'


Swiper.use([Navigation]);

const blockScript = async (container = document) => {
  const block = container.querySelector('.latest_news_block');

  let swiper = new Swiper(block.querySelector(".swiper"), {
    slidesPerView: 1.3,
    spaceBetween: 30,
    navigation: {
      nextEl: block.querySelector('.swiper-button-next'),
      prevEl: block.querySelector('.swiper-button-prev')
    },
    // loop: true,
    // loopedSlides:6,
    breakpoints: {
      600: {
        slidesPerView: 2.5,
        spaceBetween: 15,
      },
      992: {
        slidesPerView: 3.5,
        spaceBetween: 15,
      },
    },
  });




  animations(block);
  imageLazyLoading(block);
  
};
windowOnLoad(blockScript);



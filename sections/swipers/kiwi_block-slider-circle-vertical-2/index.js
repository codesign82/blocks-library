import './index.html';
import './style.scss';
// import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/wrap';
import Draggable from 'gsap/Draggable';
import InertiaPlugin from 'gsap/InertiaPlugin';

gsap.registerPlugin(Draggable);
gsap.registerPlugin(InertiaPlugin);

const maxRotation = 220;


const updateActiveIndex = (activeIndex, rotation, maxRotation, steps) => {
  const incrementDegrees = maxRotation / steps,
      currentIndex = rotation / incrementDegrees,
      remainingFraction = currentIndex % 1;
  
  if (remainingFraction < .3) {
    activeIndex.image = Math.floor(currentIndex);
    activeIndex.slide = Math.floor(currentIndex);
  }
  else if (remainingFraction > .7) {
    activeIndex.image = Math.ceil(currentIndex);
    activeIndex.slide = Math.ceil(currentIndex);
  }
  else {
    activeIndex.image = -1;
  }
  
};

const updateSlides = (activeIndex, rotation, maxRotation, images, slides, slideNumber, sliderButtons, updateActive = true, prevIndex) => {
  const oldActiveIndex = {...activeIndex};
  updateActive && updateActiveIndex(activeIndex, rotation, maxRotation, images.length - 1);
  
  if (!updateActive) {
    images[prevIndex.image].classList.remove('active');
    images[activeIndex.image].classList.add('active');
    slides[prevIndex.slide].classList.remove('active');
    slides[activeIndex.slide].classList.add('active');
    slideNumber.textContent = String(activeIndex.slide + 1).padStart(2, '0');
    sliderButtons.next.classList.toggle('swiper-button-disabled', activeIndex.slide >= images.length - 1);
    sliderButtons.prev.classList.toggle('swiper-button-disabled', activeIndex.slide <= 0);
    return;
  }
  if (activeIndex.slide === -1) {
    activeIndex.image = 0;
    activeIndex.slide = 0;
  }
  
  if (oldActiveIndex.image !== activeIndex.image) {
    images[oldActiveIndex.image]?.classList.remove('active');
    images[activeIndex.image]?.classList.add('active');
  }
  if (oldActiveIndex.slide !== activeIndex.slide) {
    slides[oldActiveIndex.slide].classList.remove('active');
    slides[activeIndex.slide].classList.add('active');
    slideNumber.textContent = String(activeIndex.slide + 1).padStart(2, '0');
    sliderButtons.next.classList.toggle('swiper-button-disabled', activeIndex.slide >= images.length - 1);
    sliderButtons.prev.classList.toggle('swiper-button-disabled', activeIndex.slide <= 0);
    
    
  }
  
  
};



const blockScript = async (container = document) => {
  const block = container.querySelector('.block-slider-circle-vertical-2');
  
    
    const imagesLoading = [];
    const slideImages = Array.from(block.querySelectorAll('img'));
    for (const slideImage of slideImages) {
      imagesLoading.push(new Promise(resolve => slideImage.complete ? resolve() : (slideImage.onload = resolve)));
    }
    
    Promise.allSettled(imagesLoading).then(() => {
      
      
      const activeIndex = {image: 0, slide: 0},
          imagesWrapper = block.querySelector('.images-wrapper'),
          imageWrapper = imagesWrapper.querySelectorAll('.image-wrapper'),
          images = imagesWrapper.querySelectorAll('picture'),
          spin = gsap.to(imageWrapper, {duration: 1000, rotation: -maxRotation, ease: 'linear', paused: true}),
          snapDegree = maxRotation / (images.length - 1),
          slides = block.querySelectorAll('.slide-text'),
          slideNumber = block.querySelector('.result-focused>.number'),
          sliderButtons = {
            next: block.querySelector('.result-focused .swiper-btns>.swiper-button-next'),
            prev: block.querySelector('.result-focused .swiper-btns>.swiper-button-prev'),
          },
          draggable = Draggable.create(imagesWrapper, {
            type: 'rotation',
            bounds: {minRotation: 0, maxRotation},
            inertia: true,
            snap: function (endValue) {
              return Math.round(endValue / snapDegree) * snapDegree;
            },
            dragResistance: 0.4,
            throwResistance: 20000,
            onDrag() {
              spin.progress(this.rotation / maxRotation);
              updateSlides(activeIndex, this.rotation, maxRotation, images, slides, slideNumber, sliderButtons);
            },
            onThrowUpdate() {
              spin.progress(this.rotation / maxRotation);
              updateSlides(activeIndex, this.rotation, maxRotation, images, slides, slideNumber, sliderButtons);
            },
          });
      
      
      const animateTo = (index) => {
        const prevIndex = {...activeIndex};
        activeIndex.slide = index;
        activeIndex.image = index;
        updateSlides(activeIndex, draggable[0].rotation, maxRotation, images, slides, slideNumber, sliderButtons, false, prevIndex);
        gsap.to(imagesWrapper, {
          rotation: `${maxRotation / (images.length - 1) * index}`, onUpdate() {
            draggable[0].update();
            spin.progress(draggable[0].rotation / maxRotation);
          },
        });
      };
      
      sliderButtons.next.addEventListener('click', () => {
        const nextSlide = activeIndex.slide + 1;
        nextSlide >= slides.length || animateTo(nextSlide);
        
      });
      sliderButtons.prev.addEventListener('click', () => {
        const prevSlide = activeIndex.slide - 1;
        prevSlide < 0 || animateTo(prevSlide);
      });
      
      
      let angleIncrement = -Math.PI * 2 * maxRotation / 360 / (images.length - 1),
          radius = imagesWrapper.offsetWidth / 2,
          angle;
      for (let i = 0; i < images.length; i++) {
        angle = angleIncrement * i - Math.PI;
        const image = images[i];
        const slide = slides[i];
        gsap.set(image.parentElement, {
          xPercent: -50,
          yPercent: -50,
          left: (radius + Math.cos(angle) * radius) / radius * 50 + '%',
          top: (radius + Math.sin(angle) * radius) / radius * 50 + '%',
        });
        i === 0 && image.classList.add('active');
        i === 0 && slide.classList.add('active');
        
        
        image.addEventListener('click', () => animateTo(i));
      }
    })
 


animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);




<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'block-slider-circle-vertical-2 circle-plus-line d-flex flex-md-nowrap flex-wrap';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/block-slider-circle-vertical-2/screenshot1.png" >';
  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$number = get_field('number');
$title = get_field('title');
$description = get_field('description');

?>
<!-- region Kiwi's Block -->
<?php general_settings_for_blocks($id, $className); ?>
<div class="result-focused">
  <?php if ($number) { ?>
    <h1 class="headline-1 size120 number word-up"><?=$number?></h1>
  <?php } ?>
  <div class="testimonials">
    <?php if ($title) { ?>
      <h3 class="headline-3 word-up"><?=$title?></h3>
    <?php } ?>
    <?php if ($description) { ?>
      <p class="paragraph size19 real-line-up"><?=$description?></p>
    <?php } ?>
  </div>
  <div class="result-focused-content d-flex flex-xl-nowrap flex-wrap-reverse flex-md-wrap">
    <div class="swiper-btns iv-st-from-left">
      <div class="swiper-button-prev swiper-button-disabled swiper-button">
        <div class="swiper-btn-bg"></div>
        <div class="swiper-btn-border"></div>
        <svg height="7" viewBox="0 0 13 7" width="13">
          <path d="M6.5 0L13 7H0z"/>
        </svg>
      </div>
      <div class="swiper-button-next swiper-button">
        <div class="swiper-btn-bg"></div>
        <div class="swiper-btn-border"></div>
        <svg height="7" viewBox="0 0 13 7" width="13">
          <path d="M6.5 7L0 0h13z"/>
        </svg>
      </div>
    </div>
    <div class="content">
      <?php
      while (have_rows('slider_content_2')) {
        the_row();
        $text = get_sub_field('text');
        ?>
        <div class="slide-text wysiwyg-block real-line-up">
          <?php if ($text) { ?>
            <?=$text?>
          <?php } ?>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<div class="images-wrapper iv-st-from-right">
  <?php
  while (have_rows('slider_content_2')) {
    the_row();
    $image = get_sub_field('image');
    $icon = get_sub_field('icon');
    ?>
    <div class="image-wrapper">
      <?php if ($image) { ?>
        <picture>
          <img alt="<?=$image['alt']?>" <?=acf_img($image['id'],'500px','medium')?>/>
        </picture>
      <?php } ?>
      <?php if ($icon) { ?>
        <img alt="<?=$icon['alt']?>" class="icon" <?=acf_img($icon['id'],'500px','medium')?>>
      <?php } ?>
    </div>
    <?php
  }
  ?>
  <svg class="circle" height="790" viewBox="0 0 790 790" width="790">
    <circle cx="395" cy="395" fill="none" opacity=".49" r="395" stroke="#707070" stroke-width="2"/>
  </svg>
</div>
</section>

<!-- endregion Kiwi's Block -->

<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'image_fixed_viewport_block';
$className = 'image_fixed_viewport_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/image_fixed_viewport_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$is_reversed = get_sub_field('is_reversed');

//$image = get_field('image');
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<div class="container">
    <div class="content-wrapper <?= $is_reversed ? "reversed" : "" ?>">
        <div class="image-wrapper">
            <?php if (have_rows('slider_tiles')) {
                while (have_rows('slider_tiles')) {
                    the_row();
                    $image = get_sub_field('image');
                    ?>
                    <?php if ($image) { ?>
                        <div class="image-parent">
                            <picture
                                    class="main-img aspect-ratio animated-image  <?= get_row_index() === 1 ? 'active' : '' ?>">
                                <?= get_acf_image($image, 'img-718-718') ?>
                            </picture>
                        </div>
                    <?php } ?>
                <?php }
            } ?>
        </div>

        <div class="text-wrapper">

            <?php if (have_rows('slider_tiles')) {
                while (have_rows('slider_tiles')) {
                    the_row();
                    $image = get_sub_field('image');

                    $title = get_sub_field('title');

                    $description = get_sub_field('description');
                    $cta = get_sub_field('cta');
                    $section_title = get_sub_field('section_title');
                    $stats_or_list = get_sub_field('stats_or_list');
                    $left_list_title = get_sub_field('left_list_title');
                    $right_list_title = get_sub_field('right_list_title');

                    $text_wrapper_class_1 = $stats_or_list === 'states' ? 'has-stats' : '';
                    $text_wrapper_class_2 = $stats_or_list === 'list' ? 'has-list' : '';
                    ?>
                    <?php if ($image) { ?>
                        <div class="at-small image-parent">
                            <picture
                                    class="main-img aspect-ratio <?= get_row_index() === 1 ? 'active' : '' ?>">
                                <img src="<?= $image["url"] ?>" alt="<?= $image["alt"] ?>">
                            </picture>
                        </div>
                    <?php } ?>

                    <div
                            class="text-content  <?= get_row_index() === 1 ? 'active' : '' ?> <?= $text_wrapper_class_1 ?> <?= $text_wrapper_class_2 ?>">
                        <?php if ($title) { ?>
                            <h2 move-to-here class="main-title iv-st-from-bottom headline-3"><?= $title ?></h2>
                        <?php } ?>
                        <?php if ($section_title) { ?>
                            <h3 class="section-title headline-4 iv-st-from-bottom"><?= $section_title ?></h3>
                        <?php } ?>

                        <div
                                class="section-desc paragraph iv-st-from-bottom"><?= $description ?></div>
                        <?php if ($stats_or_list === 'states') { ?>
                            <div class="stats-wrapper iv-st-from-bottom">
                                <?php if (have_rows('stats')) {
                                    while (have_rows('stats')) {
                                        the_row();
                                        $number = get_sub_field('number');
                                        $statistic = get_sub_field('statistic');
                                        ?>
                                        <?php if ($number || $statistic) { ?>
                                            <div class="state">
                      <span
                              class="number"><?= $number ?>% </span><?= $statistic ?>
                                            </div>
                                        <?php }
                                    }
                                } ?>
                            </div>
                        <?php } elseif (($stats_or_list === 'list')) { ?>
                            <div class="list-wrapper iv-st-from-bottom">
                                <ul class="list left-list">
                                    <li
                                            class="list-title headline-5"><?= $left_list_title ?></li>
                                    <?php if (have_rows('left_list_items')) {
                                        while (have_rows('left_list_items')) {
                                            the_row();
                                            $list_item = get_sub_field('list_item');
                                            ?>
                                            <li
                                                    class="list-item paragraph"><?= $list_item ?></li>
                                        <?php }
                                    } ?>
                                </ul>
                                <ul class="list right-list">
                                    <li
                                            class="list-title headline-5"> <?= $right_list_title ?></li>
                                    <?php if (have_rows('right_list_items')) {
                                        while (have_rows('right_list_items')) {
                                            the_row();
                                            $list_item = get_sub_field('list_item');

                                            ?>
                                            <li
                                                    class="list-item paragraph"><?= $list_item ?></li>
                                        <?php }
                                    } ?>
                                </ul>
                            </div>
                        <?php } ?>
                        <?php if ($cta) { ?>
                            <div class="iv-st-from-bottom">
                                <a class="btn"
                                   href="<?= $cta['url'] ?>"><?= $cta['title'] ?></a>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <?php if (have_rows('slider_tiles')) { ?>
            <ul class="steps-dots-wrapper step-bullets">
                <?php while (have_rows('slider_tiles')) {
                    the_row();
                    ?>
                    <li tabindex="0"
                        class="step-dot dot-link <?= get_row_index() === 1 ? 'active' : '' ?> "
                        aria-label="swipe to anther slide">
                    </li>
                <?php } ?>
            </ul>
        <?php } ?>
    </div>
</div>

</section>


<!-- endregion headversity's Block -->

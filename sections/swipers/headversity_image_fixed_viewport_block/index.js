import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/propellerhealth';


import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger)
import 'swiper/css'

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.image_fixed_viewport_block');


  const images = block.querySelectorAll(".animated-image img")
  const dotsWrapper = block.querySelector(".steps-dots-wrapper")
  const steps = block.querySelectorAll('.animated-image');
  const textBlocks = block.querySelectorAll('.text-content');
  const stepHeight = 500;
  const height = steps.length * stepHeight;
  const dots = block.querySelectorAll('.step-dot');
  const stepsWrapper = block.querySelector('.content-wrapper');
  let currentStep = 0;


  ScrollTrigger.matchMedia({
    '(min-width:600px)': () => {
      if (textBlocks.length > 1) {
        const scrollTrigger = ScrollTrigger.create({
          trigger: stepsWrapper,
          start: `center center`,
          end: '+=' + height,
          pin: true,
          pinSpacing: true,
          scrub: true,
          onUpdate(self) {
            if (self.progress === 1) return
            if (currentStep !== ~~(self.progress * steps.length))
              dots[currentStep].classList.remove('active');
            steps[currentStep].classList.remove('active');
            textBlocks[currentStep].classList.remove('active');
            currentStep = ~~(self.progress * steps.length);
            dots[currentStep].classList.add('active');
            textBlocks[currentStep].classList.add('active');
            steps[currentStep].classList.add('active');

          }
        })
        for (let i = 0; i < dots.length; i++) {
          const dot = dots[i];
          dot.addEventListener('click', () => {
            window.scrollTo(0, scrollTrigger.start + i * stepHeight * 1.05)
          })
        }
      } else {
        images.forEach(image => {

          gsap.to(image, {
            scale: 1.1,
            scrollTrigger: {
              trigger: image,
              start: "center 80%",
              end: "center 20%",
              ease: "power3.inOut",
              toggleActions: "play reverse play reverse"
            }
          })
        })
        dotsWrapper.style.display = "none"
      }

      return () => {
        for (let dot of dots) {
          dot.classList.remove('active');
        }
        for (let step of steps) {
          step.classList.remove('active');
        }
      }
    },
  })


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


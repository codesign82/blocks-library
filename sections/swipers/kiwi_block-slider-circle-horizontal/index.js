import './index.html';
import './style.scss';
// import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/wrap';
import Swiper, {Controller, EffectFade, Navigation} from 'swiper';
// import 'swiper/components/effect-fade/effect-fade.scss';
// import 'swiper/modules/effect-fade/effect-fade.scss';



const blockScript = async (container = document) => {
  const blocks = container.querySelectorAll('.block-slider-circle-horizontal');
  Swiper.use([Navigation, EffectFade, Controller]);
  for (let block of blocks) {
    
    const imagesLoading = [];
    const slideImages = Array.from(block.querySelectorAll('.swiper-slide img'));
    for (const slideImage of slideImages) {
      imagesLoading.push(new Promise(resolve => slideImage.complete ? resolve() : (slideImage.onload = resolve)));
    }
    const swiperTop = new Swiper(block.querySelector('.slide-top-part .swiper-container'), {
      slidesPerView: 1,
      spaceBetween: 30,
      effect: 'fade',
      fadeEffect: {
        crossFade: true,
      },
      simulateTouch: false,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
    const swiperBottom = new Swiper(block.querySelector('.slide-bottom-part .swiper-container'), {
      slidesPerView: 1,
      spaceBetween: 30,
      effect: 'fade',
      fadeEffect: {
        crossFade: true,
      },
      simulateTouch: false,
    });
    
    swiperTop.controller.control = swiperBottom;
    Promise.allSettled(imagesLoading).then(() => {
      console.log('sliderCircleHorizontal');
      swiperTop.update();
      swiperBottom.update();
      
    })
    
    
  }
  
  
}
windowOnLoad(blockScript);




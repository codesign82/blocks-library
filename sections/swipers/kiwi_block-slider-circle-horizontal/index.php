<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'block-slider-circle-horizontal';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/block-slider-circle-horizontal/screenshot1.png" >';
  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$iframe = get_template_directory_uri() . '/assets/images/main-laptop-iframe.png';

?>
<!-- region Kiwi's Block -->
<?php general_settings_for_blocks($id, $className); ?>
<div class="container">
  <div class="slide-circle-wrapper">
    <div class="slide-top-part">
      <hr class="laptop-hr">
      <div class="swiper-btns iv-st-from-bottom">
        <div class="swiper-button-next swiper-button">
          <div class="swiper-btn-bg"></div>
          <div class="swiper-btn-border"></div>
          <svg height="7" viewBox="0 0 13 7" width="13">
            <path d="M6.5 7L0 0h13z"/>
          </svg>
        </div>
        <div class="swiper-button-prev swiper-button">
          <div class="swiper-btn-bg"></div>
          <div class="swiper-btn-border"></div>
          <svg height="7" viewBox="0 0 13 7" width="13">
            <path d="M6.5 0L13 7H0z"/>
          </svg>
        </div>
      </div>
      <div class="slides-wrapper iv-st-from-bottom">
        <svg class="half-circle" height="43.4rem" viewBox="0 0 1018 434" width="1018" xmlns="http://www.w3.org/2000/svg">
          <defs>
            <clipPath id="y41wa">
              <path
                d="M1017.667 434H.333a514.496 514.496 0 0 1 9.296-45.403 513.238 513.238 0 0 1 13.184-43.855 513.834 513.834 0 0 1 16.883-42.118 515.578 515.578 0 0 1 20.392-40.19 517.538 517.538 0 0 1 23.708-38.07 519.256 519.256 0 0 1 26.835-35.76 520.325 520.325 0 0 1 29.772-33.26 520.345 520.345 0 0 1 32.516-30.568 519.508 519.508 0 0 1 35.071-27.687 517.87 517.87 0 0 1 37.435-24.614 515.983 515.983 0 0 1 39.608-21.35 514.013 514.013 0 0 1 41.59-17.897 513.157 513.157 0 0 1 43.384-14.251A513.808 513.808 0 0 1 414.99 8.561a516.949 516.949 0 0 1 46.395-6.389A523.639 523.639 0 0 1 509 0c15.875 0 31.895.73 47.615 2.172a516.994 516.994 0 0 1 46.395 6.39 513.846 513.846 0 0 1 44.984 10.415 513.126 513.126 0 0 1 43.382 14.251 514.033 514.033 0 0 1 41.591 17.897 515.89 515.89 0 0 1 39.608 21.35 517.863 517.863 0 0 1 37.435 24.614 519.55 519.55 0 0 1 35.071 27.687 520.43 520.43 0 0 1 32.517 30.568 520.252 520.252 0 0 1 29.77 33.26 519.262 519.262 0 0 1 26.836 35.76 517.46 517.46 0 0 1 23.708 38.07 515.392 515.392 0 0 1 20.391 40.19 513.825 513.825 0 0 1 16.884 42.118 513.185 513.185 0 0 1 13.184 43.855 514.472 514.472 0 0 1 9.296 45.403z"
                fill="#fff"/>
            </clipPath>
          </defs>
          <path clip-path="url(&quot;#y41wa&quot;)"
                d="M1017.667 434v0H.333a514.496 514.496 0 0 1 9.296-45.403 513.238 513.238 0 0 1 13.184-43.855 513.834 513.834 0 0 1 16.883-42.118 515.578 515.578 0 0 1 20.392-40.19 517.538 517.538 0 0 1 23.708-38.07 519.256 519.256 0 0 1 26.835-35.76 520.325 520.325 0 0 1 29.772-33.26 520.345 520.345 0 0 1 32.516-30.568 519.508 519.508 0 0 1 35.071-27.687 517.87 517.87 0 0 1 37.435-24.614 515.983 515.983 0 0 1 39.608-21.35 514.013 514.013 0 0 1 41.59-17.897 513.157 513.157 0 0 1 43.384-14.251A513.808 513.808 0 0 1 414.99 8.561a516.949 516.949 0 0 1 46.395-6.389A523.639 523.639 0 0 1 509 0c15.875 0 31.895.73 47.615 2.172a516.994 516.994 0 0 1 46.395 6.39 513.846 513.846 0 0 1 44.984 10.415 513.126 513.126 0 0 1 43.382 14.251 514.033 514.033 0 0 1 41.591 17.897 515.89 515.89 0 0 1 39.608 21.35 517.863 517.863 0 0 1 37.435 24.614 519.55 519.55 0 0 1 35.071 27.687 520.43 520.43 0 0 1 32.517 30.568 520.252 520.252 0 0 1 29.77 33.26 519.262 519.262 0 0 1 26.836 35.76 517.46 517.46 0 0 1 23.708 38.07 515.392 515.392 0 0 1 20.391 40.19 513.825 513.825 0 0 1 16.884 42.118 513.185 513.185 0 0 1 13.184 43.855 514.472 514.472 0 0 1 9.296 45.403z"
                fill="none" stroke="#707070"
                stroke-miterlimit="20"
                stroke-width="2"/>
          <path d="M3 389.5a6.5 6.5 0 1 1 13 0 6.5 6.5 0 0 1-13 0z" fill="#969696"/>
          <path d="M942.484 226.086a6.5 6.5 0 1 1-9.193 9.193 6.5 6.5 0 0 1 9.193-9.193z" fill="#969696"/>
          <path d="M32 311.5a6.5 6.5 0 1 1 13 0 6.5 6.5 0 0 1-13 0z" fill="#969696"/>
          <path d="M985.132 301.747a6.5 6.5 0 1 1-9.192 9.192 6.5 6.5 0 0 1 9.192-9.192z" fill="#969696"/>
          <path d="M74 230.5a6.5 6.5 0 1 1 13 0 6.5 6.5 0 0 1-13 0z" fill="#969696"/>
          <path d="M1014.708 388.721a6.5 6.5 0 1 1-9.192 9.193 6.5 6.5 0 0 1 9.192-9.193z" fill="#969696"/>
        </svg>
        <div class="swiper-images">
          <img alt="laptop-iframe" class="iframe" src="<?=$iframe?>">
          <div class="swiper-container">
            <div class="swiper-wrapper">
              <?php
              while (have_rows('portfolio_images')) {
                the_row();
                $image = get_sub_field('image');
                ?>
                <div class="swiper-slide">
                  <div class="site-image">
                    <?php if ($image) { ?>
                      <img alt="<?=$image['alt']?>" src="<?=$image['url']?>">
                    <?php } ?>
                  </div>
                </div>
                <?php
              }
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="slide-bottom-part iv-st-from-bottom">
      <div class="swiper-container">
        <div class="swiper-wrapper">
          <?php
          while (have_rows('portfolio_images')) {
            the_row();
            $site_name = get_sub_field('site_name');
            $site_logo = get_sub_field('site_logo');
            $site_link = get_sub_field('site_link');
            ?>
            <div class="swiper-slide">
              <div class="site-details d-flex flex-wrap justify-content-center justify-content-md-between align-items-center">
                <?php if ($site_name) { ?>
                  <h4 class="headline-4"><?=$site_name?></h4>
                <?php } ?>
                <?php if ($site_logo) { ?>
                  <div class="site-image">
                    <img alt="<?=$site_logo['alt']?>" src="<?=$site_logo['url']?>">
                  </div>
                <?php } ?>
              </div>
              <?php if ($site_link) { ?>
                <a class="btn view-site" href="<?=$site_link['url']?>" target="<?=$site_link['target']?>">
                  <div class="btn-border"></div>
                  <div class="btn-bg"></div>
                  <p class="btn-text"><?=$site_link['title']?></p>
                </a>
              <?php } ?>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
</section>

<!-- endregion Kiwi's Block -->

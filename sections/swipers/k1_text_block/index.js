import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/propellerhealth';
import Swiper, {Autoplay, Pagination} from 'swiper'


const blockScript = async (container = document) => {
  const block = container.querySelector('.text_block');

  const slides = block.querySelectorAll('.swiper-slide');

  const swiperChecker = slides.length === 1 ? {loop: false, allowTouchMove: false} : {
    loop: true, pagination: {
      el: block.querySelector('.swiper-pagination'),
      clickable: true
    },
  };

  let swiper = new Swiper(block.querySelector(".swiper"), {
    modules: [Autoplay, Pagination],
    slidesPerView: 1,
    ...swiperChecker,
    autoplay: {
      delay: 6000,
      pauseOnMouseEnter: false,
      stopOnLastSlide: false,
      waitForTransition: true,
    },

  });


  animations(block);
  imageLazyLoading(block);
  
};
windowOnLoad(blockScript);



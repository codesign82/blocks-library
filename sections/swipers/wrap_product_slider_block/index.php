<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'product_slider_block';
$className = 'product_slider_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/product_slider_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_title          = get_field( 'main_title' );
$title               = get_field( 'title' );
$product_name        = get_field( 'product_name' );
$description         = get_field( 'description' );
$product_specs_title = get_field( 'product_specs_title' );
$left_cta_button     = get_field( 'left_cta_button' );
$right_cta_button    = get_field( 'right_cta_button' );
$product_gallery     = get_field( 'product_gallery' );
$has_tabs            = get_field( 'has_tabs' );
?>
<!-- region WRAP's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <div class="product iv-st-from-bottom">
    <div class="swiper-content">
      <?php if ( ! $has_tabs ) { ?>
        <div class="swiper-container gallery-top ">
          <div class="swiper-wrapper">
            <?php foreach ( $product_gallery as $image ): ?>
              <div class="swiper-slide">
                <div class="swiper-slide-container">
                  <div class="image-wrap" id="jsv-holder-2">
                    <picture>
                      <img id="jsv-image-2" alt="<?= esc_attr( $image['alt'] ?: 'Product Image' ) ?>" src="<?= esc_url( $image['url'] ) ?>">
                    </picture>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
            <!-- Add Arrows -->
          </div>
        </div>
        <div class="thumbnail-wrapper">
          <div class="swiper-button-prev swiper-button">
            <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M2.828 6.99999L7.778 11.95L6.364 13.364L0 6.99999L6.364 0.635986L7.778 2.04999L2.828 6.99999Z" fill="#1C3A58"/>
            </svg>
          </div>
          <div class="swiper-container gallery-thumbs main-gallery-thumbs">
            <div class="swiper-wrapper">
              <?php foreach ( $product_gallery as $image ): ?>
                <div class="swiper-slide">
                  <div class="swiper-slide-container">
                    <div class="image-wrap" id="jsv-holder-2">
                      <picture>
                        <img id="jsv-image-2" alt="<?= esc_attr( $image['alt'] ?: 'Product Image' ) ?>" src="<?= esc_url( $image['url'] ) ?>">
                      </picture>
                    </div>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
          <div class="swiper-button-next swiper-button">
            <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M5.172 7.00001L0.222001 2.05001L1.636 0.636012L8 7.00001L1.636 13.364L0.222 11.95L5.172 7.00001Z" fill="#1C3A58"/>
            </svg>
          </div>
        </div>
      <?php } else { ?>
        <?php
        $active_tab            = get_field( 'tabs' )[0];
        $active_product_images = $active_tab['product_images'];
        ?>
        <div class="swiper-container gallery-top main-gallery-top">
          <div class="swiper-wrapper">
            <?php
            foreach ( $active_product_images as $product_image ) {
              ?>
              <div class="swiper-slide">
                <div class="swiper-slide-container">
                  <div class="image-wrap" id="jsv-holder-2">
                    <picture>
                      <img id="jsv-image-2" alt="<?= @$product_image['alt'] ?: 'Product Image' ?>" src="<?= $product_image['url'] ?>">
                    </picture>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </div>
        <div class="thumbnail-wrapper">
          <div class="swiper-button-prev swiper-button">
            <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M2.828 6.99999L7.778 11.95L6.364 13.364L0 6.99999L6.364 0.635986L7.778 2.04999L2.828 6.99999Z" fill="#1C3A58"/>
            </svg>
          </div>
          <div class="swiper-container gallery-thumbs main-gallery-thumbs">
            <div class="swiper-wrapper">
              <?php
              foreach ( $active_product_images as $product_image ) { ?>
                <div class="swiper-slide">
                  <div class="swiper-slide-container">
                    <div class="image-wrap" id="jsv-holder-2">
                      <picture>
                        <img id="jsv-image-2" alt="<?= @$product_image['alt'] ?: 'Product Image' ?>" src="<?= $product_image['url'] ?>">
                      </picture>
                    </div>
                  </div>
                </div>
              <?php } ?>
            </div>
          </div>
          <div class="swiper-button-next swiper-button">
            <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M5.172 7.00001L0.222001 2.05001L1.636 0.636012L8 7.00001L1.636 13.364L0.222 11.95L5.172 7.00001Z" fill="#1C3A58"/>
            </svg>
          </div>
        </div>
      <?php } ?>
    </div>
    <div class="text-wrapper <?= $has_tabs ? 'text-wrapper-has-tabs' : '' ?>">
      <?php if ( $title ) { ?>
        <h6 class="headline-6 product-title"><?= $title ?></h6>
      <?php } ?>
      <?php if ( $product_name ) { ?>
        <h2 class="headline-2 title"><?= $product_name ?></h2>
      <?php } ?>
      <?php if ( ! $has_tabs ) { ?>
        <?php if ( $description ) { ?>
          <div class="paragraph paragraph-16"><?= $description ?></div>
        <?php } ?>
        <?php if ( have_rows( 'product_specs' ) ) { ?>
          <div class="product-specs">
            <div class="product-specs-title">
              <h6 class="headline-6"><?= $product_specs_title ?></h6>
              <svg class="product-specs-title-arrow" width="14" height="8" viewBox="0 0 14 8" fill="none">
                <path d="M7.00023 5.172L11.9502 0.222L13.3642 1.636L7.00023 8L0.63623 1.636L2.05023 0.222L7.00023 5.172Z" fill="currentColor"/>
              </svg>
            </div>
            <ul class="product-specs-info">
              <?php while ( have_rows( 'product_specs' ) ) {
                the_row();
                $product_spec_text = get_sub_field( 'product_spec_text' ); ?>
                <li class="product-specs-info-text"><?= $product_spec_text ?></li>
              <?php } ?>
              <div class="product-specs-info-spacer"></div>
            </ul>
          </div>
        <?php } ?>
        <div class="button-wrapper">
          <?php if ( $left_cta_button ) { ?>
            <a href="<?= $left_cta_button['url'] ?>" target="<?= $left_cta_button['target'] ?>" class="btn"><?= $left_cta_button['title'] ?></a>
          <?php } ?>
          <?php if ( $right_cta_button ) { ?>
            <a href="<?= $right_cta_button['url'] ?>" target="<?= $right_cta_button['target'] ?>" class="btn btn-border"><?= $right_cta_button['title'] ?></a>
          <?php } ?>
        </div>
      <?php } else { ?>
        <?php if ( have_rows( 'tabs' ) ) { ?>
          <div class="tabs-wrapper">
            <div class="tabs-buttons">
              <?php while ( have_rows( 'tabs' ) ) {
                the_row();
                $tab_button = get_sub_field( 'tab_button' );
                ?>
                <button class="tab-button btn btn-border <?= get_row_index() === 1 ? 'active' : '' ?>" aria-label="Tab button <?= $tab_button ?>"><?= $tab_button ?></button>
              <?php } ?>
            </div>
            <div class="tab-content-wrapper">
              <?php while ( have_rows( 'tabs' ) ) {
                the_row();
                $description         = get_sub_field( 'description' );
                $product_specs_title = get_sub_field( 'product_specs_title' );
                $left_cta_button     = get_sub_field( 'left_cta_button' );
                $right_cta_button    = get_sub_field( 'right_cta_button' );
                $product_images      = get_sub_field( 'product_images' ) ?>
                <div class="tab-content <?= get_row_index() === 1 ? 'active' : '' ?>">
                  <?php if ( $description ) { ?>
                    <div class="paragraph paragraph-16"><?= $description ?></div>
                  <?php } ?>
                  <?php if ( have_rows( 'tab_product_specs' ) ) { ?>
                    <div class="product-specs">
                      <div class="product-specs-title">
                        <h6 class="headline-6"><?= $product_specs_title ?></h6>
                        <svg class="product-specs-title-arrow" width="14" height="8" viewBox="0 0 14 8" fill="none">
                          <path d="M7.00023 5.172L11.9502 0.222L13.3642 1.636L7.00023 8L0.63623 1.636L2.05023 0.222L7.00023 5.172Z" fill="currentColor"/>
                        </svg>
                      </div>
                      <ul class="product-specs-info">
                        <?php while ( have_rows( 'tab_product_specs' ) ) {
                          the_row();
                          $product_spec_text = get_sub_field( 'product_spec_text' ); ?>
                          <li class="product-specs-info-text"><?= $product_spec_text ?></li>
                        <?php } ?>
                        <div class="product-specs-info-spacer"></div>
                      </ul>
                    </div>
                  <?php } ?>
                  <div class="button-wrapper">
                    <?php if ( $left_cta_button ) { ?>
                      <a href="<?= $left_cta_button['url'] ?>" target="<?= $left_cta_button['target'] ?>" class="btn"><?= $left_cta_button['title'] ?></a>
                    <?php } ?>
                    <?php if ( $right_cta_button ) { ?>
                      <a href="<?= $right_cta_button['url'] ?>" target="<?= $right_cta_button['target'] ?>" class="btn btn-border"><?= $right_cta_button['title'] ?></a>
                    <?php } ?>
                  </div>
                  <template class="tab-gallery-top">
                    <?php foreach ( $product_images as $image ): ?>
                      <div class="swiper-slide">
                        <div class="swiper-slide-container">
                          <div class="image-wrap" id="jsv-holder-2">
                            <picture>
                              <img id="jsv-image-2" alt="<?= esc_attr( $image['alt'] ) ?: 'Product Image' ?>" src="<?= esc_url( $image['url'] ) ?>">
                            </picture>
                          </div>
                        </div>
                      </div>
                    <?php endforeach; ?>
                    <!-- Add Arrows -->
                  </template>
                  <template class="tab-gallery-thumbs">
                    <?php foreach ( $product_images as $image ): ?>
                      <div class="swiper-slide">
                        <div class="swiper-slide-container">
                          <div class="image-wrap" id="jsv-holder-2">
                            <picture>
                              <img id="jsv-image-2" alt="<?= esc_attr( $image['alt'] ) ?: 'Product Image' ?>" src="<?= esc_url( $image['url'] ) ?>">
                            </picture>
                          </div>
                        </div>
                      </div>
                    <?php endforeach; ?>
                  </template>
                </div>
              <?php } ?>
            </div>
          </div>
        <?php } ?>
      <?php } ?>
    </div>
  </div>
</div>
</section>


<!-- endregion WRAP's Block -->

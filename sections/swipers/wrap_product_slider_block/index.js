import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import Swiper, {Controller, Navigation} from "swiper";
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/wrap';

Swiper.use([Controller, Navigation]);

const blockScript = async (container = document) => {
  const block = container.querySelector('.product_slider_block');
  
  // add block code here
  let galleryTopOptions = {
    spaceBetween: 10,
    slidesPerView: 1,
    loop: true,
    loopedSlides: 6,
    slideToClickedSlide: true,
    navigation: {
      nextEl: block.querySelector('.swiper-button-next'),
      prevEl: block.querySelector('.swiper-button-prev'),
    },
  };
  let galleryThumbsOptions = {
    spaceBetween: 10,
    centeredSlides: true,
    slidesPerView: 3,
    loop: true,
    loopedSlides: 6,
    slideToClickedSlide: true,
    breakpoints: {
      600: {
        slidesPerView: 5,
        spaceBetween: 10,
      },
    },
  }
  let galleryTop = new Swiper(block.querySelector('.gallery-top'), galleryTopOptions);
  let galleryThumbs = new Swiper(block.querySelector('.gallery-thumbs'), galleryThumbsOptions);
  galleryTop.controller.control = galleryThumbs;
  galleryThumbs.controller.control = galleryTop;
  let mainGalleryTop = block.querySelector('.main-gallery-top');
  let mainGalleryThumbs = block.querySelector('.main-gallery-thumbs');
  
  // region open product specs
  let productSpecsTitles = block.querySelectorAll('.product-specs-title');
  for (let productSpecsTitle of productSpecsTitles) {
    let productSpecsInfo = productSpecsTitle.nextElementSibling;
    productSpecsTitle?.addEventListener('click', (e) => {
      if (!productSpecsInfo) {
        return;
      }
      const isOpened = productSpecsTitle?.classList.toggle('active');
      if (!isOpened) {
        gsap.to(productSpecsInfo, {height: 0});
      } else {
        gsap.to(productSpecsInfo, {height: 'auto'});
      }
    });
  }
  // endregion open product specs
  
  // region product tabs
  let tabsWrapper = block.querySelector('.tabs-wrapper');
  if (tabsWrapper) {
    let tab = tabsWrapper.querySelectorAll('.tab-button');
    let tabContent = tabsWrapper.querySelectorAll('.tab-content');
    
    hideTabsContent(1);
    
    // reinitializeSwiper(tabContent[0]);
    
    tabsWrapper.addEventListener('click', function (event) {
      let target = event.target;
      if (target.classList?.contains('tab-button')) {
        for (let i = 0; i < tab.length; i++) {
          if (target === tab[i]) {
            showTabsContent(i);
            break;
          }
        }
      }
    })
    
    function hideTabsContent(a) {
      for (let i = a; i < tabContent.length; i++) {
        tabContent[i].classList?.remove('active');
        tabContent[i].classList?.add("hide");
        tab[i].classList?.remove('active');
      }
    }
    
    function showTabsContent(b) {
      if (tabContent[b].classList.contains('hide')) {
        hideTabsContent(0);
        tab[b].classList?.add('active');
        tabContent[b].classList?.remove('hide');
        tabContent[b].classList?.add('active');
        let tabGalleryTop = tabContent[b].querySelector('.tab-gallery-top');
        let tabGalleryThumbs = tabContent[b].querySelector('.tab-gallery-thumbs');
        galleryTop.destroy(true, true);
        galleryThumbs.destroy(true, true);
        mainGalleryTop.children[0].innerHTML = tabGalleryTop.innerHTML;
        mainGalleryThumbs.children[0].innerHTML = tabGalleryThumbs.innerHTML;
        galleryTop = new Swiper(block.querySelector('.gallery-top'), galleryTopOptions);
        galleryThumbs = new Swiper(block.querySelector('.gallery-thumbs'), galleryThumbsOptions);
        galleryTop.controller.control = galleryThumbs;
        galleryThumbs.controller.control = galleryTop;
        gsap.fromTo([tabContent[b], block.querySelector('.swiper-content')], {
          opacity: 0,
        }, {
          duration: 0.6,
          ease: 'none',
          opacity: 1
        })
      }
    }
  }
  
  // endregion product tabs
 
  
  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);




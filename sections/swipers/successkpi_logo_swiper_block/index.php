<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'logo_swiper';
$className = 'logo_swiper';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/logo_swiper/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');

?>
<!-- region successkpi's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <?php if ($title) { ?>
        <h2 class="title"><?= $title ?></h2>
    <?php } ?>

    <?php if (have_rows('logo_cards')) { ?>
        <div class="autoplay-swiper-cont">
            <div class="autoplay-swiper">
                <div class="autoplay-swiper-wrapper icons">
                    <?php while (have_rows('logo_cards')) {
                        the_row();
                        $logo = get_sub_field('logo');
                        ?>
                        <div class="autoplay-swiper-slide">
                            <?php if ($logo) { ?>
                                <picture class="img-wrapper amazon-picture">
                                    <img class="image" src="<?= $logo['url'] ?>"
                                         alt="<?= $logo['alt'] ?>">
                                </picture>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
</section>


<!-- endregion successkpi's Block -->

import './index.html';
import './style.scss';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {autoplaySwiper} from "../../../scripts/general/autoplay-swiper";
import '../../../scripts/sitesSizer/propellerhealth';

const blockScript = async (container = document) => {
  const block = container.querySelector('.logo_swiper');

    autoplaySwiper(block, {
      slidesPerView: 2,
      spaceBetween: 20,
      speed: 20000,
      breakpoints: {
        600: {
          slidesPerView: 3,
          spaceBetween: 30,
        },
        992: {
          slidesPerView: 5,
          spaceBetween: 60
        }
      }
    });

  
  animations(block);
  imageLazyLoading(block);
  
};
windowOnLoad(blockScript);



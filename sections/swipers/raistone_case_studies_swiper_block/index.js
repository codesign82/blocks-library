import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/raistone';

import Swiper, {Pagination, Navigation, EffectFade} from "swiper";

Swiper.use([Pagination, Navigation, EffectFade]);

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.case_studies_swiper_block');


  // add block code here
  let swiper = new Swiper(block.querySelector('.swiper-container'), {
    pagination: {
      el: block.querySelector('.swiper-pagination'),
      clickable: true,
    },

    slidesPerView: '1',
    spaceBetween: 25,
    allowTouchMove: true,
    observer: true,
    observeParents: true,
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
  });

  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


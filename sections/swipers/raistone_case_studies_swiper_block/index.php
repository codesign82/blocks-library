<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'case_studies_swiper_block';
$className = 'case_studies_swiper_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/case_studies_swiper_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$link = get_field('link');
$case_studies = get_field('case_studies');

?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="case-studies-content">
        <div class="title-and-link">
            <?php if ($title) { ?>
                <h2 class="headline-1 title iv-st-from-bottom"><?= $title ?></h2>
            <?php } ?>
            <?php if ($link) { ?>
                <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>"
                   class="btn-arrow desk-only iv-st-from-bottom"><?= $link['title'] ?>
                    <svg width="9" height="14" viewBox="0 0 9 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.03906 13L7.03906 7L1.03906 1" stroke="#0A0A0A" stroke-width="2"
                              stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </a>
            <?php } ?>
        </div>
        <?php
        if ($case_studies): ?>
            <div class="swiper-container iv-st-from-bottom">
                <div class="swiper-wrapper">
                    <?php foreach ($case_studies as $case_studie):
                        ?>
                        <div class="swiper-slide">
                            <?php get_template_part('template-parts/case-study', '', array('post_id' => $case_studie)); ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
        <!-- Add Pagination -->
        <div class="swiper-pagination circle"></div>
    </div>
    <a href="<?= site_url() . '/case_studies' ?>" class="btn-arrow mobile-only ">View all case studies
        <svg width="9" height="14" viewBox="0 0 9 14" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M1.03906 13L7.03906 7L1.03906 1" stroke="#0A0A0A" stroke-width="2" stroke-linecap="round"
                  stroke-linejoin="round"/>
        </svg>
    </a>
</div>
</section>


<!-- endregion raistone's Block -->

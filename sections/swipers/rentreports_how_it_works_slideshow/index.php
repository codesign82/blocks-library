<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'how_it_works_slideshow_block';
$className = 'how_it_works_slideshow_block editable-version';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/how_it_works_slideshow_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$link = get_field('link');
?>
<!-- region RentReporters's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="how-it-works-wrapper box-shadow">
    <div class="how-it-works-title">
      <?php if ($title) { ?>
        <h2 class="headline-1 iv-st-from-bottom-f"><?= $title ?></h2>
      <?php } ?>
      <?php if ($link) { ?>
        <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>" class="link has-arrow iv-st-from-bottom-f">
          <?= $link['title'] ?>
          <svg class="arrow" width="21" height="14" viewBox="0 0 21 14" fill="none">
            <path d="M13.8281 0.4375L12.8906 1.375C12.7031 1.60938 12.7031 1.9375 12.9375 2.17188L16.6875 5.78125H0.5625C0.234375 5.78125 0 6.0625 0 6.34375V7.65625C0 7.98438 0.234375 8.21875 0.5625 8.21875H16.6875L12.9375 11.875C12.7031 12.1094 12.7031 12.4375 12.8906 12.6719L13.8281 13.6094C14.0625 13.7969 14.3906 13.7969 14.625 13.6094L20.8125 7.42188C21 7.1875 21 6.85938 20.8125 6.625L14.625 0.4375C14.3906 0.25 14.0625 0.25 13.8281 0.4375Z" fill="currentColor"/>
          </svg>
        </a>
      <?php } ?>
    </div>
    <div class="steps-wrapper">
      <?php
      if (have_rows('steps')) {
        while (have_rows('steps')) {
          the_row();
          $step_number = get_sub_field('step_number');
          $step_title = get_sub_field('step_title');
          $is_has_logos = get_sub_field('is_has_logos');
          $step_description = get_sub_field('step_description');
          $step_illustration = get_sub_field('step_illustration');
          ?>
          <div class="step step-<?= get_row_index(); ?> <?= get_row_index() === 1 ? 'active' : '' ?>">
            <div class="step-content">
              <?php if ($step_number) { ?>
                <h3 class="headline-3 iv-st-from-bottom-f"><?= $step_number ?></h3>
              <?php } ?>
              <?php if ($step_title) { ?>
                <h3 class="headline-2 iv-st-from-bottom-f"><?= $step_title ?></h3>
              <?php } ?>
              <?php if ($is_has_logos) { ?>
                <div class="logos-wrapper">
                  <?php
                  if (have_rows('step_logos')) {
                    while (have_rows('step_logos')) {
                      the_row();
                      $logo = get_sub_field('logo');
                      $logo_link = get_sub_field('logo_link');
                      ?>
                      <?php if ($logo) { ?>
                        <a href="<?= $logo_link['url'] ?>" class="logo iv-st-from-bottom-f">
                          <img data-src="<?= $logo['url'] ?>" alt="<?= $logo['alt'] ?>">
                        </a>
                      <?php } ?>
                      <?php
                    }
                  }
                  ?>
                </div>
              <?php } ?>
              <?php if ($step_description) { ?>
                <div class="paragraph size16 step-desc iv-st-from-bottom-f"><?= $step_description ?></div>
              <?php } ?>
            </div>
            <?php if ($step_illustration) { ?>
              <picture class="step-illustration iv-st-from-right-f">
                <img data-src="<?= $step_illustration['url'] ?>" alt="<?= $step_illustration['alt'] ?>">
              </picture>
            <?php } ?>
          </div>
          <?php
        }
      }
      ?>
    </div>
    <div class="step-bullets iv-st-from-right-f"></div>
  </div>
</div>
</section>


<!-- endregion RentReporters's Block -->

import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import ScrollTrigger from 'gsap/ScrollTrigger'
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/wrap'

const blockScript = async (container = document) => {
  const blocks = container.querySelectorAll('.how_it_works_slideshow_block');
  gsap.registerPlugin(ScrollTrigger);
  for (let block of blocks) {
    const steps = block.querySelectorAll('.step');
    const dotsContainer = block.querySelector('.step-bullets');
    const dotsText = [];
    for (let i = 0; i < steps.length; i++) {
      console.log(steps[i])
      dotsText.push(`<span class="step-bullet" data-step="${i}"></span>`)
    }
    dotsContainer.innerHTML = dotsText.join('');
    dotsContainer.children[0].classList.add('step-bullet-active');
    
    const height = steps.length * 500;
    
    for (let i = 0; i < dotsContainer.children.length; i++) {
      const dot = dotsContainer.children[i];
      dot.addEventListener('click', () => {
        window.scrollBy(0,block.getBoundingClientRect().top + i / steps.length * height);
      })
    }
  
  
    ScrollTrigger.matchMedia({
      '(min-width:600px)': () => {
        let currentStep = 0;
        ScrollTrigger.create({
          trigger: block.querySelector('.how-it-works-wrapper'),
          start: 'center center+=35',
          end: '+=' + height,
          pin: true,
          pinSpacing: true,
          onUpdate(self) {
            if (self.progress === 1) return
            dotsContainer.children[currentStep].classList.remove('step-bullet-active');
            steps[currentStep].classList.remove('active');
            
            currentStep = ~~(self.progress * steps.length);
            
            dotsContainer.children[currentStep].classList.add('step-bullet-active');
            steps[currentStep].classList.add('active');
          }
        })
        
        return ()=> {
          for (let dot of dotsContainer.children) {
            dot.classList.remove('step-bullet-active');
          }
          for (let step of steps) {
            step.classList.remove('active');
          }
        }
      },
      '(max-width:599.98px)': () => {
        let currentStep = 0;
        ScrollTrigger.create({
          trigger: block.querySelector('.how-it-works-wrapper'),
          start: 'center center+=15',
          end: '+=' + height,
          pin: true,
          pinSpacing: true,
          onUpdate(self) {
            if (self.progress === 1) return
            dotsContainer.children[currentStep].classList.remove('step-bullet-active');
            steps[currentStep].classList.remove('active');
            
            currentStep = ~~(self.progress * steps.length);
            
            dotsContainer.children[currentStep].classList.add('step-bullet-active');
            steps[currentStep].classList.add('active');
          }
        })
        
        return ()=> {
          for (let dot of dotsContainer.children) {
            dot.classList.remove('step-bullet-active');
          }
          for (let step of steps) {
            step.classList.remove('active');
          }
        }
      }
    })
  
    // animations(blocks);
    // imageLazyLoading(blocks);
  }

};
windowOnLoad(blockScript);




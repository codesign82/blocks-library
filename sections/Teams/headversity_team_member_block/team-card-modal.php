<?php
$post_id = @$args['post_id'] ?: get_the_ID();
$default_image = get_field('default_image', $post_id);
$hover_image = get_field('hover_image', $post_id);
$position = get_field('position', $post_id);
$name = get_field('name', $post_id);
$bio = get_field('bio', $post_id);
?>
<div class="swiper-slide" id="swiper-slide-<?= $post_id ?>">
  <div class="modal-card" aria-modal="true" role="dialog">
    <div class="card-left">
      <?php if ($default_image) { ?>
        <picture class="img-wrapper">
          <img src="<?= $default_image['url'] ?>"
               alt="<?= $default_image['alt'] ?>">
        </picture>
      <?php } ?>
    </div>
    <div class="card-right">
      <div class="card-title"> <?= $position ?></div>
      <div class="name headline-5"><?= $name ?></div>
      <div class="description paragraph paragraph-m-paragraph  ">
        <?= $bio ?>
      </div>
    </div>
    <svg class="exit" width="39" height="39" viewBox="0 0 39 39" aria-label="<?= __('go to uncharted category', 'headversity') ?>"
         fill="none" role="application"
         xmlns="http://www.w3.org/2000/svg">
      <line x1="9.90039" y1="28.2841" x2="28.2852" y2="9.8993"
            stroke="#FF9900" stroke-width="3"
            stroke-linecap="round"/>
      <path d="M28.9922 28.2841L10.6074 9.89928" stroke="#FF9900"
            stroke-width="3" stroke-linecap="round"/>
    </svg>
  </div>
</div>

import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/propellerhealth';


import {gsap} from "gsap";
import {requestBodyGenerator} from "../../../scripts/functions/requestBodyGenerator";
import {stringToHTML} from "../../../scripts/functions/stringToHTML";
import {ScrollTrigger} from "gsap/ScrollTrigger";
import Swiper, {Navigation, Pagination} from "swiper";
import 'swiper/css'

gsap.registerPlugin(ScrollTrigger)

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.team_member_block');

// add block code here

  const mobileMedia = window.matchMedia('(max-width: 991.98px)');
  const deskMedia = window.matchMedia('(min-width: 991.98px)');
  // start region accordion //
  const modal = block.querySelector(".modal");
  const exit = block.querySelector(".exit");
  const TeamCard = block.querySelectorAll(".team-card");
  const swiper = new Swiper(block.querySelector(".mySwiper"), {
    spaceBetween: 100,
    centeredSlides: true,
    slidesPerView: 1,
    modules: [Navigation],
    navigation: {
      nextEl: block.querySelector(".swiper-button-next"),
      prevEl: block.querySelector(".swiper-button-prev"),
    },
  });


  function teamsCards(cards) {
    for (const card of cards) {

      function openCard() {
        let index = card.dataset.slideIndex;
        let slides = [...block.querySelectorAll('.swiper-slide')];
        let activeSlide = slides.indexOf(block.querySelector(`#swiper-slide-${index}`));
        swiper.slideTo(activeSlide);
        deskMedia.matches && modal.classList.add("show")
        document.documentElement.classList.add('modal-opened');
      }


      if (deskMedia.matches) {
        card.addEventListener("click", (e) => {
          openCard();
        });
      }


      const accordions = card.querySelectorAll('.content-of-card');
      accordions.forEach((accordion) => {
        const accordionHead = accordion.querySelector('.name-and-icon');
        const accordionBody = accordion.querySelector('.card-descpation');
        accordionHead?.addEventListener('click', (e) => {
          if (!accordionBody || !mobileMedia.matches) return;
          e.stopPropagation();
          const isOpened = accordion?.classList.toggle('active');
          if (!isOpened) {
            gsap.to(accordionBody, {height: 0});
          } else {
            gsap.to(Array.from(accordions).map(otherFaq => {
              const otherFaqBody = otherFaq.querySelector('.card-descpation');
              if (otherFaqBody && accordion !== otherFaq) {
                otherFaq?.classList.remove('active');
                gsap.set(otherFaq, {zIndex: 1});
              }
              return otherFaqBody;
            }), {height: 0});
            gsap.set(accordion, {zIndex: 2});
            gsap.to(accordionBody, {height: 'auto'});
          }

        });
      });
    }
  }

  teamsCards(block.querySelectorAll(".team_member-cards .team-card-col"));


  exit?.addEventListener("click", hide);
  modal?.addEventListener("click", hide);

  window.addEventListener('keydown', function (event) {
    if (event.key === 'Escape') {
      hide(event);
    }
  })

  function hide(e) {
    if (e.target.closest('.exit') || !e.target.closest('.modal-content,.swiper-button')) {
      // scroller.paused(false);
      modal.classList.remove("show")
      document.documentElement.classList.remove('modal-opened');
    }
  }

  // loadmore

  const postsContainer = block.querySelector('[posts-container]');
  const postsModalContainer = block.querySelector(".mySwiper .swiper-wrapper");
  const noPosts = block.querySelector('.no-posts');
  const loadMore = block.querySelector('.load-more-wrapper');

  loadMore.addEventListener('click', () => {
    if (loadMore.classList.contains('loading')) return;
    loadMore.classList.add('loading')
    loadMore.classList.remove('hidden')
    noPosts.classList.remove('active');

    fetch(theme_ajax_object.ajax_url, {
      method: 'POST',
      body: requestBodyGenerator(loadMore),
    }).then(async res => {
      loadMore.classList.remove('loading')
      const hasMorePages = !!res.headers.get('X-WP-Has-More-Pages');
      const totalPages = +res.headers.get('X-WP-Total-Pages');
      totalPages === 0 && noPosts.classList.add('active');
      hasMorePages || loadMore.classList.add('hidden');
      hasMorePages && JSON.parse(loadMore.dataset.args).paged === 1 && noPosts.classList.add('active');
      const htmlString = await res.json()
      const oldArgs = JSON.parse(loadMore.dataset.args);
      oldArgs.paged++;
      loadMore.dataset.args = JSON.stringify(oldArgs);
      const posts = stringToHTML(htmlString.data.teams);
      const postsModal = stringToHTML(htmlString.data.teams_modal);
      for (let i = 0; i < posts.length; i++) {
        postsContainer.appendChild(posts[i]);
        postsModalContainer.appendChild(postsModal[i]);
      }
      teamsCards(posts);
      animations(posts);
      imageLazyLoading(posts);
      ScrollTrigger.refresh();
    })

  })

  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


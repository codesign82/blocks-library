<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'team_member_block';
$className = 'team_member_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/team_member_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$link = get_field('link');
$description = get_field('description');
$query_options = get_field('query_options');
$order = @$query_options['order'];
$posts_per_page = @$query_options['posts_per_page'];
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<?php $team_members = get_field('select_team_member'); ?>
<div class="modal" aria-modal="true" role="dialog">
    <div class="modal-content">
        <div class="swiper mySwiper">
            <div class="swiper-wrapper">
                <?php
                $args = array(
                    'post_type' => 'team_members',
                    'posts_per_page' => $posts_per_page,
                    'order' => $order,
                    'post_status' => 'publish'
                );
                // The Query
                $the_query = new WP_Query($args);
                $have_posts = $the_query->have_posts();
                // The Loop
                if ($have_posts) { ?>
                    <?php while ($the_query->have_posts()) {
                        $the_query->the_post();
                        get_template_part("template-parts/team-card-modal", '', array('post_id' => get_the_ID()));
                        ?>
                    <?php } ?>
                <?php }
                /* Restore original Post Data */
                wp_reset_postdata(); ?>
            </div>
        </div>
        <button class="swiper-button swiper-button-prev arrow-left" aria-label="<?= __('slider', 'headversity') ?>">
            <svg width="24" height="37" viewBox="0 0 24 37" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
                <path d="M21.1171 34.4222L3.90214 18.3194L20.8714 2.41621"
                      stroke="#FF9900" stroke-width="4" stroke-linecap="round"/>
            </svg>
        </button>
        <button class="swiper-button swiper-button-next arrow-right" aria-label="<?= __('slider', 'headversity') ?>">
            <svg width="23" height="37" viewBox="0 0 23 37" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
                <path d="M2.7774 34.4222L19.9924 18.3194L3.02308 2.41621"
                      stroke="#FF9900" stroke-width="4" stroke-linecap="round"/>
            </svg>
        </button>
    </div>
</div>
<div class="container">
    <div class="team-member-wrapper">
        <?php if ($title) { ?>
            <h3 move-to-here class="headline-3 title text-center iv-st-from-bottom"><?= $title ?></h3>
        <?php }
        if ($description) { ?>
            <div class="paragraph paragraph-16 text-center team-member-description iv-st-from-bottom">
                <?= $description ?>
            </div>
        <?php } ?>
        <?php
        $args = array(
            'post_type' => 'team_members',
            'posts_per_page' => $posts_per_page,
            'order' => $order,
            'post_status' => 'publish'
        );
        // The Query
        $the_query = new WP_Query($args);
        $have_posts = $the_query->have_posts();
        // The Loop
        if ($have_posts) { ?>
            <div class="team_member-cards row row-cols-1 row-cols-md-2 row-cols-lg-3" posts-container>
                <?php while ($the_query->have_posts()) {
                    $the_query->the_post();
                    get_template_part("template-parts/team-card", '', array('post_id' => get_the_ID()));
                    ?>
                <?php } ?>
            </div>
        <?php }
        /* Restore original Post Data */
        wp_reset_postdata(); ?>
        <?php $args['paged'] = 2;
        $args['posts_per_page'] = $posts_per_page; ?>
        <div class="no-posts headline-2 <?= $have_posts ? '' : 'active' ?>"><?= __('No Team Members :(', 'headversity') ?></div>
        <div class="load-more-wrapper <?= $the_query->max_num_pages <= 1 ? "hidden" : "" ?>"
             data-args='<?= json_encode($args) ?>' data-action="more_team_cards"
             data-template="template-parts/team-card" data-template-modal="template-parts/team-card-modal">
            <button aria-label="Load More Posts" class="link load-more-btn"><?= __('View More', 'headversity') ?>
                <svg class="arrow" viewBox="0 0 29.5 22.1" width="28" height="15" role="application">
                    <path class="arrow-head" role="application"
                          d="M29.1,12.1a1.7,1.7,0,0,0,0-2.1L19.5.4a1.7,1.7,0,0,0-2.1,0,1.6,1.6,0,0,0,0,2.2L25.9,11l-8.5,8.5a1.6,1.6,0,0,0,0,2.2,1.7,1.7,0,0,0,2.1,0Z"
                          fill="#f90"/>
                    <path class="arrow-line" d="M0,12.5H28v-3H0Z" fill="#f90"/>
                </svg>
            </button>
            <div class="loader">
                <div class="loader-ball"></div>
                <div class="loader-ball"></div>
                <div class="loader-ball"></div>
            </div>
        </div>
    </div>
</div>
</section>


<!-- endregion headversity's Block -->

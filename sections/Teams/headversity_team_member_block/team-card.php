<?php
$post_id = @$args['post_id'] ?: get_the_ID();
$default_image = get_field('default_image', $post_id);
$hover_image = get_field('hover_image', $post_id);
$position = get_field('position', $post_id);
$name = get_field('name', $post_id);
$bio = get_field('bio', $post_id);
?>
<div class="col iv-st-from-bottom team-card-col" data-slide-index="<?= $post_id ?>">
  <div class="team-card" aria-label="<?= __('open team card modal', 'headversity') ?>">
    <?php if ($default_image ) { ?>
      <div class="profile-image aspect-ratio <?= $hover_image?'hav-hover-img':'no-hover-img' ?>">
        <?= get_acf_image($default_image, 'img-350-295' ,['img-1']) ?>
        <?php if ($hover_image) { ?>
          <?= get_acf_image($hover_image, 'img-350-295' ,['img-2']) ?>
        <?php } ?>
      </div>
    <?php } ?>
    <div class="card-content">
      <?php if ($position) { ?>
        <div class="card-title"><?= $position ?></div>
      <?php } ?>
      <div class="content-of-card">
        <?php if ($name) { ?>
          <div class="name-and-icon">
            <div class="name headline-5"><?= $name ?></div>
            <svg class="svg-icon toggle-open minus-plus"
                 viewBox="0 0 13.6 13.6">
              <line x1="0.8" y1="6.8" x2="12.8" y2="6.8" fill="none"
                    stroke="#FF9900" stroke-linecap="round"
                    stroke-miterlimit="10"
                    stroke-width="1.6"/>
              <line class="remove-to-close" x1="6.8" y1="12.8"
                    x2="6.8"
                    y2="0.8" fill="none" stroke="#FF9900"
                    stroke-linecap="round" stroke-miterlimit="10"
                    stroke-width="1.6"/>
            </svg>
          </div>
        <?php }
        if ($bio) { ?>
          <div class="card-descpation">
            <div class="paragraph paragraph-s-paragraph"><?= $bio ?>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>

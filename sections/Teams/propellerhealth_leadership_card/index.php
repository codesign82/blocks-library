<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'leadership_cards_block';
$className = 'leadership_cards_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/leadership_cards_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$automatic_or_manual = get_field('automatic_or_manual');


?>
<!-- region propellerhealth Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="content-wrapper">
        <?php if ($title) { ?>
            <div class="headline-2 title center-text iv-st-from-bottom"><?= $title ?></div>
        <?php } ?>
        <?php if ($automatic_or_manual) { ?>
            <?php
            $leaderships = get_field("leaderships");
            if ($leaderships): ?>
                <div class="row row-cols-2 row-cols-md-3 row-cols-lg-4 leadership-cards">
                    <?php foreach ($leaderships as $user):
                        $user_id = $user->ID;
                        $leadership_image = get_field('leadership_image', 'user_' . $user_id);
                        $position = get_field('position', 'user_' . $user_id);
                        $name = $user->display_name;
                        $user_nicename = $user->user_nicename;
//            $user_url = get_author_posts_url($user_id, $user_nicename);
                        $user_url = get_field('linkedin', 'user_' . $user_id);
                        ?>
                        <div class="col iv-st-from-bottom">
                            <div class="leadership-card">
                                <?php if ($leadership_image) { ?>
                                    <?php if ($user_url) { ?>
                                        <a class="user-url moving-shape" href="<?= $user_url ?>"
                                           aria-label="<?= __('open leadership page') ?>">
                                            <picture class="image-wrapper aspect-ratio">
                                                <img src="<?= $leadership_image['url'] ?>" alt="<?= $leadership_image['alt'] ?>">
                                            </picture>
                                        </a>
                                    <?php } else { ?>
                                        <div class="user-url moving-shape">
                                            <picture class="image-wrapper aspect-ratio">
                                                <img src="<?= $leadership_image['url'] ?>" alt="<?= $leadership_image['alt'] ?>">
                                            </picture>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                <?php if ($name) { ?>
                                    <?php if ($user_url) { ?>
                                        <a href="<?= $user_url ?>" aria-label="<?= __('open leadership page') ?>" class="headline-4 name has-hover fw-bold"><?= $name ?></a>
                                    <?php } else { ?>
                                        <div class="headline-4 name fw-bold"><?= $name ?></div>
                                    <?php } ?>
                                <?php } ?>
                                <?php if ($position) { ?>
                                    <div class="description paragraph"><?= $position ?></div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        <?php } else { ?>
            <?php
            $query_options = get_field('query_options');
            $number_of_posts = $query_options['number_of_leadership'];
            $order = $query_options['order'];
            $args = array(
                'role' => 'leadership',
                'number' => $number_of_posts,
                'order' => $order,
            );
            // The Query
            $user_query = new WP_User_Query($args);
            // User Loop
            ?>
            <?php if (!empty($user_query->get_results())) { ?>
                <div class="row row-cols-2 row-cols-md-3 row-cols-lg-4 leadership-cards">
                    <?php foreach ($user_query->get_results() as $user) {
                        $user_id = $user->ID;
                        $leadership_image = get_field('leadership_image', 'user_' . $user_id);
                        $position = get_field('position', 'user_' . $user_id);
                        $name = $user->display_name;
                        $user_nicename = $user->user_nicename;
//            $user_url = get_author_posts_url($user_id, $user_nicename);
                        $user_url = get_field('linkedin', 'user_' . $user_id);
                        ?>
                        <div class="col iv-st-from-bottom">
                            <div class="leadership-card ">
                                <?php if ($leadership_image) { ?>
                                    <?php if ($user_url) { ?>
                                        <a class="user-url moving-shape" href="<?= $user_url ?>"
                                           aria-label="<?= __('open leadership page') ?>">
                                            <picture class="image-wrapper aspect-ratio">
                                                <img src="<?= $leadership_image['url'] ?>" alt="<?= $leadership_image['alt'] ?>">
                                            </picture>
                                        </a>
                                    <?php } else { ?>
                                        <div class="user-url moving-shape">
                                            <picture class="image-wrapper aspect-ratio">
                                                <img src="<?= $leadership_image['url'] ?>" alt="<?= $leadership_image['alt'] ?>">
                                            </picture>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                <?php if ($name) { ?>
                                    <?php if ($user_url) { ?>
                                        <a href="<?= $user_url ?>" aria-label="<?= __('open leadership page') ?>" class="headline-4 name has-hover fw-bold"><?= $name ?></a>
                                    <?php } else { ?>
                                        <div class="headline-4 name fw-bold"><?= $name ?></div>
                                    <?php } ?>
                                <?php } ?>
                                <?php if ($position) { ?>
                                    <div class="description paragraph"><?= $position ?></div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>
</section>


<!-- endregion propellerhealth Block -->

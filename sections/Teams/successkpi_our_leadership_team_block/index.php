<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'our_leadership_team';
$className = 'our_leadership_team';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/our_leadership_team/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$automatically_or_manually = get_field('automatically_or_manually');
$teams = get_field('teams');





?>
<!-- region successkpi's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="leadership">

        <?php if ($automatically_or_manually) { ?>
            <?php if ($title) { ?>
                <h2 class="headline-2 iv-st-from-bottom"><?= $title ?></h2>
            <?php }  ?>
            <div class="row row-cols-lg-5  row-cols-md-3 row-cols-2">
                <?php if ($teams) { ?>
                    <?php foreach ($teams as $post) {
                        setup_postdata($post);
                        get_template_part("../team-card", '', array(
                            'post_id' => $post,
                        ));
                    } ?>
                <?php } ?>
                <?php wp_reset_postdata(); ?>
            </div>
        <?php } else {

            $query_options = get_field('query_options');
            $number_of_posts = @$query_options['number_of_posts'];
            $post_type = 'teams';
            $order = @$query_options['order'];
            $type = get_field('type');
            $type_slug = $type->slug;
            $args = array(
                'post_type' => 'teams',
                'posts_per_page' => $number_of_posts,
                'order' => $order,
                'post_status' => 'publish',
                'tax_query' => [
                    [
                        'taxonomy' => 'user_type',
                        'field' => 'slug',
                        'terms' => $type_slug,
                    ],
                ],
            );

            $the_query = new WP_Query($args);
            if ($the_query->have_posts()) {
                ?>

                <h2 class="headline-2"><?= $type->name ?></h2>
                <div class="row row-cols-lg-5  row-cols-md-3 row-cols-2">
                    <?php
                    // The Query
                    // The Loop
                    ?>
                    <?php while ($the_query->have_posts()) {
                        $the_query->the_post();
                        get_template_part('../team-card', '',
                            array('post_id' => get_the_id()));
                        ?>
                    <?php }
                    wp_reset_postdata(); ?>
                </div>
            <?php }} ?>

    </div>
</div>
</section>


<!-- endregion successkpi's Block -->

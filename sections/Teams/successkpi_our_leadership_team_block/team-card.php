<?php
$post_id = @$args['post_id'] ?: get_the_ID();
$post_title = get_the_title($post_id);
$position = get_field('position', $post_id);
$department = get_field('department', $post_id);
$terms = wp_get_post_terms($post_id, 'user_type');
$thumbnail_url = get_the_post_thumbnail_url($post_id);
$alt = get_post_meta(get_post_thumbnail_id($post_id), '_wp_attachment_image_alt', true);
$linked_in_url = get_field('linked_in_url', $post_id);
$biography_content = get_field('biography_content', $post_id);
?>
<div id="post-id-<?= $post_id ?>" class="col iv-st-from-bottom">
  <?php if ($linked_in_url) { ?>
    <a href="<?= $linked_in_url ?>" class="content click" target="_blank">
      <picture class="team-img aspect-ratio">
        <img src="<?= $thumbnail_url ?>" alt="<?= $alt ?>"/>
        <div class="background-layer">
          <div class="team-disc">
            <?php if ($post_title) { ?>
              <div class="headline-4 name"><?= $post_title ?></div>
            <?php } ?>
            <div class="paragraph text">
              <?= $position . ' ' . $department ?>
            </div>
          </div>
        </div>
      </picture>
    </a>
  <?php } else { ?>
    <div class="content click">
      <picture class="team-img aspect-ratio">
        <img src="<?= $thumbnail_url ?>" alt="<?= $alt ?>"/>
        <div class="background-layer">
          <div class="team-disc">
            <?php if ($post_title) { ?>
              <div class="headline-4 name"><?= $post_title ?></div>
            <?php } ?>
            <div class="paragraph text">
              <?= $position . ' ' . $department ?>
            </div>
          </div>
        </div>
      </picture>
    </div>
  <?php } ?>
</div>



import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {debouncedScrollTriggerRefresh} from "../../../scripts/functions/imageLazyLoading";
import {animations} from '../../../scripts/general/animations';
import {pageCleaningFunctions} from "../../../scripts/general/barba";
import {requestBodyGenerator} from "../../../scripts/functions/requestBodyGenerator";
import {stringToHTML} from "../../../scripts/functions/stringToHTML";
import {gsap} from 'gsap';
import {ScrollTrigger} from 'gsap/ScrollTrigger';
import '../../../scripts/sitesSizer/propellerhealth';

gsap.registerPlugin(ScrollTrigger)


const blockScript = async (container = document) => {
  const block = container.querySelector('.our_team_block');

  const categoryFilters = block.querySelectorAll('input[name="filter"]');
  const noPosts = block.querySelector('.no-posts');
  const loadMore = block.querySelector('.load-more-wrapper');
  const initialLoadMoreArgs = loadMore && JSON.parse(loadMore.dataset.args);
  const postsContainer = block.querySelector('[posts-container]');
  let loadMoreStatus = !loadMore.classList.contains('hidden');





  // region filter
  const categoryFilterChangeHandler = (event) => {
    if (loadMore.classList.contains('loading')) {
      event.preventDefault();
      return;
    }
    const isCategory = Number.isInteger(+event.target.value);
    const isAll = event.target.classList.contains('all-teams');
    const taxQuery = !isAll ? {
      tax_query: isCategory && [{
        taxonomy: 'teams',
        terms: String(event.target.value)
      }]
    } : '';
    loadMore.dataset.args = JSON.stringify({
      ...initialLoadMoreArgs,
      ...taxQuery,
      paged: 1
    });
    loadMore.classList.add('loading')
    loadMore.classList.remove('hidden')
    noPosts.classList.remove('active');

    // region add active and remove from others
    for (let categoryFilter of categoryFilters) {
      categoryFilter.parentElement.classList.remove('active-btn');
    }
    event.target?.parentElement.classList.add('active-btn');
    // endregion add active and remove from others

    postsContainer.innerHTML = '';

    fetch(theme_ajax_object.ajax_url, {
      method: 'POST',
      body: requestBodyGenerator(loadMore),
    })
        .then(async res => {
          loadMore.classList.remove('loading')
          const hasMorePages = !!res.headers.get('X-WP-Has-More-Pages');
          const totalPages = +res.headers.get('X-WP-Total-Pages');
          totalPages === 0 && noPosts.classList.add('active');
          hasMorePages || loadMore.classList.add('hidden');
          const htmlString = await res.json()
          const oldArgs = JSON.parse(loadMore.dataset.args);
          oldArgs.paged++;
          loadMore.dataset.args = JSON.stringify(oldArgs);

          const posts = stringToHTML(htmlString.data);

          for (let post of posts) {
            postsContainer.appendChild(post)
          }
          animations(posts);
          imageLazyLoading(posts);
          debouncedScrollTriggerRefresh();
        })
  };
  for (let categoryFilter of categoryFilters) {
    categoryFilter.addEventListener('input', categoryFilterChangeHandler)
  }

  if (loadMore) {
    loadMore.addEventListener('click', async () => {
      if (loadMore.classList.contains('loading')) return;
      loadMore.classList.add('loading');
      loadMore.classList.remove('hidden');
      noPosts.classList.remove('active');

      const response = await fetch(theme_ajax_object.ajax_url, {
        method: 'POST',
        body: requestBodyGenerator(loadMore),
      })
      loadMore.classList.remove('loading')
      const hasMorePages = !!response.headers.get('X-WP-Has-More-Pages');
      const totalPages = +response.headers.get('X-WP-Total-Pages');
      noPosts.classList.toggle('active', totalPages === 0)
      loadMore.classList.toggle('hidden', !hasMorePages);
      loadMoreStatus = hasMorePages;
      const htmlString = await response.json();
      const oldArgs = JSON.parse(loadMore.dataset.args);
      oldArgs.paged++;
      loadMore.dataset.args = JSON.stringify(oldArgs);

      const posts = stringToHTML(htmlString.data);
      for (let post of posts) {
        postsContainer.appendChild(post);
      }
      animations(posts);
      imageLazyLoading(posts);
      debouncedScrollTriggerRefresh();
      setTimeout(scrollHandler,100)
    })
  }
  // endregion filter

  const debouncedScrollHandler = debounce(() => !loadMore.matches('.loading, .hidden') && loadMore?.click(), 300, {leading: true, maxWait:500});
  const scrollHandler = () => {
    0 >= (block.getBoundingClientRect().bottom - window.innerHeight * (window.innerWidth <= 600 ? 5 : 3)) && debouncedScrollHandler()
  }
  window.addEventListener('scroll', scrollHandler)
  pageCleaningFunctions.push(() => window.removeEventListener('scroll', scrollHandler))
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



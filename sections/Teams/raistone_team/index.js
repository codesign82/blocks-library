import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/raistone';

import {gsap} from "gsap"

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.team_block');

  // add block code here

  const closePopupWrapper = block.querySelector('.close-popup-wrapper');
  const cardItems = block.querySelectorAll('.open-popup');
  const popupWrapper = block.querySelector('.popup-wrapper');
  const popup = block.querySelector('.popup');
  const popupImageWrapper = block.querySelector('.popup-image-wrapper img');
  const popupAvatarName = block.querySelector('.popup-avatar-name');
  const avatarJob = block.querySelector('.avatar-job');
  const linkedin = block.querySelector('.linkedin');
  const readMore = block.querySelector('.read-more-btn');
  const text = block.querySelector('.read-more-btn span');
  let avatarDescription = block.querySelector('.avatar-description');
  let avatarDescriptionHeight = '';


  //  region open popup
  for (let cardItem of cardItems) {
    cardItem.addEventListener('click', function () {
          document.documentElement.classList.add('modal-opened');
          let card = cardItem.parentElement;
          popupWrapper.classList.add('active');
          popupImageWrapper.src = card.getAttribute('data-image-url');
          popupImageWrapper.alt = card.getAttribute('data-image-alt');
          popupAvatarName.innerHTML = card.getAttribute('data-avatar-name');
          avatarJob.innerHTML = card.getAttribute('data-job-title');
          avatarDescription.innerHTML = card.querySelector('.data-description').innerHTML;
          linkedin.href = card.getAttribute('data-linkin-url');
          avatarDescriptionHeight = avatarDescription.clientHeight + 'px'
          avatarDescription.style.height = avatarDescription.clientHeight + 'px';
          console.log(avatarDescription.clientHeight);
          if (avatarDescription.clientHeight < 100) {
            readMore.classList.add('dont-show-it');
          }
        }
    );
  }
  // endregion

  // region read more
  readMore.addEventListener('click', function () {
    const active = avatarDescription.classList.toggle('max-lines');
    if (!active) {
      gsap.to(avatarDescription, {height: 'auto'});
      text.innerText = 'Read Less'
      readMore.classList.add('active');
    } else {
      avatarDescription.classList.remove('max-lines')
      text.innerText = 'Read More'
      readMore.classList.remove('active');
      gsap.to(avatarDescription, {
        height: avatarDescriptionHeight,
        onComplete: () => avatarDescription.classList.add('max-lines')
      });
    }
  });
  // endregion.

  // region close popup

  popupWrapper.addEventListener('click', function () {
    popupWrapper.classList.remove('active');
    document.documentElement.classList.remove('modal-opened');
    avatarDescription.style.height = 'auto';
    readMore.classList.remove('dont-show-it');

  });

  closePopupWrapper.addEventListener('click', function () {
    popupWrapper.classList.remove('active');
    document.documentElement.classList.remove('modal-opened');
    avatarDescription.style.height = 'auto';
    readMore.classList.remove('dont-show-it');

  });

  popup.addEventListener('click', function (e) {
    e.stopPropagation();
  });


  block.addEventListener('keydown', (event) => {
    if (event.key === 'Escape') {
      avatarDescription.style.height = 'auto';
      readMore.classList.remove('dont-show-it');

      popupWrapper.classList.remove('active');
      document.documentElement.classList.remove('modal-opened');
    }
  })
  // endregion


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


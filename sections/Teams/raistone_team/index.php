<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'team_block';
$className = 'team_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/team_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$team_members = get_field('team_members');
$hiring_card = get_field('hiring_card');
$team_card_bg = get_field('team_card_bg');
$hiring_card_bg = get_field('hiring_card_bg');
$card_title = @$hiring_card['card_title'];
$card_description = @$hiring_card['card_description'];
$cta_button = @$hiring_card['cta_button'];
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="team-content-wrapper">
        <?php if ($title) { ?>
            <h3 class="headline-2 title iv-st-from-bottom"><?= $title ?></h3>
        <?php } ?>
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 ">
            <?php
            if ($team_members): ?>
                <?php foreach ($team_members as $team_member):
                    $name = get_the_title($team_member);
                    $profile_image = get_field('profile_image', $team_member);
                    $job_title = get_field('job_title', $team_member);
                    $cta_link_text = get_field('cta_link_text', $team_member);
                    $linkedin_url = get_field('linkedin_url', $team_member);
                    $avatar_description = get_field('avatar_description', $team_member);
//          $avatar_image = get_field('avatar_image', $team_member);
                    ?>
                    <div class="col">
                        <div class="card"
                             data-image-url="<?= get_the_post_thumbnail_url($team_member) ?>"
                             data-image-alt="<?= $name ?>"
                             data-avatar-name="<?= $name ?>"
                             data-job-title="<?= $job_title ?>"
                             data-linkin-url="<?= $linkedin_url ?>" <?= $team_card_bg ? "style=background-color:$team_card_bg;" : '' ?>>
                            <div class="data-description" style="display: none"><?= $avatar_description ?></div>
                            <picture class="image-wrapper">
                                <?= get_the_post_thumbnail($team_member); ?>
                            </picture>
                            <?php if ($name) { ?>
                                <h3 class="headline-3 name"><?= $name ?></h3>
                            <?php } ?>
                            <?php if ($job_title) { ?>
                                <div class="paragraph paragraph-xl-paragraph job-description">
                                    <?= $job_title ?>
                                </div>
                            <?php } ?>
                            <?php if ($cta_link_text) { ?>
                                <button aria-label="Learn More button"
                                        class="btn-arrow open-popup"><?= $cta_link_text ?>
                                    <svg width="9" height="14" viewBox="0 0 9 14" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1.03906 13L7.03906 7L1.03906 1" stroke="#0A0A0A" stroke-width="2"
                                              stroke-linecap="round" stroke-linejoin="round"/>
                                    </svg>
                                </button>
                            <?php } ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if ($card_title || $card_description || $cta_button) { ?>
                <div class="col iv-st-from-bottom">
                    <div class="card dark-card" <?= $hiring_card_bg ? "style='background-color:$hiring_card_bg;'" : '' ?>>
                        <?php if ($card_title) { ?>
                            <h3 class="headline-3 dark-card-title"><?= $card_title ?></h3>
                        <?php } ?>
                        <?php if ($card_description) { ?>
                            <div class="paragraph paragraph-xl-paragraph dark-card-description">
                                <?= $card_description ?>
                            </div>
                        <?php } ?>
                        <?php if ($cta_button) { ?>
                            <a href="<?= $cta_button['url'] ?>" class="btn has-arrow "
                               target="<?= $cta_button['target'] ?>"><?= $cta_button['title'] ?>
                                <svg width="9" height="14" viewBox="0 0 9 14" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1.03906 13L7.03906 7L1.03906 1" stroke="#0A0A0A" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="popup-wrapper">
            <div class="popup">
                <div class="left-content">
                    <picture class="popup-image-wrapper aspect-ratio ">
                        <img src="" alt="avatar-image">
                    </picture>
                </div>
                <div class="right-content">
                    <?php if ($name) { ?>
                        <div class="avatar-name popup-avatar-name"></div>
                    <?php } ?>
                    <?php if ($job_title) { ?>
                        <div class="avatar-job paragraph paragraph-normal-paragraph"></div>
                    <?php } ?>
                    <?php if ($linkedin_url) { ?>
                        <a href="#" class="linkedin" target="_blank">
                            <svg width="24" height="23" viewBox="0 0 24 23" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                        d="M22.2283 0H1.77167C1.30179 0 0.851161 0.186657 0.518909 0.518909C0.186657 0.851161 0 1.30179 0 1.77167V22.2283C0 22.6982 0.186657 23.1488 0.518909 23.4811C0.851161 23.8133 1.30179 24 1.77167 24H22.2283C22.6982 24 23.1488 23.8133 23.4811 23.4811C23.8133 23.1488 24 22.6982 24 22.2283V1.77167C24 1.30179 23.8133 0.851161 23.4811 0.518909C23.1488 0.186657 22.6982 0 22.2283 0ZM7.15333 20.445H3.545V8.98333H7.15333V20.445ZM5.34667 7.395C4.93736 7.3927 4.53792 7.2692 4.19873 7.04009C3.85955 6.81098 3.59584 6.48653 3.44088 6.10769C3.28591 5.72885 3.24665 5.31259 3.32803 4.91145C3.40941 4.51032 3.6078 4.14228 3.89816 3.85378C4.18851 3.56529 4.55782 3.36927 4.95947 3.29046C5.36112 3.21165 5.77711 3.25359 6.15495 3.41099C6.53279 3.56838 6.85554 3.83417 7.08247 4.17481C7.30939 4.51546 7.43032 4.91569 7.43 5.325C7.43386 5.59903 7.38251 5.87104 7.27901 6.1248C7.17551 6.37857 7.02198 6.6089 6.82757 6.80207C6.63316 6.99523 6.40185 7.14728 6.14742 7.24915C5.893 7.35102 5.62067 7.40062 5.34667 7.395ZM20.4533 20.455H16.8467V14.1933C16.8467 12.3467 16.0617 11.7767 15.0483 11.7767C13.9783 11.7767 12.9283 12.5833 12.9283 14.24V20.455H9.32V8.99167H12.79V10.58H12.8367C13.185 9.875 14.405 8.67 16.2667 8.67C18.28 8.67 20.455 9.865 20.455 13.365L20.4533 20.455Z"
                                        fill="#414042"/>
                            </svg>
                        </a>
                    <?php } ?>
                    <?php if ($avatar_description) { ?>
                        <div class="avatar-description max-lines"></div>
                    <?php } ?>
                    <div class="btn-arrow read-more-btn mobile-only">
                        <span> <?= __('read More', 'raistone') ?></span>
                        <svg width="14" height="9" viewBox="0 0 14 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 1.5L7 7.5L13 1.5" stroke="#0A0A0A" stroke-width="2" stroke-linecap="round"
                                  stroke-linejoin="round"/>
                        </svg>
                    </div>
                </div>
                <div class="close-popup-wrapper">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M18 6L6 18" stroke="#0A0A0A" stroke-width="2" stroke-linecap="round"
                              stroke-linejoin="round"/>
                        <path d="M6 6L18 18" stroke="#0A0A0A" stroke-width="2" stroke-linecap="round"
                              stroke-linejoin="round"/>
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>
</section>


<!-- endregion raistone's Block -->

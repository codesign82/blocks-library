<?php wp_footer(); ?>
<?php
$footer_logo    = get_field( 'footer_logo', 'options' );
$contact_column = get_field( 'contact_column', 'options' );
$first_column   = get_field( 'first_column', 'options' );
$second_column  = get_field( 'second_column', 'options' );
$social_column  = get_field( 'social_column', 'options' );
$facebook       = get_field( 'facebook', 'options' );
$instagram      = get_field( 'instagram', 'options' );
$linked_in      = get_field( 'linked_in', 'options' );
$twitter        = get_field( 'twitter', 'options' );
$youtube        = get_field( 'youtube', 'options' );
$hide_prefooter = get_field( 'hide_prefooter' );
// Prefooter
?>
<!--region floating-modal-old-wrap-->
<?php
global $wpdb;
$lastRequestRecordStoredInDB = $wpdb->get_results( "SELECT * FROM StockPrice ORDER BY id DESC LIMIT 1" );
$lastRequestDate             = new DateTime( $lastRequestRecordStoredInDB[0]->request_date );
?>
<style>

  .req-invite-cover, .req-invite-cover * {
    font-family: inherit !important;
  }

  .req-close-btn {
    position: absolute;
    top: 20px;
    left: 30px;
    text-decoration: none;
    color: #fff;
  }

  .req-close-btn svg {
    width: 20px;
  }

  @media (max-width: 1199px) {
    .req-close-btn {
      left: auto;
      right: 20px
    }
  }

  .req-close-btn:hover, .req-close-btn:focus {
    color: #fff;
    text-decoration: none
  }

  .req-invite-cover {
    position: fixed;
    top: 50%;
    z-index: 9999;
    right: -100%;
    transform: translate(0, -50%);
    width: 57px;
    border-radius: 4px 0 0 4px;
    overflow: hidden;
    transition: 0.5s ease
  }

  @media (max-width: 1199px) {
    .req-invite-cover {
      left: 0;
      width: 100% !important;
      right: 0;
      transform: none;
      top: auto;
      bottom: -1px;
      border-radius: 0;
      min-height: 58px
    }
  }

  .req-invite-cover.site-loaded {
    right: 0
  }

  .req-invite-cover.site-loaded.active {
    width: 1000px
  }

  .req-invite-cover.site-loaded.active .req-content {
    top: 0;
  }

  .req-content {
    padding: 35px 110px 35px 40px;
    right: 0;
    background: rgba(21, 36, 47, 0.95);
    border-radius: 0 10px 10px 0;
    overflow: hidden;
    position: relative;
    min-height: 274px;
    width: 1000px
  }

  @media (min-width: 1200px) {
    .req-content {
      display: flex;
      flex-wrap: wrap;
      align-items: center;
      align-content: center
    }
  }

  @media (max-width: 1199px) {
    .req-content {
      width: 100%;
      height: auto;
      min-height: 1px;
      border-radius: 0;
      top: 0;
      display: none;
      max-height: 100vh;
      overflow-y: auto;
      padding: 50px 35px 80px;
    }

    .req-invite-cover.site-loaded.active .req-content {
      display: block;
    }
  }


  .req-content:before {
    content: "";
    height: 100%;
    width: 20px;
    position: absolute;
    background: #ffc208;
    background: -moz-linear-gradient(left, #ffc208 0%, #ff9c01 100%);
    background: -webkit-linear-gradient(left, #ffc208 0%, #ff9c01 100%);
    left: -10px;
    top: 0
  }

  @media (max-width: 1199px) {
    .req-content:before {
      display: none
    }
  }

  .req-content form {
    margin-bottom: 0;
    position: relative
  }

  .req-content form div.wpcf7-mail-sent-ok {
    margin: 0;
    bottom: -30px;
    border: 0;
    color: #398f14;
    position: absolute;
    padding-left: 0
  }

  @media (max-width: 767px) {
    .req-content form {
      margin-bottom: 0
    }
  }

  .req-content form div.wpcf7-validation-errors, .req-content form div.wpcf7-acceptance-missing {
    border: 2px solid red;
    color: red;
    padding: 3px 10px;
    font-size: 16px;
    display: inline-block;
    vertical-align: top;
    margin-bottom: 0;
    margin-left: 0;
    display: none !important
  }

  .req-button {
    position: absolute;
    right: 0;
    transform: rotate(90deg);
    transform-origin: right top;
    top: 100%;
    width: 284px;
    text-align: center;
    background: #ffc208;
    background: -moz-linear-gradient(left, #ff9c01 0%, #ffc208 100%);
    background: -webkit-linear-gradient(left, #ff9c01 0%, #ffc208 100%);
    transition: 0.5s;
    border-radius: 0 0 4px 4px;
    z-index: 1;
    padding-left: 10px
  }

  @media (max-width: 1199px) {
    .req-button {
      width: 100% !important;
      transform: rotate(0deg);
      top: auto;
      border-radius: 0;
      padding-left: 0;
      position: absolute;
      bottom: 0
    }
  }

  .req-button a {
    padding: 14px 0;
    color: #192b38;
    text-decoration: none;
    display: block;
    width: 100%;
    font-size: 17px
  }

  @media (max-width: 1023px) {
    .req-button a {
      font-size: 16px
    }
  }

  .req-button a .far {
    padding-left: 10px;
    border-left: 1px solid #c49402;
    margin-left: 15px;
    height: 24px;
    position: relative
  }

  .req-button a .far:before {
    position: relative;
    top: -5px
  }

  @media (max-width: 1023px) {
    .req-button a .far:before {
      top: 6px
    }
  }

  .req-button a .far:after {
    content: "";
    height: 7px;
    width: 7px;
    display: inline-block;
    vertical-align: middle;
    background-position: center;
    background-repeat: no-repeat;
    position: relative;
    right: 12px;
    transform: rotate(135deg);
    top: 4px;
    transition: 0.5s;
    transition-delay: 0.3s;
    animation: req_bounce 2s infinite;
    border-right: 1px solid #192b38;
    border-top: 1px solid #192b38
  }

  @media (max-width: 1023px) {
    .req-button a i.far:after {
      position: absolute;
      left: 24px;
      transform: rotate(-45deg)
    }
  }

  @keyframes req_bounce {
    0%, 20%, 50%, 80%, 100% {
      top: 10px
    }
    40% {
      top: 5px
    }
    60% {
      top: 8px
    }
  }

  .req-content input:not([type="submit"]) {
    width: 225px;
    height: 51px;
    margin-right: 20px;
    border-radius: 10px;
    background-color: transparent;
    box-shadow: 0px 5px 27px 0px rgba(255, 255, 255, 0.1);
    color: #ffc208;
    font-size: 16px;
    font-weight: 400;
    border: 0;
    margin-bottom: 10px;
    padding: 0 15px;
  }

  @media (max-width: 1023px) {
    .req-content input:not([type="submit"]) {
      width: 100%
    }
  }

  @media (max-width: 767px) {
    .req-content input:not([type="submit"]) {
      height: 36px
    }
  }

  .req-content input:not([type="submit"])::-webkit-input-placeholder {
    opacity: 1 !important;
    color: #ffc208
  }

  .req-content input:not([type="submit"]):-moz-placeholder {
    opacity: 1 !important;
    color: #ffc208
  }

  .req-content input:not([type="submit"])::-moz-placeholder {
    opacity: 1 !important;
    color: #ffc208
  }

  .req-content input:not([type="submit"]):-ms-input-placeholder {
    opacity: 1 !important;
    color: #ffc208
  }

  .req-content input:not([type="submit"]).wpcf7-not-valid {
    border: 1px solid red
  }

  .req-content input:not([type="submit"]).wpcf7-not-valid + .wpcf7-not-valid-tip {
    display: none
  }

  .req-content input {
    border-radius: 8px;
    border: 0;
    margin-bottom: 10px
  }

  .req-content input[type="submit"] {
    padding-left: 18px;
    padding-right: 18px;
    cursor: pointer;
    height: 50px
  }

  @media (max-width: 767px) {
    .req-content input[type="submit"] {
      height: 36px;
      width: 100%
    }
  }

  .req-content h4 {
    margin-bottom: 40px;
    color: #fff;
    letter-spacing: 0.03em;
    font-size: 27px;
  }

  @media (max-width: 767px) {
    .req-content h4 {
      margin-bottom: 15px
    }
  }

  .req-content p {
    color: #ffc208;
    font-size: 16px;
    margin-bottom: 0
  }

  .wpcf7 form .wpcf7-response-output {
    margin: 2em 0.5em 1em;
    padding: 0.2em 1em;
    border: 2px solid #00a0d2;
    font-size: 16px;
    color: white;
  }

  .req-content .btn, .req-content button, .req-content input[type="button"], .req-content input[type="reset"], .req-content input[type="submit"] {
    display: inline-block;
    padding: 9px 24px;
    vertical-align: top;
    color: #000;
    font-size: 16px;
    line-height: 1.5;
    text-align: center;
    border: none;
    text-transform: uppercase;
    border-radius: 7px;
    transition: background-color 0.3s ease 0s, color 0.3s ease 0s;
    text-decoration: none;
    font-weight: 700;
    background: #ff9c01;
    background: -moz-linear-gradient(left, #ff9c01 0%, #ffc208 100%);
    background: -webkit-linear-gradient(left, #ff9c01 0%, #ffc208 100%);
  }

  .req-content .btn:hover, .req-content button:hover, .req-content input[type="button"]:hover, .req-content input[type="reset"]:hover, .req-content input[type="submit"]:hover, .req-content .btn:focus, .req-content button:focus, .req-content input[type="button"]:focus, .req-content input[type="reset"]:focus, .req-content input[type="submit"]:focus {
    background: #ffc208;
    background: -moz-linear-gradient(left, #ffc208 0%, #ff9c01 100%);
    background: -webkit-linear-gradient(left, #ffc208 0%, #ff9c01 100%);
    text-decoration: none;
  }

  @media (max-width: 1199px) {
    .req-content input[type="submit"] {
      height: 36px;
      width: 100%;
    }
  }

  @media (max-width: 767px) {
    .req-content {
      padding-left: 20px;
      padding-right: 20px
    }
  }
</style>
<div class="req-invite-cover">
  <div class="req-content">
    <a href="javascript:void(0)" class="req-close-btn">
      <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="times-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="far fa-times-circle">
        <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z" class=""></path>
      </svg>
    </a>
    <h4><?php the_field( 'form_title', 'option' ) ?></h4>
    <?php echo do_shortcode( '[contact-form-7 id="20" title="Contact Form"]' ); ?>
    <!--<p><?php //the_field('form_footer_text','option') ?></p>-->
  </div>
  <div class="req-button">
    <!-- $_SESSION["stockPrice"] -->
    <a href="javascript:void(0)">
      <span>NASDAQ: <b id="stockPriceBTNText">WRAP <?php echo $lastRequestRecordStoredInDB[0]->price; ?></b></span>
      <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="envelope" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="far fa-envelope">
        <path fill="currentColor" d="M464 64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V112c0-26.51-21.49-48-48-48zm0 48v40.805c-22.422 18.259-58.168 46.651-134.587 106.49-16.841 13.247-50.201 45.072-73.413 44.701-23.208.375-56.579-31.459-73.413-44.701C106.18 199.465 70.425 171.067 48 152.805V112h416zM48 400V214.398c22.914 18.251 55.409 43.862 104.938 82.646 21.857 17.205 60.134 55.186 103.062 54.955 42.717.231 80.509-37.199 103.053-54.947 49.528-38.783 82.032-64.401 104.947-82.653V400H48z" class=""></path>
      </svg>
    </a>
  </div>
</div>
<script>
  function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
  }

  function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
      x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
      y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
      x = x.replace(/^\s+|\s+$/g, "");
      if (x == c_name) {
        return unescape(y);
      }
    }
  }

  jQuery(document).ready(function ($) {

    $('.req-button a').on('click', function () {
      $('.req-invite-cover').toggleClass('active');
      setCookie("open", true, 1);
    })
    $('.req-close-btn').on('click', function () {
      $('.req-invite-cover').removeClass('active');
      setCookie("open", true, 1);
    })

    setTimeout(() => $('.req-invite-cover').addClass('site-loaded'), 3000)
    var openToggle = getCookie("open");
    if (openToggle === undefined) {
      setTimeout(function () {
        if ($('body').hasClass('home')) {
          if (!$('.req-invite-cover').hasClass('has-clicked')) {
            $('.req-button a').trigger('click');
            setCookie("open", true, 1);
          }
        }
      }, 15000)
    }

    var wpcf7Form = document.querySelector('.req-invite-cover .wpcf7');

    wpcf7Form.addEventListener('wpcf7submit', function (event) {
      setTimeout(function () {
        if ($('.req-invite-cover .wpcf7-response-output ').hasClass('wpcf7-mail-sent-ok')) {
          $('.req-button a').trigger('click');
        }
      }, 2000)
    }, false);

    var getUrlParameter = function getUrlParameter(sParam) {
      var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
      for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
          return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
      }
    };

    var utmsource = getUrlParameter('utm_source');
    if (utmsource != 'undefined') {
      $("#utm_source").val(utmsource);
    }

    var utm_campaign = getUrlParameter('utm_campaign');
    if (utm_campaign != 'undefined') {
      $("#utm_campaign").val(utm_campaign);
    }

    var utm_medium = getUrlParameter('utm_medium');
    if (utm_medium != 'undefined') {
      $("#utm_medium").val(utm_medium);
    }


    const $theDate = new Date();

    if ($theDate) {
      const $dateMins = $theDate.getMinutes();
      const $dateSec = $theDate.getSeconds();
      const $dateHours = $theDate.getHours();


      const theDateSplit = $theDate.toISOString().replace(/T.*/, '');
      let submit_date = `${theDateSplit} ${$dateHours}:${$dateMins}:${$dateSec}`;
      $("#submit_date").val(submit_date);
    }
  });
</script>
<!--endregion floating-modal-old-wrap-->
<!--region footer-->
<footer>
  <?php if ( ! $hide_prefooter && have_rows( 'prefooter_cards', 'options' ) ) { ?>
    <div class="prefooter">
      <div class="container">
        <div class="prefooter-wrapper">
          <?php while ( have_rows( 'prefooter_cards', 'options' ) ) {
            the_row();
            $title       = get_sub_field( 'title' );
            $description = get_sub_field( 'description' );
            $cta_button  = get_sub_field( 'cta_button' );
            ?>
            <div class="prefooter-card iv-st-from-bottom">
              <?php if ( $title ) { ?>
                <h4 class="headline-4 prefooter-card-title text-uppercase"><?= $title ?></h4>
              <?php } ?>
              <?php if ( $description ) { ?>
                <div class="paragraph paragraph-14 prefooter-card-description"><?= $description ?></div>
              <?php } ?>
              <?php get_template_part( 'template-parts/cta_button', '', array(
                'className' => 'prefooter-card-btn',
                'cta_group' => $cta_button
              ) ) ?>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  <?php } ?>
  <div class="container footer-container">
    <div class="footer-wrapper">
      <a href="<?= site_url() ?>" class="footer-logo">
        <?php if ( ! $footer_logo['svg_or_img'] ) { ?>
          <?php if ( $footer_logo['svg'] ) {
            echo $footer_logo['svg'];
          } ?>
        <?php } else { ?>
          <img src="<?= $footer_logo['image']['url'] ?>" alt="<?= $footer_logo['image']['alt'] ?>">
        <?php } ?>
      </a>
      <div class="footer-links-parent">
        <div class="footer-links-wrapper footer-contact-wrapper">
          <?php if ( $contact_column['title'] ) { ?>
            <p class="footer-link-title"><?= $contact_column['title'] ?></p>
          <?php } ?>
          <?php if ( $contact_column['address'] || $contact_column['phone'] || $contact_column['email'] ) { ?>
            <ul class="footer-links">
              <?php if ( $contact_column['address'] ) { ?>
                <li class="footer-link footer-street">
                  <a data-hover href="#"><?= $contact_column['address'] ?></a>
                </li>
              <?php } ?>
              <?php if ( $contact_column['phone'] ) { ?>
                <li class="footer-link footer-phone">
                  <a data-hover href="tel:<?= $contact_column['phone'] ?>"><?= $contact_column['phone'] ?></a>
                </li>
              <?php } ?>
              <?php if ( $contact_column['email'] ) { ?>
                <li class="footer-link footer-email">
                  <a data-hover href="mailto:<?= $contact_column['email'] ?>"><?= $contact_column['email'] ?></a>
                </li>
              <?php } ?>
            </ul>
          <?php } ?>
        </div>
        <div class="footer-links-wrapper">
          <?php if ( $first_column['title'] ) { ?>
            <p class="footer-link-title"><?= $first_column['title'] ?></p>
          <?php } ?>
          <?php
          if ( have_rows( 'first_column', 'options' ) ) :
            while ( have_rows( 'first_column', 'options' ) ) :
              the_row();
              ?>
              <?php if ( have_rows( 'links' ) ) : ?>
              <ul class="footer-links">
                <?php while ( have_rows( 'links' ) ) : the_row();
                  $link = get_sub_field( 'link' ); ?>
                  <?php if ( $link ) { ?>
                    <li class="footer-link">
                      <a data-hover href="<?= $link['url'] ?>" target="<?= $link['target'] ?>"><?= $link['title'] ?></a>
                    </li>
                  <?php } ?>
                <?php endwhile; ?>
              </ul>
            <?php endif; ?>
            <?php endwhile; endif; ?>
        </div>
        <div class="footer-links-wrapper">
          <div class="footer-link-title"><?= $second_column['title'] ?></div>
          <?php
          if ( have_rows( 'second_column', 'options' ) ) :
            while ( have_rows( 'second_column', 'options' ) ) :
              the_row();
              ?>
              <?php if ( have_rows( 'links' ) ) : ?>
              <ul class="footer-links">
                <?php while ( have_rows( 'links' ) ) : the_row();
                  $link = get_sub_field( 'link' ); ?>
                  <?php if ( $link ) { ?>
                    <li class="footer-link">
                      <a data-hover href="<?= $link['url'] ?>" target="<?= $link['target'] ?>"><?= $link['title'] ?></a>
                    </li>
                  <?php } ?>
                <?php endwhile; ?>
              </ul>
            <?php endif; ?>
            <?php endwhile; endif; ?>
        </div>
        <div class="footer-links-wrapper footer-social-wrapper">
          <?php if ( $social_column['title'] ) { ?>
            <p class="footer-social-title footer-link-title"><?= $social_column['title'] ?></p>
          <?php } ?>
          <ul class="footer-social">
            <?php if ( $social_column['facebook_url'] ) { ?>
              <li class="footer-social-link footer-social-link-facebook">
                <a href="<?= $social_column['facebook_url'] ?>" target="_blank">
                  <svg width="9" height="16" viewBox="0 0 9 16" fill="none">
                    <path d="M5.5 9.125H7.375L8.125 6.125H5.5V4.625C5.5 3.8525 5.5 3.125 7 3.125H8.125V0.605C7.8805 0.57275 6.95725 0.5 5.98225 0.5C3.946 0.5 2.5 1.74275 2.5 4.025V6.125H0.25V9.125H2.5V15.5H5.5V9.125Z" fill="currentColor"/>
                  </svg>
                </a></li>
            <?php } ?>
            <?php if ( $social_column['instagram_url'] ) { ?>
              <li class="footer-social-link">
                <a href="<?= $social_column['instagram_url'] ?>" target="_blank">
                  <svg width="16" height="16" viewBox="0 0 16 16" fill="none">
                    <path d="M8 0.5C10.0378 0.5 10.292 0.5075 11.0915 0.545C11.8903 0.5825 12.434 0.70775 12.9125 0.89375C13.4075 1.08425 13.8245 1.34225 14.2415 1.7585C14.6229 2.13342 14.918 2.58694 15.1063 3.0875C15.2915 3.56525 15.4175 4.10975 15.455 4.9085C15.4903 5.708 15.5 5.96225 15.5 8C15.5 10.0378 15.4925 10.292 15.455 11.0915C15.4175 11.8903 15.2915 12.434 15.1063 12.9125C14.9185 13.4133 14.6233 13.867 14.2415 14.2415C13.8665 14.6227 13.413 14.9178 12.9125 15.1063C12.4347 15.2915 11.8903 15.4175 11.0915 15.455C10.292 15.4903 10.0378 15.5 8 15.5C5.96225 15.5 5.708 15.4925 4.9085 15.455C4.10975 15.4175 3.566 15.2915 3.0875 15.1063C2.58674 14.9184 2.13315 14.6232 1.7585 14.2415C1.37705 13.8666 1.08195 13.4131 0.89375 12.9125C0.70775 12.4347 0.5825 11.8903 0.545 11.0915C0.50975 10.292 0.5 10.0378 0.5 8C0.5 5.96225 0.5075 5.708 0.545 4.9085C0.5825 4.109 0.70775 3.566 0.89375 3.0875C1.08143 2.58663 1.3766 2.13299 1.7585 1.7585C2.13325 1.37692 2.58682 1.0818 3.0875 0.89375C3.566 0.70775 4.109 0.5825 4.9085 0.545C5.708 0.50975 5.96225 0.5 8 0.5ZM8 4.25C7.00544 4.25 6.05161 4.64509 5.34835 5.34835C4.64509 6.05161 4.25 7.00544 4.25 8C4.25 8.99456 4.64509 9.94839 5.34835 10.6517C6.05161 11.3549 7.00544 11.75 8 11.75C8.99456 11.75 9.94839 11.3549 10.6517 10.6517C11.3549 9.94839 11.75 8.99456 11.75 8C11.75 7.00544 11.3549 6.05161 10.6517 5.34835C9.94839 4.64509 8.99456 4.25 8 4.25ZM12.875 4.0625C12.875 3.81386 12.7762 3.5754 12.6004 3.39959C12.4246 3.22377 12.1861 3.125 11.9375 3.125C11.6889 3.125 11.4504 3.22377 11.2746 3.39959C11.0988 3.5754 11 3.81386 11 4.0625C11 4.31114 11.0988 4.5496 11.2746 4.72541C11.4504 4.90123 11.6889 5 11.9375 5C12.1861 5 12.4246 4.90123 12.6004 4.72541C12.7762 4.5496 12.875 4.31114 12.875 4.0625ZM8 5.75C8.59674 5.75 9.16903 5.98705 9.59099 6.40901C10.0129 6.83097 10.25 7.40326 10.25 8C10.25 8.59674 10.0129 9.16903 9.59099 9.59099C9.16903 10.0129 8.59674 10.25 8 10.25C7.40326 10.25 6.83097 10.0129 6.40901 9.59099C5.98705 9.16903 5.75 8.59674 5.75 8C5.75 7.40326 5.98705 6.83097 6.40901 6.40901C6.83097 5.98705 7.40326 5.75 8 5.75Z" fill="currentColor"/>
                  </svg>
                </a></li>
            <?php } ?>
            <?php if ( $social_column['linked_in_url'] ) { ?>
              <li class="footer-social-link">
                <a href="<?= $social_column['linked_in_url'] ?>" target="_blank">
                  <svg width="15" height="14" viewBox="0 0 15 14" fill="none">
                    <path d="M3.20508 1.74977C3.20488 2.1476 3.04665 2.52905 2.76521 2.81021C2.48376 3.09138 2.10215 3.24922 1.70433 3.24902C1.3065 3.24882 0.925052 3.0906 0.643888 2.80915C0.362724 2.52771 0.204879 2.1461 0.205078 1.74827C0.205277 1.35045 0.363503 0.968997 0.644948 0.687833C0.926394 0.406669 1.308 0.248825 1.70583 0.249024C2.10365 0.249223 2.48511 0.407449 2.76627 0.688894C3.04743 0.970339 3.20528 1.35195 3.20508 1.74977ZM3.25008 4.35977H0.250078V13.7498H3.25008V4.35977ZM7.99008 4.35977H5.00508V13.7498H7.96008V8.82227C7.96008 6.07727 11.5376 5.82227 11.5376 8.82227V13.7498H14.5001V7.80227C14.5001 3.17477 9.20508 3.34727 7.96008 5.61977L7.99008 4.35977Z" fill="currentColor"/>
                  </svg>
                </a></li>
            <?php } ?>
            <?php if ( $social_column['twitter_url'] ) { ?>
              <li class="footer-social-link">
                <a href="<?= $social_column['twitter_url'] ?>" target="_blank">
                  <svg width="16" height="14" viewBox="0 0 16 14" fill="none">
                    <path d="M15.6222 2.24219C15.0496 2.49546 14.4424 2.66179 13.8207 2.73569C14.476 2.34376 14.9665 1.72695 15.2007 1.00019C14.5857 1.36619 13.9115 1.62269 13.2087 1.76144C12.7367 1.25637 12.111 0.921411 11.4289 0.808628C10.7469 0.695844 10.0467 0.811557 9.43716 1.13778C8.82765 1.464 8.343 1.98245 8.05855 2.61253C7.77409 3.24262 7.70577 3.94902 7.86421 4.62194C6.61704 4.55943 5.39696 4.23533 4.28318 3.67069C3.16939 3.10604 2.18681 2.31347 1.39921 1.34444C1.12043 1.82328 0.973927 2.36761 0.974711 2.92169C0.974711 4.00919 1.52821 4.96994 2.36971 5.53244C1.87172 5.51676 1.38469 5.38227 0.949211 5.14019V5.17919C0.949361 5.90346 1.19999 6.6054 1.6586 7.16598C2.11721 7.72657 2.75557 8.1113 3.46546 8.25494C3.00317 8.38022 2.51844 8.39868 2.04796 8.30894C2.24811 8.93236 2.63822 9.47759 3.16365 9.86827C3.68909 10.259 4.32356 10.4755 4.97821 10.4877C4.32757 10.9987 3.58259 11.3764 2.78587 11.5993C1.98915 11.8222 1.15631 11.8859 0.334961 11.7867C1.76873 12.7088 3.43778 13.1983 5.14246 13.1967C10.9122 13.1967 14.0675 8.41694 14.0675 4.27169C14.0675 4.13669 14.0637 4.00019 14.0577 3.86669C14.6719 3.42281 15.2019 2.87295 15.623 2.24294L15.6222 2.24219Z" fill="currentColor"/>
                  </svg>
                </a></li>
            <?php } ?>
            <?php if ( $social_column['youtube_url'] ) { ?>
              <li class="footer-social-link">
                <a href="<?= $social_column['youtube_url'] ?>" target="_blank">
                  <svg width="16" height="12" viewBox="0 0 16 12" fill="none">
                    <path d="M15.1572 1.8735C15.5 3.21 15.5 6 15.5 6C15.5 6 15.5 8.79 15.1572 10.1265C14.9667 10.8653 14.4095 11.4465 13.7037 11.643C12.422 12 8 12 8 12C8 12 3.58025 12 2.29625 11.643C1.5875 11.4435 1.031 10.863 0.84275 10.1265C0.5 8.79 0.5 6 0.5 6C0.5 6 0.5 3.21 0.84275 1.8735C1.03325 1.13475 1.5905 0.5535 2.29625 0.357C3.58025 -1.3411e-07 8 0 8 0C8 0 12.422 -1.3411e-07 13.7037 0.357C14.4125 0.5565 14.969 1.137 15.1572 1.8735ZM6.5 8.625L11 6L6.5 3.375V8.625Z" fill="currentColor"/>
                  </svg>
                </a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>
<!--endregion footer-->

<div class="video-modal custom-modal" id="custom-modal">
  <div class="custom-modal-inner">
    <button class="close-modal" aria-label="Close Modal">
      <svg height="16" viewBox="0 0 17 16" width="17" xmlns="http://www.w3.org/2000/svg">
        <path d="M16.274 1.676L14.6 0l-6.19 6.19L2.22 0 .545 1.676l6.19 6.189-6.19 6.19L2.22 15.73l6.19-6.19 6.189 6.19 1.675-1.676-6.189-6.189z" fill="#9899a2"/>
      </svg>
    </button>
    <div class="custom-modal-wrap">
      <div class="video-wrapper">
        <iframe allowfullscreen src></iframe>
      </div>
    </div>
  </div>
</div>
</body>
</html>

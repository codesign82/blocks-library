<?php wp_footer(); ?>
<!--Footer ACF-->

<?php
$code_before_end_of_body_tag = get_field('code_before_end_of_body_tag', 'options');

$logo_group = get_field('logo', 'options');

$copy_rights_and_polices = get_field('copy_rights_and_polices', 'options');
$policy_links = array_key_checker('policy_links', $copy_rights_and_polices);
$phone = get_field('phone', 'options');
$working_hours = get_field('hours_of_operation', 'options');
$email = get_field('email', 'options');
$column_one = get_field('column_one', 'options');
$column_one_title = @$column_one['column_title'];
$column_two = get_field('column_two', 'options');
$column_two_title = @$column_two['column_title'];
$form = @$column_two['form'];
$linked_in = get_field('linkedin', 'options');
$facebook = get_field('facebook', 'options');
$twitter = get_field('twitter', 'options');
$youtube = get_field('youtube', 'options');

?>
<!--region footer-->
<svg class="slide-shape slide-shape-test">
    <defs>
        <clipPath id="shape-1" clipPathUnits="objectBoundingBox">
            <path d=""></path>
        </clipPath>
        <clipPath id="shape-2" clipPathUnits="objectBoundingBox">
            <path d=""></path>
        </clipPath>
        <clipPath id="shape-3" clipPathUnits="objectBoundingBox">
            <path d=""></path>
        </clipPath>
        <clipPath id="shape-4" clipPathUnits="objectBoundingBox">
            <path d=""></path>
        </clipPath>
        <clipPath id="shape-5" clipPathUnits="objectBoundingBox">
            <path d=""></path>
        </clipPath>
    </defs>
</svg>


<footer role="contentinfo">
    <div class="container">
        <div class="content-wrapper">
            <?php if ($logo_group) {
                $logo_type = $logo_group['logo_type'];
                $img = $logo_group['image'];
                $svg = $logo_group['svg'];
                ?>
                <a role="button" aria-label="main logo chick to go home page" target="_self"  href="<?= site_url() . '/' ?>"
                   class="logo" >
                    <?php if ($logo_type === 'img') { ?>
                        <img src="<?= $img['url'] ?>" alt="<?= $img['alt'] ?>">
                    <?php } else { ?>
                        <?= $svg ?>
                    <?php } ?>
                </a>
            <?php } ?>
            <div class="links-wrapper">
                <div class=' links-column pages-links'>
                    <h4
                            class='links-title footer-nav-title'><?= $column_one_title ?></h4>
                    <?php if (have_rows('column_one', 'options')) { ?>
                        <?php while (have_rows('column_one', 'options')) {
                            the_row();
                            ?>

                            <ul class="links">
                                <?php if (have_rows('list')) { ?>
                                <?php while (have_rows('list')) {
                                the_row();
                                $list_item = get_sub_field('list_item');
                                ?>
                                <li class="link-item footer-nav">
                                    <a
                                            target="<?= $list_item['target'] ?>"
                                            href="<?= $list_item['url'] ?>"> <?= $list_item['title'] ?></a>
                                    <?php } ?>
                                    <?php } ?>
                                </li>
                            </ul>
                        <?php } ?>
                    <?php } ?>
                </div>
                <?php if ($column_two) { ?>
                    <div class=' links-column pages-links'>
                        <?php if ($column_two_title) { ?>
                            <h4
                                    class='links-title footer-nav-title'><?= $column_two_title ?></h4>
                        <?php } ?>
                        <?php if ($working_hours || $phone) { ?>
                            <div class='phone_time'>
                                <a href="tel:<?= $phone ?>" type=""><?= $phone ?> </a>|<span>  <?= $working_hours ?></span>
                            </div>
                        <?php } ?>
                        <?php if ($email) { ?>
                            <a target="_blank" href="mailto: <?= $email ?>"
                               type="email"
                               class='email'><?= $email ?></a>
                        <?php } ?>
                        <?php if ($form) { ?>
                            <div class='form-wrapper'>
                                <?php echo do_shortcode('[gravityform id="' . $form . '" ajax="true" title="false" description="false"]'); ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($youtube || $twitter || $facebook || $linked_in) { ?>
                    <div class=' links-column pages-links'>
                        <h4
                                class='links-title footer-nav-title'><?php _e('Connect', 'propellerHealth') ?></h4>
                        <ul class="links">
                            <?php if ($linked_in) { ?>
                                <li class="link-item footer-nav">
                                    <a
                                            target='_blank'
                                            href="<?= $linked_in ?>"><?php _e('LinkedIn', 'propellerHealth') ?></a>
                                </li>
                            <?php } ?>
                            <?php if ($facebook) { ?>
                                <li class="link-item footer-nav">
                                    <a
                                            target='_blank'
                                            href="<?= $facebook ?>"><?php _e('Facebook', 'propellerHealth') ?></a>
                                </li>
                            <?php } ?>
                            <?php if ($twitter) { ?>
                                <li class="link-item footer-nav">
                                    <a
                                            target='_blank'
                                            href="<?= $twitter ?>"><?php _e('Twitter', 'propellerHealth') ?></a>
                                </li>
                            <?php } ?>
                            <?php if ($youtube) { ?>
                                <li class="link-item footer-nav">
                                    <a
                                            target='_blank'
                                            href="<?= $youtube ?>"><?php _e('Youtube', 'propellerHealth') ?></a>
                                </li>
                            <?php } ?>
                        </ul>

                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="references hidden">
            <div
                    class="references-title"><?php _e('References', 'propellerHealth') ?></div>
        </div>
        <?php if ($copy_rights_and_polices) { ?>
            <div class="copyright-and-links">
                <div class="copyrights">Copyright © <?php echo date('Y'); ?>
                    Propeller Health.
                </div>
                <ul class='policy-links'>
                    <?php if (is_array($policy_links) && !empty($policy_links)) {
                        foreach ($policy_links as $policy_link) {
                            $policy_link = $policy_link['policy_link_item'];
                            ?>
                            <li class="policy-link-item micro-text ">
                                <a
                                        href="<?= $policy_link['url'] ?>"><?= $policy_link['title'] ?>
                                </a>
                            </li>
                        <?php }
                    } ?>
                </ul>
            </div>
        <?php } ?>
    </div>
</footer>
</main>
<script
        src='https://code.jquery.com/jquery-3.6.1.min.js'
        integrity='sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ='
        crossorigin='anonymous'></script>
<script type='text/javascript' id='gform_checker'>
    // import $ from 'jquery';
    if ($ && document.querySelector('form[id *="gform"]')) {
        $(document).on('gform_post_render', function (event, form_id, current_page) {
            if ($('.gform_validation_errors .gfield_description')) {
                $('.gform_validation_errors .gfield_description').slideDown();
            }
        });
    } else {
        document.querySelector('script#gform_checker').remove();
    }

</script>
<!-- General Custom Modal-->
<div class="custom-modal" id="custom-modal">
    <div class="custom-modal-inner">
        <button class="close-modal" aria-label="Close Card Modal">
            <svg class="close-modal-text" x="0px" y="0px" width="20" height="20"
                 viewBox="0 0 512.001 512.001">
                <path d="M284.286,256.002L506.143,34.144c7.811-7.811,7.811-20.475,0-28.285c-7.811-7.81-20.475-7.811-28.285,0L256,227.717
L34.143,5.859c-7.811-7.811-20.475-7.811-28.285,0c-7.81,7.811-7.811,20.475,0,28.285l221.857,221.857L5.858,477.859
c-7.811,7.811-7.811,20.475,0,28.285c3.905,3.905,9.024,5.857,14.143,5.857c5.119,0,10.237-1.952,14.143-5.857L256,284.287
l221.857,221.857c3.905,3.905,9.024,5.857,14.143,5.857s10.237-1.952,14.143-5.857c7.811-7.811,7.811-20.475,0-28.285
L284.286,256.002z" fill="currentColor"/>
            </svg>
            <svg class="close-modal-video" width="20" height="20"
                 viewBox="0 0 20 20"
                 fill="none">
                <rect x="0.5" y="0.5" width="19" height="19" stroke="white"/>
                <line x1="1.00785" y1="0.95797" x2="18.8733" y2="18.8234"
                      stroke="white"/>
                <line x1="18.7129" y1="1.66508" x2="0.84748" y2="19.5305"
                      stroke="white"/>
            </svg>
            <span class="contact-form-title">Get in touch</span>
        </button>
        <div class="custom-modal-content">
        </div>
    </div>
</div>
<?= $code_before_end_of_body_tag ?>
</body>
</html>


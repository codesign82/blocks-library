import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/propellerhealth';

const blockScript = async (container = document) => {
  const footer = container.querySelector('footer');

  const submitButton =footer.querySelector('.form-wrapper [id^=gform_submit_button] ');
  const requiredMessage =footer.querySelector('.form-wrapper [id^=validation_message] ');
  if(submitButton) submitButton.setAttribute('aria-label', 'Sign-up form');
  if(requiredMessage) requiredMessage.setAttribute('aria-invalid', 'Sign-up form');


  animations(footer);
  imageLazyLoading(footer);
};
windowOnLoad(blockScript);



import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/propellerhealth';

const blockScript = async (container = document) => {
  const footer = container.querySelector('footer');
  
  
  animations(footer);
  imageLazyLoading(footer);
};
windowOnLoad(blockScript);



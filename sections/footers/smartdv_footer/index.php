<?php wp_footer(); ?>
<!--Footer ACF-->
<?php
$code_before_end_of_body_tag = get_field('code_before_end_of_body_tag', 'options');
$footer_logo = get_field('footer_logo', 'options');
$svg_or_image = @$footer_logo['svg_or_image'];
$svg = @$footer_logo['svg'];
$image = @$footer_logo['image'];
$first_column = get_field('first_column', 'options');
$second_column = get_field('second_column', 'options');
$third_column = get_field('third_column', 'options');
$fourth_column = get_field('fourth_column', 'options');
$copyright = get_field('copyright', 'options');
$privacy = get_field('privacy', 'options');
$terms = get_field('terms', 'options');
$address_encoded = urlencode($fourth_column['address']);
$address_url = "https://google.com/maps/search/?api=1&query= $address_encoded";
?>
<!--region footer-->
<footer>
    <div class="container">
        <div class="circles">
            <div class="circle"></div>
            <div class="line"></div>
            <div class="circle"></div>
        </div>
        <a href="<?= site_url() . '/' ?>" class="footer-logo"
           aria-label="Samrt Dv Logo ">
            <?php if ($svg_or_image) { ?>
                <picture class="footer-logo-img">
                    <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                </picture>
            <?php } else {
                echo $svg;
            } ?>
        </a>

        <div class="footer-wrapper">
            <div class="footer-links">
                <?php if ($first_column['footer_title']) { ?>
                    <div class="footer-title"><?= $first_column['footer_title'] ?></div>
                <?php } ?>
                <?php if (have_rows('first_column', 'options')) { ?>
                    <?php while (have_rows('first_column', 'options')) {
                        the_row();
                        ?>
                        <?php if (have_rows('footer_links', 'options')) { ?>
                            <ul>
                                <?php while (have_rows('footer_links', 'options')) {
                                    the_row();
                                    $footer_link = get_sub_field('footer_link');
                                    ?>
                                    <?php if ($footer_link) { ?>
                                        <li class="footer-link">
                                            <a href="<?= $footer_link['url'] ?>"><?= $footer_link['title'] ?></a>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="footer-links">
                <?php if ($second_column['footer_title']) { ?>
                    <div class="footer-title"><?= $second_column['footer_title'] ?></div>
                <?php } ?>
                <?php if (have_rows('second_column', 'options')) { ?>
                    <?php while (have_rows('second_column', 'options')) {
                        the_row();
                        ?>
                        <?php if (have_rows('footer_links', 'options')) { ?>
                            <ul>
                                <?php while (have_rows('footer_links', 'options')) {
                                    the_row();
                                    $footer_link = get_sub_field('footer_link');
                                    ?>
                                    <?php if ($footer_link) { ?>
                                        <li class="footer-link">
                                            <a href="<?= $footer_link['url'] ?>"><?= $footer_link['title'] ?></a>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="footer-links space-text">
                <?php if ($third_column['footer_title']) { ?>
                    <div class="footer-title"><?= $third_column['footer_title'] ?></div>
                <?php } ?>
                <?php if (have_rows('third_column', 'options')) { ?>
                    <?php while (have_rows('third_column', 'options')) {
                        the_row();
                        ?>
                        <?php if (have_rows('footer_links', 'options')) { ?>
                            <ul>
                                <?php while (have_rows('footer_links', 'options')) {
                                    the_row();
                                    $footer_link = get_sub_field('footer_link');
                                    ?>
                                    <?php if ($footer_link) { ?>
                                        <li class="footer-link">
                                            <a href="<?= $footer_link['url'] ?>"><?= $footer_link['title'] ?></a>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>

            <div class="footer-links">
                <?php if ($fourth_column['footer_title']) { ?>
                    <div class="footer-title"><?= $fourth_column['footer_title'] ?></div>
                <?php } ?>
                <?php if ($fourth_column['phone_number']) { ?>
                    <a href="tel:<?= $fourth_column['phone_number']['url'] ?>"
                       class="footer-link"><?= $fourth_column['phone_number']['title'] ?></a>
                <?php } ?>
                <?php if ($fourth_column['email']) { ?>
                    <a href="mailto:<?= $fourth_column['email']['url'] ?>"
                       class="footer-link"><?= $fourth_column['email']['title'] ?></a>
                <?php } ?>
                <?php if ($fourth_column['address']) { ?>
                    <a class="footer-link address-link" href="<?= $address_url ?>"><?= $fourth_column['address'] ?></a>
                <?php } ?>
            </div>
        </div>
        <?php if ($fourth_column['contact_us_button']) { ?>
            <div class="btn-wrapper">
                <a class="cta-button" href="<?= $fourth_column['contact_us_button']['url'] ?>">
                    <?= $fourth_column['contact_us_button']['title'] ?>
                </a>
            </div>
        <?php } ?>
        <div class="copy-right-wrapper">
            <div class="copy-text">
                <div class="copy-right-text">
                    <?php if ($copyright) { ?><?= $copyright ?><?php } ?>
                    <span>|</span> <?php if ($privacy) { ?>
                        <a href="<?= $privacy['url'] ?>"><?= $privacy['title'] ?></a>
                    <?php } ?> <span>|</span><?php if ($terms) { ?>
                        <a href=" <?= $terms['url'] ?>">
                            <?= $terms['title'] ?>
                        </a>
                    <?php } ?>
                </div>
                <a href="https://www.takeoffnyc.com/" class="copy-right-text">Site by
                    takeoff</a>
            </div>
            <div class="contact-details">
                <a href="#" class="svg-icon" aria-label="Linkedin link ">
                    <svg width="13" height="13" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3.05575 12.8193H0.448001V4.42138H3.05575V12.8193ZM1.75047 3.27583C0.916599 3.27583 0.240234 2.58513 0.240234 1.75124C0.240234 1.35069 0.399348 0.966547 0.682573 0.683316C0.965798 0.400084 1.34993 0.240967 1.75047 0.240967C2.15101 0.240967 2.53515 0.400084 2.81837 0.683316C3.1016 0.966547 3.26071 1.35069 3.26071 1.75124C3.26071 2.58513 2.58407 3.27583 1.75047 3.27583ZM12.8157 12.8193H10.2136V8.73124C10.2136 7.75696 10.1939 6.50752 8.85778 6.50752C7.50196 6.50752 7.2942 7.56603 7.2942 8.66104V12.8193H4.68925V4.42138H7.19031V5.56693H7.22681C7.57496 4.90712 8.4254 4.2108 9.69418 4.2108C12.3334 4.2108 12.8185 5.94878 12.8185 8.20619V12.8193H12.8157Z"
                              fill="#313545"/>
                    </svg>
                </a>
                <a href="#" class="svg-icon" aria-label="Twitter link ">
                    <svg width="15" height="13" viewBox="0 0 15 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13.3597 3.76914C13.369 3.90098 13.369 4.03284 13.369 4.16468C13.369 8.18587 10.3428 12.8192 4.81181 12.8192C3.10782 12.8192 1.52489 12.3201 0.193359 11.4537C0.435464 11.482 0.668225 11.4914 0.919645 11.4914C2.32564 11.4914 3.61994 11.0111 4.65351 10.1918C3.33129 10.1635 2.22323 9.28773 1.84146 8.0823C2.0277 8.11053 2.21392 8.12938 2.40948 8.12938C2.6795 8.12938 2.94955 8.09169 3.20094 8.0258C1.82286 7.74326 0.789263 6.51901 0.789263 5.04049V5.00283C1.18964 5.22885 1.65525 5.37011 2.14871 5.38892C1.33862 4.84271 0.807894 3.9104 0.807894 2.85565C0.807894 2.29062 0.956848 1.77266 1.21758 1.32062C2.6981 3.16643 4.92353 4.37183 7.41896 4.50369C7.37241 4.27767 7.34447 4.04226 7.34447 3.80682C7.34447 2.13052 8.68531 0.765015 10.352 0.765015C11.218 0.765015 12.0001 1.13229 12.5495 1.72558C13.2292 1.59375 13.881 1.33947 14.4584 0.991033C14.2349 1.69735 13.76 2.29064 13.1362 2.66731C13.7414 2.60142 14.328 2.43187 14.8681 2.19646C14.4584 2.79915 13.9463 3.33592 13.3597 3.76914Z"
                              fill="#313545"/>
                    </svg>

                </a>
                <a href="#" class="svg-icon" aria-label="Youtube link ">
                    <svg width="15" height="11" viewBox="0 0 15 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M14.6866 1.72113C14.514 1.04365 14.0058 0.510096 13.3604 0.329026C12.1906 0 7.5 0 7.5 0C7.5 0 2.80939 0 1.6396 0.329026C0.994234 0.510125 0.485952 1.04365 0.31344 1.72113C0 2.94909 0 5.51111 0 5.51111C0 5.51111 0 8.07314 0.31344 9.3011C0.485952 9.97858 0.994234 10.4899 1.6396 10.671C2.80939 11 7.5 11 7.5 11C7.5 11 12.1906 11 13.3604 10.671C14.0058 10.4899 14.514 9.97858 14.6866 9.3011C15 8.07314 15 5.51111 15 5.51111C15 5.51111 15 2.94909 14.6866 1.72113ZM5.9659 7.83724V3.18499L9.88634 5.51117L5.9659 7.83724Z"
                              fill="#313545"/>
                    </svg>


                </a>
            </div>
        </div>
    </div>
</footer>
<!-- General Custom Modal-->
<div hidden class="custom-modal" id="custom-modal">
    <div class="custom-modal-inner">
        <button class="close-modal" aria-label="Close Card Modal">
            <svg class="close-modal-text" x="0px" y="0px" width="20" height="20"
                 viewBox="0 0 512.001 512.001">
                <path d="M284.286,256.002L506.143,34.144c7.811-7.811,7.811-20.475,0-28.285c-7.811-7.81-20.475-7.811-28.285,0L256,227.717
L34.143,5.859c-7.811-7.811-20.475-7.811-28.285,0c-7.81,7.811-7.811,20.475,0,28.285l221.857,221.857L5.858,477.859
c-7.811,7.811-7.811,20.475,0,28.285c3.905,3.905,9.024,5.857,14.143,5.857c5.119,0,10.237-1.952,14.143-5.857L256,284.287
l221.857,221.857c3.905,3.905,9.024,5.857,14.143,5.857s10.237-1.952,14.143-5.857c7.811-7.811,7.811-20.475,0-28.285
L284.286,256.002z" fill="currentColor"/>
            </svg>
            <svg class="close-modal-video" width="20" height="20" viewBox="0 0 20 20" fill="none">
                <rect x="0.5" y="0.5" width="19" height="19" stroke="white"/>
                <line x1="1.00785" y1="0.95797" x2="18.8733" y2="18.8234" stroke="white"/>
                <line x1="18.7129" y1="1.66508" x2="0.84748" y2="19.5305" stroke="white"/>
            </svg>
            <span class="contact-form-title">Get in touch</span>
        </button>
        <div class="custom-modal-content">
        </div>
    </div>
</div>
<?= $code_before_end_of_body_tag ?>
<script type="text/javascript" id="gform_checker">
    if (document.querySelector('form[id *="gform"]')) {
        const addFocusBlurToInputs = (el) => {
            el.addEventListener("focus", () => {
                el.parentElement.parentElement.classList.add("focused");
            })
            el.addEventListener("blur", () => {
                if (!el.value) {
                    el.parentElement.parentElement.classList.remove("focused");
                }
            })
        }

        jQuery(document).on('gform_post_render', function (event, form_id, current_page) {
            console.log('asdasdasdasdasd')
            if (jQuery('.gform_validation_errors,.gfield_description')) {
                jQuery('.gform_validation_errors,.gfield_description').slideDown();
            }
            document.querySelectorAll(".ginput_container input").forEach(el => {
                addFocusBlurToInputs(el);
            })
        });
    } else {
        document.querySelector('script#gform_checker').remove();
    }

</script>
</body>
</html>

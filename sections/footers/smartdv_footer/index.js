import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";

import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";
import {SplitText} from "gsap/SplitText";


import '../../../scripts/sitesSizer/smartdv';

gsap.registerPlugin(SplitText, ScrollTrigger)


const blockScript = async (container = document) => {
  const footer = container.querySelector('footer');

  animations(footer);
  imageLazyLoading(footer);
};
windowOnLoad(blockScript);



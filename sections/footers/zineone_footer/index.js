import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/zineone';

/**
 *
 * @param container {Document}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
const footer = container.querySelector('footer');
  const footerLinks = footer.querySelectorAll('.footer-link.has-sublinks');
  
  const mobileMedia = window.matchMedia('(max-width: 600px)');
  footerLinks.forEach((menuItem) => {
    const menuItemBody = menuItem.querySelector('.footer-link-dropdown');
    menuItem?.addEventListener('click', (e) => {
      if (!mobileMedia.matches || !menuItemBody || e.target.closest('a')) return;
      const isOpened = menuItem?.classList.toggle('active-footer-link');
      if (!isOpened) {
        gsap.to(menuItemBody, {height: 0});
      } else {
        gsap.to(Array.from(footerLinks).map(otherMenuItem => {
          const otherMenuItemBody = otherMenuItem.querySelector('.footer-link-dropdown');
          if (otherMenuItemBody && menuItem !== otherMenuItem) {
            otherMenuItem?.classList.remove('active-footer-link');
            gsap.set(otherMenuItem, {zIndex: 1});
          }
          return otherMenuItemBody;
        }), {height: 0});
        gsap.set(menuItem, {zIndex: 2});
        gsap.to(menuItemBody, {height: 'auto'});
      }
    });
  });


  animations(footer);
  imageLazyLoading(footer);
};
windowOnLoad(blockScript);

import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/towSmart';

const blockScript = async (container = document) => {
  const footer = container.querySelector('footer');
  
 
  
  
  animations(footer);
  imageLazyLoading(footer);
};
windowOnLoad(blockScript);




<?php wp_footer(); ?>
<?php

$footer_logo = get_field('footer_logo', 'options');
$svg_or_image = @$footer_logo['svg_or_image'];
$logo_svg = @$footer_logo['svg'];
$logo_image = @$footer_logo['image'];

$first_column = get_field('first_column', 'options');
$second_column = get_field('second_column', 'options');
$third_column = get_field('third_column', 'options');
$fourth_column = get_field('fourth_column', 'options');

$bottom_content = get_field('bottom_content', 'options');
$footer_social_title = get_field('footer_social_title', 'options');
$copy_right_text = @$bottom_content['copy_right_text'];
$code_before_end_of_body_tag = get_field('code_before_end_of_body_tag', 'options');

?>
<!--region footer-->
<footer>
    <div class="footer-content">
        <!--     logo-->
        <div class="logo-wrapper">
            <?php if ($logo_svg || $logo_image) { ?>
                <a href="<?= site_url() . '/' ?>" class="logo">
                    <?php if (!$svg_or_image) { ?>
                        <picture>
                            <img src="<?= $logo_image['url'] ?>" alt="<?= $logo_image['alt'] ?>">
                        </picture>
                    <?php } else {
                        echo $logo_svg;
                    } ?>
                </a>
            <?php } ?>
        </div>
        <!--    footer links-->
        <div class="footer-wrapper">
            <!--  first column -->
            <div class="footer-links">
                <?php if ($first_column['link_title']) { ?>
                    <h5 class="links-title headline-5"><?= $first_column['link_title']; ?>
                    </h5>
                <?php } ?>
                <?php
                if (have_rows('first_column', 'options')) :
                    while (have_rows('first_column', 'options')) :
                        the_row();
                        ?>
                        <?php if (have_rows('footer_link')) : ?>
                        <ul class="links-wrapper">
                        <?php while (have_rows('footer_link')) : the_row();
                            $link = get_sub_field('link');
                            ?>
                            <?php if ($link) { ?>
                                <li class="link-parent">
                                    <a class="footer-link" href="<?= $link['url'] ?>"
                                       target="<?= $link['target'] ?>"><?= $link['title'] ?></a>
                                </li>
                            <?php } ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                        </ul>
                    <?php endwhile;
                endif; ?>
            </div>
            <!--  second column -->
            <div class="footer-links">
                <?php if ($second_column['link_title']) { ?>
                    <h5 class="links-title headline-5"><?= $second_column['link_title']; ?>
                    </h5>
                <?php } ?>

                <?php
                if (have_rows('second_column', 'options')) :
                    while (have_rows('second_column', 'options')) :
                        the_row();
                        ?>
                        <?php if (have_rows('footer_link')) : ?>
                        <ul class="links-wrapper">
                        <?php while (have_rows('footer_link')) : the_row();
                            $link = get_sub_field('link');
                            ?>
                            <?php if ($link) { ?>
                                <li class="link-parent">
                                    <a class="footer-link" href="<?= $link['url'] ?>"
                                       target="<?= $link['target'] ?>"><?= $link['title'] ?></a>
                                </li>
                            <?php } ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                        </ul>
                    <?php endwhile;
                endif; ?>
            </div>
            <!--  third column -->
            <div class="footer-links">
                <?php if ($third_column['link_title']) { ?>
                    <h5 class="links-title headline-5"><?= $third_column['link_title']; ?>
                    </h5>
                <?php } ?>

                <?php
                if (have_rows('third_column', 'options')) :
                    while (have_rows('third_column', 'options')) :
                        the_row();
                        ?>
                        <?php if (have_rows('footer_link')) : ?>
                        <ul class="links-wrapper">
                        <?php while (have_rows('footer_link')) : the_row();
                            $link = get_sub_field('link');
                            ?>
                            <?php if ($link) { ?>
                                <li class="link-parent">
                                    <a class="footer-link" href="<?= $link['url'] ?>"
                                       target="<?= $link['target'] ?>"><?= $link['title'] ?></a>
                                </li>
                            <?php } ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                        </ul>
                    <?php endwhile;
                endif; ?>
            </div>
            <!--fourth column -->
            <div class="footer-links">
                <?php if ($fourth_column['link_title']) { ?>
                    <h5 class="links-title headline-5"><?= $fourth_column['link_title']; ?>
                    </h5>
                <?php } ?>

                <?php
                if (have_rows('fourth_column', 'options')) :
                    while (have_rows('fourth_column', 'options')) :
                        the_row();
                        ?>
                        <?php if (have_rows('footer_link')) : ?>
                        <ul class="links-wrapper">
                        <?php while (have_rows('footer_link')) : the_row();
                            $link = get_sub_field('link');
                            ?>
                            <?php if ($link) { ?>
                                <li class="link-parent">
                                    <a class="footer-link" href="<?= $link['url'] ?>"
                                       target="<?= $link['target'] ?>"><?= $link['title'] ?></a>
                                </li>
                            <?php } ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                        </ul>
                    <?php endwhile;
                endif; ?>
            </div>

            <?php if ($footer_social_title && have_rows('footer_social', 'options')) { ?>
                <div class="footer-social">
                    <div class="footer-social-title paragraph paragraph-m-paragraph"><?= $footer_social_title ?></div>
                    <ul class="footer-social-wrapper">
                        <?php while (have_rows('footer_social', 'options')) {
                            the_row();
                            $social_url = get_sub_field('social_url');
                            $social_icon_group = get_sub_field('social_icon');
                            $select_from_saved_icons = @$social_icon_group['select_from_saved_icons'];
                            $saved_social_icon = @$social_icon_group['saved_social_icon'];
                            $social_icon = @$social_icon_group['social_icon'];
                            $linkedin = '<svg class="li" width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M24.375 0H1.81641C0.820312 0 0 0.878906 0 1.93359V24.375C0 25.4297 0.820312 26.25 1.81641 26.25H24.375C25.3711 26.25 26.25 25.4297 26.25 24.375V1.93359C26.25 0.878906 25.3711 0 24.375 0ZM7.91016 22.5H4.04297V10.0195H7.91016V22.5ZM5.97656 8.26172C4.6875 8.26172 3.69141 7.26562 3.69141 6.03516C3.69141 4.80469 4.6875 3.75 5.97656 3.75C7.20703 3.75 8.20312 4.80469 8.20312 6.03516C8.20312 7.26562 7.20703 8.26172 5.97656 8.26172ZM22.5 22.5H18.5742V16.4062C18.5742 15 18.5742 13.125 16.582 13.125C14.5312 13.125 14.2383 14.707 14.2383 16.3477V22.5H10.3711V10.0195H14.0625V11.7188H14.1211C14.6484 10.7227 15.9375 9.66797 17.8125 9.66797C21.7383 9.66797 22.5 12.3047 22.5 15.6445V22.5Z" fill="white"/>
</svg>';
                            $twitter = '<svg width="30" height="24" viewBox="0 0 30 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M26.3213 5.97594C26.3329 6.23994 26.3329 6.49194 26.3329 6.75595C26.3444 14.76 20.4639 24 9.70979 24C6.53798 24 3.42405 23.052 0.75 21.276C1.21304 21.336 1.67608 21.36 2.13911 21.36C4.76686 21.36 7.32514 20.448 9.39724 18.756C6.89683 18.708 4.6974 17.016 3.93339 14.544C4.81316 14.724 5.71608 14.688 6.5727 14.436C3.85236 13.884 1.89602 11.4 1.88444 8.50795C1.88444 8.48395 1.88444 8.45995 1.88444 8.43595C2.69476 8.90395 3.60926 9.16795 4.53534 9.19195C1.97705 7.41595 1.17831 3.87594 2.72949 1.10393C5.70451 4.89594 10.0802 7.18795 14.7801 7.43995C14.3054 5.33994 14.9537 3.13193 16.4701 1.64393C18.8201 -0.648079 22.5244 -0.528079 24.747 1.90793C26.055 1.64393 27.3168 1.13993 28.4628 0.431924C28.0229 1.83593 27.1084 3.02393 25.893 3.77994C27.0506 3.63594 28.185 3.31193 29.25 2.83193C28.4628 4.05594 27.4673 5.11194 26.3213 5.97594Z" fill="#414042"/>
              </svg>';
                            $facebook = '<svg  height="25" viewBox="0 0 512 512" width="24"
     xmlns="http://www.w3.org/2000/svg">
    <path d="m512 256c0-141.4-114.6-256-256-256s-256 114.6-256 256 114.6 256 256 256c1.5 0 3 0 4.5-.1v-199.2h-55v-64.1h55v-47.2c0-54.7 33.4-84.5 82.2-84.5 23.4 0 43.5 1.7 49.3 2.5v57.2h-33.6c-26.5 0-31.7 12.6-31.7 31.1v40.8h63.5l-8.3 64.1h-55.2v189.5c107-30.7 185.3-129.2 185.3-246.1z"/>
</svg>';
                            $instagram = '<svg height="25" viewBox="0 0 511 511.9" width="25" xmlns="http://www.w3.org/2000/svg">
    <path d="m510.949219 150.5c-1.199219-27.199219-5.597657-45.898438-11.898438-62.101562-6.5-17.199219-16.5-32.597657-29.601562-45.398438-12.800781-13-28.300781-23.101562-45.300781-29.5-16.296876-6.300781-34.898438-10.699219-62.097657-11.898438-27.402343-1.300781-36.101562-1.601562-105.601562-1.601562s-78.199219.300781-105.5 1.5c-27.199219 1.199219-45.898438 5.601562-62.097657 11.898438-17.203124 6.5-32.601562 16.5-45.402343 29.601562-13 12.800781-23.097657 28.300781-29.5 45.300781-6.300781 16.300781-10.699219 34.898438-11.898438 62.097657-1.300781 27.402343-1.601562 36.101562-1.601562 105.601562s.300781 78.199219 1.5 105.5c1.199219 27.199219 5.601562 45.898438 11.902343 62.101562 6.5 17.199219 16.597657 32.597657 29.597657 45.398438 12.800781 13 28.300781 23.101562 45.300781 29.5 16.300781 6.300781 34.898438 10.699219 62.101562 11.898438 27.296876 1.203124 36 1.5 105.5 1.5s78.199219-.296876 105.5-1.5c27.199219-1.199219 45.898438-5.597657 62.097657-11.898438 34.402343-13.300781 61.601562-40.5 74.902343-74.898438 6.296876-16.300781 10.699219-34.902343 11.898438-62.101562 1.199219-27.300781 1.5-36 1.5-105.5s-.101562-78.199219-1.300781-105.5zm-46.097657 209c-1.101562 25-5.300781 38.5-8.800781 47.5-8.601562 22.300781-26.300781 40-48.601562 48.601562-9 3.5-22.597657 7.699219-47.5 8.796876-27 1.203124-35.097657 1.5-103.398438 1.5s-76.5-.296876-103.402343-1.5c-25-1.097657-38.5-5.296876-47.5-8.796876-11.097657-4.101562-21.199219-10.601562-29.398438-19.101562-8.5-8.300781-15-18.300781-19.101562-29.398438-3.5-9-7.699219-22.601562-8.796876-47.5-1.203124-27-1.5-35.101562-1.5-103.402343s.296876-76.5 1.5-103.398438c1.097657-25 5.296876-38.5 8.796876-47.5 4.101562-11.101562 10.601562-21.199219 19.203124-29.402343 8.296876-8.5 18.296876-15 29.398438-19.097657 9-3.5 22.601562-7.699219 47.5-8.800781 27-1.199219 35.101562-1.5 103.398438-1.5 68.402343 0 76.5.300781 103.402343 1.5 25 1.101562 38.5 5.300781 47.5 8.800781 11.097657 4.097657 21.199219 10.597657 29.398438 19.097657 8.5 8.300781 15 18.300781 19.101562 29.402343 3.5 9 7.699219 22.597657 8.800781 47.5 1.199219 27 1.5 35.097657 1.5 103.398438s-.300781 76.300781-1.5 103.300781zm0 0"/>
    <path d="m256.449219 124.5c-72.597657 0-131.5 58.898438-131.5 131.5s58.902343 131.5 131.5 131.5c72.601562 0 131.5-58.898438 131.5-131.5s-58.898438-131.5-131.5-131.5zm0 216.800781c-47.097657 0-85.300781-38.199219-85.300781-85.300781s38.203124-85.300781 85.300781-85.300781c47.101562 0 85.300781 38.199219 85.300781 85.300781s-38.199219 85.300781-85.300781 85.300781zm0 0"/>
    <path d="m423.851562 119.300781c0 16.953125-13.746093 30.699219-30.703124 30.699219-16.953126 0-30.699219-13.746094-30.699219-30.699219 0-16.957031 13.746093-30.699219 30.699219-30.699219 16.957031 0 30.703124 13.742188 30.703124 30.699219zm0 0"/>
</svg>';
                            $return_icon = $select_from_saved_icons ? $$saved_social_icon : $social_icon;
                            ?>
                            <?php if ($social_url && $return_icon) { ?>
                                <li class="footer-social-li">
                                    <a href="<?= $social_url ?>" target="_blank">
                                        <?= $return_icon ?>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php
                        } ?>
                    </ul>
                </div>
            <?php } ?>

        </div>
    </div>
    <div class="copyright">
        <p class="navigation-link">
            © <?php echo date("Y"); ?>  <?= $copy_right_text ?> </p>
        <div class="copyright-content">
            <?php
            if (have_rows('bottom_content', 'options')) :
                while (have_rows('bottom_content', 'options')) :
                    the_row();
                    ?>
                    <?php if (have_rows('text_page_link')) : ?>
                    <div class="privacy ">
                    <?php while (have_rows('text_page_link')) : the_row();
                        $link = get_sub_field('link');
                        ?>
                        <?php if ($link) { ?>
                            <a class="navigation-link text-page" href="<?= $link['url'] ?>"
                               target="<?= $link['target'] ?>"><?= $link['title'] ?></a>
                        <?php } ?>
                    <?php endwhile; ?>
                <?php endif; ?>
                    </div>
                <?php endwhile;
            endif; ?>
            <a href="https://www.takeoffnyc.com/" class="company-logo navigation-link" target="_blank">
                <?= __('Site By ', 'raistone') ?> <span><?= __('TakeOff', 'raistone') ?></span>
                <!--        <img src="-->
                <? //= get_template_directory_uri() . '/front-end/src/images/takeoff.png' ?><!--" alt="">-->
            </a>
        </div>
    </div>
</footer>

<script type="text/javascript" id="gform_checker">
    if (document.querySelector('form[id *="gform"]')) {
        jQuery(document).on('gform_post_render', function (event, form_id, current_page) {
            if (jQuery('.gform_validation_errors,.gfield_description')) {
                jQuery('.gform_validation_errors,.gfield_description').slideDown();
            }
        });
    } else {
        document.querySelector('script#gform_checker').remove();
    }

</script>
<?php if ($code_before_end_of_body_tag) {
    echo $code_before_end_of_body_tag;
} ?>
</body>
</html>

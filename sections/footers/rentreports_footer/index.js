import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/wrap'

const blockScript = async (container = document) => {
  
  const blocks = container.querySelectorAll('footer');
  for (let block of blocks) {
    animations(block);
    imageLazyLoading(block);
  }
};
windowOnLoad(blockScript);




<?php wp_footer(); ?>
<?php
$footer_logo          = get_field( 'footer_logo', 'options' );
$contact_info         = get_field( 'contact_info', 'options' );
$address_mobile       = get_field( 'address_mobile', 'options' );
$phone_mobile         = get_field( 'phone_mobile', 'options' );
$connect_us_text      = get_field( 'connect_us_text', 'options' );
$facebook             = get_field( 'facebook', 'options' );
$instagram            = get_field( 'instagram', 'options' );
$youtube              = get_field( 'youtube', 'options' );
$twitter              = get_field( 'twitter', 'options' );
$enroll_cta_button    = get_field( 'enroll_cta_button', 'options' );
$enroll_text          = get_field( 'enroll_text', 'options' );
$enroll_text_cta_link = get_field( 'enroll_text_cta_link', 'options' );
$copyright_text       = get_field( 'copyright_text', 'options' );
// our promise prefooter
$title        = get_field( 'title', 'options' );
$description  = get_field( 'description', 'options' );
$promise_text = get_field( 'promise_text', 'options' );
// hide prefooter
$hide_prefooter = get_field( 'hide_prefooter', get_the_ID() );
?>
<?php if ( ! $hide_prefooter ) { ?>
  <section class="our_promise_prefooter_block" data-section-class="our_promise_prefooter_block">
    <div class="container">
      <div class="content">
		  <?php
		  $icon      = get_field( 'icon', 'options' )[0];
		  $icon_type = get_field( 'icon_type', $icon );
		  if ( $icon_type === 'svg_code' ) {
			  $icon_final = get_field( 'svg_code', $icon );
		  } else {
			  $icon_file  = get_field( 'file', $icon );
			  $icon_final = "<img src=" . $icon_file['url'] . " />";
		  }
		  ?>
        <div class="medal-icon iv-st-from-bottom-f">
			<?php if ( $icon_final ) { ?>
				<?= $icon_final; ?>
			<?php } ?>
        </div>
		  <?php if ( $title ) { ?>
            <h2 class="headline-1 iv-st-from-bottom-f"><?= $title ?></h2>
		  <?php } ?>
		  <?php if ( $description ) { ?>
            <div class="description paragraph iv-st-from-bottom-f"><?= $description ?></div>
		  <?php } ?>
		  <?php if ( $promise_text ) { ?>
            <div class="paragraph size16 iv-st-from-bottom-f"><?= $promise_text ?></div>
		  <?php } ?>
      </div>
    </div>
  </section>
<?php } ?>
<footer>
  <div class="container">
    <div class="footer-top">
      <div class="footer-contact-info">
		  <?php if ( $footer_logo ) { ?>
            <a title="Go Home" aria-label="Home" href="<?= site_url(); ?>" class="footer-logo">
              <img data-src="<?= $footer_logo['url'] ?>" alt="<?= $footer_logo['alt'] ?>">
            </a>
		  <?php } ?>
		  <?php if ( $contact_info ) { ?>
            <div class="contact-info"><?= $contact_info ?></div>
		  <?php } ?>
        <div class="contact-info contact-info-mobile">
			<?php if ( $address_mobile ) { ?>
              <p class="address"><?= $address_mobile ?></p>
			<?php } ?>
			<?php if ( $phone_mobile ) { ?>
              <a class="phone" href="<?= $phone_mobile['url'] ?>" target="<?= $phone_mobile['target'] ?>">
                <svg width="12" height="13" viewBox="0 0 12 13">
                  <path fill="#0977e9"
                        d="M11.555 1.09L9.117.529a.57.57 0 0 0-.656.328L7.336 3.481a.58.58 0 0 0 .164.656L8.93 5.31A8.798 8.798 0 0 1 4.78 9.458L3.61 8.028a.58.58 0 0 0-.656-.164L.328 8.989A.598.598 0 0 0 0 9.645l.563 2.438c.07.258.28.422.562.422C7.125 12.505 12 7.653 12 1.63c0-.258-.187-.47-.445-.54z"/>
                </svg>
				  <?= $phone_mobile['title'] ?></a>
			<?php } ?>
        </div>
        <div class="footer-social-wrapper">
			<?php if ( $connect_us_text ) { ?>
              <p class="social-title"><?= $connect_us_text ?></p>
			<?php } ?>
          <ul class="footer-social reset-ul">
			  <?php if ( $facebook ) { ?>
                <li class="social-icon">
                  <a target="_blank" aria-label="Facebook" rel="noopener" href="<?= $facebook ?>">
                    <svg width="24" height="24" viewBox="0 0 24 24">
                      <path fill="#0977e9"
                            d="M23.63 12.005C23.63 5.583 18.427.38 12.005.38S.38 5.583.38 12.005c0 5.813 4.219 10.64 9.797 11.484V15.38H7.224v-3.375h2.953V9.474c0-2.906 1.734-4.547 4.36-4.547 1.312 0 2.624.234 2.624.234v2.86h-1.453c-1.453 0-1.922.89-1.922 1.828v2.156h3.235l-.516 3.375h-2.719v8.11c5.578-.844 9.844-5.672 9.844-11.485z"/>
                    </svg>
                  </a></li>
			  <?php } ?>
			  <?php if ( $instagram ) { ?>
                <li class="social-icon">
                  <a target="_blank" aria-label="Instagram" rel="noopener" href="<?= $instagram ?>">
                    <svg width="21" height="22" viewBox="0 0 21 22">
                      <path fill="#0977e9"
                            d="M15.894 10.997c0 3-2.437 5.39-5.39 5.39-3 0-5.391-2.39-5.391-5.39 0-2.954 2.39-5.391 5.39-5.391 2.954 0 5.391 2.437 5.391 5.39zm-1.922 0a3.461 3.461 0 0 0-3.469-3.47c-1.968 0-3.515 1.548-3.515 3.47a3.51 3.51 0 0 0 3.515 3.515c1.922 0 3.47-1.547 3.47-3.515zm2.11-4.313a1.26 1.26 0 0 1-1.266-1.266 1.26 1.26 0 0 1 1.266-1.265 1.26 1.26 0 0 1 1.265 1.265 1.26 1.26 0 0 1-1.265 1.266zm4.828 8.672c-.094 1.687-.47 3.14-1.688 4.406-1.219 1.219-2.719 1.594-4.406 1.688-1.734.093-6.938.093-8.672 0-1.687-.094-3.14-.47-4.406-1.688C.519 18.497.144 17.043.05 15.356c-.093-1.734-.093-6.938 0-8.672.094-1.687.47-3.187 1.688-4.406C3.003 1.059 4.457.684 6.144.59c1.734-.093 6.938-.093 8.672 0 1.687.094 3.187.47 4.406 1.688 1.219 1.219 1.594 2.719 1.688 4.406.093 1.734.093 6.938 0 8.672zm-1.828-4.36c0-1.5.14-4.78-.422-6.187-.375-.89-1.078-1.64-1.97-1.969-1.405-.562-4.687-.422-6.187-.422-1.546 0-4.828-.14-6.187.422A3.616 3.616 0 0 0 2.3 4.81c-.562 1.405-.422 4.687-.422 6.187 0 1.546-.14 4.828.422 6.187A3.554 3.554 0 0 0 4.316 19.2c1.36.562 4.64.422 6.187.422 1.5 0 4.782.14 6.188-.422.89-.375 1.64-1.078 1.969-2.016.562-1.36.422-4.64.422-6.187z"/>
                    </svg>
                  </a></li>
			  <?php } ?>
			  <?php if ( $youtube ) { ?>
                <li class="social-icon"><a target="_blank" aria-label="Youtube" rel="noopener" href="<?= $youtube ?>">
                    <svg width="27" height="18" viewBox="0 0 27 18">
                      <path fill="#0977e9"
                            d="M26.3 9.047s0 4.172-.562 6.187a3.127 3.127 0 0 1-2.25 2.25C21.472 18 13.504 18 13.504 18s-8.016 0-10.031-.516a3.127 3.127 0 0 1-2.25-2.25C.66 13.22.66 9.047.66 9.047s0-4.219.563-6.188C1.503 1.734 2.394.844 3.473.563 5.488 0 13.502 0 13.502 0s7.97 0 9.985.563c1.078.28 1.969 1.171 2.25 2.296.563 1.97.563 6.188.563 6.188zm-8.765 0L10.879 5.25v7.594z"/>
                    </svg>
                  </a></li>
			  <?php } ?>
			  <?php if ( $twitter ) { ?>
                <li class="social-icon"><a target="_blank" aria-label="Twitter" rel="noopener" href="<?= $twitter ?>">
                    <svg width="24" height="20" viewBox="0 0 24 20">
                      <path fill="#0977e9"
                            d="M21.516 5.128c.937-.703 1.78-1.547 2.437-2.531a9.225 9.225 0 0 1-2.812.75A4.809 4.809 0 0 0 23.297.628c-.938.563-2.016.985-3.094 1.219A4.922 4.922 0 0 0 16.593.3a4.921 4.921 0 0 0-4.921 4.922c0 .375.047.75.14 1.125A14.28 14.28 0 0 1 1.641 1.19c-.422.703-.657 1.547-.657 2.484 0 1.688.844 3.188 2.204 4.078-.797-.047-1.594-.234-2.25-.61v.048c0 2.39 1.687 4.359 3.937 4.828-.375.094-.844.187-1.266.187-.328 0-.609-.047-.937-.093.61 1.968 2.437 3.375 4.594 3.421a9.896 9.896 0 0 1-6.094 2.11c-.422 0-.797-.047-1.172-.094 2.156 1.406 4.734 2.203 7.547 2.203 9.047 0 13.969-7.453 13.969-13.969v-.656z"/>
                    </svg>
                  </a></li>
			  <?php } ?>
          </ul>
        </div>
      </div>
      <div class="footer-links">
        <ul class="footer-links-wrapper reset-ul">
			<?php
			if ( have_rows( 'first_column', 'options' ) ) {
				while ( have_rows( 'first_column', 'options' ) ) {
					the_row();
					$link = get_sub_field( 'link' );
					?>
					<?php if ( $link ) { ?>
                    <li class="link-hover">
                      <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>"><?= $link['title'] ?></a></li>
					<?php } ?>
					<?php
				}
			}
			?>
        </ul>
        <ul class="footer-links-wrapper reset-ul">
			<?php
			if ( have_rows( 'second_column', 'options' ) ) {
				while ( have_rows( 'second_column', 'options' ) ) {
					the_row();
					$link = get_sub_field( 'link' );
					?>
					<?php if ( $link ) { ?>
                    <li class="link-hover">
                      <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>"><?= $link['title'] ?></a></li>
					<?php } ?>
					<?php
				}
			}
			?>
        </ul>
        <ul class="footer-links-wrapper reset-ul">
			<?php
			if ( have_rows( 'third_column', 'options' ) ) {
				while ( have_rows( 'third_column', 'options' ) ) {
					the_row();
					$link = get_sub_field( 'link' );
					?>
					<?php if ( $link ) { ?>
                    <li class="link-hover">
                      <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>"><?= $link['title'] ?></a></li>
					<?php } ?>
					<?php
				}
			}
			?>
        </ul>
      </div>
      <div class="footer-enroll">
		  <?php if ( $enroll_cta_button ) { ?>
            <a href="<?= $enroll_cta_button['url'] ?>" target="<?= $enroll_cta_button['target'] ?>" class="btn"><?= $enroll_cta_button['title'] ?></a>
		  <?php } ?>
		  <?php if ( $enroll_text ) { ?>
            <div class="enroll-text"><?= $enroll_text ?> <?php if ( $enroll_text_cta_link ) { ?>
                <a href="<?= $enroll_text_cta_link['url'] ?>" target="<?= $enroll_text_cta_link['target'] ?>" class="link"><?= $enroll_text_cta_link['title'] ?></a><?php } ?>
            </div>
		  <?php } ?>
      </div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container">
      <div class="footer-bottom-wrapper">
		  <?php if ( $copyright_text ) { ?>
            <div class="copy-right-text">
              <p><?= $copyright_text ?></p>
            </div>
		  <?php } ?>
        <div class="site-by">
          <a href="https://www.takeoffnyc.com/?utm_source=client_site&utm_medium=footer&utm_campaign=rentreporters/" target="_blank">Site
            by Takeoff</a>
        </div>
      </div>
    </div>
  </div>
</footer>
</div><!--end of page-transition -->
</main>
<div class="video-model custom-model" id="custom-model">
  <div class="custom-model-inner">
    <button class="close-model" aria-label="Close Model">
      <svg height="16" viewBox="0 0 17 16" width="17" xmlns="http://www.w3.org/2000/svg">
        <path d="M16.274 1.676L14.6 0l-6.19 6.19L2.22 0 .545 1.676l6.19 6.189-6.19 6.19L2.22 15.73l6.19-6.19 6.189 6.19 1.675-1.676-6.189-6.189z" fill="#9899a2"/>
      </svg>
    </button>
    <div class="custom-model-wrap">
    
    </div>
  </div>
</div>
</body>
</html>
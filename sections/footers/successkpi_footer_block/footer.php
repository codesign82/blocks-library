<?php wp_footer(); ?>
<?php
$footer_logo_image = get_field('footer_logo_image', 'options');
$footer_logo_svg = get_field('footer_logo_svg', 'options');
$footer_logo_png_or_svg = get_field('footer_logo_png_or_svg', 'options');
$address_title = get_field('address_title', 'options');
$phone_title = get_field('phone_title', 'options');
$phone_link = get_field('phone_link', 'options');
$email_title = get_field('email_title', 'options');
$email_link = get_field('email_link', 'options');
$first_column = get_field('first_column', 'options');
$second_column = get_field('second_column', 'options');
$copy_right_text = get_field('copy_right_text', 'options');
$hide_footer = get_field('hide_footer', get_the_ID());
?>
<!--region footer-->
<footer class="cyberHill-block logo-dark burger-dark" <?php if ($hide_footer) { ?> style="display: none;" <?php } ?>>
  <!--   top content-->
  <div class="top-content">
    <!--     logo + address + contact info -->
    <div class=left-content>
      <!--      logo  image or svg-->
      <a class="main-logo" aria-label="Home" href="<?= site_url() ?>" title="Home">
        <?php if ($footer_logo_png_or_svg === 'png') { ?>
          <picture>
            <img
              data-src="<?= $footer_logo_image['url'] ?>"
              alt="<?= $footer_logo_image['alt'] ?>">
          </picture>
        <?php } elseif ($footer_logo_png_or_svg === 'svg') { ?>
          <?= $footer_logo_svg ?>
        <?php } ?>
      </a>


      <div class="information">
        <?php if ($address_title) { ?>
          <div class="address-text"><?= $address_title ?></div>
        <?php } ?>
        <div class="contact-information">
          <div class="text-and-link">
            <?php if ($phone_title) { ?>
              <div class="footer-title"><?= $phone_title ?></div>
            <?php } ?>
            <?php if ($phone_link) { ?>
              <a class="footer-link" href="tel:<?= $phone_link['url'] ?>"><?= $phone_link['title'] ?></a>
            <?php } ?>
          </div>
          <div class="text-and-link">

            <?php if ($email_title) { ?>
              <div class="footer-title"><?= $email_title ?></div>
            <?php } ?>
            <?php if ($email_link) { ?>
              <a class="footer-link" href="mailto: <?= $email_link['url'] ?>"><?= $email_link['title'] ?></a>
            <?php } ?>

          </div>
        </div>
      </div>
    </div>

    <!--     title  + page links-->

    <div class="right-content">

      <!--  first column -->
      <div class="title-and-links">
        <?php if ($first_column['title']) { ?>
          <div class="footer-title"><?= $first_column['title']; ?></div>
        <?php } ?>
        <?php
        if (have_rows('first_column', 'options')) :
          while (have_rows('first_column', 'options')) :
            the_row();
            ?>
            <?php if (have_rows('links')) : ?>
            <?php while (have_rows('links')) : the_row();
              $link = get_sub_field('link');
              ?>
              <?php if ($link) { ?>
                <a class="footer-link" href="<?= $link['url'] ?>" target="<?= $link['target'] ?>"><?= $link['title'] ?></a>
              <?php } ?>
            <?php endwhile; ?>
          <?php endif; ?>
          <?php endwhile;
        endif; ?>
      </div>

      <!-- second column -->
      <div class="title-and-links">
        <?php if ($second_column['title']) { ?>
          <div class="footer-title"><?= $second_column['title']; ?></div>
        <?php } ?>
        <?php
        if (have_rows('second_column', 'options')) :
          while (have_rows('second_column', 'options')) :
            the_row();
            ?>
            <?php if (have_rows('links')) : ?>
            <?php while (have_rows('links')) : the_row();
              $link = get_sub_field('link');
              ?>
              <?php if ($link) { ?>
                <a class="footer-link" href="<?= $link['url'] ?>" target="<?= $link['target'] ?>"><?= $link['title'] ?></a>
              <?php } ?>
            <?php endwhile; ?>
          <?php endif; ?>
          <?php endwhile;
        endif; ?>
      </div>
    </div>
  </div>
  <!-- bottom-content  -->
  <div class="bottom-content">
    <div class="copy-right">
      <?php if ($copy_right_text) { ?>
        <div class="footer-title copy-right-text">
          &copy; <?php echo date("Y"); ?> <?= $copy_right_text ?>

          <?php if (have_rows('pages_links', 'options')) : ?>
            <?php while (have_rows('pages_links', 'options')) : the_row();
              $link = get_sub_field('link');
              ?>
              <?php if ($link) { ?>
                <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>"><?= $link['title'] ?></a>
              <?php } ?>
            <?php endwhile; ?>
          <?php endif; ?>
        </div>
      <?php } ?>
      <a href="https://www.takeoffnyc.com/" target="_blank" class="footer-title text-and-logo">
        <?= __('Website by', 'cyberhill') ?>
        <picture>
          <img src="<?= get_template_directory_uri() . '/front-end/src/images/takeoff-logo.png' ?>" alt="Website by Takeoff">
        </picture>
      </a>

    </div>
  </div>
</footer>


<!--endregion footer-->
</div><!--end of page-transition -->
</main>
</body>
</html>

import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {ScrollTrigger} from "gsap/ScrollTrigger";
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/propellerhealth';

const blockScript = async (container = document) => {
  const block = container.querySelector('.form_block');
  
  // add block code here
  

  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


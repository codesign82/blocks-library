<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'contact_form';
$className = 'contact_form';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/contact_form/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$form = get_field('form');
?>
<!-- region samrt_dv Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class='box-wrapper'>

    <div class='box-shapes-wrapper'>
        <div class='horizontal-style box-shape right'></div>
        <div class='horizontal-style box-shape left'></div>
        <div class='vertical-style box-shape top'></div>
        <div class='vertical-style box-shape bottom'></div>
    </div>
    <div class="container">
        <?php echo do_shortcode('[gravityform id="' . $form . '" ajax="true" title="false" description="false"]'); ?>
    </div>
</div>
</section>


<!-- endregion samrt_dv Block -->

import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";

import '../../../scripts/sitesSizer/propellerhealth';


const blockScript = async (container = document) => {
  const block = container.querySelector('.contact_form');

  const formInputs = block.querySelectorAll(".ginput_container input");

  formInputs.forEach(el => {
    el.addEventListener("focus", (e) => {
      el.parentElement.parentElement.classList.add("focused");
    })
    el.addEventListener("blur", (e) => {
      if (!el.value) {
        el.parentElement.parentElement.classList.remove("focused");
      }
    })
  })

  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



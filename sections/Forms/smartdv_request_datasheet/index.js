import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import $ from "jquery";
import {debounce} from "../../../scripts/functions/debounce";

import '../../../scripts/sitesSizer/propellerhealth';


const blockScript = async (container = document) => {
  const block = container.querySelector('.request_datasheet');

  const desktopSlopValue = 8.333334;
  const tabletSlopValue = 5.04;
  const mobileSlopValue = (parseFloat(window.getComputedStyle(block.querySelector(".container"), null).getPropertyValue('padding-left')) / window.innerWidth) * 100;
  ;
  let pathSlopValue = desktopSlopValue;
  const fixPaths = debounce(() => {
    pathSlopValue = window.innerWidth >= 992
        ? desktopSlopValue
        : window.innerWidth >= 600
            ? tabletSlopValue
            : mobileSlopValue;
    const path = block.querySelector('.slide-bg path')
    path.setAttribute('d', `M 0 100 L ${pathSlopValue} 0 L ${100 - pathSlopValue} 0 L 100 100 Z`);
  }, 300);

  fixPaths();
  window.addEventListener('resize', fixPaths)

  const formInputs = block.querySelectorAll(".ginput_container input");

  const addFocusBlurToInputs = (el) => {
    el.addEventListener("focus", () => {
      el.parentElement.parentElement.classList.add("focused");
    })
    el.addEventListener("blur", () => {
      if (!el.value) {
        el.parentElement.parentElement.classList.remove("focused");
      }
    })
  }

  formInputs.forEach(el => {
    addFocusBlurToInputs(el);
  })

  if (document.querySelector('form[id *="gform"]')) {
    $(document).on('gform_post_render', function () {
      block.querySelectorAll(".ginput_container input").forEach(el => {
        addFocusBlurToInputs(el);
      })
    });
  }

  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



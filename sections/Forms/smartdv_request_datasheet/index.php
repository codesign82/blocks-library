<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'request_datasheet';
$className = 'request_datasheet';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/request_datasheet/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$form = get_field('form');
?>
<!-- region samrt_dv Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="slide-bg">
    <svg preserveAspectRatio="none" viewBox="0 0 100 100">
        <path d="M -12.4 20 L 9.3 0 L 90.7 0 L 112.4 20 L 0 20 Z"></path>
    </svg>
</div>
<div class="container">
    <div class="request-datasheet-wrapper">
        <?php if ($title) { ?>
            <div class="title headline-2 word-up">
                <?= $title ?>
            </div>
        <?php } ?>
        <?php echo do_shortcode('[gravityform id="' . $form . '" ajax="true" title="false" description="false"]'); ?>
    </div>
</div>
</section>


<!-- endregion samrt_dv Block -->

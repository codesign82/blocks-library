<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'more_content_block';
$className = 'more_content_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/more_content_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$link = get_field('link');
$form = get_field('form')
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="more-content-group">
        <svg class="top-content" viewBox="0 0 332.36 211.11">
            <defs>
                <mask id="theMask8" maskUnits="userSpaceOnUse">
                    <path class="dots-animation-i-e"
                          d="M10.64,139.5s26.75,33.33,58.05,33.33,49.45-3.35,70.79-37S127,92.1,120.8,103.62s11.47,30.76,25.66,38.37,39.78,28.39,87.8-16.22c27.64-25.68,30.41-47.62,27.4-62.06"
                          transform="translate(-9.64)" fill="none" stroke="#fff" stroke-linecap="round"
                          stroke-linejoin="round"
                          stroke-width="2"/>
                </mask>
            </defs>
            <g class="circle-animation-i-e">
                <path d="M278.09,27.18,271.86,0l68.7,20.9L288.82,65.33l-8.3-31.73,56.35-12.18Z"
                      transform="translate(-9.64)"
                      fill="#2f705f"/>
                <path d="M267.65,46.35c-.89-.34-1-.89-.81-1.34l9.92-18.33c.7-.47,63.72-6.75,63.72-6.75a.93.93,0,0,1,.59.25,1,1,0,0,1,.27.58.91.91,0,0,1-.7,1l-62.52,6.21L268.44,45.8a.85.85,0,0,1-.31.39A.92.92,0,0,1,267.65,46.35Z"
                      transform="translate(-9.64)" fill="#0a0a0a"/>
                <path d="M267.89,46.48c-1-.89-.89-1.31-.6-1.57l12.76-12a1,1,0,0,1,.63-.16,1,1,0,0,1,.57.29.93.93,0,0,1,0,1.21L270.53,44.33l12.61-1.62a.89.89,0,0,1,.62.22.92.92,0,0,1,.31.58.88.88,0,0,1-.16.63.84.84,0,0,1-.53.37l-15.36,2Z"
                      transform="translate(-9.64)" fill="#0a0a0a"/>
                <path d="M278.09,28.1a.91.91,0,0,1-.89-.71L271,.21c1.12-1.07,70.37,19.85,70.37,19.85a.91.91,0,0,1,.49.35A.9.9,0,0,1,342,21a.9.9,0,0,1-.81.81l-63,6.26Zm-5-26.76,5.73,24.87,57.68-5.73Z"
                      transform="translate(-9.64)" fill="#0a0a0a"/>
                <path d="M288.82,66.25c-.87-.68-9-32.5-9-32.5C280.5,32.65,340.9,20,340.9,20a1,1,0,0,1,.58.09,1,1,0,0,1,.42.41.94.94,0,0,1,.08.57.88.88,0,0,1-.29.5L289.37,66.12A1,1,0,0,1,288.82,66.25Zm-7.12-32,7.56,29.49,48.48-41.19Z"
                      transform="translate(-9.64)" fill="#0a0a0a"/>
            </g>
            <g id="maskReveal8" mask="url(#theMask8)">
                <path d="M10.64,139.5s26.75,33.33,58.05,33.33,49.45-3.35,70.79-37S127,92.1,120.8,103.62s11.47,30.76,25.66,38.37,39.78,28.39,87.8-16.22c27.64-25.68,30.41-47.62,27.4-62.06"
                      transform="translate(-9.64)" fill="none" stroke="#000" stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2" stroke-dasharray="10 8"/>
            </g>
            <circle class="circle-animation-i-e" cx="155.16" cy="195.31" r="2.78" fill="none" stroke="#000"
                    stroke-linecap="round" stroke-linejoin="round"
                    stroke-width="1.52"/>
            <g class="circle-animation-i-e">
                <line x1="161.08" y1="203.24" x2="168.19" y2="210.35" fill="none" stroke="#000" stroke-linecap="round"
                      stroke-linejoin="round" stroke-width="1.52"/>
                <line x1="168.19" y1="203.24" x2="161.08" y2="210.35" fill="none" stroke="#000" stroke-linecap="round"
                      stroke-linejoin="round" stroke-width="1.52"/>
            </g>
        </svg>
        <svg class="bottom-content" viewBox="0 0 111.82 308.76">
            <defs>
                <mask id="theMask7" maskUnits="userSpaceOnUse">
                    <path class="dots-animation-i-e"
                          d="M53.34,1.5s114.24,68.64,19.2,154.32c0,0-45.89,25.8-55.92-3.6-10.32-30.24,32.16-24,53.76-4.8,15.07,13.4,49.68,124.8-68.88,159.84"
                          transform="translate(0)" fill="none" stroke="#fff" stroke-linecap="round"
                          stroke-miterlimit="10"
                          stroke-width="3"/>
                </mask>
            </defs>
            <g id="maskReveal7" mask="url(#theMask7)">
                <path d="M53.34,1.5s114.24,68.64,19.2,154.32c0,0-45.89,25.8-55.92-3.6-10.32-30.24,32.16-24,53.76-4.8,15.07,13.4,49.68,124.8-68.88,159.84"
                      transform="translate(0)" fill="none" stroke="#0A0A0A" stroke-linecap="round"
                      stroke-miterlimit="10"
                      stroke-width="3" stroke-dasharray="21 14"/>
            </g>
        </svg>
        <?php if ($title) { ?>
            <h2 class="headline-2 content-title iv-st-from-bottom"><?= $title ?></h2>
        <?php } ?>
        <?php if ($description) { ?>
            <div class="paragraph-xl-paragraph content-description iv-st-from-bottom"><?= $description ?></div>
        <?php } ?>
        <?php if ($form) { ?>
            <div class="input-wrapper iv-st-from-bottom">
                <?php echo do_shortcode('[gravityform id="' . $form . '" ajax="true" title="false" description="false"]'); ?>
            </div>
        <?php } ?>
    </div>

</div>
</section>


<!-- endregion raistone's Block -->

import './index.html';
import './style.scss';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';

import '../../../scripts/sitesSizer/propellerhealth';

const blockScript = async (container = document) => {
  const block = container.querySelector('.contact_us_form_block');
  
  // add block code here
  let tabs = block.querySelectorAll('.input-title');
  let tab_content = block.querySelectorAll('.form-wrapper');
  let activeTab = 0;

  tabs.forEach((tab, index) => {
    tab.addEventListener("click", () => {
      if (index === activeTab) return;
      tab_content[activeTab].classList.remove("form-active");
      tabs[activeTab].classList.remove("radio-checked");
      tab_content[index].classList.add("form-active");
      tab.classList.add("radio-checked");
      // selector.innerHTML = tab.innerHTML;
      activeTab = index;
    })
  })

  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



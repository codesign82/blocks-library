<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'text_and_form_block';
$className = 'text_and_form_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/text_and_form_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$form = get_field('form');
$form_logo = get_field('form_logo');
$form_title = get_field('form_title');
$form_description = get_field('form_description');
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="form-wrapper">
        <div class="left-content">
            <?php if ($title) { ?>
                <h2 move-to-here class="headline-3 left-title"><?= $title ?></h2>
            <?php } ?>
            <?php if ($description) { ?>
                <div class="paragraph m-paragraph">
                    <?= $description ?>
                </div>
            <?php } ?>
        </div>
        <div class="right-content">
            <?php if ($form_logo) { ?>
                <picture class="form-img">
                    <img src="<?= $form_logo['url'] ?>" alt="<?= $form_logo['alt'] ?>">
                </picture>
            <?php } ?>
            <?php if ($form_title) { ?>
                <h3 class="form-title">
                    <?= $form_title ?>
                </h3>
            <?php } ?>
            <?php if ($form_description) { ?>
                <div class="form-description paragraph m-paragraph">
                    <?= $form_description ?>
                </div>
            <?php } ?>
            <?php if ($form) { ?>
                <?php
                echo do_shortcode('[gravityform id="' . $form . '" ajax="true" title="false"  description="false"]');
                ?>
            <?php } ?>
        </div>
    </div>
</div>
</section>

<!-- endregion headversity's Block -->

import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/propellerhealth';


import {gsap} from "gsap";

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.text_and_form_block');

  // add block code here
  //Todo
  //   add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
  //   function form_submit_button( $button, $form ) {
  //     return "<button class='button gform_button' id='gform_submit_button_{$form['id']}'><span>Submit</span></button>";
  //   }


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


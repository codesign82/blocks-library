import './index.html';
import './style.scss';
import {gsap} from "gsap";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/propellerhealth';

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.subscribe_block');

  const subsTextField = block.querySelector(".subs-text-field");
  const mobileSubmit = block.querySelector(".mobile-submit");
  const textContent = block.querySelector(".text-content");
  const thanksWrapper = block.querySelector(".thanks-wrapper");
  const errorMessage = block.querySelector(".error-message");

  mobileSubmit?.addEventListener("click", function (e) {
    mobileSubmit.closest("form")?.querySelector('button[type="submit"]')?.click()
  })
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


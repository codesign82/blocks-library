<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'subscribe_block';
$className = 'subscribe_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/subscribe_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$logo = get_field('logo');
$cta_button = get_field('cta_button');
$form = get_field('form');
?>
<!-- region headversity's Block -->

<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="block-content">
    <?php if ($logo) { ?>
      <picture class="subs-logo iv-st-from-bottom">
        <?= get_acf_image($logo, 'img-106-97') ?>
      </picture>
    <?php } ?>
    <div class="thanks-wrapper">
      <div class="headline-4 thanks-message">
        <?= __(' Thank you!' , 'headversity') ?>
      </div>
      <div class="paragraph thanks-desc">
        <?= __('Your information has been submitted.' , 'headversity') ?>
      </div>

    </div>
    <div class="text-content ">
      <?php if ($title) { ?>
        <h2 move-to-here class="title headline-4 iv-st-from-bottom"> <?= $title ?></h2>
      <?php } ?>
      <?php if ($description) { ?>
        <div class="description paragraph iv-st-from-bottom">
          <?= $description ?>
        </div>
      <?php } ?>
      <div class="subs-form iv-st-from-bottom">
        <?php echo do_shortcode('[gravityform id="' . $form . '" ajax="true" title="false" description="false"]'); ?>

      </div>
    </div>
  </div>
</div>
</section>


<!-- endregion headversity's Block -->

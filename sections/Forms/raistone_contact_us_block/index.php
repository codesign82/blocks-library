<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'contact_us_block';
$className = 'contact_us_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/contact_us_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$top_image = get_field('top_image');
$bottom_image = get_field('bottom_image');
$form_title = get_field('form_title');
$form = get_field('form');
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="contact-wrapper">
        <div class="left-content">
            <?php if ($title) { ?>
                <h2 class="headline-2 contact-title iv-st-from-bottom"><?= $title ?></h2>
            <?php } ?>
            <?php if ($description) { ?>
                <div class="paragraph-xl-paragraph contact-description iv-st-from-bottom">
                    <?= $description ?>
                </div>
            <?php } ?>
        </div>
        <div class="right-content iv-st-from-bottom">
            <?php if ($top_image) { ?>
                <picture class="contact-img">
                    <img src="<?= $top_image['url'] ?>" alt="<?= $top_image['alt'] ?>">
                </picture>
            <?php } ?>
            <div class="form-box">
                <?php if ($form_title) { ?>
                    <h3 class="form-text"><?= $form_title ?></h3>
                <?php } ?>
                <?php if ($form) { ?>
                    <?php echo do_shortcode('[gravityform id="' . $form . '" ajax="true" title="false" description="false"]'); ?>
                <?php } ?>
            </div>
            <?php if ($bottom_image) { ?>
                <picture class="form-img">
                    <img src="<?= $bottom_image['url'] ?>" alt="<?= $bottom_image['alt'] ?>">
                </picture>
            <?php } ?>
        </div>
    </div>
</div>
</section>


<!-- endregion raistone's Block -->

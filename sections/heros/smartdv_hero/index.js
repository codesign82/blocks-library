import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";

import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";
import {SplitText} from "gsap/SplitText";


import '../../../scripts/sitesSizer/smartdv';

gsap.registerPlugin(SplitText, ScrollTrigger)


const blockScript = async (container = document) => {
  const block = container.querySelector('.hero_block');

  const topShape = block.querySelector(".right-content svg path:last-of-type")
  const centerShape = block.querySelector(".right-content svg path:nth-of-type(2)")
  const heroTitle = block.querySelector(".left-content .hero-title")
  const heroDescription = block.querySelector(".left-content .hero-description")
  const scrollLine = block.querySelector(".scroll-line")
  const contentWrapper = block.querySelector(".content-wrapper")


    new SplitText(heroTitle, {
      type: 'words',
      wordsClass: 'word-overflow',
    });
    let splitWords = new SplitText(heroTitle, {
      type: 'words',
      wordsClass: 'child-word',
    });
    gsap.set(centerShape, {
      scale: 1.1,
      opacity: .3,
      transformOrigin: "center",
    })
    gsap.timeline()
        .from(splitWords.words, {
          duration: 1.5,
          opacity: 0,
          yPercent: 120,
          rotate: 15,
          transformOrigin: "left top",
          stagger: 0.1,
          ease: "power3.out",
        })
        .from(heroDescription, {
          y: 40,
          opacity: 0,
          ease: "power1.out",
        }, "<70%")
        .call(() => {
          heroTitle.classList.add('active')
        }, [], "<90%")

    if (!block.classList.contains("theme2")) {
      gsap.set(topShape, {
        y: -145,
        x: 116,
        scale: .928,
        transformOrigin: "center",
      })
      const tl = gsap.timeline({
        delay: .5, onComplete: () => {
          gsap.timeline({
            scrollTrigger: {
              trigger: block.querySelector(".left-content"),
              scrub: 1.5,
              start: "0% 10%",
              end: "200% center",
            }
          })
              .to(topShape, {
                y: 0,
                x: 0,
                scale: 1,
              })
              .to(centerShape, {
                scale: 1,
                opacity: 1,
                transformOrigin: "center",
              }, "<")
        }
      })
          .to(topShape, {
            y: 0,
            x: 0,
            scale: .9,
            duration: 2,
            ease: "power3.in"
          })
          .to(centerShape, {
            scale: .928,
            opacity: 1,
            duration: 2,
            transformOrigin: "center",
            ease: "power3.in"
          }, "<")
          .set(centerShape, {
            fill: "#fff"
          })
          .to(centerShape, {
            scale: 1.5,
            opacity: 0,
            transformOrigin: "center",
          })
          .set(centerShape, {
            scale: 1,
            opacity: 1,
          })
          .to(centerShape, {
            scale: 1.5,
            opacity: 0,
            transformOrigin: "center",
          }, ">.5")
          .set(centerShape, {
            scale: 1,
            opacity: 1,
          })
          .to(centerShape, {
            scale: 1.5,
            opacity: 0,
            transformOrigin: "center",
          }, ">.5")
          .to(topShape, {
            scale: 1,
            ease: "power2"
          }, "2")
          .call(() => {
            scrollLine.classList.add('active')
          }, [], "<")
          .set(centerShape, {
            fill: "#064952"
          })
          .to(topShape, {
            y: -145,
            x: 116,
            scale: .928,
            duration: 2,
            ease: "power2.inOut"

          }, ">.5")
          .fromTo(centerShape, {
            scale: 1,
            transformOrigin: "center",
            opacity: .7,
          }, {
            scale: .928,
            duration: 2,
            transformOrigin: "center",
            ease: "power2.inOut"
          }, "<")

    }

    // if (block.nextElementSibling.classList.contains("icon_with_a_text_block")){
    // gsap.to(block, {
    //   y:contentWrapper.clientHeight + 70,
    //   opacity:0,
    //   scrollTrigger:{
    //     trigger:block,
    //     scrub: true,
    //     start:"top top",
    //     end: "+=500",
    //   }
    // })
    // }

    console.log("Working ????")
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'homepage_hero';
$className = 'homepage_hero';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
$choose_your_theme = get_field('choose_your_theme');
if ($choose_your_theme === 'hero-1') {
    $className .= ' hero-version-1 ';
} elseif ($choose_your_theme === 'hero-2') {
    $className .= ' hero-version-2 ';
} elseif ($choose_your_theme === 'hero-3') {
    $className .= ' hero-version-3 ';
} elseif ($choose_your_theme === 'hero-4') {
    $className .= ' hero-version-4';
} elseif ($choose_your_theme === 'hero-5') {
    $className .= ' hero-version-5';
}


if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/homepage_hero/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$subtitle = get_field('subtitle');
$title = get_field('title');
$description = get_field('description');
$position = get_field('position');
$cta_button = get_field('cta_button');
$background_color = get_field('background_color');
$image = get_field('image');
$manually_or_automatically = get_field('manually_or_automatically' , get_the_ID());
$swap_button_direction = get_field('swap_button_direction');
$title_large = get_field('title_large');
$current_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if (isset($_SERVER['HTTP_REFERER'])) {
    $back_url = $_SERVER['HTTP_REFERER'];
}
//$current_page_name =basename($_SERVER['REQUEST_URI']);
$current_page_name = get_the_title();
//$url_name = explode( '/', $_SERVER['REQUEST_URI'] );
//$back_url_name = $url_name[1];

$title_wide = get_field('title_wide');

global $post;
$parent_page_title = get_the_title(str_replace('_',' ' , $post->post_parent));
$parent_page_link = get_permalink($post->post_parent);
$chain_links = get_field('chain_links', $post);
?>

<style>
    <?php if($choose_your_theme === 'hero-2'){ ?>
    <?php if($background_color){ ?>
    <?='#'.$id?>{
        background-color: <?=$background_color?>;
        color: <?=$background_color?> ;
    }
    <?php } }?>
</style>
<!-- region propellerhealth Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<?php if ($choose_your_theme === 'hero-1') { ?>
    <svg class="dots-animation-i-e site-dots" fill="none">
        <path class="dot-line-animation" d="" stroke="#90E2C8" stroke-width="3" stroke-linecap="round" stroke-dasharray="1 10 1 10"/>
    </svg>
<?php } ?>
<div class='hero-wrapper-content'>
    <div class="container">
        <!--region version-1-->
        <?php if ($choose_your_theme === 'hero-1') {   ?>
            <div>
                <div class="wrapper-one">
                    <div class="left-side text-animation">
                        <?php if ($title) { ?>
                            <h1 class="headline-1 title-1 "><?= $title ?></h1>
                        <?php } ?>
                        <?php if ($description) { ?>
                            <div class="headline-4 description-1 "><?= $description ?></div>
                        <?php } ?>
                        <?php if ($cta_button) { ?>
                            <a href="<?= $cta_button['url'] ?>"
                               target="<?= $cta_button['target'] ?>"
                               class="cta-button "><?= $cta_button['title'] ?></a>
                        <?php } ?>
                    </div>
                    <div class="right-side image-animation">
                        <?php if ($image) { ?>
                            <div class="moving-shape  ">
                                <picture class="aspect-ratio image-wrapper ">
                                    <?= get_acf_image( $image, 'img-758-710' ) ?>
                                </picture>
                            </div>
                        <?php } ?>
                    </div>

                </div>
            </div>
        <?php } ?>
        <!--endregion version-1-->

        <!--region version-2-->
        <?php if ($choose_your_theme === 'hero-2') { ?>
            <div>
                <div class="content-two">
                    <div class="text-content text-animation">
                        <?php if ($subtitle) { ?>
                            <div class="header-nav-title sub-title-2 "><?= $subtitle ?></div>
                        <?php } ?>
                        <?php if ($title) { ?>
                            <div class="headline-2 title-2  "><?= $title ?></div>
                        <?php } ?>
                        <?php if ($cta_button) { ?>
                            <a href="<?= $cta_button['url'] ?>"
                               target="<?= $cta_button['target'] ?>"
                               class="cta-button white-cta "><?= $cta_button['title'] ?></a>
                        <?php } ?>
                    </div>
                    <svg class="hero-shape">
                        <defs>
                            <clipPath id="hero-shape" clipPathUnits="objectBoundingBox">
                                <path d="M 1 0 L 1 0.899 C 0.895 0.963 0.774 0.998 0.65 0.999 C 0.291 0.999 -0.001 0.659 0 0.3 C 0 0.195 0.028 0.091 0.08 0 L 1 0 Z"/>
                            </clipPath>
                        </defs>
                    </svg>
                    <?php if ($image) { ?>
                        <picture class="img-left image-animation">
                            <?= get_acf_image( $image, 'img-705-695' ) ?>
                        </picture>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
        <!--endregion version-2-->

        <!--region version-3-->
        <?php if ($choose_your_theme === 'hero-3') { ?>

            <?php if($manually_or_automatically === true){
                if (have_rows('chain_links' , $post)){
                    ?>
                    <div class="chain-links">
                        <?php while (have_rows('chain_links', $post)){
                            the_row();
                            $link = get_sub_field('link' , $post);
                            ?>
                            <a href="<?= $link['url'] ?>"
                               target="<?= $link['target'] ?>"
                               class="cta-link parent-link"><?= $link['title'] ?>
                                <svg class="arrow-right" width="9" height="13"
                                     viewBox="0 0 9 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 12L7 6.5L1 1" stroke="#0075E3" stroke-width="2"/>
                                </svg>
                            </a>
                        <?php } ?>
                    </div>
                <?php }
            }
            elseif($manually_or_automatically === false){?>
                <?php if ($parent_page_title != $current_page_name ): ?>
                    <div class="chain-links">
                        <a href="<?= $parent_page_link ?>" class="cta-link parent-link"><?= $parent_page_title ?>
                            <svg class="arrow-right" width="9" height="13"
                                 viewBox="0 0 9 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 12L7 6.5L1 1" stroke="#0075E3" stroke-width="2"/>
                            </svg>
                        </a>
                        <a href="<?= $current_link ?>" class="cta-link fw-bold sub-link"><?= $current_page_name ?></a>
                    </div>
                <?php endif; ?>
            <?php  }?>

            <div class="content-three">
                <div class="left-side image-animation">
                    <?php if ($image) { ?>
                        <picture class="aspect-ratio">
                            <?= get_acf_image( $image, 'img-475-600' ) ?>
                        </picture>
                    <?php } ?>
                </div>
                <div class="right-side text-animation">
                    <?php if ($title) { ?>
                        <div class="headline-2 title-3"><?= $title?></div>
                    <?php } ?>
                    <?php if($position){?>
                        <div class="headline-4 sub-text-3 "><?= $position?></div>
                    <?php } ?>
                    <?php if ($description) { ?>
                        <div class="paragraph  text-3"><?= $description ?></div>
                    <?php } ?>
                    <?php if ($cta_button) { ?>
                        <a href="<?= $cta_button['url'] ?>"
                           target="<?= $cta_button['target'] ?>"
                           class="cta-button"><?= $cta_button['title'] ?></a>
                    <?php } ?>
                </div>
            </div>

        <?php } ?>
        <!--endregion version-3-->

        <!--region version-4-->
        <?php if ($choose_your_theme === 'hero-4') { ?>

            <div class="content-four">
                <div class="left-side text-animation">
                    <?php if($manually_or_automatically === true){
                        if (have_rows('chain_links' , $post)){
                            ?>
                            <div class="chain-links">
                                <?php while (have_rows('chain_links', $post)){
                                    the_row();
                                    $link = get_sub_field('link' , $post);
                                    ?>
                                    <a href="<?= $link['url'] ?>"
                                       target="<?= $link['target'] ?>"
                                       class="cta-link parent-link"><?= $link['title'] ?>
                                        <svg class="arrow-right" width="9" height="13"
                                             viewBox="0 0 9 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1 12L7 6.5L1 1" stroke="#0075E3" stroke-width="2"/>
                                        </svg>
                                    </a>
                                <?php } ?>
                            </div>
                        <?php }
                    }
                    elseif($manually_or_automatically === false){?>
                        <?php if ($parent_page_title != $current_page_name ): ?>
                            <div class="chain-links">
                                <a href="<?= $parent_page_link ?>" class="cta-link parent-link"><?= $parent_page_title ?>
                                    <svg class="arrow-right" width="9" height="13"
                                         viewBox="0 0 9 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1 12L7 6.5L1 1" stroke="#0075E3" stroke-width="2"/>
                                    </svg>
                                </a>
                                <a href="<?= $current_link ?>" class="cta-link fw-bold sub-link"><?= $current_page_name ?></a>
                            </div>
                        <?php endif; ?>
                    <?php  }?>
                    <?php if ($image) { ?>
                        <picture class="aspect-ratio">
                            <?= get_acf_image( $image, 'img-683-600' ) ?>
                        </picture>
                    <?php } ?>
                </div>
                <div class="right-side text-animation">
                    <?php if ($subtitle) { ?>
                        <div class="header-nav-title sub-title-4"><?= $subtitle ?></div>
                    <?php } ?>
                    <?php if ($title) { ?>
                        <div class="headline-2 title-4"><?= $title ?></div>
                    <?php } ?>
                    <?php if ($description) { ?>
                        <div class="headline-4 text-4">
                            <?= $description ?>
                        </div>
                    <?php } ?>
                    <?php if ($cta_button) { ?>
                        <a href="<?= $cta_button['url'] ?>"
                           target="<?= $cta_button['target'] ?>"
                           class="cta-button"><?= $cta_button['title'] ?></a>
                    <?php } ?>
                </div>
            </div>

        <?php } ?>
        <!--endregion version-4-->

        <!--region version-5-->
        <?php if ($choose_your_theme === 'hero-5') { ?>

            <?php if($manually_or_automatically === true){
                if (have_rows('chain_links' , $post)){
                    ?>
                    <div class="chain-links">
                        <?php while (have_rows('chain_links', $post)){
                            the_row();
                            $link = get_sub_field('link' , $post);
                            ?>
                            <a href="<?= $link['url'] ?>"
                               target="<?= $link['target'] ?>"
                               class="cta-link parent-link"><?= $link['title'] ?>
                                <svg class="arrow-right" width="9" height="13"
                                     viewBox="0 0 9 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 12L7 6.5L1 1" stroke="#0075E3" stroke-width="2"/>
                                </svg>
                            </a>
                        <?php } ?>
                    </div>
                <?php }
            }
            elseif($manually_or_automatically === false){?>
                <?php if ($parent_page_title != $current_page_name ): ?>
                    <div class="chain-links">
                        <a href="<?= $parent_page_link ?>" class="cta-link parent-link"><?= $parent_page_title ?>
                            <svg class="arrow-right" width="9" height="13"
                                 viewBox="0 0 9 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 12L7 6.5L1 1" stroke="#0075E3" stroke-width="2"/>
                            </svg>
                        </a>
                        <a href="<?= $current_link ?>" class="cta-link fw-bold sub-link"><?= $current_page_name ?></a>
                    </div>
                <?php endif; ?>
            <?php  }?>

            <div class="content-five text-animation">
                <?php if ($title) { ?>
                    <div class="headline-2 title-5 <?= $title_large? 'title-large':''?>  <?= $title_wide? 'title-wide':''?>"><?= $title ?></div>
                <?php } ?>
                <div class="text-and-cta <?= $swap_button_direction? ' order-button ':' '?> text-animation ">
                    <?php if ($description) { ?>
                        <div class="paragraph text-5"><?= $description ?></div>
                    <?php } ?>
                    <?php if ($cta_button) { ?>
                        <a href="<?= $cta_button['url'] ?>"
                           target="<?= $cta_button['target'] ?>"
                           class="cta-button"><?= $cta_button['title'] ?></a>
                    <?php } ?>
                </div>
            </div>

        <?php } ?>
        <!--endregion version-5-->
    </div>
</div>
</section>


<!-- endregion propellerhealth Block -->


<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'hero_block';
$className = 'hero_block ';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/hero_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 *
 *
 *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$hero_animation = get_field('hero_animation');
$animation_timescale = @$hero_animation['animation_timescale'];
$autoplay_duration = @$hero_animation['autoplay_duration'];
//$select_hero_constellations = get_field( 'select_hero_constellations' );
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="hero-content" data-animation-timescale="<?= $animation_timescale ?>"
         data-autoplay-duration="<?= $autoplay_duration ?>">
        <?php if (have_rows('hero_content')) { ?>
            <div class="swiper-container ">
                <div class="swiper-wrapper">
                    <?php while (have_rows('hero_content')) {
                        the_row();
                        $title = get_sub_field('title');
                        $description = get_sub_field('description');
                        $cta_button = get_sub_field('cta_button');
                        $link = get_sub_field('link');
                        $image = get_sub_field('image');
                        ?>
                        <div class="swiper-slide">
                            <div class="left-content">
                                <?php if ($title) { ?>
                                    <h2 class="headline-1 title"><?= $title ?></h2>
                                <?php } ?>
                                <?php if ($description) { ?>
                                    <div class="paragraph description "><?= $description ?></div>
                                <?php } ?>
                                <div class="cta-button-and-link ">
                                    <?php if ($cta_button) { ?>
                                        <a href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>"
                                           class="btn has-arrow cta-button"> <?= $cta_button['title'] ?>
                                            <svg width="9" height="14" viewBox="0 0 9 14" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path d="M1.03906 13L7.03906 7L1.03906 1" stroke="#0A0A0A"
                                                      stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                            </svg>
                                        </a>
                                    <?php } ?>
                                    <?php if ($link) { ?>
                                        <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>"
                                           class="link link-white "><?= $link['title'] ?></a>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php if ($image) { ?>
                                <div class="right-content">


                                    <img class="image-wrapper " src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                                </div>
                            <?php } ?>
                        </div>

                    <?php } ?>
                </div>
            </div>
        <?php } ?>
        <svg class="hero-logo" width="669" height="669" viewBox="0 0 669 669" fill="none">
            <path d="M0 334.463C0 468.963 79.4689 584.974 193.976 638.058V30.9414C79.4689 84.0259 0 199.962 0 334.463Z"
                  fill="#E9E9E9" fill-opacity="0.3"/>
            <path d="M453.703 317.836C471.818 309.784 489.263 300.315 505.812 289.43C566.793 249.318 596.464 203.764 596.464 150.232C596.464 140.987 595.495 131.966 593.631 123.168C532.277 47.9401 439.017 0 334.425 0C314.67 0 295.361 1.78936 276.575 5.06987V228.89C293.721 219.421 313.477 214.053 334.425 214.053C395.182 214.053 445.503 259.309 453.703 317.836Z"
                  fill="#E9E9E9" fill-opacity="0.3"/>
            <path d="M276.575 663.93C295.361 667.211 314.67 669 334.425 669C397.27 669 455.939 651.628 506.185 621.507L276.575 463.148V663.93Z"
                  fill="#E9E9E9" fill-opacity="0.3"/>
            <path d="M553.151 337.219C517.293 359.587 480.019 376.958 441.552 389.484C428.208 415.281 405.843 435.709 378.633 446.445L565.601 576.1C629.266 515.187 669 429.446 669 334.461C669 301.208 664.08 269.149 655.059 238.879C634.26 274.964 600.266 307.844 553.151 337.219Z"
                  fill="#E9E9E9" fill-opacity="0.3"/>
        </svg>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
    </div>
</div>

</section>


<!-- endregion LoftySky's Block -->

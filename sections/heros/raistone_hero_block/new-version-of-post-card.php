<?php
$post_id = @$args['post_id'];
$post_type = @$args['post_type'];
$thumbnail_id = get_post_thumbnail_id();
$alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);
$excerpt = get_the_excerpt($post_id);

?>
<div id="post-id-<?= $post_id; ?>" class="swiper-slide iv-st-from-bottom">
  <div class="swiper-slide-content card">
    <a class="image-wrapper aspect-ratio" href="<?= get_the_permalink($post_id); ?>">
      <picture>
        <img src="<?= get_the_post_thumbnail_url($post_id) ?>" alt="<?= $alt ?>">
      </picture>
    </a>
    <div class="card-content">
      <a class="headline-4 card-title" href="<?= get_the_permalink($post_id); ?>">
        <?= get_the_title($post_id) ?>
      </a>
      <?php if (has_excerpt($post_id)) { ?>
        <div class="paragraph paragraph-m-paragraph description"><?= $excerpt ?></div>
      <?php } ?>
      <a href="<?= get_the_permalink($post_id); ?>" class="btn-arrow">
        <?= __('Read Now', 'raistone') ?>
        <svg width="9" height="14" viewBox="0 0 9 14" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M1.03906 13L7.03906 7L1.03906 1" stroke="#0A0A0A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>
      </a>
    </div>
  </div>
</div>



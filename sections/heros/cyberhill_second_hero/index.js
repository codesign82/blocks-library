import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/cyberhill';
import {ScrollToPlugin} from "gsap/ScrollToPlugin";


gsap.registerPlugin(ScrollToPlugin);
const blockScript = async (container = document) => {
  const block = container.querySelector('.second_hero_block');
  // add block code here
  
  block.querySelector('.scroll-down')?.addEventListener('click', () => {
    // window.scrollTo({
    //   top: block.offsetHeight,
    //   behavior: 'smooth'
    // })
    gsap.to(window, {duration: 3, scrollTo: block.offsetHeight });
  })
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



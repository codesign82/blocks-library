<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$choose_type_of_hero = get_field('choose_type_of_hero');

$dataClass = 'second_hero_block';
$className = 'second_hero_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}


if ($choose_type_of_hero === 'first_type') {
  $className .= ' background-color-and-image ';
}
if ($choose_type_of_hero === 'second_type') {
  $className .= ' full-background-image ';
}
if ($choose_type_of_hero === 'third_type') {
  $className .= ' only-image ';
}

if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/second_hero_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$choose_back_ground_color = get_field('choose_back_ground_color');
$hero_height = get_field('hero_height');
$second_image = get_field('second_image');
$has_second_image = get_field('has_second_image');
$title = get_field('title');
$sub_title = get_field('sub_title');
$description = get_field('description');
$link = get_field('link');
$link_text = get_field('link_text');
$scroll_down = get_field('scroll_down');
$white = $choose_back_ground_color === 'white';
$blue = $choose_back_ground_color === 'blue';
$grey = $choose_back_ground_color === 'grey';
$back_ground_hero_image = get_field('back_ground_hero_image');
$default_background_pattern = get_field('default_background_pattern', 'options');
?>

<style>
  @media screen and (min-width: 600px) {
  <?='#'.$id?> .hero-wrapper {
    height: <?=$hero_height?>vh !important;
  }
  }

  <?php if (!$back_ground_hero_image && !$default_background_pattern) { ?>
  <?='#'.$id?>
  .hero-content {
    flex-basis: 100%;
    width: 100%;
  }

  @media screen and (max-width: 600px) {
  <?='#'.$id?> .hero-content {
    padding-top: 18rem;
  }
  }

  <?php } ?>
</style>


<!-- region CyberHill's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="hero-wrapper">
  <div class="hero-content <?= $choose_back_ground_color ?>"
    <?php if (!$back_ground_hero_image && !$default_background_pattern) { ?>
      style="
      flex-basis: 100%;
      width: 100%;
        "
    <?php } ?>>
    <?php if ($title) { ?>
      <div class="title-with-shape iv-st-from-bottom"><?= $title ?></div>
    <?php } ?>
    <?php if ($sub_title) { ?>
      <h2 class="headline-2 iv-st-from-bottom"><?= $sub_title ?></h2>
    <?php } ?>
    <?php if ($description) { ?>
      <div class="paragraph iv-st-from-bottom"><?= $description ?></div>
    <?php } ?>
    <?php if ($link) { ?>
      <a class="link-and-arrow iv-st-from-bottom" href="<?= $link['url'] ?>" target="<?= $link['target'] ?>">
        <?= $link['title'] ?>
        <svg class="arrow" viewBox="0 0 10 16" fill="none"
             xmlns="http://www.w3.org/2000/svg">
          <path
            d="M9.50003 8L2.00003 15.5L0.950027 14.45L7.40003 8L0.950027 1.55L2.00003 0.5L9.50003 8Z"
            fill="#1D4070"/>
        </svg>
      </a>
    <?php } ?>
    <?php if ($scroll_down) { ?>
      <div class="scroll-down">
        <div class="down-icon">
          <svg class="arrow-animation" width="14" height="24" viewBox="0 0 14 24" fill="none"
               xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd"
                  d="M13.2058 15.9653L7.60735 21.7318V-1.52588e-05H6.48433V21.8263L0.794195 15.9653L0 16.7829L6.20602 23.1752L6.1991 23.1824L6.99329 24L7.00001 23.9931L7.00673 24L7.80093 23.1824L7.794 23.1752L14 16.7829L13.2058 15.9653Z"
                  fill="#1D4070"/>
          </svg>
        </div>
        <p class="small-text small-text-14">Scroll Down</p>
      </div>
    <?php } ?>
  </div>
  <?php if ($back_ground_hero_image) : ?>
    <picture class="background-image" data-reveal-direction="right">
      <img src="<?= $back_ground_hero_image['url'] ?>" alt="<?= $back_ground_hero_image['alt'] ?>">
    </picture>
  <?php elseif ($default_background_pattern) : ?>
    <picture class="background-image" data-reveal-direction="right">
      <img src="<?= $default_background_pattern['url'] ?>" alt="<?= $default_background_pattern['alt'] ?>">
    </picture>
  <?php endif; ?>
</div>
<?php if ($has_second_image === true) { ?>
  <div class="image-wrapper" data-reveal-direction="left">
    <picture class="aspect-ratio">
      <img src="<?= $second_image['url'] ?>" alt="<?= $second_image['alt'] ?>">
    </picture>
  </div>
<?php } ?>


</section>


<!-- endregion CyberHill's Block -->

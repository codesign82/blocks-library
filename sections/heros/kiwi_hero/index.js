import './index.html';
import './style.scss';
// import {debounce} from 'lodash';
import {gsap} from "gsap";
import shaders from "./_ignore_shaders";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/wrap';
import DrawSVGPlugin from 'gsap/DrawSVGPlugin';
import ScrambleTextPlugin from 'gsap/ScrambleTextPlugin';
import {setFormFunctionality} from '../../../scripts/general/set-form-functionality';
import {debounce} from "../../../scripts/functions/debounce";
import {isMobile} from "../../../scripts/functions/isMobile";

import {
  LoadingManager,
  Mesh,
  Object3D,
  PerspectiveCamera,
  PlaneGeometry,
  Scene,
  ShaderMaterial,
  TextureLoader,
  Vector2,
  WebGLRenderer
} from 'three'
gsap.registerPlugin(DrawSVGPlugin);
gsap.registerPlugin(ScrambleTextPlugin);
let NODE_ENV_ADMIN;
const isAdmin = NODE_ENV_ADMIN;
const blockScript = async (container = document) => {
  
  const blocks = container.querySelectorAll('.block-hero-journey');
  
  for (let block of blocks) {
    !isAdmin && setTimeout(() => initSlider({block}), 100);
    setFormFunctionality(block)
  }
  window.dispatchEvent(new Event('heroLoaded'));
};

gsap.registerPlugin(DrawSVGPlugin);

const initSlider = ({block,}) => {
  
  const threeJsObj = {};
  const props = {
    perVisible: 0,
    percentCoef: 0,
    deformation: 0,
    paused: true,
    stopped: true,
    firstRun: true,
    timeOut: null,
    currentIndex: null,
    currentPath: 0,
  };
  
  block.progressPaths = Array.from(block.querySelectorAll('.shuffle-btn-progress path'));
  const progressAnimation = path => gsap.fromTo(path, {drawSVG: '0 0'}, {
    drawSVG: '0 100%',
    duration: 10,
    paused: true,
    ease: 'linear'
  });
  
  block.progressPathsAnimation = block.progressPaths.map(path => progressAnimation(path))
  
  initiateThree({threeJsObj, props, block})
      .then(() => {
        props.totalImages = threeJsObj.planes.length;
        resize(threeJsObj);
        render({props, data: {threeJsObj, block}})
        window.addEventListener('resize', debounce(() => resize(threeJsObj), 251));
        setupEvents({props, data: {threeJsObj, block}})
      })
      .catch(console.log);
}

const initiateThree = ({threeJsObj, props, block}) => {
  
  threeJsObj.object3D = new Object3D();
  threeJsObj.scene = new Scene();
  threeJsObj.camera = new PerspectiveCamera(45, 1, 0.1, 4e3);
  threeJsObj.camera.position.z = 100;
  threeJsObj.camera.rotation.x = 2 * Math.PI
  threeJsObj.renderer = new WebGLRenderer({alpha: !0, antialias: !0});
  // threeJsObj.renderer.outputEncoding = sRGBEncoding
  threeJsObj.renderer.setPixelRatio(Math.min(1, window.devicePixelRatio || 1));
  threeJsObj.renderer.setSize(window.innerWidth, window.innerHeight);
  threeJsObj.planes = [];
  threeJsObj.renderer.domElement.classList.add('image');
  block.appendChild(threeJsObj.renderer.domElement);
  
  
  const loader = new TextureLoader(new LoadingManager(() => console.log('Texture Added')));
  
  const promises = [];
  const covers = Array.from(block.querySelector('.kiwi-hero-images-data').children).map(e => e.dataset);
  const image = block.querySelector('img.image');
  
  covers.unshift({img:image.getAttribute('src'),...image.dataset})
  
  
  
  let index = 0;
  const totalImages = covers.length;
  for (let cover of covers) {
    let promise = new Promise(resolve => {
      const plane = {
        index: ++index, //fixme don't know what is this
        color: cover.color,
        title: cover.title,
        subtitle: cover.subtitle,
        texture: loader.load(cover.img, (texture) => {
          // texture.encoding = sRGBEncoding;
          // texture.center.x=texture.image.width/2;
          // texture.center.y=texture.image.height/2;
          // texture.offset.x=0.5;
          // texture.offset.y=0.5;
          resolve()
          const mesh = new Mesh(
              new PlaneGeometry(1, 1, 40, 40),
              new ShaderMaterial({
                uniforms: {
                  resolution: {value: new Vector2(0, 0)},
                  deformation: {value: 0},
                  perVisible: {value: props.perVisible},
                  time: {value: 0},
                  shapeIndex: {value: Math.floor(totalImages * Math.random())},
                  texture_self: {value: texture},
                  texture_size: {value: {x: texture.image.width, y: texture.image.height}},
                  textureImSize: {value: {x: texture.image.width, y: texture.image.height}},
                  texture_alpha: {value: 1},
                  texture_rotation: {value: 0}
                },
                // wireframe: true,
                transparent: !0,
                ...shaders
              })
          );
          plane.mesh = mesh
          mesh.z = 0;
          threeJsObj.object3D.add(mesh);
        })
      }
      
      threeJsObj.planes.push(plane);
    });
    promises.push(promise);
  }
  
  
  threeJsObj.scene.add(threeJsObj.object3D)
  
  return Promise.all(promises).then(() => {
    image.parentElement.removeChild(image)
    console.log('canvas loaded');
  });
}

const render = ({props, data}) => {
  if (props.stopped) {
    if (props.firstRun) props.firstRun = false;
    else return;
  }
  
  const totalImages = props.totalImages;
  const r = 0.015;
  props.paused || (props.perVisible = (r * props.percentCoef + props.perVisible) % totalImages);
  
  
  data.threeJsObj.planes.forEach(function (plane, index) {
    const {mesh, color, title, subtitle} = plane;
    if (mesh) {
      const uniforms = mesh.material.uniforms;
      if (uniforms) {
        const perVisible = props.perVisible;
        let d = 1;
        perVisible >= index - 1 && perVisible <= index + 1
            ? (d = perVisible - (index - 1))
            : 0 === index && perVisible >= totalImages - 1
                ? (d = perVisible - (index - 1) - totalImages)
                : (perVisible < index - 1 || perVisible > index + 1) && (d = 0);
        uniforms.perVisible.value = d;
        if (d >= 1 && d < 2) {
          playProgress({props, index, color, data, progress: d - 1});
          nextImage({props, index, color,title, subtitle, data});
        }
        uniforms.deformation.value = props.deformation;
        mesh.position.z = (index - ~~perVisible + totalImages) % totalImages;
      }
    }
  });
  data.threeJsObj.renderer.render(data.threeJsObj.scene, data.threeJsObj.camera);
  requestAnimationFrame(() => render({props, data}));
}

const nextImage = ({props, index, color,title, subtitle, data}) => {
  if (index === props.currentIndex) return;
  const titleElement=data.block.querySelector('.colored-headline'),
      subtitleElement=data.block.querySelector('.comfortta');
  props.currentIndex = index;
  gsap.set(data.block.progressPaths[props.currentPath], {zIndex: 0});
  gsap.fromTo(data.block.progressPaths[props.currentPath], {drawSVG: '100% 0'}, {drawSVG: '100% 100%', duration: 0.2});
  props.currentPath = (props.currentPath + 1) % 2
  data.block.progressPathsAnimation[props.currentPath].progress(0);
  gsap.set(data.block.progressPaths[props.currentPath], {zIndex: 2, stroke: color});
  gsap.to(titleElement, {color, scrambleText:title});
  gsap.to(subtitleElement, {color, scrambleText:subtitle});
}
const playProgress = ({props, index, color, data, progress}) => {
  if (progress >= 1) {
    return;
  }
  data.block.progressPathsAnimation[props.currentPath].progress(progress);
}

const resize = (threeJsObj) => {
  const {innerWidth, innerHeight} = window
  threeJsObj.planes.forEach(function (plane) {
    const mesh = plane.mesh;
    if (mesh) {
      const uniforms = mesh.material.uniforms;
      
      if (uniforms) {
        uniforms.resolution.value.x = innerWidth;
        uniforms.resolution.value.y = innerHeight;
      }
    }
  });
  threeJsObj.renderer.setSize(innerWidth, innerHeight);
  threeJsObj.camera.fov = 2 * (180 / Math.PI) * Math.atan(1 / (2 * 100));
  threeJsObj.camera.aspect = window.innerWidth / window.innerHeight;
  threeJsObj.camera.updateProjectionMatrix();
  threeJsObj.object3D.scale.x = threeJsObj.camera.aspect;
  threeJsObj.object3D.scale.y = 1;
}

const stop = (props) => {
  clearTimeout(props.timeOut);
  props.paused = true;
  let perVisible = props.perVisible;
  perVisible = (Math.round(perVisible + .2)) + 0.001;
  gsap.to(props, {percentCoef: 0, deformation: 0, ease: 'power4.InOut', duration: 0.7 * 1.5});
  gsap.to(props, {
    perVisible,
    ease: 'power4.InOut',
    duration: 0.7 * 0.7,
    onComplete: () => {
      clearTimeout(props.timeOut);
      props.timeOut = setTimeout(() => props.stopped = props.paused, 1000)
    }
  });
}

const play = ({props, data}) => {
  clearTimeout(props.timeOut);
  props.paused = false;
  if (props.stopped) {
    props.stopped = false;
    render({props, data});
  }
  gsap.to(props, {deformation: 1, ease: 'power4.InOut', duration: 0.8});
  gsap.to(props, {percentCoef: 1, ease: 'power4.InOut', duration: 0.4});
}


const setupEvents = ({props, data}) => {
  const {block} = data
  const mouseCircle = block.querySelector('.shuffle-btn-mouse')
  const journeyBtn = block.querySelector('.timeline .btn');
  block.addEventListener('contextmenu', event => event.preventDefault());
  console.log(isMobile());
  if (isMobile()) {
    mouseCircle.parentElement.removeChild(mouseCircle)
    // disableLP(block);
    
    block.addEventListener('touchstart', () => {
      play({props, data});
    });
    
    block.addEventListener('touchend', () => {
      stop(props);
    });
  } else {
    block.addEventListener('mousemove', ({clientX, clientY, target, path}) => {
      if (target.closest('.timeline .btn')) {
        return
      }
      const mouseCircleRect = mouseCircle.getBoundingClientRect();
      const blockRect = block.getBoundingClientRect(),
          x = clientX - blockRect.left - mouseCircleRect.left - mouseCircleRect.width / 2,
          y = clientY - blockRect.top - mouseCircleRect.top - mouseCircleRect.height / 2;
      gsap.to(mouseCircle, {x, y, duration: 1, overwrite: true})
    })
    block.addEventListener('mousedown', () => {
      play({props, data});
      gsap.to(mouseCircle, {scale: .8});
      mouseCircle.classList.add('clicked');
    });
    
    block.addEventListener('mouseup', () => {
      stop(props);
      gsap.to(mouseCircle, {scale: 1});
      mouseCircle.classList.remove('clicked');
    });
    block.addEventListener('mouseover', ({target}) => {
      mouseCircle.classList.toggle('active', !target.closest('.timeline .btn'));
    });
    block.addEventListener('mouseleave', () => {
      stop(props);
      gsap.to(mouseCircle, {x: 0, y: 0, overwrite: 'auto', duration: .7})
      mouseCircle.classList.remove('active');
    });
    journeyBtn.addEventListener('mouseenter', () => {
      gsap.to(mouseCircle, {x: 0, y: 0, overwrite: 'auto', duration: .7})
      mouseCircle.classList.remove('active');
    })
  }
  
  block.addEventListener('focus', () => {
    play({props, data});
  });
  
  block.addEventListener('blur', () => {
    stop(props);
  });
  
}


const sinkEvent = (event) => {
  console.log('sink');
  var e = event || window.event;
  e.preventDefault && e.preventDefault();
  e.stopPropagation && e.stopPropagation();
  e.cancelBubble = true;
  e.returnValue = false;
  return false;
}

const disableLP = (node) => {
  node.ontouchstart = sinkEvent;
  node.ontouchmove = sinkEvent;
  node.ontouchend = sinkEvent;
  node.ontouchcancel = sinkEvent;
}


windowOnLoad(blockScript);




export default {
  vertexShader:
      `

uniform float deformation;
uniform float perVisible;

varying vec2 vUv;



float getDis(vec2 p, vec2 q)
{
	return sqrt((p.x - q.x) * (p.x - q.x) + (p.y - q.y) * (p.y - q.y));
}

void main()
{


	vUv = uv;
	
	if (perVisible <= 0. || perVisible >= 2.) {
		vec3 p = position;
		p.z -= 1000.;
		gl_Position = projectionMatrix * modelViewMatrix * vec4(p, 1.0);
		return;
	}

	float coef = 400.;
	float dis = getDis(uv, vec2(.5, .5));

	float offsetZ = abs(dis * dis * coef / 1. * deformation);
	offsetZ -= abs((1. - dis * dis) * coef / 5. * deformation);
	
	vec3 p = position;
	p.z += offsetZ;
	
	gl_Position = projectionMatrix * modelViewMatrix * vec4(p, 1.0);
}`,
  fragmentShader:
      `

		#define M_PI 3.1415926535897932384626433832795

		uniform float deformation;
		uniform vec2 resolution;
		uniform float perVisible;
		uniform float shapeIndex;

		uniform sampler2D texture_self;
		uniform vec2 texture_size;
		uniform vec2 textureImSize;
		uniform float texture_alpha;
		uniform float texture_rotation;

		varying vec2 vUv;



		vec2 getTextureCovUv(vec2 baseUv, vec2 resolution, vec2 textureSize, vec2 imageSize)
		{
			float ratio = resolution.x / resolution.y;
			float textureRatio = imageSize.x / imageSize.y;
			vec2 scale = vec2(1.0, 1.0);
			if (textureRatio >= ratio) scale.x = textureRatio / ratio;
			if (textureRatio < ratio) scale.y = ratio / textureRatio;
			scale.x *= textureSize.x / imageSize.x;

			vec2 offset = vec2((scale.x - 1.0) * 0.5, (scale.y - 1.0) * 0.5);
			vec2 uv = (offset + baseUv) * vec2(1.0 / scale.x, 1.0 / scale.y);
			return uv;
		}

		float getDis(vec2 p, vec2 q)
		{
			return sqrt((p.x - q.x) * (p.x - q.x) + (p.y - q.y) * (p.y - q.y));
		}

		vec4 blurIt(sampler2D textureB, vec2 uv, float coef)
		{
			vec2 velocity = vec2(0.6, 0.);
			vec4 finalColor = vec4(0.);
			vec2 offset = vec2(0.);
			float dis = getDis(uv, vec2(0.5, 0.5));
			const int samples = 20;
			for(int i = 0; i < samples; i++)
			{
				offset = coef * velocity * (float(i) / (float(samples) - 1.) - .5) * dis * dis * dis;
				vec4 c = texture2D(textureB, uv + offset);
				finalColor += c;
			}
			finalColor /= float(samples);
			return finalColor;
		}

		vec2 rotateUv(vec2 uv, vec2 pivot, float rotation) {
			float cosa = cos(rotation);
			float sina = sin(rotation);
			uv -= pivot;
			return vec2(
				cosa * uv.x - sina * uv.y,
				cosa * uv.y + sina * uv.x
			) + pivot;
		}

		vec2 scaleUv(vec2 uv, vec2 scale) {
			vec2 signedUv = 2. * uv - 1.;
			vec2 scaleUv = signedUv / scale;
			return (scaleUv + 1.) / 2.;
		}







		float transToCenter(vec2 uv, float ratio, float pct, float deformation)
		{
			if (pct < 0.) return 0.;
			else if (pct > 1.) return 1.;

			float _pct = pow(pct, 1.5);

			vec2 scale_dist = vec2(1., 1.);

			scale_dist *= 1.3;

			vec2 uv_dist = scaleUv(uv, scale_dist);
			vec2 uv_signed = 2. * uv_dist - 1.;
			float dist = getDis(uv_dist, vec2(.5, .5));
			float coef;

			float dist_2 = sqrt(pow(uv_signed.x, 2.) + pow(uv_signed.y, 2.));

			float radius = (1. - _pct) * (1. + (1. - deformation));
			float x = uv_signed.x;
			float y = - uv_signed.y;

			float angle = acos(x / dist_2);
			if (y < 0.) angle = M_PI + M_PI - angle;
			float pct_angle = angle / (M_PI * 2.);

			float _coef = 1.;
			_coef *= deformation;

			float _steps = 8.;
			float _step_pct = 1. / _steps;
			float _step_rad = (M_PI * 2.) / _steps;

			float offsets[ 8 ];
			float _shapeIndex = shapeIndex;
			float speed;

		
			
			float blur = 0.2;
			
			speed = _pct * 1.5;

			if (_shapeIndex == 0.) {
				offsets[0] =  0.0 + speed * 3.;
				offsets[1] = -1.0 + speed * 2.;
				offsets[2] =  1.0 + speed * 1.;
				offsets[3] = -0.0 + speed * 2.;
				offsets[4] =  2.0 + speed * 2.;
				offsets[5] =  0.0 + speed * 1.;
				offsets[6] =  0.0 + speed * 3.;
				offsets[7] =  2.0 + speed * 2.;
			}
			else if (_shapeIndex == 1.) {
				offsets[0] = -2.0 + speed * 4.;
				offsets[1] =  0.0 + speed * 2.;
				offsets[2] =  1.0 + speed * 1.;
				offsets[3] = -2.0 + speed * 3.;
				offsets[4] =  2.0 + speed * 4.;
				offsets[5] =  4.0 + speed * 4.;
				offsets[6] = -1.0 + speed * 1.;
				offsets[7] =  2.0 + speed * 1.;
			}
			else if (_shapeIndex == 2.) {
				offsets[0] =  3.0 + speed * 4.;
				offsets[1] = -1.0 + speed * 2.;
				offsets[2] =  0.0 + speed * 1.;
				offsets[3] = -2.0 + speed * 4.;
				offsets[4] =  1.0 + speed * 1.;
				offsets[5] =  2.0 + speed * 2.;
				offsets[6] =  4.0 + speed * 4.;
				offsets[7] =  1.0 + speed * 1.;
			}
			else {
				offsets[0] =  0.0 + speed * 3.;
				offsets[1] = -1.0 + speed * 2.;
				offsets[2] =  0.0 + speed * 4.;
				offsets[3] =  1.0 + speed * 2.;
				offsets[4] =  2.0 + speed * 1.;
				offsets[5] =  0.0 + speed * 3.;
				offsets[6] =  3.0 + speed * 2.;
				offsets[7] =  1.0 + speed * 4.;
			}

			_coef = 0.;

				 if (floor(pct_angle / _step_pct) == 0.) { _coef = offsets[0]; }
			else if (floor(pct_angle / _step_pct) == 1.) { _coef = offsets[1]; }
			else if (floor(pct_angle / _step_pct) == 2.) { _coef = offsets[2]; }
			else if (floor(pct_angle / _step_pct) == 3.) { _coef = offsets[3]; }
			else if (floor(pct_angle / _step_pct) == 4.) { _coef = offsets[4]; }
			else if (floor(pct_angle / _step_pct) == 5.) { _coef = offsets[5]; }
			else if (floor(pct_angle / _step_pct) == 6.) { _coef = offsets[6]; }
			else if (floor(pct_angle / _step_pct) == 7.) { _coef = offsets[7]; }

			_coef = sin(_coef);
			_coef *= (1. - _pct);
			_coef *= .1;
			if (_pct < 0.5)
				_coef *= deformation;

			radius += (1. + sin((_step_rad * .75 + angle) * _steps)) * _coef;

			/**/

			float line = sqrt(x * x + y * y);

			if (radius > line - blur && radius < line + blur) {
				coef = 1. - (radius - line) / blur;
			} else if (radius < line) {
				coef = 1.;
			} else {
				coef = 0.;
			}

			return coef;
		}

		void main()
		{
			float _deformation = deformation;
			float _perVisible = perVisible;


			float dist, coefColor;
			vec4 color;
			vec2 uv, uv_mask;
			float scale = 1., rotation = 0.;
			vec4 blackColor = vec4(0., 0., 0., 1.);
			float ratio = resolution.x / resolution.y;
			
			uv = vUv;

			if (perVisible <= 0. || perVisible >= 2.) {
				color = blackColor;
				color.a = 0.;
				gl_FragColor = color;
				return;
			}

			float scaleSpeed = 1.3 * .9;
			float scaleCoef = 1.5;
			scale = 1. + (1. - _perVisible * scaleSpeed) * scaleCoef * _deformation;
			scale += .5 * _deformation;
			if (scaleCoef > 1.)
				scale += (scaleCoef * scaleSpeed - 1.) * _deformation;

			rotation = - (_perVisible - 1.) * .6;

			uv_mask = uv;
			uv_mask = rotateUv(uv_mask, vec2(.5, .5), M_PI * .15);

			uv = rotateUv(uv, vec2(.5, .5), rotation);
			uv = getTextureCovUv(uv, resolution, texture_size, textureImSize);
			uv = scaleUv(uv, vec2(scale, scale));

			float margin = (texture_size.x - textureImSize.x) / texture_size.x * .5;
			if (uv.x < margin || uv.x > 1. - margin || uv.y < 0. || uv.y > 1.)
			{
				color = vec4(0., 0., 0., 0.);
				color.a = 0.;
			}
			else
			{
				color = blurIt(texture_self, uv, _deformation);
				color = blackColor * (1. - texture_alpha) + color * texture_alpha;
				color.a *= transToCenter(uv_mask, ratio, _perVisible, _deformation);
			}

			gl_FragColor = color;
		}`
};
<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'block-hero-journey image-plus-layout';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/block-hero-journey/screenshot1.png" >';
  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$subtitle = get_field('subtitle');
$form_shortcode = get_field('form_shortcode');
$form_title = get_field('form_title');
$cta_button = get_field('cta_button');
?>
<!-- region Kiwi's Block -->
<?php general_settings_for_blocks($id, $className); ?>
<div class="kiwi-hero-images-data">
  <?php
  $first_img = false;
  while (have_rows('slider_images')):the_row();
    // A few settings
    $image = get_sub_field('image');
    $img_file = $image['url'];
    $img_id = $image['id'];
    $text_color = get_sub_field('text_color');
    $title = get_sub_field('title');
    $subtitle = get_sub_field('subtitle');
    
    if ($first_img == false) {
//      $imgData = base64_encode(file_get_contents($img_file));
//      $first_img = [
//        'data:' . $image['mime_type'] . ';base64,' . $imgData,
//        $text_color
//      ];
      $first_img=[$img_id,$text_color,$title,$subtitle];
    } else {
      ?>
      <span data-title="<?=$title?>" data-subtitle="<?=$subtitle?>" data-img="<?=$img_file?>" data-color="<?=$text_color ? $text_color : '#2196f3'?>"></span>
      <?php
    }
  endwhile; ?>
</div>
<div class="draggable-controller">
  <svg class="shuffle shuffle-btn-bg" height="8.2rem" viewBox="0 0 82 82" width="8.2rem" xmlns="http://www.w3.org/2000/svg">
    <path d="M41.1,1c11,0,21,4.5,28.2,11.7C76.5,19.9,81,29.9,81,41s-4.5,21-11.7,28.3S52,81,41,81s-21-4.5-28.3-11.7 S1,52,1,41s4.5-21,11.7-28.3S30,1,41.1,1"></path>
  </svg>
  <svg class="shuffle shuffle-btn-progress" height="8.2rem" viewBox="0 0 82 82" width="8.2rem" xmlns="http://www.w3.org/2000/svg">
    <path d="M41.1,1c11,0,21,4.5,28.2,11.7C76.5,19.9,81,29.9,81,41s-4.5,21-11.7,28.3S52,81,41,81s-21-4.5-28.3-11.7 S1,52,1,41s4.5-21,11.7-28.3S30,1,41.1,1"></path>
    <path d="M41.1,1c11,0,21,4.5,28.2,11.7C76.5,19.9,81,29.9,81,41s-4.5,21-11.7,28.3S52,81,41,81s-21-4.5-28.3-11.7 S1,52,1,41s4.5-21,11.7-28.3S30,1,41.1,1"></path>
  </svg>
  <div class="shuffle shuffle-btn-icon">
    <svg viewBox="0 0 23 15" width="2.3rem" xmlns="http://www.w3.org/2000/svg" y="15">
      <path d="M17.5 -0.9H19.4V9.6H17.5z" transform="rotate(-45.001 18.46 4.391)"></path>
      <path d="M13.2 9.6H23.7V11.5H13.2z" transform="rotate(-45.001 18.467 10.557)"></path>
      <circle cx="13.5" cy="7.5" r="1.7"></circle>
      <circle cx="7.7" cy="7.5" r="1.7"></circle>
      <circle cx="1.7" cy="7.5" r="1.7"></circle>
    </svg>
  </div>
  <div class="shuffle shuffle-btn-mouse">
    <svg height="8.2rem" viewBox="0 0 82 82" width="8.2rem" xmlns="http://www.w3.org/2000/svg">
      <path d="M41.1,1c11,0,21,4.5,28.2,11.7C76.5,19.9,81,29.9,81,41s-4.5,21-11.7,28.3S52,81,41,81s-21-4.5-28.3-11.7 S1,52,1,41s4.5-21,11.7-28.3S30,1,41.1,1"></path>
    </svg>
  </div>
</div>
<div class="container">
  <div class="hero-content">
    <h3 class="headline-3 colored-headline word-up"><?=$first_img[2]?></h3>
    <h4 class="headline-4 comfortta word-up"><?=$first_img[3]?></h4>
    <div class="form-wrapper iv-st-from-bottom">
      <h6 class="headline-6"><?=$form_title?></h6>
      <?=$form_shortcode?>
    </div>
  </div>
</div>
<div class="timeline iv-st-from-bottom">
  <div class="hr">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </div>
  <a class="btn" href="<?=$cta_button['url']?>" target="<?=$cta_button['target']?>">
    <div class="btn-border"></div>
    <div class="btn-bg"></div>
    <div class="btn-text d-flex align-items-center">
      <div class="icon-wrapper">
        <svg height="13" viewBox="0 0 16 13" width="16" xmlns="http://www.w3.org/2000/svg">
          <path
            d="M6.666 12.742a1.203 1.203 0 0 1-1.746-1.021l-.18-5.4-4.262-3.32a1.203 1.203 0 0 1 .2-2.013c.16-.08.315-.12.486-.126L14.3.226c.456-.03.903.213 1.126.65.223.439.157.944-.135 1.294L7.055 12.423a1.134 1.134 0 0 1-.39.32z"
            fill="#2196f3"/>
        </svg>
      </div>
      <span><?=$cta_button['title']?></span>
    </div>
  </a>
</div>
<a class="scroll-down link-with-arrow down-arrow iv-st-from-right" href="#">
  <span>Scroll Down</span>
  <svg class="arrow" height="7" viewBox="0 0 13 7" width="13" xmlns="http://www.w3.org/2000/svg">
    <path d="M6.5 0L13 7H0z" fill="#666767"/>
  </svg>
</a>
<img style="z-index: 1;" alt="Hero IMG" class="image" <?php acf_img($first_img[0],'1024px','full');?> data-color="<?=$first_img[1]?>" data-title="<?= $first_img[2]?>" data-subtitle="<?= $first_img[3]?>" />
</section>

<!-- endregion Kiwi's Block -->

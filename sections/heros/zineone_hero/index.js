import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {preventPageScroll} from "../../../scripts/functions/prevent_allowPageScroll";
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/zineone';

/**
 *
 * @param container {Document}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
const block = container.querySelector('.hero_block');
  
  // const btn = block.querySelector("#play-btn");
  // btn?.addEventListener('click', () => {
  //   if (!btn.classList.contains('playing')) {
  //     btn.classList.add('playing')
  //   } else {
  //     btn.classList.remove('playing')
  //   }
  // });
  // const videoToggler = block.querySelector('.video-toggler');
  // const video = block.querySelector('video');
  // console.log(videoToggler)
  // videoToggler.addEventListener('click',()=>{
  //   let videoState = video.classList.toggle('video-playing');
  //   if (videoState) {
  //     videoToggler.classList.remove('video-toggler-paused');
  //     videoToggler.classList.add('video-toggler-playing');
  //     video.play();
  //   } else {
  //     video.pause();
  //     videoToggler.classList.remove('video-toggler-playing');
  //     videoToggler.classList.add('video-toggler-paused');
  //   }
  // });
  
  
  const wysiwygBlock = block.querySelector('.wysiwyg-block');
  wysiwygBlock?.querySelector('.resources-content-wrapper')?.removeAttribute('class');
  
  
  /// Make Every Visit Count Animation
  
  const startAnimation = gsap.timeline({defaults: {ease: "back(1)"}})
      .from(block.querySelectorAll(".waves-wrapper circle"), {
        scale: 0,
        stagger: .2,
        transformOrigin: "center",
        duration: 1.5
      })
      .from(block.querySelectorAll(".small-circle-wrapper path"), {
        opacity: 0,
        duration: 1,
      }, "<1")
      .to(block.querySelector(".site-box"), {
        clipPath: "polygon(0% 0%, 0% 100%, 100% 100%, 100% 0%)",
        duration: 1,
        ease: "power2.in"
      }, "<")
      
      .to(block.querySelector(".numbers-move"), {
        clipPath: "polygon(0 0, 100% 0, 100% 100%, 0% 100%)",
        duration: .5,
        ease: "linear"
      })
      .from(block.querySelectorAll(".spotlight-base path"), {
        scale: 0,
        transformOrigin: "center",
        stagger: .1
      }, "<.5")
      .from(block.querySelectorAll(".spotlight .glow-wrapper"), {
        scale: 0,
        transformOrigin: "bottom",
        stagger: .1,
        ease: "power2"
      }, "<.2")
      
      .from(block.querySelectorAll(".clint"), {
        scale: 0,
        x: -300,
        opacity: 0,
        duration: 1,
        transformOrigin: "center",
        ease: "back",
        stagger: .3
      }, "<")
      
      .from(block.querySelectorAll(".wave"), {
        opacity: 1,
        repeat: -1,
        duration: 1,
        yoyo: true,
        ease: "steps(4)",
        stagger: .3
      })
  
  
  const count = gsap.timeline({
    defaults: {
      duration: 3,
      ease: "linear",
      repeat: -1
    }
  })
      .to(block.querySelectorAll(".site-text-wrapper path"), {
        y: -95
      })
      .set(block.querySelector(".numbers-move g:last-child"), {
        xPercent: -100,
        x: -20
      })
      .fromTo(block.querySelector(".numbers-move"), {x: 60, xPercent: 0}, {
        x: 70,
        xPercent: 50
      })
      .to(block.querySelector(".spin-3"), {
        transformOrigin: "center",
        rotate: 360,
        duration: 5,
      }, "<")
      .to(block.querySelector(".spin-2"), {
        transformOrigin: "50% 57%",
        rotate: 360,
      }, "<.2")
      .to(block.querySelector(".spin-1"), {
        transformOrigin: "center",
        rotate: 360,
        duration: 2,
        
      }, "<.3");
  
  
  // region hero form modal
  const btnModal = block.querySelector('.btn-modal');
  const formModal = block.querySelector('.form-modal');
  
  if (formModal) {
    btnModal?.addEventListener('click', function () {
      // preventPageScroll();
      // region prevent page scroll
      document.documentElement.style.overflowY = 'hidden';
      document.body.style.overflowY = 'hidden';
      // endregion prevent page scroll
      formModal.classList.add('active');
    })
  }
  // endregion hero form modal
  
  // region hero video modal
  const btnVideoModal = block.querySelector('.hero-video-toggle');
  const VideoModal = block.querySelector('.hero-video-modal');
  
  if (VideoModal) {
    btnVideoModal?.addEventListener('click', function () {
      preventPageScroll();
      VideoModal.classList.add('active');
    })
  }
  // endregion hero video modal
  
  // let COLORS, Confetti, NUM_CONFETTI, PI_2, canvas, confetti, context, drawCircle, i, range, resizeWindow, xpos;
  
  const NUM_CONFETTI = 35;
  
  const COLORS = [[85, 71, 106], [174, 61, 99], [219, 56, 83], [244, 92, 68], [248, 182, 70]];
  
  const PI_2 = 2 * Math.PI;
  
  
  const canvas = block.querySelector("canvas.circles");
  
  if (canvas) {
    
    const context = canvas.getContext("2d");
    
    const resizeWindow = function () {
      const parent = canvas.parentElement;
      const {width, height} = parent.getBoundingClientRect();
      canvas.width = 1.2 * width;
      canvas.height = 1.2 * height;
    };
    resizeWindow();
    window.addEventListener('resize', resizeWindow, false);
    
    const range = function (a, b) {
      return (b - a) * Math.random() + a;
    };
    
    const drawCircle = function (x, y, r, style) {
      context.beginPath();
      context.arc(x, y, r, 0, PI_2, false);
      context.fillStyle = style;
      return context.fill();
    };
    
    let xpos = 0.5;
    
    document.onmousemove = function (e) {
      return xpos = e.pageX / window.innerWidth;
    };
    
    
    const Confetti = class Confetti {
      constructor() {
        this.style = COLORS[~~range(0, 5)];
        this.rgb = `rgba(${this.style[0]},${this.style[1]},${this.style[2]}`;
        this.r = ~~range(2, 6);
        this.r2 = 2 * this.r;
        this.replace();
      }
      
      replace() {
        this.opacity = 0;
        this.dop = 0.03 * range(1, 4);
        this.x = range(-this.r2, canvas.width - this.r2);
        this.y = range(-20, canvas.height - this.r2);
        this.xmax = canvas.width - this.r;
        this.ymax = canvas.height - this.r;
        this.vx = range(0, 2) + 8 * xpos - 5;
        return this.vy = 0.7 * this.r + range(-1, 1);
      }
      
      draw() {
        let ref;
        this.x += this.vx;
        this.y += this.vy;
        this.opacity += this.dop;
        if (this.opacity > 1) {
          this.opacity = 1;
          this.dop *= -1;
        }
        if (this.opacity < 0 || this.y > this.ymax) {
          this.replace();
        }
        if (!((0 < (ref = this.x) && ref < this.xmax))) {
          this.x = (this.x + this.xmax) % this.xmax;
        }
        return drawCircle(~~this.x, ~~this.y, this.r, `${this.rgb},${this.opacity})`);
      }
      
    };
    
    const confetti = (function () {
      let j, ref, i;
      const results = [];
      for (i = j = 1, ref = NUM_CONFETTI; (1 <= ref ? j <= ref : j >= ref); i = 1 <= ref ? ++j : --j) {
        results.push(new Confetti());
      }
      return results;
    })();
    
    const step = function () {
      requestAnimationFrame(step);
      context.clearRect(0, 0, canvas.width, canvas.height);
      const results = [];
      for (let j = 0, len = confetti.length; j < len; j++) {
        results.push(confetti[j].draw());
      }
      return results;
    };
    
    step();
    
  }
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);

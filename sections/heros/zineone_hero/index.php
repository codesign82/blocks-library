<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$hero_versions = get_field( 'hero_versions' );
$dataClass     = 'hero_block';
$className     = 'hero_block ';
$className     .= $hero_versions;
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/hero_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <div class="hero-content" parallax-container>
    <?php
    switch ( $hero_versions ) {
      case "hero_block_version_2":
        get_template_part( "template-parts/blocks/hero_block/hero-version-2" );
        break;
      case "hero_block_version_3":
        get_template_part( "template-parts/blocks/hero_block/hero-version-3" );
        break;
      case "hero_block_version_4":
        get_template_part( "template-parts/blocks/hero_block/hero-version-4" );
        break;
      case "hero_block_version_5":
        get_template_part( "template-parts/blocks/hero_block/hero-version-5" );
        break;
      case "hero_block_version_6":
        get_template_part( "template-parts/blocks/hero_block/hero-version-6" );
        break;
      case "hero_block_version_7":
        get_template_part( "template-parts/blocks/hero_block/hero-version-7" );
        break;
      default:
        get_template_part( "template-parts/blocks/hero_block/hero-version-1" );
    } ?>
  </div>
</div>
</section>


<!-- endregion ZineOne's Block -->

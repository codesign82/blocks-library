<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'hero_block';
$className = 'hero_block js-loaded';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/takeoff-code/gutenberg-blocks/hero_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$background_image = get_field('background_image');
$background_type = get_field('background_type');
$background_video = get_field('background_video');
$mobile_video_file = @$background_video['mobile_video_file'];
$desktop_video_file = @$background_video['desktop_video_file'];
$mobile_poster_image = @$background_video['mobile_poster_image'];
$desktop_poster_image = @$background_video['desktop_poster_image'];
$cta_button = get_field('cta_button');
$cta_link = get_field('cta_link');

?>
<!-- region senet's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <?php if ($background_type) { ?>
        <div class="video-cont">
            <div class="cover-background aspect-ratio">
                <?php if ($background_type === 'image' && $background_image) { ?>
                    <picture>
                        <img src='<?= $background_image['url'] ?>  ' alt="<?= $background_image['alt'] ?>">
                    </picture>
                <?php } ?>
                <?php if ($background_type === 'video') { ?>
                    <video
                            class="video"
                            data-mobile-src="<?= $mobile_video_file ?>"
                            data-desktop-src="<?= $desktop_video_file ?>"
                            data-mobile-poster="<?= $mobile_poster_image ? $mobile_poster_image['url'] : "" ?>"
                            data-desktop-poster="<?= $desktop_poster_image ? $desktop_poster_image['url'] : "" ?>"
                            type="video/mp4"
                            playsinline
                            loop
                            muted
                            autoplay
                    >
                    </video>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <div class="main-content-cont">
        <div class='main-content'>
            <div class='content'>
                <?php if ($title) { ?>
                    <div class='headline-1 title'>
                        <?= $title ?>
                    </div>
                <?php } ?>
                <?php if ($description) { ?>
                    <div class='paragraph desc'>
                        <?= $description ?>
                    </div>
                <?php } ?>
                <div class='cta-wrapper'>
                    <?php if ($cta_button) { ?>
                        <a target="<?= $cta_button['target'] ?>" href=' <?= $cta_button['url'] ?>'
                           class='cta-btn'><?= $cta_button['title'] ?></a>
                    <?php } ?>
                    <?php if ($cta_link) { ?>
                        <div class='separator'> or</div>
                        <a target="<?= $cta_link ['target'] ?>" href=' <?= $cta_link ['url'] ?>' class='cta-link'>
                            <?= $cta_link ['title'] ?>
                            <svg class='arrow' width='22' height='11' viewBox='0 0 22 11' fill='none'
                                 xmlns='http://www.w3.org/2000/svg'>
                                <path d='M15.3292 3.54817H15.4542V3.42317V1.17424C15.4542 0.24156 16.5818 -0.225521 17.2413 0.433966L21.4434 4.63607L21.5318 4.54768L21.4434 4.63607C21.8522 5.04487 21.8523 5.70771 21.4434 6.11656L17.2413 10.3187C16.5818 10.9782 15.4542 10.5111 15.4542 9.57839V7.32942V7.20442H15.3292H0.585938C0.331389 7.20442 0.125 6.99803 0.125 6.74348V4.0091C0.125 3.75455 0.331389 3.54817 0.585938 3.54817H15.3292Z'
                                      fill='#FF5900' stroke='#FF5900' stroke-width='0.25'/>
                            </svg>
                        </a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import DrawSVGPlugin from 'gsap/DrawSVGPlugin'
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/wrap'

const blockScript = async (container = document) => {
  
  gsap.registerPlugin(DrawSVGPlugin)
  const blocks = container.querySelectorAll('.page_header_block');
  for (let block of blocks) {
    if (block.classList.contains('home-hero')) {
      
      // await import(`./style_home-hero.scss`);
      
      const mainSVG = block.querySelector('#blue-building-svg')
      const building = {
        curtain: {
          leftTop: mainSVG.querySelectorAll('[id^="blue-building_window_upper_left_close_curtain"]'),
          rightTop: gsap.utils.toArray(mainSVG.querySelectorAll('#blue-building_window_upper_right_curtain_close path')).reverse(),
          leftBottom: gsap.utils.toArray(mainSVG.querySelectorAll('#blue-building_window_lower_left_curtain_close path')).reverse(),
          rightBottom: mainSVG.querySelectorAll('[id^="blue-building_window_lower_right_close_curtain"]'),
        },
        bubble: {
          leftTop: mainSVG.querySelector('#blue-building_left-top-bubble'),
          rightTop: mainSVG.querySelector('#blue-building_right-top-bubble'),
          leftBottom: mainSVG.querySelector('#blue-building_left-bottom-bubble'),
          rightBottom: mainSVG.querySelector('#blue-building_right-bottom-bubble'),
        }
      }
      const leftTopBubbleTl = gsap.timeline()
          .fromTo(building.bubble.leftTop, {scale: 0, transformOrigin: '100% 100%'}, {scale: 1})
          .to(building.bubble.leftTop.querySelector('#blue-building_stars_line'), {
            rotation: 135,
            transformOrigin: '100% 50%'
          })
          .fromTo(building.bubble.leftTop.querySelector('#blue-building_pie_chart_green_fill'), {drawSVG: 0}, {drawSVG: 135 / 180 * 100 + '%'}, '<')
          .to(building.bubble.leftTop.querySelector('#blue-building_stars'), {
            rotation: -135,
            transformOrigin: '50% 50%'
          }, '<')
          .from(building.bubble.leftTop.querySelector('#blue-building_stars'), {opacity: 0, duration: 1})
          .from(building.bubble.leftTop.querySelector('#blue-building_pie_chart_number'), {
            scale: 0,
            transformOrigin: '50% 50%',
            ease: 'elastic.out',
            duration: 1
          }, '<')
      
      const rightTopBubbleTl = gsap.timeline()
          .fromTo(building.bubble.rightTop, {scale: 0, transformOrigin: '0 100%'}, {scale: 1})
          .from(building.bubble.rightTop.querySelector('#blue-building_graduation_hat'), {
            scale: 0,
            rotation: 360,
            transformOrigin: '50% 50%'
          })
          .from(building.bubble.rightTop.querySelector('#blue-building_certificate'), {
            scale: 0,
            ease: 'elastic.out',
            duration: 1,
            transformOrigin: '100% 0'
          }, '<')
      
      const leftBottomBubbleTl = gsap.timeline()
          .fromTo(building.bubble.leftBottom, {scale: 0, transformOrigin: '100% 100%'}, {scale: 1})
          .to(building.bubble.leftBottom.querySelectorAll('.light-me'), {fill: '#ffe494', stagger: .2})
      
      const rightBottomBubbleTl = gsap.timeline()
          .fromTo(building.bubble.rightBottom, {scale: 0, transformOrigin: '0 0'}, {scale: 1})
          .fromTo(building.bubble.rightBottom.querySelector('#blue-building_surffing'), {
            rotation: -10,
            transformOrigin: '32.4 1.8'
          }, {rotation: 10, duration: 2, repeat: -1, yoyo: true, ease: 'power1.inOut'})
      
      
      gsap.timeline({delay: 0.1, duration: 1})
          .to(building.curtain.leftTop, {attr: {d: (_, e) => e.dataset.open}})
          .add(leftTopBubbleTl)
          .to(building.curtain.rightTop, {y: -8, stagger: 0.09, duration: .1})
          .to(building.curtain.rightTop, {opacity: 0, stagger: 0.09, duration: .1}, '<.05')
          .add(rightTopBubbleTl)
          .to(building.curtain.leftBottom, {y: -8, stagger: 0.09, duration: .1}, '-=1.25')
          .to(building.curtain.leftBottom, {opacity: 0, stagger: 0.09, duration: .1}, '<.05')
          .add(leftBottomBubbleTl)
          .to(building.curtain.rightBottom, {attr: {d: (_, e) => e.dataset.open}}, '-=.4')
          .add(rightBottomBubbleTl)
    }
    // if (block.classList.contains('property-managers-hero')) {
    //   await import(`./style_property-managers-hero.scss`);
    //   const girlLoverLeavesSvg = block.querySelector('#girl-lover-leaves-svg')
    //   const girlLoverLeavesSvgHand = girlLoverLeavesSvg.querySelector('#girl-lover-leaves-svg_hand')
    //   const girlLoverLeavesSvgLeaves = girlLoverLeavesSvg.querySelector('#girl-lover-leaves-svg_leaves')
    //   //region girl lover animation
    //   gsap.timeline({
    //     repeat: -1, repeatDelay: 4,
    //     defaults: {
    //       transformOrigin: '44.8 115.1'
    //     }
    //   })
    //       .to(girlLoverLeavesSvgHand.querySelector('#girl-lover-leaves-svg_salute'), {opacity: 1, duration: .2})
    //       .to(girlLoverLeavesSvgHand.querySelector('#girl-lover-leaves-svg_fist'), {opacity: 0, duration: .2}, '<')
    //       .to(girlLoverLeavesSvgHand, {rotation: 30, ease: 'power2.inOut', duration: .3}, '<')
    //       .to(girlLoverLeavesSvgHand, {rotation: 5, repeat: 3, yoyo: true, ease: 'power2.inOut', duration: .3})
    //       .to(girlLoverLeavesSvgHand, {rotation: 0, ease: 'power2.inOut', duration: .7})
    //       .to(girlLoverLeavesSvgHand.querySelector('#girl-lover-leaves-svg_salute'), {opacity: 0, duration: .2}, '-=.5')
    //       .to(girlLoverLeavesSvgHand.querySelector('#girl-lover-leaves-svg_fist'), {opacity: 1, duration: .2}, '<')
    //
    //   gsap.fromTo(girlLoverLeavesSvgLeaves.querySelector('#girl-lover-leaves-svg_right-lower'),
    //       {
    //         transformOrigin: '0 50%',
    //         rotation: '-10',
    //       }, {
    //         rotation: '10',
    //         repeat: -1,
    //         yoyo: true,
    //         duration: 10,
    //         ease: 'power1.inOut',
    //       })
    //
    //   gsap.fromTo(girlLoverLeavesSvgLeaves.querySelector('#girl-lover-leaves-svg_left'),
    //       {
    //         transformOrigin: '100% 100%',
    //         rotation: '10',
    //       }, {
    //         rotation: '-10',
    //         repeat: -1,
    //         yoyo: true,
    //         duration: 14,
    //         ease: 'power1.inOut',
    //       })
    //
    //   gsap.fromTo(girlLoverLeavesSvgLeaves.querySelector('#girl-lover-leaves-svg_right-upper'),
    //       {
    //         transformOrigin: '0 100%',
    //         rotation: '-8',
    //         delay: 3,
    //       }, {
    //         rotation: '8',
    //         repeat: -1,
    //         yoyo: true,
    //         duration: 12,
    //         ease: 'power1.inOut',
    //       })
    //   //endregion girl lover animation
    //
    // }
    // if (block.classList.contains('faq-hero')) {
    //   await import(`./style_faq-hero.scss`);
    // }
    // if (block.classList.contains('about-hero')) {
    //   await import(`./style_about-hero.scss`);
    //   confettiFall(block);
    // }
    // if (block.classList.contains('competitors-hero')) {
    //   await import(`./style_competitors-hero.scss`);
    //   confettiFall(block);
    // }
    // if (block.classList.contains('brokers-hero')) {
    //   await import(`./style_broker_hero.scss`);
    //   confettiFall(block);
    // }
    // if (block.classList.contains('affiliate-program-hero')) {
    //   await import(`./style_affiliate-program-hero.scss`);
    // }
    // if (block.classList.contains('success-hero')) {
    //   await import(`./style_success-hero.scss`);
    //   const windowSVGs = {
    //     'boy-book': block.querySelector('#boy-book-svg #boy-book_hand-book'),
    //     'girl-lover-svg': block.querySelector('#girl-lover-svg #girl-lover-svg_hand'),
    //     'girl-cupcake-svg': block.querySelector('#girl-cupcake-svg #girl-cupcake-svg_hand_cupcake'),
    //   }
    //
    //
    //   //region boy book animation
    //   gsap.to(windowSVGs["boy-book"], {
    //     rotation: 10,
    //     repeat: -1,
    //     yoyo: true,
    //     ease: 'power2.inOut',
    //     duration: 3,
    //     transformOrigin: '28 87.2'
    //   })
    //   //endregion boy book animation
    //
    //   //region girl lover animation
    //   gsap.timeline({
    //     repeat: -1, repeatDelay: 4,
    //     defaults: {
    //       transformOrigin: '26.3 67.4'
    //     }
    //   })
    //       .to(windowSVGs["girl-lover-svg"].querySelector('#girl-lover-svg_salute'), {opacity: 1, duration: .2})
    //       .to(windowSVGs["girl-lover-svg"].querySelector('#girl-lover-svg_fist'), {opacity: 0, duration: .2}, '<')
    //       .to(windowSVGs["girl-lover-svg"], {rotation: 30, ease: 'power2.inOut', duration: .3}, '<')
    //       .to(windowSVGs["girl-lover-svg"], {rotation: 5, repeat: 3, yoyo: true, ease: 'power2.inOut', duration: .3})
    //       .to(windowSVGs["girl-lover-svg"], {rotation: 0, ease: 'power2.inOut', duration: .7})
    //       .to(windowSVGs["girl-lover-svg"].querySelector('#girl-lover-svg_salute'), {opacity: 0, duration: .2}, '-=.5')
    //       .to(windowSVGs["girl-lover-svg"].querySelector('#girl-lover-svg_fist'), {opacity: 1, duration: .2}, '<')
    //
    //   //endregion girl lover animation
    //
    //   //region girl cupcake animation
    //   gsap.timeline({
    //     repeat: -1, repeatDelay: 4,
    //   })
    //       .to(windowSVGs["girl-cupcake-svg"], {
    //         rotation: -10,
    //         ease: 'power2.inOut',
    //         duration: 2,
    //         repeat: 1,
    //         yoyo: true,
    //         repeatDelay: .3,
    //         transformOrigin: '50% 100%'
    //       })
    //   //endregion girl cupcake animation
    // }
    // if (block.classList.contains('free-credit-report-hero')) {
    //   await import(`./style_free-credit-report-hero.scss`);
    // }
    animations(block);
    imageLazyLoading(block);
  }
};
windowOnLoad(blockScript);




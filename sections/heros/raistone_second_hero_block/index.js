import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/raistone';

import {gsap} from "gsap"
import DrawSVGPlugin from "gsap/DrawSVGPlugin";

gsap.registerPlugin(DrawSVGPlugin)

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
    const blocks = container.querySelectorAll('.second_hero_block');
    for (let block of blocks) {


        const gridLines = block.querySelectorAll(".grid-lines path")
        const shapesImg = block.querySelector(".who-we-help-hero .image-wrapper")
        const icon = block.querySelector(".who-we-help-hero .dollar-icon")
        const iconShape = block.querySelector(".who-we-help-hero .icon-shape")
        // const charts = [...block.querySelectorAll(".charts path")].reverse()
        const charts = block.querySelectorAll(".who-we-help-hero .charts path")


        const gridAnimation = gsap.timeline()

            // who-we-help-hero Animation

            .from(gridLines, {
                drawSVG: 0,
                stagger: .05
            })
            .from(charts, {
                scaleY: 0,
                transformOrigin: "bottom center",
                stagger: .1,
                duration: 1,
            }, "<50%")
            .from(shapesImg, {
                opacity: 0
            }, "<50%")
            .from(icon, {
                transformOrigin: "center center",
                scale: 0,
                ease: "power3"
            }, "<50%")
            .from(iconShape, {
                transformOrigin: "bottom left",
                scale: 0,
                ease: "power3"
            }, "<20%")

        // Resources Animation
        const greenBackground = block.querySelector(".resources .green-background")
        const smallBoxWrapper = block.querySelector(".resources .small-box-wrapper")
        const boxIcon = block.querySelector(".resources .icon")
        const boxBorder = block.querySelectorAll(".resources .box-border")
        const lineBox = block.querySelectorAll(".resources .line-box")
        const resourcesLines = block.querySelectorAll(".resources .line")
        const resourcesImg = block.querySelector(".resources .image-wrapper")


        const resourcesAnimation = gsap.timeline()
            .from(boxBorder, {
                opacity: 0,
                stagger: .2
            })
            .from(smallBoxWrapper, {
                opacity: 0
            })

            .from(greenBackground, {
                xPercent: -10,
                yPercent: -10,
                opacity: 0,
            }, "<")
            .from(lineBox, {
                scale: 0,
                ease: "power2",
                stagger: .2,
                transformOrigin: "center center"
            })
            .from(boxIcon, {
                scale: 0,
                ease: "power2",
                transformOrigin: "center center"
            })

            .from(resourcesImg, {
                scale: 0,
                ease: "power3",
                transformOrigin: "right bottom"
            }, "<50%")
            .from(resourcesLines, {
                scaleX: 0,
                stagger: .2,
                transformOrigin: "left center"
            })


        // Heros Svg Animation

        const boxLines = block.querySelectorAll(".lines-wrapper line")
        const howWorkBoxBorder = block.querySelector(".border-shape")
        const howWorkImg = block.querySelector(".how-it-work-img")

        gsap.timeline()
            .from(howWorkBoxBorder, {
                drawSVG: 0,
                duration: .75,
                ease: "power3.inOut"
            })
            .from(howWorkImg, {
                opacity: 0,
                duration: .75,
                ease: "power3.inOut"
            }, "<")
            .from(boxLines, {
                drawSVG: 0,
                stagger: .1,
                duration: .75,
                ease: "power3.inOut"
            })

        // Heros Svg Animation About

        const aboutFirstCircle = block.querySelector(".first-circle")
        const aboutSecondCircle = block.querySelector(".second-circle")
        const aboutCircleLine = block.querySelector(".circle-line")
        const aboutImg = block.querySelector(".about-hero .image-wrapper")
        const aboutSmallBorder = block.querySelector(".about-hero .small-box-border")
        const smallBoxGreen = block.querySelector(".small-box-green")
        const aboutSmallBox = block.querySelector(".about-small-box")
        const aboutSmallLine = block.querySelectorAll(".about-small-line")
        const aboutSmallCircle = block.querySelectorAll(".about-small-box-circle")


        gsap.timeline()

            .from(aboutImg, {
                opacity: 0,
                duration: .75,
                ease: "power3.inOut"
            })

            .from(aboutFirstCircle, {
                scale: 0,
                transformOrigin: "center center",
                ease: "power3.inOut"
            })
            .from(aboutCircleLine, {
                drawSVG: 0,
                ease: "power3.inOut"
            }, "<70%")
            .from(aboutSecondCircle, {
                scale: 0,
                transformOrigin: "center center",
                ease: "power3.inOut"
            }, "<70%")
            .from(aboutSmallBorder, {
                drawSVG: 0,
                ease: "power3.inOut"
            }, "<")
            .from(smallBoxGreen, {
                opacity: 0,
                ease: "power3.inOut"
            }, "<70%")
            .from(aboutSmallBox, {
                drawSVG: 0,
                ease: "power3.inOut"
            }, "<")
            .from(aboutSmallLine, {
                drawSVG: 0,
                stagger: .2,
                ease: "power3.inOut"
            }, "<70%")
            .from(aboutSmallCircle, {
                opacity: 0,
                stagger: .2,
                ease: "power3.inOut"
            }, "<70%")


        const careersLeftLines = block.querySelectorAll(".careers-svg .left-line")
        const careersRightLines = block.querySelectorAll(".careers-svg .right-line")
        const careersChartLine = block.querySelectorAll(".careers-svg .line-chart")
        const careersDashLine = block.querySelectorAll(".careers-svg .dash-line")
        const careersSvgBorder = block.querySelectorAll(".careers-svg .careers-svg-border")
        const careersPersonCard = block.querySelectorAll(".careers-svg .person-card")

        gsap.timeline()
            .from(careersSvgBorder, {
                drawSVG: 0,
                duration: 1,
                delay: .5
            })
            .from(careersPersonCard, {
                opacity: 0
            }, "<50%")
            .from(careersLeftLines, {
                scaleX: 0,
                stagger: .1,
            })
            .from(careersRightLines, {
                scaleX: 0,
                transformOrigin: "right",
                stagger: .1,
            })

        gsap.fromTo(careersChartLine, {
            drawSVG: "0%"
        }, {
            drawSVG: "100%",
            duration: 2,
            ease: "power1",
        })
        gsap.to(careersDashLine, {
            strokeDashoffset: "3.5%",
            duration: 2,
            ease: "power1",
        })


        animations(block);
        imageLazyLoading(block);
    }

};
windowOnLoad(blockScript);


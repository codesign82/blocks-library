import './index.html';
import './style.scss';
import {gsap} from "gsap";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/propellerhealth';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {allowPageScroll, preventPageScroll} from "../../../scripts/functions/prevent_allowPageScroll";

gsap.registerPlugin(ScrollTrigger)
/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.hero_block');


  // Hero Animation

  const heroContent = block.querySelectorAll(".text-wrapper >*, .images-wrapper >*")
  const title = block.querySelector(".title")
  const floatingImages = block.querySelector(".images-wrapper")
  const floating1 = block.querySelector(".floating-1")
  const floating2 = block.querySelector(".floating-2")
  const floating3 = block.querySelector(".floating-3")
  const floating4 = block.querySelector(".floating-4")

  const tl = gsap.timeline({paused: true})
      .from(heroContent, {
        y: 50,
        opacity: 0,
        duration: 1,
        ease: "power4",
        stagger: .2,
        scrollTrigger: {
          trigger: block
        },
      }, .5)
  tl.call(() => {
    title.classList.add('animated')
  }, null, "<50%")
  window.addEventListener('js-loaded', () => {
    tl.play();
  });

  gsap.timeline({
    defaults: {
      duration: 5,
      ease: 'power2.inOut'
    },
    scrollTrigger: {
      trigger: floatingImages,
      start: "30% center",
    }
  })
      .from(floating1, {
        y: -10
      })
      .from(floating2, {
        y: 15
      }, "<")
      .from(floating3, {
        y: 20
      }, "<")
      .from(floating4, {
        y: -36
      }, "<")

  const modal = block.querySelector(".modal");
  const exit = block.querySelector(".exit");
  const btn = block.querySelector(".btn");
  // add block code here
  if (modal) {
    exit?.addEventListener("click", hide);
    modal?.addEventListener("click", hide);
    btn?.addEventListener("click", openModal);

    function hide(e) {
      if (e.target.closest('.webinar-wrapper') && !e.target.closest('.exit')) return;
      // scroller.paused(false);
      modal.classList.remove("show")
      allowPageScroll();
    }

    function openModal(e) {
      e.preventDefault()
      modal.classList.add("show")
      preventPageScroll();
    }
  }

  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


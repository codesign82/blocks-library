<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'hero_block';
$has_media = get_field('has_media');
$number_of_images = get_field('number_of_images');

$className = 'hero_block ';
if (!$has_media) {
  $className .= "has-no-media";
}

if ($number_of_images === "one") {
  $className .= "has-main-img";
}

if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/hero_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 *
 *
 *
 ****************************/
$has_subtitle = get_field('has_subtitle');
$has_links = get_field('has_links');

//////
$subtitle = get_field('subtitle');
$block_title = get_field('block_title');
$description = get_field('description');
$links_group = get_field('links_group');
///////Media Variable
$main_image = get_field('main_image');
$svg_or_image = get_field('svg_or_image');
$svg = get_field('svg');
$left_image = get_field('left_image');
$large_image = get_field('large_image');
$mobile_image = get_field('mobile_image');
$right_image = get_field('right_image');


//$select_hero_constellations = get_field( 'select_hero_constellations' );
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="text-wrapper" role="main" aria-label="hero title">
    <?php if ($has_subtitle) { ?>
      <h3 class="sub-title "><?= $subtitle ?></h3>
    <?php }
    if ($block_title) { ?>
      <h1 move-to-here class="title headline-2"><?= $block_title ?></h1>
    <?php }
    if ($description) { ?>
      <div class="paragraph description">
        <?= $description ?>
      </div>
    <?php } ?>
    <!-- region   links -->
    <?php if ($has_links) { ?>
      <div class="ctas">
        <?php if ($links_group['cta_btn']) { ?>
          <a href="<?= $links_group['cta_btn']['url'] ?>"
             class="btn"> <?= $links_group['cta_btn']['title'] ?></a>
        <?php } ?>
        <?php if ($links_group['link']) { ?>
          <a href="<?= $links_group['link']['url'] ?>"
             class="link">  <?= $links_group['link']['title'] ?>
            <svg class='arrow' viewBox='0 0 29.5 22.1' role="img">
              <path class='arrow-head'
                    d='M29.1,12.1a1.7,1.7,0,0,0,0-2.1L19.5.4a1.7,1.7,0,0,0-2.1,0,1.6,1.6,0,0,0,0,2.2L25.9,11l-8.5,8.5a1.6,1.6,0,0,0,0,2.2,1.7,1.7,0,0,0,2.1,0Z'
                    fill='#f90'/>
              <path class='arrow-line' d='M0,12.5H28v-3H0Z' fill='#f90'/>
            </svg>
          </a>
        <?php } ?>
      </div>
    <?php } ?>
    <!-- endregion   links -->
  </div>
  <?php if ($has_media) { ?>
    <?php
    if ($number_of_images === "one") { ?>
      <picture class="main-img-wrapper aspect-ratio">
        <?= get_acf_image($main_image, 'img-1104-536') ?>
      </picture>
    <?php } else { ?>
      <div class="images-wrapper">
        <div class="pic-1  <?= $svg_or_image === 'image' ? 'rounded' : '' ?>">
          <?php if ($svg_or_image === 'svg') { ?>
            <div class="fade-effect">
              <div class="pic-1-content-wrapper floating-4">
                <div class="pic1-content">
                  <div class="pic1-left">
                    <div class="people-svg">
                      <?= $svg ?>
                    </div>
                  </div>
                  <div class="pic1-right">
                    <span class="grey-fill" role="img"></span>
                    <span class="grey-fill" role="img"></span>
                    <span class="grey-fill" role="img"></span>
                  </div>
                </div>
              </div>
            </div>
          <?php } else { ?>
            <div class="fade-effect">
              <picture class="img-wrapper aspect-ratio floating-4">
                <?= get_acf_image($left_image, 'img-70-40') ?>
              </picture>
            </div>
          <?php } ?>
        </div>
        <?php if ($large_image) { ?>
          <div class="fade-effect">
            <picture class="aspect-ratio pic-2 ">
              <?= get_acf_image($large_image, 'img-613-384', ['floating-1']) ?>
            </picture>
          </div>
        <?php } ?>
        <?php if ($mobile_image) { ?>
          <div class="fade-effect">
            <picture class="aspect-ratio pic-3 ">
              <?= get_acf_image($mobile_image, 'img-193-374', ['floating-2']) ?>
            </picture>
          </div>
        <?php } ?>
        <?php if ($right_image) { ?>
          <div class="fade-effect">
            <picture class="aspect-ratio pic-4  rounded">
              <?= get_acf_image($right_image, 'img-173-173', ['floating-3']) ?>
            </picture>
          </div>
        <?php } ?>
      </div>
    <?php }
  } ?>
</div>
</section>

<!-- endregion headversity's Block -->

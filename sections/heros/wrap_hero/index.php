<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'hero_block';
$className = 'hero_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/hero_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title       = get_field( 'title' );
$description = get_field( 'description' );
$index       = 0;
?>
<!-- region WRAP's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="hero_block_video">
  <?php while ( have_rows( 'hero_sliders' ) ) {
    the_row();
    $is_video_file = get_sub_field( 'is_video_file' );
    $video_url     = get_sub_field( 'video_url' );
    $video_url    = is_the_url_youtube( $video_url );
    $video_file    = get_sub_field( 'video_file' );
    ?>
    <?php if ( $index === 0 ) { ?>
      <video class="hero_block_video_wrapper" playsinline autoplay muted loop src="<?= $is_video_file ? $video_file : $video_url ?>" type="video/mp4"></video>
      <?php $index ++;
    } ?>
  <?php } ?>
</div>
<div class="container">
  <?php if ( have_rows( 'hero_sliders' ) ) { ?>
    <div class="hero-sliders iv-st-from-bottom">
      <?php while ( have_rows( 'hero_sliders' ) ) {
        the_row();
        $is_video_file     = get_sub_field( 'is_video_file' );
        $video_url         = get_sub_field( 'video_url' );
        $video_url    = is_the_url_youtube( $video_url );
        $video_file        = get_sub_field( 'video_file' );
        $slide_duration    = get_sub_field( 'slide_duration' );
        $title             = get_sub_field( 'title' );
        $description       = get_sub_field( 'description' );
        $first_cta_button  = get_sub_field( 'first_cta_button' );
        $second_cta_button = get_sub_field( 'second_cta_button' );
        $third_cta_button  = get_sub_field( 'third_cta_button' );
        ?>
        <div class="hero-slider" data-slider-duration="<?= $slide_duration ?>" data-video-src="<?= $is_video_file ? $video_file : $video_url ?>">
          <?php if ( $title ) { ?>
            <h1 class="hero-title headline-3 text-uppercase"><?= $title ?></h1>
          <?php } ?>
          <div class="hero-slider-progress">
            <div class="hero-slider-progress-track"></div>
          </div>
          <?php if ( $description ) { ?>
            <div class="hero-description paragraph paragraph-16"><?= $description ?></div>
          <?php } ?>
          <div class="buttons-wrapper">
            <?php if ( $first_cta_button ) { ?>
              <?php get_template_part( 'template-parts/cta_button', '', array(
                'className' => 'btn-request-demo',
                'cta_group' => $first_cta_button
              ) ) ?>
            <?php } ?>
            <?php if ( $second_cta_button ) { ?>
              <?php get_template_part( 'template-parts/cta_button', '', array(
                'className' => 'btn-buy-now',
                'cta_group' => $second_cta_button
              ) ) ?>
            <?php } ?>
            <?php if ( $third_cta_button ) { ?>
              <?php get_template_part( 'template-parts/cta_button', '', array(
                'className' => 'btn-video',
                'cta_group' => $third_cta_button
              ) ) ?>
            <?php } ?>
          </div>
        </div>
      <?php } ?>
    </div>
  <?php } ?>
</div>
<div class="contact-us hide-only-sm">
  <!--  <div class="contact-us-box contact-us-box-email iv-st-from-bottom">-->
  <!--    <svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg">-->
  <!--      <path d="M1.25033 0.25H20.7503C21.0376 0.25 21.3132 0.364137 21.5164 0.567301C21.7195 0.770465 21.8337 1.04602 21.8337 1.33333V18.6667C21.8337 18.954 21.7195 19.2295 21.5164 19.4327C21.3132 19.6359 21.0376 19.75 20.7503 19.75H1.25033C0.963008 19.75 0.687458 19.6359 0.484293 19.4327C0.281129 19.2295 0.166992 18.954 0.166992 18.6667V1.33333C0.166992 1.04602 0.281129 0.770465 0.484293 0.567301C0.687458 0.364137 0.963008 0.25 1.25033 0.25ZM19.667 4.84117L11.0783 12.5328L2.33366 4.81733V17.5833H19.667V4.84117ZM2.88724 2.41667L11.0664 9.63383L19.1275 2.41667H2.88724Z" fill="white"/>-->
  <!--    </svg>-->
  <!--  </div>-->
  <div data-tf-popover="QrBE1v" data-tf-button-color="#13273C" data-tf-button-text="Launch me" data-tf-hidden="utm_source=,utm_campaign=,utm_medium=,gclid=,utm_content=" style="all:unset;"></div>
  <script src="//embed.typeform.com/next/embed.js"></script>
</div>

</section>


<!-- endregion WRAP's Block -->

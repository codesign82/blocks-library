import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/wrap';

const blockScript = async (container = document) => {
  const block = container.querySelector('.hero_block');
  
  // add block code here
  
  const heroVideo = block.querySelector('.hero_block_video_wrapper');
  const defaultDuration = 3;
  let activeIndex = 0;
  const sliders = block.querySelectorAll('.hero-slider');
  const activeSlider = sliders[activeIndex];
  const activeSliderDuration = activeSlider.dataset.sliderDuration;
  const sliderProgress = activeSlider.querySelector('.hero-slider-progress-track');
  activeSlider.classList.add('hero-slider-active');
  
  gsap.fromTo(sliderProgress,
      {scaleX: 0}, {
        scaleX: 1,
        ease: 'linear',
        duration: activeSliderDuration ? activeSliderDuration : defaultDuration,
        onComplete: nextSlider
      })
  
  function nextSlider() {
    sliders[activeIndex].classList.remove('hero-slider-active');
    activeIndex = (activeIndex + 1) % sliders.length;
    // sliders[activeIndex].querySelector('.hero-slider-progress-track').style.width = 0;
    heroVideo.src = sliders[activeIndex].dataset.videoSrc;
    sliders[activeIndex].classList.add('hero-slider-active');
    const activeSliderDuration = sliders[activeIndex].dataset.sliderDuration;
    const sliderProgress = sliders[activeIndex].querySelector('.hero-slider-progress-track');
    gsap.fromTo(sliderProgress, {scaleX: 0}, {
      scaleX: 1,
      ease: 'linear',
      duration: activeSliderDuration ? activeSliderDuration : defaultDuration,
      onComplete: nextSlider
    })
  }
  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);




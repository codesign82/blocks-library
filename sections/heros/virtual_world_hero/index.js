import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/zineone';


/**
 *
 * @param container {Document}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  

  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);

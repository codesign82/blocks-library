<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'about_hero_block';
$className = 'about_hero_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/about_hero_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_title = get_field('main_title');
$description = get_field('description');

$top_right_media = get_field('top_right_media');
$top_right_bollen = @$top_right_media['media_is_video'];
$top_right_image = @$top_right_media['image'];

$top_right_video_type = @$top_right_media['video_type'];
$top_right_file = @$top_right_media['video_file'];
if ($top_right_video_type === 'url') {
  $top_right_file = @$top_right_media['video_url'];
}

$bottom_right_media = get_field('bottom_right_media');
$bottom_right_bollen = @$bottom_right_media['media_is_video'];
$bottom_right_file = @$bottom_right_media['video_file'];
$bottom_right_image = @$bottom_right_media['image'];

$bottom_right_video_type = @$bottom_right_media['video_type'];
$bottom_right_file = @$bottom_right_media['video_file'];
if ($bottom_right_video_type === 'url') {
  $bottom_right_file = @$bottom_right_media['video_url'];
}


$top_center_media = get_field('top_center_media');
$top_center_bollen = @$top_center_media['media_is_video'];
$top_center_file = @$top_center_media['video_file'];
$top_center_image = @$top_center_media['image'];

$top_center_video_type = @$top_center_media['video_type'];
$top_center_file = @$top_center_media['video_file'];
if ($top_center_video_type === 'url') {
  $top_center_file = @$top_center_media['video_url'];
}


$bottom_center_media = get_field('bottom_center_media');
$bottom_center_bollen = @$bottom_center_media['media_is_video'];
$bottom_center_file = @$bottom_center_media['video_file'];
$bottom_center_image = @$bottom_center_media['image'];

$bottom_center_video_type = @$bottom_center_media['video_type'];
$bottom_center_file = @$bottom_center_media['video_file'];
if ($bottom_center_video_type === 'url') {
  $bottom_center_file = @$bottom_center_media['video_url'];
}


$top_left_media = get_field('top_left_media');
$top_left_bollen = @$top_left_media['media_is_video'];
$top_left_file = @$top_left_media['video_file'];
$top_left_image = @$top_left_media['image'];

$top_left_video_type = @$top_left_media['video_type'];
$top_left_file = @$top_left_media['video_file'];
if ($top_left_video_type === 'url') {
  $top_left_file = @$top_left_media['video_url'];
}

$bottom_left_media = get_field('bottom_left_media');
$bottom_left_bollen = @$bottom_left_media['media_is_video'];
$bottom_left_file = @$bottom_left_media['video_file'];
$bottom_left_image = @$bottom_left_media['image'];


$bottom_left_video_type = @$bottom_left_media['video_type'];
$bottom_left_file = @$bottom_left_media['video_file'];
if ($bottom_left_video_type === 'url') {
  $bottom_left_file = @$bottom_left_media['video_url'];
}


?>
<!-- region virtual_world's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="about-hero-wrapper">
    <?php if ($main_title) { ?>
      <h2 class="headline-2 main-title iv-st-from-bottom"><?= $main_title ?></h2>
    <?php } ?>
    <?php if ($description) { ?>
      <h3 class="headline-3 description iv-st-from-bottom"><?= $description ?></h3>
    <?php } ?>
    <div class="gallery-wrapper iv-st-from-bottom">
      <div class="media-wrapper media-wrapper-1 right">
        <?php if ($top_right_bollen) { ?>
          <?php if ($top_right_file) { ?>
            <video class="video" playsinline="" autoplay muted loop >
              <source src="<?= $top_right_file ?>" type="video/mp4">
            </video>
          <?php } ?>
        <?php } else { ?>
          <?php if ($top_right_image) { ?>
            <picture>
              <img src="<?= $top_right_image['url'] ?>" alt="<?= $top_right_image['alt'] ?>">
            </picture>
          <?php } ?>
        <?php } ?>
      </div>
      <div class="media-wrapper media-wrapper-2 right">
        <?php if ($bottom_right_bollen) { ?>
          <?php if ($bottom_right_file) { ?>
            <video class="video"  playsinline="" autoplay loop muted >
              <source src="<?= $bottom_right_file ?>" type="video/mp4">
            </video>
          <?php } ?>
        <?php } else { ?>
          <?php if ($bottom_right_image) { ?>
            <picture>
              <img src="<?= $bottom_right_image['url'] ?>" alt="<?= $bottom_right_image['alt'] ?>">
            </picture>
          <?php } ?>
        <?php } ?>
      </div>
      <div class="media-wrapper media-wrapper-6">
        <?php if ($bottom_center_bollen) { ?>
          <?php if ($bottom_center_file) { ?>
            <video class="video" playsinline="" autoplay loop muted >
              <source src="<?= $bottom_center_file ?>" type="video/mp4">
            </video>
          <?php } ?>
        <?php } else { ?>
          <?php if ($bottom_center_image) { ?>
            <picture>
              <img src="<?= $bottom_center_image['url'] ?>" alt="<?= $bottom_center_image['alt'] ?>">
            </picture>
          <?php } ?>
        <?php } ?>
      </div>
      <div class="media-wrapper media-wrapper-5 right">
        <?php if ($top_center_bollen) { ?>
          <?php if ($top_center_file) { ?>
            <video class="video" playsinline="" autoplay loop muted >
              <source src="<?= $top_center_file ?>" type="video/mp4">
            </video>
          <?php } ?>
        <?php } else { ?>
          <?php if ($top_center_image) { ?>
            <picture>
              <img src="<?= $top_center_image['url'] ?>" alt="<?= $top_center_image['alt'] ?>">
            </picture>
          <?php } ?>
        <?php } ?>
      </div>
      <div class="media-wrapper media-wrapper-3 left">
        <?php if ($top_left_bollen) { ?>
          <?php if ($top_left_file) { ?>
            <video class="video" playsinline="" autoplay muted loop >
              <source src="<?= $top_left_file ?>" type="video/mp4">
            </video>
          <?php } ?>
        <?php } else { ?>
          <?php if ($top_left_image) { ?>
            <picture>
              <img src="<?= $top_left_image['url'] ?>" alt="<?= $top_left_image['alt'] ?>">
            </picture>
          <?php } ?>
        <?php } ?>
      </div>
      <div class="media-wrapper media-wrapper-4 left">
        <?php if ($bottom_left_bollen) { ?>
          <?php if ($bottom_left_file) { ?>
            <video class="video" playsinline="" autoplay loop muted >
              <source src="<?= $bottom_left_file ?>" type="video/mp4">
            </video>
          <?php } ?>
        <?php } else { ?>
          <?php if ($bottom_left_image) { ?>
            <picture>
              <img src="<?= $bottom_left_image['url'] ?>" alt="<?= $bottom_left_image['alt'] ?>">
            </picture>
          <?php } ?>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="hero-shape "></div>
</section>


<!-- endregion virtual_world's Block -->

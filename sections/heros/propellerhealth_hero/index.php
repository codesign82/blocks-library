<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
$versions = get_field('hero_versions');

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'hero_block';
$className = 'hero_block ';
if ($versions) {
    $className .= $versions;
}
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/hero_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 *
 *
 *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$svg = get_field('svg');
$scroll_button = get_field('scroll_button');
?>
<!-- region smart_dv's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="content-wrapper">
    <div class="container">
        <div class="hero-wrapper">
            <div class="left-content">
                <div class="circles-line">
                    <div class="circle"></div>
                    <div class="line"></div>
                    <div class="circle"></div>
                </div>
                <div class="text-wrapper">
                    <?php if (have_rows('custom_breadcrumb')) { ?>
                        <div class="breadcrumb_wrapper">
                            <?php while (have_rows('custom_breadcrumb')) {
                                the_row();
                                $cta_link = get_sub_field('cta_link');
                                ?>
                                <?php if ($cta_link) { ?>
                                    <a href="<?= $cta_link['url'] ?>"
                                       class="breadcrumb-link"><?= $cta_link["title"] ?></a>
                                    <span class="slash">//</span>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php if ($title) { ?>
                        <div class="headline-1 hero-title"><?= $title ?></div>
                    <?php } ?>
                    <?php if ($description) { ?>
                        <div class="paragraph hero-description">
                            <?= $description ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <?php if ($svg) { ?>

                <div class="right-content">
                    <?= $svg ?>
                </div>
            <?php } ?>

        </div>
        <?php if ($scroll_button) { ?>
            <div class="scroll-line">
                <div class="scroll-text"><?= $scroll_button ?></div>
            </div>
        <?php } ?>
    </div>
</div>
</section>

<!-- endregion LoftySky's Block -->

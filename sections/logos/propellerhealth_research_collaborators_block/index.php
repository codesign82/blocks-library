<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'research_collaborators_block';
$className = 'research_collaborators_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/research_collaborators_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$cta_button = get_field('cta_button');
?>
<!-- region propellerhealth Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="research-collaborats">
        <?php if ($title) { ?>
            <div class="headline-2 title iv-st-from-bottom"><?= $title ?></div>
        <?php } ?>
        <?php if ($description) { ?>
            <div class="paragraph paragraph-20 description iv-st-from-bottom">
                <?= $description ?>
            </div>
        <?php } ?>
        <?php if (have_rows('logos_repeater')) { ?>
            <div class="logos iv-st-from-bottom">
                <?php while (have_rows('logos_repeater')) {
                    the_row();
                    $image = get_sub_field('image');
                    $logo_type = get_sub_field('logo_type');
                    $svg = get_sub_field('svg');
                    $logo_link = get_sub_field('logo_link');
                    ?>
                    <?php if ($logo_link) { ?>
                        <a href="<?= $logo_link['url'] ?>"
                           target="<?= $logo_link['target'] ?>"
                           class="square hover-link">
                            <?php if ($logo_type === 'image') { ?>
                                <?php if ($image) { ?>
                                    <picture>
                                        <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>"/>
                                    </picture>
                                <?php } ?>
                            <?php } elseif ($logo_type === 'svg') { ?>
                                <?= $svg ?>
                            <?php } ?>
                        </a>
                    <?php } else { ?>
                        <?php if ($logo_type === 'image') { ?>
                            <?php if ($image) { ?>
                                <picture class="square">
                                    <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>"/>
                                </picture>
                            <?php } ?>
                        <?php } elseif ($logo_type === 'svg') { ?>
                            <div class="square">
                                <?= $svg ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
        <?php } ?>
        <?php if ($cta_button) { ?>
            <a href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>" class="cta-button iv-st-from-bottom">
                <?= $cta_button['title'] ?></a>
        <?php } ?>
    </div>
</div>
</section>


<!-- endregion propellerhealth Block -->

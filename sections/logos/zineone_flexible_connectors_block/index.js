import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import Swiper from "swiper";
import'swiper/scss';
import '../../../scripts/sitesSizer/zineone';



/**
 *
 * @param container {Document}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
const block = container.querySelector('.flexible_connectors_block');
  
  
  const breakpoint = window.matchMedia('(min-width:992px)');
  
  let mySwiper;
  
  const breakpointChecker = function () {
    if (breakpoint.matches === true) {
      // clean up old instances and inline styles when available
      if (mySwiper !== undefined) {
        // console.log(mySwiper);
        // mySwiper.detachEvents();
        mySwiper.destroy(true, true);
      }
      // or/and do nothing
      return false;
    } else {
      return enableSwiper();
    }
  };
  
  const enableSwiper = function () {
    
    mySwiper = new Swiper(block.querySelector('.swiper-container'), {
      // loop: true,
      slidesPerView: 1.3,
      spaceBetween: 20,
      // slidesPerView: 'auto'
      grabCursor: true,
      breakpoints: {
        600: {
          slidesPerView: 3,
          
        },
        
      }
    });
    
  };
  
  breakpoint.addEventListener('change', breakpointChecker);
  
  breakpointChecker();
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);

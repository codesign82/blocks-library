<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$block_versions = get_field( 'block_versions' );
$dataClass      = 'flexible_connectors_block';
$className      = 'flexible_connectors_block ';
$className      .= $block_versions;
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/flexible_connectors_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title       = get_field( 'title' );
$description = get_field( 'description' );
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>

<div class="container g-container-remove-sm-md">
  <div class="text-wrapper g-container-set-sm-md">
    <?php if ( $title ) { ?>
      <h2 class="headline-2"><?= $title ?></h2>
    <?php } ?>
    <?php if ( $description ) { ?>
      <div class="paragraph"><?= $description ?></div>
    <?php } ?>
  </div>
  <?php if ( have_rows( 'text_and_image' ) ) { ?>
    <div class="swiper-container g-container-set-sm-md">
      <div class="swiper-wrapper">
        <?php while ( have_rows( 'text_and_image' ) ) {
          the_row();
          $image = get_sub_field( 'image' );
          $text  = get_sub_field( 'text' ); ?>
          <div class="swiper-slide">
            <div class="flexible_connectors_group box-shadow">
              <?php if ( $image ) { ?>
                <picture class="flexible_connectors_img">
                  <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                </picture>
              <?php } ?>
              <?php if ( $text ) { ?>
                <h3 class="headline-3"><?= $text ?></h3>
              <?php } ?>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  <?php } ?>
</div>
</section>


<!-- endregion ZineOne's Block -->

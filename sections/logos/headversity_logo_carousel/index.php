<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'logo_carousel';
$className = 'logo_carousel';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/logo_carousel/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('block_title');
$description = get_field('description');
$cards = get_field('cards');


?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="block-content">
        <?php if ($title) { ?>
            <h2 move-to-here class="block-title headline-4 iv-st-from-bottom "><?= $title ?></h2>
        <?php } ?>
        <div class="row partners row-cols-2 row-cols-md-<?= count($cards) <= 5 ? count($cards) : 5 ?> ">
            <?php if (have_rows('cards')) {
                while (have_rows('cards')) {
                    the_row();
                    $has_filter = get_sub_field('has_filter');
                    $logo = get_sub_field('logo');
                    $link = get_sub_field('link');
                    $svg_or_image = get_sub_field('svg_or_image');
                    $svg = get_sub_field('svg');
                    $has_filter = $has_filter ? 'has-filter' : '';
                    ?>
                    <div class="col iv-st-from-bottom">
                        <?php if ($link) { ?> <a href="<?= $link['url'] ?>"><?php } ?>
                            <?php if ($svg_or_image === 'svg') { ?>
                                <?php if ($svg) { ?>
                                    <div class="svg-wrapper">
                                        <?= $svg ?>
                                    </div>
                                <?php }
                            } else { ?>
                            <picture class="logo-wrapper" tabindex="0">
                                <?php if ($logo) { ?>
                                    <?= get_acf_image($logo, 'img-105-51', [$has_filter]) ?>
                                <?php }
                                } ?>
                            </picture>
                            <?php if ($link) { ?></a><?php } ?>
                    </div>
                <?php }
            } ?>
        </div>
    </div>
</div>
</section>


<!-- endregion headversity's Block -->

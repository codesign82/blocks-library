import './index.html';
import './style.scss';
import {gsap} from "gsap";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/propellerhealth';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {allowPageScroll, preventPageScroll} from "../../../scripts/functions/prevent_allowPageScroll";

gsap.registerPlugin(ScrollTrigger)
/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.logo_carousel');

  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/propellerhealth';
import Swiper, {Pagination, Grid, Autoplay, Lazy} from "swiper";


const blockScript = async (container = document) => {
  const block = container.querySelector('.image_swiper_block');

  // add block code here
  let swiper = new Swiper(block.querySelector('.swiper'), {
    slidesPerView: 3,
    spaceBetween: 25,
    slidesPerGroup: 3,
    modules: [Grid, Pagination, Autoplay, Lazy],
    preloadImages: false,
    watchSlidesProgress: true,
    lazy: {
      checkInView: true,
      loadPrevNextAmount: 9
    },
    autoplay: {
      delay: 5000
    },
    grid: {
      rows: 3,
      // fill: 'row',
    },
    breakpoints: {

      600: {
        slidesPerGroup: 4,
        slidesPerView: 4,
        grid: {
          rows: 3,
          // fill: 'row',
        },
      },
      992: {
        slidesPerGroup: 5,
        slidesPerView: 5,
        grid: {
          rows: 3,
          // fill: 'row',
        },
      }
    },
    pagination: {
      el: block.querySelector('.swiper-pagination'),
      clickable: true
    },
    longSwipes: true,
    longSwipesRatio: 0.1
  });


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


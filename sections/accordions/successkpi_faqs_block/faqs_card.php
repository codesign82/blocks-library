<?php
$post_id = @$args['post_id'] ?: get_the_ID();
$answer = get_field('answer_faq', $post_id);
$title = get_the_title($post_id);
//$thumbnail_url = get_the_post_thumbnail_url($post_id);
$row_index = @$args['row_index'];

?>

<div class="faq-wrapper iv-st-from-bottom <?= $row_index === 0 ? 'active' : '' ?>">
    <div class="faq-question-title">
        <?php if($title){?>
            <h4 class="headline-4 has-question"><?=$title?></h4>
        <?php } ?>
        <svg class="toggle-open minus-plus" viewBox="0 0 13.6 13.6">
            <line x1="0.8" y1="6.8" x2="12.8" y2="6.8" fill="none" stroke="#fff"
                  stroke-linecap="round" stroke-miterlimit="10"
                  stroke-width="1.6"/>
            <line class="remove-to-close" x1="6.8" y1="12.8" x2="6.8" y2="0.8"
                  fill="none" stroke="#fff" stroke-linecap="round"
                  stroke-miterlimit="10"
                  stroke-width="1.6"/>
        </svg>
        <svg class="toggle-open arrow-icon" viewBox="0 0 14.6 8.6">
            <polyline points="1.3 1.3 7.3 7.3 13.3 1.3" fill="none"
                      stroke="#fff" stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2.62"/>
        </svg>
    </div>
    <?php if($answer){?>
        <div class="have-answer">
            <div class="paragraph faq-answer"><?= $answer?></div>
        </div>
    <?php }?>
</div>

import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {accordion} from "../../../scripts/general/accordion";
import '../../../scripts/sitesSizer/propellerhealth';
import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";
import DrawSVGPlugin from "gsap/DrawSVGPlugin";

gsap.registerPlugin(ScrollTrigger, DrawSVGPlugin)


const blockScript = async (container = document) => {
  const block = container.querySelector('.faqs_block');

    const accordions = block.querySelectorAll('.faq-wrapper')
    accordions.forEach((accordion) => {
        const accordionHead = accordion.querySelector('.faq-question-title');
        const accordionBody = accordion.querySelector('.have-answer');
        accordionHead?.addEventListener('click', (e) => {
            if (!accordionBody) return;
            const isOpened = accordion?.classList.toggle('active');
            if (!isOpened) {
                gsap.to(accordionBody, {height: 0});
            } else {
                gsap.to(Array.from(accordions).map(otherFaq => {
                    const otherFaqBody = otherFaq.querySelector('.have-answer');
                    if (otherFaqBody && accordion !== otherFaq) {
                        otherFaq?.classList.remove('active');
                        gsap.set(otherFaq, {zIndex: 1});
                    }
                    return otherFaqBody;
                }), {height: 0});
                gsap.set(accordion, {zIndex: 2});
                gsap.to(accordionBody, {height: 'auto'});
            }
        });

    });

    animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



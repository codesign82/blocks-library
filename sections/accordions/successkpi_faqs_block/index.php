<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'faqs_block';
$className = 'faqs_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/faqs_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$automatically_or_manually = get_field('automatically_or_manually');
$faqs_posts = get_field('faqs_posts');
$query_options = get_field('query_options');
$have_svg = get_field('have_svg');
$order = @$query_options['order'];
$posts_per_page = @$query_options['number_of_posts'];
$paged = (get_query_var('page_val') ? get_query_var('page_val') : 1);
$note = get_field('note');
$faqs_type = get_field('faqs_type');
$container_width =get_field('container_width');


?>
<!-- region successkpi's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container <?=$container_width === 'wide' ? $container_width : ' ' ?> " >
    <?php if ($title) { ?>
        <div class="headline-2 main-title"><?= $title ?></div>
    <?php } ?>
    <?php if ($faqs_type === 'faqs_cpt') { ?>
        <?php
        $args = array(
            'paged' => $paged,
            'post_type' => 'faqs',
            'posts_per_page' => $posts_per_page,
            'order' => $order,
            'post_status' => 'publish'
        );
        // The Query
        $the_query = new WP_Query($args);
        $have_posts = $the_query->have_posts(); ?>
        <?php if ($automatically_or_manually === 'manually') { ?>
            <?php
            if ($faqs_posts): ?>
                <div class="faqs-content-wrapper">
                    <?php foreach ($faqs_posts as $index=>$faq):
                        get_template_part("template-parts/faqs-card", '', array('post_id' => $faq, 'row_index'=>$index));
                        ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        <?php } else { ?>
            <?php if ($have_posts) { ?>
                <div class="faqs-content-wrapper ">
                    <?php while ($the_query->have_posts()) {
                        $the_query->the_post();
                        get_template_part("template-parts/faqs-card", '', array('post_id' => get_the_ID(), 'row_index'=> $the_query->current_post));
                        ?>
                    <?php } ?>
                </div>
            <?php }
            /* Restore original Post Data */
            wp_reset_postdata(); ?>

            <?php $args['paged'] = 2; ?>
            <div
                    class="no-posts headline-2 <?= $the_query->have_posts() ? '' : 'active' ?>">
                <?= __('No faqs Here :(', 'successkpi') ?></div>
            <div
                    class="load-more-wrapper  <?= $the_query->max_num_pages <= 1 ? "hidden" : "" ?>"
                    data-args='<?= json_encode($args) ?>'
                    data-template="template-parts/faq-card">
                <button aria-label="Load More Posts"
                        class="cta-button load-more-btn"><?= __('Load More', 'successkpi') ?>
                </button>
                <div class="loader">
                    <div class="loader-ball"></div>
                    <div class="loader-ball"></div>
                    <div class="loader-ball"></div>
                </div>
            </div>
        <?php } ?>
    <?php } else { ?>

        <div class='faqs-content-wrapper'>
            <?php if (have_rows('manual_faqs_repeater')) { ?>
                <?php while (have_rows('manual_faqs_repeater')) {
                    the_row();
                    $faq_question = get_sub_field('faq_question');
                    $faq_answer = get_sub_field('faq_answer');

                    ?>
                    <div class='faq-wrapper iv-st-from-bottom <?= get_row_index() === 1 ? 'active' : '' ?>'>

                        <div class='faq-question-title'>
                            <?php if ('$faq_question') { ?>
                                <h4 class="headline-4 has-question"><?= $faq_question ?></h4>
                            <?php } ?>
                            <svg class="toggle-open minus-plus" viewBox="0 0 13.6 13.6">
                                <line x1="0.8" y1="6.8" x2="12.8" y2="6.8" fill="none"
                                      stroke="#fff"
                                      stroke-linecap="round" stroke-miterlimit="10"
                                      stroke-width="1.6"/>
                                <line class="remove-to-close" x1="6.8" y1="12.8" x2="6.8"
                                      y2="0.8"
                                      fill="none" stroke="#fff" stroke-linecap="round"
                                      stroke-miterlimit="10"
                                      stroke-width="1.6"/>
                            </svg>
                        </div>

                        <?php if ('$faq_answer') { ?>
                            <div class="have-answer" style='height : <?= get_row_index() === 1 ? 'auto' : '0' ?>'>
                                <div class="paragraph faq-answer"><?= $faq_answer ?></div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>


    <?php } ?>
    <?php if ($note) { ?>
        <div class="paragraph note iv-st-from-bottom"><?= $note ?></div>
    <?php } ?>
</div>

</section>


<!-- endregion successkpi's Block -->
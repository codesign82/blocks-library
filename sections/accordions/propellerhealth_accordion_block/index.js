import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {accordion} from "../../../scripts/general/accordion";
import {requestBodyGenerator} from "../../../scripts/functions/requestBodyGenerator";
import {stringToHTML} from "../../../scripts/functions/stringToHTML";
import {vimeoPlayer} from "../../../scripts/general/vimeo-player";
import {youtubePlayer} from "../../../scripts/general/youtube-player";
import {v4 as uuidv4} from 'uuid';
import '../../../scripts/sitesSizer/propellerhealth';
import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";
import DrawSVGPlugin from "gsap/DrawSVGPlugin";

gsap.registerPlugin(ScrollTrigger, DrawSVGPlugin)


const blockScript = async (container = document) => {
  const block = container.querySelector('.accordion_block');

    const players = {};
    let activeAccordion;

    const accordion = (accordions) =>   {
        if (accordions.length === 0) return;
        accordions.forEach((accordion) => {
            if (!accordion.id) {
                accordion.id = `faq-${uuidv4()}`;
            }
            const accordionHead = accordion.querySelector('.accordion-head');
            const accordionBody = accordion.querySelector('.accordion-body');
            accordionHead?.addEventListener('click', (e) => {
                if (!accordionBody) {
                    return;
                }
                accordion?.classList.toggle('accordion-active');
                if (activeAccordion === accordion.id) {
                    gsap.to(accordionBody, {height: 0});
                    activeAccordion = '';
                } else {
                    if (activeAccordion) {
                        const currentActiveAccordion = block.querySelector(`#${activeAccordion}`);
                        currentActiveAccordion.classList.remove('accordion-active');
                        gsap.set(currentActiveAccordion, {zIndex: 1});
                        gsap.to(currentActiveAccordion.querySelector('.accordion-body'), {height: 0});
                    }
                    gsap.set(accordion, {zIndex: 2});
                    gsap.to(accordionBody, {height: 'auto'});
                    activeAccordion = accordion.id;
                }
            });
        });
    };

    const runVideo = (id, videoType) => {
        const player = players[id];
        block.querySelector(`#${id} .poster-image`)?.classList.add('active');
        block.querySelector(`#${id} .play-btn`)?.classList.add('active');
        if (!['video_file', 'video_url'].includes(videoType)) {
            player.setVolume(1);
        } else {
            player.volume = 1;
        }
        player.play();
    };

    const pauseVideo = (id) => {
        const player = players[id];
        player.pause();
    };

    const initVideoInAccordion = (accordion) => {
        const id = `faq-${uuidv4()}`;
        accordion.id = id;
        accordion.addEventListener("click", () => {
            if (!accordion.classList.contains("accordion-active")) {
                pauseVideo(id);
            }
        });

        const videoPlayer = accordion.querySelector(".video-player");
        const videoType = videoPlayer.dataset.videoType;
        const videoURL = videoPlayer.dataset.videoUrl;
        if (videoType === 'vimeo') {
            const player = vimeoPlayer(videoPlayer, videoURL);
            players[id] = player;
            player.on('playing', () => {
                accordion.querySelector('.play-btn')?.classList.add('active');
            });
        } else if (videoType === 'youtube') {
            const player = youtubePlayer(videoPlayer, videoURL, {
                annotations: false,
                modestBranding: false,
                related: false
            });
            players[id] = player;
            player.on('playing', () => {
                accordion.querySelector('.play-btn')?.classList.add('active');
            });
        } else {
            players[id] = videoPlayer;
        }

        accordion.querySelector(".media-wrapper").addEventListener("click", (e) => {
            runVideo(id, videoType);
        }, {once: true});
    };

    block.querySelectorAll('.accordion-video').forEach((accordion) => {
        initVideoInAccordion(accordion);
    });


    accordion(block.querySelectorAll('.accordion-card'));

    const postsContainer = block.querySelector('[posts-container]');
    const loadMore = block.querySelector('.load-more-wrapper');
    const noPosts = block.querySelector('.no-posts');
    let loadMoreStatus = !loadMore.classList.contains('hidden');


    if (loadMore) {
        loadMore.addEventListener('click', async () => {
            if (loadMore.classList.contains('loading')) return;
            loadMore.classList.add('loading');
            loadMore.classList.remove('hidden');
            noPosts.classList.remove('active');
            const response = await fetch(theme_ajax_object.ajax_url, {
                method: 'POST',
                body: requestBodyGenerator(loadMore),
            });
            loadMore.classList.remove('loading');
            const hasMorePages = !!response.headers.get('X-WP-Has-More-Pages');
            const totalPages = +response.headers.get('X-WP-Total-Pages');
            noPosts.classList.toggle('active', totalPages === 0);
            loadMore.classList.toggle('hidden', !hasMorePages);
            loadMoreStatus = hasMorePages;
            const htmlString = await response.json();
            const oldArgs = JSON.parse(loadMore.dataset.args);
            oldArgs.paged++;
            loadMore.dataset.args = JSON.stringify(oldArgs);
            const posts = stringToHTML(htmlString.data);
            for (let post of posts) {
                postsContainer.appendChild(post);
                if (post.classList.contains("accordion-video")) {
                    initVideoInAccordion(post);
                }
            }
            accordion(posts);
            animations(posts);
            imageLazyLoading(posts);
        });
    }

    animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



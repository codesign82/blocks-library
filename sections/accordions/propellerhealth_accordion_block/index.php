<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$tight_content = get_field('tight_content');
$tight_content = $tight_content ? ' tight-content' : ' ';
$dataClass = 'accordion_block';
$className = 'accordion_block';
//$className .= ' dark-background';
$className .= $tight_content;

if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/accordion_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$option_title = get_field('option_title');
$description = get_field('description');
$automatically_or_manually = get_field('automatically_or_manually');
$faqs_posts = get_field('faqs_posts');
$query_options = get_field('query_options');
$have_svg = get_field('have_svg');
$order = @$query_options['order'];
$posts_per_page = @$query_options['number_of_posts'];
$video_type = get_field('video_type');
$paged = (get_query_var('page_val') ? get_query_var('page_val') : 1);

?>
<!-- region propellerhealth Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="accordion-cards-wrapper-center">
        <div class="accordion-cards iv-st-from-bottom">
            <div class="accordion-left">
                <?php if ($title) { ?>
                    <h2 class="headline-2 large-title small-title accordion-title"> <?= $title ?></h2>
                <?php } ?>
            </div>
            <div class="accordion-right">
                <?php if ($description) { ?>
                    <div class="accordion-description headline-4">
                        <?= $description ?>
                    </div>
                <?php } ?>
            </div>
        </div>
        <?php
        $args = array(
            'paged' => $paged,
            'post_type' => 'faqs',
            'posts_per_page' => $posts_per_page,
            'order' => $order,
            'post_status' => 'publish'
        );
        // The Query
        $the_query = new WP_Query($args);
        $have_posts = $the_query->have_posts(); ?>
        <?php if ($automatically_or_manually) { ?>
            <?php
            if ($faqs_posts): ?>
                <div class="accordion-cards-wrapper">
                    <?php foreach ($faqs_posts as $faq):
                        get_template_part("template-parts/faq-card", '', array('post_id' => $faq));
                        ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        <?php } else { ?>
            <?php if ($have_posts) { ?>
                <div class="accordion-cards-wrapper" posts-container>
                    <?php while ($the_query->have_posts()) {
                        $the_query->the_post();
                        get_template_part("template-parts/faq-card", '', array('post_id' => get_the_ID()));
                        ?>
                    <?php } ?>
                </div>
            <?php }
            /* Restore original Post Data */
            wp_reset_postdata(); ?>

            <?php $args['paged'] = 2; ?>
            <div class="no-posts headline-2 <?= $the_query->have_posts() ? '' : 'active' ?>">
                <?= __('No faqs Here :(', 'propellerhealth') ?></div>
            <div class="load-more-wrapper  <?= $the_query->max_num_pages <= 1 ? "hidden" : "" ?>"
                 data-args='<?= json_encode($args) ?>'
                 data-template="template-parts/faq-card">
                <button aria-label="Load More Posts" class="cta-button load-more-btn"><?= __('Load More', 'propellerhealth_client') ?>
                </button>
                <div class="loader">
                    <div class="loader-ball"></div>
                    <div class="loader-ball"></div>
                    <div class="loader-ball"></div>
                </div>
            </div>
        <?php } ?>
        <?php if ($have_svg) { ?>
            <svg class="dots-animation-i-e site-dots" fill="none">
                <path class="dot-line-animation" d="" stroke="#90E2C8" stroke-width="3" stroke-linecap="round" stroke-dasharray="1 10 1 10"/>
            </svg>

        <?php } ?>
    </div>
</div>
</section>


<!-- endregion propellerhealth Block -->

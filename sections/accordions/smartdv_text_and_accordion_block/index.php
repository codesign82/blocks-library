<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'text_and_accordion_block';
$className = 'text_and_accordion_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/text_and_accordion_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_title = get_field('main_title');
$main_description = get_field('main_description');
$select_category = get_field('select_category');
$index = 0;
?>
<!-- region samrt_dv Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<?php


?>
<div class="container">
    <div class="text-and-accordion-wrapper">
        <div class="left-content">
            <?php if ($main_title) { ?>
                <div class="headline-2 main-title word-up"><?= $main_title ?></div>
            <?php } ?>
            <?php if ($main_description) { ?>
                <div class="paragraph main_description iv-st-from-bottom">
                    <?= $main_description ?>
                </div>
            <?php } ?>
        </div>
        <?php if (have_rows('accordion_repeater')) { ?>
            <div class="cards-wrapper">
                <?php

                while (have_rows('accordion_repeater')) {
                    the_row();
                    $icon = get_sub_field('icon');
                    $title = get_sub_field('title');
                    $description = get_sub_field('description');
                    $cta_link = get_sub_field('cta_link');
                    $number = get_row_index();
                    $ones = ($number % 10);
                    $tenth = floor($number / 10);

                    ?>
                    <div class="card-svg-title iv-st-from-bottom">
                        <div class="accordion-head card-action">
                            <div class="icon-text-wrapper">

                                <div class="icon-wrapper  <?= $number <= 9 ? 'ones-' . $ones : 'tens-' . $tenth . ' ones-' . $ones . ' bigger-width' ?>">
                                    <div class="green-line"></div>
                                </div>

                                <?php if ($title) { ?>
                                    <div class="title-card headline-4"><?= $title ?></div>
                                <?php } ?>
                            </div>
                            <div class="hover-action">
                                <svg class="toggle-open minus-plus in-desk"
                                     viewBox="0 0 13.6 13.6">
                                    <line x1="0.8" y1="6.8" x2="12.8" y2="6.8" fill="none"
                                          stroke="#fff" stroke-linecap="round"
                                          stroke-miterlimit="10"
                                          stroke-width="1.6"/>
                                    <line class="remove-to-close" x1="6.8" y1="12.8" x2="6.8"
                                          y2="0.8" fill="none" stroke="#fff"
                                          stroke-linecap="round" stroke-miterlimit="10"
                                          stroke-width="1.6"/>
                                </svg>
                            </div>
                        </div>
                        <?php if ($description) { ?>
                            <div class="description-card paragraph accordion-body">
                                <?= $description ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>
</section>


<!-- endregion samrt_dv Block -->

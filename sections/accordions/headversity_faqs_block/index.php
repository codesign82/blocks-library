<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'faqs_block';
$className = 'faqs_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/faqs_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$faqs = get_field('faqs');
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="ellipse ellipse-orange"></div>
<div class="container">
    <div class="faqs-content-wrapper">
        <?php if ($title) { ?>
            <h3 move-to-here class="headline-3 title text-center iv-st-from-bottom"><?= $title ?></h3>
        <?php } ?>
        <?php
        if ($faqs): ?>
            <div class="faqs-wrapper">
                <?php foreach ($faqs as $faq):
                    $question = get_field('question', $faq);
                    $answer = get_field('answer', $faq);
                    ?>
                    <div class="faq-wrapper iv-st-from-bottom" itemscope itemprop="mainEntity"
                         itemtype="https://schema.org/Question">
                        <?php if ($question) { ?>
                            <div class="faq-question-title">
                                <h4 class="headline-5 has-question color-transition"><?= $question ?></h4>
                                <svg class="toggle-open minus-plus" viewBox="0 0 13.6 13.6">
                                    <line x1="0.8" y1="6.8" x2="12.8" y2="6.8" fill="none" stroke="#fff"
                                          stroke-linecap="round" stroke-miterlimit="10"
                                          stroke-width="1.6"/>
                                    <line class="remove-to-close" x1="6.8" y1="12.8" x2="6.8" y2="0.8" fill="none"
                                          stroke="#fff" stroke-linecap="round" stroke-miterlimit="10"
                                          stroke-width="1.6"/>
                                </svg>
                            </div>
                        <?php } ?>
                        <?php if ($answer) { ?>
                            <div class="have-answer" itemscope itemprop="acceptedAnswer"
                                 itemtype="https://schema.org/Answer">
                                <div class="paragraph  faq-answer"><?= $answer ?></div>
                            </div>
                        <?php } ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
</section>


<!-- endregion headversity's Block -->

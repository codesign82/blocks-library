<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'faqs_block';
$className = 'faqs_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/faqs_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_title = get_field('main_title');
$automatically_or_manual = get_field('automatically_or_manual');
$faqs = get_field('faqs');
$query_options = get_field('query_options');
$order = @$query_options['order'];
$posts_per_page = @$query_options['number_of_posts'];
$args = array(
    'post_type' => 'faqs',
    'posts_per_page' => $posts_per_page,
    'order' => $order,
    'post_status' => 'publish'
);

// The Query
$the_query = new WP_Query($args);
$have_posts = $the_query->have_posts();

?>
<!-- region samrt_dv Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="cards-wrapper dark-background">
        <?php if ($main_title) { ?>
            <div class="title headline-2 word-up "><?= $main_title ?></div>
        <?php } ?>
        <?php if ($automatically_or_manual) { ?>
            <?php if ($faqs) { ?>
                <div class="faqs_cards">
                    <?php foreach ($faqs as $faq):
                        get_template_part("template-parts/faqs-card", '', array('post_id' => $faq));
                        ?>
                    <?php endforeach; ?>
                </div>
            <?php } ?>
        <?php } else { ?>
            <?php if ($have_posts) { ?>
                <div class="faqs_cards" posts-container>
                    <?php while ($the_query->have_posts()) {
                        $the_query->the_post();
                        get_template_part("template-parts/faqs-card", '', array('post_id' => get_the_ID()));
                        ?>
                    <?php } ?>
                </div>
            <?php }
            /* Restore original Post Data */
            wp_reset_postdata(); ?>
            <?php $args['paged'] = 2; ?>
        <?php } ?>
    </div>
</div>
</section>


<!-- endregion samrt_dv Block -->

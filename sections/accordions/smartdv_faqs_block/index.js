import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {accordion} from "../../../scripts/general/accordion";
import {gsap} from "gsap";

import '../../../scripts/sitesSizer/propellerhealth';



const blockScript = async (container = document) => {
  const block = container.querySelector('.faqs_block');

  accordion(block.querySelectorAll('.faq-card'))


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



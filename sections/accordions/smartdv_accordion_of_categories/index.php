<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'accordion_of_categories';
$className = 'accordion_of_categories';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/accordion_of_categories/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_title = get_field('main_title');
$main_description = get_field('main_description');
$select_category = get_field('select_category');

//global $post;
//$post_id = get_the_ID();

?>

<!-- region samrt_dv Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<?php
$terms = get_terms(array(
    'taxonomy' => $select_category,
    'hide_empty' => false,
));
?>
<div class="container">
    <div class="card-content">
        <?php if ($main_title) { ?>
            <div class="headline-2 title  word-up"><?= $main_title ?></div>
        <?php } ?>
        <?php if ($main_description) { ?>
            <div class="description paragraph iv-st-from-bottom">
                <?= $main_description ?>
            </div>
        <?php } ?>
    </div>
    <div class="cards-wrapper">
        <?php if (!empty($terms) && is_array($terms)) { ?>
            <?php foreach ($terms as $term) {
                $term_icon = get_field('icon', 'term_' . $term->term_id);
                $svg = @$term_icon['svg'];
                ?>
                <div class="card-svg-title iv-st-from-bottom">
                    <div class="accordion-head card-action">
                        <div class="icon-text-wrapper">
                            <div class="icon-wrapper">
                                <?= $svg ?>
                            </div>
                            <div class="title-card headline-4"><?= $term->name ?></div>
                        </div>
                        <?php if ($term->description) { ?>
                            <div class="hover-action">
                                <svg class="toggle-open minus-plus in-desk"
                                     viewBox="0 0 13.6 13.6">
                                    <line x1="0.8" y1="6.8" x2="12.8" y2="6.8" fill="none"
                                          stroke="#fff" stroke-linecap="round"
                                          stroke-miterlimit="10"
                                          stroke-width="1.6"/>
                                    <line class="remove-to-close" x1="6.8" y1="12.8" x2="6.8"
                                          y2="0.8" fill="none" stroke="#fff"
                                          stroke-linecap="round" stroke-miterlimit="10"
                                          stroke-width="1.6"/>
                                </svg>
                            </div>
                        <?php } ?>
                    </div>
                    <?php if ($term->description) { ?>
                        <div class="description-card paragraph accordion-body">
                            <?= $term->description ?>
                            <a class='cta-link' href=" <?= get_term_link($term) ?>">
                                <?= __("Learn More", "smart-dv") ?>
                                <svg class='arrow-link' width='23' height='12' viewBox='0 0 23 12' fill='none'>
                                    <rect y='5' width='13' height='2' fill='#BFD730'/>
                                    <path d='M13 0L23 6L13 12V0Z' fill='#BFD730'/>
                                </svg>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>
</div>
</section>


<!-- endregion samrt_dv Block -->

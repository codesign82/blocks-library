<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'question_and_answer_block';
$className = 'question_and_answer_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/question_and_answer_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$first_title = '';
$first_result = '';
$first_type = '';
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<div class="container">
    <?php if (have_rows('faq_card')) { ?>
        <div class="faqs-content-wrapper">
            <div class="left-content">
                <?php while (have_rows('faq_card')) {
                    the_row();
                    $svg_or_image = get_sub_field('svg_or_image');
                    $svg = get_sub_field('svg');
                    $image = get_sub_field('image');
                    $card_title = get_sub_field('card_title');
                    $card_description = get_sub_field('card_description');
                    $video_or_image = get_sub_field('video_or_image');
                    $type_of_video = get_sub_field('type_of_video');
                    $video_file = get_sub_field('video_file');
                    $video_url = get_sub_field('video_url');
                    $youtube_url = get_sub_field('youtube_url');
                    $image_result = get_sub_field('image_result');
                    if ($type_of_video == 'url') {
                        $video_result = $video_url;
                    } elseif ($type_of_video == 'youtube') {
                        $video_result = generateVideoEmbedUrl($youtube_url);
                    } else {
                        $video_result = $video_file;
                    }
                    $final = $video_or_image === 'video' ? $video_result : $image_result;
                    if (get_row_index() === 1) {
                        $first_title = $card_title;
                        $first_result = $final;
                        $first_type = $video_or_image;
                    }
                    ?>
                    <div class="faq-wrapper iv-st-from-bottom <?= $youtube_url ? 'youtube' : '' ?>"
                         data-url="<?= $final ?>" data-url-type="<?= $video_or_image ?>">
                        <div class="faq-question-title">
                            <?php if (!$svg_or_image) { ?>
                                <div class="icon-wrapper">
                                    <?= $svg ?>
                                </div>
                            <?php } else { ?>
                                <?php if ($image) { ?>
                                    <picture class="icon-wrapper">
                                        <img src="<?= $image['url'] ?>" alt="">
                                    </picture>
                                <?php } ?>
                            <?php } ?>
                            <?php if ($card_title) { ?>
                                <h4 class="paragraph has-question"><?= $card_title ?></h4>
                            <?php } ?>
                            <div class="open-toggle-icon">
                                <svg width="3.9rem" height="3.9rem" viewBox="0 0 39 39" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M10.3211 14.2196C11.0827 13.4581 12.3173 13.4581 13.0789 14.2196L19.5 20.6407L25.9211 14.2196C26.6827 13.4581 27.9173 13.4581 28.6789 14.2196C29.4404 14.9811 29.4404 16.2158 28.6789 16.9773L20.8789 24.7773C20.1173 25.5388 18.8827 25.5388 18.1211 24.7773L10.3211 16.9773C9.55962 16.2158 9.55962 14.9811 10.3211 14.2196Z"
                                          fill="white"/>
                                </svg>
                            </div>
                        </div>
                        <?php if ($card_description) { ?>
                            <div class="have-answer list paragraph paragraph-normal-paragraph">
                                <div class="have-answer-spacer"></div>
                                <?= $card_description ?>
                                <?php if ($video_or_image === 'video') { ?>
                                    <?php if ($type_of_video !== 'youtube') { ?>
                                        <div class="aspect-ratio card-content iv-st-from-bottom">
                                            <video playsinline class="home-video" src="<?= $final ?>"
                                                   type="video/mp4"></video>
                                            <div class="play-button">
                                                <svg class="play" width="27" height="30" viewBox="0 0 27 30"
                                                     fill="none">
                                                    <path d="M24.6758 12.5817C26.6758 13.7364 26.6758 16.6232 24.6758 17.7779L5.18146 29.0329C3.18146 30.1876 0.681463 28.7443 0.681463 26.4349L0.681463 3.92476C0.681463 1.61535 3.18146 0.171979 5.18146 1.32668L24.6758 12.5817Z"
                                                          fill="#0A0A0A"/>
                                                </svg>
                                                <svg class="pause" width="26" height="28" viewBox="0 0 26 28"
                                                     fill="none">
                                                    <rect width="9" height="28" rx="3" fill="#0A0A0A"/>
                                                    <rect x="17" width="9" height="28" rx="3" fill="#0A0A0A"/>
                                                </svg>
                                            </div>
                                        </div>
                                    <?php } else {
                                        ?>
                                        <div class="youtube-aspect-ratio iv-st-from-bottom">
                                            <iframe src="<?= $final ?>" frameborder="0"
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                    allowfullscreen></iframe>
                                        </div>
                                        <?php
                                    } ?>
                                <?php } else { ?>
                                    <picture class="content-image aspect-ratio iv-st-from-bottom">
                                        <img class="home-img" src="<?= $final ?>"
                                             alt="<?= __('Answer Image', 'raistone') ?>">
                                    </picture>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
            <div class="right-content video-content <?= $first_type === 'video' ? 'content-active' : '' ?>">
                <div class="content-video">
                    <h2 class="paragraph title iv-st-from-bottom"><?= $first_title ?></h2>
                    <div class="aspect-ratio card-content iv-st-from-bottom">
                        <video playsinline class="home-video" src="<?= $first_result ?>" type="video/mp4"></video>
                        <div class="play-button">
                            <svg class="play" width="27" height="30" viewBox="0 0 27 30" fill="none">
                                <path d="M24.6758 12.5817C26.6758 13.7364 26.6758 16.6232 24.6758 17.7779L5.18146 29.0329C3.18146 30.1876 0.681463 28.7443 0.681463 26.4349L0.681463 3.92476C0.681463 1.61535 3.18146 0.171979 5.18146 1.32668L24.6758 12.5817Z"
                                      fill="#0A0A0A"/>
                            </svg>
                            <svg class="pause" width="26" height="28" viewBox="0 0 26 28" fill="none">
                                <rect width="9" height="28" rx="3" fill="#0A0A0A"/>
                                <rect x="17" width="9" height="28" rx="3" fill="#0A0A0A"/>
                            </svg>
                        </div>
                    </div>
                    <div class="youtube-aspect-ratio iv-st-from-bottom">
                        <iframe src="<?= $first_result ?>" frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="right-content image-content <?= $first_type === 'image' ? 'content-active' : '' ?>">
                <picture class="content-image aspect-ratio iv-st-from-bottom">
                    <img class="home-img" src="<?= $first_result ?>" alt="<?= __('Answer Image', 'raistone') ?>">
                </picture>
            </div>
        </div>
    <?php } ?>
</div>
</section>


<!-- endregion raistone's Block -->

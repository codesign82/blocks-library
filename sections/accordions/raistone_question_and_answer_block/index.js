import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/raistone';

import {gsap} from "gsap";

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.question_and_answer_block');

  const video = block?.querySelector(".video-content video");
  const iframe = block?.querySelector(".video-content iframe");
  const image = block?.querySelector(".image-content img");
  const videoTitle = block.querySelector(".right-content .title")
  const videoContent = block.querySelector(".right-content .card-content")
  const youtubeContent = block.querySelector(".right-content .youtube-aspect-ratio")
  const firstAccordion = block.querySelectorAll('.faq-wrapper')[0];

  if (firstAccordion.classList.contains('youtube')) {
    videoContent.style.display = 'none';
    youtubeContent.style.display = 'block';
  } else {
    youtubeContent.style.display = 'none';
    videoContent.style.display = 'block';
  }
  const accordions = block.querySelectorAll('.faq-wrapper')
  accordions.forEach((accordion) => {
    const accordionHead = accordion.querySelector('.faq-question-title');
    const accordionBody = accordion.querySelector('.have-answer');
    accordionHead?.addEventListener('click', (e) => {
      if (!accordionBody) return;
      const isOpened = accordion?.classList.toggle('active');
      videoContent.classList.remove('video-active')
      if (!isOpened) {
        gsap.to(accordionBody, {height: 0});
      } else {
        gsap.to(Array.from(accordions).map(otherFaq => {
          const otherFaqBody = otherFaq.querySelector('.have-answer');
          if (otherFaqBody && accordion !== otherFaq) {
            otherFaq?.classList.remove('active');
            gsap.set(otherFaq, {zIndex: 1});
          }
          return otherFaqBody;
        }), {height: 0});
        gsap.set(accordion, {zIndex: 2});
        gsap.to(accordionBody, {height: 'auto'});
        // region video or image
        const resultType = accordion.dataset.urlType;
        const resultURL = accordion.dataset.url;
        block.querySelector('.content-active')?.classList.remove('content-active');
        block.querySelector(`.${resultType}-content`)?.classList.add('content-active');
        if (accordion.classList.contains('youtube')) {
          videoContent.style.display = 'none';
          youtubeContent.style.display = 'block';
        } else {
          youtubeContent.style.display = 'none';
          videoContent.style.display = 'block';
          const faqContent = accordionBody.querySelector(".card-content");
          const faqvideo = faqContent?.querySelector("video");
          faqContent?.addEventListener('click', () => {
            faqContent.classList.toggle('video-active');
            if (faqContent.classList.contains('video-active')) {
              faqvideo.play();
            } else {
              faqvideo.pause();
            }
          });
          faqvideo?.addEventListener('ended', () => {
            faqContent.classList.remove('video-active');
          });
        }
        if (resultType === 'video') {
          image.src = '';
          if (video.src !== resultURL) {
            if (accordion.classList.contains('youtube')) {
              video.src = '';
              iframe.src = resultURL;
            } else {
              iframe.src = '';
              video.src = resultURL;
            }
            videoTitle.textContent = accordion.querySelector(".has-question").textContent;
            gsap.timeline()
                .fromTo(videoContent, {
                  autoAlpha: 0,
                  scale: .8
                }, {
                  autoAlpha: 1,
                  scale: 1
                }, "<")
                .fromTo(videoTitle, {
                  autoAlpha: 0,
                }, {
                  autoAlpha: 1,
                }, "<")
          }
        } else {
          video.src = '';
          if (image.src !== resultURL) {
            image.src = resultURL;
            gsap.timeline()
                .fromTo(image, {
                  autoAlpha: 0,
                  scale: .8
                }, {
                  autoAlpha: 1,
                  scale: 1
                }, "<")
          }
        }
        // endregion video or image
      }
    });
  });

  // When click on video buttons


  const videoButton = block.querySelector(".play-button");
  videoContent?.addEventListener('click', () => {
    videoContent.classList.toggle('video-active');
    if (videoContent.classList.contains('video-active')) {
      video.play();
    } else {
      video.pause();
    }
  });

  video?.addEventListener('ended', () => {
    videoContent.classList.remove('video-active');
  });
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


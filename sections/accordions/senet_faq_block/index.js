import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";

import '../../../scripts/sitesSizer/propellerhealth';
import $ from 'jquery';

const blockScript = async (container = document) => {
  const block = container.querySelector('.faq_block');

    let action = "click";
    let speed = "500";

    $(block).ready(function () {

        // Question handler
        $('.accordion-head').on(action, function () {
            $(this).next().slideToggle(speed).closest('.card-faq').toggleClass("active").siblings().removeClass("active").find('.accordion-body').slideUp();

        });
    });

    animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



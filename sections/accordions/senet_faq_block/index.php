<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'faq_block';
$className = 'faq_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/takeoff-code/gutenberg-blocks/faq_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$left_title = get_field('left_title');
$right_title = get_field('right_title');
$description_right = get_field('description_right');
//button_text
?>
<!-- region senet's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="cards-wrapper">
        <div class="left-content">
            <?php if ($left_title) { ?>
                <h2 class="title headline-2"><?= $left_title ?></h2>
            <?php } ?>
            <div class="faqs-content">
                <?php if (have_rows('qa_repeater')) {
                    ?>
                    <?php while (have_rows('qa_repeater')) {
                        the_row();
                        $title = get_sub_field('title');
                        $description = get_sub_field('description');
                        ?>
                        <div class="card-faq <?= get_row_index() === 1 ? "active" : "" ?>">
                            <div class="content-svg accordion-head">
                                <?php if ($title) { ?>
                                    <h3 class="title-faq  faq-title-accordion"><?= $title ?></h3>
                                <?php } ?>
                                <svg class="action-svg" width="14" height="8" viewBox="0 0 14 8" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 0.999999L7 7L13 1" stroke="#FF5900" stroke-width="2"
                                          stroke-linecap="round"
                                          stroke-linejoin="round"/>
                                </svg>
                            </div>
                            <?php if ($description) { ?>
                                <div class="accordion-body paragraph"
                                     style="display: <?= get_row_index() === 1 ? "block" : "none" ?>">
                                    <?= $description ?>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>

        </div>
        <div class="line-center">

        </div>
        <div class="right-content">
            <?php if ($right_title) { ?>
                <h2 class="headline-2 right-title">
                    <?= $right_title ?>
                </h2>
            <?php } ?>
            <?php if ($description_right) { ?>
                <div class="description-right paragraph">
                    <?= $description_right ?>
                </div>
            <?php } ?>
            <div class="cards-link">
                <?php if (have_rows('buttons')) {
                    ?>
                    <?php while (have_rows('buttons')) {
                        the_row();
                        $cta = get_sub_field('cta');
                        $button_type = get_sub_field('button_type');
                        $button_text = get_sub_field('button_text');

                        ?>
                        <?php if ($button_type === "CTA") { ?>
                            <?php if ($cta) { ?>
                                <a class="cta-link" href="<?= $cta['url'] ?>"
                                   target="<?= $cta['target'] ?>"><?= $cta['title'] ?>
                                    <svg width="22" height="11" viewBox="0 0 22 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M15.3292 3.54817H15.4542V3.42317V1.17424C15.4542 0.24156 16.5818 -0.225521 17.2413 0.433966L21.4434 4.63607L21.5318 4.54768L21.4434 4.63607C21.8522 5.04487 21.8523 5.70771 21.4434 6.11656L17.2413 10.3187C16.5818 10.9782 15.4542 10.5111 15.4542 9.57839V7.32942V7.20442H15.3292H0.585938C0.331389 7.20442 0.125 6.99803 0.125 6.74348V4.0091C0.125 3.75455 0.331389 3.54817 0.585938 3.54817H15.3292Z" fill="#FF5900" stroke="#FF5900" stroke-width="0.25"/>
                                    </svg>
                                </a>
                            <?php } ?>
                        <?php } else if ($button_text) { ?>
                            <button class="cta-link open-contact-us-form"><?= $button_text ?>
                                <svg width="22" height="11" viewBox="0 0 22 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M15.3292 3.54817H15.4542V3.42317V1.17424C15.4542 0.24156 16.5818 -0.225521 17.2413 0.433966L21.4434 4.63607L21.5318 4.54768L21.4434 4.63607C21.8522 5.04487 21.8523 5.70771 21.4434 6.11656L17.2413 10.3187C16.5818 10.9782 15.4542 10.5111 15.4542 9.57839V7.32942V7.20442H15.3292H0.585938C0.331389 7.20442 0.125 6.99803 0.125 6.74348V4.0091C0.125 3.75455 0.331389 3.54817 0.585938 3.54817H15.3292Z" fill="#FF5900" stroke="#FF5900" stroke-width="0.25"/>
                                </svg>
                            </button>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>

            </div>
        </div>
    </div>
</div>
</section>

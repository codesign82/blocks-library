import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {accordion} from "../../../scripts/general/accordion";

import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";
import DrawSVGPlugin from "gsap/DrawSVGPlugin";
import '../../../scripts/sitesSizer/propellerhealth';



gsap.registerPlugin(ScrollTrigger, DrawSVGPlugin)


const blockScript = async (container = document) => {
  const block = container.querySelector('.benefits_block');


  accordion(block.querySelectorAll('.card-svg-title'))
  const cardTitle = block.querySelectorAll('.card-svg-title')

  for (let i = 0; i < cardTitle.length; i++) {
    const iconPaths = cardTitle[i].querySelectorAll(".icon-wrapper svg path")
    gsap.timeline({
      delay: i * .1,
      scrollTrigger: {
        trigger: cardTitle[i],
        start: "center 95%",
      }
    })
        .from(iconPaths, {
          drawSVG: 0,
          stagger: .03
        })
        .call(() => {
          cardTitle[i].classList.add('active-draw')
        }, [], "<80%")
  }


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



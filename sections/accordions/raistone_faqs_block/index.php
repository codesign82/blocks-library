<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$choose_your_theme = get_field('choose_your_theme');
$dataClass = 'faqs_block';
$className = 'faqs_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}

if ($choose_your_theme === 'light') {
    $className .= ' light';
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/faqs_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$svg_or_image = get_field('svg_or_image');
$logo_svg = get_field('svg');
$image = get_field('image');
$title = get_field('title');
$subtitle = get_field('subtitle');
$description = get_field('description');
$faqs = get_field('faqs');
$link = get_field('link');


?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="faqs-content-wrapper">
        <div class="left-content">
            <?php if (!$svg_or_image) { ?>
                <?php if ($logo_svg) { ?>
                    <div class="icon iv-st-zoom">
                        <?= $logo_svg ?>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <?php if ($image) { ?>
                    <picture class="icon iv-st-zoom">
                        <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                    </picture>
                <?php } ?>
            <?php } ?>
            <?php if ($title) { ?>
                <h2 class="headline-2 title iv-st-from-bottom"><?= $title ?></h2>
            <?php } ?>
            <?php if ($subtitle) { ?>
                <h3 class="headline-3 question iv-st-from-bottom"><?= $subtitle ?></h3>
            <?php } ?>
            <?php if ($description) { ?>
                <div class="paragraph paragraph-xl-paragraph description iv-st-from-bottom"><?= $description ?></div>
            <?php } ?>
            <?php if ($link) { ?>
                <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>"
                   class="btn-arrow btn-arrow-white iv-st-from-bottom desk-only"><?= $link['title'] ?>
                    <svg width="9" height="14" viewBox="0 0 9 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.03906 13L7.03906 7L1.03906 1" stroke="#0A0A0A" stroke-width="2"
                              stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </a>
            <?php } ?>
        </div>
        <?php
        if ($faqs): ?>
            <div class="right-content">
                <?php foreach ($faqs as $faq):
                    $question = get_field('question', $faq);
                    $answer = get_field('answer', $faq);
                    ?>
                    <div class="faq-wrapper iv-st-from-right" itemscope itemprop="mainEntity"
                         itemtype="https://schema.org/Question">
                        <?php if ($question) { ?>
                            <div class="faq-question-title">
                                <h4 class="headline-4 has-question"><?= $question ?></h4>
                                <svg class="toggle-open minus-plus" viewBox="0 0 13.6 13.6">
                                    <line x1="0.8" y1="6.8" x2="12.8" y2="6.8" fill="none" stroke="#fff"
                                          stroke-linecap="round" stroke-miterlimit="10"
                                          stroke-width="1.6"/>
                                    <line class="remove-to-close" x1="6.8" y1="12.8" x2="6.8" y2="0.8" fill="none"
                                          stroke="#fff" stroke-linecap="round" stroke-miterlimit="10"
                                          stroke-width="1.6"/>
                                </svg>
                                <svg class="toggle-open arrow-icon" viewBox="0 0 14.6 8.6">
                                    <polyline points="1.3 1.3 7.3 7.3 13.3 1.3" fill="none" stroke="#fff"
                                              stroke-linecap="round" stroke-linejoin="round"
                                              stroke-width="2.62"/>
                                </svg>
                            </div>
                        <?php } ?>
                        <?php if ($answer) { ?>
                            <div class="have-answer" itemscope itemprop="acceptedAnswer"
                                 itemtype="https://schema.org/Answer">
                                <div class="paragraph  faq-answer"><?= $answer ?></div>
                            </div>
                        <?php } ?>
                    </div>
                <?php endforeach; ?>
                <?php if ($link) { ?>
                    <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>"
                       class="btn-arrow btn-arrow-white iv-st-from-bottom mobile-only"><?= $link['title'] ?>
                        <svg width="9" height="14" viewBox="0 0 9 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1.03906 13L7.03906 7L1.03906 1" stroke="#0A0A0A" stroke-width="2"
                                  stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </a>
                <?php } ?>
            </div>
        <?php endif; ?>
    </div>
</div>
</section>


<!-- endregion raistone's Block -->

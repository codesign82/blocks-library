import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/propellerhealth';


import {gsap} from "gsap";

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.text_and_icon_card_block');


  const accordions = block.querySelectorAll('.faq-wrapper')
  accordions.forEach((accordion) => {
    const accordionHead = accordion.querySelector('.faq-question-title');
    const iconInMobile = accordion.querySelector('.in-mobile');

    function openAndClose() {
      const accordionBody = accordion.querySelectorAll('.have-answer');
      if (!accordionBody) return;
      const isOpened = accordion?.classList.toggle('active');
      if (!isOpened) {
        gsap.to(accordionBody, {height: 0});
      } else {
        gsap.to(Array.from(accordions).map(otherFaq => {
          const otherFaqBody = otherFaq.querySelector('.have-answer');
          if (otherFaqBody && accordion !== otherFaq) {
            otherFaq?.classList.remove('active');
            gsap.set(otherFaq, {zIndex: 1});
          }
          return otherFaqBody;
        }), {height: 0});
        gsap.set(accordion, {zIndex: 2});
        gsap.to(accordionBody, {height: 'auto'});
      }
    }

    accordionHead?.addEventListener('click', (e) => {
      openAndClose();
    });

    iconInMobile?.addEventListener('click', (e) => {
      openAndClose();
    });

  });


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


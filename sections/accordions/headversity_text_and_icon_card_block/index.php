<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'text_and_icon_card_block';
$className = 'text_and_icon_card_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/text_and_icon_card_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$cta = get_field('cta');
$icon_card_repeater = get_field('icon_card_repeater');
$sub_title = get_field('sub_title');
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="employee-cards-wrapper">
        <div class="left-content">
            <?php if ($title) { ?>
                <p move-to-here class="headline-3 left-text iv-st-from-bottom"><?= $title ?></p>
            <?php } ?>
            <?php if ($description) { ?>
                <div class="paragraph left-description iv-st-from-bottom">
                    <?= $description ?>
                </div>
            <?php } ?>
            <?php if ($cta) { ?>
                <div class="iv-st-from-bottom">
                    <a href="<?= $cta['url'] ?>" class="btn"><?= $cta['title'] ?></a>
                </div>
            <?php } ?>
        </div>
        <div class="right-content">
            <?php if ($sub_title) { ?>
                <h2 class="main-title iv-st-from-bottom"><?= $sub_title ?></h2>
            <?php } ?>
            <?php if (have_rows('icon_card_repeater')) { ?>
            <?php while (have_rows('icon_card_repeater')) {
                the_row();
                $icon_or_image = get_sub_field('icon_or_image');
                $icon = get_sub_field('icon');
                $image = get_sub_field('image');
                $title = get_sub_field('title');
                $description = get_sub_field('description');
                $index = get_row_index();
                ?>
                <div class="faq-wrapper iv-st-from-bottom <?= $index === 1 ? 'active' : ''; ?>">
                    <div class="faq-question-title">
                        <div class="icon">
                            <?php if ($icon_or_image) { ?>
                                <picture class="icon">
                                    <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                                </picture>
                            <?php } else {
                                echo $icon;
                            }
                            ?>
                        </div>
                        <?php if ($title) { ?>
                            <h4 class="headline-5 has-question color-transition"><?= $title ?></h4>
                        <?php } ?>
                        <svg class="toggle-open minus-plus in-desk" viewBox="0 0 13.6 13.6">
                            <line x1="0.8" y1="6.8" x2="12.8" y2="6.8" fill="none" stroke="#fff" stroke-linecap="round"
                                  stroke-miterlimit="10"
                                  stroke-width="1.6"/>
                            <line class="remove-to-close" x1="6.8" y1="12.8" x2="6.8" y2="0.8" fill="none" stroke="#fff"
                                  stroke-linecap="round" stroke-miterlimit="10"
                                  stroke-width="1.6"/>
                        </svg>
                    </div>
                    <div class="have-answer" <?= $index === 1 ? 'style="height: auto;"' : ''; ?>>
                        <?php if ($description) { ?>
                            <div class="faq-answer">
                                <?= $description ?>
                            </div>
                        <?php } ?>
                    </div>
                    <svg class="toggle-open minus-plus in-mobile" viewBox="0 0 13.6 13.6">
                        <line x1="0.8" y1="6.8" x2="12.8" y2="6.8" fill="none" stroke="#fff" stroke-linecap="round"
                              stroke-miterlimit="10"
                              stroke-width="1.6"/>
                        <line class="remove-to-close" x1="6.8" y1="12.8" x2="6.8" y2="0.8" fill="none" stroke="#fff"
                              stroke-linecap="round" stroke-miterlimit="10"
                              stroke-width="1.6"/>
                    </svg>

                </div>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
</div>
</section>


<!-- endregion headversity's Block -->

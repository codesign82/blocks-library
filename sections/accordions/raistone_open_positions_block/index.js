import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/raistone';

import gsap from "gsap";

import {debounce} from "lodash/function";
/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.open_positions_block');


  let maxH = 0;
  const titles = block.querySelectorAll('.card-title');

  function calculateTitleHeight() {
    for (let title of titles) {
      title.scrollHeight > maxH && (maxH = title.scrollHeight);
    }
    document.querySelector(':root').style.setProperty('--card-title-height', `${maxH}px`);

  }

  calculateTitleHeight();

  debounce(() => {
    window.addEventListener('resize', calculateTitleHeight)
  }, 300)


  const cards = block.querySelectorAll('.card');
  if (cards.length === 0) return;
  cards.forEach((card) => {
    const cardHead = card.querySelector('.card-head');
    const cardBody = card.querySelector('.card-body');
    cardHead?.addEventListener('click', (e) => {
      if (!cardBody) {
        return;
      }
      const isOpened = card?.classList.toggle('faq-active');
      if (!isOpened) {
        gsap.to(cardBody, {height: 0});
      } else {
        gsap.to(Array.from(cards).map(otherFaq => {
          const otherFaqBody = otherFaq.querySelector('.card-body');
          if (otherFaqBody && card !== otherFaq) {
            otherFaq?.classList.remove('faq-active');
            gsap.set(otherFaq, {zIndex: 1});
          }
          return otherFaqBody;
        }), {height: 0});
        gsap.set(card, {zIndex: 2});
        gsap.to(cardBody, {height: 'auto'});
      }
    });
  });

  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


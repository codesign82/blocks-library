<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'open_positions_block';
$className = 'open_positions_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/open_positions_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$open_positions = get_field('open_positions');
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<?php /* Function which displays your post date in time ago format */
if (!function_exists('meks_time_ago')) {
    function meks_time_ago($time, $post_id)
    {
        return human_time_diff(get_the_time($time ?: "U", $post_id), current_time('timestamp')) . ' ' . __('ago');
    }
} ?>

<svg class="open-position-img" width="258" height="419" viewBox="0 0 258 419" fill="none"
     xmlns="http://www.w3.org/2000/svg">
    <path d="M123.159 199.063C134.505 194.02 145.43 188.09 155.796 181.272C193.989 156.15 212.571 127.619 212.571 94.0916C212.571 88.3013 211.964 82.6512 210.797 77.1411C172.371 30.0253 113.961 0 48.4544 0C36.0815 0 23.9886 1.12069 12.2227 3.1753V143.355C22.9615 137.425 35.3344 134.063 48.4544 134.063C86.5071 134.063 118.023 162.407 123.159 199.063Z"
          fill="#E9E9E9" fill-opacity="0.3"/>
    <path d="M12.2227 415.825C23.9886 417.88 36.0815 419.001 48.4544 419.001C87.8145 419.001 124.56 408.121 156.029 389.256L12.2227 290.074V415.825Z"
          fill="#E9E9E9" fill-opacity="0.3"/>
    <path d="M185.443 211.205C162.985 225.213 139.64 236.093 115.547 243.938C107.19 260.095 93.1826 272.89 76.1406 279.614L193.24 360.817C233.114 322.667 258 268.967 258 209.477C258 188.651 254.918 168.572 249.269 149.613C236.242 172.214 214.951 192.807 185.443 211.205Z"
          fill="#E9E9E9" fill-opacity="0.3"/>
</svg>
<div class="container">

    <div class="text">
        <?php if ($title) { ?>
            <h1 class="headline-1 title"><?= $title ?></h1>
        <?php } ?>
        <?php if ($description) { ?>
            <div class="paragraph description"><?= $description ?></div>
        <?php } ?>
    </div>
    <?php if ($open_positions) { ?>
        <div class="cards-wrapper">
            <?php foreach ($open_positions as $open_position) {
                $title = get_the_title($open_position);
                $job_location = get_field('job_location', $open_position);
                $job_url = get_field('job_url', $open_position);
                $date = get_field('job_date', $open_position);
                $job_description = get_field('job_description', $open_position);
                $cta_button1 = get_field('cta_button1', $open_position);
                $date = meks_time_ago($date, $open_position);
                ?>
                <div class="card">
                    <div class="card-head">
                        <div class="card-content">
                            <?php if ($job_url) { ?>
                                <a href="<?= $job_url ?>" target="_blank" class="paragraph card-title"><?= $title ?></a>
                            <?php } else {
                                ?>
                                <h3 class="paragraph card-title"><?= $title ?></h3>
                                <?php
                            } ?>
                            <p class="paragraph paragraph-l-paragraph date-country"><?= $job_location ?></p>
                            <p class="paragraph paragraph-l-paragraph date-country"> <?= $date ?></p>
                        </div>
                        <svg class="arrow" width="14" height="8" viewBox="0 0 14 8" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 1L7 7L13 1" fill="#214D42"/>
                            <path d="M1 1L7 7L13 1" stroke="#0A0A0A" stroke-width="2" stroke-linecap="round"
                                  stroke-linejoin="round"/>
                        </svg>
                    </div>
                    <div class="card-body">
                        <?php if ($job_description) { ?>
                            <div class="paragraph-l-paragraph card-body-text">
                                <?= $job_description ?>
                            </div>
                        <?php } ?>
                        <?php if ($cta_button1) { ?>
                            <a href="<?= $cta_button1['url'] ?>" target="<?= $cta_button1['target'] ?>"
                               class="btn btn-arrow btn-orange"><?= $cta_button1['title'] ?>
                                <svg width="9" height="14" viewBox="0 0 9 14" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1.80078 0.476074L8.03876 6.62948L1.23762 13.5241" stroke="#0A0A0A"
                                          stroke-width="1.5"/>
                                </svg>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } ?>

</div>
</section>

<!-- endregion raistone's Block -->

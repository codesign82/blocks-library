<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.![](screenshot.png)
$dataClass = 'map_block';
$className = 'map_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/map_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$counties = get_field('counties');

?>
<!-- region successkpi's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="map-container"></div>
<div data-lists="<?= base64_encode(json_encode(get_field('counties'))) ?>" class="location-cards"></div>
</section>


<!-- endregion successkpi's Block -->

import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/propellerhealth';
// import 'mapbox-gl/dist/mapbox-gl.css';
import mapboxgl from 'mapbox-gl';
import {v4 as uuidv4} from "uuid";

const blockScript = async (container = document) => {
  const block = container.querySelector('.map_block');

  const element = block.querySelector('.map-container');
  const cardsInfo = JSON.parse(atob(block.querySelector(".location-cards").dataset.lists));

  mapboxgl.accessToken = 'pk.eyJ1IjoiZGV2b3BzLXRha2VvZmYiLCJhIjoiY2w4dTY0dDhtMDB1MzNxbW84anh0YW52NCJ9.jgmyabrBY8Q50d0ZduoneQ';

  const map = new mapboxgl.Map({
    container: element,
    style: 'mapbox://styles/devops-takeoff/cla89egqj00ho15phvpsdzmgo',
    zoom: 2,
  });

  map.dragRotate.disable();
  map.touchZoomRotate.disableRotation();
  map.setRenderWorldCopies(false);


  map.on('style.load', () => {
    map.setFog({});
    cardsInfo.forEach((card) => {
      const el = document.createElement('div');
      const markerClass = `marker-${uuidv4()}`;
      el.className = `marker ${markerClass}`;
      const marker = new mapboxgl.Marker(el)
          .setLngLat([card.longitude, card.latitude]).addTo(map);

      const popup = new mapboxgl.Popup({
        className: "success-popup"
      }).setLngLat([card.longitude, card.latitude])
          .setHTML(
              `<h3 class="location-title headline-5"> ${card.state}</h3>
             <p class="paragraph location-state"> ${card.country}</p>
`
          )

      el.addEventListener('mouseenter', () => {
        popup.addTo(map);
      });

      block.addEventListener('mousemove', (e) => {
        if (block.querySelector(`.success-popup`)) {
          if (!e.target.closest('.success-popup') && !e.target.closest(`.${markerClass}`)) {
            popup.remove();
          }
        }
      });

      el.addEventListener('click', () => {
        marker.setPopup(popup)
      });

    })
  })

  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


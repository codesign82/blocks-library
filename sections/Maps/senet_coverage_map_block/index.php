<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'coverage_map_block';
$className = 'coverage_map_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/takeoff-code/gutenberg-blocks/coverage_map_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$cta_button = get_field('cta_button');
?>
<!-- region senet's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <?php if ($title) { ?>
        <h2 class="headline-2 title nero-color"><?= $title ?></h2>
    <?php } ?>
    <div class="content-wrapper">
        <?php if ($cta_button) { ?>
            <a class="map-wrapper" href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>">
                <div class="loader">
                    <div></div>
                </div>
                <div id="globe"></div>
            </a>
        <?php } else { ?>
            <div class="map-wrapper">
                <div id="globe"></div>
            </div>
        <?php } ?>
        <div class="right-content">
            <?php if (have_rows('stats_repeater')) { ?>
                <div class="stats-repeater">
                    <?php while (have_rows('stats_repeater')) {
                        the_row();
                        $stat = get_sub_field('stat');
                        $description = get_sub_field('description');
                        ?>
                        <div class="number-and-text">
                            <?php if ($stat) { ?>
                                <div class="headline-2 fw-900 dark-color number"><?= $stat ?></div>
                            <?php } ?>
                            <?php if ($description) { ?>
                                <div class="label description"><?= $description ?></div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>

            <?php if ($cta_button) { ?>
                <a class="cta-btn" href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>"><?= $cta_button['title'] ?></a>
            <?php } ?>
        </div>

    </div>
</div>
</section>

import './index.html';
import './style.scss';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
//import {initGlobe} from "../../../scripts/functions/initGlobe";
import '../../../scripts/sitesSizer/propellerhealth';


const blockScript = async (container = document) => {

  const block = container.querySelector('.coverage_map_block');
    initGlobe({
        globeElement: block.querySelector("#globe"),
        options: {
            clickable: false,
            enableUserInteraction: false,
            firstZoom: 2,
            autoRotateSpeed: 3,
            backgroundColor: "#fff",
        }
    });



    animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


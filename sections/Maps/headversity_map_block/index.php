<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'map_block';
$className = 'map_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/map_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$images = get_field('images');
$text = get_field('text');
$location_title = get_field('location_title');
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="content">
        <div class="left-content">
            <div class="map-wrapper img-wrapper">
                <div class="map"></div>
            </div>
            <div class="current-loc hidden">
                <svg class="location-svg" width="38" height="55" viewBox="0 0 38 55" role="img"
                     fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path
                            d="M18.9991 0C8.50588 0 0 8.54517 0 19.0868C0 20.8982 0.257312 22.649 0.726312 24.3082C1.01465 25.3312 1.33218 26.345 1.83038 27.2635C5.72655 34.43 18.9991 54.9982 18.9991 54.9982C18.9991 54.9982 32.2734 34.4282 36.1696 27.2635C36.6678 26.345 36.9835 25.3312 37.2737 24.3082C37.7427 22.6472 38 20.8982 38 19.0868C38 8.54517 29.4923 0 18.9991 0ZM18.9991 27.4927C16.3493 27.4927 13.9842 26.2478 12.4513 24.31C11.3144 22.8745 10.6337 21.0595 10.6337 19.0887C10.6337 14.454 14.3876 10.6847 19.0009 10.6847C23.6143 10.6847 27.3663 14.4558 27.3663 19.0887C27.3663 21.0613 26.6856 22.8763 25.5487 24.31C24.0139 26.2478 21.6507 27.4927 19.0009 27.4927H18.9991Z"
                            fill="#FF9900"/>
                </svg>
                <div class="location-content">
                    <div class="country"></div>
                    <div class="city"></div>
                </div>
            </div>
        </div>
        <?php if ($text) { ?>
            <div class="right-content">
                <?php if ($text['right_text']) { ?>
                    <h2 move-to-here class="title headline-3 iv-st-from-bottom"><?= $text['right_text'] ?></h2>
                <?php } ?>
                <?php if ($text['cta_button']) { ?>
                    <div class="iv-st-from-bottom">
                        <a class="btn" href="<?= $text['cta_button']['url'] ?>"><?= $text['cta_button']['title'] ?></a>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
        <!--      region percentage card-->
        <?php if (have_rows('card')) { ?>
            <div class="percentage-card">
                <?php while (have_rows('card')) {
                    the_row();
                    $percentage_value = get_sub_field('percentage_value');
                    $percentage_name = get_sub_field('percentage_name');
                    $prefix = get_sub_field('prefix');
                    $suffix = get_sub_field('suffix');
                    ?>
                    <div class="rate-field iv-st-from-bottom">
                        <?php if ($percentage_value) { ?>
                            <div class="percentage-value headline-1">
                                <?= $prefix ?>
                                <span data-number='<?= $percentage_value ?>'>0</span><?= $suffix ?>
                            </div>
                        <?php } ?>
                        <?php if ($percentage_name) { ?>
                            <div class="percentage-name headline-5"><?= $percentage_name ?></div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
        <!--      endregion percentage card-->

    </div>

</div>
</section>

<!-- endregion headversity's Block -->

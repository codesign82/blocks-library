import './index.html';
import './style.scss';

import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/propellerhealth';


import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";
// import 'mapbox-gl/dist/mapbox-gl.css';
import mapboxgl from 'mapbox-gl';

const codeStatus = {
  added: false,
  loaded: false,
  locations: false,
};
gsap.registerPlugin(ScrollTrigger);
mapboxgl.accessToken = 'pk.eyJ1IjoiZGV2b3BzLXRha2VvZmYiLCJhIjoiY2w4dTY0dDhtMDB1MzNxbW84anh0YW52NCJ9.jgmyabrBY8Q50d0ZduoneQ';


/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.map_block');


  const mobileMedia = window.matchMedia('(max-width: 599.9px)');

  if (!codeStatus.added) {
    codeStatus.loaded = new Promise(resolve => {
      const my_awesome_script = document.createElement('script');

      my_awesome_script.src = 'https://www.webglearth.com/v2/api.js';
      my_awesome_script.onload = resolve;

      document.head.appendChild(my_awesome_script);
    })
  }
  // if (!codeStatus.locations) {
  //   codeStatus.locations = new Promise(async resolve => {
  //     const res = await fetch('/wp-json/wp/v2/map_location?per_page=100&page=1')
  //     const pages = res.headers.get('x-wp-totalpages');
  //     const locations = (await res.json()).map(({acf}) => acf);
  //     for (let i = 2; i <= pages; i++) {
  //       const res = await fetch(`/wp-json/wp/v2/map_location?per_page=100&page=${i}`)
  //       locations.push(...(await res.json()).map(({acf}) => acf));
  //     }
  //     resolve(locations)
  //   });
  // }
  const numEl = block.querySelectorAll(".percentage-value span");
  const currentLocation = block.querySelector(".current-loc");
  const locationCountry = currentLocation.querySelector(".country");
  const locationCity = currentLocation.querySelector(".city");
  ScrollTrigger.batch(numEl, {
    onEnter: batch => {
      gsap.fromTo(batch, {innerHTML: 0}
          , {
            innerHTML: (_, element) => +element.dataset.number,
            duration: 3,
            ease: "power2.out",
            modifiers: {
              innerHTML: (value, target) =>
                  value.toFixed?.(target.dataset.toFixed ?? 0),
            },
            stagger: 0.2,
            immediateRender: true,
            clearProps: 'transform',
          });
    },
    start: 'top 80%',
    once: true,
  });

  codeStatus.loaded.then(async () => {
    const locations = await codeStatus.locations
    const mapContainer = block.querySelector(".left-content .map")
    const map = new mapboxgl.Map({
      container: mapContainer,
      style: 'mapbox://styles/devops-takeoff/cl90zyq5h001w14l3ca3o4m2t',
      center: [-99.5118087059997, 38.807455912438],
      zoom: 2.3,
      projection: 'globe',
    });
    map.scrollZoom.disable();
    map.doubleClickZoom.disable();

    const adjustZoomLevel = () => {
      if (mobileMedia.matches) {
        map.setZoom(1.3);
      } else {
        map.setZoom(2.3);
      }
    }
    adjustZoomLevel();
    window.addEventListener("resize", adjustZoomLevel)
    const distancePerSecond = 7;
    let userInteracting = false;
    let spinEnabled = true;
    const spinGlobe = () => {
      if (spinEnabled && !userInteracting) {
        const center = map.getCenter();
        center.lng -= distancePerSecond;
        map.easeTo({center, duration: 1000, easing: (n) => n});
      }
    }
    map.on('style.load', () => {
      spinGlobe();
      map.on('mousemove', () => {
        userInteracting = true;
      });
      map.on('mouseout', (e) => {
        if (!e.originalEvent.relatedTarget.closest(".map-wrapper")) {
          userInteracting = false;
          spinGlobe();
        }
      });
      map.on('moveend', () => {
        spinGlobe();
      });
      for (let {country, city, latitude, longitude} of locations) {
        const el = document.createElement('div');
        el.className = 'marker';
        el.addEventListener("click", () => {
          currentLocation.classList.remove('hidden')
          locationCountry.textContent = country;
          locationCity.textContent = city;
        })
        new mapboxgl.Marker(el).setLngLat([longitude, latitude]).addTo(map);
      }
    })

  })


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


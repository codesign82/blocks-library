<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'map';
$className = 'map';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/map/screenshot.png" >';
    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$location_cards = get_field('location_cards');
?>
<!-- region samrt_dv Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="map-container"></div>
    <div class="main-content dark-background">
        <div data-lists="<?= base64_encode(json_encode(get_field('location_cards'))) ?>" class="location-cards">
            <?php if (have_rows('location_cards')) { ?>
                <?php while (have_rows('location_cards')) {
                    the_row();
                    $location_type = get_sub_field('location_type');
                    $sub_title = get_sub_field('card_subtitle');
                    $title = get_sub_field('card_title');
                    ?>
                    <div role="button" aria-label="location card" aria-pressed="true">
                        <div class="card <?= $location_type ? $location_type : '' ?>">
                            <div class="card-header">
                                <div class="card-header-wrapper">
                                    <div>
                                        <?php if ($sub_title) { ?>
                                            <p class="subtitle"><?= $sub_title ?></p>
                                        <?php } ?>
                                        <?php if ($title) { ?>
                                            <h4 class='title headline-4'><?= $title ?></h4>
                                        <?php } ?>
                                    </div>
                                    <div role='button' aria-pressed='true'
                                         aria-label='open and close icon'
                                         class='svg-wrapper  hover-action'>
                                        <svg class=' toggle-open minus-plus  '
                                             viewBox='0 0 13.6 13.6'>
                                            <line x1='0.8' y1='6.8' x2='12.8' y2='6.8' fill='none'
                                                  stroke='#fff' stroke-linecap='round'
                                                  stroke-miterlimit='10'
                                                  stroke-width='1.6'/>
                                            <line class='remove-to-close' x1='6.8' y1='12.8' x2='6.8'
                                                  y2='0.8' fill='none' stroke='#fff'
                                                  stroke-linecap='round' stroke-miterlimit='10'
                                                  stroke-width='1.6'/>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body-wrapper">
                                <?php if (have_rows('card_body')) { ?>
                                    <?php while (have_rows('card_body')) {
                                        the_row();
                                        $country = get_sub_field('country');
                                        $selling_items = get_sub_field('selling_items');
                                        $address = get_sub_field('address');
                                        $email = get_sub_field('email');
                                        $website = get_sub_field('website');
                                        $phone = get_sub_field('phone');
                                        $fax = get_sub_field('fax');
                                        ?>
                                        <div class="card-body">
                                            <?php if ($location_type === 'sales') { ?>
                                                <?php if ($country) { ?>
                                                    <div class="country paragraph"><?= $country ?></div>
                                                <?php } ?>
                                                <?php if ($selling_items) { ?>
                                                    <div class="soled_stuff paragraph"><?= $selling_items ?></div>
                                                <?php } ?>
                                                <?php if ($address) { ?>
                                                    <div class="address paragraph"><?= $address ?></div>
                                                <?php } ?>
                                                <?php if ($website) { ?>
                                                    <div class="email paragraph">
                                                        <?= _e('Web: ', 'smart_dv') ?>
                                                        <a href="<?= $website ?>"><?= $website ?></a></div>
                                                <?php } ?>
                                                <?php if ($phone) { ?>
                                                    <div class="phone paragraph">
                                                        <a href="tel:<?= $phone ?>"><?= $phone ?></a></div>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <?php if ($address) { ?>
                                                    <div class='address paragraph'><?= $address ?></div>
                                                <?php } ?>
                                                <?php if ($phone) { ?>
                                                    <div class='phone paragraph'><?= _e('Phone: ', 'smart_dv') ?>
                                                        <a href="tel:<?= $phone ?>"><?= $phone ?></a></div>
                                                <?php } ?>
                                                <?php if ($fax) { ?>
                                                    <div class='fax paragraph'><?= _e('Fax: ', 'smart_dv') ?>
                                                        <a href="fax:<?= $fax ?>"> <?= $fax ?></a></div>
                                                <?php } ?>
                                                <?php if ($email) { ?>
                                                    <div class='email paragraph'><?= _e('Email: ', 'smart_dv') ?>
                                                        <a href="mailto:<?= $email ?>"> <?= $email ?></a>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    <?php }
                                } ?>
                            </div>
                        </div>
                    </div>
                <?php }
            } ?>
        </div>
    </div>
</div>
</section>
<!-- endregion samrt_dv Block -->

import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";

import '../../../scripts/sitesSizer/propellerhealth';

import {gsap} from "gsap";
// import 'mapbox-gl/dist/mapbox-gl.css';
import mapboxgl from 'mapbox-gl';
import {v4 as uuidv4} from 'uuid';

mapboxgl.accessToken = 'pk.eyJ1IjoiZGV2b3BzLXRha2VvZmYiLCJhIjoiY2w4dTY0dDhtMDB1MzNxbW84anh0YW52NCJ9.jgmyabrBY8Q50d0ZduoneQ'


const blockScript = async (container = document) => {
  const block = container.querySelector('.map');


  const cards = block.querySelectorAll('.card');
  const cardBody = block.querySelectorAll('.card-body-wrapper')
  const cardsInfo = JSON.parse(atob(block.querySelector(".location-cards").dataset.lists));
  const element = block.querySelector('.map-container');

  const map = new mapboxgl.Map({
    container: element,
    style: 'mapbox://styles/devops-takeoff/cla6mhbc0009o14nukwgfruy6',
    zoom: window.innerWidth >= 600 ? 0 : 1.3,
  });

  map.dragRotate.disable();
  map.touchZoomRotate.disableRotation();
  map.scrollZoom.disable();
  map.doubleClickZoom.disable();
  map.setRenderWorldCopies(false);

  const markers = {};

  map.on('style.load', () => {
    cardsInfo.forEach((card, index) => {
      cards[index].addEventListener('click', () => {
        const active = cards[index].classList.toggle('active');
        gsap.to(cardBody[index], {height: active ? 'auto' : 0});
        if (window.innerWidth > 600) {
          markers[index]?.forEach(marker => {
            marker.classList.toggle('active-marker');
          })
        }
      });

      card?.["card_body"]?.forEach((office) => {
        const el = document.createElement('div');
        const markerId = `marker-${uuidv4()}`;
        el.id = markerId;
        el.className = `marker ${card["location_type"] === "office" ? "office-marker" : "sales-marker"}`;
        markers[index] = markers[index] ? [...markers[index], el] : [el];

        new mapboxgl.Marker(el)
            .setLngLat([office.longitude, office.latitude]).addTo(map);

        const popup = new mapboxgl.Popup({
          className: `smart_dv-popup ${markerId}`
        }).setLngLat([office.longitude, office.latitude])
            .setHTML(`
             <h3 class="location-title">${card["card_title"]}${card["card_subtitle"] ? `: ${card["card_subtitle"]}` : ""}</h3>
             <p class="content description">${office.address}</p>
             <p class="content">Phone: ${office.phone}</p>
             <p class="content">Fax:  ${office.fax}</p>
             <p class="content">Email: ${office.email}</p>
          `);

        el.addEventListener('mouseenter', () => {
          popup.addTo(map);
        });

        block.addEventListener('mousemove', (e) => {
          if (!e.target.closest(`.${markerId}`) && !e.target.closest(`#${markerId}`)) {
            popup.remove();
          }
        });
      });
    });
  });
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/propellerhealth';
//import 'mapbox-gl/dist/mapbox-gl.css';
import mapboxgl from 'mapbox-gl';

import {v4 as uuidv4} from "uuid";

const blockScript = async (container = document) => {
  const block = container.querySelector('.map_block');


    // initGlobe({
    //     globeElement: block.querySelector("#globe"),
    //     mapElement: block.querySelector("#map"),
    //     options: {
    //         animateGlobeOnInit: true,
    //     }
    // });

    const dropdown = block.querySelector(".dropdown");

    dropdown.addEventListener("click", () => {
        dropdown.classList.add("active");
    })

    block.querySelector(".dropdown-menu .dropdown-title").addEventListener("click", (e) => {
        e.stopPropagation();
        dropdown.classList.remove("active");
    })

    window.addEventListener("click", (event) => {
        if (!event.target.closest(".dropdown")) {
            dropdown.classList.remove("active");
        }
    })

    block.querySelector(".scroll").addEventListener("click", () => {
        window.scrollTo(0, window.innerHeight - 30);
    });


    animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


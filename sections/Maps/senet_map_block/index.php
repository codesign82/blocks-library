<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'map_block';
$className = 'map_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/takeoff-code/gutenberg-blocks/map_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$select_form = get_field('select_form');
$dropdown_title = get_field('dropdown_title');
$dropdown_description = get_field('dropdown_description');
$form_title = get_field("form_title");
$form_description = get_field('form_description');
$no_interaction_time = get_field('no_interaction_time');
?>
<!-- region dh-updated-theme's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="globe-container">
    <div class="loader">
        <div></div>
    </div>
    <div id="globe" data-timeout="<?= $no_interaction_time ?>"></div>
    <div id="map"></div>
    <div class="satellite-icon open-sat-coverage-modal">
        <svg width="49" height="49" viewBox="0 0 49 49" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="24.5002" cy="24.5001" r="20.5" fill="#FF5900"/>
            <circle cx="24.5" cy="24.5" r="22.9082" stroke="#FF5900" stroke-width="3.18367"/>
            <path d="M20.2699 32.6167C18.4712 32.6167 17.0053 31.1508 17.0053 29.3522C17.0053 28.9809 16.7064 28.6819 16.3351 28.6819C15.9686 28.6819 15.6648 28.9809 15.6648 29.3522C15.6648 31.8886 17.7286 33.9524 20.265 33.9524C20.6315 33.9524 20.9353 33.6534 20.9353 33.2821C20.9353 32.9157 20.6363 32.6167 20.2699 32.6167Z" fill="white"/>
            <path d="M20.2696 35.7221C16.7592 35.7221 13.8997 32.8626 13.8997 29.3522C13.8997 28.9809 13.6007 28.6819 13.2294 28.6819C12.8581 28.6819 12.564 28.9857 12.564 29.3522C12.564 33.6004 16.0214 37.0578 20.2696 37.0578C20.6361 37.0578 20.9399 36.7589 20.9399 36.3876C20.9351 36.0211 20.6361 35.7221 20.2696 35.7221Z" fill="white"/>
            <path d="M20.2698 28.677C19.8985 28.677 19.5947 28.9808 19.5947 29.3569C19.5947 29.7282 19.8985 30.032 20.2698 30.032C20.6411 30.032 20.9449 29.7282 20.9449 29.3569C20.9449 28.9808 20.6411 28.677 20.2698 28.677Z" fill="white"/>
            <path d="M31.9972 25.8369L29.3547 25.2534L28.5494 26.0635L29.1328 28.7012L32.5855 32.1538L35.4498 29.2895L31.9972 25.8369ZM20.9209 20.4892L23.5585 21.0727L24.3686 20.2674L23.7852 17.6249L20.3326 14.1723L17.4683 17.0366L20.9209 20.4892Z" fill="white"/>
            <path d="M29.3302 17.1909L28.3416 18.1794L29.8943 19.7321C30.1547 19.9925 30.1547 20.4168 29.8943 20.6772C29.634 20.9376 29.2096 20.9376 28.9492 20.6772L27.3965 19.1245L26.461 20.06L27.1072 20.7061C27.3676 20.9665 27.3676 21.3909 27.1072 21.6513C26.8468 21.9117 26.4225 21.9117 26.1621 21.6513L25.5159 21.0051L21.8752 24.6458L24.9758 27.7464L32.4308 20.2914L29.3302 17.1909Z" fill="white"/>
            <path d="M36.8625 28.8169L32.8023 24.7568C32.7107 24.6651 32.5998 24.6024 32.4744 24.5783L30.4732 24.1347L33.8438 20.7641C34.0705 20.5519 34.1332 20.1131 33.8438 19.819L29.7981 15.7732C29.5377 15.5128 29.1134 15.5128 28.853 15.7732L25.4872 19.1439L25.0436 17.1427C25.0146 17.0173 24.952 16.9064 24.8652 16.8148L20.805 12.7594C20.5446 12.4991 20.1202 12.4991 19.8598 12.7594L16.0504 16.5689C15.7804 16.8582 15.7948 17.2584 16.0504 17.514L20.1106 21.5742C20.2022 21.6658 20.3131 21.7285 20.4385 21.7526L22.4397 22.1962L20.4578 24.1781C20.1974 24.4385 20.1974 24.8628 20.4578 25.1232L22.0057 26.6711L21.1377 27.5391C20.8725 27.4137 20.5783 27.3414 20.2697 27.3414C19.1606 27.3414 18.2541 28.2431 18.2541 29.357C18.2541 30.4661 19.1558 31.3726 20.2697 31.3726C21.3788 31.3726 22.2853 30.4709 22.2853 29.357C22.2853 29.0436 22.213 28.7494 22.0876 28.489L22.9556 27.6211L24.5035 29.1689C24.7446 29.4293 25.1689 29.4342 25.4486 29.1689L27.4305 27.1871L27.8741 29.1882C27.9031 29.3136 27.9657 29.4245 28.0525 29.5161L32.1127 33.5763C32.4358 33.8705 32.8312 33.8078 33.0579 33.5763L36.8673 29.7669C37.1277 29.5017 37.1277 29.0773 36.8625 28.8169ZM20.2697 30.0321C19.8984 30.0321 19.5946 29.7283 19.5946 29.357C19.5946 28.9857 19.8984 28.6771 20.2697 28.6771C20.641 28.6771 20.9448 28.9809 20.9448 29.357C20.9448 29.7283 20.641 30.0321 20.2697 30.0321ZM29.1327 28.7012L28.5492 26.0635L29.3545 25.2582L31.997 25.8417L35.4496 29.2943L32.5853 32.1586L29.1327 28.7012ZM23.785 17.6249L24.3685 20.2674L23.5584 21.0727L20.9159 20.4892L17.4633 17.0366L20.3276 14.1723L23.785 17.6249Z" fill="white"/>
        </svg>
    </div>
    <p class="country-info"></p>
    <div class="map-key">
        <div id="senet-data" class="map-key-item">
            <div class="map-key-shape"></div>
            <p>
                <?= __("Senet COVERAGE", "senetco") ?>
            </p>
        </div>
        <div id="helium-data" class="map-key-item">
            <div class="map-key-shape"></div>
            <p>
                <?= __("HELIUM COVERAGE", "senetco") ?>
            </p>
        </div>
        <div id="senet-ready-high-level" class="map-key-item-level">
            <div class="map-key-shape"></div>
            <p>
                <?= __("Senet Ready", "senetco") ?>
            </p>
        </div>
        <div id="active-deployments" class="map-key-item active">
            <div class="map-key-shape"></div>
            <p>
                <?= __("Active Deployments", "senetco") ?>
            </p>
        </div>
        <div id="senet-ready" class="map-key-item active">
            <div class="map-key-shape"></div>
            <p>
                <?= __("Senet Ready", "senetco") ?>
            </p>
        </div>
        <div id="coming-soon" class="map-key-item coming-soon active">
            <div class="map-key-shape"></div>
            <p>
                <?= __("Coming Soon", "senetco") ?>
            </p>
        </div>
        <div id="lora-alliance-unspecified" class="map-key-item active">
            <div class="map-key-shape"></div>
            <p>
                <?= __("LoRa Alliance UNSPECIFIED", "senetco") ?>
            </p>
        </div>
    </div>
    <?php if ($dropdown_title) { ?>
        <div class="dropdown">
            <p><?= $dropdown_title ?></p>
            <svg width="14" height="8" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M1 0.999999L7 7L13 1" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
            <div class="dropdown-menu">
                <div class="dropdown-title-desc">
                    <div class="dropdown-title">
                        <?= $dropdown_title ?>
                        <svg width="24" height="14" viewBox="0 0 24 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M2 2L12 12L22 2" stroke="currentColor" stroke-width="3.33333" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </div>
                    <?php if ($dropdown_description) { ?>
                        <div class="dropdown-description">
                            <?= $dropdown_description ?>
                        </div>
                    <?php } ?>
                </div>
                <?php if (have_rows('dropdown_stats')) { ?>
                    <div class="dropdown-menu-stats">
                        <?php while (have_rows('dropdown_stats')) {
                            the_row();
                            $state = get_sub_field('state');
                            $description = get_sub_field('description');
                            ?>
                            <div class="number-and-text">
                                <?php if ($state) { ?>
                                    <div class="state"><?= $state ?></div>
                                <?php } ?>
                                <?php if ($description) { ?>
                                    <div class="description"><?= $description ?></div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <div id="mapModal" class="modal">
        <div class="modal-content">
            <div class="close">
                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M18.5166 18.5165C17.7517 19.2814 16.4768 19.2814 15.7544 18.559L9.63524 12.4398L3.51605 18.559C2.79364 19.2814 1.5613 19.2389 0.8389 18.5165C0.0740006 17.7516 0.0740012 16.5618 0.796405 15.8394L6.9156 9.72016L0.796405 3.60097C0.031506 2.83607 0.0740006 1.60373 0.8389 0.838835C1.5613 0.11643 2.75115 0.11643 3.51605 0.881329L9.63524 7.00052L15.7544 0.881329C16.5193 0.11643 17.7517 0.0739354 18.5166 0.838835C19.239 1.56124 19.239 2.83607 18.4741 3.60097L12.3549 9.72016L18.4741 15.8394C19.239 16.5193 19.239 17.7941 18.5166 18.5165Z" fill="#FF5900"/>
                </svg>
            </div>
            <?php if ($form_title || $form_description) { ?>
                <div class="modal-title-desc">
                    <?php if ($form_title) { ?>
                        <h2 class="modal-title"><?= $form_title ?></h2>
                    <?php } ?>
                    <?php if ($form_description) { ?>
                        <div class="modal-description">
                            <?= $form_description ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php echo do_shortcode('[gravityform id="' . $select_form . '" ajax="true" title="false" description="false"]'); ?>
        </div>
    </div>
</div>
<div class="scroll">
    <?= __("Learn More", "senetco") ?>
    <div class="scroll-arrow">
        <svg width="12" height="22" viewBox="0 0 12 22" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M7.7658 15.3292L7.7658 15.4542L7.8908 15.4542L10.1397 15.4542C11.0724 15.4542 11.5395 16.5818 10.88 17.2413L6.6779 21.4434L6.76629 21.5318L6.67789 21.4434C6.2691 21.8522 5.60625 21.8523 5.1974 21.4434L0.995302 17.2413C0.335808 16.5818 0.802897 15.4542 1.73557 15.4542L3.98455 15.4542L4.10955 15.4542L4.10955 15.3292L4.10955 0.585937C4.10955 0.331389 4.31594 0.125 4.57049 0.125L7.30486 0.125C7.55941 0.125 7.7658 0.331389 7.7658 0.585937L7.7658 15.3292Z" fill="#FF5900" stroke="#FF5900" stroke-width="0.25"/>
        </svg>
    </div>
</div>
</section>


<!-- endregion dh-updated-theme's Block -->

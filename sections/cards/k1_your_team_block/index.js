import './index.html';
import './style.scss';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {isChrome} from "../../../scripts/functions/isChrome";
import '../../../scripts/sitesSizer/propellerhealth';
import {accordion} from "../../../scripts/general/accordion";

const blockScript = async (container = document) => {
  const block = container.querySelector('.your_team_block');

  // add block code here
  const customModal = document.querySelector('#custom-modal');
  const customModalContent = customModal && customModal.querySelector('.custom-modal-content');
  const videoModalButtons = block.querySelectorAll('.video-modal-button');
  let search_plus = block.querySelectorAll('.search-plus');
  let red_box = block.querySelectorAll('.box');
  let close_box = block.querySelectorAll('.close-box');

  for (let i = 0; i <= search_plus.length; i++) {
    search_plus[i]?.addEventListener('click', function () {
      search_plus[i].classList.add('active');
      search_plus[i].closest('.intelligent-card').classList.add('no-hover');
      red_box[i].classList.remove('hide');
    });

    close_box[i]?.addEventListener('click', function () {
      red_box[i].classList.add('hide');
      search_plus[i].closest('.intelligent-card').classList.remove('no-hover');
      search_plus[i].classList.remove('active');
    });
  }
  // region custom modal
  for (let button of videoModalButtons) {
    button?.addEventListener('click', () => {
      const modalTemplate = button.closest('.intelligent-card')?.querySelector('.intelligent-template');
      const modalType = button.dataset.modalType;
      customModal.classList.add('custom-modal-' + modalType);
      let clone = modalTemplate.content.cloneNode(true);
      // region prevent page scroll
      document.documentElement.classList.add('modal-opened');
      const scrollEnabled = new Event('scroll-disabled');
      document.dispatchEvent(scrollEnabled);
      // endregion prevent page scroll
      customModalContent.appendChild(clone);
      customModal.classList.add('modal-active');
      button.blur();
      customModal.querySelector('video')?.focus()
    })
  }
  // endregion custom modal
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


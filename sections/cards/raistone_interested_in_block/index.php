<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'interested_in_block';
$className = 'interested_in_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/interested_in_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="interested-in-wrapper">
        <?php if ($title) { ?>
            <h2 class="headline-2 interested-title iv-st-from-bottom"><?= $title ?></h2>
        <?php } ?>

        <?php if (have_rows('card')) { ?>
            <div class="row row-cols-1 row-cols-md-1 row-cols-lg-2">
                <?php while (have_rows('card')) {
                    the_row();
                    $card_title = get_sub_field('card_title');
                    $card_description = get_sub_field('card_description');
                    $link = get_sub_field('link');
                    ?>
                    <div class="col">
                        <div class="interested-box">
                            <?php if ($card_title) { ?>
                                <div class="way-name paragraph iv-st-from-bottom"><?= $card_title ?></div>
                            <?php } ?>
                            <?php if ($card_description) { ?>
                                <div class="paragraph-l-paragraph interested-description iv-st-from-bottom"><?= $card_description ?></div>
                            <?php } ?>
                            <?php if ($link) { ?>
                                <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>"
                                   class="btn-arrow help-link iv-st-from-bottom">
                                    <?= $link['title'] ?>
                                    <svg width="10" height="16" viewBox="0 0 10 16" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1.80078 1.47607L8.03876 7.62948L1.23762 14.5241" stroke="#0A0A0A"
                                              stroke-width="1.5"/>
                                    </svg>
                                </a>
                            <?php } ?>

                            <?php if (have_rows('card_details')) { ?>

                                <?php while (have_rows('card_details')) {
                                    the_row();
                                    $svg_or_image = get_sub_field('svg_or_image');
                                    $svg = get_sub_field('svg');
                                    $image = get_sub_field('image');
                                    $description = get_sub_field('description');
                                    ?>
                                    <div class="interested-wrapper iv-st-from-bottom">
                                        <?php if (!$svg_or_image) { ?>
                                            <?= $svg ?>
                                        <?php } else { ?>
                                            <?php if ($image) { ?>
                                                <picture class="interested-img">
                                                    <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                                                </picture>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php if ($description) { ?>
                                            <div class="paragraph-l-paragraph right-description"><?= $description ?></div>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>
</section>


<!-- endregion raistone's Block -->

<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'about_raistone_block';
$className = 'about_raistone_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/about_raistone_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>


<div class="section-shapes">

    <!--  Dots Lines -->
    <svg class="line" viewBox="0 0 461.64 132.08">
        <defs>
            <mask id="theMask" maskUnits="userSpaceOnUse">
                <path class="dots-animation-i-e" d="M1.5,22.22s79.39,113.1,240.81,107.13S457.5,31.46,460.14.5"
                      transform="translate(0 1)" fill="none"
                      stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
            </mask>
        </defs>
        <g id="maskReveal" mask="url(#theMask)">
            <path d="M1.5,22.22s79.39,113.1,240.81,107.13S457.5,31.46,460.14.5" transform="translate(0 1)" fill="none"
                  stroke="#521437" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"
                  stroke-dasharray="26 17"/>
        </g>
    </svg>
    <svg class="dots-with-circle" viewBox="0 0 514.08 204.46">
        <defs>
            <clipPath id="dot-line-with-circle2">
                <path d="M0,0V204.46H514.08V0ZM231.89,204.46A34.15,34.15,0,1,1,266,170.27v0A34.15,34.15,0,0,1,231.89,204.46Z"
                      fill="none"/>
            </clipPath>
            <mask id="theMask2" maskUnits="userSpaceOnUse">
                <path class="dots-animation-i-e" d="M503.08,21.11s-83.92,149.2-271.18,149.2S-9.94,86.4-9.94,86.4"
                      fill="none" stroke="#fff"
                      stroke-linecap="round" stroke-miterlimit="10" stroke-width="3"/>
            </mask>
        </defs>
        <path class="circle-animation-i-e"
              d="M227.56,145.89a25.25,25.25,0,0,1,17.13,3.1,24.18,24.18,0,0,1,10.77,13.34,23.49,23.49,0,0,1-.94,16.94,24.41,24.41,0,0,1-12.19,12.13A25.43,25.43,0,0,1,225,192.68a24.69,24.69,0,0,1-14-10.21,23.56,23.56,0,0,1-3.54-16.61,24,24,0,0,1,8.59-14.77,25.1,25.1,0,0,1,11.48-5.2Zm25.91,25.62a20.66,20.66,0,0,0-2.54-12,21.33,21.33,0,0,0-9-8.6A22.15,22.15,0,0,0,218,153.35a21.1,21.1,0,0,0-6.93,10.22,20.6,20.6,0,0,0,.07,12.24,21.13,21.13,0,0,0,7,10.15,22.08,22.08,0,0,0,27.49-.16,21,21,0,0,0,7.76-14.3Z"
              fill="#521437"/>
        <g id="maskReveal2" mask="url(#theMask2)" clip-path="url(#dot-line-with-circle2)">
            <path d="M503.08,21.11s-83.92,149.2-271.18,149.2S-9.94,86.4-9.94,86.4" fill="none" stroke="#521437"
                  stroke-linecap="round" stroke-miterlimit="10" stroke-width="3" stroke-dasharray="23.71 30"/>
        </g>
    </svg>

    <!--  Background Shapes-->

    <svg class="single-shape" width="60.4rem" height="78.1rem" viewBox="0 0 604 781" fill="none">
        <g opacity="0.5">
            <path d="M603.008 769.561L603.008 302.287C603.008 135.718 467.977 0.687494 301.409 0.687487V0.687487C134.84 0.68748 -0.190754 135.718 -0.190761 302.287L-0.190782 780.672"
                  stroke="#C1DA9B"/>
        </g>
    </svg>
    <svg class="double-shape" width="81.9rem" height="89.3rem" viewBox="0 0 819 893" fill="none">
        <g opacity="0.5">
            <path d="M558.461 881.929L558.461 463.352C558.461 309.502 433.741 184.781 279.891 184.781V184.781C126.041 184.781 1.32067 309.501 1.32067 463.351L1.32065 892.003"
                  stroke="#C1DA9B" stroke-width="1.27468"/>
            <path d="M818 888.82L818 329.17C818 147.927 671.073 0.999994 489.829 0.999986V0.999986C308.586 0.999978 161.659 147.927 161.659 329.171L161.659 888.82"
                  stroke="#C1DA9B" stroke-width="1.27468"/>
        </g>
    </svg>

</div>
<div class="container">
    <?php if ($title) { ?>

        <h2 class="headline-2 areas-title iv-st-from-bottom"><?= $title ?></h2>
    <?php } ?>
    <?php if (have_rows('cards')) { ?>
        <div class="cards-wrapper">
            <?php while (have_rows('cards')) {
                the_row();
                $title = get_sub_field('title');
                $svg_or_image = get_sub_field('svg_or_image');
                $image = get_sub_field('image');
                $svg = get_sub_field('svg');
                $description = get_sub_field('description');
                $card_image = get_sub_field('card_image');
                $text_is_dark = get_sub_field('text_is_dark') ? 'text-is-dark' : '';
                $background_color = get_sub_field('background_color');
                ?>
                <div class="card iv-st-from-bottom <?= $text_is_dark ?>"
                     style="background-color: <?= $background_color ?>">
                    <?php if (!$svg_or_image) { ?>
                        <div class="logo">
                            <?= $svg ?>
                        </div>
                    <?php } else { ?>
                        <?php if ($image) { ?>
                            <div class="logo">
                                <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if ($card_image) { ?>
                        <picture class="card-person">
                            <img src="<?= $card_image['url'] ?>" alt="<?= $card_image['alt'] ?>">
                        </picture>
                    <?php } ?>
                    <?php if ($title) { ?>
                        <h4 class="headline-3 card-title"><?= $title ?></h4>
                    <?php } ?>
                    <?php if ($description) { ?>
                        <div class="paragraph description paragraph-xl-paragraph"><?= $description ?></div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>
</section>


<!-- endregion raistone's Block -->

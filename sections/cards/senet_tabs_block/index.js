import './index.html';
import './style.scss';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/propellerhealth';
import {gsap} from "gsap";

const blockScript = async (container = document) => {
  const block = container.querySelector('.tabs_block');

    const contentSectionHolder = block.querySelectorAll(".tabs_block .cards");
    const taps = document.querySelectorAll(".tabs_block .slide_preview");
    for (let i = 0; i < taps.length; i++) {
        contentSectionHolder[0].classList.add('active_form')
        const tap = taps[i];
        tap.addEventListener('click', () => {
            if (tap.classList.contains('slide_active')) return;
            for (let x = 0; x < taps.length; x++) {
                contentSectionHolder[x].classList.remove("active_form");
                taps[x].classList.remove("slide_active");
            }
            contentSectionHolder[i].classList.add("active_form");
            tap.classList.add("slide_active");
        })
    }
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


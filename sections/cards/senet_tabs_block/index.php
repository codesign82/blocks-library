<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'tabs_block';
$className = 'tabs_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/takeoff-code/gutenberg-blocks/tabs_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');

?>
<!-- region senet's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<div class="container" id="tabs_block">
    <div class="cards-title">
        <?php if ($title) { ?>
            <div class="headline-2 title"><?= $title ?></div>
        <?php } ?>
        <?php if ($description) { ?>
            <div class="description paragraph">
                <?= $description ?>
            </div>
        <?php } ?>
    </div>
    <?php if (have_rows('tabs_repeater')) { ?>
        <div class="change_slide">
            <?php $index = 0;
            while (have_rows('tabs_repeater')) {
                the_row();
                $tab_title = get_sub_field('tab_title');
                ?>
                <h3 class="slide_preview <?php if ($index === 0) {
                    echo 'slide_active';
                } ?> paragraph">
                    <?= $tab_title ?>
                </h3>
                <?php $index++;
            } ?>
        </div>
    <?php } ?>
    <?php if (have_rows('tabs_repeater')) { ?>
        <?php $count = 0;

        while (have_rows('tabs_repeater')) {
            the_row();
            $tab_title = get_sub_field('tab_title');
            $manual_or_automatic = get_sub_field('manual_or_automatic');
            $number_of_posts = get_sub_field('number_of_posts');
            $get_index = get_row_index();
            $class = '';
            if ($get_index === 1) {
                $class = 'grid-2-col';
            } else {
                $class = 'grid-3-col';
            }
            ?>
            <?php if ($manual_or_automatic === 'manual') { ?>
                <div class="cards manual-cards <?php if ($count === 0) {
                    echo 'active_form';
                } ?> ">
                    <div class="content-section">
                        <div class="<?= $class ?>">
                            <?php if (have_rows('add_products')) { ?>
                                <?php while (have_rows('add_products')) {
                                    the_row();
                                    $title = get_sub_field('title');
                                    $subtitle = get_sub_field('subtitle');
                                    $description = get_sub_field('description');
                                    $link = get_sub_field('link');
                                    $icon = get_sub_field('icon');
                                    $svg = $icon['svg'];
                                    $svg_icon = $icon['svg_icon'];
                                    $png_icon = $icon['png_icon'];
                                    ?>
                                    <div class="card <?php if ($count === 0) {
                                        echo 'first_form';
                                    } else {
                                        echo 'second_form';
                                    } ?> ">
                                        <?php if ($svg && $svg_icon) { ?>
                                            <div class="svg-card">
                                                <?= $svg_icon ?>
                                            </div>
                                        <?php } elseif ($png_icon) { ?>
                                            <img class="svg-card" src="<?= $png_icon['url'] ?>" alt="<?= $png_icon['alt'] ?>">
                                        <?php } ?>
                                        <?php if ($link) { ?>
                                            <a class="cta-link" href="<?= $link['url'] ?>"
                                               target="<?= $link['target'] ?>"><h2 class="headline-4 title_card">
                                                    <?= $title ?></h2>
                                            </a>
                                        <?php } ?>
                                        <?php if ($subtitle) { ?>
                                            <h4 class="headline-5 sub_title "><?= $subtitle ?></h4>
                                        <?php } ?>
                                        <?php if ($description) { ?>
                                            <div class="paragraph description_text"><?= $description ?></div>
                                        <?php } ?>
                                        <?php if ($link) { ?>
                                            <a class="cta-link link_card" href="<?= $link['url'] ?>"
                                               target="<?= $link['target'] ?>">
                                                <p><?= $link['title'] ?></p>
                                                <svg class="svg_style" width="22" height="11" viewBox="0 0 22 11" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M15.3292 3.54817H15.4542V3.42317V1.17424C15.4542 0.24156 16.5818 -0.225521 17.2413 0.433966L21.4434 4.63607L21.5318 4.54768L21.4434 4.63607C21.8522 5.04487 21.8523 5.70771 21.4434 6.11656L17.2413 10.3187C16.5818 10.9782 15.4542 10.5111 15.4542 9.57839V7.32942V7.20442H15.3292H0.585938C0.331389 7.20442 0.125 6.99803 0.125 6.74348V4.0091C0.125 3.75455 0.331389 3.54817 0.585938 3.54817H15.3292Z"
                                                          fill="#FF5900" stroke="#FF5900" stroke-width="0.25"/>
                                                </svg>
                                            </a>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } elseif ($manual_or_automatic === 'automatic') { ?>
                <div class="cards automatic-cards <?php if ($count === 0) {
                    echo 'active_form';
                } ?> ">
                    <div class="content-section">
                        <div class="<?= $class ?>">
                            <?php
                            $args = array(
                                'post_type' => 'products',
                                'posts_per_page' => $number_of_posts ?: 5,
                                'post_status' => 'publish'
                            );
                            $the_query = new WP_Query($args);
                            // The Loop
                            if ($the_query->have_posts()) {
                                while ($the_query->have_posts()) {
                                    $the_query->the_post();
                                    $subtitle = get_field('sub-title', get_the_ID());
                                    $description = get_field('description', get_the_ID());
                                    $link = get_field('link', get_the_ID());
                                    $icon = get_field('icon', get_the_ID());
                                    $svg = $icon['svg'];
                                    $svg_icon = $icon['svg_icon'];
                                    $png_icon = $icon['png_icon'];
                                    ?>
                                    <div class="card <?php if ($count === 0) {
                                        echo 'first_form';
                                    } else {
                                        echo 'second_form';
                                    } ?> ">
                                        <?php if ($svg && $svg_icon) { ?>
                                            <div class="svg-card">
                                                <?= $svg_icon ?>
                                            </div>
                                        <?php } elseif ($png_icon) { ?>
                                            <img class="svg-card" src="<?= $png_icon['url'] ?>" alt="<?= $png_icon['alt'] ?>">
                                        <?php } ?>
                                        <h2 class="headline-4 title_card"><?php the_title() ?></h2>

                                        <?php if ($subtitle) { ?>
                                            <h4 class="headline-5 sub_title "><?= $subtitle ?></h4>
                                        <?php } ?>
                                        <?php if ($description) { ?>
                                            <div class="headline-6 description_text"><?= $description ?></div>
                                        <?php } ?>
                                        <?php if ($link) { ?>
                                            <a class="cta-link link_card" href="<?= $link['url'] ?>"
                                               target="<?= $link['target'] ?>">
                                                <?= $link['title'] ?>
                                                <svg class="svg_style" width="22" height="11" viewBox="0 0 22 11" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M15.3292 3.54817H15.4542V3.42317V1.17424C15.4542 0.24156 16.5818 -0.225521 17.2413 0.433966L21.4434 4.63607L21.5318 4.54768L21.4434 4.63607C21.8522 5.04487 21.8523 5.70771 21.4434 6.11656L17.2413 10.3187C16.5818 10.9782 15.4542 10.5111 15.4542 9.57839V7.32942V7.20442H15.3292H0.585938C0.331389 7.20442 0.125 6.99803 0.125 6.74348V4.0091C0.125 3.75455 0.331389 3.54817 0.585938 3.54817H15.3292Z"
                                                          fill="#FF5900" stroke="#FF5900" stroke-width="0.25"/>
                                                </svg>
                                            </a>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            <?php }
                            wp_reset_postdata(); ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php $count++;
        } ?>
    <?php } ?>
</div>
</sec
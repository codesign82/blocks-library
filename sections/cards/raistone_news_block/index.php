<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'news_block';
$className = 'news_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/news_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$query_options = get_field('query_options');
$featured_post = get_field('featured_post');
$left_text = get_field('left_text');
$right_text = get_field('right_text');
$cta_button = get_field('cta_button');

?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="news-content">

        <div class="left-content">
            <?php if ($left_text) { ?>
                <h2 class="headline-2 title iv-st-from-bottom"><?= $left_text ?></h2>
            <?php } ?>
            <?php
            if ($featured_post):
                setup_postdata($featured_post);
                get_template_part("template-parts/post-card", '', array(
                    'post_id' => $featured_post,
                    'post_type' => 'featured_post',
                ));
                wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
                ?>
            <?php endif; ?>


        </div>


        <div class="right-content">
            <?php if ($right_text) { ?>
                <h5 class="headline-5 previous-text"><?= $right_text ?></h5>
            <?php } ?>

            <?php
            $posts = get_field('posts');
            if ($posts): ?>
                <ul>
                    <?php foreach ($posts as $post):
                        // Setup this post for WP functions (variable must be named $post).
                        setup_postdata($post);
                        get_template_part("template-parts/post-card", '', array(
                            'post_id' => $post,
                        ));
                        ?>
                    <?php endforeach; ?>
                </ul>
                <?php
                // Reset the global post object so that the rest of the page works correctly.
                wp_reset_postdata(); ?>
            <?php endif; ?>

            <?php if ($cta_button) { ?>
                <a href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>"
                   class="btn cta-button iv-st-from-bottom"><?= $cta_button['title'] ?></a>
            <?php } ?>
        </div>
    </div>
</div>

</section>


<!-- endregion raistone's Block -->

<?php
$post_id = @$args['post_id'];
$post_type = @$args['post_type'];
$thumbnail_id = get_post_thumbnail_id();
$alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);
$color = get_field('color', $post_id);
$excerpt = get_the_excerpt($post_id);
?>

<style>

  #
  <?='post-id-'.$post_id; ?>
  .image-wrapper:after {
    background-color: <?=$color?> !important;

  }
</style>
<div id="post-id-<?= $post_id; ?>" class="card iv-st-from-bottom">
  <?php if ($post_type === 'featured_post') { ?>
    <h5 class="headline-5 card-title"> <?= __('FEATURED ARTICLE', 'raistone') ?></h5>
  <?php } ?>
  <a class="image-wrapper" href="<?= get_the_permalink($post_id); ?>" style=" ">
    <picture>
      <img src="<?= get_the_post_thumbnail_url($post_id) ?>" alt="<?= $alt ?>">
    </picture>
  </a>
  <div class="card-content">
    <a href="<?= get_the_permalink($post_id); ?>">
      <h3 class="headline-3 post-title"><?= get_the_title($post_id) ?></h3>
    </a>
    <?php if (has_excerpt($post_id)) { ?>
      <div class="paragraph paragraph-l-paragraph post-description"><?= $excerpt ?></div>
    <?php } ?>
    <a href="<?= get_the_permalink($post_id); ?>" class="btn-arrow btn-arrow-white">
      <?= __('Learn more', 'raistone') ?>
      <svg width="9" height="14" viewBox="0 0 9 14" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M1.03906 13L7.03906 7L1.03906 1" stroke="#0A0A0A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
      </svg>
    </a>
  </div>
</div>

import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/towSmart';

const blockScript = async (container = document) => {
  const block = container.querySelector('.tow_smart_in_3_easy_steps_block');
  
 
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);




import './index.html';
import './style.scss';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {Navigation, Swiper} from 'swiper'
import {isChrome} from "../../../scripts/functions/isChrome";
import '../../../scripts/sitesSizer/propellerhealth';

const blockScript = async (container = document) => {
  const block = container.querySelector('.additional_resources');


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


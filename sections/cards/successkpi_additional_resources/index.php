<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'additional_resources';
$className = 'additional_resources';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/additional_resources/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region successkpi's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="resources">
        <?php if ($title) { ?>
            <h1 class="headline-2 title iv-st-from-bottom"><?= $title ?></h1>
        <?php } ?>
        <?php if ($description) { ?>
            <div class="headline-5 description iv-st-from-bottom"><?= $description ?></div>
        <?php } ?>
        <?php if (have_rows('cards')) { ?>
            <div class="row row-cols-lg-3  row-cols-md-2 row-cols-1">
                <?php while (have_rows('cards')) {
                    the_row();
                    $card_icon = get_sub_field('icon');
                    $card_link = get_sub_field('card_link');
                    $card_desc = get_sub_field('card_description');
                    $has_description = get_sub_field('has_description');
                    ?>
                    <div class="col iv-st-from-bottom">
                        <a aria-label="description-resources " href="<?= $card_link['url'] ?>"
                           class="card  <?= $has_description ? '' : 'no-disc' ?>">
                            <div class="title-and-icon">
              <span class="svg-wrapper">
            <?= $card_icon ?>
          </span>

                                <div class="card-title"><?= $card_link['title'] ?></div>
                            </div>
                            <?php if ($has_description) { ?>
                                <div class=" headline-6 card-disc"><?= $card_desc ?></div>
                            <?php } ?>
                        </a>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>

</div>
</section>


<!-- endregion successkpi's Block -->

<?php
// Create id attribute allowing for custom "anchor" value.
$id = 'move_features_block';
// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'move_features_block';
$className = 'move_features_block';
if ( ! empty( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
    $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/move_features_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$text_with_gradient = get_field( 'text_with_gradient' );
$first_text         = @$text_with_gradient['first_text'];
$gradient_text      = @$text_with_gradient['gradient_text'];
$last_text          = @$text_with_gradient['last_text'];
$description        = get_field( 'description' );
?>
<!-- region MOVING's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
    <div class="circle-with-line">
        <div class="circle-div"></div>
        <div class="line-div"></div>
    </div>
    <div class="move-features">
        <h2 class="headline-1 headline-1-gradient a7a-test-from0-bottom">
            <?php if ( $first_text ) { ?>
                <?= $first_text ?>
            <?php } ?>
            <?php if ( $gradient_text ) { ?>
                <span class="red-gradiant"><?= $gradient_text ?></span>
            <?php } ?>
            <?php if ( $last_text ) { ?>
                <?= $last_text ?>
            <?php } ?>
        </h2>
        <?php
        if ( have_rows( 'flip_cards' ) ) {
            ?>
            <div class="swiper-container all-cards a7a-test-from0-bottom">
                <div class="swiper-wrapper">
                    <?php
                    while ( have_rows( 'flip_cards' ) ) {
                        the_row();
                        $off_hover_title         = get_sub_field( 'off_hover_title' );
                        $image_svg               = acf_icon(get_sub_field( 'image_svg' ));
                        $hover_state_title       = get_sub_field( 'hover_state_title' );
                        $hover_state_description = get_sub_field( 'hover_state_description' );
                        ?>
                        <div class="swiper-slide perspective-flip">
                            <div class="card flip ">
                                <div class="flip-card flip-front">
                                    <div class="icon-and-description">
                                        <svg class="arrow-down" width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15.1421 8.5L8.07104 15.5711L0.999977 8.5" stroke="#595959" stroke-width="2"/>
                                            <path d="M8.07104 15V0" stroke="#595959" stroke-width="2"/>
                                        </svg>
                                        <?php if ( $off_hover_title ) { ?>
                                            <div class="paragraph"><?= $off_hover_title ?></div>
                                        <?php } ?>
                                    </div>
                                    <?php if ( $image_svg ) { ?>
                                        <div class="image-wrapper">
                                            <?= $image_svg ?>
                                        </div>
                                    <?php } ?>

                                </div>
                                <div class="flip-card flip-back">
                                    <?php if ( $hover_state_title ) { ?>
                                        <div class="paragraph "><?= $hover_state_title ?></div>
                                    <?php } ?>
                                    <?php if ( $hover_state_description ) { ?>
                                        <div class="paragraph paragraph-18"><?= $hover_state_description ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    } ?>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <div class="full-lines full-lines-flipped">
        <div class="line line-width"></div>
        <div class="line line-height"></div>
    </div>
</div>
</section>


<!-- endregion MOVING's Block -->

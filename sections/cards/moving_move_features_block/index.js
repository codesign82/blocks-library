import './index.html';
import './style.scss';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/moving';
import {gsap} from "gsap";
import {Swiper} from "swiper";
import DrawSVGPlugin from "gsap/DrawSVGPlugin";
import {ScrollTrigger} from "gsap/ScrollTrigger";

gsap.registerPlugin(DrawSVGPlugin, ScrollTrigger)



const blockScript = async (container = document) => {
  const block = container.querySelector('.move_features_block');

  // add block code here

  const breakpoint = window.matchMedia('(min-width:600px)');

  let mySwiper;

  const breakpointChecker = function () {
    if (breakpoint.matches === true) {
      // clean up old instances and inline styles when available
      if (mySwiper !== undefined) {
        // console.log(mySwiper);
        // mySwiper.detachEvents();
        mySwiper.destroy(true, true);
      }
      // or/and do nothing
      return false;
    } else {
      return enableSwiper();
    }
  };

  const enableSwiper = function () {

    mySwiper = new Swiper(block.querySelector('.swiper-container'), {
      // slidesPerView: 1.196,
      slidesPerView: 1.205,
      spaceBetween: 0,
      initialSlide: 1,
      centeredSlides: true,
    });

  };

  breakpointChecker();

  breakpoint.addEventListener('change', breakpointChecker);


  //region icons animation

  //region computer animation

  const computer = gsap.timeline({
    paused: true,
    yoyo: true,
    repeat: -1,
    repeatDelay: 2,
    defaults: {
      ease: "back",
    }
  })
  computer
      .from(block.querySelector("#computer-message-1"), {
        scale: 0,
        transformOrigin: "right bottom"
      })
      .from(block.querySelector("#computer-message-2"), {
        scale: 0,
        transformOrigin: "right bottom"
      }, "<.4")
      .from(block.querySelector("#computer-location"), {
        y: -100,
        opacity: 0,
        ease: "bounce",
        transformOrigin: "center bottom"
      })
      .from(block.querySelectorAll("#computer-rect"), {
        scaleY: 0,
        transformOrigin: "bottom"
      })
      .from(block.querySelectorAll("#computer-shapes path"), {
        scaleX: 0,
        stagger: .2,
        transformOrigin: "left center"
      }, "<")
      .from(block.querySelectorAll("#shape-wrapper"), {
        scale: 0,
        transformOrigin: "center"
      })
      .from(block.querySelector("#computer-line"), {
        drawSVG: 0,
      })
  //endregion computer animation

  //region man animation

  const man = gsap.timeline({
    paused: true,
    yoyo: true,
    repeat: -1,
    repeatDelay: 2,
    defaults: {
      ease: "linear",
      duration: 5
    }
  })
  man
      .set(block.querySelector("#snow-2"), {
        yPercent: 100,
        y: 8
      })
      .fromTo(block.querySelector("#snow-wrapper"), {
        yPercent: -50,
      }, {
        yPercent: 0,
        repeat: -1,
      })
      .fromTo(block.querySelector("#umbrela"), {
        rotate: 5,
        transformOrigin: "75% 90%",
        duration: 2
      }, {
        rotate: -10,
        transformOrigin: "75% 90%",
        repeat: -1,
        yoyo: true,
        duration: 2
      }, "<")


  //endregion man animation

  //region gift animation

  const girl = gsap.timeline({
    paused: true,
    yoyo: true,
    repeat: -1,
    repeatDelay: 2,
    defaults: {
      ease: "back",
      duration: 1
    }
  })
  girl

      .from(block.querySelector("#label"), {
        scaleY: 0,
        transformOrigin: "bottom center"
      }, "<.3")
      .from(block.querySelector("#circle-wrapper"), {
        scale: 0,
        opacity: 0,
        transformOrigin: "bottom center",
        ease: "back"
      }, "<")

  //endregion gift animation

  //region car animation

  const car = gsap.timeline({
    paused: true,
    yoyo: true, repeat: -1,
    repeatDelay: 2,
    defaults: {
      repeat: -1,
      ease: "linear"
    }
  })
  car
      .to(block.querySelector("#tire"), {
        rotate: -360,
        transformOrigin: "center center",
      })
      .to(block.querySelectorAll("#phone-wave circle"), {
        scale: 1.1,
        stagger: .2,
        transformOrigin: "center",
        yoyo: true
      }, "<")


  //endregion car animation

  //region stars animation

  const stars = gsap.timeline({
    paused: true,
    yoyo: true, repeat: -1,
    repeatDelay: 2,
    defaults: {}
  })
  stars

      .from(block.querySelectorAll("#os ellipse"), {
        scale: 0,
        stagger: .2,
        transformOrigin: "center",
      })
      .from(block.querySelectorAll("#dash-road circle"), {
        scale: 0,
        stagger: .2,
        transformOrigin: "center",
      })
      .from(block.querySelectorAll("#os path"), {
        scale: 0,
        transformOrigin: "center",
      })

      .from(block.querySelectorAll("#stars_orang polygon"), {
        fill: "#9EA8DA",
        transformOrigin: "center center",
        stagger: .1,
        duration: .3
      })


  //endregion stars animation

  //region phone animation
  const phone = gsap.timeline({
    paused: true,
    yoyo: true, repeat: -1,
    repeatDelay: 2,
    defaults: {
      ease: "back"
    }
  })
  phone
      .from(block.querySelector("#mail"), {
        scale: 0,
        transformOrigin: "center center",
      })
      .from(block.querySelector("#emails-numbers"), {
        scale: 0,
        transformOrigin: "left bottom",
      }, "<.2")
      .from(block.querySelectorAll("#apps g"), {
        scale: 0,
        transformOrigin: "center center",
        stagger: .3
      })


  gsap.fromTo(block.querySelectorAll("#bell"), {
    transformOrigin: "top center",
    rotate: -10
  }, {
    transformOrigin: "top center", yoyo: true, repeat: -1,
    rotate: 10
  })
  //endregion phone animation

  //region bell animation
  const prize = gsap.timeline({
    paused: true,
    yoyo: true, repeat: -1,
    repeatDelay: 2,
    defaults: {
      ease: "back",
    }
  })
  prize
      .from(block.querySelector("#x-line"), {
        scaleX: 0,
        transformOrigin: "left",
      })
      .from(block.querySelector("#y-line"), {
        scaleY: 0,
        transformOrigin: "bottom",
      }, "<")
      .from(block.querySelectorAll(".chart"), {
        scaleY: 0,
        stagger: .2,
        transformOrigin: "bottom",
        ease: "linear"
      })
      .to(block.querySelectorAll(".chart"), {
        scaleY: 1.2,
        stagger: .2,
        transformOrigin: "bottom center",
        yoyo: true, repeat: -1,
        ease: "linear"
      })
      .to(block.querySelectorAll(".star"), {
        scale: 1.2,
        transformOrigin: "center",
        yoyo: true, repeat: -1,
      })
  //endregion bell animation

  //region bike animation
  const bikeTire = block.querySelectorAll(".bike-tire")
  const bike = block.querySelectorAll(".bike-girl #girl-bike")
  const train = block.querySelectorAll(".bike-girl #train")
  const smallCar = block.querySelectorAll(".bike-girl #car")

  const vehicles = gsap.timeline({
    paused: true,
    repeat: -1,
    repeatDelay: 2,
    defaults: {
      repeat: -1,
      ease: "linear",
    }
  })
      .fromTo(bike, {
        x: -100
      }, {
        x: 70,
        duration: 5,
      }, "<")
      .to(bikeTire, {
        rotate: 360,
        transformOrigin: "center",
      }, "<")
      .from(train, {
        xPercent: -20,
        duration: 5,
      }, "<")
      .from(smallCar, {
        xPercent: -20,
        duration: 5,
      }, "<")
  //endregion bike animation

  ScrollTrigger.create({
    trigger: block,
    start: 'top bottom',
    end: 'bottom top',
    onToggle({isActive}) {
      computer.paused(!isActive)
      man.paused(!isActive)
      girl.paused(!isActive)
      car.paused(!isActive)
      stars.paused(!isActive)
      phone.paused(!isActive)
      prize.paused(!isActive)
      vehicles.paused(!isActive)
    }
  })
  //endregion icons animation

  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


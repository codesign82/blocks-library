import './index.html';
import './style.scss';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {requestBodyGenerator} from "../../../scripts/functions/requestBodyGenerator";
import {stringToHTML} from "../../../scripts/functions/stringToHTML";
import '../../../scripts/sitesSizer/propellerhealth';

const blockScript = async (container = document) => {
  const block = container.querySelector('.events_and_webinars_block');


  const locale = 'en';
  const dateNow = new Date();
  const date = dateNow.getFullYear() + '-' + (dateNow.getMonth() + 1) + '-' + dateNow.getDate();

  const addDatesToCards = (events) => {
    const locale = 'en';

    function dateFormat(sDate, eDate) {

      let date1 = `<p>${sDate.toLocaleDateString(locale, {month: 'long'})} ${sDate.getDate()}-${eDate.getDate()} , ${sDate.getFullYear()}</p>`;
      let date2 = `<p class="mb">${sDate.toLocaleDateString(locale, {month: 'long'})} ${sDate.getDate()} - </p>
        <p>${eDate.toLocaleDateString(locale, {month: 'long'})} ${eDate.getDate()} , ${sDate.getFullYear()}</p>`;

      return sDate.getMonth() === eDate.getMonth() ? date1 : date2;
    }

    function timeFormat(x) {
      return `<p>${x.toLocaleString(locale, {
        hour: 'numeric',
        minute: 'numeric',
        hour12: true,
      })}</p>`;
    }

    for (const event of events) {
      let start = event.querySelector('.start');
      let end = event.querySelector('.end');
      let time = event.querySelector('.event-time');
      let start_date = start.dataset.startdate;
      let end_date = end.dataset.enddate;
      let sDate = new Date(start_date);
      let eDate = new Date(end_date);
      start.innerHTML = dateFormat(sDate, eDate);
      time.innerHTML = timeFormat(sDate);
    }
  };

  let events = block.querySelectorAll('.event-box');

  addDatesToCards(events);

  const postsContainer = block.querySelector('[posts-container]');
  const loadMore = block?.querySelector('.load-more-wrapper');
  let loadMoreStatus = !loadMore?.classList.contains('hidden');



  if (loadMore) {
    const initialLoadMoreArgs = loadMore && JSON.parse(loadMore.dataset.args);

    loadMore.dataset.args = JSON.stringify({
      ...initialLoadMoreArgs,
      'meta_query': {
        relation: "AND",
        0: {
          key: "end_date",
          value: date,
          compare: ">=",
          type: "DATE"
        }
      }
    });

    loadMore.addEventListener('click', async () => {
      if (loadMore.classList.contains('loading')) return;
      loadMore.classList.add('loading');
      loadMore.classList.remove('hidden');
      const response = await fetch(theme_ajax_object.ajax_url, {
        method: 'POST',
        body: requestBodyGenerator(loadMore),
      });
      loadMore.classList.remove('loading');
      const hasMorePages = !!response.headers.get('X-WP-Has-More-Pages');
      const totalPages = +response.headers.get('X-WP-Total-Pages');
      loadMore.classList.toggle('hidden', !hasMorePages);
      loadMoreStatus = hasMorePages;
      const htmlString = await response.json();
      const oldArgs = JSON.parse(loadMore.dataset.args);
      oldArgs.paged++;
      loadMore.dataset.args = JSON.stringify(oldArgs);
      const posts = stringToHTML(htmlString.data);
      for (let post of posts) {
        postsContainer.appendChild(post);
      }
      addDatesToCards(posts);
      animations(posts);
      imageLazyLoading(posts);
    });
  }

  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


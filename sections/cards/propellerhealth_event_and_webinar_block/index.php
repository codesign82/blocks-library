<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'events_and_webinars_block';
$className = 'events_and_webinars_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/events_and_webinars_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$manually_or_automatically = get_field('manually_or_automatically');
$webinar_event_posts = get_field('webinar_event_posts');

$query_options = get_field('query_options');
$number_of_posts = @$query_options['number_of_posts'];
$order = @$query_options['order'];
$args = array(
    'post_type' => 'events',
    'posts_per_page' => $number_of_posts,
    'meta_query' => array(
        'relation' => 'AND',
        array(
            'compare' => '>=',
            'key' => 'end_date',
            'value' => date('Y-m-d', strtotime('today')),
            'type' => 'DATE'
        ),
    ),
    'post_status' => 'publish',
    'order' => $order
);

$the_query = new WP_Query($args);
$have_posts = $the_query->have_posts();


?>
<!-- region propellerhealth Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="events-and-webinars">
        <?php if ($title) { ?>
            <div class="headline-2 main-title iv-st-from-bottom"><?= $title ?></div>
        <?php } ?>
        <?php if ($manually_or_automatically) { ?>
            <div class="events-wrapper">
                <?php if ($webinar_event_posts) { ?>
                    <?php foreach ($webinar_event_posts as $post) {
                        setup_postdata($post);
                        $id = $post;
                        $end_date = strtotime(get_field('end_date', $id));
                        // $end_date = get_field('end_date', $id);
                        $current_date = time();
                        $datediff = $current_date - $end_date;
                        $days = round($datediff / (60 * 60 * 24));
                        if ($datediff >= 0) {
                            continue;
                        }
//            echo $datediff;
                        get_template_part("template-parts/webinar-events-card", '', array(
                            'post_id' => $post,
                        )); ?>
                    <?php }
                } ?>
                <?php wp_reset_postdata(); ?>
            </div>
        <?php } else{ ?>
            <div class="events-wrapper" posts-container>
                <?php
                // The Query
                // The Loop
                if ($the_query->have_posts()) { ?>
                    <?php while ($the_query->have_posts()) {
                        $the_query->the_post();
                        get_template_part('template-parts/webinar-events-card', '',
                            array('post_id' => get_the_id()));
                        ?>
                    <?php } ?>
                <?php }
                wp_reset_postdata(); ?>

            </div>
            <?php $args['paged'] = 2;
            $args['meta_query'] = ''; ?>
            <div class="no-posts headline-2 <?= $the_query->have_posts() ? '' : 'active' ?>">
                <?= __('No webinar and event  Here :(', 'propellerhealth') ?></div>
            <div class="load-more-wrapper <?= $the_query->max_num_pages <= 1 ? "hidden" : '' ?>"
                 data-args='<?= json_encode($args) ?>'
                 data-template="template-parts/webinar-events-card"
            >
                <button aria-label="Load More Posts" class="cta-button load-more-btn">
                    <?= __('Load More', 'propellerhealth_client') ?>
                </button>
                <div class="loader">
                    <div class="loader-ball"></div>
                    <div class="loader-ball"></div>
                    <div class="loader-ball"></div>
                </div>
            </div>

        <?php } ?>
    </div>
</div>
</section>


<!-- endregion propellerhealth Block -->

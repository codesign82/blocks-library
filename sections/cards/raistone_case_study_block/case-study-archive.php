<?php
$post_id      = @$args['post_id'];
$post_type    = @$args['post_type'];
$thumbnail_id = get_post_thumbnail_id();
$alt          = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
$image        = get_field( 'image', $post_id );
$quotation    = get_field( 'quotation', $post_id );
$job_title    = get_field( 'job_title', $post_id );
$permalink    = get_the_permalink( $post_id );
$title        = get_the_title( $post_id ); ?>
<div class="col iv-st-from-bottom">
  <div class="card green-card">
    <div class="image-wrapper">
      <a href="<?= $permalink ?>">
        <img src="<?php thumbnail_url( $post_id ) ?>" alt="<?= $alt ?>">
      </a>
    </div>
    <a href="<?= $permalink ?>" class="headline-3 company-name"><?= $title ?></a>
    <?php if ( $quotation ) { ?>
      <div class="paragraph paragraph-normal-paragraph description">
        “<?= $quotation ?>”
      </div>
    <?php } ?>
    <a href="<?= $permalink ?>" class="btn-arrow "><?= __( 'Read Study', 'raistone' ) ?>
      <svg width="9" height="14" viewBox="0 0 9 14" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M1.03906 13L7.03906 7L1.03906 1" stroke="#0A0A0A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
      </svg>
    </a>
  </div>
</div>

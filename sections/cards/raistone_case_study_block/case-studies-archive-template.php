<?php
get_header();
/*
 * Template Name: Case Studies - Archive
 * */
$id = get_the_ID();
$archive_title        = get_field( 'archive_title',$id );
$archive_description = get_field( 'archive_description',$id );
$query_options       = get_field( 'query_options',$id );
$order               = @$query_options['order'];
$posts_per_page      = @$query_options['posts_per_page'];
?>
  <section class="second_hero_block second_hero_archive theme-two" data-section-class="second_hero_block">
    <div class="container">
      <div class="hero-content-wrapper">
        <div class="left-content">
          <?php if ( $archive_title ) { ?>
            <h2 class="headline-1 title post-title"><?= $archive_title ?></h2>
          <?php } ?>
          <?php if ( $archive_description ) { ?>
            <div class="paragraph paragraph-xl-paragraph-md description"><?= $archive_description ?></div>
          <?php } ?>
        </div>
      </div>
    </div>
  </section>
<?php
$args       = array(
  'post_type'      => 'case_studies',
  'posts_per_page' => $posts_per_page,
  'order'          => $order
);
$the_query  = new WP_Query( $args );
$have_posts = $the_query->have_posts();
?>
<?php if ( $have_posts ) { ?>
  <section class="case_study_block" data-section-class="case_study_block">
    <div class="container">
      <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3" posts-container>
        <?php while ( $the_query->have_posts() ) : $the_query->the_post();
          get_template_part( 'template-parts/case-study-archive', '', array( 'post_id' => get_the_ID() ) );
        endwhile; ?>
        <?php wp_reset_postdata(); ?>
      </div>
      <?php $args['paged']    = 2;?>
      <div class="no-posts headline-2 <?= $have_posts ? '' : 'active' ?>"><?= __( 'No Case Study Here :(', 'raistone' ) ?></div>
      <div class="load-more-wrapper <?= $the_query->max_num_pages <= 1 ? "hidden" : "" ?>"
           data-args='<?= json_encode( $args ) ?>'
           data-template="template-parts/case-study-archive">
        <button aria-label="Load More Posts" class="btn green-btn load-more-btn"><?= __( 'Load More', 'raistone' ) ?>
        </button>
        <div class="loader">
          <div class="loader-ball"></div>
          <div class="loader-ball"></div>
          <div class="loader-ball"></div>
        </div>
      </div>
    </div>
  </section>
<?php } ?>
<?php
get_footer();


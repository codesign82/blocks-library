<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'success_stories_block';
$className = 'success_stories_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/success_stories_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title       = get_field( 'title' );
$title_width = get_field( 'title_width' );
$description = get_field( 'description' );
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<?php if ( $title_width ) { ?>
  <style>
    @media screen and (min-width: 992px) {
    <?='#'.$id?> .success_stories_block_title {
      max-width: <?=$title_width?>%;
    }
    }
  </style>
<?php } ?>
<div class="container">
  <div class="success_stories_text bottom-border">
    <?php if ( $title ) { ?>
      <h2
        class="success_stories_block_title headline-2 "><?= $title ?></h2>
    <?php } ?>
    <?php if ( $description ) { ?>
      <div class="success_stories_description paragraph ">
        <?= $description ?>
      </div>
    <?php } ?>
  </div>
  <?php if ( have_rows( 'success_stories' ) ) { ?>
  <div class="swiper-container">
    <div class="swiper-wrapper">
      <?php while ( have_rows( 'success_stories' ) ) {
      the_row();
      $title              = get_sub_field( 'title' );
      $description        = get_sub_field( 'description' );
      $read_more          = get_sub_field( 'read_more' );
      $logo               = get_sub_field( 'logo' );
      $title_and_logo_url = get_sub_field( 'title_and_logo_url' ); ?>
      <div class="swiper-slide">
        <div class="rolls box-shadow">
          <div class="rolls-text">
            <?php if ( $title ) { ?>
            <<?= ! $title_and_logo_url ? 'h3' : 'a href="' . $title_and_logo_url . '" target="_blank"' ?>
            class="headline-3"><?= $title ?></<?= ! $title_and_logo_url ? 'h3' : 'a' ?>
          >
          <?php } ?>
          <?php if ( $description ) { ?>
            <div class="paragraph"><?= $description ?></div>
          <?php } ?>
          <?php if ( $read_more ) { ?>
            <a class="learn-more" href="<?= $read_more['url'] ?>"
               target="<?= $read_more['target'] ?: '_self' ?>">
              <?= $read_more['title'] ?>
              <svg width="16" height="14" viewBox="0 0 16 14" fill="none">
                <path fill-rule="evenodd" clip-rule="evenodd"
                      d="M0 7C0 6.73483 0.105345 6.48052 0.292861 6.29302C0.480376 6.10552 0.734701 6.00018 0.999888 6.00018H12.5846L8.29107 1.70896C8.10332 1.52122 7.99784 1.26659 7.99784 1.00108C7.99784 0.735579 8.10332 0.48095 8.29107 0.293211C8.47883 0.105471 8.73347 0 8.99899 0C9.26452 0 9.51916 0.105471 9.70692 0.293211L15.7062 6.29213C15.7994 6.385 15.8732 6.49533 15.9236 6.6168C15.9741 6.73827 16 6.86849 16 7C16 7.13151 15.9741 7.26173 15.9236 7.3832C15.8732 7.50467 15.7994 7.615 15.7062 7.70787L9.70692 13.7068C9.51916 13.8945 9.26452 14 8.99899 14C8.73347 14 8.47883 13.8945 8.29107 13.7068C8.10332 13.5191 7.99784 13.2644 7.99784 12.9989C7.99784 12.7334 8.10332 12.4788 8.29107 12.291L12.5846 7.99982H0.999888C0.734701 7.99982 0.480376 7.89448 0.292861 7.70698C0.105345 7.51948 0 7.26517 0 7Z"
                      fill="white"/>
              </svg>
            </a>
          <?php } ?>
        </div>
        <?php if ( $logo ) { ?>
        <<?= ! $title_and_logo_url ? 'picture' : 'a href="' . $title_and_logo_url . '" target="_blank"' ?>
        class="rolls-img">
        <img src="<?= $logo['url'] ?>" alt="<?= $logo['alt'] ?>">
      </<?= ! $title_and_logo_url ? 'picture' : 'a' ?>>
    <?php } ?>
    </div>
  </div>
<?php } ?>
</div>
  </div>
<?php } ?>
</div>
</section>


<!-- endregion ZineOne's Block -->

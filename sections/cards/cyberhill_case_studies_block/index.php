<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$back_ground_color = get_field('back_ground_color');
$block_theme = get_field('block_theme');
$dataClass = 'case_studies_block';
$className = 'case_studies_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}


if ($back_ground_color === 'blue') {
  $className .= ' blue no-side-border ';
}
if ($back_ground_color === 'white') {
  $className .= ' white';
}
if ($back_ground_color === 'grey') {
  $className .= ' grey   show-side-border ';
}
if ($block_theme === 'blue' && $back_ground_color === 'grey') {
  $className .= ' grey  show-side-border ';
}

if ($back_ground_color === 'blue' && $block_theme === 'grey') {
  $className .= ' grey  show-side-border ';
}

if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/case_studies_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$section_title = get_field('section_title');
$view_all = get_field('view_all');
$manual_entry = get_field('manual_entry');
$view_all = get_field('view_all');
$view_all_manual_entry = get_field('view_all_manual_entry');
$new_version = get_field('new_version');
$has_layer = $new_version ? ' ' : 'has-layer';
?>
<!-- region CyberHill's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="case_studies-wrapper">
    <?php if ($section_title) { ?>
      <div class="title-with-shape title-with-shape-blue iv-st-from-left">
        <?= $section_title ?>
      </div>
    <?php } ?>
    <?php
    if (!$manual_entry) {
      $case_studies = get_field('case_studies');
      if ($case_studies): ?>
        <div class="swiper mySwiper <?= $has_layer ?> iv-st-from-bottom">
          <div class="swiper-wrapper">
            <?php foreach ($case_studies as $case_study):
              if ($new_version) {
                get_template_part('template-parts/new-version-case-studies-card', '', array('id' => $case_study));

              } else {
                get_template_part('template-parts/case-studies-card', '', array('id' => $case_study));
              }
            endforeach; ?>
          </div>
        </div>
      <?php endif; ?>
    <?php } else {
      if (have_rows('manual_entry_repeater')) { ?>
        <div class="swiper mySwiper <?= $has_layer ?> iv-st-from-bottom">
          <div class="swiper-wrapper">
            <?php while (have_rows('manual_entry_repeater')) {
              the_row();
              if ($new_version) {
                get_template_part('template-parts/new-version-case-studies-card-manual');
              } else {
                get_template_part('template-parts/case-studies-card-manual');
              }
              ?>
            <?php } ?>
          </div>
        </div>
      <?php } ?>
    <?php } ?>
    <div class="all-and-swiper-button iv-st-from-right">
      <?php if ($manual_entry) : ?>
        <?php if ($view_all_manual_entry) { ?>
          <a href="<?= $view_all_manual_entry['url'] ?>" target="<?= $view_all_manual_entry['target'] ?>" class="primary-button"><?= $view_all_manual_entry['title'] ?></a>
        <?php } ?>
      <?php else : ?>
        <?php if ($view_all) { ?>
          <a href="<?= $view_all['url'] ?>" target="<?= $view_all['target'] ?>" class="primary-button"><?= $view_all['title'] ?></a>
        <?php } ?>
      <?php endif; ?>
      <div class="swiper-buttons">
        <div class="swiper-button swiper-button-prev ">
          <svg viewBox="0 0 24 14" fill="none"
               xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd"
                  d="M8.03467 13.2058L2.26815 7.60735L24 7.60735V6.48433L2.17373 6.48433L8.03467 0.794195L7.21706 0L0.824749 6.20602L0.817617 6.1991L1.90735e-06 6.99329L0.00692368 7.00001L1.90735e-06 7.00673L0.817617 7.80093L0.824749 7.794L7.21706 14L8.03467 13.2058Z"
                  fill="#1D4070"/>
          </svg>

        </div>
        <div class="swiper-button swiper-button-next ">
          <svg viewBox="0 0 24 14" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M15.9653 0.794176L21.7319 6.39265L-7.62939e-06 6.39265L-7.62939e-06 7.51567L21.8263 7.51567L15.9653 13.2058L16.7829 14L23.1753 7.79398L23.1824 7.8009L24 7.00671L23.9931 6.99999L24 6.99327L23.1824 6.19907L23.1753 6.206L16.7829 -1.90735e-05L15.9653 0.794176Z" fill="#1D4070"/>
          </svg>

        </div>
      </div>
    </div>
  </div>
</div>
</section>


<!-- endregion CyberHill's Block -->

import './index.html';
import './style.scss';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {Navigation, Swiper} from 'swiper'
import {isChrome} from "../../../scripts/functions/isChrome";
import '../../../scripts/sitesSizer/cyberhill';

const blockScript = async (container = document) => {
  const block = container.querySelector('.case_studies_block');
  
  function isMacintosh() {
    return navigator.platform.indexOf('Mac') > -1
  }
  
  function isWindows() {
    return navigator.platform.indexOf('Win') > -1
  }

// add block code here
  if (isWindows() && isChrome()) {
    block.classList.add('is_chrome');
  }
  
  Swiper.use([Navigation]);
  // add block code here
  let slides = block.querySelectorAll('.swiper-slide');
  
  new Swiper(block.querySelector('.mySwiper'), {
    slidesPerView: 1.14,
    loop: true,
    spaceBetween: 16,
    navigation: {
      nextEl: block.querySelector('.swiper-button-next'),
      prevEl: block.querySelector('.swiper-button-prev')
    },
    breakpoints: {
      600: {
        slidesPerView: 2,
        spaceBetween: 24,
        
      },
      992: {
        // slidesPerView: 2.85,
        slidesPerView: 3,
        spaceBetween: 24,
      }
    }
    
  })
  
  let perspectiveFlips = block.querySelectorAll('.perspective-flip');
  
  perspectiveFlips.forEach((perspectiveFlips) => {
    perspectiveFlips?.addEventListener('click', function (e) {
      perspectiveFlips.classList.toggle('add-hover');
    });
  })
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


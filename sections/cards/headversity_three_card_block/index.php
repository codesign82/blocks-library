<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'three_card_block';
$className = 'three_card_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/three_card_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$sub_title = get_field('sub_title');
//$cards_repeater = get_field('cards_repeater');
//$form = get_field('form');
//$logo = get_field('logo');
//$modal_title = get_field('modal_title');
//$modal_description = get_field('modal_description');
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="contact-us-wrapper">
        <?php if ($sub_title) { ?>
            <div class="sub-title iv-st-from-bottom">
                <?= $sub_title ?>
            </div>
        <?php } ?>
        <?php if ($title) { ?>
            <div move-to-here class="headline-3 contact-title iv-st-from-bottom">
                <?= $title ?>
            </div>
        <?php } ?>
        <div class="contact-wrapper ">
            <?php if (have_rows('cards_repeater')) { ?>
                <div class="row  row-cols-1 row-cols-md-2 row-cols-lg-3">
                    <?php while (have_rows('cards_repeater')) {
                        the_row();
                        $card_title = get_sub_field('card_title');
                        $card_description = get_sub_field('card_description');
                        $link = get_sub_field('link');
//            $modal = get_sub_field('modal');
                        $form = get_sub_field('form');
                        $logo = get_sub_field('logo');
                        $modal_title = get_sub_field('modal_title');
                        $modal_description = get_sub_field('modal_description');
                        ?>
                        <div class="col iv-st-from-bottom">
                            <div class="contact-column ">
                                <?php if ($card_title) { ?>
                                    <h2 class="title">
                                        <?= $card_title ?>
                                    </h2>
                                <?php } ?>
                                <?php if ($card_description) { ?>
                                    <div class="contact-description paragraph">
                                        <?= $card_description ?>
                                    </div>
                                <?php } ?>
                                <div class="link">
                                    <div>
                                        <svg class="arrow" viewBox="0 0 29.5 22.1" width="28" height="15"
                                             role="application">
                                            <path class="arrow-head" role="application"
                                                  d="M29.1,12.1a1.7,1.7,0,0,0,0-2.1L19.5.4a1.7,1.7,0,0,0-2.1,0,1.6,1.6,0,0,0,0,2.2L25.9,11l-8.5,8.5a1.6,1.6,0,0,0,0,2.2,1.7,1.7,0,0,0,2.1,0Z"
                                                  fill="#f90"/>
                                            <path class="arrow-line" d="M0,12.5H28v-3H0Z"
                                                  fill="#f90"/>
                                        </svg>
                                    </div>
                                </div>
                                <!--Modal-->

                                <div class="modal-form " aria-modal="true" role="dialog">
                                    <div class="container">
                                        <div class="webinar-wrapper">
                                            <svg class="exit" width="39" height="39" role="application"
                                                 viewBox="0 0 39 39"
                                                 fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <line x1="9.90039" y1="28.2841" x2="28.2852" y2="9.8993"
                                                      stroke="#FF9900" stroke-width="3"
                                                      stroke-linecap="round"/>
                                                <path d="M28.9922 28.2841L10.6074 9.89928"
                                                      stroke="#FF9900"
                                                      stroke-width="3" stroke-linecap="round"/>
                                            </svg>
                                            <?php if ($logo) { ?>
                                                <picture class="form-img">
                                                    <img src="<?= $logo['url'] ?>"
                                                         alt="<?= $logo['alt'] ?>">
                                                </picture>
                                            <?php }
                                            if ($modal_title) { ?>
                                                <div class="form-title">
                                                    <?= $modal_title ?>
                                                </div>
                                            <?php }
                                            if ($modal_description) { ?>
                                                <div
                                                        class=" paragraph m-paragraph form-description ">
                                                    <?= $modal_description ?>
                                                </div>
                                            <?php } ?>
                                            <?php if ($form) { ?>
                                                <?php
                                                echo do_shortcode('[gravityform id="' . $form . '" ajax="true" title="false"  description="false"]');
                                                ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
</section>


<!-- endregion headversity's Block -->

import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/propellerhealth';


import {gsap} from "gsap";

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.three_card_block');

    // add block code here


    // const modal = block.querySelectorAll(".modal-form");
    // const exit = block.querySelectorAll(".exit");
    // const arrows = block.querySelectorAll(".arrow");
    //
    //
    // for (let i = 0; i < arrows.length; i++) {
    //     arrows[i].addEventListener("click", function (e) {
    //         e.preventDefault();
    //         modal[i].classList.add("show")
    //         document.documentElement.classList.add('modal-opened');
    //
    //     });
    //     exit[i]?.addEventListener("click", hide);
    //     modal[i]?.addEventListener("click", hide);
    //
    //     function hide(e) {
    //
    //         if (e.target.closest('.webinar-wrapper') && !e.target.closest('.exit')) return;
    //         modal[i].classList.remove("show")
    //         document.documentElement.classList.remove('modal-opened');
    //
    //     }
    //
    //
    //     window.addEventListener('keydown', function (event) {
    //         if (event.key === 'Escape') {
    //             hide(event);
    //         }
    //     })
    //
    // }


    animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


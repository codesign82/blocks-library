<?php
$post_id = @$args['post_id'] ?: get_the_ID();
$background_color = get_field('background_color', $post_id);
$post_title = get_the_title($post_id);
$post_link = get_permalink($post_id);
$post_type = get_post_type($post_id);
$thumbnail_url = get_the_post_thumbnail_url($post_id);

?>
<div class="cards_posts iv-st-from-bottom">
  <div class="card_post" style="background:<?= $background_color ?>">
    <a href="<?= $post_link ?>">
      <?= get_post_image($post_id, 'thumbnail') ?>
    </a>
  </div>
  <div class="card_content_center">
    <div class="content_post">
      <div class="title_content_post paragraph"> <?= $post_type ?></div>
      <?php if ($post_title) { ?>
        <a class="cta-link post_link headline-4" href="<?= $post_link ?>"><?= $post_title ?></a>
      <?php } ?>
    </div>
    <a class="cta-link last_link" href="<?= $post_link ?>"><?= __('learn more', 'smart_dv') ?>
      <svg width="23" height="12" viewBox="0 0 23 12" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M23 6L13 0.226497V11.7735L23 6ZM0 7H14V5H0V7Z" fill="#BFD730"/>
      </svg>
    </a>
  </div>

</div>



<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'whats_new_section';
$className = 'whats_new_section';
$has_shape = get_field('has_shape');
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if ($has_shape) {
    $className .= ' has-shape';
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/whats_new_section/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$automatically_or_manual = get_field('automatically_or_manual');
$featured_posts = get_field('featured_posts');
$automatically_or_manual = get_field('automatically_or_manual');
$testimonials = get_field('testimonials');
$cta_link = get_field('cta_link');
$query_options = get_field('query_options');
$cat = get_field('select_category');
$query_options = get_field('query_options');
$order = @$query_options['order'];
$posts_per_page = @$query_options['number_of_posts'];
$isEvent = get_field('is_event');
$args = '';
$the_query = '';
$have_posts = '';
//$event_link = get_post_type_archive_link('events');

?>
<!--   region samrt_dv Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <?php if ($automatically_or_manual === 'manual') { ?>
        <?php if ($featured_posts) { ?>
            <?php if ($title) { ?>
                <div class="have-title">
                    <div class="headline-2 title-posts word-up"><?= $title ?></div>
                </div>
            <?php } ?>
            <div class="wrapper_post">
                <?php foreach ($featured_posts as $post):
                    $background_color = get_field('background_color', $post);
                    $post_title = get_the_title($post);
                    $post_link = get_permalink($post);
                    $post_type = get_post_type($post);
                    ?>
                    <div class="cards_posts iv-st-from-bottom">
                        <div class="card_post" style="background:<?= $background_color ?>">
                            <?= get_post_image($post, 'thumbnail') ?>
                        </div>
                        <div class="card_content_center">
                            <div class="content_post">
                                <div class="title_content_post paragraph"><?= $post_type ?></div>
                                <a class="cta-link post_link headline-4" href="<?= $post_link ?>"><?= $post_title ?></a>
                            </div>
                            <a class="cta-link last_link" href="<?= $post_link ?>"><?= __('learn more', 'smart_dv') ?>
                                <svg width="23" height="12" viewBox="0 0 23 12" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M23 6L13 0.226497V11.7735L23 6ZM0 7H14V5H0V7Z" fill="#BFD730"/>
                                </svg>
                            </a>
                        </div>

                    </div>
                <?php endforeach; ?>
            </div>
            <?php if ($cta_link) { ?>
                <div class="have_link">
                    <a class="cta-button iv-st-from-bottom" href="<?= $cta_link['url'] ?>"
                       target="<?= $cta_link['target'] ?>">
                        <?= $cta_link['title'] ?></a>
                </div>
            <?php } ?>
        <?php } ?>
    <?php } else { ?>
        <?php
        if ($isEvent) {
            $args = array(
                'posts_per_page' => 3,
                'post_status' => 'publish',
                'post_type' => 'events',
                'order' => $order,
            );
            $the_query = new WP_Query($args);
            $have_posts = $the_query->have_posts();
            if ($have_posts) {
                ?>
                <div class="headline-2 title-posts word-up"> <?= __('Upcoming Events', 'smart_dv') ?></div>
                <div class="wrapper_post">
                    <?php while ($the_query->have_posts()) {
                        $the_query->the_post();
                        get_template_part("template-parts/post-card", '', array('post_id' => get_the_ID()));
                        ?>
                    <?php } ?>
                    <?php wp_reset_postdata(); ?>
                </div>
                <div class="have_link">
                    <a class="cta-button iv-st-from-bottom" href="<?= get_post_type_archive_link('events') ?>">
                        <?= __('See All Events', 'smart_dv') ?></a>
                </div>
                <?php
            }
        } else {
            if ($cat) {
                $cat_id = $cat;
                $cat_name = get_cat_name($cat);
                $cat_slug = get_category_by_slug($cat);
                $cat_link = get_category_link($cat_id);
                $args = array(
                    'posts_per_page' => 10,
                    'cat' => $cat_id,
                    'post_status' => 'publish',
                    'post_type' => 'post',
                    'order' => $order,
                );
                $the_query = new WP_Query($args);
                $have_posts = $the_query->have_posts();
                if ($have_posts) {
                    ?>
                    <div class="headline-2 title-posts iv-st-from-bottom"> <?= $cat_name ?></div>
                    <div class="wrapper_post">
                        <?php while ($the_query->have_posts()) {
                            $the_query->the_post();
                            get_template_part("template-parts/post-card", '', array('post_id' => get_the_ID()));
                            ?>
                        <?php } ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                    <div class="have_link">
                        <a class="cta-button iv-st-from-bottom" href="<?= $cat_link ?>">
                            <?= __("See All $cat_name", 'smart_dv') ?></a>
                    </div>
                    <?php
                }
            }
        }
        ?>
        <?php
        $args['paged'] = 2;
        $args['posts_per_page'] = $posts_per_page;
        ?>
    <?php } ?>
</div>
</section>


<!-- endregion samrt_dv Block -->


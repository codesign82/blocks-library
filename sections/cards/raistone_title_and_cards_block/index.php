<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'title_and_cards_block';
$className = 'title_and_cards_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/title_and_cards_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="title-and-cards-content">
        <?php if ($title) { ?>
            <h2 class="headline-2 title iv-st-from-bottom"><?= $title ?></h2>
        <?php } ?>
        <?php if (have_rows('cards')) { ?>
            <div class="row">
                <?php while (have_rows('cards')) {
                    the_row();
                    $svg_or_image = get_sub_field('svg_or_image');
                    $svg = get_sub_field('svg');
                    $image = get_sub_field('image');
                    $title = get_sub_field('title');
                    $subtitle = get_sub_field('subtitle');
                    $description = get_sub_field('description');
                    ?>
                    <div class="col-12 col-md-6 col-lg-4 iv-st-from-bottom">
                        <div class="card">
                            <?php if (!$svg_or_image) { ?>
                                <?php if ($svg) { ?>
                                    <div class="card-icon">
                                        <?= $svg ?>
                                    </div>
                                <?php } ?>
                            <?php } else { ?>
                                <?php if ($image) { ?>
                                    <picture class="card-icon">
                                        <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                                    </picture>
                                <?php } ?>
                            <?php } ?>
                            <?php if ($title) { ?>
                                <h3 class="headline-3 card-title"><?= $title ?></h3>
                            <?php } ?>
                            <?php if ($subtitle) { ?>
                                <div class="paragraph paragraph-xl-paragraph-md small-title"><?= $subtitle ?></div>
                            <?php } ?>
                            <?php if ($description) { ?>
                                <div class="list paragraph paragraph-l-paragraph">
                                    <?= $description ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>

</section>


<!-- endregion raistone's Block -->

<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'knowledge_portal_cards_block';
$className = 'knowledge_portal_cards_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/knowledge_portal_cards_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$section_title = get_field('section_title');
$description = get_field('description');
$select_category = get_field('select_category')

?>
<!-- region CyberHill's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="posts-wrapper">
    <?php if ($section_title) { ?>
      <div class="title-with-shape title-with-shape-blue">
        <?= $section_title ?>
      </div>
    <?php } ?>




    <?php
    $index = 1;
    $args = [
      'post_type' => 'post',
      'posts_per_page' => 3,
      'cat' => $select_category,
      'post_status' => 'publish',
    ];
    $the_query = new WP_Query($args);
    $have_posts = $the_query->have_posts();
    if ($have_posts) { ?>
      <div class="row" posts-container>
        <?php while ($the_query->have_posts()) {
          $the_query->the_post();

          get_template_part('template-parts/post-card', '', array('post_id' => get_the_ID(),'index' => $index));
          $index++;
        }
        wp_reset_postdata(); ?>
      </div>

    <?php } ?>

    <?php $args['paged'] = 2 ?>
    <div class="no-posts headline-2 <?= $have_posts ? '' : 'active' ?>"><?= __('No Posts Here', 'cyberHill') ?></div>
    <div class="load-more-wrapper <?= $the_query->max_num_pages <= 1 ? "hidden" : "" ?>"
         data-args='<?= json_encode($args) ?>'
         data-template="template-parts/post-card"
    >
      <button aria-label="Load More Faqs" class="primary-button">
        Load More
      </button>
      <div class="loader">
        <div class="loader-ball"></div>
        <div class="loader-ball"></div>
        <div class="loader-ball"></div>
      </div>
    </div>

  </div>

</div>
</section>


<!-- endregion CyberHill's Block -->

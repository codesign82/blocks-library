import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {requestBodyGenerator} from "../../../scripts/functions/requestBodyGenerator";
import {stringToHTML} from "../../../scripts/functions/stringToHTML";
import '../../../scripts/sitesSizer/cyberhill';

const blockScript = async (container = document) => {
  const block = container.querySelector('.knowledge_portal_cards_block');
  
  // add block code here
  const noPosts = block.querySelector('.no-posts');
  const loadMore = block.querySelector('.load-more-wrapper');
  const postsContainer = block.querySelector('[posts-container]');
  
  loadMore.addEventListener('click', async () => {
    if (loadMore.classList.contains('loading')) return;
    loadMore.classList.add('loading');
    loadMore.classList.remove('hidden');
    noPosts.classList.remove('active');
    
    const response = await fetch(theme_ajax_object.ajax_url, {
      method: 'POST',
      body: requestBodyGenerator(loadMore),
    })
    
    loadMore.classList.remove('loading')
    const hasMorePages = !!response.headers.get('X-WP-Has-More-Pages');
    const totalPages = +response.headers.get('X-WP-Total-Pages');
    noPosts.classList.toggle('active', totalPages === 0)
    loadMore.classList.toggle('hidden', !hasMorePages);
    const htmlString = await response.json()
    const oldArgs = JSON.parse(loadMore.dataset.args);
    oldArgs.paged++;
    loadMore.dataset.args = JSON.stringify(oldArgs);
    
    const posts = stringToHTML(htmlString.data);
    
    for (let post of posts) {
      postsContainer.appendChild(post);
    }
    animations(posts);
    imageLazyLoading(posts);
  })
  
};
windowOnLoad(blockScript);


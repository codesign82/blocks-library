import './index.html';
import './style.scss';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/advisorhub';
import {gsap} from "gsap";

const blockScript = async (container = document) => {
  const blocks = container.querySelector('.advisorhub_posts_block');

// add block code here
  for (let block of blocks) {
    const heroContent = block.querySelectorAll(".text-animation >*");
    const heroImage = block.querySelector(".image-animation");
    window.addEventListener('js-loaded', () => {

      gsap.timeline()
          .from(heroImage, {
            yPercent: 50,
            duration: 1
          })
          .from(heroImage, {
            opacity: 0,
            duration: 1,
            ease: "power1.in"
          }, "<")
          .from(heroContent, {
            y: 50,
            opacity: 0,
            stagger: .4,
          }, "<50%")
    });
    let clipboard = document.querySelector('.clipboard');
    if (clipboard) {
      clipboard?.addEventListener('click', function () {
        clipboard?.classList.add('clicked');
        setTimeout(() => {
          clipboard?.classList.remove('clicked');
        }, 3000);
        navigator.clipboard.writeText(window.location.href);
      });
    }
    animations(block);
    imageLazyLoading(block);
  }

};
windowOnLoad(blockScript);


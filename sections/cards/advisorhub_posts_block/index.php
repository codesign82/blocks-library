<?php
// Create id attribute allowing for custom "anchor" value.
if (!is_admin()) {
    $id = $block['id'];
    if (!empty($block['anchor'])) {
        $id = $block['anchor'];
    }

// Create class attribute allowing for custom "className" and "align" values.
    $dataClass = 'advisorhub_posts_block';
    $className = 'advisorhub_posts_block testing';
    if (!empty($block['className'])) {
        $className .= ' ' . $block['className'];
    }
    if (!empty($block['align'])) {
        $className .= ' align' . $block['align'];
    }
    if (get_field('is_screenshot')) :
        /* Render screenshot for example */
        echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/advisorhub_posts_block/screenshot.png" >';

        return;
    endif;

    /****************************
     *     Custom ACF Meta      *
     ****************************/

    $query_options = get_field('query_options');
    if (is_array($query_options) && !empty($query_options) && isset($query_options['query_options'])) {
        $query_options = $query_options['query_options'];
        $has_border = array_key_exists('has_border', $query_options) ? $query_options['has_border'] : false;
        $post_background = array_key_exists('post_background', $query_options) ? $query_options['post_background'] : false;
        $main_title = array_key_exists('main_title', $query_options) ? $query_options['main_title'] : '';
        $side_title = array_key_exists('side_title', $query_options) ? $query_options['side_title'] : '';
        $show_posts_manually_or_automatically = array_key_exists('show_posts_manually_or_automatically', $query_options) ? $query_options['show_posts_manually_or_automatically'] : '';
        $choose_post_type = array_key_exists('choose_post_type', $query_options) ? $query_options['choose_post_type'] : '';
        $post_type = array_key_exists('post_type', $query_options) ? $query_options['post_type'] : '';
        $number_of_posts = array_key_exists('number_of_posts', $query_options) ? $query_options['number_of_posts'] : '';
        $offset = array_key_exists('offset', $query_options) ? $query_options['offset'] : '';
        $order = array_key_exists('order', $query_options) ? $query_options['order'] : '';
        $post_taxonomies = array_key_exists('post_taxonomies', $query_options) ? $query_options['post_taxonomies'] : '';
        $deals_comps_taxonomies = array_key_exists('deals_comps_taxonomies', $query_options) ? $query_options['deals_comps_taxonomies'] : '';
        $hub_resources_taxonomies = array_key_exists('hub_resources_taxonomies', $query_options) ? $query_options['hub_resources_taxonomies'] : '';
        $fintech_taxonomies = array_key_exists('fintech_taxonomies', $query_options) ? $query_options['fintech_taxonomies'] : '';
        $asset_manager_taxonomies = array_key_exists('asset_manager_taxonomies', $query_options) ? $query_options['asset_manager_taxonomies'] : '';
        $choose_loop_style = array_key_exists('choose_loop_style', $query_options) ? $query_options['choose_loop_style'] : '';
    } else {
        $has_border = false;
        $post_background = false;
        $main_title = '';
        $side_title = '';
        $show_posts_manually_or_automatically = '';
        $choose_post_type = '';
        $post_type = '';
        $number_of_posts = '';
        $offset = '';
        $order = '';
        $post_taxonomies = '';
        $deals_comps_taxonomies = '';
        $hub_resources_taxonomies = '';
        $fintech_taxonomies = '';
        $asset_manager_taxonomies = '';
        $choose_loop_style = '';

    }
    ?>
    <!-- region advisorhub's Block -->
    <?php general_settings_for_blocks($id, $className, $dataClass); ?>
    <?php
    if ($show_posts_manually_or_automatically === 'automatically') {
        $selected_taxonomies = array();
        switch ($post_type) {
            case 'post':
                $selected_taxonomies = $post_taxonomies;
                break;
            case 'deals_and_comps':
                $selected_taxonomies = $deals_comps_taxonomies;
                break;
            case 'hub':
                $selected_taxonomies = $hub_resources_taxonomies;
                break;
            case 'fintech':
                $selected_taxonomies = $fintech_taxonomies;
                break;
            case 'asset_manager':
                $selected_taxonomies = $asset_manager_taxonomies;
                break;
        }
        $args = array(
            'post_type' => $post_type ?: 'post',
            'posts_per_page' => $number_of_posts ?: ($choose_loop_style === 'style_5' ? 1 : 9),
            'offset' => $offset ?: '',
            'order' => $order ?: 'desc',
            'post_status' => 'publish',
            'tax_query' => array(
                'relation' => 'AND',
            )
        );
        foreach ($selected_taxonomies as $key => $value) {
            if ($value) {
                $args['tax_query'][] = array(
                    'taxonomy' => $key,
                    'field' => 'id',
                    'terms' => $value
                );
            }
        }
        $the_query = new WP_Query($args);
        if ($the_query->have_posts()) {
            $args = array(
                'the_query' => $the_query,
                'has_border' => $has_border,
                'post_background' => $post_background,
                'main_title' => $main_title,
                'side_title' => $side_title,
                'selected_taxonomies' => $selected_taxonomies,
                'post_type' => $post_type ?: 'post',
            );
            switch ($choose_loop_style) {
                case "style_1":
                    get_template_part('template-parts/loop-styles/loop-style-1', 'style_1', $args);
                    break;
                case "style_2":
                    get_template_part('template-parts/loop-styles/loop-style-2', 'style_2', $args);
                    break;
                case "style_3":
                    get_template_part('template-parts/loop-styles/loop-style-3', 'style_3', $args);
                    break;
                case "style_4":
                    get_template_part('template-parts/loop-styles/loop-style-4', 'style_4', $args);
                    break;
                case "style_5":
                    get_template_part('template-parts/loop-styles/loop-style-5', 'style_5', $args);
                    break;
                case "style_6":
                    get_template_part('template-parts/loop-styles/loop-style-6', 'style_6', $args);
                    break;
                case "style_7":
                    get_template_part('template-parts/loop-styles/loop-style-7', 'style_7', $args);
                    break;
                case "style_8":
                    get_template_part('template-parts/loop-styles/loop-style-8', 'style_8', $args);
                    break;
                case "style_9":
                    get_template_part('template-parts/loop-styles/loop-style-9', 'style_9', $args);
                    break;
                case "style_10":
                    get_template_part('template-parts/loop-styles/loop-style-10', 'style_10', $args);
                    break;
                case "style_11":
                    get_template_part('template-parts/loop-styles/loop-style-11', 'style_11', $args);
                    break;
                case "style_12":
                    get_template_part('template-parts/loop-styles/loop-style-12', 'style_12', $args);
                    break;
                default:
                    get_template_part('template-parts/loop-styles/loop-style-7');
            }
        } else {
            echo 'no posts available';
        }
        wp_reset_postdata();
    } elseif ($show_posts_manually_or_automatically === 'manually') {
        $select_posts = $query_options['select_posts'];
        $the_query = new WP_Query(array(
            'post__in' => $select_posts,
            'post_status' => 'publish',
            'post_type' => 'any',
            'orderby' => 'post__in'
        ));
        if ($select_posts) {
            $args = array(
                'the_query' => $the_query,
                'has_border' => $has_border,
                'post_background' => $post_background,
                'main_title' => $main_title,
                'side_title' => $side_title,
            );
            switch ($choose_loop_style) {
                case "style_1":
                    get_template_part('template-parts/loop-styles/loop-style-1', 'style_1', $args);
                    break;
                case "style_2":
                    get_template_part('template-parts/loop-styles/loop-style-2', 'style_2', $args);
                    break;
                case "style_3":
                    get_template_part('template-parts/loop-styles/loop-style-3', 'style_3', $args);
                    break;
                case "style_4":
                    get_template_part('template-parts/loop-styles/loop-style-4', 'style_4', $args);
                    break;
                case "style_5":
                    get_template_part('template-parts/loop-styles/loop-style-5', 'style_5', $args);
                    break;
                case "style_6":
                    get_template_part('template-parts/loop-styles/loop-style-6', 'style_6', $args);
                    break;
                case "style_7":
                    get_template_part('template-parts/loop-styles/loop-style-7', 'style_7', $args);
                    break;
                case "style_8":
                    get_template_part('template-parts/loop-styles/loop-style-8', 'style_8', $args);
                    break;
                case "style_9":
                    get_template_part('template-parts/loop-styles/loop-style-9', 'style_9', $args);
                    break;
                case "style_10":
                    get_template_part('template-parts/loop-styles/loop-style-10', 'style_10', $args);
                    break;
                case "style_11":
                    get_template_part('template-parts/loop-styles/loop-style-11', 'style_11', $args);
                    break;
                case "style_12":
                    get_template_part('template-parts/loop-styles/loop-style-12', 'style_12', $args);
                    break;
                default:
                    get_template_part('template-parts/loop-styles/loop-style-7');
            }
        } else {
            echo 'no posts available';
        }

    }
    ?>
    </section>


    <!-- endregion advisorhub's Block -->
<?php } ?>

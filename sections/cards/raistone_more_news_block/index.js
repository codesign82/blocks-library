import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/raistone';

import Swiper, {Navigation, Pagination} from 'swiper'

Swiper.use([Pagination, Navigation]);
// import 'swiper/css';


/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.more_news_block');


  // add block code here
  let swiper = new Swiper(block.querySelector(".swiper-container"), {
    pagination: {
      el: block.querySelector('.swiper-pagination'),
      clickable: true,
    },
    observer: true,
    observeParents: true,
    slidesPerView: 1,
    spaceBetween: 20,
    breakpoints: {
      600: {
        slidesPerView: 2,
        spaceBetween: 48,
      },
      992: {
        slidesPerView: 3,
        spaceBetween: 50,
      },
    },
  });


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


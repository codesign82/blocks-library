<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'more_news_block';
$className = 'more_news_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/more_news_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="more-news-wrapper">
        <?php if ($title) { ?>
            <h2 class="headline-2 main-title  iv-st-from-bottom"><?= $title ?></h2>
        <?php } ?>
        <!-- swiper-->
        <?php
        $posts = get_field('posts');
        if ($posts): ?>
            <div class="swiper-container">
                <div class="swiper-wrapper cards ">
                    <?php foreach ($posts as $post):
                        // Setup this post for WP functions (variable must be named $post).
                        setup_postdata($post);
                        get_template_part("template-parts/new-version-of-post-card", '', array(
                            'post_id' => $post,
                        ));
                        ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php
            // Reset the global post object so that the rest of the page works correctly.
            wp_reset_postdata(); ?>
        <?php endif; ?>
    </div>
</div>
<!-- Pagination -->
<div class="swiper-pagination"></div>
</section>


<!-- endregion raistone's Block -->

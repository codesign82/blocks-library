<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'four_small_card_block';
$className = 'four_small_card_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/four_small_card_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$small_card = get_field('small_card');
?>
<!-- region advisorhub's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <?php if(have_rows('small_card')){ ?>
        <div class="row row-cols-1 row-cols-md-3 row-cols-lg-4">
            <?php while(have_rows('small_card')){
                the_row();
                $image = get_sub_field('image');
                $title = get_sub_field('title');
                $description = get_sub_field('description');
                $cta_button = get_sub_field('cta_button');
                ?>
                <div class="col">
                    <div class="small-card">
                        <?php if($image){ ?>
                            <picture class="icon">
                                <img src="<?=$image['url'] ?>" alt="<?=$image['alt'] ?>"/>
                            </picture>
                        <?php } ?>
                        <?php if($title){ ?>
                            <div class="headline-4 small-card-title"><?=$title ?></div>
                        <?php } ?>
                        <?php if($description){ ?>
                            <div class="paragraph-12 small-card-text">
                                <?=$description ?>
                            </div>
                        <?php } ?>
                        <?php if($cta_button){ ?>
                            <a href="<?=$cta_button['url']?>" target="<?=$cta_button['target'] ?>"
                               class="ah-btn"><?=$cta_button['title'] ?></a>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>
</section>


<!-- endregion advisorhub's Block -->

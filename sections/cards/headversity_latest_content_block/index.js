import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/propellerhealth';

import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";
import {stringToHTML} from "../../../scripts/functions/stringToHTML";
import {requestBodyGenerator} from "../../../scripts/functions/requestBodyGenerator";
import Swiper, {Controller} from "swiper";

gsap.registerPlugin([ScrollTrigger]);
/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.latest_content_block');

  // add block code here
  let linkItems = block.querySelectorAll('.link-item');
  const mobileMedia = window.matchMedia('(max-width: 600px)');
  const dropMenu = block.querySelector('#filter-content-wrapper');
  const filterList = block.querySelector('.filter-list');
  const contentWrapper = block.querySelector('.content-wrapper .row');
  let wrapperHeight = contentWrapper?.clientHeight;


  dropMenu?.addEventListener('click', function (e) {
    if (!mobileMedia.matches) return;
    const isOpened = dropMenu?.classList.toggle('active');
    if (!isOpened) {
      gsap.to(filterList, {height: 0});
    } else {
      gsap.to(filterList, {height: 'auto'});
    }
  });

  // save dynamic post id value
  const dynamicPostID = block.querySelector('.gform_wrapper input[value="dynamic_post_id"]');
  dynamicPostID && dynamicPostID.classList.add('dynamic_post_id');


  // region handling form modal

  const modalForm = block.querySelector(".modal-form");
  const exitForm = modalForm && modalForm.querySelector(".modal-form .exit");

  exitForm?.addEventListener("click", hideForm);
  modalForm?.addEventListener("click", hideForm);

  window.addEventListener('keydown', function (event) {
    if (event.key === 'Escape') {
      hideForm(event);
    }
  })

  function hideForm(e) {
    if (e.target.closest('.webinar-wrapper') && !e.target.closest('.exit')) return;
    modalForm.classList.remove("show")
    document.documentElement.classList.remove('modal-opened');
    video.pause()

  }

  //endregion handling form modal

  // region function to get youtube ID
  function getId(url) {
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    const match = url.match(regExp);

    return (match && match[2].length === 11)
        ? match[2]
        : null;
  }

  // endregion function to get youtube ID


  // region  handling webinar modal

  const webinarModal = block.querySelector(".webinar-modal");
  const webinarExit = block.querySelector(".exit");
  let webinarVideo = block.querySelector('.modal-video video');
  let webinarIframe = block.querySelector('.modal-video iframe');

  webinarExit?.addEventListener("click", hide);

  window.addEventListener('keydown', function (event) {
    if (event.key === 'Escape') {
      hide(event);
    }
  })

  function hide(e) {
    if (e.target.closest('.modal-content') && !e.target.closest('.exit')) return;
    webinarModal.classList.remove("show")
    document.documentElement.classList.remove('modal-opened');
    videoPlayerHandler('pause', webinarModal.dataset.videoType)
  }

  // endregion  handling webinar modal

  //region handle webinar posts
  let readMoreLink = block.querySelector('.modal .read-more-link');

  function videoPlayerHandler(state, videoType, videoSrc) {
    webinarModal.dataset.videoType = videoType;
    if (state === 'play') {
      if (videoType === 'youtube') {
        const videoId = getId(videoSrc);
        webinarIframe.src = 'https://www.youtube.com/embed/' + videoId + '?autoplay=1';
        webinarVideo?.classList.remove('modal-player-active');
        webinarIframe?.classList.add('modal-player-active');
      } else {
        webinarVideo.src = videoSrc;
        webinarIframe?.classList.remove('modal-player-active');
        webinarVideo?.classList.add('modal-player-active');
        webinarVideo?.play();
      }
    } else {
      webinarVideo?.classList.remove('modal-player-active');
      webinarIframe?.classList.remove('modal-player-active');
      if (videoType === 'youtube') {
        webinarIframe.src = '';
      } else {
        webinarVideo?.pause();
      }
    }
  }

  function handleWebinar(webinars) {
    for (let webinar of webinars) {
      if (webinar.classList.contains('webinar-card')) {
        const webinarType = webinar.dataset.webinarType;
        let videoPlayerIcon = webinar.querySelector('.open-video');
        videoPlayerIcon?.addEventListener('click', function (e) {
          e.preventDefault()
          const videoSrc = videoPlayerIcon.dataset.videoSrc;
          const videoType = videoPlayerIcon.dataset.videoType;
          readMoreLink.href = videoPlayerIcon.dataset.webinarUrl;
          webinarModal.classList.add("show")
          document.documentElement.classList.add('modal-opened');
          videoPlayerHandler('play', videoType, videoSrc)
        });
        if (webinarType === 'webinar-gated') {
          let openForm = webinar.querySelector(".open-form");
          openForm?.addEventListener("click", (e) => {
            e.preventDefault();
            modalForm.classList.add("show")
            document.documentElement.classList.add('modal-opened');
            const dynamicPostID = block.querySelector('.gform_wrapper .dynamic_post_id');
            dynamicPostID ? dynamicPostID.value = openForm.dataset.postId : null;
          });
        }
      }
    }
  }

  handleWebinar(block.querySelectorAll('.webinar-card'));

  //endregion handle webinar posts


  // region old code of podcast
  //region handle podcast posts

  // const minute = 60;
  //
  // function handlePodcast(podcasts) {
  //   for (let podcast of podcasts) {
  //     if (podcast.classList.contains('podcast-card')) {
  //       const playerIcon = podcast?.querySelector('.play-icon');
  //       const podcastElm = podcast?.querySelector('.podcast-audio');
  //       const durationText = podcast?.querySelector('.podcast-duration');
  //
  //       playerIcon?.addEventListener('click', function (e) {
  //         e.preventDefault();
  //         let currentActiveAudio = block.querySelector('.podcast-audio.podcast-playing');
  //         if (currentActiveAudio && currentActiveAudio.duration > 0 && !currentActiveAudio.paused) {
  //           currentActiveAudio?.pause();
  //           currentActiveAudio?.classList.remove('podcast-playing');
  //         }
  //         playerIcon?.classList.add('active');
  //         podcastElm?.classList.add('active');
  //         // podcastElm?.classList.remove('podcast-playing');
  //         const playPromise = podcastElm?.play();
  //         if (playPromise !== undefined) {
  //           playPromise.then(function () {
  //             podcastElm.classList.add('podcast-playing');
  //             // Automatic playback started!
  //           }).catch(function (error) {
  //             // Automatic playback failed.
  //             // Show a UI element to let the user manually start playback.
  //           });
  //         }
  //       });
  //
  //       // region get podcast duration
  //       let audio = document.createElement('audio');
  //
  //       // Define the URL of the MP3 audio file
  //
  //       audio.src = podcastElm.src;
  //
  //       // Once the metadata has been loaded, display the duration in the console
  //
  //       audio?.addEventListener('loadedmetadata', function () {
  //         // Obtain the duration in seconds of the audio file (with milliseconds as well, a float value)
  //         const duration = audio.duration;
  //
  //         if (duration < minute) {
  //           durationText.innerHTML = Math.round(duration) + ' Sec.';
  //         } else {
  //           durationText.innerHTML = Math.floor(duration / 60) + ' Min.';
  //         }
  //         audio.remove();
  //       }, false);
  //
  //       // endregion get podcast duration
  //     }
  //   }
  //
  //
  // }
  //
  // handlePodcast(block.querySelectorAll('.podcast-card'))

  //endregion handle podcast posts

  // endregion


  // region  handling uncharted modal

  const unchartedModal = block.querySelector(".uncharted-modal");
  const unchartedExit = unchartedModal && unchartedModal.querySelector(".exit");

  function hideUncharted(e) {
    if (e.target.closest('.modal-content') && !e.target.closest('.exit')) return;
    document.documentElement.classList.remove('modal-opened');
    unchartedModal?.classList.remove("show");
    const activeSlide = block.querySelector('.uncharted-modal .swiper-slide-active');
    const activeVideo = activeSlide?.querySelector('.video');
    if (activeVideo.dataset.videoType === 'youtube') {
      const videoId = getId(activeVideo.src);
      activeVideo.src = 'https://www.youtube.com/embed/' + videoId;
    } else {
      activeVideo?.pause();
    }
  }

  unchartedExit?.addEventListener("click", hideUncharted);

  window.addEventListener('keydown', function (event) {
    if (event.key === 'Escape') {
      hideUncharted(event);
    }
  })
  // endregion  handling uncharted modal

  //region handle uncharted posts

  // handle uncharted modal
  let galleryThumbs = '';
  if (block.classList.contains('has-uncharted')) {

    handleUncharted(block.querySelectorAll('.uncharted-card'))

    const allVideosLoaded = [];
    const unchartedThumbs = block.querySelectorAll('.uncharted-modal .bottom-section .swiper-slide');

    // region get video duration

    for (let unchartedThumb of unchartedThumbs) {
      const videoURL = unchartedThumb.dataset.videoUrl;
      const videoType = unchartedThumb.dataset.videoType;
      const thumbVideoDuration = unchartedThumb?.querySelector('.episode-time');
      if (videoURL && videoType !== 'youtube') {
        const videoLoaded = new Promise(resolve => {
          // const episodeTime = mediaCard?.querySelector('.episode-time');
          let dummyVideo = document.createElement('video');

          // Define the URL of the MP3 audio file
          dummyVideo.src = videoURL;

          // Once the metadata has been loaded, display the duration in the console

          dummyVideo?.addEventListener('loadedmetadata', function () {
            // Obtain the duration in seconds of the audio file (with milliseconds as well, a float value)
            const duration = dummyVideo.duration;
            if (duration < 60) {
              thumbVideoDuration.innerHTML = Math.round(duration) + ' Sec.';
            } else {
              thumbVideoDuration.innerHTML = Math.floor(duration / 60) + ' Min.';
            }
            dummyVideo.remove();
            resolve();
          }, false);
        })
        allVideosLoaded.push(videoLoaded)
      } else {
        thumbVideoDuration.style.opacity = 0;
      }
    }


    // endregion get video duration

    await Promise.all(allVideosLoaded);

    galleryThumbs = new Swiper(block.querySelector('.uncharted-modal .mySwiper'), {
      // initialSlide:1,
      spaceBetween: 10,
      slidesPerView: 2,
      // centeredSlides:true,
      slideToClickedSlide: true,
      loop: true,
      loopedSlides: 5, //looped slides should be the same
      modules: [Controller],
      breakpoints: {
        600: {
          spaceBetween: 10,
          slidesPerView: 3,
        },
        992: {
          slidesPerView: 3,
          spaceBetween: 112,
        }
      }
    });
    const galleryTop = new Swiper(block.querySelector('.uncharted-modal .mySwiper2'), {
      // initialSlide: 1,
      spaceBetween: 10,
      centeredSlides: true,
      loop: true,
      loopedSlides: 5, //looped slides should be the same
      modules: [Controller],
    });

    galleryThumbs.controller.control = galleryTop;
    galleryTop.controller.control = galleryThumbs;

  }

  function handleUncharted(uncharted) {
    for (let unchartedElm of uncharted) {
      if (unchartedElm.classList.contains('uncharted-card')) {
        const playVideo = unchartedElm?.querySelector(".play-icon");
        const mediaCard = unchartedElm?.querySelector('.media-card');
        const videoURL = mediaCard.dataset.videoUrl;
        const videoType = mediaCard.dataset.videoType;
        const videoDuration = mediaCard?.querySelector('.video-duration');
        if (videoURL && videoType !== 'youtube') {
          // const episodeTime = mediaCard?.querySelector('.episode-time');
          let dummyVideo = document.createElement('video');

          // Define the URL of the MP3 audio file
          dummyVideo.src = videoURL;

          // Once the metadata has been loaded, display the duration in the console

          dummyVideo?.addEventListener('loadedmetadata', function () {
            // Obtain the duration in seconds of the audio file (with milliseconds as well, a float value)
            const duration = dummyVideo.duration;

            if (duration < 60) {
              videoDuration.innerHTML = Math.round(duration) + ' Sec.';
            } else {
              videoDuration.innerHTML = Math.floor(duration / 60) + ' Min.';
            }
            dummyVideo.remove();

          }, false);
        } else {
          videoDuration.closest('.date')?.classList.add('no-date');
        }
        playVideo?.addEventListener("click", () => {
          unchartedModal?.classList.add("show");
          document.documentElement.classList.add('modal-opened');
          const slideIndex = +playVideo.dataset.slideNumber;
          typeof galleryThumbs === 'object' ? galleryThumbs?.slideToLoop(slideIndex, 0) : '';
          const activeSlide = block.querySelector('.uncharted-modal .swiper-slide-active');
          const activeVideo = activeSlide?.querySelector('.video');
          if (activeVideo.dataset.videoType === 'youtube') {
            const videoId = getId(activeVideo.src);
            activeVideo.src = 'https://www.youtube.com/embed/' + videoId + '?autoplay=1';
          } else {
            activeVideo?.play();
          }
          // galleryTop.slideToLoop(slideIndex,0);
        });
      }
    }
  }

  //endregion handle uncharted posts


  if (!block.classList.contains('manually')) {

    // region filter
    const categoryFilters = block.querySelectorAll('input[name="latest_filter"]');
    const postsContainer = block.querySelector('[posts-container]');
    const initialPostsString = postsContainer?.innerHTML;
    const initialHasMore = !loadMore?.classList.contains('hidden');
    const loadMore = block.querySelector('.load-more-wrapper');
    let loadMoreStatus = !loadMore.classList.contains('hidden');
    const categoryFilterChangeHandler = (event) => {
      contentWrapper.style.minHeight = wrapperHeight + 'px';
      for (let linkItem of linkItems) {
        for (let i = 0; i < linkItems.length; i++) {
          linkItems[i].classList.remove('active');
        }
      }
      event.target.parentElement?.classList.add('active');
      const tax_query = {};
      tax_query[0] = {
        taxonomy: event.target.dataset.taxonomy,
        terms: [String(event.target.value)],
        field: 'slug',
      }
      loadMore.dataset.args = JSON.stringify({
        post_type: ['post', 'podcast', 'uncharted', 'webinar'],
        post_status: 'publish',
        order: 'DESC',
        posts_per_page: 3,
        tax_query,
        paged: (!tax_query) ? 2 : 1
      });
      loadMore.dataset.template = event.target.dataset.postTemplate;
      postsContainer.innerHTML = '';

      if (event.target.value === 'featured') {
        loadMore.classList.toggle('hidden', !initialHasMore);
        const posts = stringToHTML(initialPostsString);
        for (let post of posts) {
          postsContainer.appendChild(post);
        }
        handleWebinar(posts);
        handlePodcast(posts);
        animations(posts);
        imageLazyLoading(posts);
        ScrollTrigger.refresh();
        return;
      }

      loadMore.click();
    };

    if (loadMore) {
      loadMore.addEventListener('click', async () => {
        if (loadMore.classList.contains('loading')) return;
        loadMore.classList.add('loading');
        loadMore.classList.remove('hidden');

        const response = await fetch(theme_ajax_object.ajax_url, {
          method: 'POST',
          body: requestBodyGenerator(loadMore),
        })
        loadMore.classList.remove('loading')
        const hasMorePages = !!response.headers.get('X-WP-Has-More-Pages');
        loadMore.classList.toggle('hidden', !hasMorePages);
        loadMoreStatus = hasMorePages;
        const htmlString = await response.json();
        const oldArgs = JSON.parse(loadMore.dataset.args);
        oldArgs.paged++;
        loadMore.dataset.args = JSON.stringify(oldArgs);

        const posts = stringToHTML(htmlString.data);
        const template = htmlString.data[1];
        for (let post of posts) {
          postsContainer.appendChild(post);
        }

        // handle posts type

        if (template === 'template-parts/webinar-card') {
          handleWebinar(posts);
        } else if (template === 'template-parts/podcast-card') {
          handlePodcast(posts);
        } else if (template === 'template-parts/uncharted-card') {
          handleUncharted(posts)
        }

        animations(posts);
        imageLazyLoading(posts);
        // reset wrapper height
        contentWrapper.style.minHeight = 'auto';
        ScrollTrigger.refresh();
      })
    }

    for (let categoryFilter of categoryFilters) {
      categoryFilter.addEventListener('input', categoryFilterChangeHandler);
    }

    // endregion filter

  } else {
    // region tabs

    let animating = false;
    let tabs = block.querySelectorAll('.tab-links');
    let tabContent = block.querySelectorAll('.tab-content');

    for (let i = 0; i < tabs.length; i++) {
      tabs[i].addEventListener('click', function (event) {
        if (animating || tabs[i].classList.contains('active')) return;

        animating = true;
        showTabsContent(i);
      });
    }

    function hideTabsContent(j) {
      for (let i = 0; i < tabContent.length; i++) {
        if (tabContent[i].classList.contains('active')) {
          tabs[i].classList?.remove('active');
          return gsap.timeline({
            paused: true,
            onComplete: () => animating = false,
          })
              .call(() => {
                tabContent[i].classList?.remove('active')
                tabContent[j].classList?.add('active')
              })
        }
      }
    }

    async function showTabsContent(i) {
      tabs[i].classList?.add('active');
      hideTabsContent(i)
          .fromTo(tabContent[i], {
            X: 100,
            opacity: 0,
          }, {
            y: 0,
            opacity: 1,
            stagger: 0.15,
          }, '<25%')
          .play();
    }


    // endregion
  }


  // region  handling podcast modal
  const podcasteModel = block.querySelector('#podcaste-model');
  const exit = podcasteModel.querySelector(".exit");
  const modalVideo = podcasteModel.querySelector('.modal-video');

  exit?.addEventListener("click", closeModel);
  podcasteModel?.addEventListener("click", closeModel);


  function closeModel(e) {
    if (e.target.closest('.modal-content') && !e.target.closest('.exit')) return;
    document.documentElement.classList.remove('modal-opened');
    podcasteModel.classList.remove("show");
    document.documentElement.classList.remove('modal-opened');
    const podcastAudio = modalVideo.querySelector('.audio');
    podcastAudio?.pause();
    modalVideo.innerHTML = '';
  }

  window.addEventListener('keydown', function (event) {
    if (event.key === 'Escape') {
      closeModel(event);
      const podcastAudio = modalVideo.querySelector('.audio');
      document.documentElement.classList.remove('modal-opened');
      podcastAudio?.pause();
      modalVideo.innerHTML = '';
    }
  })

  // endregion  handling podcast modal


  function handlePodcast(cards) {
    const playerIcon = block.querySelectorAll('.podcast-icon');
    for (let i = 0; i < cards.length; i++) {
      playerIcon[i]?.addEventListener('click', function (e) {
        podcasteModel.classList.add('show');
        document.documentElement.classList.add('modal-opened');
        const parent = playerIcon[i].closest('.podcast-card');
        const temp = parent.querySelector('template');
        const clone = temp.content.cloneNode(true);
        modalVideo.append(clone);
        const podcastAudio = modalVideo.querySelector('.audio');
        podcastAudio?.play();
        const iframe = podcasteModel.querySelector("iframe");
        const videoId = getId(iframe.src);
        iframe.src = 'https://www.youtube.com/embed/' + videoId + '?autoplay=1';

      });
    }
  }


  handlePodcast(block.querySelectorAll('.podcast-card'))


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


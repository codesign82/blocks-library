<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$categories = get_field('multiple_taxonomies_selector');
$select_your_posts = get_field('select_your_posts');
$has_uncharted = '';
$uncharted_terms = '';
$how_to_show_posts_g = '';
$uncharted_manually_posts = '';
if ($select_your_posts === 'automatically') {
    $has_uncharted = !empty($categories) && strpos(implode($categories), 'uncharted_category') ? 'has-uncharted' : '';
} else {
    $posts = get_field('tab_and_posts');
    if (is_array($posts) && !empty($posts)):
        foreach ($posts as $po) {
            if ($po['select_post_type'] === 'uncharted') {
                $uncharted_terms = $po['uncharted_category'];
                $how_to_show_posts_g = $po['how_to_show_posts'];
                $uncharted_manually_posts = $po['uncharted'];
                $has_uncharted = 'has-uncharted';
            }
        }
    endif;
}

$dataClass = 'latest_content_block';
$className = 'latest_content_block';
$className .= " $has_uncharted";
$className .= " $select_your_posts";
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/latest_content_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$link = get_field('link');
$posts = get_field('posts');
$uncharted_current_category = '';

//region webinar form modal fields
$webinar_form_in_resources = get_field('webinar_form_in_resources', 'options');
$form_logo = @$webinar_form_in_resources['logo'];
$form_title = @$webinar_form_in_resources['title'];
$form_description = @$webinar_form_in_resources['description'];
$form = @$webinar_form_in_resources['form'];


//endregion webinar form modal fields
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="content-wrapper">
        <?php if ($title) { ?>
            <h2 move-to-here
                class="headline-3 title text-center iv-st-from-bottom"><?= $title ?></h2>
        <?php } ?>
        <!--   automatically select   -->
        <?php if ($select_your_posts === 'automatically') { ?>
            <div class="filter-content-wrapper iv-st-from-bottom"
                 id="filter-content-wrapper">
                <div
                        class="selector"><?= __('Filter by content type', 'headversity') ?></div>
                <div class="icon-wrapper">
                    <svg role="img" class="down-arrow" width="16" height="9"
                         viewBox="0 0 16 9" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path
                                d="M7.29289 8.70711C7.68342 9.09763 8.31658 9.09763 8.70711 8.70711L15.0711 2.34315C15.4616 1.95262 15.4616 1.31946 15.0711 0.928932C14.6805 0.538407 14.0474 0.538407 13.6569 0.928932L8 6.58579L2.34315 0.928933C1.95262 0.538408 1.31946 0.538408 0.928932 0.928933C0.538407 1.31946 0.538407 1.95262 0.928932 2.34315L7.29289 8.70711ZM7 7L7 8L9 8L9 7L7 7Z"
                                fill="#0A1B57"/>
                    </svg>
                </div>
                <?php if (!empty($categories)) {
                    ?>
                    <ul class="filter-list">
                        <li class="link-item active featured" tabindex="0">
                            <input type="radio" name="latest_filter" value="featured">
                            <?= __('Featured', 'headversity') ?>
                        </li>
                        <?php foreach ($categories as $cat) {
                            $cat_exp = explode('&', $cat);
                            $cat_post_type = $cat_exp[1];
                            $cat = get_term($cat_exp[0]);
                            if ($cat_post_type === 'podcast') {
                                $template = 'template-parts/podcast-card';
                            } else if ($cat_post_type === 'webinar') {
                                $template = 'template-parts/webinar-card';
                            } else if ($cat_post_type === 'uncharted') {
                                $uncharted_terms = $cat;
                                $template = 'template-parts/uncharted-card';
                            } else {
                                $template = 'template-parts/post-card';
                            }
                            ?>
                            <li class="link-item">
                                <input type="radio" name="latest_filter"
                                       data-post-template="<?= $template ?>"
                                       data-taxonomy="<?= $cat->taxonomy ?>"
                                       value="<?= $cat->slug ?>">
                                <?= $cat->name ?>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </div>
            <?php
            $the_query_1 = new WP_Query([
                'post_type' => 'podcast',
                'posts_per_page' => 1,
                'post_status' => 'publish',
                'order' => 'DESC',
            ]);
            $the_query_2 = new WP_Query([
                'post_type' => 'post',
                'posts_per_page' => 1,
                'post_status' => 'publish',
                'order' => 'DESC',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'category',
                        'field' => 'slug',
                        'terms' => 'blog',
                    ),
                ),
            ]);
            $the_query_3 = new WP_Query([
                'post_type' => 'webinar',
                'posts_per_page' => 1,
                'post_status' => 'publish',
                'order' => 'DESC',
            ]);
            $the_query = new WP_Query();
            $the_query->posts = array_merge($the_query_1->posts, $the_query_2->posts, $the_query_3->posts);
            $the_query->post_count = $the_query_1->post_count + $the_query_2->post_count + $the_query_3->post_count;
            $have_posts = $the_query->have_posts();
            ?>

            <div
                    class="row row-cols-1 row-cols-md-2 row-cols-lg-3 justify-content-lg-center center-col"
                    posts-container>


                <?php if ($have_posts) { ?>
                    <?php while ($the_query->have_posts()) {
                        $the_query->the_post();
                        $post_type = get_post_type(get_the_ID());
                        if ($post_type === 'podcast') {
                            get_template_part("template-parts/podcast-card", '', array('post_id' => get_the_ID()));
                        } elseif ($post_type === 'webinar') {
                            get_template_part("template-parts/webinar-card", '', array('post_id' => get_the_ID()));
                        } else {
                            get_template_part("template-parts/post-card", '', array('post_id' => get_the_ID()));
                        }
                        ?>
                    <?php } ?>
                    <?php wp_reset_postdata(); ?>
                <?php } ?>
            </div>

            <div class="load-more-wrapper" data-template="template-parts/post-card">
                <div class="loader">
                    <div class="loader-ball"></div>
                    <div class="loader-ball"></div>
                    <div class="loader-ball"></div>
                </div>
            </div>

        <?php } else { ?>

            <!--   manually select   -->

            <div class="filter-content-wrapper " id="filter-content-wrapper">
                <div
                        class="selector"><?= __('Filter by content type', 'headversity') ?></div>
                <div class="icon-wrapper">
                    <svg role="img" class="down-arrow" width="16" height="9"
                         viewBox="0 0 16 9" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path
                                d="M7.29289 8.70711C7.68342 9.09763 8.31658 9.09763 8.70711 8.70711L15.0711 2.34315C15.4616 1.95262 15.4616 1.31946 15.0711 0.928932C14.6805 0.538407 14.0474 0.538407 13.6569 0.928932L8 6.58579L2.34315 0.928933C1.95262 0.538408 1.31946 0.538408 0.928932 0.928933C0.538407 1.31946 0.538407 1.95262 0.928932 2.34315L7.29289 8.70711ZM7 7L7 8L9 8L9 7L7 7Z"
                                fill="#0A1B57"/>
                    </svg>
                </div>
                <?php if (have_rows('tab_and_posts')) { ?>
                    <ul class="filter-list">
                        <?php
                        $isFirst = true;
                        while (have_rows('tab_and_posts')) {
                            the_row();
                            $tab_name = get_sub_field('tab_name');
                            $is_featured = get_sub_field('is_featured');
                            $is_featured = $is_featured ? 'featured' : '';
                            ?>
                            <li
                                    class="link-item tab-links <?= $is_featured ?> <?= $isFirst ? 'active' : '' ?>"><?= $tab_name ?></li>
                            <?php $isFirst = false;
                        } ?>
                    </ul>
                <?php } ?>
            </div>

            <div class="tabs-parent">
                <div class="tab-content-wrapper" posts-container>
                    <?php if (have_rows('tab_and_posts')) { ?>
                        <?php
                        $isFirst = true;
                        while (have_rows('tab_and_posts')) {
                            the_row();
                            $how_to_show_posts = get_sub_field('how_to_show_posts');
                            $post_automatically = get_sub_field('post_automatically');
                            $select_post_type = get_sub_field('select_post_type');
                            $selected_terms = '';
                            $selected_tax = '';
                            $post_type = '';

                            if ($select_post_type === 'podcast') {
                                $template = 'template-parts/podcast-card';
                                $selected_terms = get_sub_field('podcasts_category');
                                $selected_tax = 'podcast_category';
                                $post_type = get_sub_field('podcast');
                            } elseif ($select_post_type === 'webinar') {
                                $template = 'template-parts/webinar-card';
                                $selected_terms = get_sub_field('webinar_category');
                                $selected_tax = 'webinar_category';
                                $post_type = get_sub_field('webinar');
                            } elseif ($select_post_type === 'uncharted') {
                                $template = 'template-parts/uncharted-card';
                                $selected_terms = get_sub_field('uncharted_category');
                                $selected_tax = 'uncharted_category';
                                $post_type = get_sub_field('uncharted');

                            } else {
                                $template = 'template-parts/post-card';
                                $selected_terms = get_sub_field('post_category');
                                $selected_tax = 'category';
                                $post_type = get_sub_field('post');
                            }
                            ?>

                            <!--   automatically select -->
                            <?php if ($how_to_show_posts === 'automatically') { ?>
                                <div class="tab-content <?= $isFirst ? 'active' : '' ?>">
                                    <?php
                                    $args = array(
                                        'post_type' => $select_post_type,
                                        'posts_per_page' => 3,
                                        'order' => 'DESC',
                                        'post_status' => 'publish',
                                        'tax_query' => $selected_terms ? array(
                                            array(
                                                'taxonomy' => $selected_tax,
                                                'field' => 'term_id',
                                                'terms' => $selected_terms,
                                            )
                                        ) : array(),
                                    );
                                    // The Query
                                    $the_query = new WP_Query($args);
                                    $have_posts = $the_query->have_posts();
                                    // The Loop
                                    if ($have_posts) {
                                        $post_index = 0;
                                        ?>
                                        <div
                                                class="row row-cols-1 row-cols-md-2 row-cols-lg-3 justify-content-lg-center center-col">
                                            <?php while ($the_query->have_posts()) {
                                                $the_query->the_post();
                                                get_template_part($template, '', array('post_id' => get_the_ID(),
                                                    'post_index' => $post_index
                                                ));
                                                ?>

                                                <?php $post_index++;
                                            } ?>
                                        </div>
                                    <?php }
                                    /* Restore original Post Data */
                                    wp_reset_postdata(); ?>
                                    <?php $args['paged'] = 2;
                                    $args['post__not_in'] = null;
                                    ?>
                                </div>
                            <?php } else {
                                $cards = $post_type;
                                if ($cards):
                                    $post_index = 0;
                                    ?>
                                    <div class="tab-content <?= $isFirst ? 'active' : '' ?>">
                                        <div
                                                class="row row-cols-1 row-cols-md-2 row-cols-lg-3 justify-content-lg-center center-col ">
                                            <?php foreach ($cards as $post): ?>
                                                <?php setup_postdata($post);
                                                get_template_part($template, '', array('post_id' => $post,
                                                    'post_index' => $post_index
                                                ));
                                                ?>
                                                <?php
                                                $post_index++;
                                            endforeach; ?>
                                        </div>
                                    </div>
                                    <?php wp_reset_postdata(); ?>
                                <?php endif;
                            } ?>
                            <?php $isFirst = false;
                        } ?>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>


        <?php if ($link) { ?>
            <a class="link iv-st-from-bottom" href="<?= $link['url'] ?>"
               target="<?= $link['target'] ?>"><?= $link['title'] ?>
                <svg class="arrow" viewBox="0 0 39.5 22.1" width="28" height="15"
                     role="application">
                    <path class="arrow-head"
                          d="M29.1,12.1a1.7,1.7,0,0,0,0-2.1L19.5.4a1.7,1.7,0,0,0-2.1,0,1.6,1.6,0,0,0,0,2.2L25.9,11l-8.5,8.5a1.6,1.6,0,0,0,0,2.2,1.7,1.7,0,0,0,2.1,0Z"
                          fill="#f90"></path>
                    <path class="arrow-line" d="M0,12.5H28v-3H0Z" fill="#f90"></path>
                </svg>
            </a>
        <?php } ?>
    </div>
</div>

<!--  region model of webinar-->
<div class="modal webinar-modal" aria-modal="true" role="dialog">
    <div class="modal-content">
        <div class="modal-video aspect-ratio">
            <video
                    data-src="" type="video/mp4" playsinline controls
                    class="video modal-player">
            </video>
            <iframe class="the-video modal-player" src="" title="YouTube video player"
                    width="600" height="500"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
        </div>
    </div>
    <a href="#"
       class="btn read-more-link"><?= __('Read More', 'headversity') ?></a>
    <svg class="exit" width="39" height="39" viewBox="0 0 39 39" fill="none"
         role="application"
         xmlns="http://www.w3.org/2000/svg">
        <line x1="9.90039" y1="28.2841" x2="28.2852" y2="9.8993"
              stroke="#FF9900" stroke-width="3" stroke-linecap="round"/>
        <path d="M28.9922 28.2841L10.6074 9.89928" stroke="#FF9900"
              stroke-width="3" stroke-linecap="round"/>
    </svg>
</div>
<!--endregion-->
<!--  region model of form-->
<div class="modal-form " aria-modal="true" role="dialog">
    <div class="container">
        <div class="webinar-wrapper">
            <svg class="exit" width="39" height="39" viewBox="0 0 39 39"
                 role="application"
                 fill="none"
                 xmlns="http://www.w3.org/2000/svg">
                <line x1="9.90039" y1="28.2841" x2="28.2852" y2="9.8993"
                      stroke="#FF9900" stroke-width="3" stroke-linecap="round"/>
                <path d="M28.9922 28.2841L10.6074 9.89928" stroke="#FF9900"
                      stroke-width="3" stroke-linecap="round"/>
            </svg>
            <?php if ($form_logo) { ?>
                <picture class="form-img">
                    <img data-src="<?= @$form_logo['url'] ?>"
                         alt="<?= @$form_logo['alt'] ?>">
                </picture>
            <?php }
            if ($form_title) { ?>
                <div class="form-title">
                    <?= $form_title ?>
                </div>
            <?php }
            if ($form_description) { ?>
                <div
                        class=" paragraph m-paragraph form-description ">
                    <?= $form_description ?>
                </div>
            <?php } ?>
            <?= $form ? do_shortcode('[gravityform id="' . $form . '" ajax="true" title="false"  description="false"]') : null; ?>
        </div>
    </div>
</div>
<!-- endregion-->
<!--region modal for uncharted-->
<?php if ($has_uncharted != ''):
    $uncharted_args = array(
        'post_type' => 'uncharted',
        'posts_per_page' => 3,
        'order' => 'DESC',
        'post_status' => 'publish',
        'tax_query' => $categories || $uncharted_terms ? array(
            array(
                'taxonomy' => 'uncharted_category',
                'field' => 'term_id',
                'terms' => $uncharted_terms
            )

        ) : array(),
    );
    if ($uncharted_manually_posts && $how_to_show_posts_g !== 'automatically') {
        $uncharted_args['post__in'] = $uncharted_manually_posts;
    }
    ?>
    <div class="modal uncharted-modal" aria-modal="true" role="dialog">
        <div class="modal-content">
            <div class="swiper mySwiper2">
                <div class="swiper-wrapper">
                    <?php // The Query
                    $the_query = new WP_Query($uncharted_args);
                    // The Loop
                    if ($the_query->have_posts()) { ?>
                        <?php while ($the_query->have_posts()) {
                            $the_query->the_post();
                            $thumb_id = get_the_ID();
                            $episode_title = get_field('episode_title', $thumb_id);
                            $episode_number = get_field('episode_number', $thumb_id);
                            $video_type = get_field('video_type', $thumb_id);
                            $video_url = get_field('video_url', $thumb_id);
                            $video_file = get_field('video_file', $thumb_id);
                            $youtube = get_field('youtube', $thumb_id);
                            $final_video = $video_type === 'url' ? $video_url : $video_file;
                            // DO Not Delete Video Type For JS
                            $video_type_final = $video_type === 'youtube' ? 'youtube' : 'video';
                            ?>
                            <div class="swiper-slide modal-video aspect-ratio"
                                 aria-modal="true" role="dialog">
                                <?php if ($video_type !== 'youtube') { ?>
                                    <video poster="<?php thumbnail_url($thumb_id); ?>"
                                           data-src="<?= $final_video ?>"
                                           type="video/mp4" playsinline controls
                                           class="video"
                                           data-video-type="<?= $video_type_final ?>">
                                    </video>
                                <?php } else {
                                    ?>
                                    <iframe class="video"
                                            data-video-type="<?= $video_type_final ?>" width="560"
                                            height="315"
                                            src="<?= generateVideoEmbedUrl($youtube) ?>"
                                            title="YouTube video player"
                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen></iframe>
                                    <?php
                                } ?>
                            </div>
                        <?php } ?>
                    <?php }
                    wp_reset_postdata(); ?>
                </div>
            </div>
            <div class="bottom-section">
                <div class="episodes-section-title headline-5">
                    <?= __('Next episodes:', 'headversity') ?>
                </div>
                <div class="swiper mySwiper">
                    <div class="swiper-wrapper episodes-wrapper">
                        <?php // The Query
                        $the_query = new WP_Query($uncharted_args);
                        // The Loop
                        if ($the_query->have_posts()) { ?>
                            <?php while ($the_query->have_posts()) {
                                $the_query->the_post();
                                $alt = get_post_meta(get_the_ID(), '_wp_attachment_image_alt', true);
                                $episode_title = get_field('episode_title', get_the_ID());
                                $episode_number = get_field('episode_number', get_the_ID());
                                $episode_number_final = $episode_number ?: get_post_meta(get_the_ID(), 'custom_episode_number', true);
                                $video_type = get_field('video_type', get_the_ID());
                                $video_url = get_field('video_url', get_the_ID());
                                $video_file = get_field('video_file', get_the_ID());
                                $youtube = get_field('youtube', get_the_ID());
                                $final_video = $video_type === 'url' ? $video_url : $video_file;
                                if ($video_type === 'youtube') {
                                    $final_video = $youtube;
                                }
                                $video_type_final = $video_type === 'youtube' ? 'youtube' : 'video';
                                // DO Not Delete Video Type For JS
                                ?>
                                <div class="swiper-slide episode episode-thumb"
                                     data-video-url="<?= $final_video ?>"
                                     data-video-type="<?= $video_type_final ?>">
                                    <picture class="poster-img">
                                        <img data-src="<?php thumbnail_url(get_the_ID()); ?>"
                                             alt="<?= $alt ?: 'Image not found' ?>">
                                    </picture>
                                    <div class="episode-desc">
                                        <div
                                                class="episode-title"><?= $episode_title . ' ' . $episode_number_final ?></div>
                                        <div class="episode-time">00:00</div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php }
                        wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
            <svg class="exit" width="39" height="39" viewBox="0 0 39 39" fill="none"
                 role="application"
                 xmlns="http://www.w3.org/2000/svg">
                <line x1="9.90039" y1="28.2841" x2="28.2852" y2="9.8993"
                      stroke="#FF9900" stroke-width="3" stroke-linecap="round"/>
                <path d="M28.9922 28.2841L10.6074 9.89928" stroke="#FF9900"
                      stroke-width="3" stroke-linecap="round"/>
            </svg>
        </div>
    </div>
<?php endif; ?>
<!--endregion modal for uncharted-->
<!-- region modal of podcaste-->
<div class="podcaste-model" id="podcaste-model" aria-modal="true" role="dialog">
    <div class="modal-content" aria-modal="true" role="dialog">
        <div class="modal-video podcaste-content aspect-ratio">

        </div>
    </div>
    <svg class="exit" width="39" height="39" viewBox="0 0 39 39" fill="none"
         xmlns="http://www.w3.org/2000/svg" role="application">
        <line x1="9.90039" y1="28.2841" x2="28.2852" y2="9.8993"
              stroke="#FF9900" stroke-width="3" stroke-linecap="round"/>
        <path d="M28.9922 28.2841L10.6074 9.89928" stroke="#FF9900"
              stroke-width="3" stroke-linecap="round"/>
    </svg>
</div>

<!-- endregion -->
</section>


<!-- endregion headversity's Block -->

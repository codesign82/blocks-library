<?php
$post_id = @$args['post_id'] ?: get_the_ID();
$thumbnail_id = get_post_thumbnail_id();
$alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);
$episode_title = get_field('episode_title', $post_id);
$episode_number = get_field('episode_number', $post_id);
$recording_type = get_field('recording_type', $post_id);
$recording_file = get_field('recording_file', $post_id);
$youtube_video = get_field('youtube_video', $post_id);
$embed = get_field('embed', $post_id);
$terms = get_the_terms($post_id, 'podcast_category');
$episode_number_final = $episode_number ?: get_post_meta($post_id, 'custom_episode_number', true);

?>


<div id="post-id-<?= $post_id; ?>" class="col  iv-st-from-bottom podcast-card">
  <template class="template">
    <?php if ($recording_type === 'file') { ?>
      <audio class="audio" controls>
        <source src="<?= $recording_file ?>">
      </audio>
    <?php } elseif ($recording_type === 'youtube') {
      ?>
      <iframe class="video" width="560" height="315" src="<?= generateVideoEmbedUrl($youtube_video) ?>" title="YouTube video player" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <?php } else { ?>
      <?= $embed ?>
    <?php } ?>
  </template>
  <div class="card media-card">
    <div class="card-image aspect-ratio">
      <svg aria-label="<?= __('open audio', 'headversity') ?>" class="play-icon podcast-icon" width="76" height="76" viewBox="0 0 76 76" fill="none"
           xmlns="http://www.w3.org/2000/svg">
        <circle cx="38" cy="38" r="38" fill="white"/>
        <path d="M55.8691 34.7311L26.9434 51.4314V18.0308L55.8691 34.7311Z"
              fill="#FF9900"/>
        <path d="M30.1562 22.924L56.082 37.8923L30.1562 52.8605V22.924Z"
              stroke="#0A1B57" stroke-width="2"/>
      </svg>
      <img src="<?php thumbnail_url($post_id); ?>" alt="<?= $alt ?: 'Image not found' ?>">
    </div>
    <?php if (!empty($terms) && is_array($terms)) { ?>
      <div class="category-title paragraph color-transition">
        <?php foreach ($terms as $term) { ?>
          <a class="category-link" href="<?= get_term_link($term->slug, 'podcast_category') ?>"
             aria-label="<?= __('go to podcast category', 'headversity') ?> "
          >
            <?= $term->name ?>
          </a>
        <?php } ?>
      </div>
    <?php } ?>
    <a href="<?= get_the_permalink($post_id) ?>" class="card-title headline-5 display-block"
       aria-label="<?= __('go to post page', 'headversity') ?>">
      <?= get_the_title($post_id); ?>
    </a>
    <?php if ($episode_title || $episode_number) { ?>
      <div class="date paragraph">
        <?= $episode_title . ' ' . $episode_number_final ?> |
        <span class="podcast-duration">00:00</span>
      </div>
    <?php } ?>
  </div>
</div>


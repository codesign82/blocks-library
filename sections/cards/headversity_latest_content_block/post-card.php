<?php
$post_id = @$args['post_id'] ?: get_the_ID();
$post_type = @$args['post_type'];
$thumbnail_id = get_post_thumbnail_id();
$alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);
$category_detail = get_the_category($post_id);
$media_information = get_field('media_information', $post_id);
$show_arrow_icon = get_field('show_arrow_icon', $post_id);
$cover_image = get_field('cover_image', $post_id);

?>
<div class="col iv-st-from-bottom post-card">
  <div id="post-id-<?= $post_id; ?>" class="card">
    <a href="<?= get_the_permalink($post_id); ?>" class="card-image aspect-ratio"
       aria-label="<?= __('go to post page', 'headversity') ?>">
      <?= get_the_post_thumbnail($post_id,'medium'); ?>
    </a>
    <div class="category-title paragraph color-transition">
      <?php
      foreach ($category_detail as $category_name) { ?>
        <a class="category-link" href="<?= get_category_link($category_name) ?>"
           aria-label="<?= __('go to category', 'headversity') ?> "
        > <?= $category_name->cat_name; ?></a>
      <?php } ?>
    </div>
    <a href="<?= get_the_permalink($post_id); ?>" class="card-title headline-5"
       aria-label="<?= __('go to post page', 'headversity') ?>">
      <?= get_the_title($post_id) ?>
    </a>
    <a href="<?= get_the_permalink($post_id); ?>" class="arrow-icon" aria-label="<?= __('go to post page', 'headversity') ?>">
      <svg class="arrow" viewBox="0 0 29.5 22.1" width="28" height="15" role="application">
        <path class="arrow-head" d="M29.1,12.1a1.7,1.7,0,0,0,0-2.1L19.5.4a1.7,1.7,0,0,0-2.1,0,1.6,1.6,0,0,0,0,2.2L25.9,11l-8.5,8.5a1.6,1.6,0,0,0,0,2.2,1.7,1.7,0,0,0,2.1,0Z" fill="#f90"></path>
        <path class="arrow-line" d="M0,12.5H28v-3H0Z" fill="#f90"></path>
      </svg>
    </a>
  </div>
</div>

<?php
$post_id = @$args['post_id'] ?: get_the_ID();
$thumbnail_id = get_post_thumbnail_id();
$alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);
$terms = get_the_terms($post_id, 'webinar_category');
$is_gated = get_field('is_gated', $post_id);
$date_and_time = get_field('date_and_time', $post_id);
$date_format = date('F jS', strtotime($date_and_time));
$type_of_video = get_field('video_type', $post_id);
$video_file = get_field('video_file', $post_id);
$video_url = get_field('video_url', $post_id);
$video_youtube = get_field('youtube', $post_id);
$video_link = '';

if ($type_of_video === 'file') {
  $video_link = $video_file;
} elseif ($type_of_video === 'url') {
  $video_link = $video_url;
} elseif ($type_of_video === 'youtube') {
  $video_link = $video_youtube;
}

// DO Not Delete Video Type For JS
$video_type_final = $type_of_video === 'youtube' ? 'youtube' : 'video';
?>
<div class="col iv-st-from-bottom webinar-card" id="post-id-<?= $post_id; ?>" data-webinar-type="<?=$is_gated ? 'webinar-gated' : 'webinar-upcoming' ?>">
  <div class="card media-card">
    <?php if (!$is_gated) { ?>
      <a href="<?= get_the_permalink($post_id); ?>" class="card-image aspect-ratio" >
        <img src="<?php thumbnail_url($post_id); ?>" alt="<?= $alt ?: 'Image not found' ?>">
        <button class="open-video" aria-label="<?= __('Open Video', 'headversity') ?>" data-video-src="<?= $video_link ?>" data-video-type="<?=$video_type_final ?>"
                data-webinar-url="<?= get_the_permalink($post_id) ?>">
          <svg class="play-icon" width="76" height="76" viewBox="0 0 76 76" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="38" cy="38" r="38" fill="white"/>
            <path d="M55.8691 34.7311L26.9434 51.4314V18.0308L55.8691 34.7311Z" fill="#FF9900"/>
            <path d="M30.1562 22.924L56.082 37.8923L30.1562 52.8605V22.924Z" stroke="#0A1B57" stroke-width="2"/>
          </svg>
        </button>
      </a>
    <?php } else {
      ?>
      <picture class="card-image aspect-ratio">
        <img src="<?php thumbnail_url($post_id); ?>" alt="<?= $alt ?: 'Image not found' ?>">
      </picture>
      <?php
    } ?>
    <?php if (!empty($terms) && is_array($terms)) { ?>
      <div class="category-title paragraph color-transition">
        <?php foreach ($terms as $term) { ?>
          <a class="category-link" href="<?= get_term_link($term->slug, 'webinar_category') ?>">
            <?= $term->name ?>
          </a>
        <?php } ?>
      </div>
    <?php } ?>
    <?php if (!$is_gated) { ?>
      <a href="<?= get_the_permalink($post_id); ?>" class="card-title headline-5">
        <?= get_the_title($post_id) ?>
      </a>
    <?php } else {
      ?>
      <h3 class="card-title headline-5">
        <?= get_the_title($post_id) ?>
      </h3>
      <?php
    } ?>
    <span class="date"><?= $date_format ?></span>
    <?php if ($is_gated) { ?>
      <button aria-label="<?= __('Open Sign up Form') ?>" class="arrow-icon open-form" data-post-id="<?= $post_id ?>">
        <svg class="arrow" viewBox="0 0 29.5 22.1" width="28" height="15" role="application">
          <path class="arrow-head" role="application" d="M29.1,12.1a1.7,1.7,0,0,0,0-2.1L19.5.4a1.7,1.7,0,0,0-2.1,0,1.6,1.6,0,0,0,0,2.2L25.9,11l-8.5,8.5a1.6,1.6,0,0,0,0,2.2,1.7,1.7,0,0,0,2.1,0Z" fill="#f90"></path>
          <path class="arrow-line" d="M0,12.5H28v-3H0Z" fill="#f90"></path>
        </svg>
      </button>
    <?php } else {
      ?>
      <a href="<?= get_the_permalink($post_id); ?>" class="arrow-icon">
        <svg class="arrow" viewBox="0 0 29.5 22.1" width="28" height="15" role="application">
          <path class="arrow-head" role="application" d="M29.1,12.1a1.7,1.7,0,0,0,0-2.1L19.5.4a1.7,1.7,0,0,0-2.1,0,1.6,1.6,0,0,0,0,2.2L25.9,11l-8.5,8.5a1.6,1.6,0,0,0,0,2.2,1.7,1.7,0,0,0,2.1,0Z" fill="#f90"></path>
          <path class="arrow-line" d="M0,12.5H28v-3H0Z" fill="#f90"></path>
        </svg>
      </a>
      <?php
    } ?>
  </div>
</div>


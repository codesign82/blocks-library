<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'work_project_block';
$className = 'work_project_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/work_project_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title       = get_field( 'title' );
$description = get_field( 'description' );
$projects    = get_field( 'projects' );
$link        = get_field( 'link' );
?>
<!-- region virtual_world's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <div class="section-line"></div>
  <?php if ( $projects ): ?>
    <div class="swiper-parent">
      <div class="swiper iv-st-from-bottom">
        <div class="swiper-wrapper">
          <?php $project_index = 1;
          foreach ( $projects as $post ):
            // Setup this post for WP functions (variable must be named $post).
            $thumbnail_id = get_post_thumbnail_id( $post );
            $alt               = get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true );
            $hack_query        = 9999;
            $video_type        = get_field( 'video_type', $post );
            $video_url         = get_field( 'video_url', $post );
            if ( $video_type === 'file' ) {
              $video_url = get_field( 'video_file', $post );
            }
            setup_postdata( $post ); ?>
            <div class="swiper-slide">
              <a class="card display-block <?= $video_url ? 'card-has-video' : '' ?>" href="<?= site_url() ?>/our-work/?projectID=<?= $post + $hack_query ?>">
                <div class="card-content">
                  <?php if ( has_excerpt( $post ) ) { ?>
                    <div class="paragraph"><?= get_the_excerpt( $post ) ?></div>
                  <?php } ?>
                  <h2 class="headline-2"><?= get_the_title( $post ) ?></h2>
                  <?php if ( $video_url ) { ?>
                    <video class="project-video" src="<?= $video_url ?>" muted playsinline poster="<?php thumbnail_url( $post ); ?>"></video>
                  <?php } else { ?>
                    <div class="media-wrapper">
                      <div class="project-image">
                        <img src="<?php thumbnail_url( $post ); ?>" alt="<?= $alt ?>">
                      </div>
                    </div>
                  <?php } ?>
                </div>
              </a>
            </div>
            <?php $project_index ++; endforeach; ?>
        </div>
      </div>
      <!-- Add Pagination -->
      <div class="swiper-paginations iv-st-from-bottom"></div>
    </div>
    <?php
    // Reset the global post object so that the rest of the page works correctly.
    wp_reset_postdata(); ?>
  <?php endif; ?>
  <?php if ( $link ) { ?>
    <a href="<?= $link['url'] ?>" class="see-all-text iv-st-from-bottom" target="<?= $link['target'] ?>"><?= $link['title'] ?></a>
  <?php } ?>
</div>
</section>


<!-- endregion virtual_world's Block -->

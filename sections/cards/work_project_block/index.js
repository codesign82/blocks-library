import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/cyberhill';
import {ScrollTrigger} from "gsap/ScrollTrigger";
import Swiper, {Navigation, Pagination} from 'swiper';

Swiper.use([Pagination, Navigation]);
gsap.registerPlugin(ScrollTrigger);
const blockScript = async (container = document) => {
  const block = container.querySelector('.work_project_block');
  
  const swiper = new Swiper('.swiper', {
    slidesPerView: 1.3,
    spaceBetween: 10,
    breakpoints: {
      600: {
        spaceBetween: 30,
        slidesPerView: 2.3,
      },
      991: {
        spaceBetween: 73,
        slidesPerView: 2.35,
      }
    },
    pagination: {
      el: block.querySelector('.swiper-paginations'),
      clickable: true,
    },
    
    
    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });
  
  const sectionLine = block.querySelector(".section-line")
  
  const whenLeave = gsap.timeline({
    scrollTrigger: {
      invalidateOnRefresh: true,
      trigger: block,
      scrub: .3,
      start: "50% bottom",
      end: 'center center',
    }
  })
      .fromTo(sectionLine, {
        y: 300
      }, {
        y: 0
      });
  
  // region card video on hover show and play video
  const videos = block.querySelectorAll('.card-has-video');
  if (videos.length > 0) {
    for (let video of videos) {
      const videoElm = video.querySelector('.project-video');
      video.addEventListener('mouseenter', () => {
        videoElm.play();
      })
      video.addEventListener('mouseleave', () => {
        videoElm.pause();
      })
    }
  }
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


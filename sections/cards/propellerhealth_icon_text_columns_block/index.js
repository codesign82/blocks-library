import './index.html';
import './style.scss';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/propellerhealth';

const blockScript = async (container = document) => {
  const block = container.querySelector('.icon_text_columns_block');


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


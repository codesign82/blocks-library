import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import Swiper from "swiper";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/wrap';

const blockScript = async (container = document) => {
  const block = container.querySelector('.ways_to_get_bolawrap');
  
  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);




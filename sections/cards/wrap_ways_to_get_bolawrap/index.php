<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'ways_to_get_bolawrap';
$className = 'ways_to_get_bolawrap';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/ways_to_get_bolawrap/screenshot.png" >';

  return;
endif;


/****************************
 *     Custom ACF Meta      *
 ****************************/
$title            = get_field( 'title' );
$height           = get_field( 'height' );
$background_color = get_field( 'background_color' );
?>
<style>
  <?='#'.$id?>:after {
  <?=$background_color ? 'background-color: '.$background_color.' !important;':''?><?=$height ? 'height'.$height.'%!important;':''?>
  }
</style>
<!-- region WRAP's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <?php if ( $title ) { ?>
    <h5 class="headline-6 iv-st-from-bottom"><?= $title ?></h5>
  <?php } ?>
  <div class="row">
    <?php
    if ( have_rows( 'cards' ) ) {
      ?>
      <?php
      $cards_count = count( get_field( 'cards' ) );
      while ( have_rows( 'cards' ) ) {
        the_row();
        $image          = get_sub_field( 'image' );
        $is_small_title = get_sub_field( 'is_small_title' );
        $is_small_title = $is_small_title ? 'small-title' : '';
        $title          = get_sub_field( 'title' );
        $description    = get_sub_field( 'description' );
        $cta_button     = get_sub_field( 'cta_button' );
        ?>
        <div class="<?php if ( $cards_count === 1 ) {
          echo 'col-12';
        } else if ( $cards_count === 2 ) {
          echo 'col-12 col-md-6';
        } else {
          echo 'col-12 col-md-6 col-lg-4';
        } ?>">
          <div class="card-container iv-st-from-bottom <?= $is_small_title ?>">
            <?php if ( $image ) { ?>
              <picture class="icon">
                <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
              </picture>
            <?php } ?>
            <?php if ( $title ) { ?>
              <h3 class="title"><?= $title ?></h3>
            <?php } ?>
            <?php if ( $description ) { ?>
              <div class="paragraph paragraph-14"><?= $description ?></div>
            <?php } ?>
            <?php if ( $cta_button ) { ?>
              <a href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>" class="btn btn-border"><?= $cta_button['title'] ?></a>
            <?php } ?>
          </div>
        </div>
        <?php
      } ?>
      <?php
    }
    ?>
  </div>
</div>
</section>
<!-- endregion WRAP's Block -->

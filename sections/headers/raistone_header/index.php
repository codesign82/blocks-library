<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="description" content="<?php if (is_single()) {
        single_post_title('', true);
    } else {
        bloginfo('name');
        echo " - ";
        bloginfo('description');
    } ?>"/>
    <meta
            content="width=device-width, initial-scale=1.0, maximum-scale=5, minimum-scale=1.0"
            name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <script>
        const BiggerThanDesignWidth = 2100;
        const designWidth = 1920;
        const desktop = 1920;
        const tablet = 992;
        const sTablet = 768;
        const mobile = 600;
        const sMobile = 375;

        function fixContainer() {
            const resizeHandler = function () {
                if (window.innerWidth >= BiggerThanDesignWidth) {
                    document.documentElement.style.fontSize = `${10}px`;
                    // document.documentElement.style.fontSize = `${9 * window.innerWidth / desktop}px`;
                } else if (window.innerWidth < BiggerThanDesignWidth && window.innerWidth >= desktop) {
                    document.documentElement.style.fontSize = `${10 * window.innerWidth / desktop}px`;
                } else if (window.innerWidth < desktop && window.innerWidth >= tablet) {
                    document.documentElement.style.fontSize = `${10 * window.innerWidth / designWidth}px`;
                } else if (window.innerWidth < tablet && window.innerWidth >= sTablet) {
                    document.documentElement.style.fontSize = `${10}px`;
                } else if (window.innerWidth < tablet && window.innerWidth >= mobile) {
                    document.documentElement.style.fontSize = `${10 * window.innerWidth / tablet}px`;
                } else if (window.innerWidth < mobile && window.innerWidth >= sMobile) {
                    document.documentElement.style.fontSize = `${10}px`;
                } else {
                    document.documentElement.style.fontSize = `${10 * window.innerWidth / sMobile}px`;
                }
            };
            resizeHandler();
            window.addEventListener('resize', resizeHandler);
        }

        fixContainer();
    </script>
    <style>
        /* latin */
        @font-face {
            font-family: 'Montserrat';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/montserrat/v23/JTUSjIg1_i6t8kCHKm459Wlhyw.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin */
        @font-face {
            font-family: 'Montserrat';
            font-style: normal;
            font-weight: 600;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/montserrat/v23/JTUSjIg1_i6t8kCHKm459Wlhyw.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin */
        @font-face {
            font-family: 'Montserrat';
            font-style: normal;
            font-weight: 700;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/montserrat/v23/JTUSjIg1_i6t8kCHKm459Wlhyw.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }


        /* latin */
        @font-face {
            font-family: 'Montserrat';
            font-style: normal;
            font-weight: 500;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/montserrat/v23/JTUHjIg1_i6t8kCHKm4532VJOt5-QNFgpCtZ6Hw5aXo.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }


        /* latin */
        @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/lato/v22/S6uyw4BMUTPHjx4wXg.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin */
        @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 700;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/lato/v22/S6u9w4BMUTPHh6UVSwiPGQ.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
    </style>
    <?php wp_head(); ?>
    <?php
    $code_before_body_tag = get_field('code_before_body_tag', 'options');
    $code_in_head_tag = get_field('code_in_head_tag', 'options');
    $code_before_body_tag_after_head_tag = get_field('code_before_body_tag_after_head_tag', 'options');
    $code_after_body_tag = get_field('code_after_body_tag', 'options');
    echo $code_before_body_tag;
    ?>
</head>
<!--preloader style-->
<style>
    body:not(.loaded) {
        opacity: 0;
    }

    body:not(.loaded) * {
        transition: none !important;
    }

    body {
        transition: opacity .5s;
    }

    [modal-content] {
        display: none !important;
    }

    /*   region preloader */
    .lds-roller {
        display: none;
        position: relative;
        width: 8rem;
        height: 8rem;
        margin: 0 auto;
    }

    .lds-roller div {
        animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
        transform-origin: 40px 40px;
    }

    .lds-roller div:after {
        content: " ";
        display: block;
        position: absolute;
        width: 7px;
        height: 7px;
        border-radius: 50%;
        background: #fff;
        margin: -4px 0 0 -4px;
    }

    .lds-roller div:nth-child(1) {
        animation-delay: -0.036s;
    }

    .lds-roller div:nth-child(1):after {
        top: 63px;
        left: 63px;
    }

    .lds-roller div:nth-child(2) {
        animation-delay: -0.072s;
    }

    .lds-roller div:nth-child(2):after {
        top: 68px;
        left: 56px;
    }

    .lds-roller div:nth-child(3) {
        animation-delay: -0.108s;
    }

    .lds-roller div:nth-child(3):after {
        top: 71px;
        left: 48px;
    }

    .lds-roller div:nth-child(4) {
        animation-delay: -0.144s;
    }

    .lds-roller div:nth-child(4):after {
        top: 72px;
        left: 40px;
    }

    .lds-roller div:nth-child(5) {
        animation-delay: -0.18s;
    }

    .lds-roller div:nth-child(5):after {
        top: 71px;
        left: 32px;
    }

    .lds-roller div:nth-child(6) {
        animation-delay: -0.216s;
    }

    .lds-roller div:nth-child(6):after {
        top: 68px;
        left: 24px;
    }

    .lds-roller div:nth-child(7) {
        animation-delay: -0.252s;
    }

    .lds-roller div:nth-child(7):after {
        top: 63px;
        left: 17px;
    }

    .lds-roller div:nth-child(8) {
        animation-delay: -0.288s;
    }

    .lds-roller div:nth-child(8):after {
        top: 56px;
        left: 12px;
    }

    @keyframes lds-roller {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }

    /* endregion preloader */
</style>
<!--end preloader style-->
<!-- ACF Fields -->
<?php

$header_is_dark = get_field('header_is_dark');
$header_is_dark = $header_is_dark ? 'dark-theme' : '';
$container_width = get_field('container_width', 'options');
$header_logo_dark_theme = get_field('header_logo_dark_theme', 'options');
$svg_or_image_dark_theme = @$header_logo_dark_theme['svg_or_image'];
$logo_svg_dark_theme = @$header_logo_dark_theme['svg'];
$logo_image_dark_theme = @$header_logo_dark_theme['image'];


$header_logo_white_theme = get_field('header_logo_white_theme', 'options');
$svg_or_image_white_theme = @$header_logo_white_theme['svg_or_image'];
$logo_svg_white_theme = @$header_logo_white_theme['svg'];
$logo_image_white_theme = @$header_logo_white_theme['image'];

$login = get_field('login_link', 'options');
$cta_button = get_field('cta_button', 'options');

// products number
?>
<!-- END ACF -->
<body <?php body_class(); ?> data-barba="wrapper">
<?php
$code_after_body_tag = get_field('code_after_body_tag', 'options');
?>
<header class="<?= $header_is_dark ?>">
    <div class="header-wrapper">
        <div class="left-wrapper">
            <!--          burger menu and cross-->
            <div class="burger-and-menu">
                <div class="menu">
                    <button aria-label="burger" class="burger-menu" id="burger-menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </div>
            </div>
            <!--     logo-->
            <?php if ($logo_svg_dark_theme || $logo_image_dark_theme) { ?>
                <a href="<?= site_url() . '/' ?>" class="main-logo dark-logo">
                    <?php if (!$svg_or_image_dark_theme) { ?>
                        <picture>
                            <img src="<?= $logo_image_dark_theme['url'] ?>" alt="<?= $logo_image_dark_theme['alt'] ?>">
                        </picture>
                    <?php } else {
                        echo $logo_svg_dark_theme;
                    } ?>
                </a>
            <?php } ?>
            <?php if ($logo_svg_white_theme || $logo_image_white_theme) { ?>
                <a href="<?= site_url() . '/' ?>" class="main-logo white-logo">
                    <?php if (!$svg_or_image_white_theme) { ?>
                        <picture>
                            <img src="<?= $logo_image_white_theme['url'] ?>"
                                 alt="<?= $logo_image_white_theme['alt'] ?>">
                        </picture>
                    <?php } else {
                        echo $logo_svg_white_theme;
                    } ?>
                </a>
            <?php } ?>

            <!--     links  -->
            <nav class="navbar">
                <div class="small-logo-and-login">
                    <div class="small-logo">
                        <picture class="small-logo-dark">
                            <img src="<?= get_template_directory_uri() . '/front-end/src/images/small-logo-dark-theme.png' ?>"
                                 alt="">
                        </picture>
                        <picture class="small-logo-white">
                            <img src="<?= get_template_directory_uri() . '/front-end/src/images/small-logo-white-theme.png' ?>"
                                 alt="">
                        </picture>
                    </div>
                    <?php if ($login) { ?>
                        <a href="<?= $login['url'] ?>" target="<?= $login['target'] ?>" class="login">
                            <?= $login['title'] ?></a>
                    <?php } ?>
                </div>
                <div class="navbar-wrapper">
                    <?php if (have_rows('menu_links', 'options')) { ?>
                        <ul class="primary-menu">
                            <?php while (have_rows('menu_links', 'options')) {
                                the_row();
                                $menu_link = get_sub_field('menu_link');
                                $is_has_sub_menu = get_sub_field('is_has_sub_menu');
                                ?>
                                <?php if ($menu_link) { ?>
                                    <li class="menu-item  <?= ($is_has_sub_menu) ? 'menu-item-has-children' : '' ?>">
                                        <a class="header-link" href="<?= $menu_link['url'] ?>"
                                           target="<?= $menu_link['target'] ?>">
                                            <?= $menu_link['title'] ?></a>
                                        <?php if ($is_has_sub_menu && have_rows('submenu', 'options')) { ?>
                                            <div class="arrow">
                                                <svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 448 512">
                                                    <path fill="#d1d1d1"
                                                          d="M441.9 167.3l-19.8-19.8c-4.7-4.7-12.3-4.7-17 0L224 328.2 42.9 147.5c-4.7-4.7-12.3-4.7-17 0L6.1 167.3c-4.7 4.7-4.7 12.3 0 17l209.4 209.4c4.7 4.7 12.3 4.7 17 0l209.4-209.4c4.7-4.7 4.7-12.3 0-17z"
                                                          class=""></path>
                                                </svg>
                                            </div>
                                            <ul class="sub-menu">
                                                <?php while (have_rows('submenu', 'options')) {
                                                    the_row();
                                                    $submenu_link = get_sub_field('submenu_link');
                                                    ?>
                                                    <?php if ($submenu_link) { ?>
                                                        <li class="menu-item-in-sub-menu">
                                                            <a class="header-sublink" href="<?= $submenu_link['url'] ?>"
                                                               target="<?= $submenu_link['target'] ?>">
                                                                <?= $submenu_link['title'] ?>
                                                            </a>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            </ul>
                                        <?php } ?>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                    <?php if ($cta_button) { ?>
                        <a href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>"
                           class="btn btn-in-mobile">
                            <?= $cta_button['title'] ?></a>
                    <?php } ?>
                </div>
            </nav>
        </div>
        <div class="right-wrapper">
            <?php if ($login) { ?>
                <a href="<?= $login['url'] ?>" target="<?= $login['target'] ?>" class="login">
                    <?= $login['title'] ?></a>
            <?php } ?>
            <?php if ($cta_button) { ?>
                <a href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>" class="btn btn-desktop">
                    <?= $cta_button['title'] ?></a>
            <?php } ?>
        </div>
    </div>
</header>

import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/raistone';


import {gsap} from "gsap";

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const header = container.querySelector('header');

  header.classList.add('sticky');
  document.documentElement.style.setProperty('--header-sticky', header?.getBoundingClientRect()?.height - 2 ?? 0);
// window.headerSticky=78
//   header.dataset.headerSticky=78;
  header.classList.remove('sticky');

  const burgerMenu = header.querySelector('.burger-menu');
  const menuLinks = header.querySelector('.navbar');

  if (!burgerMenu) return;
  const burgerTl = gsap.timeline({paused: true});
  const burgerSpans = burgerMenu.querySelectorAll('span');
  // gsap.set(burgerSpans, {transformOrigin: 'center'});
  // burgerTl
  //   .to(burgerSpans, {
  //     y: gsap.utils.wrap([`0.6rem`, 0, `-0.6rem`]),
  //     duration: 0.25,
  //   })
  //   .to(header.querySelector(".burger-menu span:first-child"), {
  //     marginBottom: -1,
  //     duration: .4,
  //     ease: "power3.inOut"
  //   }, "<")
  //   .set(burgerSpans, {autoAlpha: gsap.utils.wrap([1, 0, 1])})
  //   .to(burgerSpans, {rotation: gsap.utils.wrap([45, 0, -45])})
  //   .set(burgerSpans, {rotation: gsap.utils.wrap([45, 0, 135])});
  burgerMenu.addEventListener('click', function () {
    console.log("dffdddfdf")
    if (burgerMenu.classList.contains('burger-menu-active')) {
      // region allow page scroll
      document.documentElement.classList.remove('modal-opened');
      // endregion allow page scroll
      burgerMenu.classList.remove('burger-menu-active');
      menuLinks.classList.remove('header-links-active');
      header.classList.remove('header-active');
      // burgerTl.timeScale(2).reverse();
    } else {
      burgerMenu.classList.add('burger-menu-active');
      menuLinks.classList.add('header-links-active');
      header.classList.add('header-active');
      // burgerTl.timeScale(1).play();
      // region prevent page scroll
      document.documentElement.classList.add('modal-opened');
      // endregion prevent page scroll
      gsap.fromTo(menuLinks.querySelectorAll('.menu-item'), {
        y: 30,
        autoAlpha: 0,
      }, {
        y: 0,
        autoAlpha: 1,
        stagger: .05,
        duration: .2,
        delay: .2,
      });
    }
  });

  // region open sub menu in responsive
  const menuItems = header.querySelectorAll('.menu-item-has-children');
  const mobileMedia = window.matchMedia('(max-width: 992px)');
  menuItems.forEach((menuItem) => {
    const menuItemBody = menuItem.querySelector('.sub-menu');
    menuItem?.addEventListener('click', (e) => {

      if (!mobileMedia.matches || !menuItemBody || e.target.classList.contains('header-link') || e.target.closest('.sub-menu,.menu-item-in-sub-menu a')) return;
      const isOpened = menuItem?.classList.toggle('menu-item-active');
      if (!isOpened) {
        gsap.to(menuItemBody, {height: 0});
      } else {
        gsap.to(Array.from(menuItems).map(otherMenuItem => {
          const otherMenuItemBody = otherMenuItem.querySelector('.sub-menu');
          if (otherMenuItemBody && menuItem !== otherMenuItem) {
            otherMenuItem?.classList.remove('menu-item-active');
            gsap.set(otherMenuItem, {zIndex: 1});

          }
          return otherMenuItemBody;
        }), {height: 0});
        gsap.set(menuItem, {zIndex: 2});
        gsap.to(menuItemBody, {height: 'auto'});
      }
    });
  });

  // endregion open sub menu in responsive


  header.classList.toggle('sticky', window.scrollY >= 20);
  window.addEventListener('scroll', function () {
    header.classList.toggle('sticky', window.scrollY >= 20);
  });


  const logos = header.querySelectorAll(".main-logo svg")
  for (let logo of logos) {
    const logoShapes = logo.querySelectorAll(".logo-shapes path")
    gsap.timeline({
      repeat: -1,
      delay: 1,
      repeatDelay: 30
    })
        .from(logoShapes[0], {
          fill: "#FBBF49",
          opacity: 0,
          xPercent: -50,
        })
        .from(logoShapes[1], {
          fill: "#FBBF49",
          opacity: 0,
          xPercent: 30,
          yPercent: -50,
        }, "<50%")
        .from(logoShapes[2], {
          fill: "#FBBF49",
          opacity: 0,
          xPercent: 50,
        }, "<50%")
        .from(logoShapes[3], {
          fill: "#FBBF49",
          opacity: 0,
          xPercent: 20,
          yPercent: 50,
        }, "<50%")

  }


  animations(header);
  imageLazyLoading(header);
};
windowOnLoad(blockScript);


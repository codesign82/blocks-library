<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="description" content="<?php if (is_single()) {
        single_post_title('', true);
    } else {
        bloginfo('name');
        echo ' - ';
        bloginfo('description');
    } ?>"/>
    <meta
            content="width=device-width, initial-scale=1.0, maximum-scale=5, minimum-scale=1.0"
            name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <script>
        const BiggerThanDesignWidth = 1920;
        const designWidth = 1440;
        const desktop = 1440;
        const tablet = 992;
        const mobile = 600;
        const sMobile = 390;

        function fixContainer() {
            const resizeHandler = function () {
                if (window.innerWidth >= BiggerThanDesignWidth) {
                    document.documentElement.style.fontSize = `${10}px`;
                    // document.documentElement.style.fontSize = `${9 * window.innerWidth / desktop}px`;
                } else if (window.innerWidth < BiggerThanDesignWidth && window.innerWidth >= desktop) {
                    document.documentElement.style.fontSize = `${10 * window.innerWidth / desktop}px`;
                } else if (window.innerWidth < desktop && window.innerWidth >= tablet) {
                    document.documentElement.style.fontSize = `${10 * window.innerWidth / designWidth}px`;
                } else if (window.innerWidth < tablet && window.innerWidth >= mobile) {
                    document.documentElement.style.fontSize = `${10 * window.innerWidth / tablet}px`;
                } else if (window.innerWidth < mobile && window.innerWidth >= sMobile) {
                    document.documentElement.style.fontSize = `${10}px`;
                } else {
                    document.documentElement.style.fontSize = `${10 * window.innerWidth / sMobile}px`;
                }
            };
            resizeHandler();
            window.addEventListener('resize', resizeHandler);
        }

        fixContainer()
    </script>


    <style>
        /* latin */
        @font-face {
            font-family: 'Montserrat';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/montserrat/v23/JTUSjIg1_i6t8kCHKm459Wlhyw.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
    </style>
    <!-- Third party code ACF-->
    <?php
    $code_in_head_tag = get_field('code_in_head_tag', 'options');
    $code_before_body_tag_after_head_tag = get_field('code_before_body_tag_after_head_tag', 'options');
    $code_after_body_tag = get_field('code_after_body_tag', 'options');
    ?>
    <?php wp_head(); ?>
    <?= $code_in_head_tag ?>
</head>
<?= $code_before_body_tag_after_head_tag ?>
<!--preloader style-->
<style>
    body:not(.loaded) {
        opacity: 0;
    }

    body:not(.loaded) * {
        transition: none !important;
    }

    body {
        transition: opacity .5s;
    }

    [modal-content] {
        display: none !important;
    }

    /*   region preloader */

    /* endregion preloader */
</style>
<!--end preloader style-->
<!-- ACF Fields -->
<?php
$header_logo = get_field('header_logo', 'options');
$svg_or_image = @$header_logo['svg_or_image'];
$svg = @$header_logo['svg'];
$image = @$header_logo['image'];
$is_header_dark = get_field('is_header_dark',);
$header_links = get_field('header_links', 'options');
$cta_btn = get_field('cta_btn', 'options');
?>
<!-- END ACF -->
<body <?php body_class(); ?> data-barba="wrapper">
<?= $code_after_body_tag ?>
<header class="fixed">
    <div class="header-wrapper">
        <div class='overlay'></div>
        <!--     logo-->
        <a href="<?= site_url() ?>" class='main-logo'
           aria-label="Samrt Dv Logo ">
            <?php if ($svg_or_image) { ?>
                <?php if ($image) { ?>
                    <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                <?php } ?>
            <?php } else {
                echo $svg;
            } ?>
        </a>
        <div class="middle-wrapper">

            <!--     links  -->
            <nav class="navbar">
                <div class="navbar-wrapper">
                    <?php if (have_rows('header_links', 'options')) { ?>
                        <ul class="primary-menu">
                            <?php while (have_rows('header_links', 'options')) {
                                the_row();
                                $header_link = get_sub_field('header_link');
                                $has_menu = get_sub_field('has_menu');
                                ?>
                                <?php if ($header_link) { ?>
                                    <li
                                            class="menu-item <?= $has_menu ? 'menu-item-has-children  menu-item-has-arrow ' : ' ' ?> ">
                                        <a href="<?= $header_link['url'] ?>" class="header-link">
                <span class="menu-item-text">
                  <?= $header_link['title'] ?>
                </span>
                                        </a>


                                        <?php if ($has_menu) { ?>
                                            <div class='svg-wrapper'>
                                                <svg class='navigation-triangle' width='35'
                                                     height='21' viewBox='0 0 35 21'
                                                     fill='none' xmlns='http://www.w3.org/2000/svg'>
                                                    <path d='M17 0.5L34.5 21H0L17 0.5Z' fill='#D9D9D9'/>
                                                </svg>
                                            </div>
                                            <svg class='arrow open-mega-menu' width='37' height='37'
                                                 viewBox='0 0 37 37' fill='none'
                                                 xmlns='http://www.w3.org/2000/svg'>
                                                <circle cx='18.5' cy='18.5' r='18'
                                                        transform='rotate(-180 18.5 18.5)'
                                                        fill='transparent' stroke='#BFD730'/>
                                                <path
                                                        d='M25.8885 19.1141C26.0837 18.9188 26.0837 18.6022 25.8885 18.4069L22.7065 15.225C22.5112 15.0297 22.1946 15.0297 21.9994 15.225C21.8041 15.4202 21.8041 15.7368 21.9994 15.9321L24.8278 18.7605L21.9994 21.5889C21.8041 21.7842 21.8041 22.1008 21.9994 22.296C22.1946 22.4913 22.5112 22.4913 22.7065 22.296L25.8885 19.1141ZM25.5349 18.2605H10.9434V19.2605H25.5349V18.2605Z'
                                                        fill='#313545'/>
                                            </svg>
                                            <div class="mega-menu">
                                                <div class="mega-menu-wrapper">
                                                    <svg class="back-arrow arrow back-link" width="37"
                                                         height="37" viewBox="0 0 37 37" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <circle cx="18.5" cy="18.5" r="18"
                                                                fill='transparent' stroke="#BFD730"/>
                                                        <path
                                                                d="M11.1115 17.8859C10.9163 18.0812 10.9163 18.3978 11.1115 18.5931L14.2935 21.775C14.4888 21.9703 14.8054 21.9703 15.0006 21.775C15.1959 21.5798 15.1959 21.2632 15.0006 21.0679L12.1722 18.2395L15.0006 15.4111C15.1959 15.2158 15.1959 14.8992 15.0006 14.704C14.8054 14.5087 14.4888 14.5087 14.2935 14.704L11.1115 17.8859ZM11.4651 18.7395H26.0566V17.7395H11.4651V18.7395Z"
                                                                fill="#313545"/>
                                                    </svg>

                                                    <?php if (have_rows('left_menu', 'options')) { ?>
                                                        <div class="mega-menu-left">
                                                            <div class="border-title">

                                                                <div
                                                                        class="title"><?= $header_link['title'] ?></div>
                                                                <div class="border"></div>
                                                            </div>
                                                            <?php while (have_rows('left_menu', 'options')) {
                                                                the_row();
                                                                $icon = get_sub_field('icon');
                                                                $mega_title = get_sub_field('mega_title');
                                                                $mega_menu_links = get_sub_field('mega_menu_links');
                                                                ?>
                                                                <div class="desc">
                                                                    <?php if ($icon) { ?>
                                                                        <?= $icon ?>
                                                                    <?php } ?>
                                                                    <?php if ($mega_title) { ?>
                                                                        <div
                                                                                class="menu-title"><?= $mega_title ?></div>
                                                                    <?php } ?>
                                                                    <?php if (have_rows('mega_menu_links', 'options')) { ?>
                                                                        <ul class="menu-items-in-mega-menu">
                                                                            <?php while (have_rows('mega_menu_links', 'options')) {
                                                                                the_row();
                                                                                $mega_link = get_sub_field('mega_link');
                                                                                ?>
                                                                                <?php if ($mega_link) { ?>
                                                                                    <li class="menu-item-in-mega-menu ">
                                                                                        <a class="header-sublink"
                                                                                           href="<?= $mega_link['url'] ?>"><?= $mega_link['title'] ?></a>
                                                                                    </li>
                                                                                <?php } ?>
                                                                            <?php } ?>

                                                                        </ul>
                                                                    <?php } ?>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if (have_rows('right_menu', 'options')) { ?>
                                                        <div class="mega-menu-right">
                                                            <?php while (have_rows('right_menu', 'options')) {
                                                                the_row();
                                                                $icon = get_sub_field('icon');
                                                                $mega_title = get_sub_field('mega_title');
                                                                $mega_menu_links = get_sub_field('mega_menu_links');
                                                                ?>
                                                                <div class="desc">
                                                                    <?php if ($icon) { ?>
                                                                        <?= $icon ?>
                                                                    <?php } ?>
                                                                    <?php if ($mega_title) { ?>
                                                                        <div
                                                                                class="menu-title"><?= $mega_title ?></div>
                                                                    <?php } ?>
                                                                    <?php if (have_rows('mega_menu_links', 'options')) { ?>
                                                                        <ul class="menu-items-in-mega-menu">
                                                                            <?php while (have_rows('mega_menu_links', 'options')) {
                                                                                the_row();
                                                                                $mega_link = get_sub_field('mega_link');
                                                                                ?>
                                                                                <?php if ($mega_link) { ?>
                                                                                    <li class="menu-item-in-mega-menu ">
                                                                                        <a class="header-sublink"
                                                                                           href="<?= $mega_link['url'] ?>"><?= $mega_link['title'] ?></a>
                                                                                    </li>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        </ul>
                                                                    <?php } ?>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>

                                                </div>
                                            </div>
                                        <?php } ?>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    <?php } ?>


                    <a href="<?= $cta_btn['url'] ?>"
                       class="cta-button transparent-bg-cta-button contact-us">
                        <span class="cta-title"><?= $cta_btn['title'] ?></span>

                    </a>
                    <form class='search-mob' action="/" method="get">
                        <label aria-hidden="true" style="display:none">search</label>
                        <input type='text' placeholder='Search...' name="s" class='search-input-mob'/>
                        <button class='search-btn-mob' aria-label="Search" title="Search"
                                type='submit'>
                            <svg class="search-icon-mob" width='29' height='30'
                                 viewBox='0 0 29 30' fill='none'
                                 xmlns='http://www.w3.org/2000/svg'>
                                <circle cx='16.7967' cy='11.2336' r='7'
                                        transform='rotate(38.1791 16.7967 11.2336)'
                                        stroke='#313545'
                                        stroke-width='2'/>
                                <line x1='13.2559' y1='17.3544' x2='7.07471' y2='25.2153'
                                      stroke='#313545' stroke-width='2'/>
                            </svg>
                        </button>
                    </form>


                </div>
            </nav>
        </div>


        <div class="right-wrapper">
            <form class="search" action="/" method="get">
                <label aria-hidden="true" style="display:none" for="s"><?= __("Search", "smart-dv") ?></label>
                <input type="text" placeholder="Search..." name='s'
                       class="search-input"/>

                <button class="search-btn" type="button" aria-label="Search">
                    <svg class="search-icon-dt" width="29" height="30" viewBox="0 0 29 30"
                         fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <circle cx="16.7967" cy="11.2336" r="7"
                                transform="rotate(38.1791 16.7967 11.2336)" stroke="#313545"
                                stroke-width="2"/>
                        <line x1="13.2559" y1="17.3544" x2="7.07471" y2="25.2153"
                              stroke="#313545" stroke-width="2"/>
                    </svg>
                </button>
            </form>
            <button class="cta-button transparent-bg-cta-button menu-btn">
        <span class="cta-title">
          Menu</span>

            </button>
            <?php if ($cta_btn) { ?>
                <a href="<?= $cta_btn['url'] ?>"
                   class="cta-button transparent-bg-cta-button contact-us">
                    <?= $cta_btn['title'] ?></a>
            <?php } ?>
            <button class="cta-button transparent-bg-cta-button close">
        <span class="cta-title">
          Close</span>
            </button>

        </div>
    </div>
</header>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="description" content="<?php if ( is_single() ) {
    single_post_title( '', true );
  } else {
    bloginfo( 'name' );
    echo " - ";
    bloginfo( 'description' );
  } ?>"/>
  <meta
    content="width=device-width, initial-scale=1.0, maximum-scale=5, minimum-scale=1.0"
    name="viewport">
  <meta content="ie=edge" http-equiv="X-UA-Compatible">
  <script>
    const BiggerThanDesignWidth = 1920;
    const designWidth = 1440;
    const desktop = 1440;
    const tablet = 992;
    const mobile = 600;
    const sMobile = 375;

    function fixContainer() {
      const resizeHandler = function () {
        if (window.innerWidth >= BiggerThanDesignWidth) {
          document.documentElement.style.fontSize = `${10}px`;
          // document.documentElement.style.fontSize = `${9 * window.innerWidth / desktop}px`;
        } else if (window.innerWidth < BiggerThanDesignWidth && window.innerWidth >= desktop) {
          document.documentElement.style.fontSize = `${10 * window.innerWidth / desktop}px`;
        } else if (window.innerWidth < desktop && window.innerWidth >= tablet) {
          document.documentElement.style.fontSize = `${10 * window.innerWidth / designWidth}px`;
        } else if (window.innerWidth < tablet && window.innerWidth >= mobile) {
          document.documentElement.style.fontSize = `${10 * window.innerWidth / tablet}px`;
        } else if (window.innerWidth < mobile && window.innerWidth >= sMobile) {
          document.documentElement.style.fontSize = `${10}px`;
        } else {
          document.documentElement.style.fontSize = `${10 * window.innerWidth / sMobile}px`;
        }
      };
      resizeHandler();
      window.addEventListener('resize', resizeHandler);
    }
  </script>
  <style>
    /* latin */
    @font-face {
      font-family: 'Product Sans';
      src: url(<?=get_template_directory_uri() ?>/fonts/product-sans-bold.ttf) format('truetype');
      font-weight: 700;
      font-style: normal;
      font-display: swap;
    }

    @font-face {
      font-family: 'Product Sans';
      src: url(<?=get_template_directory_uri() ?>/fonts/product-sans-regular.ttf) format('truetype');
      font-weight: 400;
      font-style: normal;
      font-display: swap;
    }

    @font-face {
      font-family: 'Product Sans Light';
      src: url(<?=get_template_directory_uri() ?>/fonts/product-sans-light.ttf) format('truetype');
      font-weight: 400;
      font-style: normal;
      font-display: swap;
    }

    /* latin */
    @font-face {
      font-family: 'Roboto';
      font-style: normal;
      font-weight: 400;
      font-display: swap;
      src: url(https://fonts.gstatic.com/s/roboto/v29/KFOmCnqEu92Fr1Mu4mxK.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    /* latin */
    @font-face {
      font-family: 'Roboto';
      font-style: normal;
      font-weight: 300;
      font-display: swap;
      src: url(https://fonts.gstatic.com/s/roboto/v29/KFOlCnqEu92Fr1MmSU5fBBc4.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    /* latin */
    @font-face {
      font-family: 'Raleway';
      font-style: normal;
      font-weight: 800;
      font-display: swap;
      src: url(https://fonts.gstatic.com/s/raleway/v22/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVtapbCIPrE.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    /* latin */
    @font-face {
      font-family: 'Raleway';
      font-style: normal;
      font-weight: 600;
      font-display: swap;
      src: url(https://fonts.gstatic.com/s/raleway/v22/1Ptug8zYS_SKggPNyC0ITw.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }
  </style>
  <?php wp_head(); ?>
  <?php
  $code_before_body_tag = get_field( 'code_before_body_tag', 'options' );
  echo $code_before_body_tag;
  ?>
</head>
<!--preloader style-->
<style>
  body:not(.loaded) {
    opacity: 0;
  }

  body:not(.loaded) * {
    transition: none !important;
  }

  body {
    transition: opacity .5s;
  }

  [modal-content] {
    display: none !important;
  }

  /*   region preloader */
  .lds-roller {
    display: none;
    position: relative;
    width: 8rem;
    height: 8rem;
    margin: 0 auto;
  }

  .lds-roller div {
    animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    transform-origin: 40px 40px;
  }

  .lds-roller div:after {
    content: " ";
    display: block;
    position: absolute;
    width: 7px;
    height: 7px;
    border-radius: 50%;
    background: #fff;
    margin: -4px 0 0 -4px;
  }

  .lds-roller div:nth-child(1) {
    animation-delay: -0.036s;
  }

  .lds-roller div:nth-child(1):after {
    top: 63px;
    left: 63px;
  }

  .lds-roller div:nth-child(2) {
    animation-delay: -0.072s;
  }

  .lds-roller div:nth-child(2):after {
    top: 68px;
    left: 56px;
  }

  .lds-roller div:nth-child(3) {
    animation-delay: -0.108s;
  }

  .lds-roller div:nth-child(3):after {
    top: 71px;
    left: 48px;
  }

  .lds-roller div:nth-child(4) {
    animation-delay: -0.144s;
  }

  .lds-roller div:nth-child(4):after {
    top: 72px;
    left: 40px;
  }

  .lds-roller div:nth-child(5) {
    animation-delay: -0.18s;
  }

  .lds-roller div:nth-child(5):after {
    top: 71px;
    left: 32px;
  }

  .lds-roller div:nth-child(6) {
    animation-delay: -0.216s;
  }

  .lds-roller div:nth-child(6):after {
    top: 68px;
    left: 24px;
  }

  .lds-roller div:nth-child(7) {
    animation-delay: -0.252s;
  }

  .lds-roller div:nth-child(7):after {
    top: 63px;
    left: 17px;
  }

  .lds-roller div:nth-child(8) {
    animation-delay: -0.288s;
  }

  .lds-roller div:nth-child(8):after {
    top: 56px;
    left: 12px;
  }

  @keyframes lds-roller {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }

  /* endregion preloader */
</style>
<!--end preloader style-->
<!-- ACF Fields -->
<?php
$container_width = get_field( 'container_width', 'options' );
$header_logo     = get_field( 'header_logo', 'options' );
$svg_or_image    = @$header_logo['svg_or_image'];
$logo_svg        = @$header_logo['svg'];
$logo_image      = @$header_logo['image'];
$select_manually = get_field( 'select_manually', 'options' );
// products number
global $woocommerce;
$products_cart_count = $woocommerce->cart->cart_contents_count;
?>
<?php if ( $container_width ) { ?>
  <style>
    @media screen and (min-width: 1680px) {
      .container {
        max-width: <?=$container_width?> !important;
      }
    }
  </style>
<?php } ?>
<!-- END ACF -->
<body <?php body_class(); ?> data-barba="wrapper">
<?php
$code_after_body_tag = get_field( 'code_after_body_tag', 'options' );
?>
<header class="tow_smart_header">
  <div class="menu-top-menu-container">
    <?php if ( have_rows( 'top_links', 'options' ) ) { ?>
      <ul id="menu-top-menu" class="top-menu reset-ul">
        <?php while ( have_rows( 'top_links', 'options' ) ) {
          the_row();
          $link = get_sub_field( 'link' );
          ?>
          <?php if ( $link ) { ?>
            <li class="menu-item ">
              <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?: '_self' ?>" class="paragraph paragraph-14 header-link"><?= $link['title'] ?></a>
            </li>
          <?php } ?>
        <?php } ?>
      </ul>
    <?php } ?>
  </div>
  <div class="bottom-menu">
    <div class="logo-and-menu">
      <div class="burger-and-menu">
        <div class="menu">
          <button class="burger-menu">
            <span class="span1"></span>
            <span class="span2"></span>
            <span class="span3"></span>
          </button>
        </div>
      </div>
      <?php if ( $logo_svg || $logo_image ) { ?>
        <a class="header-logo" href="<?= site_url(); ?>" title="Home">
          <?php if ( ! $svg_or_image ) { ?>
            <img class="header-logo-img" src="<?= $logo_image['url'] ?>" alt="<?= __( "TowSmart Logo", 'tow_smart' ) ?>">
          <?php } else {
            echo $logo_svg;
          } ?>
        </a>
      <?php } ?>
      <a href="<?= wc_get_cart_url(); ?>" class="icon shop d1-shop">
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
          <path d="M21.08 7.0001C20.9072 6.70072 20.6598 6.45123 20.3618 6.27597C20.0639 6.10071 19.7256 6.00566 19.38 6.0001H6.58L6 3.7401C5.9414 3.52194 5.81066 3.33004 5.62908 3.19567C5.44749 3.0613 5.22576 2.99236 5 3.0001H3C2.73478 3.0001 2.48043 3.10545 2.29289 3.29299C2.10536 3.48053 2 3.73488 2 4.0001C2 4.26531 2.10536 4.51967 2.29289 4.7072C2.48043 4.89474 2.73478 5.0001 3 5.0001H4.24L7 15.2601C7.0586 15.4783 7.18934 15.6702 7.37092 15.8045C7.55251 15.9389 7.77424 16.0078 8 16.0001H17C17.1847 15.9995 17.3656 15.9479 17.5227 15.8508C17.6798 15.7537 17.8069 15.615 17.89 15.4501L21.17 8.8901C21.3122 8.59211 21.3783 8.26357 21.3626 7.93378C21.3469 7.604 21.2498 7.28323 21.08 7.0001ZM16.38 14.0001H8.76L7.13 8.0001H19.38L16.38 14.0001Z" fill="#fff"/>
          <path d="M7.5 21C8.32843 21 9 20.3284 9 19.5C9 18.6716 8.32843 18 7.5 18C6.67157 18 6 18.6716 6 19.5C6 20.3284 6.67157 21 7.5 21Z" fill="#fff"/>
          <path d="M17.5 21C18.3284 21 19 20.3284 19 19.5C19 18.6716 18.3284 18 17.5 18C16.6716 18 16 18.6716 16 19.5C16 20.3284 16.6716 21 17.5 21Z" fill="#fff"/>
        </svg>
        <span class="cart-number"><?= $products_cart_count ?></span>
      </a>
    </div>
    <nav class="navbar hide-scrollbar">
      <div class="navbar-wrapper">
        <div class="search-popup">
          <div class="input-search-wrapper">
            <svg class="search-icon" width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M17.71 16.29L14.31 12.9C15.407 11.5025 16.0022 9.77666 16 8C16 6.41775 15.5308 4.87103 14.6518 3.55544C13.7727 2.23985 12.5233 1.21447 11.0615 0.608967C9.59966 0.00346625 7.99113 -0.15496 6.43928 0.153721C4.88743 0.462403 3.46197 1.22433 2.34315 2.34315C1.22433 3.46197 0.462403 4.88743 0.153721 6.43928C-0.15496 7.99113 0.00346625 9.59966 0.608967 11.0615C1.21447 12.5233 2.23985 13.7727 3.55544 14.6518C4.87103 15.5308 6.41775 16 8 16C9.77666 16.0022 11.5025 15.407 12.9 14.31L16.29 17.71C16.383 17.8037 16.4936 17.8781 16.6154 17.9289C16.7373 17.9797 16.868 18.0058 17 18.0058C17.132 18.0058 17.2627 17.9797 17.3846 17.9289C17.5064 17.8781 17.617 17.8037 17.71 17.71C17.8037 17.617 17.8781 17.5064 17.9289 17.3846C17.9797 17.2627 18.0058 17.132 18.0058 17C18.0058 16.868 17.9797 16.7373 17.9289 16.6154C17.8781 16.4936 17.8037 16.383 17.71 16.29ZM2 8C2 6.81332 2.3519 5.65328 3.01119 4.66658C3.67047 3.67989 4.60755 2.91085 5.7039 2.45673C6.80026 2.0026 8.00666 1.88378 9.17055 2.11529C10.3344 2.3468 11.4035 2.91825 12.2426 3.75736C13.0818 4.59648 13.6532 5.66558 13.8847 6.82946C14.1162 7.99335 13.9974 9.19975 13.5433 10.2961C13.0892 11.3925 12.3201 12.3295 11.3334 12.9888C10.3467 13.6481 9.18669 14 8 14C6.4087 14 4.88258 13.3679 3.75736 12.2426C2.63214 11.1174 2 9.5913 2 8Z" fill="rgba(255, 255, 255, 0.5)"/>
            </svg>
            <div class="lds-roller">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
            </div>
            <input id='input-small' onkeypress="cu_search(this.value);" type="search" placeholder="Search TowSmart" autocomplete="off">
            <div class="close-search">
              <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M13.4099 12L17.7099 7.71C17.8982 7.5217 18.004 7.2663 18.004 7C18.004 6.7337 17.8982 6.47831 17.7099 6.29C17.5216 6.1017 17.2662 5.99591 16.9999 5.99591C16.7336 5.99591 16.4782 6.1017 16.2899 6.29L11.9999 10.59L7.70994 6.29C7.52164 6.1017 7.26624 5.99591 6.99994 5.99591C6.73364 5.99591 6.47824 6.1017 6.28994 6.29C6.10164 6.47831 5.99585 6.7337 5.99585 7C5.99585 7.2663 6.10164 7.5217 6.28994 7.71L10.5899 12L6.28994 16.29C6.19621 16.383 6.12182 16.4936 6.07105 16.6154C6.02028 16.7373 5.99414 16.868 5.99414 17C5.99414 17.132 6.02028 17.2627 6.07105 17.3846C6.12182 17.5064 6.19621 17.617 6.28994 17.71C6.3829 17.8037 6.4935 17.8781 6.61536 17.9289C6.73722 17.9797 6.86793 18.0058 6.99994 18.0058C7.13195 18.0058 7.26266 17.9797 7.38452 17.9289C7.50638 17.8781 7.61698 17.8037 7.70994 17.71L11.9999 13.41L16.2899 17.71C16.3829 17.8037 16.4935 17.8781 16.6154 17.9289C16.7372 17.9797 16.8679 18.0058 16.9999 18.0058C17.132 18.0058 17.2627 17.9797 17.3845 17.9289C17.5064 17.8781 17.617 17.8037 17.7099 17.71C17.8037 17.617 17.8781 17.5064 17.9288 17.3846C17.9796 17.2627 18.0057 17.132 18.0057 17C18.0057 16.868 17.9796 16.7373 17.9288 16.6154C17.8781 16.4936 17.8037 16.383 17.7099 16.29L13.4099 12Z" fill="white"/>
              </svg>
            </div>
          </div>
          <ul class="search-menu reset-ul search-result hide-scrollbar layer">
          </ul>
        </div>

        <?php if ( ! $select_manually ) {
          $args = array(
            'taxonomy'   => 'product_cat',
            'orderby'    => 'name',
            'order'      => 'ASC',
            'parent'     => 0,
            'hide_empty' => false,
          ); ?>
          <ul class="primary-menu reset-ul">
            <?php
            foreach ( get_categories( $args ) as $category ) {
              $cat_slug        = $category->slug; //category ID
              $cat_name        = $category->name;
              $cat_description = $category->description;
              $cat_id          = $category->term_id;
              $thumbnail_id    = get_term_meta( $cat_id, 'thumbnail_id', true );
              $image           = wp_get_attachment_url( $thumbnail_id );
              $sub_categories  = get_categories( array(
                'child_of'   => $cat_id,
                'taxonomy'   => 'product_cat',
                'hide_empty' => false,
                'fields'     => 'ids',
              ) );
              if ( $cat_slug == 'uncategorized' ) {
                continue;
              }
              ?>
              <?php if ( $sub_categories ) { ?>
                <li class="menu-item has-megaMenu menu-item-has-children ">
                  <a href="<?= get_category_link( $cat_id ) ?>" class="paragraph paragraph-16 header-main-link header-link"><?= $cat_name ?></a>
                  <div class="sub-menu two-side">
                    <div class="left-and-arrow">
                      <div class="left-side">
                        <?php if ( $image ) { ?>
                          <picture class="aspect-ratio circle">
                            <img src="<?= $image ?>" alt="<?= $cat_name ?>">
                          </picture>
                        <?php } ?>
                        <div class="caption">
                          <a href="<?= get_category_link( $cat_id ) ?>" class="headline-5"><?= $cat_name ?></a>
                          <?php if ( $cat_description ) { ?>
                            <div class="my-paragraph d-paragraph"><?= $cat_description ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="arrow-left">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M10.4999 17C10.3683 17.0008 10.2379 16.9755 10.116 16.9258C9.9942 16.876 9.88338 16.8027 9.78994 16.71C9.69621 16.617 9.62182 16.5064 9.57105 16.3846C9.52028 16.2627 9.49414 16.132 9.49414 16C9.49414 15.868 9.52028 15.7373 9.57105 15.6154C9.62182 15.4936 9.69621 15.383 9.78994 15.29L13.0999 12L9.91994 8.68999C9.73369 8.50263 9.62915 8.24918 9.62915 7.98499C9.62915 7.7208 9.73369 7.46735 9.91994 7.27999C10.0129 7.18626 10.1235 7.11187 10.2454 7.0611C10.3672 7.01033 10.4979 6.98419 10.6299 6.98419C10.762 6.98419 10.8927 7.01033 11.0145 7.0611C11.1364 7.11187 11.247 7.18626 11.3399 7.27999L15.1999 11.28C15.3832 11.4669 15.4858 11.7182 15.4858 11.98C15.4858 12.2417 15.3832 12.4931 15.1999 12.68L11.1999 16.68C11.1102 16.7769 11.0021 16.8551 10.882 16.91C10.7618 16.965 10.632 16.9955 10.4999 17Z" fill="black"/>
                        </svg>
                      </div>
                    </div>
                    <div class="right-side">
                      <hr class="hr">
                      <ul class="sub-menu2">
                        <li class="menu-item2">
                          <a href="#" class="paragraph paragraph-14">All</a>
                          <svg width="16" height="16" viewBox="0 0 16 16" fill="none">
                            <path d="M8.27317 6.46004L9.1065 7.33337H5.33317C5.15636 7.33337 4.98679 7.40361 4.86177 7.52863C4.73674 7.65366 4.6665 7.82323 4.6665 8.00004C4.6665 8.17685 4.73674 8.34642 4.86177 8.47144C4.98679 8.59647 5.15636 8.6667 5.33317 8.6667H9.05984L8.19317 9.52671C8.06763 9.65224 7.99711 9.8225 7.99711 10C7.99711 10.1776 8.06763 10.3478 8.19317 10.4734C8.31871 10.5989 8.48897 10.6694 8.6665 10.6694C8.84404 10.6694 9.0143 10.5989 9.13984 10.4734L11.1398 8.47337C11.2005 8.40997 11.2481 8.33521 11.2798 8.25337C11.3151 8.17357 11.3333 8.08729 11.3333 8.00004C11.3333 7.91279 11.3151 7.8265 11.2798 7.7467C11.264 7.70751 11.2415 7.67141 11.2132 7.64004C11.1973 7.60287 11.1747 7.56896 11.1465 7.54004L9.23984 5.54004C9.11784 5.41185 8.94991 5.33738 8.773 5.333C8.59609 5.32862 8.42469 5.3947 8.2965 5.5167C8.16832 5.6387 8.09384 5.80663 8.08947 5.98354C8.08509 6.16045 8.15117 6.33185 8.27317 6.46004Z" fill="#E21932"/>
                            <path d="M1.3335 7.99996C1.3335 9.3185 1.72449 10.6074 2.45703 11.7038C3.18957 12.8001 4.23077 13.6546 5.44894 14.1592C6.66711 14.6637 8.00756 14.7958 9.30076 14.5385C10.594 14.2813 11.7819 13.6464 12.7142 12.714C13.6466 11.7817 14.2815 10.5938 14.5387 9.30056C14.796 8.00735 14.6639 6.66691 14.1594 5.44873C13.6548 4.23056 12.8003 3.18937 11.704 2.45683C10.6076 1.72428 9.3187 1.33329 8.00016 1.33329C7.12468 1.33329 6.25778 1.50573 5.44894 1.84076C4.6401 2.17579 3.90517 2.66685 3.28612 3.28591C2.66706 3.90497 2.176 4.6399 1.84097 5.44873C1.50593 6.25757 1.3335 7.12448 1.3335 7.99996ZM13.3335 7.99996C13.3335 9.05479 13.0207 10.0859 12.4347 10.963C11.8486 11.8401 11.0157 12.5236 10.0411 12.9273C9.0666 13.331 7.99425 13.4366 6.95968 13.2308C5.92512 13.025 4.97481 12.5171 4.22893 11.7712C3.48305 11.0253 2.9751 10.075 2.76931 9.04044C2.56352 8.00587 2.66914 6.93352 3.07281 5.95898C3.47647 4.98444 4.16006 4.15149 5.03712 3.56545C5.91418 2.97942 6.94533 2.66662 8.00016 2.66662C9.41465 2.66662 10.7712 3.22853 11.7714 4.22872C12.7716 5.22891 13.3335 6.58547 13.3335 7.99996Z" fill="#E21932"/>
                          </svg>
                        </li>
                        <?php foreach ( $sub_categories as $sub_category ) {
                          $sub_category_name = get_the_category_by_ID( $sub_category );
                          ?>
                          <li class="menu-item2">
                            <a href="<?= get_category_link( $sub_category ) ?>" class="paragraph paragraph-14 header-link sublink"><?= $sub_category_name ?></a>
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none">
                              <path d="M8.27317 6.46004L9.1065 7.33337H5.33317C5.15636 7.33337 4.98679 7.40361 4.86177 7.52863C4.73674 7.65366 4.6665 7.82323 4.6665 8.00004C4.6665 8.17685 4.73674 8.34642 4.86177 8.47144C4.98679 8.59647 5.15636 8.6667 5.33317 8.6667H9.05984L8.19317 9.52671C8.06763 9.65224 7.99711 9.8225 7.99711 10C7.99711 10.1776 8.06763 10.3478 8.19317 10.4734C8.31871 10.5989 8.48897 10.6694 8.6665 10.6694C8.84404 10.6694 9.0143 10.5989 9.13984 10.4734L11.1398 8.47337C11.2005 8.40997 11.2481 8.33521 11.2798 8.25337C11.3151 8.17357 11.3333 8.08729 11.3333 8.00004C11.3333 7.91279 11.3151 7.8265 11.2798 7.7467C11.264 7.70751 11.2415 7.67141 11.2132 7.64004C11.1973 7.60287 11.1747 7.56896 11.1465 7.54004L9.23984 5.54004C9.11784 5.41185 8.94991 5.33738 8.773 5.333C8.59609 5.32862 8.42469 5.3947 8.2965 5.5167C8.16832 5.6387 8.09384 5.80663 8.08947 5.98354C8.08509 6.16045 8.15117 6.33185 8.27317 6.46004Z" fill="#E21932"/>
                              <path d="M1.3335 7.99996C1.3335 9.3185 1.72449 10.6074 2.45703 11.7038C3.18957 12.8001 4.23077 13.6546 5.44894 14.1592C6.66711 14.6637 8.00756 14.7958 9.30076 14.5385C10.594 14.2813 11.7819 13.6464 12.7142 12.714C13.6466 11.7817 14.2815 10.5938 14.5387 9.30056C14.796 8.00735 14.6639 6.66691 14.1594 5.44873C13.6548 4.23056 12.8003 3.18937 11.704 2.45683C10.6076 1.72428 9.3187 1.33329 8.00016 1.33329C7.12468 1.33329 6.25778 1.50573 5.44894 1.84076C4.6401 2.17579 3.90517 2.66685 3.28612 3.28591C2.66706 3.90497 2.176 4.6399 1.84097 5.44873C1.50593 6.25757 1.3335 7.12448 1.3335 7.99996ZM13.3335 7.99996C13.3335 9.05479 13.0207 10.0859 12.4347 10.963C11.8486 11.8401 11.0157 12.5236 10.0411 12.9273C9.0666 13.331 7.99425 13.4366 6.95968 13.2308C5.92512 13.025 4.97481 12.5171 4.22893 11.7712C3.48305 11.0253 2.9751 10.075 2.76931 9.04044C2.56352 8.00587 2.66914 6.93352 3.07281 5.95898C3.47647 4.98444 4.16006 4.15149 5.03712 3.56545C5.91418 2.97942 6.94533 2.66662 8.00016 2.66662C9.41465 2.66662 10.7712 3.22853 11.7714 4.22872C12.7716 5.22891 13.3335 6.58547 13.3335 7.99996Z" fill="#E21932"/>
                            </svg>
                          </li>
                        <?php } ?>
                      </ul>
                    </div>
                  </div>
                </li>
              <?php } else { ?>
                <li class="menu-item has-megaMenu">
                  <a href="<?= get_category_link( $cat_id ) ?>" class="paragraph paragraph-16 header-link"><?= $cat_name ?></a>
                  <div class="sub-menu">
                    <div class="left-side p-16">
                      <?php if ( $image ) { ?>
                        <picture class="aspect-ratio circle">
                          <img src="<?= $image ?>" alt="<?= $cat_name ?>">
                        </picture>
                      <?php } ?>
                      <div class="caption">
                        <a href="<?= get_category_link( $cat_id ) ?>" class="headline-5"><?= $cat_name ?></a>
                        <?php if ( $cat_description ) { ?>
                          <div class="my-paragraph d-paragraph"><?= $cat_description ?></div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </li>
              <?php } ?>
            <?php } ?>
          </ul>
        <?php } else {
          echo 'lol';
        } ?>
        <div class="icons">
          <div class="icon" id="search">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
              <path d="M20.71 19.29L17.31 15.9C18.407 14.5025 19.0022 12.7767 19 11C19 9.41775 18.5308 7.87103 17.6518 6.55544C16.7727 5.23985 15.5233 4.21447 14.0615 3.60897C12.5997 3.00347 10.9911 2.84504 9.43928 3.15372C7.88743 3.4624 6.46197 4.22433 5.34315 5.34315C4.22433 6.46197 3.4624 7.88743 3.15372 9.43928C2.84504 10.9911 3.00347 12.5997 3.60897 14.0615C4.21447 15.5233 5.23985 16.7727 6.55544 17.6518C7.87103 18.5308 9.41775 19 11 19C12.7767 19.0022 14.5025 18.407 15.9 17.31L19.29 20.71C19.383 20.8037 19.4936 20.8781 19.6154 20.9289C19.7373 20.9797 19.868 21.0058 20 21.0058C20.132 21.0058 20.2627 20.9797 20.3846 20.9289C20.5064 20.8781 20.617 20.8037 20.71 20.71C20.8037 20.617 20.8781 20.5064 20.9289 20.3846C20.9797 20.2627 21.0058 20.132 21.0058 20C21.0058 19.868 20.9797 19.7373 20.9289 19.6154C20.8781 19.4936 20.8037 19.383 20.71 19.29V19.29ZM5 11C5 9.81332 5.3519 8.65328 6.01119 7.66658C6.67047 6.67989 7.60755 5.91085 8.7039 5.45673C9.80026 5.0026 11.0067 4.88378 12.1705 5.11529C13.3344 5.3468 14.4035 5.91825 15.2426 6.75736C16.0818 7.59648 16.6532 8.66558 16.8847 9.82946C17.1162 10.9933 16.9974 12.1997 16.5433 13.2961C16.0892 14.3925 15.3201 15.3295 14.3334 15.9888C13.3467 16.6481 12.1867 17 11 17C9.4087 17 7.88258 16.3679 6.75736 15.2426C5.63214 14.1174 5 12.5913 5 11Z" fill="#333333"/>
            </svg>
          </div>
          <a href="<?= get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ); ?>" class="icon person">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
              <path d="M12 11C12.7911 11 13.5645 10.7654 14.2223 10.3259C14.8801 9.88635 15.3928 9.26164 15.6955 8.53074C15.9983 7.79983 16.0775 6.99556 15.9231 6.21964C15.7688 5.44372 15.3878 4.73098 14.8284 4.17157C14.269 3.61216 13.5563 3.2312 12.7804 3.07686C12.0044 2.92252 11.2002 3.00173 10.4693 3.30448C9.73836 3.60723 9.11365 4.11992 8.67412 4.77772C8.2346 5.43552 8 6.20888 8 7C8 8.06087 8.42143 9.07828 9.17157 9.82843C9.92172 10.5786 10.9391 11 12 11ZM12 5C12.3956 5 12.7822 5.1173 13.1111 5.33706C13.44 5.55683 13.6964 5.86918 13.8478 6.23463C13.9991 6.60009 14.0387 7.00222 13.9616 7.39018C13.8844 7.77814 13.6939 8.13451 13.4142 8.41422C13.1345 8.69392 12.7781 8.8844 12.3902 8.96157C12.0022 9.03874 11.6001 8.99914 11.2346 8.84776C10.8692 8.69639 10.5568 8.44004 10.3371 8.11114C10.1173 7.78224 10 7.39556 10 7C10 6.46957 10.2107 5.96086 10.5858 5.58579C10.9609 5.21072 11.4696 5 12 5Z" fill="#333333"/>
              <path d="M12 13C10.1435 13 8.36301 13.7375 7.05025 15.0503C5.7375 16.363 5 18.1435 5 20C5 20.2652 5.10536 20.5196 5.29289 20.7071C5.48043 20.8946 5.73478 21 6 21C6.26522 21 6.51957 20.8946 6.70711 20.7071C6.89464 20.5196 7 20.2652 7 20C7 18.6739 7.52678 17.4021 8.46447 16.4645C9.40215 15.5268 10.6739 15 12 15C13.3261 15 14.5979 15.5268 15.5355 16.4645C16.4732 17.4021 17 18.6739 17 20C17 20.2652 17.1054 20.5196 17.2929 20.7071C17.4804 20.8946 17.7348 21 18 21C18.2652 21 18.5196 20.8946 18.7071 20.7071C18.8946 20.5196 19 20.2652 19 20C19 18.1435 18.2625 16.363 16.9497 15.0503C15.637 13.7375 13.8565 13 12 13Z" fill="#333333"/>
            </svg>
          </a>
          <a href="<?= wc_get_cart_url(); ?>" class="icon shop d-shop">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
              <path d="M21.08 7.0001C20.9072 6.70072 20.6598 6.45123 20.3618 6.27597C20.0639 6.10071 19.7256 6.00566 19.38 6.0001H6.58L6 3.7401C5.9414 3.52194 5.81066 3.33004 5.62908 3.19567C5.44749 3.0613 5.22576 2.99236 5 3.0001H3C2.73478 3.0001 2.48043 3.10545 2.29289 3.29299C2.10536 3.48053 2 3.73488 2 4.0001C2 4.26531 2.10536 4.51967 2.29289 4.7072C2.48043 4.89474 2.73478 5.0001 3 5.0001H4.24L7 15.2601C7.0586 15.4783 7.18934 15.6702 7.37092 15.8045C7.55251 15.9389 7.77424 16.0078 8 16.0001H17C17.1847 15.9995 17.3656 15.9479 17.5227 15.8508C17.6798 15.7537 17.8069 15.615 17.89 15.4501L21.17 8.8901C21.3122 8.59211 21.3783 8.26357 21.3626 7.93378C21.3469 7.604 21.2498 7.28323 21.08 7.0001ZM16.38 14.0001H8.76L7.13 8.0001H19.38L16.38 14.0001Z" fill="#333333"/>
              <path d="M7.5 21C8.32843 21 9 20.3284 9 19.5C9 18.6716 8.32843 18 7.5 18C6.67157 18 6 18.6716 6 19.5C6 20.3284 6.67157 21 7.5 21Z" fill="#333333"/>
              <path d="M17.5 21C18.3284 21 19 20.3284 19 19.5C19 18.6716 18.3284 18 17.5 18C16.6716 18 16 18.6716 16 19.5C16 20.3284 16.6716 21 17.5 21Z" fill="#333333"/>
            </svg>
            <span class="cart-number"><?= $products_cart_count ?></span>
          </a>
        </div>
        <?php if ( have_rows( 'top_links', 'options' ) ) { ?>
          <ul class="top-menu menu1 reset-ul">
            <?php while ( have_rows( 'top_links', 'options' ) ) {
              the_row();
              $menu_link = get_sub_field( 'menu_link' );
              ?>
              <?php if ( $menu_link ) { ?>
                <li class="menu-item ">
                  <a href="<?= $menu_link['url'] ?>" target="<?= $menu_link['target'] ?>" class="paragraph paragraph-14"><?= $menu_link['title'] ?></a>
                </li>
              <?php } ?>
            <?php } ?>
          </ul>
        <?php } ?>
      </div>
    </nav>
  </div>
</header>

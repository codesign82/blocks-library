import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/towSmart';

const blockScript = async (container = document) => {
  const header = container.querySelector('header');
  
  let burger_menu = header.querySelector('.burger-menu');
  let navbar = header.querySelector('.navbar');
  burger_menu.addEventListener('click', function () {
    burger_menu.classList.toggle("change");
    navbar.classList.toggle("navbar-active");
  });
  
  
  // region open sub menu in responsive
  const headerLinks = header.querySelectorAll('.menu-item-has-children');
  const mobileMedia = window.matchMedia('(max-width: 992px)');
  headerLinks.forEach((menuItem) => {
    const menuItemBody = menuItem.querySelector('.right-side');
    const paragraph = menuItem.querySelector('.d-paragraph');
    menuItem?.addEventListener('click', (e) => {
      if (!mobileMedia.matches || !menuItemBody) return;
      const isOpened = menuItem?.classList.toggle('active-header-link');
      if (!isOpened) {
        gsap.to([menuItemBody, paragraph], {height: 0});
      } else {
        gsap.to(Array.from(headerLinks).map(otherMenuItem => {
          const otherMenuItemBody = otherMenuItem.querySelector('.right-side');
          const paragraph = otherMenuItem.querySelector('.d-paragraph');
          if (otherMenuItemBody && menuItem !== otherMenuItem) {
            otherMenuItem?.classList.remove('active-header-link');
            gsap.set(otherMenuItem, {zIndex: 1});
          }
          return [otherMenuItemBody, paragraph];
        }), {height: 0});
        gsap.set(menuItem, {zIndex: 2});
        gsap.to([menuItemBody, paragraph], {height: 'auto'});
      }
    });
  });
// endregion open sub menu in responsive
  
  // const deskTop = window.matchMedia('(min-width: 992px)');
  // if (deskTop.matches) {
  navbar.classList.remove("navbar-active");
  const searchBtn = header.querySelector('#search');
  const inputSearchWrapper = header.querySelector('.input-search-wrapper');
  let search_result = header.querySelector('.search-result');
  let input_small = header.querySelector('#input-small');
  const close_search = header.querySelector('.close-search');
  searchBtn?.addEventListener('click', function () {
    inputSearchWrapper.classList.add('active');
  });
  close_search?.addEventListener('click', function () {
    inputSearchWrapper.classList.remove('active');
    search_result.classList.remove('open-search-result');
    input_small.value = '';
    document.documentElement.classList.remove('modal-opened');
    close_search.classList.remove('close-search-active');
  });
  
  // }
  
  
  // let search_result = header.querySelector('.search-result');
  let links = header.querySelector('.search-menu');
  input_small?.addEventListener('focus', function () {
    // search_result.classList.add('layer');
    search_result.classList.add('open-search-result');
    close_search.classList.add('close-search-active');
    document.documentElement.classList.add('modal-opened');
  });
  // input_small?.addEventListener('blur', function () {
  //   search_result.classList.remove('layer');
  //   links.style.opacity = 0;
  
  // });
  
  
  
  
  animations(header);
  imageLazyLoading(header);
};
windowOnLoad(blockScript);




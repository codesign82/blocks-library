<!doctype html>
<html <?php language_attributes(); ?>>
<head>

    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="description" content="<?php if (is_single()) {
        single_post_title('', true);
    } else {
        bloginfo('name');
        echo " - ";
        bloginfo('description');
    } ?>"/>
    <meta
            content="width=device-width, initial-scale=1.0, maximum-scale=5, minimum-scale=1.0"
            name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <script>
        const BiggerThanDesignWidth = 1920;
        const designWidth = 1440;
        const desktop = 1440;
        const tablet = 992;
        const mobile = 600;
        const sMobile = 390;

        function fixContainer() {
            const resizeHandler = function () {
                if (window.innerWidth >= BiggerThanDesignWidth) {
                    document.documentElement.style.fontSize = `${10}px`;
                    // document.documentElement.style.fontSize = `${9 * window.innerWidth / desktop}px`;
                } else if (window.innerWidth < BiggerThanDesignWidth && window.innerWidth >= desktop) {
                    document.documentElement.style.fontSize = `${10 * window.innerWidth / desktop}px`;
                } else if (window.innerWidth < desktop && window.innerWidth >= tablet) {
                    document.documentElement.style.fontSize = `${10 * window.innerWidth / designWidth}px`;
                } else if (window.innerWidth < tablet && window.innerWidth >= mobile) {
                    document.documentElement.style.fontSize = `${10 * window.innerWidth / tablet}px`;
                } else if (window.innerWidth < mobile && window.innerWidth >= sMobile) {
                    document.documentElement.style.fontSize = `${10}px`;
                } else {
                    document.documentElement.style.fontSize = `${10 * window.innerWidth / sMobile}px`;
                }
            };
            resizeHandler();
            window.addEventListener('resize', resizeHandler);
        }

        fixContainer();
    </script>
    <style>
        /* latin */
        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/poppins/v20/pxiEyp8kv8JHgFVrJJfecg.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin */
        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 400;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/poppins/v20/pxiGyp8kv8JHgFVrJJLucHtA.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin */
        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 500;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/poppins/v20/pxiByp8kv8JHgFVrLGT9Z1xlFQ.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin */
        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 600;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/poppins/v20/pxiByp8kv8JHgFVrLEj6Z1xlFQ.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin */
        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 600;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/poppins/v20/pxiDyp8kv8JHgFVrJJLmr19VF9eO.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }


        /* latin */
        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 700;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/poppins/v20/pxiByp8kv8JHgFVrLCz7Z1xlFQ.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }


        /* latin */
        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 700;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/poppins/v20/pxiDyp8kv8JHgFVrJJLmy15VF9eO.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin */
        @font-face {
            font-family: 'Inter';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/inter/v11/UcCO3FwrK3iLTeHuS_fvQtMwCp50KnMw2boKoduKmMEVuLyfAZ9hiA.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin */
        @font-face {
            font-family: 'DM Sans';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/dmsans/v11/rP2Hp2ywxg089UriCZOIHQ.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin */
        @font-face {
            font-family: 'DM Sans';
            font-style: normal;
            font-weight: 700;
            src: url(https://fonts.gstatic.com/s/dmsans/v11/rP2Cp2ywxg089UriASitCBimCw.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin */
        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            font-stretch: 100%;
            src: url(https://fonts.gstatic.com/s/opensans/v29/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsjZ0B4gaVI.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

    </style>
    <?php wp_head(); ?>
    <?php
    $code_before_body_tag = get_field('code_before_body_tag', 'options');
    echo $code_before_body_tag;
    ?>
</head>
<!--preloader style-->
<style>
    body:not(.loaded) {
        opacity: 0;
    }

    section:not(.js-loaded):not(:first-of-type) {
        height: 100vh;
    }

    body:not(.loaded) * {
        transition: none !important;
    }

    body {
        transition: opacity .5s;
    }

    [modal-content] {
        display: none !important;
    }

    /*   region preloader */
    .lds-roller {
        display: none;
        position: relative;
        width: 8rem;
        height: 8rem;
        margin: 0 auto;
    }

    .lds-roller div {
        animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
        transform-origin: 40px 40px;
    }

    .lds-roller div:after {
        content: " ";
        display: block;
        position: absolute;
        width: 7px;
        height: 7px;
        border-radius: 50%;
        background: #fff;
        margin: -4px 0 0 -4px;
    }

    .lds-roller div:nth-child(1) {
        animation-delay: -0.036s;
    }

    .lds-roller div:nth-child(1):after {
        top: 63px;
        left: 63px;
    }

    .lds-roller div:nth-child(2) {
        animation-delay: -0.072s;
    }

    .lds-roller div:nth-child(2):after {
        top: 68px;
        left: 56px;
    }

    .lds-roller div:nth-child(3) {
        animation-delay: -0.108s;
    }

    .lds-roller div:nth-child(3):after {
        top: 71px;
        left: 48px;
    }

    .lds-roller div:nth-child(4) {
        animation-delay: -0.144s;
    }

    .lds-roller div:nth-child(4):after {
        top: 72px;
        left: 40px;
    }

    .lds-roller div:nth-child(5) {
        animation-delay: -0.18s;
    }

    .lds-roller div:nth-child(5):after {
        top: 71px;
        left: 32px;
    }

    .lds-roller div:nth-child(6) {
        animation-delay: -0.216s;
    }

    .lds-roller div:nth-child(6):after {
        top: 68px;
        left: 24px;
    }

    .lds-roller div:nth-child(7) {
        animation-delay: -0.252s;
    }

    .lds-roller div:nth-child(7):after {
        top: 63px;
        left: 17px;
    }

    .lds-roller div:nth-child(8) {
        animation-delay: -0.288s;
    }

    .lds-roller div:nth-child(8):after {
        top: 56px;
        left: 12px;
    }

    @keyframes lds-roller {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }

    /* endregion preloader */
</style>
<!--end preloader style-->
<!-- ACF Fields -->
<?php


// products number
?>
<!-- END ACF -->
<body <?php body_class(); ?> data-barba="wrapper">
<?php

$container_width = get_field('container_width', 'options');
$header_logo = get_field('header_logo', 'options');
$svg_or_image = @$header_logo['svg_or_image'];
$logo_svg = @$header_logo['svg'];
$logo_image = @$header_logo['image'];
$cta_button = get_field('cta_button', 'options');
$get_started = get_field('get_started', 'options');
$main_language = get_field('main_language', 'options');
$add_more_languages = get_field('add_more_languages', 'options');
$active_lang = apply_filters('wpml_current_language', NULL);
$languages = apply_filters('wpml_active_languages', NULL);
$banner = get_field('banner', 'options');
$banner_link = get_field('banner_link', 'options');
?>

<header class="header-wrapper scroll-up <?= ($banner_link) ? 'has-banner' : '' ?>">
    <?php if ($banner_link) { ?>
        <div class='banner'>
            <a class='link banner-link' target="<?= $banner_link['target'] ?>" href='<?= $banner_link['url'] ?>'>
                <?= $banner_link['title'] ?>
                <svg class='arrow' viewBox='0 0 29.5 22.1' width='28' height='15'
                     role='application'>
                    <path class='arrow-head' role='application'
                          d='M29.1,12.1a1.7,1.7,0,0,0,0-2.1L19.5.4a1.7,1.7,0,0,0-2.1,0,1.6,1.6,0,0,0,0,2.2L25.9,11l-8.5,8.5a1.6,1.6,0,0,0,0,2.2,1.7,1.7,0,0,0,2.1,0Z'
                          fill='#f90'/>
                    <path class='arrow-line' d='M0,12.5H28v-3H0Z' fill='#f90'/>
                </svg>
            </a>
        </div>
    <?php } ?>
    <div class="content-wrapper">
        <div class='left-wrapper'>
            <!--          burger menu and cross-->
            <div class='burger-and-menu'>
                <div class='menu'>
                    <button aria-label='burger' class='burger-menu' id='burger-menu'>
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </div>
            </div>

            <!--     logo-->
            <?php if ($header_logo) { ?>
                <a href="<?= site_url() ?>" class="main-logo white-logo">
                    <?php if ($svg_or_image) { ?>
                        <?php if ($logo_image) { ?>
                            <img src="<?= $logo_image['url'] ?>"
                                 alt="<?= $logo_image['alt'] ?>">
                        <?php } ?>
                    <?php } else {
                        echo $logo_svg;
                    } ?>
                </a>
            <?php } ?>
        </div>

        <!--     links  -->
        <div class="middle-wrapper">
            <nav class="navbar">
                <div
                        class="ellipse ellipse-light-blue  position  position-center_right orbs-10 "></div>
                <div class="navbar-wrapper">
                    <?php if (have_rows('header_links', 'options')) { ?>
                        <ul class="primary-menu">
                            <?php while (have_rows('header_links', 'options')) {
                                the_row();
                                $header_link = get_sub_field('header_link');
                                $is_has_megamenu = get_sub_field('is_has_megamenu');
                                $has_menu = get_sub_field('has_menu');
                                $select_your_menu = get_sub_field('select_your_menu');
                                $chek_has_menu = $select_your_menu === 'mega_menu' ? 'menu-item-has-children menu-item-has-megamenu' : 'menu-item-has-items menu-item-has-children';
                                $mega_menu_description = get_sub_field('mega_menu_description');
                                $mega_menu_link = get_sub_field('mega_menu_link');
                                ?>
                                <?php if ($header_link) { ?>
                                    <li class="menu-item <?= $chek_has_menu ?>">
                    <span class="link-and-icon" role="tab" aria-haspopup="true" aria-expanded="true">
                     <a class="menu-item-flex"
                        href="<?= $header_link['url'] ?>"><?= $header_link['title'] ?>
                  </a>
                  <?php if ($select_your_menu && $has_menu) { ?>
                      <svg
                              class=" toggle-open minus-plus <?php if ($select_your_menu === 'mega_menu') { ?>open-mega-menu<?php } ?>"
                              viewBox="0 0 13.6 13.6">
                      <line x1="0.8" y1="6.8" x2="12.8" y2="6.8" fill="none"
                            stroke="#fff" stroke-linecap="round"
                            stroke-miterlimit="10"
                            stroke-width="1.6"/>
                      <line class="remove-to-close" x1="6.8" y1="12.8" x2="6.8"
                            y2="0.8" fill="none" stroke="#fff"
                            stroke-linecap="round"
                            stroke-miterlimit="10"
                            stroke-width="1.6"/>
                    </svg>
                  <?php } ?>
                  </span>
                                        <?php if ($has_menu && $select_your_menu === 'mega_menu') { ?>
                                            <div class="mega-menu-wrapper">
                                                <div class="mega-menu">
                                                    <div class="upper-mega-menu">
                                                        <div class="link back-link link-left">
                                                            <svg class="arrow arrow-left" role="application"
                                                                 viewBox="0 0 29.5 22.1">
                                                                <path class="arrow-head" role="application"
                                                                      d="M29.1,12.1a1.7,1.7,0,0,0,0-2.1L19.5.4a1.7,1.7,0,0,0-2.1,0,1.6,1.6,0,0,0,0,2.2L25.9,11l-8.5,8.5a1.6,1.6,0,0,0,0,2.2,1.7,1.7,0,0,0,2.1,0Z"
                                                                      fill="#f90"/>
                                                                <path class="arrow-line" d="M0,12.5H28v-3H0Z"
                                                                      fill="#f90"/>
                                                            </svg>
                                                            <?= __(' Back To Menu', 'headversity') ?>
                                                        </div>
                                                    </div>
                                                    <div class="mega-menu-left">
                                                        <h2 class="title"> <?= $header_link['title'] ?></h2>
                                                        <?php if ($mega_menu_description) { ?>
                                                            <div
                                                                    class="description"><?= $mega_menu_description ?></div>
                                                        <?php } ?>
                                                        <?php if ($mega_menu_link) { ?>
                                                            <a class="link " target="<?= $mega_menu_link['target'] ?>"
                                                               href="<?= $mega_menu_link['url'] ?>">
                                                                <?= $mega_menu_link['title'] ?>
                                                                <svg class="arrow" viewBox="0 0 29.5 22.1" width="28"
                                                                     height="15" role="application">
                                                                    <path class="arrow-head" role="application"
                                                                          d="M29.1,12.1a1.7,1.7,0,0,0,0-2.1L19.5.4a1.7,1.7,0,0,0-2.1,0,1.6,1.6,0,0,0,0,2.2L25.9,11l-8.5,8.5a1.6,1.6,0,0,0,0,2.2,1.7,1.7,0,0,0,2.1,0Z"
                                                                          fill="#f90"/>
                                                                    <path class="arrow-line" d="M0,12.5H28v-3H0Z"
                                                                          fill="#f90"/>
                                                                </svg>
                                                            </a>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="mega-menu-right">
                                                        <?php
                                                        $webinar_card = get_sub_field('webinar_card', 'options');
                                                        $webinar_card_text = get_sub_field('webinar_card_text', 'options');

                                                        if ($webinar_card): ?>
                                                            <?php get_template_part('template-parts/webinar-card', '', array('post_id' => $webinar_card)); ?>
                                                            <div
                                                                    class="card-text"><?= $webinar_card_text ?></div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } elseif ($has_menu && $select_your_menu === 'sub_menu') { ?>
                                            <?php if (have_rows('sub_menu', 'options')) { ?>
                                                <ul class="sub-menu">
                                                    <?php while (have_rows('sub_menu', 'options')) {
                                                        the_row();
                                                        $submenu_link = get_sub_field('submenu_link');
                                                        ?>
                                                        <li>
                                                            <a class="menu-item-flex"
                                                               href="<?= $submenu_link['url'] ?>"><?= $submenu_link['title'] ?>
                                                            </a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            <?php } ?>
                                        <?php } ?>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                            <li class="menu-item  menu-item-has-children language-mobile">
                                <div class="link-and-icon" role="button">
                                    <span class="menu-item-flex"><?= $active_lang ?: 'EN' ?></span>
                                    <svg class="arrow-icon" width="16" height="9" role="application"
                                         viewBox="0 0 16 9"
                                         fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path
                                                d="M7.29289 8.70711C7.68342 9.09763 8.31658 9.09763 8.70711 8.70711L15.0711 2.34315C15.4616 1.95262 15.4616 1.31946 15.0711 0.928932C14.6805 0.538407 14.0474 0.538407 13.6569 0.928932L8 6.58579L2.34315 0.928933C1.95262 0.538408 1.31946 0.538408 0.928932 0.928933C0.538407 1.31946 0.538407 1.95262 0.928932 2.34315L7.29289 8.70711ZM7 7L7 8L9 8L9 7L7 7Z"
                                                fill="#FF9900"/>
                                    </svg>
                                </div>
                                <?php
                                if (!empty($languages)) { ?>
                                    <ul class="sub-menu">
                                        <?php foreach ($languages as $language) {
                                            $native_name = $language['active'] ? strtoupper($language['native_name']) : $language['native_name'];

                                            if (!$language['active']) echo '<li><a
            class="menu-item-flex" href="' . esc_url($language['url']) . '">';
                                            if (!$language['active']) echo esc_html($language['language_code']) . ' ';
                                            if (!$language['active']) echo '</a></li>';
                                        } ?>
                                    </ul>
                                <?php } ?>
                            </li>
                        </ul>
                    <?php } ?>
                    <?php if ($cta_button) { ?>
                        <a href="<?= $cta_button['url'] ?>"
                           target="<?= $cta_button['url'] ?>"
                           class="btn btn-in-mobile"><?= $cta_button['title'] ?></a>
                    <?php } ?>
                </div>
            </nav>
        </div>
        <!--   end of  links  -->
        <div class="right-wrapper">
            <div
                    class="ellipse ellipse-light-blue  position  position-center_right orbs-10 "></div>
            <div class="language ">
                <button
                        class="lang-switcher language-text link"
                        aria-label="<?= __('english language', 'headversity') ?>">
                    <?= $active_lang ?: 'EN' ?>
                </button>
                <?php
                if (!empty($languages)) { ?>
                    <div class="list-of-language">
                        <?php foreach ($languages as $language) {
                            $native_name = $language['active'] ? strtoupper($language['native_name']) : $language['native_name'];

                            if (!$language['active']) echo '<a
            class="right lang-switcher language-text" href="' . esc_url($language['url']) . '">';
                            if (!$language['active']) echo esc_html($language['language_code']) . ' ';
                            if (!$language['active']) echo '</a>';
                        } ?>
                    </div>
                <?php } ?>
            </div>
            <?php if ($get_started) { ?>
                <a href="<?= $get_started['url'] ?>"
                   target="<?= $get_started['target'] ?>" class="link">
                    <?= $get_started['title'] ?>
                    <svg class="arrow" viewBox="0 0 29.5 22.1" width="28" height="15" role="application">
                        <path class="arrow-head" role="application"
                              d="M29.1,12.1a1.7,1.7,0,0,0,0-2.1L19.5.4a1.7,1.7,0,0,0-2.1,0,1.6,1.6,0,0,0,0,2.2L25.9,11l-8.5,8.5a1.6,1.6,0,0,0,0,2.2,1.7,1.7,0,0,0,2.1,0Z"
                              fill="#f90"/>
                        <path class="arrow-line" d="M0,12.5H28v-3H0Z" fill="#f90"/>
                    </svg>
                </a>
            <?php } ?>
        </div>
    </div>
</header>



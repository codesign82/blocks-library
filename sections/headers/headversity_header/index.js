import './index.html';
import './style.scss';
import {gsap} from "gsap";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/propellerhealth';
import {windowOnLoad} from "../../../scripts/windowOnLoad";

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const header = container.querySelector('header');

  const burgerMenu = header.querySelector('.burger-menu');
  const menuLinks = header.querySelector('.navbar');


  if (!burgerMenu) return;
  const burgerTl = gsap.timeline({paused: true});
  const burgerSpans = burgerMenu.querySelectorAll('span');
  gsap.set(burgerSpans, {transformOrigin: 'center'});
  burgerTl
      .to(burgerSpans, {
        y: gsap.utils.wrap([`0.8rem`, 0, `-0.8rem`]),
        duration: 0.25,
      })
      // .to(header.querySelector(".burger-menu span:first-child"), {
      //   marginBottom: -1,
      //   duration: .3,
      //   ease: "power3.inOut"
      // }, "<")
      .set(burgerSpans, {autoAlpha: gsap.utils.wrap([1, 0, 1])})
      .to(burgerSpans, {rotation: gsap.utils.wrap([45, 0, -45])})
      .set(burgerSpans, {rotation: gsap.utils.wrap([45, 0, 135])})

  burgerMenu.addEventListener('click', function () {

    if (burgerMenu.classList.contains('burger-menu-active')) {
      // region allow page scroll
      document.documentElement.classList.remove('modal-opened');
      // endregion allow page scroll
      burgerMenu.classList.remove('burger-menu-active');
      menuLinks.classList.remove('header-links-active');
      header.classList.remove('header-active');
      const activeItem = header.querySelector('.menu-item-has-children.menu-item-active');
      activeItem && activeItem.classList.remove('menu-item-active');
      burgerTl.reverse();
    } else {
      burgerMenu.classList.add('burger-menu-active');
      menuLinks.classList.add('header-links-active');
      header.classList.add('header-active');
      burgerTl.play();
      // region prevent page scroll
      document.documentElement.classList.add('modal-opened');
      // endregion prevent page scroll
      gsap.fromTo(menuLinks.querySelectorAll('.link-and-icon , .btn-in-mobile , .sub-menu'), {
        y: 30,
        autoAlpha: 0,
      }, {
        y: 0,
        autoAlpha: 1,
        stagger: .05,
        duration: .4,
        delay: .5,
      });
    }
  });


  // region open sub menu in responsive
  const menuItems = header.querySelectorAll('.menu-item-has-items');
  const languageMobile = header.querySelector('.language-mobile');
  const menuItemsMega = header.querySelectorAll('.menu-item-has-megamenu');
  const openMegaMenuICon = header.querySelectorAll('.open-mega-menu');
  const btnBack = header?.querySelectorAll('.back-link');
  const mobileMedia = window.matchMedia('(max-width: 992px)');

  function OPenMenu() {
    menuItems.forEach((menuItem) => {
      const subMenu = menuItem.querySelector('.sub-menu');
      menuItem?.addEventListener('click', (e) => {
        if (!mobileMedia.matches || !subMenu || e.target.classList.contains('header-link') || e.target.closest('.sub-menu,.menu-item-in-sub-menu a')) return;
        const isOpened = menuItem?.classList.toggle('menu-item-active');
        if (!isOpened) {
          gsap.to(subMenu, {height: 0});
        } else {
          gsap.to(Array.from(menuItems).map(otherMenuItem => {
            const otherMenuItemBody = otherMenuItem.querySelector('.sub-menu');
            if (otherMenuItemBody && menuItem !== otherMenuItem) {
              otherMenuItem?.classList.remove('menu-item-active');
              gsap.set(otherMenuItem, {zIndex: 1});
            }
            return otherMenuItemBody;
          }), {height: 0});
          gsap.set(menuItem, {zIndex: 2});
          gsap.to(subMenu, {height: 'auto'});
        }
      });
    });
    openMegaMenuICon.forEach((icon) => {
      const ParentOfMegaMenu = icon.closest('.menu-item-has-megamenu');
      icon?.addEventListener('click', (e) => {
        ParentOfMegaMenu.classList.add('menu-item-active');
      });
    });
    btnBack.forEach((backButton) => {
      const ParentOfMegaMenu = backButton.closest('.menu-item-has-megamenu');
      backButton?.addEventListener('click', function (e) {
        e.stopPropagation();
        ParentOfMegaMenu.classList.remove('menu-item-active');
      });
    });
  }


  if (mobileMedia.matches) {
    OPenMenu();
  }


  if (languageMobile && mobileMedia.matches) {
    languageMobile.addEventListener('click', function () {
      const subMenu = languageMobile.querySelector('.sub-menu');
      const isOpened = languageMobile?.classList.toggle('menu-item-active');
      if (!isOpened) {
        gsap.to(subMenu, {height: 0});
      } else {
        gsap.set(languageMobile, {zIndex: 2});
        gsap.to(subMenu, {height: 'auto'});
      }

    });
  }
  // endregion open sub menu in responsive


  // const langSwitchers = header.querySelectorAll('.lang-switcher');
  // for (let langSwitcher of langSwitchers) {
  //   langSwitcher.addEventListener('click', function (e) {
  //     e.preventDefault();
  //     window.location.href = window.location.origin + window.location.pathname + langSwitcher.dataset.lang;
  //   });
  // }
  const sticky = "sticky";
  const scrollUp = 'scroll-up';
  const hasBanner = 'has-banner';

  let lastScroll = 0;
  window.addEventListener("scroll", () => {
    const currentScroll = window.scrollY;
    if (currentScroll <= 50) {
      header.classList.add(scrollUp)
      header.classList.add(hasBanner)
      header.classList.remove(sticky);
    } else {
      header.classList.remove(scrollUp);
      header.classList.remove(hasBanner);
      header.classList.toggle(sticky, currentScroll > lastScroll);
    }
    lastScroll = currentScroll;
  });


  animations(header);
  imageLazyLoading(header);
};
windowOnLoad(blockScript);


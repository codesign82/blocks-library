<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <?php
  $code_before_body_tag = get_field( 'code_before_body_tag', 'options' );
  echo $code_before_body_tag;
  ?>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="description" content="<?php if (is_single()) {
    single_post_title('', true);
  } else {
    bloginfo('name');
    echo " - ";
    bloginfo('description');
  } ?>"/>
  <meta
    content="width=device-width, initial-scale=1.0, maximum-scale=5, minimum-scale=1.0"
    name="viewport">
  <meta content="ie=edge" http-equiv="X-UA-Compatible">
  <script>
    const BiggerThanDesignWidth = 1680;
    const designWidth = 1440;
    const desktop = 1440;
    const tablet = 992;
    const mobile = 600;
    const sMobile = 375;
    const resizeHandler = function () {
      if (window.innerWidth >= BiggerThanDesignWidth) {
        document.documentElement.style.fontSize = `${10}px`;
        // document.documentElement.style.fontSize = `${9 * window.innerWidth / desktop}px`;
      } else if (window.innerWidth < BiggerThanDesignWidth && window.innerWidth >= desktop) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / desktop}px`;
      } else if (window.innerWidth < desktop && window.innerWidth >= tablet) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / designWidth}px`;
      } else if (window.innerWidth < tablet && window.innerWidth >= mobile) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / tablet}px`;
      } else if (window.innerWidth < mobile && window.innerWidth >= sMobile) {
        document.documentElement.style.fontSize = `${10}px`;
      } else {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / sMobile}px`;
      }
    };
    resizeHandler();
    window.addEventListener('resize', resizeHandler);
  </script>
  <style>
    /* latin */
    @font-face {
      font-family: 'Macklin Sans';
      src: url('<?=get_template_directory_uri() .'/fonts/macklinlight.woff' ?>') format('woff');
      font-weight: 400;
      font-style: normal;
      font-display: swap;
    }

    @font-face {
      font-family: 'Space Grotesk';
      src: url('<?=get_template_directory_uri() .'/fonts/SpaceGrotesk-Regular.woff' ?>') format('woff');
      font-weight: 400;
      font-style: normal;
      font-display: swap;
    }

    @font-face {
      font-family: 'Space Grotesk';
      src: url('<?=get_template_directory_uri() .'/fonts/SpaceGrotesk-SemiBold.woff' ?>') format('woff');
      font-weight: 600;
      font-style: normal;
      font-display: swap;
    }
  </style>
  <?php wp_head(); ?>
</head>
<!--preloader style-->
<style>
  body:not(.loaded) {
    opacity: 0;
  }

  body:not(.loaded) * {
    transition: none !important;
  }

  body {
    transition: opacity .5s;
  }
</style>
<!--end preloader style-->
<!-- ACF Fields -->

<?php
$container_width = get_field('container_width', 'options');
$is_logo_is_png = get_field('is_logo_is_png', 'options');
$logo = get_field('header_logo', 'options');
$svg = get_field('svg', 'options');
$location_details = get_field('location_details', 'options');
$location_title = @$location_details['title'];
$location = @$location_details['location'];
$email_details = get_field('email_details', 'options');
$email_title = @$email_details['title'];
$email = @$email_details['email'];
$phone_number_details = get_field('phone_number_details', 'options');
$phone_number_title = @$phone_number_details['title'];
$phone_number = @$phone_number_details['phone_number'];
?>
<?php if ($container_width) { ?>
  <style>
    @media screen and (min-width: 1650px) {
      .container, [class*="g-container"] {
      } {
      max-width: <?=$container_width?> !important;
    }
    }
  </style>
<?php } ?>

<!-- END ACF -->
<body <?php body_class(); ?> data-barba="wrapper">
<?php
$code_after_body_tag = get_field( 'code_after_body_tag', 'options' );
echo $code_after_body_tag;
?>
<main>
  <header class="logo-light burger-light">
    <div style="display: none;" class="preloader" >
      <svg preserveAspectRatio="none" data-name="Layer 1"
           style="pointer-events: none;"
           viewBox="0 0 588 384">
        <defs>
          <clipPath>
            <rect x="-623.04" y="366.18" width="588" height="384" fill="none"/>
          </clipPath>
        </defs>
        <path class="path"
          d="M444.92,206.69l-82-82s-18.69-19.38-41.19-19.38S286.08,113.6,274,125.71s-38.75,38.42-38.75,38.42,20-15.23,43.55-15.23,35,11.42,40.15,15.91,41.53,41.54,41.53,41.54Z"
          fill="none" stroke="#1d4070" stroke-miterlimit="10"/>
        <path class="path2"
          d="M143.08,177.31l82,82s18.69,19.38,41.19,19.38,35.64-8.31,47.76-20.42,38.75-38.41,38.75-38.41-20,15.22-43.55,15.22-35-11.42-40.15-15.91-41.53-41.54-41.53-41.54Z"
          fill="none" stroke="#82fa81" stroke-miterlimit="10"/>
        <path class="path3"
          d="M268.08,174.69s24.68,3.37,39.63,9.78c23,9.82,45.08,27.36,45.08,27.36s-23.14-13-36.05-18.87C297.62,184.26,268.08,174.69,268.08,174.69Z"
          fill="#1d4070"/>
      </svg>
    </div>
    <div class="header-wrapper">
      <a href="<?= site_url(); ?>" class="header-logo hidden-animation">
        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
             viewBox="0 0 55.7 32">
          <path id="green"
                d="M55.7,18.7,40.6,3.6S37.2,0,33,0a11,11,0,0,0-8.8,3.8C21.9,6,17,10.9,17,10.9a15,15,0,0,1,8-2.8A10.25,10.25,0,0,1,32.4,11c1,.8,7.7,7.7,7.7,7.7Z"
                fill="#82fa81"/>
          <path id="blue"
                d="M0,13.3,15.1,28.4S18.5,32,22.7,32a11,11,0,0,0,8.8-3.8c2.2-2.3,7.2-7.1,7.2-7.1A14.83,14.83,0,0,1,30.6,24a10,10,0,0,1-7.4-2.9c-1-.8-7.7-7.7-7.7-7.7Z"
                fill="#1d4070"/>
          <path
            d="M20.7,12.8A48.22,48.22,0,0,1,28,14.7a49.41,49.41,0,0,1,8.4,4.9s-4.3-2.4-6.7-3.5C26.2,14.6,20.7,12.8,20.7,12.8Z"
            fill="#1d4070"/>
        </svg>
        <svg
          data-name="Layer 1"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 96 23.9"
          id="logoText"
        >
          <defs>
            <linearGradient id="myGradient" x1="0%" y1="0%" x2="0%" y2="100%">
              <stop offset="0%" class="logo-stop" stop-color="#fff"/>
              <stop offset="49.99999%" class="logo-stop" stop-color="#fff"/>
              <stop offset="50%" class="logo-stop" stop-color="#fff"/>
              <stop offset="100%" class="logo-stop" stop-color="#fff"/>
            </linearGradient>
          </defs>

          <g fill="url('#myGradient')">
            <path
              id="logoTextPath"
              d="M3.1,12.6c0,2.5,1.6,3.9,3.5,3.9a3,3,0,0,0,3.1-2.3l2.6,1a5.82,5.82,0,0,1-5.8,4.1A6.47,6.47,0,0,1,0,12.6,6.31,6.31,0,0,1,6.4,6a5.51,5.51,0,0,1,5.7,4.1l-2.7,1a2.9,2.9,0,0,0-3-2.3C4.8,8.7,3.1,10.1,3.1,12.6Zm12,11.3,3-6.6-5.3-11h3.4l3.6,7.9,3.4-7.9h3.2l-8,17.6Zm12.9-5V.3h2.9V7.9A4.49,4.49,0,0,1,34.8,6c3.8,0,5.8,2.9,5.8,6.5,0,3.8-2.2,6.6-5.9,6.6a4.47,4.47,0,0,1-3.9-2v1.7ZM34.3,8.7c-1.9,0-3.4,1.4-3.4,3.9s1.5,4,3.4,4,3.4-1.5,3.4-4S36.3,8.7,34.3,8.7Zm20,6.6a5.66,5.66,0,0,1-5.7,4,6.33,6.33,0,0,1-6.4-6.7A6.25,6.25,0,0,1,48.3,6c3.9,0,6.1,2.5,6.1,6.5a3.75,3.75,0,0,1-.1,1H45.1a3.26,3.26,0,0,0,1,2.3,3.41,3.41,0,0,0,2.4.9,3,3,0,0,0,3.1-2.2Zm-2.9-4a2.82,2.82,0,0,0-3.1-2.8,3.23,3.23,0,0,0-2.1.8,3,3,0,0,0-1,2Zm12.8-2a3.08,3.08,0,0,0-1-.1c-2.3,0-3.4,1.3-3.4,3.7v6h-3V6.4h2.9v2a3.78,3.78,0,0,1,3.6-2.2,2.2,2.2,0,0,1,.8.1Zm5.1,9.6h-3V.3h3V7.6A4.51,4.51,0,0,1,72.9,6c3.1,0,4.6,2.2,4.6,5v7.9h-3V11.5c0-1.5-.7-2.8-2.6-2.8-1.7,0-2.5,1.2-2.6,2.8ZM81.6,0c.3,0,.5.1.7.1a.82.82,0,0,1,.6.4,2.09,2.09,0,0,1,.4.6,1.7,1.7,0,0,1,.1.7,2,2,0,0,1-.3,1.1,1.6,1.6,0,0,1-.9.7,2.08,2.08,0,0,1-1.1.1,3.13,3.13,0,0,1-1-.5,1.39,1.39,0,0,1-.5-1,2,2,0,0,1,.1-1.1,1.6,1.6,0,0,1,.7-.9A6,6,0,0,1,81.6,0ZM80.2,18.9V6.4h3V18.9Zm6.4,0V.3h3V18.9Zm6.4,0V.3h3V18.9Z"
            />
          </g>
        </svg>
      </a>
      <div id="burger-menu" class="menu hidden-animation">
        <div class="bg"></div>
        <span id="menu-shape-1" class="menu-shape"></span>
        <span id="menu-shape-2" class="menu-shape"></span>
        <span id="menu-shape-3" class="menu-shape"></span>
        <span id="menu-shape-4" class="menu-shape"></span>
        <div id="burger-menu-circle" class="burger-menu-circle"></div>
        <svg id="burger-menu-border" xmlns="http://www.w3.org/2000/svg"
             width="100" height="100" viewBox="0 0 100 100">
          <defs>
            <linearGradient id="gradient" x1="0%" y1="0%" x2="0%" y2="100%">
              <stop offset="0%" class="burger-stop" stop-color="#fff"/>
              <stop offset="50%" class="burger-stop" stop-color="#fff"/>
              <stop offset="50%" class="burger-stop" stop-color="#fff"/>
              <stop offset="100%" class="burger-stop" stop-color="#fff"/>
            </linearGradient>
          </defs>
          <circle cx="50" cy="50" r="47" stroke="url(#gradient)"
                  stroke-width="2" id="circle" fill="none"/>
        </svg>
        <!--        <div id="burger-menu-border"></div>-->
      </div>
    </div>
    <div class="container">
      <nav>
        <?php if (have_rows('menu_links', 'options')) { ?>
          <ul class="link-wrapper sub-menu-item reset-ul">
            <?php while (have_rows('menu_links', 'options')) {
              the_row();
              $link = get_sub_field('link');
              $is_has_sub_menu = get_sub_field('is_has_sub_menu');
              $first_submenu_title = get_sub_field('first_submenu_title');
              $second_submenu_title = get_sub_field('second_submenu_title');
              ?>
              <?php if ($link) { ?>
                <li
                  class="menu-item <?= $is_has_sub_menu ? 'menu-item-has-children' : '' ?>">
                  <a class="headline-2" href="<?= $link['url'] ?>"
                     target="<?= $link['target'] ?>">
                    <?= $link['title'] ?>
                    <?php if ($is_has_sub_menu) { ?>
                      <div class="primary-button-only-arrow">
                        <svg class="arrow" width="25" height="15"
                             viewBox="0 0 25 15"
                             fill="none">

                          <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M16.2524 1.29418L21.9363 6.89262L0.515991 6.89262V8.01564L22.0294 8.01564L16.2524 13.7058L17.0583 14.5L23.359 8.29398L23.366 8.3009L24.1719 7.50671L24.1651 7.49999L24.1719 7.49327L23.366 6.69907L23.359 6.706L17.0583 0.499981L16.2524 1.29418Z"
                                fill="#82FA81"/>
                        </svg>
                      </div>
                    <?php } ?>
                  </a>
                  <?php if ($is_has_sub_menu) { ?>
                    <div class="arrow-down">
                      <svg width="10" height="6" viewBox="0 0 10 6"
                           fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M5 6L0 0.999995L0.7 0.299995L5 4.6L9.3 0.299995L10 0.999995L5 6Z"
                          fill="white"/>
                      </svg>
                    </div>
                  <?php } ?>
                  <?php if ($is_has_sub_menu && have_rows('first_submenu_links', 'options') || have_rows('second_submenu_links', 'options')) { ?>
                    <div class="sub-menu">
                      <div class="spacer">
                        <?php if (have_rows('first_submenu_links', 'options')) { ?>
                          <ul class="hover-content sub reset-ul ">
                            <?php if ($first_submenu_title) { ?>
                              <li
                                class="headline-6 links-title"><?= $first_submenu_title ?></li>
                            <?php } ?>
                            <?php while (have_rows('first_submenu_links', 'options')) {
                              the_row();
                              $link = get_sub_field('link'); ?>
                              <?php if ($link) { ?>
                                <li class="paragraph paragraph-16 hover-link">
                                  <a href="<?= $link['url'] ?>"
                                     target="<?= $link['target'] ?>"><?= $link['title'] ?></a>
                                </li>
                              <?php } ?>
                            <?php } ?>
                          </ul>
                        <?php } ?>
                        <?php if (have_rows('second_submenu_links', 'options')) { ?>
                          <ul
                            class="hover-content hover-content-2 sub reset-ul">
                            <?php if ($second_submenu_title) { ?>
                              <li
                                class="headline-6 links-title"><?= $second_submenu_title ?></li>
                            <?php } ?>
                            <?php while (have_rows('second_submenu_links', 'options')) {
                              the_row();
                              $link = get_sub_field('link'); ?>
                              <?php if ($link) { ?>
                                <li class="paragraph paragraph-16 hover-link">
                                  <a href="<?= $link['url'] ?>"
                                     target="<?= $link['target'] ?>"><?= $link['title'] ?></a>
                                </li>
                              <?php } ?>
                            <?php } ?>
                          </ul>
                        <?php } ?>
                      </div>
                    </div>
                  <?php } ?>
                </li>
              <?php } ?>
            <?php } ?>
          </ul>
        <?php } ?>
        <ul class="sub-menu-item reset-ul">
          <li class="menu-item-two">
            <div class="header-more-links">
              <?php if ($location_title && $location) { ?>
                <ul class="hover-content sub-menu-item reset-ul">
                  <li class="headline-6 links-title"><?= $location_title ?></li>
                  <li
                    class="paragraph paragraph-16 hover-link"><?= $location ?></li>
                </ul>
              <?php } ?>
              <?php if ($email_title && $email) { ?>
                <ul class="hover-content sub-menu-item reset-ul">
                  <li class="headline-6 links-title"><?= $email_title ?></li>
                  <li class="paragraph paragraph-16 hover-link"><a
                      href="mailto:<?= $email ?>"><?= $email ?></a>
                  </li>
                </ul>
              <?php } ?>
              <?php if ($phone_number_title && $phone_number) { ?>
                <ul class="hover-content sub-menu-item reset-ul">
                  <li
                    class="headline-6 links-title"><?= $phone_number_title ?></li>
                  <li class="paragraph paragraph-16 hover-link"><a
                      href="tel:<?= $phone_number ?>"><?= $phone_number ?></a>
                  </li>
                </ul>
              <?php } ?>
            </div>
          </li>
        </ul>
      </nav>
    </div>
  </header>

  <div class="page-transition">

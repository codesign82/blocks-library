import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/advisorhub';
import Swiper, {Autoplay} from "swiper";

const blockScript = async (container = document) => {
  const header = container.querySelector('header');

  const burgerMenu = header.querySelector('.burger-menu');
  const menuLinks = header.querySelector('.ah-navbar');
  const items = header.querySelectorAll('.menu-item');
  const text = header.querySelectorAll('.text');
  const searchIcon = header.querySelector('.open-input-search');
  const searchContentWrapper = header.querySelector('.search-content-wrapper');
  const searchBoxWrapper = header.querySelector('.search-box-wrapper');
  const inputSearch = header.querySelector('.input-search');
  const closeIcon = header.querySelector('.close-icon');
  const headerWrapper = header.querySelector('.header-wrapper');
  const mainLogo = header.querySelector('.main-logo');

  //region swiper for ticker
  let slideSpeed = header.querySelector('[data-speed]')?.dataset.speed;
  let slideDelay = header.querySelector('[data-transition]')?.dataset.transition;

  let swiper = new Swiper(header.querySelector('.ticker-news'), {
    direction: 'vertical',
    slidesPerView: 1,
    spaceBetween: 30,
    mousewheel: true,
    grabCursor: true,
    loop: true,
    speed: +slideSpeed, //slide speed
    modules: [Autoplay],
    autoplay: {
      delay: +slideDelay, // transition speed
      disableOnInteraction: false,
      pauseOnMouseEnter: true,
    },
  });
//endregion

  // region open and close the search
  function OpenORCloseSearch(check) {
    if (check === 'true') {
      searchContentWrapper?.classList.add('active');
      searchIcon?.classList.add('active');
      closeIcon?.classList.add('active');
      for (let menuLink of items) {
        menuLink.classList.add('disabled-when-open-search');
      }
    } else {
      searchIcon?.classList.remove('active');
      closeIcon?.classList.remove('active');
      searchContentWrapper?.classList.remove('active');
      for (let menuLink of items) {
        menuLink.classList.remove('disabled-when-open-search');
      }
    }
  }

  searchIcon?.addEventListener('click', function (e) {
    OpenORCloseSearch('true');
    e.stopPropagation();
  });
  closeIcon?.addEventListener('click', function () {
    OpenORCloseSearch('false');
  });

  searchBoxWrapper?.addEventListener('click', function () {
    inputSearch.focus();
  });

  searchContentWrapper?.addEventListener('click', function (e) {
    e.stopPropagation();
  });

  window.addEventListener('click', function () {
    OpenORCloseSearch(false)
  })

  // endregion

// Execute a function when the user releases a key on the keyboard
  inputSearch?.addEventListener("keyup", function (event) {
    if (event.keyCode === 13) {

    }
  });

  // region burger menu animation
  if (!burgerMenu) return;
  const burgerTl = gsap.timeline({paused: true});
  const burgerSpans = burgerMenu.querySelectorAll('span');
  gsap.set(burgerSpans, {transformOrigin: 'center'});
  burgerTl
      .to(burgerSpans, {
        y: gsap.utils.wrap([`0.6rem`, 0, `-0.6rem`]),
        duration: 0.25,
      })
      .set(burgerSpans, {autoAlpha: gsap.utils.wrap([1, 0, 1])})
      .to(burgerSpans, {rotation: gsap.utils.wrap([45, 0, -45])})
      .set(burgerSpans, {rotation: gsap.utils.wrap([45, 0, 135])});


  window.addEventListener("scroll", () => {

    if (jQuery(header).offset().top < window.scrollY) {
      header.classList.add('sticky');
    } else {
      header.classList.remove('sticky');
    }


  });

  const headerAD = header.previousElementSibling;
  burgerMenu?.addEventListener('click', function () {
    if (burgerMenu.classList.contains('burger-menu-active')) {
      // region allow page scroll
      document.documentElement.classList.remove('modal-opened');
      // endregion allow page scroll
      burgerMenu?.classList.remove('burger-menu-active');
      menuLinks?.classList.remove('header-links-active');
      header?.classList.remove('header-active');
      // hide header hidden logo
      // mainLogo.style.opacity = 0;
      headerAD.classList.contains('container-gray') && $(headerAD).slideDown(200)
      burgerTl?.reverse();
    } else {
      burgerMenu?.classList.add('burger-menu-active');
      menuLinks?.classList.add('header-links-active');
      header?.classList.add('header-active');
      // show header hidden logo
      // mainLogo.style.opacity = 1;
      burgerTl?.play();
      headerAD.classList.contains('container-gray') && $(headerAD).slideUp(200)
      // region prevent page scroll
      document.documentElement.classList.add('modal-opened');
      // endregion prevent page scroll
      gsap.fromTo(menuLinks.querySelectorAll('.menu-item'), {
        y: 30,
        autoAlpha: 0,
      }, {
        y: 0,
        autoAlpha: 1,
        stagger: .05,
        duration: .4,
        delay: .5,
      });
    }
  });

  // endregion

  // region open sub menu in responsive
  const menuItems = header.querySelectorAll('.menu-item-has-children');
  const mobileMedia = window.matchMedia('(max-width: 992px)');
  menuItems.forEach((menuItem) => {
    const menuItemBody = menuItem.querySelector('.sub-menu');
    menuItem?.addEventListener('click', (e) => {

      if (!mobileMedia.matches || !menuItemBody || e.target.classList.contains('header-link') || e.target.closest('.sub-menu,.menu-item-in-sub-menu a')) return;
      const isOpened = menuItem?.classList.toggle('menu-item-active');
      if (!isOpened) {
        gsap.to(menuItemBody, {height: 0});
      } else {
        gsap.to(Array.from(menuItems).map(otherMenuItem => {
          const otherMenuItemBody = otherMenuItem.querySelector('.sub-menu');
          if (otherMenuItemBody && menuItem !== otherMenuItem) {
            otherMenuItem?.classList.remove('menu-item-active');
            gsap.set(otherMenuItem, {zIndex: 1});
          }
          return otherMenuItemBody;
        }), {height: 0});
        gsap.set(menuItem, {zIndex: 2});
        gsap.to(menuItemBody, {height: 'auto'});
      }
    });
  });


  // endregion open sub menu in responsive

  for (let item of items) {
    item.addEventListener('mouseenter', function (e) {
      if (item.classList.contains('current-menu-item')) return;
      for (let i = 0; i < items.length; i++) {
        items[i].classList.add('disabled');
      }
      item.classList.remove('disabled');
    })

    item.addEventListener('mouseleave', function (e) {
      if (item.classList.contains('current-menu-item')) return;
      for (let i = 0; i < items.length; i++) {
        items[i].classList.remove('disabled');
      }
      item.classList.remove('disabled');
    })
  }


  // const enableTransition = debounce(() => header.classList.remove('no-transition'), 500);
  // window.addEventListener("resize", resizeHandler);
  // resizeHandler();
  //
  // function resizeHandler() {
  //   if (window.innerWidth >= 992) {
  //     enableTransition();
  //   } else {
  //     header.classList.add('no-transition')
  //   }
  // }



  animations(header);
  imageLazyLoading(header);
};
windowOnLoad(blockScript);

/**
 * Call this function to update the logo colors according to the underlying element
 * @param element {HTMLElement} this element will be used to control the logo's colors
 */
export const updateColors = element => {
  const event = new CustomEvent('header-update-colors', {detail: element})
  window.dispatchEvent(event);
}

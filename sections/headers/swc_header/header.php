<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="description" content="<?php if ( is_single() ) {
    single_post_title( '', true );
  } else {
    bloginfo( 'name' );
    echo " - ";
    bloginfo( 'description' );
  } ?>"/>
  <meta
    content="width=device-width, initial-scale=1.0, maximum-scale=5, minimum-scale=1.0"
    name="viewport">
  <meta content="ie=edge" http-equiv="X-UA-Compatible">
  <script>
    const BiggerThanDesignWidth = 1650;
    const designWidth = 1440;
    const desktop = 1440;
    const tablet = 992;
    const mobile = 600;
    const sMobile = 375;
    const resizeHandler = function () {
      if (window.innerWidth >= BiggerThanDesignWidth) {
        document.documentElement.style.fontSize = `${11}px`;
        // document.documentElement.style.fontSize = `${9 * window.innerWidth / desktop}px`;
      } else if (window.innerWidth < BiggerThanDesignWidth && window.innerWidth >= desktop) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / desktop}px`;
      } else if (window.innerWidth < desktop && window.innerWidth >= tablet) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / designWidth}px`;
      } else if (window.innerWidth < tablet && window.innerWidth >= mobile) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / tablet}px`;
      } else if (window.innerWidth < mobile && window.innerWidth >= sMobile) {
        document.documentElement.style.fontSize = `${10}px`;
      } else {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / sMobile}px`;
      }
    };
    resizeHandler();
    window.addEventListener('resize', resizeHandler);
  </script>
  <style>
    /* latin */
    @font-face {
      font-family: 'Open Sans';
      font-style: normal;
      font-weight: 400;
      font-display: swap;
      src: url(<?=get_template_directory_uri() ?>/fonts/mem8YaGs126MiZpBA-UFVZ0b.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    /* latin */
    @font-face {
      font-family: 'Open Sans';
      font-style: italic;
      font-weight: 400;
      font-display: swap;
      src: url(<?=get_template_directory_uri() ?>/fonts/mem6YaGs126MiZpBA-UFUK0Zdc0.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    /* latin */
    @font-face {
      font-family: 'Open Sans';
      font-style: normal;
      font-weight: 600;
      font-display: swap;
      src: url(<?=get_template_directory_uri() ?>/fonts/mem5YaGs126MiZpBA-UNirkOUuhp.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    /* latin */
    @font-face {
      font-family: 'Open Sans';
      font-style: normal;
      font-weight: 700;
      font-display: swap;
      src: url(<?=get_template_directory_uri() ?>/fonts/mem5YaGs126MiZpBA-UN7rgOUuhp.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    /* latin */
    @font-face {
      font-family: 'Crimson Text';
      font-style: normal;
      font-weight: 400;
      font-display: swap;
      src: url(<?=get_template_directory_uri() ?>/fonts/wlp2gwHKFkZgtmSR3NB0oRJfbwhT.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    /* latin */
    @font-face {
      font-family: 'Crimson Text';
      font-style: normal;
      font-weight: 600;
      font-display: swap;
      src: url(<?=get_template_directory_uri() ?>/fonts/wlppgwHKFkZgtmSR3NB0oRJXsCxGDNNQ.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    /* latin */
    @font-face {
      font-family: 'Crimson Text';
      font-style: italic;
      font-weight: 600;
      font-display: swap;
      src: url(<?=get_template_directory_uri() ?>/fonts/wlprgwHKFkZgtmSR3NB0oRJfajCOD-NS_LU.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    /* latin */
    @font-face {
      font-family: 'Crimson Text';
      font-style: italic;
      font-weight: 400;
      font-display: swap;
      src: url(<?=get_template_directory_uri() ?>/fonts/wlpogwHKFkZgtmSR3NB0oRJfajhRK_Y.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    /* latin */
    @font-face {
      font-family: 'Crimson Text';
      font-style: normal;
      font-weight: 700;
      font-display: swap;
      src: url(<?=get_template_directory_uri() ?>/fonts/wlppgwHKFkZgtmSR3NB0oRJX1C1GDNNQ.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }
  </style>
  <?php wp_head(); ?>
</head>
<!--preloader style-->
<style>
  body:not(.loaded) {
    opacity: 0;
  }

  body {
    transition: opacity .5s;
  }
</style>
<!--end preloader style-->
<!-- ACF Fields -->
<?php
$header_logo_black      = get_field( 'header_logo_black', 'options' );
$header_logo_white      = get_field( 'header_logo_white', 'options' );
$header_logo_black_svg  = get_field( 'header_logo_black_svg', 'options' );
$header_logo_white_svg  = get_field( 'header_logo_white_svg', 'options' );
$is_the_logo_png_or_svg = get_field( 'is_the_logo_png_or_svg', 'options' );
$container_width        = get_field( 'container_width', 'options' );
$top_menu               = get_field( 'top_menu', 'options' );
$support_icon           = acf_icon( $top_menu['support_icon'] );
$search_icon            = acf_icon( $top_menu['search_icon'] );
$is_home_header         = get_field( 'is_home_header', get_the_ID() );
$is_home_header         = $is_home_header ? 'home-header' : '';
?>
<?php if ( $container_width ) { ?>
  <style>
    @media screen and (min-width: 1650px) {
      .container, .two_columns_block {
        max-width: <?=$container_width?> !important;
      }
    }
  </style>
<?php } ?>
<!-- END ACF -->
<body <?php body_class(); ?> data-barba="wrapper">
<!--  <div class="preloader"></div>-->
<main>
  <header>
    <div class="menu-top-menu-container">
        <span class="text">
          <?= $top_menu['menu_text'] ?>
        </span>
      <?php
      wp_nav_menu( array(
        'theme_location' => 'top-menu',
        'menu_class'     => 'top-menu reset-ul',
        'container'      => '',
      ) );
      ?>
      <div class="support-and-search">
        <a class="icon-and-text text"
           href="<?= $top_menu['support_link']['url'] ?>"
           target="<?= $top_menu['support_link']['target'] ?>">
          <?= $support_icon ?>
          <?= $top_menu['support_link']['title'] ?>
        </a>
        <a href="<?= site_url() ?>?s= " class="icon-and-text text search" id="search">
          <?= $search_icon ?>
          <?= $top_menu['search_text'] ?>
        </a>
      </div>
    </div>
    <div class="bottom-menu">
      <a class="header-logo" href="<?= site_url() ?>" title="Home">
        <?php if ( $is_the_logo_png_or_svg === 'png' ) { ?>
          <img class="header-logo-white" data-src="<?= $header_logo_white['url'] ?>"
               alt="<?= $header_logo_white['alt'] ?>">
          <img class="header-logo-dark" data-src="<?= $header_logo_black['url'] ?>"
               alt="<?= $header_logo_black['alt'] ?>">
        <?php } elseif ( $is_the_logo_png_or_svg === 'svg' ) { ?>
          <div class="header-logo-white"><?= $header_logo_white_svg ?></div>
          <div class="header-logo-dark"><?= $header_logo_black_svg ?></div>
        <?php } ?>
      </a>

      <!--          burger menu and cross-->
      <div class="burger-and-menu">
        <div class="menu">
          <button aria-label="burger" class="burger-menu" id="burger-menu">
            <span></span>
            <span></span>
            <span></span>
          </button>
        </div>
      </div>

      <nav class="navbar">
        <div class="navbar-wrapper">
          <?php
          wp_nav_menu( array(
            'theme_location' => 'primary-menu',
            'menu_class'     => 'primary-menu reset-ul',
            'container'      => ''
          ) );
          ?>
          <?php
          wp_nav_menu( array(
            'theme_location' => 'top-menu',
            'menu_class'     => 'top-menu reset-ul',
            'container'      => ''
          ) );
          ?>
        </div>
      </nav>
    </div>
  </header>
  <div class="page-transition">

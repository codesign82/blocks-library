import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import Swiper from "swiper";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/wrap';

const blockScript = async (container = document) => {
  const header = container.querySelector('header');
  
  // hover on links has megamenu
  
  const headerLinksWrapper = header.querySelector('.header-links');
  const headerLogo = header.querySelector('.header-logo');
  const headerLinks = header.querySelectorAll('.header-link.has-megamenu');
  const burgerMenu = header.querySelector('.burger-menu');
  const menuLinks = header.querySelector('.header-links');
  const burgerTl = gsap.timeline({paused: true});
  const desktopMedia = window.matchMedia('(min-width: 992px)')
  
  const linkMouseOver = (headerLink, headerLinkmegamenu) => {
    headerLink.classList.add('header-link-active');
    headerLinkmegamenu.classList.add('megamenu-active');
    headerLinksWrapper.classList.add('header-links-active');
    headerLogo.classList.add('header-logo-active');
  }
  
  const linkMouseOut = (headerLink, headerLinkmegamenu) => {
    headerLink.classList.remove('header-link-active');
    headerLinkmegamenu.classList.remove('megamenu-active');
    headerLinksWrapper.classList.remove('header-links-active');
    headerLogo.classList.remove('header-logo-active');
  }
  
  function mediaChecker() {
    if (desktopMedia.matches) {
      for (let headerLink of headerLinks) {
        const headerLinkmegamenu = headerLink.querySelector('.megamenu');
        headerLink.onmouseover = () => linkMouseOver(headerLink, headerLinkmegamenu);
        headerLink.onmouseout = () => linkMouseOut(headerLink, headerLinkmegamenu);
      }
      let megamenus = header.querySelectorAll('.megamenu');
      if (megamenus) {
        for (let megamenu of megamenus) {
          new Swiper(megamenu, {
            slidesPerView: 6,
            spaceBetween: 20,
            // autoplay: {
            //   delay: 1000,
            //   stopOnLast: true,
            // }
          })
        }
      }
      burgerMenu.classList.remove('burger-menu-active');
      menuLinks.classList.remove('header-links-active');
      header.classList.remove('header-active');
      burgerTl.reverse();
    } else {
      for (let headerLink of headerLinks) {
        headerLink.onmouseover = null;
        headerLink.onmouseout = null;
      }
    }
  }
  
  mediaChecker();
  desktopMedia.addEventListener('change', mediaChecker)
  
  
 
  
  if (!burgerMenu) return;
  const burgerSpans = burgerMenu.querySelectorAll('span');
  gsap.set(burgerSpans, {transformOrigin: 'center'});
  burgerTl
      .to(burgerSpans, {
        y: gsap.utils.wrap([`0.95rem`, 0, `-0.95rem`]),
        duration: 0.25,
      })
      .set(burgerSpans, {autoAlpha: gsap.utils.wrap([1, 0, 1])})
      .to(burgerSpans, {rotation: gsap.utils.wrap([45, 0, -45])})
      .set(burgerSpans, {rotation: gsap.utils.wrap([45, 0, 135])});
  burgerMenu.addEventListener('click', function () {
    if (burgerMenu.classList.contains('burger-menu-active')) {
      // allowPageScroll()
      burgerMenu.classList.remove('burger-menu-active');
      menuLinks.classList.remove('header-links-active');
      header.classList.remove('header-active');
      burgerTl.reverse();
    } else {
      burgerMenu.classList.add('burger-menu-active');
      menuLinks.classList.add('header-links-active');
      header.classList.add('header-active');
      burgerTl.play();
      // preventPageScroll();
      gsap.fromTo([menuLinks.querySelectorAll('.header-link '), menuLinks.querySelector('.header-link-contact')], {
        y: 30,
        autoAlpha: 0,
      }, {
        y: 0,
        autoAlpha: 1,
        stagger: .05,
        duration: .4,
        delay: .5,
        clearProps: true,
      });
    }
  });
  
  
  // region open sub menu in responsive
  const mobileMedia = window.matchMedia('(max-width: 992px)');
  headerLinks.forEach((menuItem) => {
    const menuItemBody = menuItem.querySelector('.megamenu');
    menuItem?.addEventListener('click', (e) => {
      if (!mobileMedia.matches) return;
      if (!menuItemBody) {
        return;
      }
      const isOpened = menuItem?.classList.toggle('active-header-link');
      if (!isOpened) {
        gsap.to(menuItemBody, {height: 0});
      } else {
        gsap.to(Array.from(headerLinks).map(otherMenuItem => {
          const otherMenuItemBody = otherMenuItem.querySelector('.megamenu');
          if (otherMenuItemBody && menuItem !== otherMenuItem) {
            otherMenuItem?.classList.remove('active-header-link');
            gsap.set(otherMenuItem, {zIndex: 1});
          }
          return otherMenuItemBody;
        }), {height: 0});
        gsap.set(menuItem, {zIndex: 2});
        gsap.to(menuItemBody, {height: 'auto'});
      }
    });
  });
  // endregion open sub menu in responsive
  
  
  
  header.classList.toggle('header-sticky', window.scrollY >= 20)
  window.addEventListener('scroll', function () {
    header.classList.toggle('header-sticky', window.scrollY >= 20)
  });
  
  
  
  animations(header);
  imageLazyLoading(header);
};
windowOnLoad(blockScript);




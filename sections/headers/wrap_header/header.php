<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="description" content="<?php if (is_single()) {
    single_post_title('', true);
  } else {
    bloginfo('name');
    echo " - ";
    bloginfo('description');
  } ?>"/>
  <meta
    content="width=device-width, initial-scale=1.0, maximum-scale=5, minimum-scale=1.0"
    name="viewport">
  <meta content="ie=edge" http-equiv="X-UA-Compatible">
  <script>
    const BiggerThanDesignWidth = 1650;
    const designWidth = 1440;
    const desktop = 1440;
    const tablet = 992;
    const mobile = 600;
    const sMobile = 375;
    const resizeHandler = function () {
      if (window.innerWidth >= BiggerThanDesignWidth) {
        document.documentElement.style.fontSize = `${10}px`;
        // document.documentElement.style.fontSize = `${9 * window.innerWidth / desktop}px`;
      } else if (window.innerWidth < BiggerThanDesignWidth && window.innerWidth >= desktop) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / desktop}px`;
      } else if (window.innerWidth < desktop && window.innerWidth >= tablet) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / designWidth}px`;
      } else if (window.innerWidth < tablet && window.innerWidth >= mobile) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / tablet}px`;
      } else if (window.innerWidth < mobile && window.innerWidth >= sMobile) {
        document.documentElement.style.fontSize = `${10}px`;
      } else {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / sMobile}px`;
      }
    };
    resizeHandler();
    window.addEventListener('resize', resizeHandler);
  </script>
  <style>
    @font-face {
      font-family: 'DIN Pro';
      src: url('<?=get_template_directory_uri() .'/fonts/DINPro-Bold.woff' ?>') format('woff');
      font-weight: 700;
      font-style: normal;
      font-display: swap;
    }

    @font-face {
      font-family: 'DIN Pro';
      src: url('<?=get_template_directory_uri() .'/fonts/DINPro-Light.woff' ?>') format('woff');
      font-weight: 300;
      font-style: normal;
      font-display: swap;
    }

    /* latin */
    @font-face {
      font-family: 'Montserrat';
      font-style: normal;
      font-weight: 400;
      font-display: swap;
      src: url(https://fonts.gstatic.com/s/montserrat/v18/JTUSjIg1_i6t8kCHKm459Wlhyw.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    /* latin */
    @font-face {
      font-family: 'Montserrat';
      font-style: normal;
      font-weight: 500;
      font-display: swap;
      src: url(https://fonts.gstatic.com/s/montserrat/v18/JTURjIg1_i6t8kCHKm45_ZpC3gnD_g.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    /* latin */
    @font-face {
      font-family: 'Montserrat';
      font-style: normal;
      font-weight: 700;
      font-display: swap;
      src: url(https://fonts.gstatic.com/s/montserrat/v18/JTURjIg1_i6t8kCHKm45_dJE3gnD_g.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }
  </style>
  <?php wp_head(); ?>
</head>
<!--preloader style-->
<style>
  body:not(.loaded) {
    opacity: 0;
  }

  body {
    transition: opacity .5s;
  }

  [modal-content] {
    display: none !important;
  }
</style>
<!--end preloader style-->
<!-- ACF Fields -->
<?php
$container_width = get_field('container_width', 'options');
$header_logo = get_field('header_logo', 'options');
$header_contact_link = get_field('header_contact_link', 'options');

?>
<?php if ($container_width) { ?>
  <style>
    @media screen and (min-width: 1650px) {
      /*.swiper-container,*/
      .container, [class*="g-container"] {
        max-width: <?=$container_width?> !important;
      }
    }
  </style>
<?php } ?>
<!-- END ACF -->
<body <?php body_class(); ?> data-barba="wrapper">
<!--  <div class="preloader"></div>-->
<!--<main>-->
<header>
  <a href="<?= site_url() ?>" class="header-logo">
    <?php if (!$header_logo['svg_or_img']) { ?>
      <?php if ($header_logo['svg']) {
        echo $header_logo['svg'];
      } ?>
    <?php } else { ?>
      <img src="<?= $header_logo['image']['url'] ?>" alt="<?= $header_logo['image']['alt'] ?>">
    <?php } ?>
  </a>
  <button aria-label="Open Menu Links" class="burger-menu hide-only-lg">
    <span></span>
    <span></span>
    <span></span>
  </button>
  <?php if (have_rows('header_links', 'options')) { ?>
    <ul class="header-links">
      <?php while (have_rows('header_links', 'options')) {
        the_row();
        $header_link = get_sub_field('header_link');
        $is_has_megamenu = get_sub_field('is_has_megamenu');
        $megamenu_links = get_sub_field('megamenu_links'); ?>
        <li class="header-link <?= $megamenu_links ? 'has-megamenu' : '' ?>">
          <?php if ($header_link) { ?>
            <a href="<?= $header_link['url'] ?>" target="<?= $header_link['target'] ?>"><?= $header_link['title'] ?></a>
          <?php } ?>
          <?php if ($megamenu_links && have_rows('megamenu_links', 'options')) { ?>
            <svg class="header-link-angle" width="10" height="7" viewBox="0 0 10 7" fill="none">
              <path d="M5.0249 4.25324L8.52506 0.753086L9.5249 1.75293L5.0249 6.25293L0.524902 1.75293L1.52474 0.753086L5.0249 4.25324Z" fill="currentColor"/>
            </svg>
            <ul class="megamenu swiper-container">
              <li class="megamenu-spacer hide-only-lg"></li>
              <li class="swiper-wrapper flex-lg-nowrap flex-wrap">
                <?php while (have_rows('megamenu_links', 'options')) {
                  the_row();
                  $title = get_sub_field('title');
                  $image = get_sub_field('image');
                  $is_has_video = get_sub_field('is_has_video');
                  $video_url = get_sub_field('video_url'); ?>
                  <div class="swiper-slide megamenu-slide <?= $is_has_video ? 'megamenu-slide-has-video open-modal-button' : '' ?>" <?php if ($is_has_video) { ?> aria-label="Open Video Modal" data-iframe-src="<?= $video_url ?>" <?php } ?>>
                    <?php if ($title) { ?>
                      <a class="megamenu-slide-title" href="<?= $title['url'] ?>" target="<?= $title['target'] ?>"><?= $title['title'] ?></a>
                    <?php } ?>
                    <?php if ($image || $video_url) { ?>
                      <div class="megamenu-slide-image hide-between-sm-md aspect-ratio">
                        <?php if (!$is_has_video) { ?>
                        <a href="<?= $title['url'] ?>" target="<?= $title['target'] ?>">
                          <?php } ?>
                          <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>"/>
                          <?php if (!$is_has_video) { ?>
                        </a>
                      <?php } ?>
                        <?php if ($is_has_video && $video_url) { ?>
                          <button class="open-video-modal">
                            <svg width="8" height="14" viewBox="0 0 8 14" fill="none">
                              <path d="M5.172 6.99977L0.222001 2.04977L1.636 0.635768L8 6.99977L1.636 13.3638L0.222 11.9498L5.172 6.99977Z" fill="currentColor"/>
                            </svg>
                          </button>
                        <?php } ?>
                      </div>
                    <?php } ?>
                  </div>
                <?php } ?>
              </li>
            </ul>
          <?php } ?>
        </li>
      <?php } ?>
      <?php if ($header_contact_link) { ?>
        <li class="header-link-contact hide-only-lg">
          <a href="<?= $header_contact_link['url'] ?>" class="btn header-contact" target="<?= $header_contact_link['target'] ?>"><?= $header_contact_link['title'] ?></a>
        </li>
      <?php } ?>
    </ul>
  <?php } ?>
  <?php if ($header_contact_link) { ?>
    <a href="<?= $header_contact_link['url'] ?>" target="<?= $header_contact_link['target'] ?>" class="btn header-contact hide-between-sm-md"><?= $header_contact_link['title'] ?></a>
  <?php } ?>
</header>

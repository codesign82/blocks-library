<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="description" content="<?php if ( is_single() ) {
	  single_post_title( '', true );
  } else {
	  bloginfo( 'name' );
	  echo " - ";
	  bloginfo( 'description' );
  } ?>"/>
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=5, minimum-scale=1.0"
        name="viewport">
  <meta content="ie=edge" http-equiv="X-UA-Compatible">
  <script>
    const BiggerThanDesignWidth = 1650;
    const designWidth = 1440;
    const desktop = 1440;
    const tablet = 992;
    const mobile = 600;
    const sMobile = 375;
    const resizeHandler = function () {
      if (window.innerWidth >= BiggerThanDesignWidth) {
        document.documentElement.style.fontSize = `${11}px`;
        // document.documentElement.style.fontSize = `${9 * window.innerWidth / desktop}px`;
      } else if (window.innerWidth < BiggerThanDesignWidth && window.innerWidth >= desktop) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / desktop}px`;
      } else if (window.innerWidth < desktop && window.innerWidth >= tablet) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / designWidth}px`;
      } else if (window.innerWidth < tablet && window.innerWidth >= mobile) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / tablet}px`;
      } else if (window.innerWidth < mobile && window.innerWidth >= sMobile) {
        document.documentElement.style.fontSize = `${10}px`;
      } else {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / sMobile}px`;
      }
    };
    resizeHandler();
    window.addEventListener('resize', resizeHandler);
  </script>
	<?php wp_head(); ?>
	<?php
	$code_before_body_tag = get_field( 'code_before_body_tag', 'options' );
	echo $code_before_body_tag;
	?>
</head>
<!--preloader style-->
<style>
  body:not(.loaded) {
    opacity: 0;
  }
  
  body {
    transition: opacity .5s;
  }
</style>
<!--end preloader style-->
<!-- ACF Fields -->
<?php
$header_logo     = get_field( 'header_logo', 'options' );
$container_width = get_field( 'container_width', 'options' );
$hide_nav_only   = get_field( 'hide_nav_only', get_the_ID() );
$hide_header     = get_field( 'hide_header', get_the_ID() );
$hide_footer     = get_field( 'hide_footer', get_the_ID() ); ?>
<style>
  <?php if($hide_nav_only){ ?>
  .menu-top-menu-container, nav.navbar {
    display: none !important;
  }
  
  header.sticky .bottom-menu {
    margin-top: 1rem !important;
  }
  
  <?php }  if ($hide_header){ ?>
  header {
    display: none !important;
  }
  
  <?php } if ($hide_footer){?>
  footer {
    display: none !important;
  }
  
  <?php } ?>
</style>
<?php if ( $container_width ) { ?>
  <style>
    @media screen and (min-width: 1650px) {
      .container, .wp-block-columns {
        max-width: <?=$container_width?> !important;
      }
    }
  </style>
<?php } ?>
<!-- END ACF -->
<body <?php body_class(); ?> data-barba="wrapper">
<?php
$code_after_body_tag = get_field( 'code_after_body_tag', 'options' );
echo $code_after_body_tag;
?>
<!--<div class="preloader"></div>-->
<main>
  <header>
    <div class="menu-top-menu-container">
		<?php
		wp_nav_menu( array(
			'theme_location' => 'top-menu',
			'menu_class'     => 'top-menu reset-ul',
			'container'      => '',
		) );
		?>
      
      <div class="language-wrapper">
        
        
        <div class="left lang-switcher <?php if ( ! $_GET['lang'] && ! $_GET['lang'] == 'es' ) {
			echo 'active';
		} ?> language-text" data-lang="">
          English
        </div>
        <div class="right lang-switcher <?php if ( @$_GET['lang'] && @$_GET['lang'] == 'es' ) {
			echo 'active';
		} ?> language-text" data-lang="?lang=es">
          Español
        </div>
      
      </div>
    </div>
    
    <div class="bottom-menu">
		<?php if ( @$_GET['lang'] && @$_GET['lang'] == 'es' ) { ?>
          <a class="header-logo" href="<?= site_url() ?>?lang=es" title="Home">
            <img src="<?= $header_logo['url'] ?>" alt="<?= $header_logo['alt'] ?>">
          </a>
		<?php } else { ?>
          <a class="header-logo" href="<?= site_url() ?>?lang=en" title="Home">
            <img src="<?= $header_logo['url'] ?>" alt="<?= $header_logo['alt'] ?>">
          </a>
		<?php } ?>
      <!--      burger menu and cross-->
		<?php $mobile_menu = get_field( 'mobile_menu', 'options' ); ?>
      <div class="burger-and-menu">
        <div class="menu">
          <button aria-label="burger" class="burger-menu" id="burger-menu">
            <span></span>
            <span></span>
            <span></span>
          </button>
        </div>
        <span><?= $mobile_menu['menu_word'] ?></span>
      </div>
      <nav class="navbar">
        <div class="mobile-mask"></div>
        <div class="navbar-wrapper">
			<?php
			wp_nav_menu( array(
				'theme_location' => 'primary-menu',
				'menu_class'     => 'primary-menu reset-ul',
				'container'      => ''
			) );
			?>
			<?php
			wp_nav_menu( array(
				'theme_location' => 'top-menu',
				'menu_class'     => 'top-menu reset-ul',
				'container'      => '',
			) );
			?>
          <a href="<?= $mobile_menu['cat_btn']['url'] ?>"
             target="<?= $mobile_menu['cat_btn']['target'] ?>"
             class="cta-btn-mob">
			  <?= $mobile_menu['cat_btn']['title'] ?>
          </a>
          <div class="login">
			  <?= $mobile_menu['text'] ?>
            <a href="<?= $mobile_menu['login_link']['url'] ?>"
               target="<?= $mobile_menu['login_link']['target'] ?>">
				<?= $mobile_menu['login_link']['title'] ?>
            </a>
          </div>
			<?php if ( have_rows( 'mobile_very_bottom_links', 'options' ) ) { ?>
              <ul class="reset-ul top-menu">
				  <?php while ( have_rows( 'mobile_very_bottom_links', 'options' ) ) {
					  the_row();
					  $link = get_sub_field( 'link' );
					  if ( $link ) { ?>
                        <li class="menu-item">
                          <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>">
							  <?= $link['title'] ?>
                          </a>
                        </li>
					  <?php }
				  } ?>
              </ul>
			<?php } ?>
        </div>
      </nav>
    </div>
  </header>
  <div class="page-transition">
 
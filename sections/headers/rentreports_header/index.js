import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {allowPageScroll, preventPageScroll} from "../../../scripts/functions/prevent_allowPageScroll";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/wrap'

const blockScript = async (container = document) => {
  
  const blocks = container.querySelectorAll('header');
  for (let block of blocks) {
    const menuItems = block.querySelectorAll('.current-menu-item');
    const languageText = block.querySelectorAll('.language-text');
    
    for (let menuItem of menuItems) {
      menuItem.closest('ul').classList.add('ul-has-active');
    }
    
    languageText.forEach((link) => {
      link.addEventListener('click', function () {
        if (link.classList.contains('active')) {
          link.classList.remove('active');
        } else {
          languageText.forEach((link) => {
            link.classList.remove('active');
          });
          link.classList.add('active');
        }
      });
    });
    const burgerMenu = block.querySelector('#burger-menu'),
        menu = block.querySelector('.navbar'),
        menuWrapper = menu.querySelector('.navbar-wrapper');
    // hide menu
    
    menu.addEventListener('click', () => {
      menu.classList.remove('active');
      burgerTl.reverse();
      block.classList.remove('active');
      burgerMenu.classList.remove('active');
      // allowPageScroll()
    });
    menuWrapper.addEventListener('click', (e) => {
      e.stopPropagation();
    });
    if (!burgerMenu) return;
    const burgerTl = gsap.timeline({paused: true});
    const burgerSpans = burgerMenu.querySelectorAll('span');
    gsap.set(burgerSpans, {transformOrigin: 'center'});
    burgerTl
        .to(burgerSpans, {y: gsap.utils.wrap([`0.6rem`, 0, `-0.6rem`]), duration: 0.35})
        .set(burgerSpans, {autoAlpha: gsap.utils.wrap([1, 0, 1])})
        .to(burgerSpans, {rotation: gsap.utils.wrap([45, 0, -45])})
        .set(burgerSpans, {rotation: gsap.utils.wrap([45, 0, 135])});
    burgerMenu.addEventListener('click', function () {
      if (burgerMenu.classList.contains('active')) {
        // allowPageScroll()
        burgerMenu.classList.remove('active');
        menu.classList.remove('active');
        block.classList.remove('active');
        burgerTl.reverse();
      } else {
        burgerMenu.classList.add('active');
        menu.classList.add('active');
        block.classList.add('active');
        
        burgerTl.play();
        // preventPageScroll();
        gsap.fromTo(menu.querySelectorAll('.menu-item ,.cta-btn-mob, .login'), {y: 30, autoAlpha: 0}, {
          y: 0,
          autoAlpha: 1,
          stagger: .05,
          duration: .25,
          delay: .5,
        });
      }
    });
    
    block.classList.toggle('sticky', window.scrollY >= 20)
    window.addEventListener('scroll', function () {
      block.classList.toggle('sticky', window.scrollY >= 20)
    });
    const langSwitchers = block.querySelectorAll('.lang-switcher');
    for (let langSwitcher of langSwitchers) {
      langSwitcher.addEventListener('click', function (e) {
        e.preventDefault();
        window.location.href = window.location.origin + window.location.pathname + langSwitcher.dataset.lang;
      });
    }
    animations(block);
    imageLazyLoading(block);
  }
};
windowOnLoad(blockScript);




import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/zineone';

/**
 *
 * @param container {Document}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
const header = container.querySelector('header');
  
  // region check of megamenu out of window
  
  const isOutOfViewport = function (elem) {
    // Get element's bounding
    const bounding = elem.getBoundingClientRect();
    // Check if it's out of the viewport on each side
    const out = {};
    out.left = bounding.left < 0;
    out.right = bounding.right > (window.innerWidth || document.documentElement.clientWidth);
    if (out.left) {
      elem.classList.add('megamenu-outside-left')
    } else if (out.right) {
      elem.classList.add('megamenu-outside-right')
    }
    return out;
    
  };
  const megamenus = header.querySelectorAll('.megamenu');
  for (let megamenu of megamenus) {
    isOutOfViewport(megamenu)
  }
  
  // endregion check of megamenu out of window
  
  // hover on links has megamenu
  
  const headerMegamenuLinks = header.querySelectorAll('.megamenu-link-wrapper');
  const headerLinks = header.querySelectorAll('.header-link.has-megamenu');
  
  // menu links toggler
  
  const burgerMenu = header.querySelector('.burger-menu');
  const menuLinks = header.querySelector('.header-links');
  
  
  if (!burgerMenu) return;
  const burgerTl = gsap.timeline({paused: true});
  const burgerSpans = burgerMenu.querySelectorAll('span');
  gsap.set(burgerSpans, {transformOrigin: 'center'});
  burgerTl
      .to(burgerSpans, {
        y: gsap.utils.wrap([`0.6rem`, 0, `-0.6rem`]),
        duration: 0.25,
      })
      .set(burgerSpans, {autoAlpha: gsap.utils.wrap([1, 0, 1])})
      .to(burgerSpans, {rotation: gsap.utils.wrap([45, 0, -45])})
      .set(burgerSpans, {rotation: gsap.utils.wrap([45, 0, 135])});
  burgerMenu.addEventListener('click', function () {
    if (burgerMenu.classList.contains('burger-menu-active')) {
      // allowPageScroll()
      // region allow page scroll
      document.documentElement.style.overflowY = 'auto';
      document.body.style.overflow = 'auto';
      // endregion allow page scroll
      burgerMenu.classList.remove('burger-menu-active');
      menuLinks.classList.remove('header-links-active');
      header.classList.remove('header-active');
      burgerTl.reverse();
    } else {
      burgerMenu.classList.add('burger-menu-active');
      menuLinks.classList.add('header-links-active');
      header.classList.add('header-active');
      burgerTl.play();
      // region prevent page scroll
      document.documentElement.style.overflowY = 'hidden';
      document.body.style.overflowY = 'hidden';
      // endregion prevent page scroll
      gsap.fromTo([menuLinks.querySelectorAll('.header-link '), menuLinks.querySelectorAll('.btn')], {
        y: 30,
        autoAlpha: 0,
      }, {
        y: 0,
        autoAlpha: 1,
        stagger: .05,
        duration: .4,
        delay: .5,
      });
    }
  });
  
  
  // region open sub menu in responsive
  const mobileMedia = window.matchMedia('(max-width: 992px)');
  headerLinks.forEach((menuItem) => {
    const menuItemBody = menuItem.querySelector('.megamenu');
    menuItem?.addEventListener('click', (e) => {
      if (!mobileMedia.matches || !menuItemBody || e.target.closest('.megamenu,.header-link-a')) return;
      const isOpened = menuItem?.classList.toggle('active-header-link');
      if (!isOpened) {
        gsap.to(menuItemBody, {height: 0});
      } else {
        gsap.to(Array.from(headerLinks).map(otherMenuItem => {
          const otherMenuItemBody = otherMenuItem.querySelector('.megamenu');
          if (otherMenuItemBody && menuItem !== otherMenuItem) {
            otherMenuItem?.classList.remove('active-header-link');
            gsap.set(otherMenuItem, {zIndex: 1});
          }
          return otherMenuItemBody;
        }), {height: 0});
        gsap.set(menuItem, {zIndex: 2});
        gsap.to(menuItemBody, {height: 'auto'});
      }
    });
  });
  headerMegamenuLinks.forEach((megamenuItem) => {
    const megamenuItemBody = megamenuItem.querySelector('.megamenu-links');
    megamenuItem?.addEventListener('click', (e) => {
      if (!mobileMedia.matches || !megamenuItemBody || e.target.closest('.megamenu-links,.megamenu-image,.megamenu-info-title')) return;
      const isOpened = megamenuItem?.classList.toggle('active');
      if (!isOpened) {
        gsap.to(megamenuItemBody, {height: 0});
      } else {
        gsap.to(Array.from(headerMegamenuLinks).map(otherMenuItem => {
          const otherMenuItemBody = otherMenuItem.querySelector('.megamenu-links');
          if (otherMenuItemBody && megamenuItem !== otherMenuItem) {
            otherMenuItem?.classList.remove('active');
            gsap.set(otherMenuItem, {zIndex: 1});
          }
          return otherMenuItemBody;
        }), {height: 0});
        gsap.set(megamenuItem, {zIndex: 2});
        gsap.to(megamenuItemBody, {height: 'auto'});
      }
    });
  });
  // endregion open sub menu in responsive
  
  //region prevent a default if href = #
  
  // const aLinks = header.querySelectorAll('.header-links a');
  // for (let aLink of aLinks) {
  //   if (aLink.getAttribute("href") === '#') {
  //     let currentColor = getComputedStyle(aLink).color;
  //     aLink.addEventListener('click', (e) => {
  //       e.preventDefault()
  //     })
  //     aLink.addEventListener('mouseenter', (e) => {
  //       aLink.style.color = currentColor;
  //     })
  //   }
  // }

//endregion prevent a default if href = #
  
  animations(header);
  imageLazyLoading(header);
};
windowOnLoad(blockScript);

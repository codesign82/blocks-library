<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="description" content="<?php if ( is_single() ) {
    single_post_title( '', true );
  } else {
    bloginfo( 'name' );
    echo " - ";
    bloginfo( 'description' );
  } ?>"/>
  <meta
      content="width=device-width, initial-scale=1.0, maximum-scale=5, minimum-scale=1.0"
      name="viewport">
  <meta content="ie=edge" http-equiv="X-UA-Compatible">
  <script>
    const BiggerThanDesignWidth = 1920;
    const designWidth = 1680;
    const desktop = 1680;
    const tablet = 992;
    const mobile = 600;
    const sMobile = 414;
    const resizeHandler = function () {
      if (window.innerWidth >= BiggerThanDesignWidth) {
        document.documentElement.style.fontSize = `${10}px`;
        // document.documentElement.style.fontSize = `${9 * window.innerWidth / desktop}px`;
      } else if (window.innerWidth < BiggerThanDesignWidth && window.innerWidth >= desktop) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / desktop}px`;
      } else if (window.innerWidth < desktop && window.innerWidth >= tablet) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / designWidth}px`;
      } else if (window.innerWidth < tablet && window.innerWidth >= mobile) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / tablet}px`;
      } else if (window.innerWidth < mobile && window.innerWidth >= sMobile) {
        document.documentElement.style.fontSize = `${10}px`;
      } else {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / sMobile}px`;
      }
    };
    resizeHandler();
    window.addEventListener('resize', resizeHandler);
  </script>
  <style>
    /* latin Montserrat 400 */
    @font-face {
      font-family: 'Montserrat';
      font-style: normal;
      font-weight: 400;
      font-display: swap;
      src: url(https://fonts.gstatic.com/s/montserrat/v18/JTUSjIg1_i6t8kCHKm459Wlhyw.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }
    
    /* latin Montserrat 600 */
    @font-face {
      font-family: 'Montserrat';
      font-style: normal;
      font-weight: 600;
      font-display: swap;
      src: url(https://fonts.gstatic.com/s/montserrat/v18/JTURjIg1_i6t8kCHKm45_bZF3gnD_g.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }
    
    /* latin Montserrat 700 */
    @font-face {
      font-family: 'Montserrat';
      font-style: normal;
      font-weight: 700;
      font-display: swap;
      src: url(https://fonts.gstatic.com/s/montserrat/v18/JTURjIg1_i6t8kCHKm45_dJE3gnD_g.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }
  </style>
  <?php wp_head(); ?>
</head>
<!--preloader style-->
<style>
  body:not(.loaded) {
    opacity: 0;
  }
  
  body:not(.loaded) * {
    transition: none !important;
  }
  
  body {
    transition: opacity .5s;
  }
  
  [modal-content] {
    display: none !important;
  }
</style>
<!--end preloader style-->
<!-- ACF Fields -->
<?php
$container_width     = get_field( 'container_width', 'options' );
$header_logo         = get_field( 'header_logo', 'options' );
$header_contact_link = get_field( 'header_contact_link', 'options' );

?>
<?php if ( $container_width ) { ?>
  <style>
    @media screen and (min-width: 1680px) {
      .container {
        max-width: <?=$container_width?> !important;
      }
    }
  </style>
<?php } ?>
<!-- END ACF -->
<body <?php body_class(); ?> data-barba="wrapper">
<!--  <div class="preloader"></div>-->
<!--<main>-->
<header>
  <div class="header-wrapper">
    <a href="#" class="header-logo">
      <svg width="168" height="40" viewBox="0 0 168 40" fill="none">
        <rect width="168" height="40" fill="url(#pattern0)"/>
        <defs>
          <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
            <use xlink:href="#image0" transform="translate(0 0.114748) scale(0.00179856 0.00755396)"/>
          </pattern>
          <image id="image0" width="556" height="102" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAiwAAABmCAYAAADoK8rKAAAAAXNSR0IArs4c6QAAIABJREFUeF7tXX2sZVV1X+vchLy598ZEKUpAjRMotEkbjMIIRkdTS4cWaOrQRjqDQxtjTRsBxYSSYmNsxsT+wbfRKn9YB1FMLTSFNpLSph2sCKhx0pp06BiJgAFHmJTc+3gheXc1680+w5373jlnr73XPl933YQAeftj7d/e5+zfWZ8ILfhNJpNdiLiHiC5cEOd5ALhrNBr9HSKuaYlKRGcAwHsBYH4+nusQADysOZeWzDaOIWAIGAKGgCGwzAhgk4ufTqcXENFXEfGcCjmOEdF14/H47hh5HVH5HAC8v2ScZwHgC4j4mZi5rK8hYAgYAoaAIWAI6CHQGGFZXV29nohuFi7lwGg0ulrYZ6M5Ed0EAPsFfZm4XIqIrHWxnyFgCBgChoAhYAg0iEAjhCWQrOQwiUkLEd0BANcE4rwTER8J7GvdDAFDwBAwBAwBQ0ABgdoJC5uBAODxGNkR8RPD4fAWnzGIaA8A3OPTtqANa1p2IOLPIsawroaAIWAIGAKGgCEQgUDthGUymRxExHdHyMxdjw2Hw7MR8cWycYhoBQCOAMCZkfPdiYjXRo5h3Q0BQ8AQMAQMAUMgEIFaCYuGdiVfp4+WJcBvpQzGM03LEnjKrJshYAgYAoaAIRCJQK2EJdJ35aSlEtEj4/F4Z4WG5TsAcFEkRnn3axHxTqWxbBhDwBAwBAwBQ8AQECBQK2FRMgflyys1C7kQZvaViTUH5fOZWUhwsKypIWAIGAKGgCGgiUCXCQtkWbZ927ZtT20FiCMs7DCr9XsUEd+pNZiNYwgYAoaAIWAIGAL+CNRNWA57JInzlx5gx2g0esIIiwQya2sIGAKGgCFgCHQPgboJi0aE0DzKWxIWFx30OgDQ1LDcj4i7u7fFJrEhYAgYAoaAIdB9BOomLLci4se0YCOifWXp+onoGUUflk9aun6tnbNxDAFDwBAwBAwBGQJ1ExYucvgtmYjFratCm4novoq6QRJRLkfEByUdrK0hYAgYAoaAIWAI6CBQK2FhkSeTiZofS1VoMxFxgrqDClA9i4hvVBjHhjAEDAFDwBAwBAyBAASaICyaWpZXsiw7tyhSiPFQ0rLsRcSvBeBrXQwBQ8AQMAQMAUNAAYHaCYvTsqj5sniYhc5wtYtC87GYs63CQbMhDAFDwBAwBAyBGAQaISyapIWInhyPx+eWgRBhGjKyEnO6rK8hYAgYAoaAIaCEQGOExZGWXQBwR2xuliotizMNsablm4JU/ZbZVumQ2TCGgCFgCBgChkAsAo0Sllz4yWTCfi17iOhCRDwNAEbub6d4LtCrerMjLpcBwF8UEBfO28KRRZ9d9kKHvBcA8D5P/PvW7F8R8bt9W5Stp/8IENHr1tbWXiNZ6Ww2Oy3LsqMrKysvSfqlbMtrcPKsIuJayrls7O4g0ArCMg/X/APHDxIR/QoAvA0A3o6IHPVT9DswGo2u9oXeJZdjU9KbAOD/AODHy05ScuyIaC8RfdUXyz62Q8SrEPGePq7N1tQPBKbT6QX8TiSi84hoOwC8Ye6Dz/djbx6MV1qGzNR9ZB7l57Eoq3nLZDZxEiLQOsJSttaXX375LUS0m4g+CQCvXWzrYxpKiGVvhp7NZo8BwI7eLChsIY9nWfaOsK7WyxBIgwB/0E2n079ERP442/QOVJiVScs82Wn6/zeW5OOrqLB2G6LlCHSKsMxjubq6ej0R3WykRf+EGWHZeEF+YzAYXKmPro1oCIQhUPTOCxutk70Ka8d1cjUmtBiBzhIWXilrXNbX1x9adNolotvG4/HHxWhYhw0EjLAAIOJF5sdiD0RbEJhMJmqpINqyJqkcRHTJeDx+SNrP2vcHgU4TlnwbJpPJpqKKnAV3MBjsK0sq159t1F3JshMWRLS6UbpHykaLQMDIynHwjLBEHKKedO0FYeG92Iq0uD06kGXZp4y4+J/YZSYsZgryPyfWMj0CLoJSrf5aeonTzWCEJR22XRm5N4TFOaM9WpTThTUuAHDXYDB4xMhL+fFcYsJijrZdeXMtiZwlH2JLgsCryzTCsnRbvmnBhYSFiDjR2hUA8MsAkBf+ewYA/hcA/l47BNiFGV8AAL8KAJyLhX+HAOAHvnOVfI0serofI6L/RsSf5Igg4qHZbPYjbRsphxwCAOd+efsc+owj5/n4d9+11XlUl5SwvICI5yPiU3VibXMZAkUImHblZGSIaN94PL7bTszyIrCJsLgL9gseGWHvB4CPxl64jhjdCADXlGzDowDw54jIWpLS33Q6/QoA7KtqV/L3Y4i4fzgc3hIxBttbmaT8DQBU1TC6EwBuaFNypPX19XsR8QMx6+9aX0S8GBEf7prcJm9/ETDflU2ExZxu+3vcvVZ2EmEhIiYNd3j1fLXRRxDxS8I+G80D5qus7cORQ7PZ7PBCLgGxeBz3PxgMdoWYjwIqRHOG3UtZyyMWNEEHIuJ8N98DgFMTDN+6Ic3JtnVbYgId98s7HFu2pC9AWh6Wvuxk3DpOEBYiugkA9gcOJ46qICImRmValSJR2E/lnWVyKn+ZiGL/A8jK/FJ2+miRAvdI1M2Z6N4q6lRf4ycB4Lc1svGak219m2Yz+SPgPrxOmKz9e55oyWZv1jZ3/pdl2dPbtm37W0R8sfOLsQVEIbBBWJz54oGokQC8L9sAzcqiaKWaFk5ZDQCPR64n7y6pUxRD+ng+1rSc3SbzkBKG6sMoma3Yb+WNhrf69tiAkQjE+q9kWbY9RDscKbZ1NwSSIpATlu94+KxUCVKp+XDkiJ15+WKO/e1FxK8VDaKsTq2sU+R8f34YuygAsCrRFSAqkRVLDqdwWG2INAjEZLXliMjxeLwzjWQ2qiHQHAJIRHsAQKvI2+WI+GDZciJNT/NDlxIkZbMQz1tqGoowcS3CxWRuR6wzc3NHKu3MfH6IKNR0eUI4K26Ydp9s9DgEYggLAFR+YMVJZ70NgWYQYMJyHwC8X2n6Su0AEXFIb1XkjK84hWaoyWTyQUQ84DtQVbuqworK6yrVHlXJ2te/E9FvEtG/xK6PiG4dDAbXx47D/dlBmc147CNZMN4YAI5IwqU5p5CrUF40pobosWPwuli+5/ifOs1qzr/qdADgf5r88dpfSuFbEfnBZYTFnQp2D+DnKcUeaR68LMuOrqysJDlLRXLyc7S2ttbYMzSbzU5z6/Z+fzBh0TAH5ZiUaj0UzSb5fIXOvsp+LHwxFapZE6yrkvhpPixdGEsxckktOZxE2+Or0eGXiCO/XYrQegEAfkxEP8myjH3hOE/Tmta5cgTlitlsxhrc7S2sJP4CET2cZdl/sbZaQk6LMDLCEn96IjGMF0A+witExHmgns+y7B8Q8T5NPyR25Cai3XxfEdGFiMj5zlJU/JaunB3Ej/qsmwmLpsbjWXZiLGF0nJsk1rl3fvjCi13By/6kZZSF1SUgLJXh29IT0fX2Ssns1JLDBRCLF7Is+6WqfZCQoKqxGvw743wdIkabmruIB0eeZVn2RzGkLTKf1NJrWLTf/zU/SycSnWoU8nVFgg8g4rtrXod0uvl1b1kLsOuEpfBi1z6wFYSFD8JB6e6UtDfCMgfO+vr6LYgYXX1bMzlcgMbHKyJJa62KZzF4qFjTmxJJDZY/siPv92WhFb8jCAu/9O8djUZXR8qfvLszffrMsyolf9oadh8hE7XZ0LqMRqPzpBiwPNquEYnWWDjsYnZjbcJSZRLqq4ZFm7CYScgdYSLaq5FvRTs5HL9smcQKkustHWHhLfQ1hS2+sbQiwep+wS7MF6zRiyAsLEKrNSzuEr0dAEYAMPXYI25373A45CSlXqbGHhGWDXhCIr9iQ+M99qWWJvOkRdvptlQz4NLwa4Q050AVZtlNsFmFLwFnYz+i6EwcnD24lhNU0yRsZyUiLssQ9UuRHM4Ii/eWeBG1+dG0SKq3hAkbhp69CMLSag1LjOZbYh7pGWHZMJVIainxnTSdTg/1IFMyr306HA45P9mLTFhik53NP+6VF62yk+9bi9LZR4YFbnqFVR0W5WirwnUlfLe2augAH5Ei+cUXpg8QRlh8UDreRqrd6rgpaBMw7CgsdcSNICyt1rDEmCgkWoaeERaxlkX7/vN/2tO0zKN0mbBoJXLzytKqSJBKzU+RD/wi6pXZbpWyBfO85r8CAFomAUS8KNSPoOzRM8IiejF5R2ZpadVE0iVuLCVsLE7E+6vVGhYjLHGHzTeD8WQyOdgBJ1tvMHKymme61dCyeNUTcgSJ0+bH5mIpTVKnmem2KgdLjrqS9si7xIH3bnesoVZkSKj/hA9cRlh8UDrRxtuXQ2vvRdIlbhxiFoogLG3XsOxCxG+FQL7sGhbGrErTz21izG4h+1JXHyZr88UPY/KxiLQCCtl1S51SNf1XJA+JAhnzIn11HZAm5mljcritcGgLYckjcVwCuzq27CVOakdE9wocjtksdDEiPlwloFSz5kKI/wwAXlM1ttLfOVncFUJHcLFZMoKwmIbluIZKVE+OHegHg8EuTt62traW/Czl86yvr4vCjX38eKT3nwseuHYwGBzmZG5Kz0nhMJwsbn19/VwAuEPiY0NEl8wTlhUA+LeAmkJeNYQWpY8wDVWSIy11GJOV0Wj0W76e6Y4Bh0YMLX1kkAsVjqlQmx8zbxNE6MPZFsISYm4IXfN8P2n4ta+2S+q/EuIforH+1HJGEJa2a1iCM5BLPh5DCMt4POZLtNaflFz4RIBJ/Vd8LQjawITIeYKw5MIIa+JEXbIBmpbK+aQHtWQTgkMDnablm57kj31/bigr5Kh9UNo6nvQSKFgHf82ekzoVd1sIS2yuk9CzIDXd+BKr2Wz2C4HmRqy5CF3vYj+pJshXw5TPE0FYTMMSqGFpgrBIzTc+pE2a4Zc1F+Px+CGtZ8N3HClZY+3SJsLitATnAcCHAGD3Fr4mHGb6PQD4rEaBPne531gwV772+wHg00URQfMARWpXOEnPY8w4R6PRE77AF7UjIta2cMKzHQs4Mkn5KQB8HQDukmhwYmVqa3/pBVC0DunFEIpHWwiLLxEIXWfJ2RYVofSRMxWm2mvn8aTnVXouIwhLqzUs0q/q+b3zuaznCJ/YJNQgYTnMYcs+59QHA+nZ6QphYe3SloRl4ZBwFFFu1zqqQVIqLvg3AUBeWO1pAHjC90L3eRicve6fsyx7ejabcf2C/PdzJimpvswdMWMcec4Xfdfkc4i73kb6tV5CVmrzAUp1uQaYWmpb88J7wQgL4gd8n70aCUurNSw+7+iS+6GwnttiH6mmvSyTue8eh7RzGhYjLH7gVRMWv3Gab1WmXuLDmGXZF7WLSTW/6u5LoOhk+43BYHBlXYi0hbB0xSTkI2cqTFOciQANy1WS2krSr+SFNQabs1NgNT+mEZaTETbCIooY6wdhcWz624tqtdz7uQn7XOoHvw/jB9TjKVp2cifbxYlTXa6mYdEvd5DiWWkxYTENS/d8WEzD4veQdp+wOM3KP25BVm4bj8fRBfP8cLRWIQgoOdlyyKw4k2iIvAsmkVbUEvLxDYld61b9pWY8HzlTkcAU628xYeHlmoYlIKzZfFhSPCnFY0qdbr18WOpdgmy2IvViU05EMumXu7VUk1CElm+4rDbaqS5XKS4+phbttfN4UsLiI2cqTFOsv8WExTQspmH5CgDs8z33Td2XS0NY2ATEiZu2SDrDD+u7NCJ8fDfb2skR0Cpu5/PVLpfOr0eqy1VKWJrCQEpYfORMhanfjspatZiwmIbFCIsRFtnjrN+aX2bT6fRSAPhwUY2Eppii/mr7OaKran06EUUnhwtJd66JaqrL1QhLZ3xYbkFEb5OzVBMY4XRrGhYjLJ0gLNJIrlaZhPgCWF1dPYv/DQCv58sly7LTZrMZhzm/HRHfURarLs3W58KMzwIAHp9/PwKAwynCjRfm4tTmHK6dZC7NS1l7LFeB+T9cXpqY4RtLGJYL3RbC4mNqiQG6qK9Uw+Ijpyamjhy/CwDekGD9zxPRZyTnuEbCYhoWIyxqhIWjmFwa/Y3HiNN+uDs66r+5DAAR7SaimwXPZ7HTrUt6tpPJAgCc7gblOhrfB4AHfZK4+Qjiwro+DQAckuqVPGdxXJ9kOnMXzR4A+GhJFlrvJHVV6yOiawDgDwvm4uRxXATSKyFe1Vxd+LtUjV60plQVmCUYal6u8/OahkVHw6J11iRnoqxtjYTFNCxGWFQIS4CPidbjUjTOZsLiiMpfe6SV54y3fxpDXKQphEvQ2FHlt0JEnL33nwRVoitrFpV8fXKGW85i61uRurLkQOqTkHp86Rd5CVlpJFHaFiTZooSI9vuemzp9WBRrUvkur7JdjYTFNCxGWFQIS4RZsvJ5CGxwMmEJLEgYdIFEptCfX29lCB8RXQYADwSAJC7sGFAfKRdLPFfAehrp0tXkcGVgtUXD4mNqSbHpUgLqI6cWpktOWEzDYoSl/4RFWPRw8R0oIi2azK3K0dZpVn4Y8dL2JhIRxCgXL1irE7G+pF27nByuC4TFR3ORYoOlhMVHTiMsr+5U5Duy8iMuxZnwGdMy3Z6MUpsz3UaeQZ/jIG1zXMMSoRWYn/ByRHywSoKYA7uFWv7JqmQ/RPQdD/NWldhehIyInhGYgYrm3Nunys1KyeHYyfZ8RHyqaqPq+rvW5boor/mwxPuwmIYF7h2NRlfX9SxI5ol5/0t8FaURKFZLaHO15jYTFo2L9llEfGPFV+nKdDo9tEX+FMmZn29b+iWhoPHI52IH2bPLIoiI6E8A4IuhC5nr563RUZgr6RDSy7dIGGnhuKSLcoO3hbD4mFpS4CHVsPjIqYXpkhMW3m7TsFim2+jEca0kLEralfydWKodmEwmH0TEA1ov0KpQZiK6DwDerzRf6dqUNDm5qDsR8REluWsfxoWUXsHJ/WIn9zElxM4R0l/rcjUNy6sIaGHqwuf5I+zUkL1N0UdKuiMuC/NhMR+W/vqwKF/qpdEuEQ9h0TukMDrIXZpHFEw0+dyFa3N5VjhE2TcqqOqd6GWCqhqkqb8T0YVExFFkUb+mk8OVCa91uRph0ScsPKLLpnx71AHU6XxqCOmOfFeahsU0LNEaFpeD5SFnEWEi3NTvFDbZDQaDXaisGSg1ZyhGBuXAlRGWMwCATTlav0KHWBcKflBrIgDorPNtwEVeBFvjyeG6QFh8TC2K5/LEUG02Cc2v1324pIDAe8yQZJQRhMU0LKZhUdGwOOK/sra2ludh8z7z2g1XVlae4+eICYuG/0ouX6kfy2Qy4eyu5ygt5pUsy87dtm3blo6YTuuhSVgKyZhCJNIiJJ0lLFoJu9qQHK4LhCXk613j+ZMSFh85A8huq0ltDM4RhIWnNQ2LaViiNSwx5zdV3y4TFk7dv30rwuK+qjjFf12EhRPFLb2GRdHJ9ipEvCfVodcYN9XlKsXQhwhorHdxDCMsKVB9dcwIwmIaFtOwqGlY0p5y+ejahKVtJiFNv5IqHxZNctQ5HxatCsxNmTikj05bCEtTeAUQlkoSmgpT6d62oX0EYTENixGWXhMWzUiaUqfbmBj8rV4iRLRvPB7fXfSCUXYotiihAqAVw0gfz7KMi1y2/pfqcjUNS3weltYfHg8BIwiLaViMsPSasHCBvjs8niGfJqWXujSZT9WERHTbeDwuLPGumBvFJw/LTQDgXVulZG2V+WyqcKnz78oVmFuVHK4MRyMsdBPJagmZhkXwYMbUWat6LwrEUG8a89FqieM2ot8eGY/HXJS48Cclu1XZ4tUPQcSAbBLiaBoN00nlpc5ySsEsWdsrRPSUR6ZbDafiShONIo7XIuKdEXtaa1dFJ9uLEfHhWoWPmKwthKVDJiGfZyhJQcmIbW6sq0vZ/gMAeK1QiGNZlr2tKBhBOJZ6cyMsJ0Pa5tT86puvMGCeml9DO/ARRPxSlUz8ol9dXeX8KNIHsWjo0krNCtluvTUeCkn4OpXlVurHULSBTTmOVp1V07AUIyDde59qxalIYMw+N9mXLzMi2i2RARHvaytZ4XUYYTHCIjnPi203CAv/Iv09RGG4zjT0bQA4JUZ4AGAty+fLzEJubaGEjLVGlyLiIV85Ayte8/A81w5E/JnvXE2262MFZgmeqS5X82ExHxbJOexa20hTV6U5JMdD6n5gtYQ21xJq49k6QVgiSIuIrOQgRKg8F3EszccyR8j2AIAkVJYztf5+CIEgIqlfEM/1GyEJppo4VJoVmBHxPV1Z9zzWbSEsHTIJmQ9LEw9ry+Y0wmIalpgjeRJhCdBGRPtbOBXhJ2NMRL5OZs7P5HMV9YVY0/FXPuatMuBdMrlPecx1Q9eqMytVYIa2J4er2N8k/hamYTENS8wLve19zSRkhCXmjG4iLI60sCPuH7M5BADePFcjhy/znwLA1wHgLq0vY440mU6n7wGASxCRE75t/IhoOyL+mg+ZkXg6O+LyNgA4bw68owDwmMT84wO8m+u9ALA99Vw+8sS2kV6oRfP5+DTEypqyf1s0LE35/5gPS8rT1d+xYzQskgy+ZhKCfma6rXo05rLG8hdx7f4VPP/q6uqvI+K7Z7PZ7/G/C2Q+NhwOz0bEF6vWZH8PQ2DZksN1QcPSIZOQRQmFPXa96hUTJeqrSWfAjLAsKWFp29PifF8+DbB5Q5pynGobRinkUazAfOtgMLg+hYx1jmkaFsvDUud568NcTpN+KLSeHCJ+Yjgc3uKDhREWIyw+56S2Nq709YEFjctGbpbRaHSRaVr0tsIlh+N8NqfGjsoagSzLvg8Ab4kdK2H/p6pqGRlhEROWSg0L7+dsNvuF4Jz1tvhhriXgVAlsJieiPmiOfyeUrDAeErP/MhMWqdlNgmvCd67X0Fv6sHj1bEmjgs05BgC7RqPREy0Rs9NiaCWH6xIIVaaWthCWKjlTYZ7Ch8URlsc4vN9XbkRkP7ctK7b7jtHGdpPJ5IOIeKCNsjUkk1c0aC7bMhMWqWOzRHPV0N6fmLbzhIVXssUGcT2NUyQ2z6Y3oq3zaznZtnV9ZXKVXYZtISwdcrr10rBIyTERfSPLshtbfr6ekwYoTCaTgyX+ei1frr54Pinp52ddZsIymUx2IeK3PHdhwyoxGAx2raysPAcAQ89+dTdb5WeoF4SlgLTkgB5DxP1tzwBZ9+77zKflZOszVxvblIVdG2ERm4Qq87DwGZBqbtp4braQiU1XojpZRlhORlGqBVhmwiJd+xzSbJlo44+z4jOx+t3eEBZGucoDnVk6ALD/xA8Q8X+yLDu6srLykvm7bD6jihWY2/gAeMnUBcLSIZOQl4ZFy7nba4NrbCTdJyMsJ22OOAJUemk3FbCRopYQIzeZTA7H+AvV+Gh4T8V71ArC4pKsfQgAznd5X/JFcFFGJhhf9gmpDqxTdIyIOAcLh21/l4geHo/Hd3uj2NOGWsnhugxPFwhLh0xCXhoWPi99PHtsuhoMBlf6Pg99vHB8177YTqpdcR+vF7iivl7T9pCw3IqIH/NafHcaHWuUsHhmns3h5ArGnBV2rQxfqcNRwVhMYq5bVuIi9SPoznmXSWqEpRgvqelGQqz6aIqUrL+vX8iyp+94a6nvSj5HXzUsvsnzFEvfhGxbkj6NalgCqyh7FQhU+DpZWqfdPl4WoU9PFwiL1NQQisVivwDC4q1h4bn6RpqNsMhPHl9QoSkq+kpYJASub5FmjREWIuJstQflR3ijRyVpUdKy5Az/tqpq0IHraGW3PqrjQ4HuAmGRXoShWCgQFi8flvl5+kRapMRS4aNLa6sbGYcv5tFoxJnNg/LPdIyw/EQA8oHRaHS1b3tpThbfcZto1whhcWYg9k05M2LRjyLiO4v6Sx2ZquToUmKdqrVU/b1Pl0TVWiv+XhrZEZBMzyvJmTSMvEOERaRhyffGaXI+LkgmF7ntabpL92lZnW75UgKA/bHm+L4SFomGJT/JLsyZ6/9xtE1nf00RljsA4BoF1PaWVTnWfOBDDonC+mofwtWNOp2Ivtf1CyIWPJ/ijBKziO+FJY2SaaritTCKTBzWO79/7lxeMZvNLufcOABwVsfOp3j9wlwasce9qf4bOUAA4HkOruD3uWayT8kdEOLYqwVaXXK6M7WH3zGIyJnGT9FaQx3j8B7V6nTrXjxHIrUrOTb3I+LuIqA0zUI8xzJpWdw+vbWOQ9jCOcYAcMQ3eyo//ADAfSYFaxGN584aj/k+ACjL4MovnHt85UyBM5MWANhbISdP/Z+acubEGgBOT7GuBGM+GWLaYC0Bp+bPsuzpEJlms9lG9GOLfj/PZWE8hsPhj0Nw8V0PR41Op9NLy9pnWXbabDb70Xg8fsh3XO12uZxOli33TFtOnnNtbe016+vr52qvJ8F4r+c0JExm6yYslwHAA4oLOrMo3FnZ4Yi/BD6/TL4sintkQ0UgwJdzHhk3/98RQybtmsu4+O+kk9rghoAhoIbA4num7P/r+Nv8wuomLDexfVINWYCdiMjJ4Db9pDbMKpmWxSxUhYP93RAwBAwBQ8AQaAKBugmLlv9KjlWhH4tzvJV4X5fib4SlieNpcxoChoAhYAgYAscRMMLieRKayoToKZ41MwQMAUPAEDAEeo1A3YSlNpNQYJr+ws02DUuvnwNbnCFgCBgChkDLEaibsNTmdMu4S8LFqvaJiJYqgVwVHvZ3Q8AQMAQMAUOgTgTqJiwrHDJaR1gzg6gZ2rxMYc11HkCbyxAwBAwBQ8AQ8EGgVsLCAnGyLaVIIU4i9WDZIrUKQJn/is9RsjaGgCFgCBgChkA6BJogLBpaltKkcfNwaWhZTLuS7gDayIaAIWAIGAKGgA8CtRMWp2VJWvxwceHT6fQrALDPB5DFNk2mbA6R1/oYAoaAIWAIGAJ9RKARwuJIS4gDbmWl5qJNCiEtRlb6eORtTYaAIWAIGAJdRKAxwuJIyxkA8E0AuMgDvDsB4IY8TblH+01NXLr+26uqVm5UhUS8SrMQV4i81scQMAQe3PwEAAAAWklEQVQMAUPAEDAEjiPQKGHJN4GIzgOADwHA+QDw5rnNeZyreALAl4tqBkk3kmsfTKfTPwCADwPAGxDxNEeejiLid4noa00WwpKux9obAoaAIWAIGALLgMD/A0Ww7Jd4rv0WAAAAAElFTkSuQmCC"/>
        </defs>
      </svg>
    </a>
    <button aria-label="Open Menu Links" class="burger-menu hide-only-lg">
      <span></span>
      <span></span>
      <span></span>
    </button>
    <ul class="header-links reset-ul hide-scrollbar">
      <li class="header-link has-megamenu">
        <a href="#">Solutions</a>
        <svg class="dropdown-angle" width="12" height="8" viewBox="0 0 12 8" fill="none">
          <path d="M2 0C1.17595 0 0.705573 0.940764 1.2 1.6L5.2 6.93333C5.6 7.46667 6.4 7.46667 6.8 6.93333L10.8 1.6C11.2944 0.940764 10.824 0 10 0L2 0Z" fill="currentColor"/>
        </svg>
        <div class="megamenu-outside-spacer hide-only-lg"></div>
        <ul class="megamenu megamenu-two-columns box-shadow reset-ul">
          <span class="megamenu-spacer"></span>
          <li class="megamenu-link-wrapper">
            <div class="megamenu-link-title">
              <h3>About Us</h3>
              <svg width="12" height="8" viewBox="0 0 12 8" fill="none">
                <path d="M2 0C1.17595 0 0.705573 0.940764 1.2 1.6L5.2 6.93333C5.6 7.46667 6.4 7.46667 6.8 6.93333L10.8 1.6C11.2944 0.940764 10.824 0 10 0L2 0Z" fill="currentColor"/>
              </svg>
            </div>
            <div class="megamenu-link">
              <a href="#" class="megamenu-image">
                <picture class="aspect-ratio">
                  <img src="<?= get_template_directory_uri() . '/front-end/src/images/about-us.png' ?>" alt="">
                </picture>
              </a>
              <div class="megamenu-info">
                <h6 class="megamenu-info-title">About Us</h6>
                <h6 class="megamenu-info-description">Our Mission Statement and
                  Values</h6>
                <ul class="megamenu-links reset-ul">
                  <li class="megamenu-sublink">
                    <a href="#">Intelligent Audience Segmentation</a>
                  </li>
                  <li class="megamenu-sublink">
                    <a href="#">Real-Time Omnichannel Experiences</a>
                  </li>
                  <li class="megamenu-sublink">
                    <a href="#">Testing & Optimization</a>
                  </li>
                  <li class="megamenu-sublink">
                    <a href="#">Connected Data Platform</a>
                  </li>
                  <li class="megamenu-sublink">
                    <a href="#">Measurement & Reporting</a>
                  </li>
                  <li class="megamenu-sublink">
                    <a href="#">Rev Predict</a>
                  </li>
                </ul>
              </div>
            </div>
          </li>
          <li class="megamenu-link-wrapper">
            <div class="megamenu-link-title">
              <h3>About Us</h3>
              <svg width="12" height="8" viewBox="0 0 12 8" fill="none">
                <path d="M2 0C1.17595 0 0.705573 0.940764 1.2 1.6L5.2 6.93333C5.6 7.46667 6.4 7.46667 6.8 6.93333L10.8 1.6C11.2944 0.940764 10.824 0 10 0L2 0Z" fill="currentColor"/>
              </svg>
            </div>
            <div class="megamenu-link">
              <a href="#" class="megamenu-image">
                <picture class="aspect-ratio">
                  <img src="<?= get_template_directory_uri() . '/front-end/src/images/parterner-and-awards.png' ?>" alt="">
                </picture>
              </a>
              <div class="megamenu-info">
                <h6 class="megamenu-info-title">Parterner & Awards</h6>
                <h6 class="megamenu-info-description">Delivering effective
                  engagement</h6>
                <ul class="megamenu-links reset-ul">
                  <li class="megamenu-sublink">
                    <a href="#">Retail</a>
                  </li>
                  <li class="megamenu-sublink">
                    <a href="#">Travel & Hospitality</a>
                  </li>
                  <li class="megamenu-sublink">
                    <a href="#">Financial Services</a>
                  </li>
                  <li class="megamenu-sublink">
                    <a href="#">Grocery</a>
                  </li>
                  <li class="megamenu-sublink">
                    <a href="#">Telecomm</a>
                  </li>
                </ul>
              </div>
            </div>
          </li>
          <li class="megamenu-link-wrapper">
            <div class="megamenu-link-title">
              <h3>About Us</h3>
              <svg width="12" height="8" viewBox="0 0 12 8" fill="none">
                <path d="M2 0C1.17595 0 0.705573 0.940764 1.2 1.6L5.2 6.93333C5.6 7.46667 6.4 7.46667 6.8 6.93333L10.8 1.6C11.2944 0.940764 10.824 0 10 0L2 0Z" fill="currentColor"/>
              </svg>
            </div>
            <div class="megamenu-link">
              <a href="#" class="megamenu-image">
                <picture class="aspect-ratio">
                  <img src="<?= get_template_directory_uri() . '/front-end/src/images/leadership-team.png' ?>" alt="">
                </picture>
              </a>
              <div class="megamenu-info">
                <h6 class="megamenu-info-title">Integrations</h6>
                <h6 class="megamenu-info-description">AI-driven personalization
                  platform</h6>
                <div class="megamenu-links-wrapper">
                  <ul class="megamenu-links reset-ul">
                    <li class="megamenu-sublink">
                      <a href="#">BI/Anlytics</a>
                    </li>
                    <li class="megamenu-sublink">
                      <a href="#">Cloud Storage</a>
                    </li>
                    <li class="megamenu-sublink">
                      <a href="#">CRM</a>
                    </li>
                    <li class="megamenu-sublink">
                      <a href="#">Data Source</a>
                    </li>
                    <li class="megamenu-sublink">
                      <a href="#">Data Storage</a>
                    </li>
                    <li class="megamenu-sublink">
                      <a href="#">eCommerce</a>
                    </li>
                  </ul>
                  <ul class="megamenu-links reset-ul">
                    <li class="megamenu-sublink">
                      <a href="#">ESPs</a>
                    </li>
                    <li class="megamenu-sublink">
                      <a href="#">IoT</a>
                    </li>
                    <li class="megamenu-sublink">
                      <a href="#">Marketing</a>
                    </li>
                    <li class="megamenu-sublink">
                      <a href="#">Mobile</a>
                    </li>
                    <li class="megamenu-sublink">
                      <a href="#">Weather Services</a>
                    </li>
                    <li class="megamenu-sublink">
                      <a href="#">Other</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </li>
          <li class="megamenu-link-wrapper">
            <div class="megamenu-link-title">
              <h3>About Us</h3>
              <svg width="12" height="8" viewBox="0 0 12 8" fill="none">
                <path d="M2 0C1.17595 0 0.705573 0.940764 1.2 1.6L5.2 6.93333C5.6 7.46667 6.4 7.46667 6.8 6.93333L10.8 1.6C11.2944 0.940764 10.824 0 10 0L2 0Z" fill="currentColor"/>
              </svg>
            </div>
            <div class="megamenu-link">
              <a href="#" class="megamenu-image">
                <picture class="aspect-ratio">
                  <img src="<?= get_template_directory_uri() . '/front-end/src/images/leadership-team.png' ?>" alt="">
                </picture>
              </a>
              <div class="megamenu-info">
                <h6 class="megamenu-info-title">Connectors</h6>
                <h6 class="megamenu-info-description">AI-driven personalization
                  platform</h6>
                <ul class="megamenu-links reset-ul">
                  <li class="megamenu-sublink">
                    <a href="#">Flexible Connectors</a>
                  </li>
                  <li class="megamenu-sublink">
                    <a href="#">SDKs</a>
                  </li>
                </ul>
              </div>
            </div>
          </li>
          <span class="megamenu-spacer"></span>
        </ul>
      </li>
      <li class="header-link has-megamenu">
        <a href="#">Why ZineOne</a>
        <svg class="dropdown-angle" width="12" height="8" viewBox="0 0 12 8" fill="none">
          <path d="M2 0C1.17595 0 0.705573 0.940764 1.2 1.6L5.2 6.93333C5.6 7.46667 6.4 7.46667 6.8 6.93333L10.8 1.6C11.2944 0.940764 10.824 0 10 0L2 0Z" fill="currentColor"/>
        </svg>
        <div class="megamenu-outside-spacer hide-only-lg"></div>
        <ul class="megamenu box-shadow reset-ul">
          <div class="megamenu-spacer"></div>
          <li class="megamenu-link">
            <a href="#" class="megamenu-image">
              <picture class="aspect-ratio">
                <img src="<?= get_template_directory_uri() . '/front-end/src/images/about-us.png' ?>" alt="">
              </picture>
            </a>
            <div class="megamenu-info">
              <h6 class="megamenu-info-title">All Resources</h6>
              <h6 class="megamenu-info-description">Our Mission Statement and
                Values</h6>
            </div>
          </li>
          <li class="megamenu-link">
            <a href="#" class="megamenu-image">
              <picture class="aspect-ratio">
                <img src="<?= get_template_directory_uri() . '/front-end/src/images/parterner-and-awards.png' ?>" alt="">
              </picture>
            </a>
            <div class="megamenu-info">
              <h6 class="megamenu-info-title">Customer Success Stories</h6>
              <h6 class="megamenu-info-description">Delivering effective
                engagement</h6>
            </div>
          </li>
          <li class="megamenu-link">
            <a href="#" class="megamenu-image">
              <picture class="aspect-ratio">
                <img src="<?= get_template_directory_uri() . '/front-end/src/images/leadership-team.png' ?>" alt="">
              </picture>
            </a>
            <div class="megamenu-info">
              <h6 class="megamenu-info-title">Though Leadership Interviews</h6>
              <h6 class="megamenu-info-description">Join us and make a
                difference</h6>
            </div>
          </li>
          <li class="megamenu-link">
            <a href="#" class="megamenu-image">
              <picture class="aspect-ratio">
                <img src="<?= get_template_directory_uri() . '/front-end/src/images/leadership-team.png' ?>" alt="">
              </picture>
            </a>
            <div class="megamenu-info">
              <h6 class="megamenu-info-title">Product Demos</h6>
              <h6 class="megamenu-info-description">Feel free to reach out for
                questions</h6>
            </div>
          </li>
          <li class="megamenu-link">
            <a href="#" class="megamenu-image">
              <picture class="aspect-ratio">
                <img src="<?= get_template_directory_uri() . '/front-end/src/images/leadership-team.png' ?>" alt="">
              </picture>
            </a>
            <div class="megamenu-info">
              <h6 class="megamenu-info-title">Blog</h6>
              <h6 class="megamenu-info-description">Feel free to reach out for
                questions </h6>
            </div>
          </li>
          <div class="megamenu-spacer"></div>
        </ul>
      </li>
      <li class="header-link has-megamenu">
        <a href="#">Resources</a>
        <svg class="dropdown-angle" width="12" height="8" viewBox="0 0 12 8" fill="none">
          <path d="M2 0C1.17595 0 0.705573 0.940764 1.2 1.6L5.2 6.93333C5.6 7.46667 6.4 7.46667 6.8 6.93333L10.8 1.6C11.2944 0.940764 10.824 0 10 0L2 0Z" fill="currentColor"/>
        </svg>
        <div class="megamenu-outside-spacer hide-only-lg"></div>
        <ul class="megamenu box-shadow reset-ul">
          <div class="megamenu-spacer"></div>
          <li class="megamenu-link">
            <a href="#" class="megamenu-image">
              <picture class="aspect-ratio">
                <img src="<?= get_template_directory_uri() . '/front-end/src/images/about-us.png' ?>" alt="">
              </picture>
            </a>
            <div class="megamenu-info">
              <h6 class="megamenu-info-title">All Resources</h6>
              <h6 class="megamenu-info-description">Our Mission Statement and
                Values</h6>
              <ul class="megamenu-links reset-ul">
                <li class="megamenu-sublink">
                  <a href="#">BI/Anlytics</a>
                </li>
                <li class="megamenu-sublink">
                  <a href="#">Cloud Storage</a>
                </li>
                <li class="megamenu-sublink">
                  <a href="#">CRM</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="megamenu-link">
            <a href="#" class="megamenu-image">
              <picture class="aspect-ratio">
                <img src="<?= get_template_directory_uri() . '/front-end/src/images/parterner-and-awards.png' ?>" alt="">
              </picture>
            </a>
            <div class="megamenu-info">
              <h6 class="megamenu-info-title">Customer Success Stories</h6>
              <h6 class="megamenu-info-description">Delivering effective
                engagement</h6>
            </div>
          </li>
          <li class="megamenu-link">
            <a href="#" class="megamenu-image">
              <picture class="aspect-ratio">
                <img src="<?= get_template_directory_uri() . '/front-end/src/images/leadership-team.png' ?>" alt="">
              </picture>
            </a>
            <div class="megamenu-info">
              <h6 class="megamenu-info-title">Though Leadership Interviews</h6>
              <h6 class="megamenu-info-description">Join us and make a
                difference</h6>
            </div>
          </li>
          <li class="megamenu-link">
            <a href="#" class="megamenu-image">
              <picture class="aspect-ratio">
                <img src="<?= get_template_directory_uri() . '/front-end/src/images/leadership-team.png' ?>" alt="">
              </picture>
            </a>
            <div class="megamenu-info">
              <h6 class="megamenu-info-title">Product Demos</h6>
              <h6 class="megamenu-info-description">Feel free to reach out for
                questions</h6>
            </div>
          </li>
          <li class="megamenu-link">
            <a href="#" class="megamenu-image">
              <picture class="aspect-ratio">
                <img src="<?= get_template_directory_uri() . '/front-end/src/images/leadership-team.png' ?>" alt="">
              </picture>
            </a>
            <div class="megamenu-info">
              <h6 class="megamenu-info-title">Blog</h6>
              <h6 class="megamenu-info-description">Feel free to reach out for
                questions </h6>
            </div>
          </li>
          <div class="megamenu-spacer"></div>
        </ul>
      </li>
      <li class="header-link has-megamenu">
        <a href="#">Company</a>
        <svg class="dropdown-angle" width="12" height="8" viewBox="0 0 12 8" fill="none">
          <path d="M2 0C1.17595 0 0.705573 0.940764 1.2 1.6L5.2 6.93333C5.6 7.46667 6.4 7.46667 6.8 6.93333L10.8 1.6C11.2944 0.940764 10.824 0 10 0L2 0Z" fill="currentColor"/>
        </svg>
        <div class="megamenu-outside-spacer hide-only-lg"></div>
        <ul class="megamenu box-shadow reset-ul">
          <div class="megamenu-spacer"></div>
          <li class="megamenu-link">
            <a href="#" class="megamenu-image">
              <picture class="aspect-ratio">
                <img src="<?= get_template_directory_uri() . '/front-end/src/images/about-us.png' ?>" alt="">
              </picture>
            </a>
            <div class="megamenu-info">
              <h6 class="megamenu-info-title">All Resources</h6>
              <h6 class="megamenu-info-description">Our Mission Statement and
                Values</h6>
            </div>
          </li>
          <li class="megamenu-link">
            <a href="#" class="megamenu-image">
              <picture class="aspect-ratio">
                <img src="<?= get_template_directory_uri() . '/front-end/src/images/parterner-and-awards.png' ?>" alt="">
              </picture>
            </a>
            <div class="megamenu-info">
              <h6 class="megamenu-info-title">Customer Success Stories</h6>
              <h6 class="megamenu-info-description">Delivering effective
                engagement</h6>
            </div>
          </li>
          <li class="megamenu-link">
            <a href="#" class="megamenu-image">
              <picture class="aspect-ratio">
                <img src="<?= get_template_directory_uri() . '/front-end/src/images/leadership-team.png' ?>" alt="">
              </picture>
            </a>
            <div class="megamenu-info">
              <h6 class="megamenu-info-title">Though Leadership Interviews</h6>
              <h6 class="megamenu-info-description">Join us and make a
                difference</h6>
            </div>
          </li>
          <li class="megamenu-link">
            <a href="#" class="megamenu-image">
              <picture class="aspect-ratio">
                <img src="<?= get_template_directory_uri() . '/front-end/src/images/leadership-team.png' ?>" alt="">
              </picture>
            </a>
            <div class="megamenu-info">
              <h6 class="megamenu-info-title">Product Demos</h6>
              <h6 class="megamenu-info-description">Feel free to reach out for
                questions</h6>
            </div>
          </li>
          <li class="megamenu-link">
            <a href="#" class="megamenu-image">
              <picture class="aspect-ratio">
                <img src="<?= get_template_directory_uri() . '/front-end/src/images/leadership-team.png' ?>" alt="">
              </picture>
            </a>
            <div class="megamenu-info">
              <h6 class="megamenu-info-title">Blog</h6>
              <h6 class="megamenu-info-description">Feel free to reach out for
                questions </h6>
            </div>
          </li>
          <div class="megamenu-spacer"></div>
        </ul>
      </li>
      <li class="header-button hide-only-lg">
        <a href="#" class="btn btn-border">Login</a>
      </li>
      <li class="header-button hide-only-lg">
        <a href="#" class="btn">Book a demo</a>
      </li>
    </ul>
    <div class="header-buttons hide-between-sm-md">
      <a href="#" class="btn btn-login btn-border">Login</a>
      <a href="#" class="btn btn-book-demo">Book a demo</a>
    </div>
  </div>
</header>
<!--  <div class="page-transition">-->

<!doctype html>
<html <?php language_attributes(); ?>>

<head>

    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="description" content="<?php if (is_single()) {
        single_post_title('', true);
    } else {
        bloginfo('name');
        echo " - ";
        bloginfo('description');
    } ?>"/>
    <meta
            content="width=device-width, initial-scale=1.0, maximum-scale=1, minimum-scale=1.0"
            name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <script>
        const BiggerThanDesignWidth = 1920;
        const designWidth = 1440;
        const desktop = 1440;
        const tablet = 992;
        const mobile = 600;
        const sMobile = 375;

        function fixContainer() {
            const resizeHandler = function () {
                if (window.innerWidth >= BiggerThanDesignWidth) {
                    document.documentElement.style.fontSize = `${10}px`;
                    // document.documentElement.style.fontSize = `${9 * window.innerWidth / desktop}px`;
                } else if (window.innerWidth < BiggerThanDesignWidth && window.innerWidth >= desktop) {
                    document.documentElement.style.fontSize = `${10 * window.innerWidth / desktop}px`;
                } else if (window.innerWidth < desktop && window.innerWidth >= tablet) {
                    document.documentElement.style.fontSize = `${10 * window.innerWidth / designWidth}px`;
                } else if (window.innerWidth < tablet && window.innerWidth >= mobile) {
                    document.documentElement.style.fontSize = `${10 * window.innerWidth / tablet}px`;
                } else if (window.innerWidth < mobile && window.innerWidth >= sMobile) {
                    document.documentElement.style.fontSize = `${10}px`;
                } else {
                    document.documentElement.style.fontSize = `${10 * window.innerWidth / sMobile}px`;
                }
            };
            resizeHandler();
            window.addEventListener('resize', resizeHandler);
        }

        fixContainer()
    </script>

    <style>
        /* latin-ext */
        @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/lato/v23/S6uyw4BMUTPHjxAwXjeu.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */
        @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/lato/v23/S6uyw4BMUTPHjx4wXg.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin-ext */
        @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 700;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/lato/v23/S6u9w4BMUTPHh6UVSwaPGR_p.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */
        @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 700;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/lato/v23/S6u9w4BMUTPHh6UVSwiPGQ.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin-ext */
        @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 900;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/lato/v23/S6u9w4BMUTPHh50XSwaPGR_p.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */
        @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 900;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/lato/v23/S6u9w4BMUTPHh50XSwiPGQ.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

    </style>
    <!-- Third party code ACF-->
    <?php
    $code_in_head_tag = get_field('code_in_head_tag', 'options');
    $code_before_body_tag_after_head_tag = get_field('code_before_body_tag_after_head_tag', 'options');
    $code_after_body_tag = get_field('code_after_body_tag', 'options');
    ?>
    <?php wp_head(); ?>
    <?= $code_in_head_tag ?>
</head>

<?= $code_before_body_tag_after_head_tag ?>
<!--preloader style-->
<style>
    body:not(.loaded) {
        opacity: 0;
    }

    body:not(.loaded) * {
        transition: none !important;
    }

    body {
        transition: opacity .5s;
    }

    [modal-content] {
        display: none !important;
    }


    /*  region image image-placeholder*/
    @keyframes placeHolderShimmer {
        0% {
            -webkit-transform: translateZ(0);
            transform: translateZ(0);
            background-position: -468px 0
        }
        to {
            -webkit-transform: translateZ(0);
            transform: translateZ(0);
            background-position: 468px 0
        }
    }

    .image-placeholder {
        max-width: 100%;
        max-height: 100%;
        background: #e6e6e6;
        display: block;
        transition: opacity 300ms ease, height 300ms ease;
        will-change: transform;
        animation: placeHolderShimmer 1s linear infinite forwards;
        -webkit-backface-visibility: hidden;
        background: linear-gradient(90deg, #eee 8%, #ddd 18%, #eee 33%);
        background-size: 800px 104px;
        margin: 0 auto;
    }

    .image-placeholder.image-placeholder-aspect-ratio {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    .image-placeholder.image-placeholder-hidden {
        opacity: 0;
        height: 0 !important;
    }

    /*  endregion image image-placeholder*/

    /**:focus{border:2px solid red};*/

</style>

<!--end preloader style-->
<!-- ACF Fields -->
<?php
$header_logo = get_field('header_logo', 'options');
$is_image_or_svg = @$header_logo['is_image_or_svg'];
$logo_image = @$header_logo['logo_image'];
$logo_svg = @$header_logo['logo_svg'];
$cta_button = get_field('cta_button', 'options');
$menu_links = get_field('menu_links', 'options')
?>
<!-- END ACF -->
<body <?php body_class(); ?> data-barba="wrapper">
<a skip-to-main-content href="#main-content"> <?= __('Skip to main content', 'propellerhealth') ?></a>
<?= $code_after_body_tag ?>
<header class="fixed" role="banner">
    <div class="bottom-menu">
        <div class="logo-and-menu">
            <?php if ($header_logo) { ?>
                <a class="header-logo" role="button" target="_self" aria-lable="go to home page"
                   href="<?= site_url(); ?>" title="PropellerHealth Home">
                    <?php if (!$is_image_or_svg && $logo_image) { ?>
                        <img  class="header-logo-img" src="<?= $logo_image['url'] ?>" alt="<?= $logo_image['alt'] ?>">
                    <?php } else {
                        echo $logo_svg;
                    }
                    ?>
                </a>
            <?php } ?>
            <!--          burger menu and cross-->
            <div class="burger-and-menu" role="button" aria-pressed="true" aria-haspopup="true" data-bs-toggle="collapse" aria-label="navbar menu">
                <div class="menu">
                    <button aria-label="navbar menu" class="burger-menu" id="burger-menu">
                        <span style="transform-origin: 50% 50%; transform: translate(0px, 0px); opacity: 1; visibility: inherit;"></span>
                        <span style="transform-origin: 50% 50%; transform: translate(0px, 0px); opacity: 1; visibility: inherit;"></span>
                        <span style="transform-origin: 50% 50%; transform: translate(0px, 0px); opacity: 1; visibility: inherit;"></span>
                    </button>
                </div>
            </div>
        </div>
        <nav class="navbar ">
            <div class="navbar-wrapper">
                <div class="menu-top-menu-container">
                    <?php if (have_rows('top_links', 'options')) { ?>
                        <ul  role="menubar" aria-label='top nav menu' id="menu-top-menu" class="top-menu reset-ul">
                            <?php while (have_rows('top_links', 'options')) {
                                the_row();
                                $link = get_sub_field('link');
                                ?>
                                <li role="none" class="menu-item ">
                                    <a href="<?= $link['url'] ?>"
                                       target="<?= $link['target'] ?>"
                                       aria-expanded="false"
                                       aria-haspopup='false'
                                       tabindex="-1"
                                       role="menuitem"
                                       class="paragraph p-link header-link"><?= $link['title'] ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                    <div role="search" class="lg-search">
                        <?php get_search_form(); ?>
                    </div>
                </div>
                <div role="search" class="input-search-wrapper">
                    <?php get_search_form(); ?>
                </div>
                <div class="main-menu">
                    <?php if (have_rows('menu_links', 'options')) { ?>
                        <div role="navigation">
                            <ul role="menubar" aria-label='main links' class="primary-menu reset-ul">
                                <?php while (have_rows('menu_links', 'options')) {
                                    the_row();
                                    $menu_link = get_sub_field('menu_link');
                                    $has_sub_menu = get_sub_field('has_sub_menu');

                                    if ($menu_link) { ?>
                                        <li role="none" class="menu-item <?= $has_sub_menu ? 'has-megaMenu' : '' ?> ">
                                            <a href="<?= $menu_link['url'] ?>"
                                               class="paragraph menu-link header-link"
                                               tabindex="0"
                                            >
                                                <?= $menu_link['title'] ?></a>
                                            <?php if ($has_sub_menu === true) { ?>
                                                <div role="menuitem"
                                                     aria-expanded="false"
                                                     aria-haspopup='<?= $has_sub_menu ? 'true' : 'false' ?>'
                                                     tabindex="0"
                                                     data-bs-toggle="dropdown"
                                                     role="button" aria-label="open sub menu" class="svg-wrapper">
                                                    <svg aria-label="open sub menu" width="12" height="8" viewBox="0 0 12 8"
                                                         fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1 1L6 6L11 1" stroke="#003B72" stroke-width="2"/>
                                                    </svg>
                                                </div>
                                                <div class="sub-menu">
                                                    <?php if (have_rows('sub_menu')) { ?>
                                                        <ul class="list-items"
                                                            aria-hidden="true"
                                                            tabindex="-1"
                                                            aria-label="sub menu"
                                                            role="menu">
                                                            <?php while (have_rows('sub_menu')) {
                                                                the_row();
                                                                $sub_menu_link = get_sub_field('sub_menu_link');
                                                                ?>
                                                                <?php if ($sub_menu_link) { ?>
                                                                    <li role="none"tabindex="-1"  class="item-1 ">
                                                                        <a href="<?= $sub_menu_link['url'] ?>"
                                                                           class="paragraph header-sublink"
                                                                           role="menuitem"
                                                                           target="<?= $sub_menu_link['target'] ?>"
                                                                        ><?= $sub_menu_link['title'] ?></a>
                                                                    </li>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </ul>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php } ?>
                    <?php if ($cta_button) { ?>
                        <a role="button" href="<?= $cta_button['url'] ?>"
                           target="<?= $cta_button['target'] ?>"
                           class="cta-button"><?= $cta_button['title'] ?></a>
                    <?php } ?>
                </div>
            </div>
        </nav>

    </div>
</header>
<main id="main-content" role="main">

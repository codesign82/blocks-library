<?php
// Create id attribute allowing for custom "anchor" value.
$id = 'move_technology_block';
// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'move_technology_block';
$className = 'move_technology_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/move_technology_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$text_with_gradient = get_field('text_with_gradient');
$first_text = @$text_with_gradient['first_text'];
$gradient_text = @$text_with_gradient['gradient_text'];
$last_text = @$text_with_gradient['last_text'];
$description = get_field('description');
$image = get_field('image');

?>
<!-- region MOVING's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="move-technology section-border">
        <div class="content">
            <h2 class="headline-1 headline-1-gradient title-circle-wrapper a7a-test-from0-bottom">
                <svg class="title-circle dark" width="34" height="34" viewBox="0 0 34 34" fill="none">
                    <circle cx="17" cy="17" r="15" stroke="#DEDFE3" stroke-width="3"/>
                </svg>
                <?php if ($first_text) { ?>
                    <?= $first_text ?>
                <?php } ?>

                <?php if ($gradient_text) { ?>
                    <span class="blue-gradiant"><?= $gradient_text ?></span>
                <?php } ?>

                <?php if ($last_text) { ?>
                    <?= $last_text ?>
                <?php } ?>
            </h2>
            <?php if ($description) { ?>
                <div class="paragraph a7a-test-from0-bottom"><?= $description ?></div>
            <?php } ?>
        </div>
        <?php if ($image) { ?>
            <div class="image-wrapper a7a-test-from0-bottom">
                <picture>
                    <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                </picture>
            </div>
        <?php } ?>
    </div>
</div>

</section>


<!-- endregion MOVING's Block -->

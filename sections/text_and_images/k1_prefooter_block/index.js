import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/propellerhealth';

const blockScript = async (container = document) => {
  const block = container.querySelector('.prefooter_block');

  const customModal = block.querySelector('.custom-modal');
  const openModel = block.querySelector('.open-contact-us-model');
  const starts = block.querySelectorAll(".stars-repeat");
  starts.forEach((star, index) => {
    gsap.timeline({
      repeat: -1, repeatDelay: 5, delay: index * 1.5
    })

        .fromTo(star, {
          opacity: 1,
        }, {
          repeat: 1,
          yoyo: true,
          opacity: 0,
          duration: 1,
          repeatDelay: 0,
        })
  })

  openModel?.addEventListener('click', function () {
    openModel.blur();
    // // region prevent page scroll
    // document.documentElement.classList.add('modal-opened')
    // const scrollEnabled = new Event('scroll-disabled');
    // document.dispatchEvent(scrollEnabled);
    // // endregion prevent page scroll
    customModal.classList.add('modal-active');
  });

  if (customModal) {
    const customModalInner = customModal.querySelector('.custom-modal-inner');
    const closeModalElm = customModalInner.querySelector('.close-modal');

    const closeModal = (e) => {
      if (e && e.target.classList.contains('contact-form-title')) return ;
      // // region allow page scroll
      // const scrollEnabled = new Event('scroll-enabled');
      // document.dispatchEvent(scrollEnabled);
      // document.documentElement.classList.remove('modal-opened')
      // // endregion allow page scroll
      customModal.classList.remove('modal-active');
    }

    const keyHandler = (e) => {
      if (e.key === 'Escape') {
        closeModal();
      }
    }
    window.addEventListener('keydown', keyHandler)
    pageCleaningFunctions.push(() => window.removeEventListener('keydown', keyHandler))

    customModal?.addEventListener('click', closeModal);
    customModalInner?.addEventListener('click', e => e.stopPropagation());
    closeModalElm?.addEventListener('click', closeModal);
  }


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


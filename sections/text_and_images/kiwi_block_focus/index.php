<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'block-focus';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/block-focus/screenshot1.png" >';
  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$override_the_general_options = get_field('override_the_general_options');
$title_results_focused = get_field('title_results_focused', 'options');
$sub_title_results_focused = get_field('sub_title_results_focused', 'options');
$link_results_focused = get_field('link_results_focused', 'options');
$what_do_you_want_to_show = get_field('what_do_you_want_to_show', 'options');
$posts_results_focused = get_field('posts_results_focused', 'options');


$different_first_card = get_field('different_first_card');
$first_card = get_field('first_card');
$title = get_field('title');
$sub_title = get_field('sub_title');
$link = get_field('link');
$posts = get_field('posts');
if (!$override_the_general_options) {
  $title = $title_results_focused;
  $sub_title = $sub_title_results_focused;
  $link = $link_results_focused;
  $posts = $posts_results_focused;
} else {
  $what_do_you_want_to_show = 'custom_posts';
}

?>
<!-- region Kiwi's Block -->
<?php general_settings_for_blocks($id, $className); ?>
<div class="container block-focus-wrapper">
  <?php if ($title) { ?>
    
    <div class="title d-flex align-items-end justify-content-between flex-wrap flex-md-nowrap">
      <div class="wrapper">
        <h3 class="headline-3 word-up"><?=$title?></h3>
        <?php if ($sub_title) { ?>
          <div class="paragraph word-up"><?=$sub_title?></div>
        <?php } ?>
      </div>
      <?php if ($link) { ?>
        <a class="view-news link word-up" href="<?=$link['url']?>" target="<?=$link['target']?>">
          <?=$link['title']?>
        </a>
      <?php } ?>
    </div>
  <?php } ?>
  <div class="row gutter--115">
    <?php
    if ($what_do_you_want_to_show != 'custom_posts') {
      $args = array(
        'posts_per_page' => 2
      );
      $the_query = new WP_Query($args); ?>
      <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
        <div class="col-12 col-lg-6 iv-st-from-bottom">
          <div class="circles-container">
            <a class="focus-card hover-zoom-child image-plus-layout" href="<?php the_permalink(); ?>">
              <div class="black-layout"></div>
              <img alt="Post Image" class="zoom-in image" src="<?php the_post_thumbnail_url(); ?>"/>
              <div class="wrapper">
                <h6 class="headline-6 heavy word-up"><?=get_the_date(); ?></h6>
                <div class="bottom-side">
                  <p class="paragraph custom word-up"><?php the_title(); ?></p>
                  <span class="link word-up">Read article</span>
                </div>
              </div>
            </a>
          </div>
        </div>
      <?php endwhile;
      wp_reset_postdata();
    } else {
      if ($different_first_card) { ?>
        
        <div class="col-12 col-lg-6">
          <div class="circles-container">
            <div class="focus-card focus-border">
              <div class="wrapper">
                <h6 class="headline-6 heavy word-up"><?=$first_card['title']?></h6>
                <div class="bottom-side">
                  <div class="paragraph custom real-line-up">
                    <a href="<?=$first_card['link']['url']?>" target="<?=$first_card['link']['target']?>"><?=$first_card['text']?></a></div>
                  <a class="link iv-st-from-bottom" target="<?=$first_card['link']['target']?>" href="<?=$first_card['link']['url']?>"><?=$first_card['link']['title']?></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php }
      if ($posts): ?>
        <?php foreach ($posts as $i => $spost):
          $permalink = get_permalink($spost->ID);
          $title = get_the_title($spost->ID);
          $date = get_the_date('',$spost->ID);
          $image = get_the_post_thumbnail_url($spost->ID);
          ?>
          <div class="col-12 col-lg-6">
            <div class="circles-container">
              <a class="focus-card hover-zoom-child image-plus-layout" href="<?=$permalink?>">
                <div class="black-layout"></div>
                <img alt="Post Image" class="zoom-in image" src="<?=$image?>"/>
                <div class="wrapper">
                  <h6 class="headline-6 heavy word-up"><?=$date?></h6>
                  <div class="bottom-side">
                    <p class="paragraph custom real-line-up"><?=$title?></p>
                    <span class="link iv-st-from-bottom">Read article</span>
                  </div>
                </div>
              </a>
            </div>
          </div>
          <?php if ($different_first_card && $i == 0) {
          break;
        } endforeach; ?>
        <?php
        wp_reset_postdata(); ?>
      <?php endif; ?>
    <?php } ?>
  </div>
</div>
</section>

<!-- endregion Kiwi's Block -->

import './index.html';
import './style.scss';
// import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/wrap';
import Swiper, {Autoplay} from 'swiper';
import Scrollbar from "smooth-scrollbar";
import {allowPageScroll, preventPageScroll} from "../../../scripts/functions/prevent_allowPageScroll";
import {debounce} from "../../../scripts/functions/debounce";


Swiper.use([Autoplay]);
const blockScript = async (container = document) => {
  
  const blocks = container.querySelector('.block-focus');
 


animations(blocks);
  imageLazyLoading(blocks);
};
windowOnLoad(blockScript);




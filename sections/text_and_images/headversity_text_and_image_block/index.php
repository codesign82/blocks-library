<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'text_and_image_block';
$className = 'text_and_image_block';
$is_reversed = get_field('is_reversed');
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}


if ($is_reversed) {
    $className .= ' reversed';
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/text_and_image_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$image = get_field('image');
$cta = get_field('cta');

?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="text-and-image-wrapper">
        <div class="left-content">
            <?php if ($image) { ?>
                <picture class="text-img aspect-ratio">
                    <?= get_acf_image($image, 'img-730-800') ?>
                </picture>
            <?php } ?>
        </div>
        <div class="right-content">
            <?php if ($title) { ?>
                <h3 move-to-here class="headline-3 right-text iv-st-from-bottom"><?= $title ?></h3>
            <?php } ?>
            <?php if ($description) { ?>
                <div class="paragraph l-paragraph right-description iv-st-from-bottom">
                    <?= $description ?>
                </div>
            <?php } ?>
            <?php if ($cta) { ?>
                <div class="iv-st-from-bottom">
                    <a href="<?= $cta['url'] ?>" class="btn" target="<?= $cta['target'] ?>"><?= $cta['title'] ?></a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
</section>


<!-- endregion headversity's Block -->

<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'promo_with_image_block';
$className = 'promo_with_image_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/promo_with_image_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$image = get_field('image');
$image_left = get_field('image_left');
$title = get_field('title');
$subtitle = get_field('subtitle');
$description = get_field('description');
$cta_button = get_field('cta_button');
$cta_link = get_field('cta_link');
?>
<!-- region advisorhub's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="promo-with-img  <?= $image_left? 'image-left':'' ?>">
        <div class="left-side">
            <div class="promo-content">
                <?php if($title){ ?>
                    <div class="headline-2 promo-title"><?=$title?></div>
                <?php } ?>
                <?php if($subtitle){ ?>
                    <div class="headline-4 seirf promo-text1"><?=$subtitle ?></div>
                <?php } ?>
                <?php if($description){ ?>
                    <div class="paragraph-14 paragraph-Seirf promo-text2">
                        <?=$description ?>
                    </div>
                <?php } ?>
                <div class="buttons">
                    <?php if($cta_button){ ?>
                        <a href="<?=$cta_button['url'] ?>"
                           target="<?=$cta_button['target'] ?>" class="ah-btn"><?=$cta_button['title'] ?></a>
                    <?php } ?>
                    <?php if($cta_link){?>
                        <a href="<?=$cta_link['url'] ?>"
                           target="<?=$cta_link['target'] ?>" class="link"><?=$cta_link['title'] ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="right-side">
            <?php if($image){ ?>
                <picture class="aspect-ratio">
                    <img src="<?=$image['url'] ?>" alt="<?=$image['alt'] ?>"/>
                </picture>
            <?php } ?>
        </div>
    </div>
</div>
</section>


<!-- endregion advisorhub's Block -->

<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'large_icon_card_block';
$className = 'large_icon_card_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/large_icon_card_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$image = get_field('image');
$icon_square = get_field('icon_square');
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="platform-wrapper">
        <div class="platform-content">
            <?php if ($title) { ?>
                <h3 move-to-here class="headline-3 platform-title iv-st-from-bottom"><?= $title ?></h3>
            <?php } ?>
            <?php if ($description) { ?>
                <div
                        class="paragraph paragraph-m-paragraph iv-st-from-bottom"><?= $description ?> </div>
            <?php } ?>
        </div>
        <?php if ($image) { ?>
            <div class="platform-media">
                <div class="absolute-media">
                    <picture class="aspect-ratio">
                        <img
                                src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                    </picture>
                </div>
            </div>
        <?php } ?>
    </div>
    <?php if ($icon_square) { ?>
        <div class="advantage-wrapper iv-st-from-bottom">
            <div class="advantage-parent">
                <?php if (have_rows('icon_square')) {
                    while (have_rows('icon_square')) {
                        the_row();
                        $icon = get_sub_field('icon');
                        $title_sub = get_sub_field('title');
                        $description_sub = get_sub_field('description');
                        ?>
                        <div class="advantage">
                            <div>
                                <?php if ($icon) { ?>
                                    <div class="advantage-icon">
                                        <?= $icon ?>
                                    </div>
                                <?php } ?>
                                <?php if ($title_sub) { ?>

                                    <h5 class="advantage-title headline-5 "><?= $title_sub ?></h5>
                                <?php } ?>
                                <?php if ($description_sub) { ?>
                                    <div
                                            class="advantage-description paragraph paragraph-s-paragraph ">
                                        <?= $description_sub ?></div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php }
                } ?>
            </div>
        </div>
    <?php } ?>
</div>
</section>


<!-- endregion headversity's Block -->

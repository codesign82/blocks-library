<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass      = 'about_products_block';
$className      = 'about_products_block';
$image_in_right = get_field( 'image_in_right' );
$image_in_right = $image_in_right ? 'reverse' : '';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/about_products_block/screenshot.png" >';

  return;
endif;


if ( $image_in_right ) {
  $className .= ' ' . $image_in_right;

}
/****************************
 *     Custom ACF Meta      *
 ****************************/
$title            = get_field( 'title' );
$description      = get_field( 'description' );
$cta_button       = get_field( 'cta_button' );
$background_image = get_field( 'background_image' );
?>
<!-- region TowSmart's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container container-large">
  <div class="content-wrapper">
    <?php if ( $background_image ) { ?>
      <picture class="about-products-img">
        <img data-src="<?= $background_image['url'] ?>" alt="<?= $background_image['alt'] ?>">
      </picture>
    <?php } ?>
    <div class="about-products-wrapper">
      <?php if ( $title ) { ?>
        <h2 class="headline-2 iv-st-from-bottom"><?= $title ?></h2>
      <?php } ?>
      <?php if ( $description ) { ?>
        <div class="paragraph paragraph-16 iv-st-from-bottom"><?= $description ?></div>
      <?php } ?>
      <?php if ( $cta_button ) { ?>
        <a class="btn-arrow iv-st-from-bottom" href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>"><?= $cta_button['title'] ?>
          <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M8.27329 6.45955L9.10663 7.33288H5.33329C5.15648 7.33288 4.98691 7.40312 4.86189 7.52815C4.73686 7.65317 4.66663 7.82274 4.66663 7.99955C4.66663 8.17636 4.73686 8.34593 4.86189 8.47095C4.98691 8.59598 5.15648 8.66622 5.33329 8.66622H9.05996L8.19329 9.52622C8.06776 9.65175 7.99723 9.82202 7.99723 9.99955C7.99723 10.1771 8.06776 10.3473 8.19329 10.4729C8.31883 10.5984 8.48909 10.6689 8.66663 10.6689C8.84416 10.6689 9.01442 10.5984 9.13996 10.4729L11.14 8.47288C11.2007 8.40948 11.2482 8.33472 11.28 8.25288C11.3152 8.17308 11.3334 8.0868 11.3334 7.99955C11.3334 7.9123 11.3152 7.82602 11.28 7.74622C11.2642 7.70702 11.2416 7.67092 11.2133 7.63955C11.1974 7.60238 11.1748 7.56847 11.1466 7.53955L9.23996 5.53955C9.11796 5.41136 8.95003 5.33689 8.77312 5.33251C8.59622 5.32814 8.42481 5.39422 8.29663 5.51622C8.16844 5.63822 8.09396 5.80614 8.08959 5.98305C8.08521 6.15996 8.15129 6.33136 8.27329 6.45955Z" fill="white"/>
            <path d="M1.33337 8.00032C1.33337 9.31886 1.72437 10.6078 2.45691 11.7041C3.18945 12.8005 4.23064 13.6549 5.44882 14.1595C6.66699 14.6641 8.00744 14.7961 9.30064 14.5389C10.5938 14.2817 11.7817 13.6467 12.7141 12.7144C13.6464 11.782 14.2814 10.5941 14.5386 9.30092C14.7958 8.00772 14.6638 6.66727 14.1592 5.4491C13.6547 4.23092 12.8002 3.18974 11.7038 2.45719C10.6075 1.72465 9.31858 1.33366 8.00004 1.33366C7.12456 1.33366 6.25765 1.50609 5.44882 1.84113C4.63998 2.17616 3.90505 2.66722 3.286 3.28628C2.66694 3.90533 2.17588 4.64026 1.84084 5.4491C1.50581 6.25794 1.33337 7.12484 1.33337 8.00032ZM13.3334 8.00032C13.3334 9.05516 13.0206 10.0863 12.4345 10.9634C11.8485 11.8404 11.0156 12.524 10.041 12.9277C9.06648 13.3313 7.99412 13.437 6.95956 13.2312C5.92499 13.0254 4.97468 12.5174 4.2288 11.7716C3.48292 11.0257 2.97497 10.0754 2.76919 9.0408C2.5634 8.00624 2.66902 6.93388 3.07268 5.95934C3.47635 4.98481 4.15994 4.15185 5.037 3.56582C5.91406 2.97978 6.94521 2.66699 8.00004 2.66699C9.41453 2.66699 10.7711 3.22889 11.7713 4.22909C12.7715 5.22928 13.3334 6.58583 13.3334 8.00032Z" fill="white"/>
          </svg>
        </a>
      <?php } ?>
    </div>
  </div>
</div>
</section>


<!-- endregion TowSmart's Block -->

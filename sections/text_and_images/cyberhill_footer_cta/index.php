<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'footer_cta_block';
$className = 'footer_cta_block no-side-border';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/footer_cta_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$space_color = get_field('space_color');
$sub_title = get_field('sub_title');
$description = get_field('description');
$cta_button = get_field('cta_button');
$link = get_field('link');
$right_image = get_field('right_image');
?>
<style>
  <?php if ($space_color){ ?>
  <?='#'.$id?>
  :after {
    background-color: <?=$space_color?>;
  }

  <?php } ?>
</style>

<!-- region CyberHill's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="footer-cta-wrapper">
  <div class="left-content">
    <picture class="hover-image">
      <img src=" <?= get_template_directory_uri() . '/front-end/src/images/image-hover.png' ?>" alt="hover-image">
    </picture>
    <?php if ($sub_title) { ?>
      <div class="title-with-shape iv-st-from-bottom"><?= $sub_title ?></div>
    <?php } ?>
    <?php if ($title) { ?>
      <h2 class="headline-3 iv-st-from-bottom"><?= $title ?></h2>
    <?php } ?>
    <?php if ($description) { ?>
      <div class="paragraph paragraph-white iv-st-from-bottom">
        <?= $description ?>
      </div>
    <?php } ?>
    <div class="button-and-link">
      <?php if ($cta_button) { ?>
        <a class="primary-button primary-button-white iv-st-from-bottom hover-state" href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>"><?= $cta_button['title'] ?></a>
      <?php } ?>
      <?php if ($link) { ?>
        <a href="<?= $link['url'] ?>" class="text-link text-link-with-border white-color  hover-state iv-st-from-bottom" target="<?= $link['target'] ?>">
          <?= $link['title'] ?></a>
      <?php } ?>
    </div>
  </div>
  <div class="right-content" data-reveal-direction="right">
    <?php if ($right_image) { ?>
      <picture class="image">
        <img src="<?= $right_image['url'] ?>" alt="<?= $right_image['alt'] ?>">
      </picture>
    <?php } ?>
    <picture class="layout">
      <img src="<?= get_template_directory_uri() . '/front-end/src/images/layout.png' ?>" alt="">
    </picture>
  </div>
</div>
</section>


<!-- endregion CyberHill's Block -->

import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/cyberhill';

const blockScript = async (container = document) => {
  const block = container.querySelector('.footer_cta_block');
  
  let HoverState = block.querySelectorAll('.hover-state');
  
  
  // adding the event listener by looping
  
  HoverState.forEach(hover => {
    hover.addEventListener('mouseover', (e)=>{
      block.classList.toggle('hovered-animation');
    });
    hover.addEventListener('mouseleave', (e)=>{
      block.classList.toggle('hovered-animation');
    });
  });

  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


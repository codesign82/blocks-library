<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'request_a_demo_section';
$className = 'request_a_demo_section';
$is_reversed = get_field('is_reversed');
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if ($is_reversed) {
    $className .= ' reverse-dir';
}

if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/request_a_demo_section/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$cta_button = get_field('cta_button');
$image = get_field('image');
$background_color = get_field('background_color');
?>


<!-- region samrt_dv Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="background-section " style="--background: <?= $background_color ?>">
    <div class="container">
        <div class="content">
            <div class="left-text">
                <div class="text">
                    <?php if ($title) { ?>
                        <div class=" title headline-2 word-up"><?= $title ?></div>
                    <?php } ?>
                    <?php if ($description) { ?>
                        <div class="paragraph iv-st-from-bottom">
                            <?= $description ?>
                        </div>
                    <?php } ?>
                </div>
                <?php if ($cta_button) { ?>
                    <a class="cta-button iv-st-from-bottom" href="<?= $cta_button['url'] ?>"
                       target="<?= $cta_button['target'] ?>" style="opacity: 1;">
                        <?= $cta_button['title'] ?>
                    </a>
                <?php } ?>
            </div>
            <div class="right-image iv-st-from-bottom">
                <?php if ($image) { ?>
                    <picture class="aspect-ratio img-wrapper">
                        <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                    </picture>
                <?php } ?>
            </div>
        </div>
    </div>
</div>


</section>


<!-- endregion samrt_dv Block -->

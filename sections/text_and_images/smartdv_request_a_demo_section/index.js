import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";

import '../../../scripts/sitesSizer/propellerhealth';

import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger)


const blockScript = async (container = document) => {
  const block = container.querySelector('.request_a_demo_section');



  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'content_and_media_block';
$className = 'content_and_media_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/takeoff-code/gutenberg-blocks/content_and_media_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$cta_link = get_field('cta_link');


$video = get_field('video');
// Full Video
$video_type = @$video['video_type'];
$video_url = @$video['video_url'];
$video_file = @$video['video_file'];
$vimeo_url = @$video['vimeo_url'];

// DO Not Delete Video Type For JS
$poster_image = get_field('poster_image');
?>
<!-- region senet's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<div class="container">
    <div class="main-content ">
        <div class="left-content">
            <?php if ($title) { ?>
                <div class="title headline-2 "> <?= $title ?></div>
            <?php } ?>
            <?php if ($description) { ?>
                <div class="desc paragraph"> <?= $description ?></div>
            <?php } ?>
            <?php if ($cta_link) { ?>
                <a class="cta-btn" href="<?= $cta_link['url'] ?>"
                   target="<?= $cta_link['target'] ?>">  <?= $cta_link['title'] ?></a>
            <?php } ?>

        </div>
        <div class="right-content">
            <div class="video-section">
                <div
                    class="video-wrapper aspect-ratio"
                    tabindex="0"
                    aria-label="<?= __('open video', 'senet') ?>"
                    role="button"
                    data-video-type="<?= $video_type ?>"
                >
                    <?php if ($poster_image) { ?>
                        <picture>
                            <img src="<?= $poster_image['url'] ?>" alt="<?= $poster_image['alt'] ?>">
                        </picture>
                    <?php } ?>
                    <?php if ($video) { ?>
                        <?php if ($video_type === 'vimeo') { ?>
                            <div id="vimeo-video" data-url="<?= $vimeo_url ?>"></div>
                            <?php
                        } else {
                            ?>
                            <video class="video" src="<?= $video_type === 'url' ? $video_url : $video_file ?>"
                                   type="video/mp4" playsinline controls></video>
                            <?php
                        } ?>
                    <?php } ?>
                </div>
                <?php if ($poster_image) { ?>
                    <div class="open-video" role="button" aria-label="play button">
                        <svg width='174' height='174' viewBox='0 0 174 174' fill='none'
                             xmlns='http://www.w3.org/2000/svg'>
                            <path d='M110.766 80.2344C112.734 81.5 114 83.75 114 86C114 88.3906 112.734 90.6406 110.766 91.7656L70.2656 116.516C68.1562 117.781 65.4844 117.922 63.375 116.656C61.2656 115.531 60 113.281 60 110.75V61.25C60 58.8594 61.2656 56.6094 63.375 55.4844C65.4844 54.2188 68.1562 54.2188 70.2656 55.625L110.766 80.2344Z'
                                  fill='white'/>
                            <circle cx='87' cy='87' r='81.5' stroke='white' stroke-width='11'/>
                        </svg>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
</section>

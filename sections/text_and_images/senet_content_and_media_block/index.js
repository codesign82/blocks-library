import './index.html';
import './style.scss';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/propellerhealth';
import {vimeoPlayer} from "../../../scripts/general/vimeo-player";



const blockScript = async (container = document) => {
  const block = container.querySelector('.content_and_media_block');

  const videoWrapper = block.querySelector(".video-wrapper");
  const video = block.querySelector("video");
  const vimeoVideoEl = block.querySelector("#vimeo-video");
  const openVideo = block.querySelector(".open-video");
  const videoType = videoWrapper.dataset.videoType;

  let player = '';

  if (videoType === "vimeo") {
    player = vimeoPlayer(vimeoVideoEl, vimeoVideoEl.dataset.url);
  }

  if (openVideo) {
    openVideo.addEventListener('click', () => {
      openVideo.closest('.video-section ').classList.add('active-video');
      if (videoType === "vimeo") {
        player.play();
      } else if (["url", "file"].includes(videoType)) {
        video && video.play();
      }
    })
  }


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


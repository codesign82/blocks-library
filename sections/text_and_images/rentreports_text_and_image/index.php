<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'text_and_image_card_block';
$extra_part_position = get_field('extra_part_position');
$extra_part_color = get_field('extra_part_color');
$extra_part_desktop_height = get_field('extra_part_desktop_height');
$extra_part_mobile_height = get_field('extra_part_mobile_height');
$className = 'text_and_image_card_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/text_and_image_card_block/screenshot.png" >';
  
  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$image = get_field('image');
$is_has_subtitle = get_field('is_has_subtitle');
$subtitle = get_field('subtitle');
$title = get_field('title');
$description = get_field('description');
$is_has_cta_button = get_field('is_has_cta_button');
$is_has_video = get_field('is_has_video');
$video_details = get_field('video_details');
$cta_button = get_field('cta_button');
$is_has_terms = get_field('is_has_terms');
$terms = get_field('terms');

$video_url = generateVideoEmbedUrl($video_details['video_url']);
?>
<style>
  <?='#'.$id?>:before {
    
    background: <?=$extra_part_color?$extra_part_color:'#fff'?>;
  
  <?=$extra_part_position?'top:-0.1rem':'bottom:-0.1rem'?>;
  
  
  }
  
  <?php if($extra_part_mobile_height){ ?>
  @media screen and (max-width: 599.98px) {
    <?='#'.$id?>:before {
      height: <?=$extra_part_mobile_height?>%;
    }
  }
  
  <?php } ?>
  <?php if($extra_part_desktop_height){ ?>
  @media screen and (min-width: 600px) {
    <?='#'.$id?>:before {
      height: <?=$extra_part_desktop_height?>%;
    }
  }
  
  <?php } ?>
</style>
<!-- region RentReporters's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="text-and-image-card">
    <?php if ($image) { ?>
        <picture class="text-card-image text-and-image">
        <img class="zoom-in" data-src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
      </picture>
    <?php } ?>
    <div class="text-card-content text-and-image">
      <?php if ($subtitle) { ?>
          <h3 class="headline-3 iv-st-from-bottom-f"><?= $subtitle ?></h3>
      <?php } ?>
      <?php if ($title) { ?>
          <div class="title headline-1 iv-st-from-bottom-f"><?= $title ?></div>
      <?php } ?>
      <?php if ($description) { ?>
          <div class="wysiwyg-block description iv-st-from-bottom-f"><?= $description ?></div>
      <?php } ?>
      <?php if ($cta_button) { ?>
          <a href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>" class="btn has-border iv-st-from-bottom-f"><?= $cta_button['title'] ?></a>
      <?php } ?>
      <?php if ($is_has_video) { ?>
        <?php if ($video_url) { ?>
          <div class="video-wrapper iv-st-from-bottom-f" data-src-video="<?= $video_url ?>"></div>
        <?php } ?>
        <?php if ($video_details['btn_text']) { ?>
            <button aria-label="Open Video Model" class="btn has-border watch-video iv-st-from-bottom-f"><?= $video_details['btn_text'] ?></button>
        <?php } ?>
      <?php } ?>
      <?php if ($is_has_terms) { ?>
        <div class="terms">
          <?php if ($terms['title']) { ?>
                <h4 class="headline-4 iv-st-from-bottom-f"><?= $terms['title'] ?></h4>
          <?php } ?>
          <?php if ($terms['description']) { ?>
                <div class="terms-text iv-st-from-bottom-f"><?= $terms['description'] ?></div>
          <?php } ?>
        </div>
      <?php } ?>
    </div>
  </div>
</div>
</section>
<!-- endregion RentReporters's Block -->

import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {preventPageScroll} from "../../../scripts/functions/prevent_allowPageScroll";
// import '../../../scripts/sitesSizer/wrap'

const blockScript = async (container = document) => {
  const blocks = container.querySelector('.text-and-image-card');
  // const blocks = [container];
  for (let block of blocks) {
    const modelContent = block.querySelector('.video-wrapper');
    if (modelContent) {
      const watchVideo = block.querySelector('.watch-video');
      const modelContentSrc = modelContent.dataset.srcVideo;
      const customModel = document.getElementById('custom-model');
      const customModelWrap = customModel.querySelector('.custom-model-wrap');
      watchVideo?.addEventListener('click', () => {
        preventPageScroll();
        customModel.classList.add('active');
        customModelWrap.innerHTML = `<div class="video-wrapper"><iframe allowfullscreen src="${modelContentSrc}"></iframe></div>`;
      })
      modelContent.remove();
    }
  
    animations(block);
    imageLazyLoading(block);
  }
  };

windowOnLoad(blockScript);




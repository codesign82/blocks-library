<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'text_and_image';
$className = 'text_and_image';
$isDesktopReversed = get_field('is_desktop_reversed');
$isMobileReversed = get_field('is_mobile_reversed');
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if ($isDesktopReversed === "right") {
    $className .= ' Desktop_isReversed';

}

if ($isMobileReversed === "bottom") {
    $className .= ' Mobile_isReversed';

}

if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/text_and_image/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$cta_link = get_field('cta_link');
$image = get_field('image');
$image_cover_color = get_field('image_cover_color');
$has_svg = get_field('has_svg');
$svg = get_field('svg');
?>
<!-- region samrt_dv Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class='container  g-container-remove-all-0'>
    <div class='main-content dark-background <?= $has_svg ? 'has_svg' : '' ?> '>
        <div class='left-content image-parallax-effect'
             style="color: <?= $image_cover_color ?>">
            <picture class='img-wrapper aspect-ratio '>
                <?= get_acf_image($image, 'img-722-656') ?>
            </picture>
        </div>
        <div class='right-content  '>
            <div class='inner-content'>
                <?php if ($title) { ?>
                    <div class='title headline-2 word-up'><?= $title ?></div>
                <?php } ?>
                <?php if ($description) { ?>
                    <div class='desc paragraph iv-st-from-bottom'> <?= $description ?></div>
                <?php } ?>
                <?php if ($cta_link) { ?>
                    <a class='cta-link link iv-st-from-bottom' href=" <?= $cta_link['url'] ?>"
                       target="<?= $cta_link['target'] ?>">
                        <?= $cta_link['title'] ?>
                        <svg class='arrow-link' width='23' height='12' viewBox='0 0 23 12'
                             fill='none'>
                            <rect y='5' width='13' height='2' fill='#BFD730'/>
                            <path d='M13 0L23 6L13 12V0Z' fill='#BFD730'/>
                        </svg>
                    </a>
                <?php } ?>

            </div>

        </div>
        <?php if ($svg) { ?>
            <div class="svg-wrapper">
                <?= $svg ?>
            </div>
        <?php } ?>
    </div>
</div>
</section>


<!-- endregion samrt_dv Block -->

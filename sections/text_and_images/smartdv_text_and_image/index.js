import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";

import '../../../scripts/sitesSizer/propellerhealth';

import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger)


const blockScript = async (container = document) => {
  const block = container.querySelector('.text_and_image');

  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



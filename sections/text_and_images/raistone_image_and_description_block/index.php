<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$choose_your_theme = get_field('choose_your_theme');
$theme_number = '';

$dataClass = 'image_and_description_block';
$className = 'image_and_description_block ';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}

if ($choose_your_theme === 'theme_one') {
    $className .= ' title-and-description';
    $theme_number = '1';

} elseif ($choose_your_theme === 'theme_two') {
    $className .= 'only-description ';
    $theme_number = '2';

} elseif ($choose_your_theme === 'theme_three') {
    $className .= 'has-cta-button dark-theme';
    $theme_number = '3';

} elseif ($choose_your_theme === 'theme_four') {
    $className .= 'has-cta-button ';
    $theme_number = '4';
}

if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/image_and_description_block/screenshot.png" >';

    return;
endif;


/****************************
 *     Custom ACF Meta      *
 ****************************/
$subtitle = get_field('subtitle');
$title = get_field('title');
$description = get_field('description');
$cta_button = get_field('cta_button');
$image = get_field('image');
$is_reverse = get_field('is_reverse');
$is_reverse = $is_reverse ? 'reverse' : '';
$is_image_object_fit_contain = get_field('is_image_object_fit_contain');
$is_image_object_fit_contain = $is_image_object_fit_contain ? 'contain' : '';
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="content-wrapper <?= $is_reverse ?>" parallax-container>
        <div class="left-content" data-parallax-factor="-.03">
            <?php if ($subtitle) { ?>
                <h5 class="headline-5 sub-title iv-st-from-bottom"><?= $subtitle ?></h5>
            <?php } ?>
            <?php if ($title) { ?>
                <h2 class="headline-2 main-title iv-st-from-bottom"><?= $title ?></h2>
            <?php } ?>
            <?php if ($description) { ?>
                <div class="paragraph paragraph-l-paragraph description iv-st-from-bottom"><?= $description ?></div>
            <?php } ?>
            <?php if ($cta_button) { ?>
                <a href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>"
                   class="btn cta-button iv-st-from-bottom"><?= $cta_button['title'] ?></a>
            <?php } ?>
        </div>
        <?php if ($image) { ?>
            <div class="right-content" data-parallax-factor=".03">
                <div class="theme-<?= $theme_number ?>">
                    <picture class="aspect-ratio theme-one-inner1">
                        <img class="card-image theme1-pic1"
                             src="<?= get_template_directory_uri() ?>/front-end/src/images/theme-5.png" alt="">
                        <img class="card-image theme2-pic1"
                             src="<?= get_template_directory_uri() ?>/front-end/src/images/theme-2.png" alt="">
                        <img class="card-image theme3-pic1"
                             src="<?= get_template_directory_uri() ?>/front-end/src/images/theme-6.png" alt="">
                        <img class="card-image theme4-pic1"
                             src="<?= get_template_directory_uri() ?>/front-end/src/images/theme-7.png" alt="">
                    </picture>
                    <picture class="theme-one-inner2" data-reveal-direction="top">
                        <img class="content-image theme1-pic2 <?= $is_image_object_fit_contain ?>"
                             src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                    </picture>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
</section>


<!-- endregion raistone's Block -->

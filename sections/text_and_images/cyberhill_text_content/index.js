import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/cyberhill';

const blockScript = async (container = document) => {
  const block = container.querySelector('.text_content_block');
  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


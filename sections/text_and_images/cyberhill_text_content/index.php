<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$back_ground_color = get_field('back_ground_color');

$dataClass = 'text_content_block';
$className = 'text_content_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}

if ($back_ground_color === 'blue') {
  $className .= ' blue  no-side-border ';
}
if ($back_ground_color === 'white') {
  $className .= ' white ';
}
if ($back_ground_color === 'grey') {
  $className .= ' grey ';
}


if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/text_content_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$is_right = get_field('is_right');
$is_right = $is_right ? ' right ' : '';
$image = get_field('image');
$title = get_field('title');
$sub_title = get_field('sub_title');
$description_is_full_width = get_field('description_is_full_width');
$description_is_full_width = $description_is_full_width ? ' is-full-width ' : ' ';
$description = get_field('description');
$has_form = get_field('has_form');
$add_arrow_to_link = get_field('add_arrow_to_link');
$add_arrow_to_link = $add_arrow_to_link ? 'has-arrow' : ' ';
$cta_button = get_field('cta_button');
$choose_gravity_form = get_field('choose_gravity_form');
?>
<!-- region CyberHill's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="content-wrapper <?= $is_right ?>" parallax-container>
    <div class="text-content" data-parallax-factor=".01">
      <?php if ($title) { ?>
        <div class="title-with-shape iv-st-from-bottom"><?= $title ?></div>
      <?php } ?>
      <?php if ($sub_title) { ?>
        <h3 class="headline-3 iv-st-from-bottom"><?= $sub_title ?></h3>
      <?php } ?>
      <?php if ($description) { ?>
        <div class="paragraph paragraph-white iv-st-from-bottom <?= $description_is_full_width ?>"><?= $description ?></div>
      <?php } ?>
      <?php if ($has_form === true) { ?>
        <div class="input-wrapper iv-st-from-bottom">
          <?php if ($choose_gravity_form) {
            echo do_shortcode('[gravityform id="' . $choose_gravity_form . '" ajax="true" title=false description=false]');
          } ?>
        </div>
      <?php } ?>
      <?php if ($cta_button) { ?>
        <a href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>"
           class="primary-button primary-button-white primary-button-font-16 iv-st-from-bottom   <?= $add_arrow_to_link ?> ">
          <?= $cta_button['title'] ?>
          <svg class="arrow" viewBox="0 0 18 16" fill="none"
               xmlns="http://www.w3.org/2000/svg">
            <path
              d="M10.5 0.5L9.4275 1.54475L15.1125 7.25H0V8.75H15.1125L9.4275 14.4298L10.5 15.5L18 8L10.5 0.5Z"
              fill="#1D4070"/>
          </svg>
        </a>
      <?php } ?>
    </div>
    <?php if ($image) { ?>
      <div class="img-wrapper" data-parallax-factor="-.05">
        <picture class="aspect-ratio" data-reveal-direction="right">
          <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
        </picture>
      </div>
    <?php } ?>
  </div>
</div>
</section>


<!-- endregion CyberHill's Block -->

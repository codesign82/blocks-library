<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'block-services';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/block-services/screenshot1.png" >';
  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/


?>
<!-- region Kiwi's Block -->
<?php general_settings_for_blocks($id, $className); ?>
<div class="container block-services-wrapper">
  <?php
  while (have_rows('services')) {
    the_row();
    $link_button = get_sub_field('link_button');
    $description = get_sub_field('description');
    $book_button = get_sub_field('book_button');
    $title = get_sub_field('title');
    $image = get_sub_field('image');
    ?>
    <div class="service-wrapper circle-plus-line row align-items-center align-items-xl-end justify-content-xl-end gutter--98">
      <div class="col-12 col-lg-6 col-xl-auto">
        <div class="service-details">
          <?php if ($title) { ?>
            <h4 class="headline-4 real-line-up"><?=$title?></h4>
          <?php } ?>
          <?php if ($description) { ?>
            <div class="wysiwyg-block real-line-up">
              <?=$description?>
            </div>
          <?php } ?>
          <div class="btns-wrapper d-flex justify-content-between">
            <?php if ($book_button['form']) { ?>
              <div class="btn iv-st-from-bottom" open-modal>
                <template>
                  <?=$book_button['form']?>
                </template>
                <div class="btn-border"></div>
                <div class="btn-bg"></div>
                <p class="btn-text"><?=$book_button['text']?></p>
              </div>
            <?php } ?>
            <?php if ($link_button) { ?>
              <a href="<?=$link_button['url']?>" target="<?=$link_button['target']?>" class="btn iv-st-from-bottom">
                <div class="btn-border"></div>
                <div class="btn-bg"></div>
                <p class="btn-text"><?=$link_button['title']?></p>
              </a>
            <?php } ?>
          </div>
        </div>
      </div>
      <div class="col-12 col-lg-6 col-xl-6">
        <div class="service-image">
          <?php if ($image) { ?>
            <picture class="aspect-ratio iv-st-from-bottom">
              <img alt="<?=$image['alt']?>" src="<?=$image['url']?>">
            </picture>
          <?php } ?>
        </div>
      </div>
    </div>
    <?php
  }
  ?>
</div>
</section>

<!-- endregion Kiwi's Block -->

<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'line-illustrations-section';
$className = 'line-illustrations-section';
if ( ! empty( $block['className'] ) ) {
	$className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
	$className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
	/* Render screenshot for example */
	echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/line_illustrations_section/screenshot.png" >';

	return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field( 'illustrations_title' );

?>
<!-- region Takeoff's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="line-illustrations-wrapper">
		<?php if ( $title ) { ?>
            <h2 class="line-title headline-2 iv-st-from-bottom big-line">
				<?= $title ?>
            </h2>
		<?php } ?>
        <div class="row">
			<?php
			while ( have_rows( 'illustrations_cards' ) ) {
				the_row();
				$sprite_sheet         = get_sub_field( 'sprite_sheet' );
				$card_content = get_sub_field( 'card_content' );
				?>
                <div class="col-12 col-md-6">
                    <div class="line-illustrations-card">
                        <div class="card-icon">
                            <img data-lazy-load-offset="50" data-src="<?= $sprite_sheet['image']['url'] ?>" alt="<?= $sprite_sheet['image']['url'] ?>" class="shape-svg <?=$sprite_sheet['type']==='image'?'':'sprite-sheet' ?>"
                                 data-frames="<?= $sprite_sheet['frames'] ?>"
                                 data-frame_x="<?= $sprite_sheet['frame_x'] ?>"
                                 data-frame_y="<?= $sprite_sheet['frame_y'] ?>"
                                 data-loop_start_index="<?= $sprite_sheet['loop_start_index'] ?>"
                                 data-duration="<?= $sprite_sheet['duration'] ?>"
                                 data-initial_duration="<?= $sprite_sheet['initial_duration'] ?>"/>
                        </div>
						<?php if ( $card_content ) { ?>
                            <div class="wysiwyg-block iv-st-from-bottom">
								<?= $card_content ?>
                            </div>
						<?php } ?>
                    </div>
                </div>
				<?php
			}
			?>
        </div>
        <div class="html-tag tag-h2 iv-st-from-right">&lt;/h2&gt;</div>
        <div class="html-tag tag-h3 iv-st-from-right">&lt;/h3&gt;</div>
        <svg class="general-circle iv-st-from-bottom" id="Слой_2" data-name="Слой 2" width="49.36" height="49.36"
             viewBox="0 0 49.36 49.36">
            <g id="BG_Elements" data-name="BG Elements">
                <circle id="Ellipse_67" data-name="Ellipse 67" cx="24.68" cy="24.68" r="24.68"
                        transform="translate(0 0)"
                        fill="#333"/>
            </g>
        </svg>
    </div>
</div>
<svg class="big-circle iv-st-from-left" width="583.34" height="583.34"
     viewBox="0 0 583.34 583.34">
    <defs>
        <radialGradient id="radial-gradient2" cx="0.5" cy="0.5" fx="0.952304482460022"
                        fy="-0.033144354820251465"
                        r="0.699" gradientTransform="translate(-18.446 8.925) rotate(135)"
                        gradientUnits="objectBoundingBox">
            <stop offset="0.68" stop-color="#333" stop-opacity="0"/>
            <stop offset="0.86" stop-color="#333"/>
        </radialGradient>
    </defs>
    <circle id="Ellipse_85" data-name="Ellipse 85" cx="291.67" cy="291.67" r="291.67"
            fill="url(#radial-gradient2)"/>
</svg>
<div class="html-tag tag-div iv-st-from-left">&lt;/div&gt;</div>
<img class="line-illustration-svg iv-st-from-bottom" data-lazy-load-offset="50" data-src="<?php echo get_template_directory_uri() . '/assets/images/home-line.png' ?>" alt="home-line.png">
</section>

<!-- endregion Takeoff's Block -->

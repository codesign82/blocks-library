import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/propellerhealth';
import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

const blockScript = async (container = document) => {
  const block = container.querySelector('.image_illustration_and_stats_block');


    const numEl = block.querySelectorAll(".stat span");
    ScrollTrigger.batch(numEl, {
        onEnter: batch => {
            gsap.fromTo(batch, {innerHTML: 0}
                , {
                    innerHTML: (_, element) => +element.dataset.number,
                    duration: 3,
                    ease: "power2.out",
                    modifiers: {
                        innerHTML: (value, target) =>
                            value.toFixed?.(target.dataset.toFixed ?? 0),
                    },
                    stagger: 0.2,
                    immediateRender: true,
                    clearProps: 'transform',
                });
        },
        start: 'top 80%',
        once: true,
    });


    animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


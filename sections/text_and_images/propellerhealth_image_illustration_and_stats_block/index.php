<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'image_illustration_and_stats_block';
$className = 'image_illustration_and_stats_block';
if ( ! empty( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
    $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/image_illustration_and_stats_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field( 'title' );
$image = get_field( 'image' );
?>
<!-- region propellerhealth Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
    <div class="content-wrapper">
        <div class="text-wrapper">
            <?php if ($title) { ?>
                <h3 class="headline-3 title iv-st-from-bottom"><?= $title ?></h3>
            <?php } ?>
            <?php if ( have_rows( 'stats' ) ) { ?>
            <div class="info-wrapper">
                <?php while ( have_rows( 'stats' ) ) {
                    the_row();
                    $stat        = get_sub_field( 'stat' );
                    $suffix      = get_sub_field( 'suffix' );
                    $description = get_sub_field( 'description' );
                    ?>
                    <div class="info iv-st-from-bottom">
                        <?php if ( $stat ) { ?>
                            <div class="stat headline-1">
                                <span data-number='<?= $stat ?>'>0</span><?= $suffix ?>
                            </div>
                        <?php } ?>

                        <?php if ( $description ) { ?>
                            <div class="textarea paragraph"><?= $description ?></div>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php } ?>
            </div>
        </div>
        <?php if ($image) { ?>
            <div class="image-wrapper moving-shape iv-st-from-bottom">
                <?= get_acf_image( $image, 'img-464-526' ) ?>
            </div>
        <?php } ?>
    </div>
</div>
</section>


<!-- endregion propellerhealth Block -->

<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}


// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'image_and_list_block';
$className = 'image_and_list_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/image_and_list_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$image = get_field('image');
$cta_button = get_field('cta_button');
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="content">
        <div class="left-content">
            <?php if ($image) { ?>
                <picture class="image-wrapper aspect-ratio">
                    <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                </picture>
            <?php } ?>
        </div>
        <div class="right-content">
            <div class="text-wrapper">
                <h2 move-to-here class="title headline-3 iv-st-from-bottom "> <?= $title ?> </h2>
                <div class="description paragraph iv-st-from-bottom">
                    <?= $description ?>
                </div>
                <ul class=" list-wrapper">
                    <?php if (have_rows('list_item')) {
                        while (have_rows('list_item')) {
                            the_row();
                            $list_item_description = get_sub_field('list_item_description')
                            ?>
                            <li class='list-item headline-5 iv-st-from-bottom'>
                                <svg class='correct' width='26' height='24' viewBox='0 0 26 24'
                                     fill='none' xmlns='http://www.w3.org/2000/svg'>
                                    <path d='M2 11.5L10.36 21L24 2' stroke='#005DB3'
                                          stroke-width='3' stroke-linecap='round'/>
                                </svg>
                                <?= $list_item_description ?>
                            </li>

                        <?php }
                    } ?>
                </ul>
                <?php if ($cta_button) { ?>
                    <div class="iv-st-from-bottom">
                        <a class="btn"
                           href=" <?= $cta_button['url'] ?> "> <?= $cta_button['title'] ?> </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
</section>


<!-- endregion headversity's Block -->

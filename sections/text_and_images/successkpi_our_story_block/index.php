<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'our_story_block';
$className = 'our_story_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/our_story_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$image = get_field('image')
?>
<!-- region successkpi's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<div class="container">
    <div class="wrapper">
        <?php if ($title) { ?>
            <h2 class="headline-2 title iv-st-from-bottom"><?= $title?></h2>
        <?php } ?>
        <div class="image-text">
            <?php if ($image) { ?>
                <div class="photo ">
                    <picture class="aspect-ratio iv-st-from-bottom">
                        <img class="our-image" src="<?= $image['url'] ?>"
                             alt="<?= $image['alt'] ?>">
                    </picture>

                </div>
            <?php } ?>

            <div class="text">
                <?php if ($description) { ?>
                    <div class="headline-5 our-paragraph iv-st-from-bottom">
                        <?=$description?>
                    </div>
                <?php } ?>
                <?php if (have_rows('list_items')){ ?>
                    <ul>
                        <?php while (have_rows('list_items')) {
                            the_row();
                            $icon = get_sub_field('icon');
                            $item_text = get_sub_field('item_text');
                            ?>
                            <li class="link-items headline-5 iv-st-from-bottom">
                                <?= $icon ?>
                                <div class="item_text"><?= $item_text ?></div>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
</section>


<!-- endregion successkpi's Block -->

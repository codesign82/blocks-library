import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/propellerhealth';
import {ScrollTrigger} from "gsap/ScrollTrigger";


gsap.registerPlugin(ScrollTrigger)

const blockScript = async (container = document) => {
  const block = container.querySelector('.category_leaders_win_bigger_block');

  const categoryWrapper = block.querySelectorAll(".category-leader")
  const categoryWrapperTitle = block.querySelectorAll(".category-leaders-title span")


  gsap.from(block, {
    opacity: 0,
    duration: 1.5,
    ease:"power3.out",
    scrollTrigger: {
      trigger:categoryWrapper,
      start:"top 70%"
    }
  })
  gsap.from(categoryWrapperTitle, {
    color: "#fff",
    ease:"power3.out",
    scrollTrigger: {
      trigger:categoryWrapperTitle,
      start:"center 30%",
      toggleActions: "play reverse play reverse"
    }
  })
  // if (window.innerWidth >= 992) {
  //   const resizeHandler = debounce(function () {
  //
  //   },300);
  //   resizeHandler()
  //   window.addEventListener('resize', resizeHandler);

  // }
  ScrollTrigger.saveStyles(block.querySelectorAll(".left-side picture,.content"));
  ScrollTrigger.matchMedia({
    "(min-width: 992px)":()=>{
      categoryWrapper.forEach((element) => {
        const categoryImg = element.querySelector(".left-side picture")
        const categoryContent = element.querySelector(".content")
        gsap.set(categoryContent, {
          willChange:"transform"
        })
        gsap.timeline({
          defaults:{
            duration:.5
          },
          scrollTrigger:{
            trigger:element,
            toggleActions: "play reverse play reverse",
            start:"top center",
            end:"bottom center",
          },

        })

            .fromTo(categoryImg, {
              scale:1,
              opacity:0
            },{
              scale:1.05,
              opacity:1,
              transformOrigin:"center center",
              ease:"power1.inOut",
            })
            .fromTo(categoryContent, {
              y:()=> 75,
            },{
              y:()=> 0,
              ease:"power1.out",
            },"<")

      })
    }
  })
  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/propellerhealth';
import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

const blockScript = async (container = document) => {
  const block = container.querySelector('.platform_block');

  const infoCard = block.querySelectorAll(".info")
  const infoCardLeft = block.querySelectorAll(".info:nth-of-type(odd)")
  const infoCardRight = block.querySelectorAll(".info:nth-of-type(even)")

  gsap.set(infoCardRight, {
    xPercent:-30,
    opacity:0,
  })

  gsap.set(infoCardLeft, {
    xPercent:30,
    opacity:0,
  })

  ScrollTrigger.batch(infoCardLeft, {
    start: "top center+=100px",
    onEnter: batch => {
      gsap.to(batch, {
        xPercent:0,
        opacity:1,
        duration:1.5
      })
    },
    once: true,
  });
  ScrollTrigger.batch(infoCardRight, {
    start: "top center+=100px",

    onEnter: batch => {
      gsap.to(batch, {
        xPercent:0,
        opacity:1,
        duration:1.5
      })
    },
    once: true,
  });


  const shape = block.querySelector(".slide-platform path")

      gsap.to(shape, {
        duration:7,
        yoyo:true,
        repeat:-1,
        ease:"linear",
        attr:{
          d:"M 0.922 0.714 C 1.152 0.428 0.712 0.062 0.463 0.061 C 0.213 0.089 0.189 0.284 0.137 0.469 C 0.107 0.583 0.032 0.846 0.177 0.907 C 0.516 1.057 0.689 1.004 0.922 0.714 Z"
        }
      })

  console.log(shape)
  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


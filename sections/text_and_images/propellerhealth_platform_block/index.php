<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'platform_block';
$className = 'platform_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/platform_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$description = get_field('description');
$title = get_field('title');
$image = get_field('image');
?>
<!-- region propellerhealth Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="text-wrapper">
        <?php if ($description) { ?>
            <div class="headline-4 main-description iv-st-from-bottom"><?= $description ?></div>
        <?php } ?>
        <?php if ($title) { ?>
            <h2 class="headline-3 title iv-st-from-bottom"><?= $title ?></h2>
        <?php } ?>
    </div>
    <div class="content-wrapper">
        <?php if ($image) { ?>
            <picture class="image">
                <?= get_acf_image( $image, 'img-374-364' ) ?>
            </picture>
        <?php } ?>
        <?php if (have_rows('product_info')) { ?>
            <?php while (have_rows('product_info')) {
                the_row();
                $icon_img = get_sub_field('icon_image');
                $icon_svg = get_sub_field('icon_svg');
                $title = get_sub_field('title');
                $description = get_sub_field('description');
                ?>
                <div class="info">
                    <?php if ($icon_img) { ?>
                        <picture class="icon">
                            <?= get_acf_image( $image, 'img-52-52' ) ?>
                        </picture>
                    <?php } ?>
                    <?php if ($icon_svg) { ?>
                        <div class="icon"><?= $icon_svg ?></div>
                    <?php } ?>
                    <?php if ($title) { ?>
                        <h4 class="headline-4 info-title"><?= $title ?></h4>
                    <?php } ?>
                    <?php if ($description) { ?>
                        <div class="paragraph"><?= $description ?></div>
                    <?php } ?>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>
<svg class="slide-platform slide-shape">
    <defs>
        <clipPath id="shape" clipPathUnits="objectBoundingBox">
            <path
                    d="M 0.971 0.572 C 1.006 0.343 0.665 -0.027 0.436 0.021 C 0.224 0.078 0.177 0.334 0.125 0.519 C 0.095 0.633 -0.031 0.745 0.056 0.861 C 0.242 1.15 0.931 0.874 0.971 0.572 Z"></path>
        </clipPath>
    </defs>
</svg>

</section>


<!-- endregion propellerhealth Block -->

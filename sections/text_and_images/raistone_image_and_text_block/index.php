<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$choose_your_theme = get_field('choose_your_theme');
$is_reverse = get_field('is_reverse');
$theme_number = '';

$dataClass = 'image_and_text_block';
$className = 'image_and_text_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}

if ($choose_your_theme === 'first_type') {
    $theme_number .= '1';
}

if ($choose_your_theme === 'second_type') {
    $className .= ' theme-two';
    $theme_number .= '2';
}
if ($choose_your_theme === 'fourth_type') {
    $className .= ' theme-two';
    $theme_number .= '4';
}

if ($choose_your_theme === 'third_type') {
    $className .= ' theme-three';
    $theme_number .= '3';
}

if ($is_reverse) {
    $className .= ' reverse';
}

if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/image_and_text_block/screenshot.png" >';
    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$image = get_field('image');
$title = get_field('title');
$sub_title = get_field('sub_title');
$description = get_field('description');
$is_image = get_field('is_image');
$image_icon = get_field('image_icon');
$svg_icon = get_field('svg_icon');
$icon_text = get_field('icon_text');
$link = get_field('link');
$is_image_object_fit_contain = get_field('is_image_object_fit_contain');
$is_image_object_fit_contain = $is_image_object_fit_contain ? 'contain' : '';
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="content-wrapper" parallax-container>
        <?php if ($image) { ?>
            <div class="left-content" data-parallax-factor="-.03">
                <div class="theme-<?= $theme_number ?>">
                    <picture class="aspect-ratio theme-one-inner1">
                        <img class="card-image theme1-pic1"
                             src="<?= get_template_directory_uri() ?>/front-end/src/images/theme-1.png" alt="">
                        <img class="card-image theme2-pic1"
                             src="<?= get_template_directory_uri() ?>/front-end/src/images/theme-2.png" alt="">
                        <img class="card-image theme3-pic1"
                             src="<?= get_template_directory_uri() ?>/front-end/src/images/theme-3.png" alt="">
                        <img class="card-image theme4-pic1"
                             src="<?= get_template_directory_uri() ?>/front-end/src/images/theme-4.png" alt="">
                    </picture>
                    <picture class="theme-one-inner2" data-reveal-direction="top">
                        <img class="content-image theme1-pic2 <?= $is_image_object_fit_contain ?>"
                             src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                    </picture>
                </div>
            </div>
        <?php } ?>
        <div class="right-content" data-parallax-factor=".03">
            <?php if ($title) { ?>
                <h5 class="headline-5 small-title iv-st-from-bottom"><?= $title ?></h5>
            <?php } ?>
            <?php if ($sub_title) { ?>
                <h2 class="headline-2 title iv-st-from-bottom"><?= $sub_title ?></h2>
            <?php } ?>
            <?php if ($description) { ?>
                <div class="paragraph paragraph-xl-paragraph description iv-st-from-bottom"><?= $description ?></div>
            <?php } ?>
            <?php if ($link) { ?>
                <a href="<?= $link['url'] ?>" class="btn-arrow iv-st-from-bottom"
                   target="<?= $link['target'] ?>"><?= $link['title'] ?>
                    <svg width="9" height="14" viewBox="0 0 9 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.03906 13L7.03906 7L1.03906 1" stroke="#0A0A0A" stroke-width="2"
                              stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </a>
            <?php } ?>

            <div class="icon-and-title iv-st-from-bottom">

                <?php if (!$is_image) { ?>
                    <?php if ($svg_icon) { ?>
                        <div class="icon">
                            <?= $svg_icon ?>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <?php if ($image_icon) { ?>
                        <picture class="icon">
                            <img src="<?= $image_icon['url'] ?>" alt="<?= $image_icon['alt'] ?>">
                        </picture>
                    <?php } ?>
                <?php } ?>
                <?php if ($icon_text) { ?>
                    <h4 class="headline-4"><?= $icon_text ?></h4>
                <?php } ?>
            </div>

            <div class="columns-wrapper">
                <?php if (have_rows('columns')) { ?>
                    <?php while (have_rows('columns')) {
                        the_row();
                        $title = get_sub_field('title');
                        $description = get_sub_field('description');
                        $link = get_sub_field('link');
                        ?>
                        <div class="column-content iv-st-from-bottom">
                            <?php if ($title) { ?>
                                <h3 class="headline-3"><?= $title ?></h3>
                            <?php } ?>
                            <?php if ($description) { ?>
                                <div class="paragraph column-description"><?= $description ?></div>
                            <?php } ?>
                            <?php if ($link) { ?>
                                <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>"
                                   class="btn-arrow "><?= $link['title'] ?>
                                    <svg width="9" height="14" viewBox="0 0 9 14" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1.03906 13L7.03906 7L1.03906 1" stroke="#0A0A0A" stroke-width="2"
                                              stroke-linecap="round" stroke-linejoin="round"/>
                                    </svg>
                                </a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>

        </div>
    </div>
</div>

</section>


<!-- endregion raistone's Block -->

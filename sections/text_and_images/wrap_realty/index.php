<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'wrap_realty';
$className = 'wrap_realty';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/wrap_realty/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title       = get_field( 'title' );
$subtitle   = get_field( 'subtitle' );
$description = get_field( 'description' );
$cta_button  = get_field( 'cta_button' );
$image       = get_field( 'image' );
?>
<!-- region WRAP's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="wrap-realty-wrapper g-container-set-sm">
  <?php if ( $image ) { ?>
    <picture class="wrap-realty-image">
      <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>"/>
    </picture>
  <?php } ?>
  <div class="content">
    <?php if ( $title ) { ?>
      <div class="headline-6"><?= $title ?></div>
    <?php } ?>
    <?php if ( $subtitle ) { ?>
      <div class="headline-2"><?= $subtitle ?></div>
    <?php } ?>
    <?php if ( $description ) { ?>
      <div class="paragraph paragraph-16"><?= $description ?></div>
    <?php } ?>
    <?php if ( $cta_button ) { ?>
      <a class="btn" href="<?= $cta_button['url'] ?>"
         target="<?= $cta_button['target'] ?: '_self' ?>"><?= $cta_button['title'] ?></a>
    <?php } ?>
  </div>
</div>
</section>


<!-- endregion WRAP's Block -->

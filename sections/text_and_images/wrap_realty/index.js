import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import Swiper from "swiper";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/wrap';

const blockScript = async (container = document) => {
  const block = container.querySelector('.wrap_realty');
 
  
  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);




<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$title_above_image =get_field('title_above_image');
$dataClass = 'image_illustration_and_cta';
$className = 'image_illustration_and_cta';
if ( ! empty( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
    $className .= ' align' . $block['align'];
}
if(!$title_above_image){
    $className=  $className.' home_version ';
}

if ( get_field( 'is_screenshot' ) ) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/image_illustration_and_cta/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title_above_image =get_field('title_above_image');
$title            = get_field( 'title' );
$content          = get_field( 'content' );
$text_content = get_field( 'text_content' );
$image            = get_field( 'image' );
$cta_button       = get_field( 'cta_button' );
$image_position = get_field( 'image_position' );
$position_item = get_field( 'position_item' );
?>
<style>
    <?php if ($title_above_image ) { ?>
    .text-and-image{
        align-items: flex-end;
    }
    <?php } ?>
</style>

<!-- region propellerhealth Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
    <div class="text-and-image <?= ( $image_position ) ? 'reverse' : '' ?> <?= ( $position_item ) ? 'start' : '' ?>">
        <div class="left-side">
            <?php if (!$title_above_image ) { ?>
                <?php if ( $title ) { ?>
                    <div
                            class="headline-2 main-title iv-st-from-bottom wysiwyg-block"><?= $title ?></div>
                <?php } ?>
            <?php } ?>
            <?php if ( $content === 'list' ) { ?>
                <?php if ( have_rows( 'box_repeater' ) ) { ?>
                    <ul class="list-boxes">
                        <?php while ( have_rows( 'box_repeater' ) ) {
                            the_row();
                            $icon_svg_or_img = get_sub_field( 'icon_svg_or_img' );
                            $icon_svg        = get_sub_field( 'icon_svg' );
                            $icon_image      = get_sub_field( 'icon_image' );
                            $link            = get_sub_field( 'link' );
                            $text            = get_sub_field( 'text' );
                            ?>
                            <li class="box iv-st-from-bottom">
                                <div class="icon">
                                    <?php if ( ! $icon_svg_or_img ) {
                                        echo $icon_svg;
                                    } else { ?>
                                        <?= get_acf_image( $image, 'img-44-37' ) ?>
                                    <?php } ?>
                                </div>
                                <div class="list-content">
                                    <?php if ( $link ) { ?>
                                        <a href="<?= $link['url'] ?>"
                                           target="<?= $link['target'] ?>"
                                           class="cta-link"><?= $link['title'] ?>
                                            <svg class="arrow-right"
                                                 viewBox="243.81 243.737 15.017 12.535"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path class="arrow-head"
                                                      d="M 251.977 256.242 L 257.977 250.742 L 258.781 250.005 L 257.977 249.268 L 251.977 243.768 L 250.625 245.242 L 255.821 250.005 L 250.625 254.768 L 251.977 256.242 Z"
                                                      style="fill: rgb(0, 117, 227);"></path>
                                                <path class="arrow-line"
                                                      d="M 243.857 249.004 L 256.857 249.004 L 256.857 251.004 L 243.857 251.004 L 243.857 249.004 Z"
                                                      style="fill: rgb(0, 117, 227);"></path>
                                            </svg>
                                        </a>
                                    <?php } ?>
                                    <?php if ( $text ) { ?>
                                        <div class="paragraph p-text"><?= $text ?></div>
                                    <?php } ?>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                <?php }
            } elseif ( $content === 'text' ) { ?>
                <div class='text-content  wysiwyg-block iv-st-from-bottom'><?= $text_content ?></div>
            <?php } else { ?>
                <!--        <div class="links"></div>-->
            <?php } ?>

            <?php if ( $cta_button ) { ?>
                <a href="<?= $cta_button['url'] ?>"
                   target="<?= $cta_button['target'] ?>"
                   class="cta-button iv-st-from-bottom"><?= $cta_button['title'] ?></a>
            <?php } ?>
        </div>
        <div class="right-side iv-st-from-bottom">
            <?php if($title_above_image){  ?>
                <?php if ($title) { ?>
                    <div class="headline-3 above-title iv-st-from-bottom"><?= $title ?></div>
                <?php } ?>
            <?php }?>
            <?php if ( $image ) { ?>
                <div class="moving-shape shape-3 iv-st-from-bottom">
                    <picture class="aspect-ratio iv-st-from-bottom">
                        <?= get_acf_image( $image, 'img-475-600' ) ?>
                    </picture>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
</section>


<!-- endregion propellerhealth Block -->

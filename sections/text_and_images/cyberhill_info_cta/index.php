<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'info_cta_block';
$className = 'info_cta_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/info_cta_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$image = get_field('image');
$title = get_field('title');
$description = get_field('description');
$cta_button = get_field('cta_button');

?>
<!-- region CyberHill's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="info-wrapper">
    <div class="lef-content" data-reveal-direction="left">
      <div class="image-and-svg-wrapper">
        <picture class="image aspect-ratio">
          <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
        </picture>
        <svg preserveAspectRatio="xMidYMid meet" viewBox="0 0 432 574">
          <path d="M432,0H0V574H432V0Zm-2.8,304.16L310,303.63s-51.27-52.23-58.65-58.55c-.43-.42-1-.85-1.39-1.28-7.92-7-24.08-21.19-55.23-21.19-33.28,0-61.54,21.51-61.54,21.51s37.67-37.14,54.69-54.26,35.75-28.79,67.43-28.79,58.12,27.4,58.12,27.4ZM2.8,262.64l119.23.53s51.27,52.23,58.65,58.55c.43.42,1,.85,1.39,1.28,7.92,7,24.08,21.19,55.23,21.19,33.28,0,61.54-21.51,61.54-21.51s-37.67,37.14-54.69,54.26-35.75,28.79-67.43,28.79-58.12-27.4-58.12-27.4Zm158.62-3.75s34.57,5.89,55.76,14.88c32.43,13.91,63.89,37.56,63.89,37.56s-32.64-18.3-50.94-26.65C203.16,272.48,161.42,258.89,161.42,258.89Z" fill="#fff"/>
        </svg>
      </div>
      <h2 class="lef-content-text headline-3"></h2>
    </div>
    <div class="right-content">
      <?php if ($title) { ?>
        <div class="title-with-shape title-with-shape-blue iv-st-from-bottom"><?= $title ?></div>
      <?php } ?>
      <?php if ($description) { ?>
        <h3 class="headline-3 description iv-st-from-bottom"><?= $description ?></h3>
      <?php } ?>
      <?php if ($cta_button) { ?>
        <a href="<?= $cta_button['url'] ?>" class="primary-button iv-st-from-bottom" target="<?= $cta_button['target'] ?>"><?= $cta_button['title'] ?></a>
      <?php } ?>
    </div>
  </div>
</div>
</section>


<!-- endregion CyberHill's Block -->

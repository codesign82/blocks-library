import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/cyberhill';

const blockScript = async (container = document) => {
  const block = container.querySelector('.info_cta_block');
  
  
  // add block code here
  
  let HoverState = block.querySelectorAll(".description .hover-text"),
      lefContentText = block.querySelector(".lef-content-text"),
      lefContent = block.querySelector(".lef-content");
  
  // adding the event listener by looping
  
  HoverState.forEach(hover => {
    hover.addEventListener('mouseover', (e) => {
      lefContent.classList.add('hovered-animation')
      console.log(hover.dataset.textHover)
      lefContentText.innerHTML = hover.dataset.textHover;
      lefContentText.classList.add('active');
    });
    hover.addEventListener('mouseleave', (e) => {
      lefContent.classList.remove('hovered-animation');
      lefContentText.classList.remove('active');
    });
  });
  
  
  
  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


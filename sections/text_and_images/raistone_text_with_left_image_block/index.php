<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'text_with_left_image_block';
$className = 'text_with_left_image_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/text_with_left_image_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$image = get_field('image');
$image_align = get_field('image_align');
$title = get_field('title');
$text = get_field('text');
$cta_button = get_field('cta_button');
$select_from_posts = get_field('select_from_posts');


?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="inner-block <?php echo $image_align == 'right' ? 'right-img' : ''; ?>">
        <?php if (!$select_from_posts) { ?>
            <?php if ($image) { ?>
                <div class="block-image">
                    <picture class="aspect-ratio">
                        <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                    </picture>
                </div>
            <?php } ?>
            <div class="block-info">
                <?php if ($title) : ?>
                    <h3 class="title headline-2"><?= $title ?></h3>
                <?php endif; ?>

                <?php if ($text) : ?>
                    <div class="text"><?= $text ?></div>
                <?php endif; ?>

                <?php if ($cta_button) : ?>
                    <a class="btn" href="<?= $cta_button['url'] ?>"
                       target="<?= $cta_button['target'] ?>"><?= $cta_button['title'] ?></a>
                <?php endif; ?>

            </div>
        <?php } else {
            $block_post = get_field('block_post');
            if ($block_post):
                $is_cta_go_to_post_single = get_field('is_cta_go_to_post_single');
                $p_image = get_field('image', $block_post);
                $p_title = get_field('name', $block_post);
                $p_quotation = get_field('quotation', $block_post);
                $cta_text = get_field('cta_text');
                ?>
                <?php if ($p_image) { ?>
                <a class="block-image display-block" href="<?= get_the_permalink($block_post) ?>">
                    <picture class="aspect-ratio">
                        <img src="<?= $p_image['url'] ?>" alt="<?= $p_image['alt'] ?>">
                    </picture>
                </a>
            <?php } ?>
                <div class="block-info">
                    <?php if ($p_title) : ?>
                        <a class="title headline-2 display-block"
                           href="<?= get_the_permalink($block_post) ?>"><?= $p_title ?></a>
                    <?php endif; ?>

                    <?php if ($p_quotation) : ?>
                        <div class="text"><?= $p_quotation ?></div>
                    <?php endif; ?>
                    <?php if (!$is_cta_go_to_post_single) { ?>
                        <?php if ($cta_button) : ?>
                            <a class="btn" href="<?= $cta_button['url'] ?>"
                               target="<?= $cta_button['target'] ?>"><?= $cta_button['title'] ?></a>
                        <?php endif; ?>
                    <?php } else {
                        if ($cta_text) {
                            ?>
                            <a class="btn" href="<?= get_the_permalink($block_post) ?>"><?= $cta_text ?></a>
                            <?php
                        }
                    } ?>
                </div>
            <?php endif;
        } ?>
    </div>
</div>
</section>


<!-- endregion raistone's Block -->

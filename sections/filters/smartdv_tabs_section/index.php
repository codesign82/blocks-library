<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'tabs_section';
$className = 'tabs_section';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/tabs_section/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$logos = get_field('logos')
?>
<!-- region samrt_dv Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div
            class="filter-content-wrapper tabs-section-button iv-st-from-bottom"
            id="filter-content-wrapper">
        <?php $first_val = get_field('repeater_tab');
        $first_val = (!empty($first_val) && $first_val[0] && array_key_exists('tab_name', $first_val[0])) ? $first_val[0]['tab_name'] : '';
        ?>
        <div class="selector"><?= $first_val ?></div>
        <div class="icon-wrapper">
            <svg class="down-arrow" width="26" height="13" viewBox="0 0 26 13"
                 fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0.761324 1.3014L12.7508 12.2329L24.7402 1.3014"
                      stroke="#BFD730"/>
            </svg>
        </div>
        <?php if (have_rows('repeater_tab')) { ?>
            <ul class="filter-list tabs-section-button iv-st-from-bottom">
                <?php while (have_rows('repeater_tab')) {
                    the_row();
                    $tab_name = get_sub_field('tab_name');
                    $index = get_row_index();

                    ?>
                    <?php if ($tab_name) { ?>
                        <li
                                class="link-item featured  tab-links   button-link <?= $index === 1 ? 'active' : '' ?>"
                                id="<?= $tab_name ?>"
                        >
                            <?= $tab_name ?>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        <?php } ?>
    </div>
    <div class="tabs-content-container">
        <?php if (have_rows('repeater_tab')) { ?>
            <?php while (have_rows('repeater_tab')) {
                the_row();
                $tab_name = get_sub_field('tab_name');
                $index = get_row_index();
                $tab_content = get_sub_field('tab_content');
                $title = $tab_content['title'];
                $description = $tab_content['description'];
                $logos = $tab_content['logos'];
                $active = $index === 1 ? 'active' : '';
                ?>
                <div
                        class="tabs-section-cards  card  tab-content  <?= $index === 1 ? 'active' : '' ?>"
                        data-tab-title="<?= $tab_name ?>">
                    <div class="tabs-section-content">
                        <?php if ($title) { ?>
                            <div class="headline-3 tabs-section-content-title word-up">
                                <?= $title ?>
                            </div>
                        <?php } ?>

                        <?php if ($description) { ?>
                            <div class="paragraph tabs-section-content-description">
                                <?= $description ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="tabs-section-logo">
                        <?php if (is_array($logos) && !empty($logos)) { ?>
                            <?php for ($i = 0; $i < count($logos); $i++) { ?>
                                <picture class="card-img">
                                    <img src="<?= $logos[$i]['url']; ?>"
                                         alt="<?= $logos[$i]['alt']; ?>">
                                </picture>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>

</section>


<!-- endregion samrt_dv Block -->

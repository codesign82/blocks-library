import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {gsap} from "gsap";
import {debounce} from "lodash/function";

import '../../../scripts/sitesSizer/propellerhealth';



const blockScript = async (container = document) => {
    const block = container.querySelector('.tabs_section');

    const tabs = block.querySelectorAll(".tab-links");
    const contents = block.querySelectorAll(".tab-content");
    const contentsWrapper = block.querySelector(".tabs-content-container");
    const selector = block.querySelector(".selector");
    let linkItems = block.querySelectorAll('.link-item');
    const mobileMedia = window.matchMedia('(max-width: 600px)');
    const dropMenu = block.querySelector('#filter-content-wrapper');
    const filterList = block.querySelector('.filter-list');
    let activeTab = 0;
    let maxHeight = 0;

    const debounceContentsWrapperHeight = debounce(() => {
        getMaxHeight();
    }, 300)

    const getMaxHeight = () => {
        contents.forEach((content) => {
            if (content.clientHeight > maxHeight) {
                maxHeight = content.clientHeight;
            }
        })
        contentsWrapper.style.height = `${maxHeight}px`;
    }
    getMaxHeight();

    window.addEventListener("resize", debounceContentsWrapperHeight)
    tabs.forEach((tab, index) => {
        tab.addEventListener("click", () => {
            if (index === activeTab) return;
            contents[activeTab].classList.remove("active");
            tabs[activeTab].classList.remove("active");
            contents[index].classList.add("active");
            tab.classList.add("active");
            selector.innerHTML = tab.innerHTML;
            activeTab = index;
        })
    })
    dropMenu?.addEventListener('click', function (e) {
        if (!mobileMedia.matches) return;
        const isOpened = dropMenu?.classList.toggle('active');
        if (!isOpened) {
            gsap.to(filterList, {height: 0});
            filterList.classList.remove('active-list');

        } else {
            gsap.to(filterList, {height: 'auto'});
            filterList.classList.add('active-list');
        }
    });
    animations(block);
    imageLazyLoading(block);

};
windowOnLoad(blockScript);



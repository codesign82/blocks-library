<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'pre_built_integrations_block';
$className = 'pre_built_integrations_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/pre_built_integrations_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title         = get_field( 'title' );
$description   = get_field( 'description' );
$query_options = get_field( 'query_options' );
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>

<div class="container">
  <?php if ( $title ) { ?>
    <h2 class="headline-2"><?= $title ?></h2>
  <?php } ?>
  <?php if ( $description ) { ?>
    <div class="paragraph pre_built_integrations_text"><?= $description ?></div>
  <?php } ?>
  <div class="pre_built_integrations_filter">
    <button class="border-bottom is-checked" data-filter="*" aria-label="Filter By All"><?= __( 'All', 'zineone' ) ?></button>
    <?php
    $terms = get_terms( array(
      'taxonomy'   => 'integration_tag',
      // Swap in your custom taxonomy name
      'hide_empty' => true,
    ) );
    foreach ( $terms as $term ) {
      // Use get_term_link to get terms permalink
      // USe $term->name to return term name
      echo '<button class="border-bottom" data-filter=".' . $term->slug . '"  aria-label="Filter By ' . $term->name . '">' . $term->name . '</button>';
    }
    ?>
  </div>
  <?php
  $args       = array(
    'post_type'      => 'integration',
    'post_status' => 'publish',
    'posts_per_page' => @$query_options['posts_per_page'],
    'order'          => @$query_options['order'],
  );
  $the_query  = new WP_Query( $args );
  $have_posts = $the_query->have_posts(); ?>
  <?php
  if ( $have_posts ) { ?>
    <div class="cards-wrapper iv-st-from-bottom">
      <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
        <?php while ( $the_query->have_posts() ) {
          $the_query->the_post();
          get_template_part( "template-parts/integration-card", '', array( 'post_id' => get_the_ID() ) );
        }
        /* Restore original Post Data */
        wp_reset_postdata(); ?>
      </div>
    </div>
  <?php } ?>
</div>
</section>


<!-- endregion ZineOne's Block -->

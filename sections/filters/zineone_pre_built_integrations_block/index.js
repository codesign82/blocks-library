import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import Isotope from "isotope-layout/js/isotope";
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/zineone';

/**
 *
 * @param container {Document}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
const block = container.querySelector('.pre_built_integrations_block');
  
  // add block code here
  
  let grid = block.querySelector('.cards-wrapper .row');
  let filterButtonsWrapper = block.querySelector('.pre_built_integrations_filter');
  let iso;
  let NotMobile = window.matchMedia('(min-width: 600px)')
  
  // region get highest height of item
  function getHighest() {
    let items = grid.querySelectorAll('.integration-card');
    let maxHeight = 0;
    items.forEach(item => {
      const itemHeight = parseInt(getComputedStyle(item).height, 10);
      if (itemHeight > maxHeight) {
        maxHeight = itemHeight;
      }
    })
    for (let item of items) {
      item.style.height = maxHeight + 'px';
    }
  }
  
  // endregion get highest height of item
  
  function onHashchange() {
    let hashFilter = getHashFilter();
    if (!hashFilter && iso) {
      return;
    }
    if (!iso) {
      // init Isotope for first time
      // set item height of biggest item height
      iso = new Isotope(grid, {
        itemSelector: '.integration-card',
        layoutMode: 'fitRows',
        filter: hashFilter || '',
      });
    } else {
      // just filter with hash
      iso.arrange({
        filter: hashFilter
      });
    }
    
    // set selected class on button
    if (hashFilter) {
      let checkedButton = filterButtonsWrapper.querySelector('.is-checked');
      if (checkedButton) {
        checkedButton.classList.remove('is-checked');
      }
      filterButtonsWrapper.querySelector('[data-filter="' + hashFilter + '"]').classList.add('is-checked');
    }
  }
  
  window.addEventListener('hashchange', onHashchange);
// trigger event handler to init Isotope
  if (NotMobile.matches) {
    getHighest();
  }
  onHashchange()
  // onHashchange();

// bind filter button click
  filterButtonsWrapper.addEventListener('click', function (event) {
    let filterAttr = event.target.getAttribute('data-filter');
    if (!filterAttr) {
      return;
    }
    location.hash = 'filter=' + encodeURIComponent(filterAttr);
  });
  
  function getHashFilter() {
    // get filter=filterName
    let matches = location.hash.match(/filter=([^&]+)/i);
    let hashFilter = matches && matches[1];
    return hashFilter && decodeURIComponent(hashFilter);
  }
  
  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);

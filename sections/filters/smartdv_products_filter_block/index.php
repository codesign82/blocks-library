<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'products_filter_block';
$className = 'products_filter_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/products_filter_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$select_category = get_field('select_category');


$post_id = @$args['post_id'] ?: get_the_ID();
$args = [
    'paged' => 1,
    'post_type' => 'products',
    'posts_per_page' => 16,
    'post_status' => 'publish',
    'orderby' => array('date' => 'DESC')
];
$the_query = new WP_Query($args);
$have_posts = $the_query->have_posts();

$posts_num = $the_query->found_posts;

$terms = array();
if ($select_category === "verification") {
//  $taxonomy=array("verification1","verification2");

    $terms['v_1'] = get_terms(array(
        'taxonomy' => 'verification1',
        'hide_empty' => true,
    ));
    $terms['v_2'] = get_terms(array(
        'taxonomy' => 'verification2',
        'hide_empty' => true,
    ));
} elseif ($select_category === "application") {

    $terms['a_1'] = get_terms(array(
        'taxonomy' => 'application1',
        'hide_empty' => true,
    ));
    $terms['a_2'] = get_terms(array(
        'taxonomy' => 'application2',
        'hide_empty' => true,
    ));

} else {
    $terms['d'] = get_terms(array(
        'taxonomy' => 'design_ip',
        'hide_empty' => true,
    ));
}

//$terms = get_terms(array(
//  'taxonomy' => $taxonomy,
//  'hide_empty' => false,
//));


?>
<!-- region samrt_dv Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="content-wrapper">

        <div class="top-section">
            <?php if ($title) { ?>
                <div class="title headline-3 word-up">
                    <?= $title ?>
                </div>
            <?php } ?>
            <?php foreach ($terms as $term) { ?>
                <div class="filer-section" data-tax="<?= $term[0]->taxonomy ?>">
                    <div class="circles">
                        <div class="circle"></div>
                        <div class="line"></div>
                        <div class="circle"></div>
                    </div>
                    <div class="filter">

                        <div class="filter-title">
                            <?= __('Select Industry', 'smart_dv') ?>
                        </div>
                        <svg class="dropdown-svg" width="26" height="13" viewBox="0 0 26 13"
                             fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.763277 1.3014L12.7527 12.2329L24.7422 1.3014"
                                  stroke="#BFD730"/>
                        </svg>

                        <div class="tabs-wrapper hide-scrollbar">
                            <?php foreach ($term as $x) { ?>
                                <div class="tab" data-slug="<?= $x->slug ?>">
                                    <?= $x->name ?>
                                </div>
                            <?php } ?>
                        </div>

                    </div>
                </div>

            <?php } ?>
            <div class="result-wrapper">
                <span><?= $posts_num ?> </span> results
            </div>
        </div>
        <!--    ///////////-->
        <div class="cards-section">

            <section class="related_products_section"
                     data-section-class="related_products_section">
                <div class="container">
                    <?php if ($have_posts) { ?>
                        <div class="cards-wrapper" posts-container>
                            <?php while ($the_query->have_posts()) {
                                $the_query->the_post();
                                get_template_part("template-parts/product-card", '', array('post_id' => get_the_ID()));
                                ?>
                            <?php } ?>
                        </div>
                    <?php } ?>



                    <?php $args['paged'] = 2; ?>
                    <div class="no-posts headline-2 <?= $have_posts ? '' : 'active' ?>">
                        <?= __('No Posts Here :(', 'smart_dv') ?></div>
                    <?php $args['paged'] = 2 ?>
                    <div class="load-more-wrapper load-more-resources iv-st-from-bottom <?= $the_query->max_num_pages <= 1 ? "hidden" : "" ?>"
                         data-args='<?= json_encode($args) ?>'
                         data-template="template-parts/product-card">
                        <a aria-label="Load More Posts"
                           class="cta-link load-more-btn"><?= __('Load More', 'smart_dv') ?>
                            <svg width="10" height="10" viewBox="0 0 10 10" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <line x1="5" y1="10" x2="5" y2="5.96046e-07" stroke="#BFD730"
                                      stroke-width="2"/>
                                <line x1="10" y1="5" x2="5.96046e-07" y2="5" stroke="#BFD730"
                                      stroke-width="2"/>
                            </svg>

                        </a>
                        <div class="loader">
                            <div class="loader-ball"></div>
                            <div class="loader-ball"></div>
                            <div class="loader-ball"></div>

                        </div>
                    </div>

                </div>
            </section>
        </div>
    </div>

</div>
</section>


<!-- endregion samrt_dv Block -->

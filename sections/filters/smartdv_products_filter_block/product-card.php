<?php
$post_id = @$args['post_id'] ?: get_the_ID();
$product_title = get_the_title($post_id);
$permalink = get_the_permalink();
$overview = get_field("overview", $post_id);
$type = get_field("type", $post_id);
$verification_solutions = get_field("verification_solutions", $post_id);


$terms_v1 = get_the_terms($post_id, "verification1");
$terms_v2 = get_the_terms($post_id, "verification2");
$terms_d = get_the_terms($post_id, "design_ip");

?>
<a class="card iv-st-from-bottom" href="<?= $permalink ?>">
  <div class="content">
    <?php if ($product_title) { ?>
      <div class="title">
        <button class="headline-4" href="<?= $permalink ?>"> <?= $product_title ?></button>
      </div>
    <?php } ?>


    <div class="description paragraph">
      <?php if (!empty($terms_v1) && is_array($terms_v1)) { ?>

        <?php foreach ($terms_v1 as $term) { ?>
          <?= $term->name; ?>
        <?php }
      } ?>

      <?php if (!empty($terms_v2) && is_array($terms_v2)) { ?>
        <?php foreach ($terms_v2 as $term) { ?>
          <?= $term->name; ?>
        <?php }
      } ?>

      <?php if (!empty($terms_d) && is_array($terms_d)) { ?>
        <?php foreach ($terms_d as $term) { ?>
          <?= $term->name; ?>
        <?php }
      } ?>
    </div>

  </div>
  <div class="content-description">
    <?= $overview ?>
  </div>
  <div class="card-link">
    <div class="cta-link">
      <?= __(' Learn More', 'smart_dv') ?>
      <svg class="arrow-link" width="23" height="12"
           viewBox="0 0 23 12"
           fill="none">
        <rect y="5" width="13" height="2" fill="#FFFFFF"/>
        <path d="M13 0L23 6L13 12V0Z" fill="#FFFFFF"/>
      </svg>
    </div>
  </div>

</a>

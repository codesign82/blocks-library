import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {requestBodyGenerator} from "../../../scripts/functions/requestBodyGenerator";
import {stringToHTML} from "../../../scripts/functions/stringToHTML";

import '../../../scripts/sitesSizer/propellerhealth';

import {gsap} from "gsap";


const blockScript = async (container = document) => {
    const block = container.querySelector('.products_filter_block');


    const filterSection = block.querySelectorAll('.filer-section');
    const postsNum = block?.querySelector('.result-wrapper span');
    const initialPostsNum = postsNum.textContent;
    const mobileMedia = window.matchMedia('(max-width: 600px)');


    const filterValues = {};


    const postsContainer = block.querySelector('[posts-container]');
    const loadMore = block.querySelector('.load-more-wrapper');
    const noPosts = block.querySelector('.no-posts');
    const initialLoadMoreArgs = loadMore && JSON.parse(loadMore.dataset.args);
    const initialHasMore = !loadMore?.classList.contains('hidden');
    const initialPostsString = postsContainer?.innerHTML;
    let loadMoreStatus = !loadMore.classList.contains('hidden');

    const getAllSelectsQuery = () => {
        const tax_query = {};
        let hasTax = false;
        let index = 0;
        Object.entries(filterValues).forEach(([key, value]) => {
            if (value) {
                hasTax = true;
                tax_query[index] = {
                    taxonomy: key,
                    terms: [value],
                    field: 'slug',
                    operator: 'IN'
                }
                index++
            }
        })

        hasTax && (tax_query.relation = 'AND');
        return hasTax ? tax_query : null;
    }


    const categoryFilterChangeHandler = () => {
        const tax_query = getAllSelectsQuery();
        loadMore.dataset.args = JSON.stringify({
            ...initialLoadMoreArgs,
            tax_query: tax_query || [],
            paged: (!tax_query) ? 2 : 1,
        });
        postsContainer.innerHTML = '';
        if (!tax_query) {
            loadMore.classList.toggle('hidden', !initialHasMore);
            const posts = stringToHTML(initialPostsString);
            postsNum.innerHTML = initialPostsNum;

            for (let post of posts) {
                postsContainer.appendChild(post);
            }
            animations(posts);
            imageLazyLoading(posts);
            return;
        }
        loadMore.click();
    };


    if (loadMore) {
        loadMore.addEventListener('click', async () => {
            if (loadMore.classList.contains('loading')) return;
            loadMore.classList.add('loading');
            loadMore.classList.remove('hidden');
            noPosts.classList.remove('active');

            const response = await fetch(theme_ajax_object.ajax_url, {
                method: 'POST',
                body: requestBodyGenerator(loadMore),
            });

            loadMore.classList.remove('loading');
            const hasMorePages = !!response.headers.get('X-WP-Has-More-Pages');
            const totalPages = +response.headers.get('X-WP-Total-Pages');
            const totalPosts = response.headers.get('X-WP-Total-Posts')

            postsNum.innerHTML = totalPosts;

            noPosts.classList.toggle('active', totalPages === 0);
            loadMore.classList.toggle('hidden', !hasMorePages);
            loadMoreStatus = hasMorePages;
            const htmlString = await response.json();
            const oldArgs = JSON.parse(loadMore.dataset.args);
            oldArgs.paged++;
            loadMore.dataset.args = JSON.stringify(oldArgs);
            const posts = stringToHTML(htmlString.data);
            for (let post of posts) {
                postsContainer.appendChild(post);
            }
            animations(posts);
            imageLazyLoading(posts);
        })
    }

    filterSection.forEach((section) => {
        const tax = section.dataset.tax;
        const tabs = section.querySelectorAll(".tab")
        const filter = section.querySelector(".filter")
        const filterTitle = filter.querySelector('.filter-title');
        const tabsWrapper = section.querySelector(" .tabs-wrapper")
        let currentActiveTab;


        filter?.addEventListener('click', function (e) {
            if (!mobileMedia.matches) return;

            const isOpened = tabsWrapper?.classList.toggle('active');

            if (!isOpened) {

                document.documentElement.classList.remove("modal-opened")
                gsap.to(tabsWrapper, {height: 0});
                filter.classList.remove('opened');

            } else {

                gsap.to(tabsWrapper, {height: 'auto'});
                filter.classList.add('opened');
            }
        });


        tabs.forEach((tab, index) => {
            const termSlug = tab.dataset.slug;
            tab.addEventListener("click", () => {
                if (currentActiveTab === index) {
                    tab.classList.remove("selected-tab");
                    currentActiveTab = '';
                    filterTitle.textContent = "Select Industry";
                } else {
                    tab.classList.add("selected-tab");
                    tabs[currentActiveTab]?.classList.remove("selected-tab");
                    currentActiveTab = index;
                    filterTitle.textContent = termSlug;
                }
                filterValues[tax] = filterValues[tax] === termSlug ? "" : termSlug;
                categoryFilterChangeHandler();
            })

        })
    })


    animations(block);
    imageLazyLoading(block);

};
windowOnLoad(blockScript);



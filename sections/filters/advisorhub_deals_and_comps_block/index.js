import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {ScrollTrigger} from "gsap/ScrollTrigger";
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/advisorhub';
import "select2";
import $ from "jquery";




const blockScript = async (container = document) => {
  const block = container.querySelector('.deals_and_comps_block');

  // add block code here
  const selects = block.querySelectorAll('select.general-select');
  const bonusSizeOptions = block.querySelectorAll('select.bonus-size option');
  const firmTypeOptions = block.querySelectorAll('select.firm-type option');
  const firmSizeOption = block.querySelectorAll('select.firm-size option');
  const postsContainer = block.querySelector('[posts-container]');
  const loadMore = block.querySelector('.load-more-wrapper');
  const noPosts = block.querySelector('.no-posts');
  const initialLoadMoreArgs = loadMore && JSON.parse(loadMore.dataset.args);


  const initialBonusSizeObj = [...bonusSizeOptions].reduce((acc, el) => {
    if (el.value !== "initial") {
      acc[el.value] = el.dataset.initialCount
    }
    return acc
  }, {});
  const initialFirmTypeObj = [...firmTypeOptions].reduce((acc, el) => {
    if (el.value !== "initial") {
      acc[el.value] = el.dataset.initialCount
    }
    return acc
  }, {});
  const initialFirmSizeObj = [...firmSizeOption].reduce((acc, el) => {
    if (el.value !== "initial") {
      acc[el.value] = el.dataset.initialCount
    }
    return acc
  }, {});

  // const initialHasMore = !loadMore?.classList.contains('hidden');
  // const initialPostsString = postsContainer?.innerHTML;
  let loadMoreStatus = !loadMore.classList.contains('hidden');
  let firstTime = ''
  const getAllSelectsQuery = () => {
    const tax_query = {};
    let hasTax = false;
    let taxQuery = [];
    firstTime = '';
    for (let i = 0; i < selects.length; i++) {
      const select = selects[i];
      if (select.value !== 'initial') {
        hasTax = true;
        tax_query[i] = {
          taxonomy: select.dataset.taxonomy,
          terms: [String(select.value)],
          field: 'slug',
        }
        taxQuery.push(select.dataset.taxonomy);
      }
    }
    length = taxQuery.length;

    if (taxQuery.length === 1) {
      firstTime = taxQuery[0]
    }
    // else {
    //   index++;
    //   firstTime = 'none'
    // }

    hasTax && (tax_query.relation = 'AND');
    return hasTax ? tax_query : null;

  }
  const categoryFilterChangeHandler = (event) => {
    const tax_query = getAllSelectsQuery();
    loadMore.dataset.args = JSON.stringify({
      ...initialLoadMoreArgs,
      tax_query,
      paged: (!tax_query) ? 2 : 1
    });
    postsContainer.innerHTML = '<span class="filter-loader"></span>';
    loadMore.click();
  };
  const getTerms = (options, obj, tax, select) => {
    options.forEach((option) => {
      if (option.value !== "initial") {
        $(option).html(option.dataset.termName + ` (${(obj[option.value]) || 0})`);
      }
    })
    $(select).select2();
  }
  if (loadMore) {
    loadMore.addEventListener('click', async () => {
      if (loadMore.classList.contains('loading')) return;
      loadMore.classList.add('loading');
      loadMore.classList.remove('hidden');
      // noPosts.classList.remove('active');
      const response = await fetch(theme_ajax_object.ajax_url, {
        method: 'POST',
        body: requestBodyGenerator(loadMore),
      })

      loadMore.classList.remove('loading')
      const hasMorePages = !!response.headers.get('X-WP-Has-More-Pages');
      const hasPosts = !!response.headers.get('X-WP-Has-Posts');
      noPosts.classList.toggle('active', !hasPosts)
      loadMore.classList.toggle('hidden', !hasMorePages);
      loadMoreStatus = hasMorePages;
      const htmlString = await response.json();
      let data = htmlString.data[0];
      const oldArgs = JSON.parse(loadMore.dataset.args);
      oldArgs.paged++;
      loadMore.dataset.args = JSON.stringify(oldArgs);
      if (!hasPosts) {
        gsap.fromTo(noPosts, {
          autoAlpha: 0,
          scale: 0,
          transformOrigin: '50% 50%'
        }, {
          duration: .4,
          autoAlpha: 1,
          scale: 1,
          ease: 'power2.out',
          stagger: .1,
          clearProps: 'transform',
        });
      }
      const posts = stringToHTML(data);
      //region handle select sequence

      let terms = htmlString.data[1];
      let bonusSizeObj = {};
      let firmTypeObj = {};
      let firmSizeObj = {};

      if (terms) {
        Object.keys(terms).forEach((key, index) => {
          let keys = key.split('~');
          let val = terms[key];
          const [tax, termSlug, termName] = keys;
          if (tax === "bonus_size") {
            bonusSizeObj[termSlug] = val;
          } else if (tax === "firm_size") {
            firmSizeObj[termSlug] = val;
          } else {
            firmTypeObj[termSlug] = val;
          }
        })
      }

      // console.log(firstTime)

      if (firstTime !== "firm_size") {
        getTerms(firmSizeOption, firmSizeObj, 'firm_size', block.querySelector('select.firm-size'));
      } else {
        getTerms(firmSizeOption, initialFirmSizeObj, 'firm_size', block.querySelector('select.firm-size'));
      }

      if (firstTime !== "firm_type") {
        getTerms(firmTypeOptions, firmTypeObj, 'firm_type', block.querySelector('select.firm-type'));
      } else {
        getTerms(firmTypeOptions, initialFirmTypeObj, 'firm_type', block.querySelector('select.firm-type'));
      }

      if (firstTime !== "bonus_size") {
        getTerms(bonusSizeOptions, bonusSizeObj, 'bonus_size', block.querySelector('select.bonus-size'));
      } else {
        getTerms(firmTypeOptions, initialBonusSizeObj, 'firm_type', block.querySelector('select.firm-type'));
      }

      //endregion handle select sequence
      postsContainer.innerHTML = '';
      for (let post of posts) {
        post.classList.add('iv-st-zoom');
        postsContainer.appendChild(post);
      }
      animations(posts);
      imageLazyLoading(posts);
      ScrollTrigger.refresh();
    })
  }


  for (let select of selects) {
    // region select2
    const placeholder = select.dataset.placeholder;
    let selectLabel = $(select).closest('.general-label');
    $(select).select2({
      placeholder: placeholder,
      width: '100%',
      // dropdownAutoWidth : true,
      // selectionCssClass: 'input-select2',
      // dropdownCssClass: 'input-select2-dropdown',
      dropdownParent: selectLabel,
      minimumResultsForSearch: -1
    });
    $(select).on("select2:open", function (e) {
      $(selects).closest('.general-label').removeClass('select-disabled');
      $(select).closest('.general-label').siblings().addClass('select-disabled');
    })
    $(select).on("select2:close", function (e) {
      $(selects).closest('.general-label').removeClass('select-disabled');
    })
    $(select).on("select2:selecting", function (e) {
      if (loadMore.classList.contains('loading')) {
        e.preventDefault();
      }
    })
    $(select).on('select2:select', function (e) {
      e.currentTarget.dispatchEvent(new Event("change"));
      $(selects).closest('.general-label').removeClass('select-disabled');
    });

    select.addEventListener('change', categoryFilterChangeHandler)
  }


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'deals_and_comps_block';
$className = 'deals_and_comps_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/deals_and_comps_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_title = get_field('main_title');
$description = get_field('description');
$has_border = get_field('has_border');
?>
<!-- region advisorhub's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <?php if ($main_title) { ?>
        <div class="main-title headline-1 center"><?= $main_title ?></div>
    <?php } ?>
    <div class="filter">
        <div class="filter-title medium-sub-title">Filter By</div>
        <div class="filter-sorts">
            <?php $region = get_terms(array(
                'taxonomy' => 'firm_size',
                'hide_empty' => true
            ));
            if (!empty($region) && is_array($region)) { ?>
                <label class="general-label">
                    <select class="js-select2 firm-size general-select" data-taxonomy="firm_size" data-placeholder="Firm Size">
                        <option value="initial" selected><?= __('Firm Size', 'advisorhub') ?></option>
                        <?php
                        foreach ($region as $term) {
                            ?>
                            <option data-term-name="<?= $term->name ?>" data-initial-count="<?= $term->count ?>" value="<?= $term->slug ?>"><?= $term->name; ?>
                                (<?= $term->count ?>)
                            </option>
                            <?php
                        } ?>
                    </select>
                </label>
            <?php } ?>

            <?php $region = get_terms(array(
                'taxonomy' => 'firm_type',
                'hide_empty' => true
            ));
            if (!empty($region) && is_array($region)) { ?>
                <label class="general-label">
                    <select class="js-select2 firm-type general-select" data-taxonomy="firm_type" data-placeholder="Firm Type">
                        <option value="initial" selected><?= __('Firm Type', 'advisorhub') ?></option>
                        <?php
                        // add links for each category
                        foreach ($region as $term) {
                            ?>
                            <option data-term-name="<?= $term->name ?>" data-initial-count="<?= $term->count ?>" value="<?= $term->slug ?>"><?= $term->name; ?>
                                (<?= $term->count ?>)
                            </option>
                            <?php
                        } ?>
                    </select>
                </label>
            <?php } ?>


            <?php $region = get_terms(array(
                'taxonomy' => 'bonus_size',
                'hide_empty' => true
            ));
            if (!empty($region) && is_array($region)) { ?>
                <label class="general-label">
                    <select class="js-select2 bonus-size general-select" data-taxonomy="bonus_size" data-placeholder="Bonus Size">
                        <option value="initial" selected><?= __('Bonus Size', 'advisorhub') ?></option>
                        <?php
                        // add links for each category
                        foreach ($region as $term) {
                            ?>
                            <option data-term-name="<?= $term->name ?>" data-initial-count="<?= $term->count ?>" value="<?= $term->slug ?>"><?= $term->name; ?>
                                (<?= $term->count ?>)
                            </option>
                            <?php
                        } ?>
                    </select>
                </label>
            <?php } ?>

        </div>
    </div>


    <div class="deals-and-comps block-wrapper <?= $has_border ? 'has-border' : ''?>">
        <?php
        $query_options_for_cpt = get_field('query_options_for_cpt');
        $number_of_posts = @$query_options_for_cpt['number_of_posts'];
        $order = @$query_options_for_cpt['order'];
        $args = array(
            'post_type' => 'deals_and_comps',
            'posts_per_page' => $number_of_posts,
            'orderby' => array( 'menu_order' => 'ASC' ),
            'post_status' => 'publish'
        );
        // The Query
        $the_query = new WP_Query($args);
        $have_posts = $the_query->have_posts();
        // The Loop
        if ($have_posts) { ?>
            <div class="row row-cols-1 row-cols-md-4" posts-container>
                <?php while ($the_query->have_posts()) {
                    $the_query->the_post();
                    get_template_part('template-parts/deals-and-comps', '', array('post_id' => get_the_id()));
                    ?>
                <?php } ?>
            </div>
        <?php }
        /* Restore original Post Data */
        wp_reset_postdata(); ?>
        <?php $args['paged'] = 2;
        $args['post__not_in'] = null;
        ?>
        <div class="no-posts headline-2 <?= $have_posts ? '' : 'active' ?>"><?= __('No Deals & Comps', 'advisorhub') ?></div>
        <div class="load-more-wrapper <?= $the_query->max_num_pages <= 1 ? "hidden" : "" ?>" style="display:none;"
             data-args='<?= json_encode($args) ?>'
             data-template="template-parts/deals-and-comps"></div>
    </div>
    <?php if ($description) { ?>
        <div
            class="description paragraph paragraph-12 text-center"><?= $description ?></div>
    <?php } ?>
</div>
</section>


<!-- endregion advisorhub's Block -->

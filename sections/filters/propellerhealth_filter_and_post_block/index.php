<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'filter_and_posts_block';
$className = 'filter_and_posts_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/filter_and_posts_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$automatic_or_manual = get_field('automatic_or_manual');
$tags = get_tags(array( 'hide_empty' => true));

?>
<!-- region propellerhealth Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <?php if (!$automatic_or_manual) { ?>
        <!--filter block -->
        <div class="filter-wrapper iv-st-from-bottom">
            <div class='filter-content'>
                <div class='search-input-wrapper'>
                    <input class='search-input input' placeholder='Search' type='search'>
                    <svg class='search-icon' width='20' height='20' viewBox='0 0 20 20'
                         fill='none'
                         xmlns='http://www.w3.org/2000/svg'>
                        <path
                                d='M12.5351 14.4045C11.2377 15.3286 9.65061 15.8734 7.93548 15.8734C3.55349 15.8734 0 12.3193 0 7.93669C0 3.55404 3.55349 0 7.93548 0C12.3175 0 15.871 3.55404 15.871 7.93669C15.871 9.65209 15.3268 11.2394 14.4023 12.537L19.6165 17.752C20.1297 18.2653 20.126 19.0881 19.6165 19.5971L19.5952 19.6184C19.0873 20.1264 18.2593 20.128 17.7504 19.6184L12.5362 14.4034L12.5351 14.4045ZM7.93548 14.0059C11.2868 14.0059 14.0038 11.2885 14.0038 7.93669C14.0038 4.58487 11.2868 1.86746 7.93548 1.86746C4.58417 1.86746 1.86717 4.58487 1.86717 7.93669C1.86717 11.2885 4.58417 14.0059 7.93548 14.0059Z'
                                fill='#003B72'/>
                    </svg>
                </div>
                <div class='accordion-in-mob'>
                    <button class='show-filter'>
                        <span class='show'><?=_e('show Filters','propellerhealth') ?></span>
                        <span class='hide'><?=_e('Hide Filters','propellerhealth') ?></span>
                        <svg class='filter-icon' width='25' height='24' viewBox='0 0 25 24'
                             fill='none' xmlns='http://www.w3.org/2000/svg'>
                            <g clip-path='url(#clip0_998_18684)'>
                                <path
                                        d='M24.256 0.748291H0.744141L10.2828 10.3423V23.2516L14.7174 19.3002V10.3423L24.256 0.748291Z'
                                        stroke='#0075E3' stroke-width='2' stroke-linecap='round'
                                        stroke-linejoin='round'/>
                            </g>
                            <defs>
                                <clipPath id='clip0_998_18684'>
                                    <rect width='25' height='24' fill='white'/>
                                </clipPath>
                            </defs>
                        </svg>

                    </button>
                    <div class='dropdown-wrapper'>
                        <p class='select-placeholder'><?= _e( 'Select Tag', 'propellerHealth' ) ?></p>
                        <div class='select-wrapper'>
                            <select class='topic_select select input' name='topic' id=' '>
                                <?php foreach ($tags as $tag) {
                                    ; ?>
                                    <option value='<?php $tag->term_id ?>'><?php echo $tag->name ?></option>
                                <?php } ?>
                            </select>
                            <svg class='down-arrow' width='12' height='8' viewBox='0 0 12 8'
                                 fill='none' xmlns='http://www.w3.org/2000/svg'>
                                <path d='M1 1L6 6L11 1' stroke='#003B72' stroke-width='2'/>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <div class='filter-results-wrapper'>
                <div class='number-of-results paragraph'>21 result for :</div>
                <div class='categories'>articles
                    <svg class='close' width='12' height='12' viewBox='0 0 12 12'
                         fill='none' xmlns='http://www.w3.org/2000/svg'>
                        <path d='M1 1L11 11' stroke='#0075E3' stroke-width='2'/>
                        <path d='M11 1L1 11' stroke='#0075E3' stroke-width='2'/>
                    </svg>
                </div>
            </div>
        </div>
        <!--   end block-->
    <?php } ?>
    <?php if ($automatic_or_manual) { ?>
        <?php
        $posts = get_field('posts');
        if ($posts): ?>
            <div class="resources-cards">
                <?php foreach ($posts as $post):
                    // Setup this post for WP functions (variable must be named $post).
                    setup_postdata($post);
                    get_template_part("template-parts/post-card", '', array(
                        'post_id' => $post,
                    ));

                    ?>
                <?php endforeach; ?>
            </div>
            <?php
            // Reset the global post object so that the rest of the page works correctly.
            wp_reset_postdata(); ?>
        <?php endif; ?>
    <?php } else { ?>
        <div class="resources-cards" posts-container>
            <?php
            $query_options = get_field('query_options');
            $number_of_posts = @$query_options['number_of_posts'];
            $post_type = 'post';
            $order = @$query_options['order'];
            $paged = (get_query_var('page_val') ? get_query_var('page_val') : 1);

            $args = array(
                'paged' => $paged,
                'post_type' => $post_type,
                'posts_per_page' => $number_of_posts,
                'order' => $order,
                'post_status' => 'publish',
            );
            // The Query
            $the_query = new WP_Query($args);
            // The Loop
            if ($the_query->have_posts()) { ?>
                <?php while ($the_query->have_posts()) {
                    $the_query->the_post();
                    get_template_part('template-parts/post-card', '', array('post_id' => get_the_id()));
                    ?>
                <?php } ?>
            <?php }
            wp_reset_postdata(); ?>
            <?php $args['paged'] = 2; ?>
            <div class="no-posts headline-2 <?= $the_query->have_posts() ? '' : 'active' ?>">
                <?= __('No Case Study Here :(', 'propellerhealth_client') ?></div>
            <div class="load-more-wrapper iv-st-from-bottom <?= $the_query->max_num_pages <= 1 ? "hidden" : "" ?>"
                 data-args='<?= json_encode($args) ?>'
                 data-template="template-parts/case-study-archive">
                <button aria-label="Load More Posts" class="cta-button   load-more-btn"><?= __('Load More', 'propellerhealth_client') ?>
                </button>
                <div class="loader">
                    <div class="loader-ball"></div>
                    <div class="loader-ball"></div>
                    <div class="loader-ball"></div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
</section>


<!-- endregion propellerhealth Block -->

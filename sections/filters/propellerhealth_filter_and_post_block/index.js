import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {ScrollTrigger} from "gsap/ScrollTrigger";
import {requestBodyGenerator} from "../../../scripts/functions/requestBodyGenerator";
import {stringToHTML} from "../../../scripts/functions/stringToHTML";
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/propellerhealth';

const blockScript = async (container = document) => {
  const block = container.querySelector('.filter_and_post_block');

  const postsContainer = block.querySelector('[posts-container]');
  const loadMore = block.querySelector('.load-more-wrapper');
  const noPosts = block.querySelector('.no-posts');
  const initialLoadMoreArgs = loadMore && JSON.parse(loadMore.dataset.args);
  const initialHasMore = !loadMore?.classList.contains('hidden');
  const initialPostsString = postsContainer?.innerHTML;
  let loadMoreStatus = !loadMore.classList.contains('hidden');
  const searchInput = block.querySelector(".search-input");
  const selectTypeEl = block.querySelector(".tag_select");
  const select2El = $(".tag_select").select2({
    width: '100%',
  });
  const resNum = block.querySelector(".res-num");
  const allResultsNum = +resNum.textContent;
  const categoriesContainer = block.querySelector(".categories-cont");
  const filterResultsWrapper = block.querySelector(".filter-results-wrapper");
  let isSearchedBefore = false;

  const getAllSelectsQuery = () => {
    const tax_query = {};
    let hasTax = false;
    if (selectTypeEl.selectedOptions.length) {
      hasTax = true;
      tax_query[0] = {
        taxonomy: 'post_tag',
        terms: [...selectTypeEl.selectedOptions].map(({value}) => String(value)),
        field: 'slug',
      };
    }
    hasTax && (tax_query.relation = 'AND');
    return hasTax ? tax_query : null;
  };

  const createFilterCategory = (text) => {
    const categoryEl = document.createElement("div");
    categoryEl.classList.add("categories");
    categoryEl.textContent = text;
    categoriesContainer.append(categoryEl);
    return categoryEl;
  };

  const handleCategoriesFilters = (filterFunction) => {
    categoriesContainer.innerHTML = '';
    if (selectTypeEl.selectedOptions.length) {
      [...selectTypeEl.selectedOptions].forEach((option) => {
        createFilterCategory(option.textContent).addEventListener("click", () => {
          const values = [];
          [...selectTypeEl.selectedOptions].forEach((el) => {
            if (el.value !== option.value) {
              values.push(el.value);
            }
          });
          select2El.val(values).trigger("change");
          filterFunction();
        });
      });
    }
    if (searchInput.value) {
      createFilterCategory(searchInput.value).addEventListener("click", () => {
        searchInput.value = "";
        isSearchedBefore = false;
        filterFunction();
      });
    }
  };

  const categoryFilterChangeHandler = () => {
    const tax_query = getAllSelectsQuery();
    handleCategoriesFilters(categoryFilterChangeHandler);
    loadMore.dataset.args = JSON.stringify({
      ...initialLoadMoreArgs,
      tax_query: tax_query || [],
      s: searchInput.value.trim(),
      orderby: {'date': "DESC"},
      paged:(!tax_query && !searchInput.value.trim()) ? 2 : 1,
    });
    postsContainer.innerHTML = '';
    if (!tax_query && !searchInput.value.trim()) {
      loadMore.classList.toggle('hidden', !initialHasMore);
      const posts = stringToHTML(initialPostsString);
      filterResultsWrapper.classList.remove("active");
      noPosts.classList.toggle('active', allResultsNum === 0);
      for (let post of posts) {
        postsContainer.appendChild(post);
      }
      animations(posts);
      imageLazyLoading(posts);
      return;
    }

    loadMore.click();
  };
  if (loadMore) {
    loadMore.addEventListener('click', async () => {
      if (loadMore.classList.contains('loading')) return;
      loadMore.classList.add('loading');
      loadMore.classList.remove('hidden');
      noPosts.classList.remove('active');
      const response = await fetch(theme_ajax_object.ajax_url, {
        method: 'POST',
        body: requestBodyGenerator(loadMore),
      });
      loadMore.classList.remove('loading');
      const hasMorePages = !!response.headers.get('X-WP-Has-More-Pages');
      const totalPages = +response.headers.get('X-WP-Total-Pages');
      resNum.textContent = response.headers.get('X-WP-Total-Posts');
      filterResultsWrapper.classList.add("active");
      noPosts.classList.toggle('active', totalPages === 0);
      loadMore.classList.toggle('hidden', !hasMorePages);
      loadMoreStatus = hasMorePages;
      const htmlString = await response.json();
      const oldArgs = JSON.parse(loadMore.dataset.args);
      oldArgs.paged++;
      loadMore.dataset.args = JSON.stringify(oldArgs);
      const posts = stringToHTML(htmlString.data);
      for (let post of posts) {
        postsContainer.appendChild(post);
      }
      animations(posts);
      imageLazyLoading(posts);
    });
  }


  $(selectTypeEl).on("select2:selecting", function (e) {
    if (loadMore.classList.contains('loading')) {
      e.preventDefault();
    }
  });

  $(selectTypeEl).on('select2:select select2:unselect', function (e) {
    e.currentTarget.dispatchEvent(new Event("change"));
  });

  selectTypeEl.addEventListener("change", categoryFilterChangeHandler);
  block.querySelector(".search-input-wrapper").addEventListener("submit", (e) => {
    e.preventDefault();
    if (!loadMore.classList.contains('loading')) {
      if (searchInput.value) {
        isSearchedBefore = true;
        categoryFilterChangeHandler();
      } else if (!searchInput.value && isSearchedBefore) {
        categoryFilterChangeHandler();
        isSearchedBefore = false;
      }
    }
  });


  const filterBtn = block.querySelector('.show-filter');
  const dropDownWrapper = block.querySelector('.dropdown-wrapper');
  const filterResult = block.querySelector('.filter-results-wrapper');


  if (filterBtn) {
    filterBtn.addEventListener('click', () => {
      const opened = filterBtn.classList.toggle('active');
      gsap.to([dropDownWrapper, filterResult], {height: opened ? 'auto' : 0});
    });
  }


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'quick_links_block';
$className = 'quick_links_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/quick_links_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$quick_title = get_field('quick_title');
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="quick-links-group">
        <?php if ($quick_title) { ?>
            <div class="quick-title headline-6 iv-st-from-bottom"><?= $quick_title ?></div>
        <?php } ?>
        <?php if (have_rows('quick_links')) { ?>
            <div class="quick-wrapper">
                <?php while (have_rows('quick_links')) {
                    the_row();
                    $quick_link = get_sub_field('quick_link');
                    ?>
                    <?php if ($quick_link) { ?>
                        <a href="<?= $quick_link['url'] ?>" <?= $quick_link['target'] ?>
                           class="quick-links iv-st-from-bottom " data-hover><?= $quick_link['title'] ?></a>
                    <?php } ?>

                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>
</section>


<!-- endregion raistone's Block -->

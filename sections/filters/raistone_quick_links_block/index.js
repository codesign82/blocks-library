import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/raistone';

import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.quick_links_block');


  let tabs = block.querySelectorAll('.quick-links');

  for (let tab of tabs) {
    tab.addEventListener('click', () => {
      [...tab.closest('.quick-wrapper').children].forEach(sib => sib.classList.remove('active'))
      tab.classList.add('active')
    })
  }
  ScrollTrigger.saveStyles(block);
  ScrollTrigger.matchMedia({
    '(min-width: 992px)': () => {
      ScrollTrigger.create({
        trigger: block,
        endTrigger: document.querySelector('footer'),
        pin: block,
        pinSpacing: false,
        start: () => `top ${9.4 * parseFloat(document.documentElement.style.fontSize)}`,
        end: 'top bottom',
        onRefresh({isActive}) {
          isActive && gsap.to(block, {
            background: '#FFCF70',
            width: '100vw',
            maxWidth: '100vw',
            left: 0
          })
        },
        onToggle({isActive}) {
          const left = block.getBoundingClientRect().left;
          if (isActive) {
            gsap.to(block, {
              background: '#FFCF70',
              width: '100vw',
              maxWidth: '100vw',
              left: 0
            })
          } else {
            gsap.fromTo(block, {
              background: '#FFCF70',
              width: '100vw',
              maxWidth: '100vw',
              left: -left,
            }, {
              width: '100%',
              maxWidth: '100%',
              background: '#fff',
              left: 0,
            })
          }
          gsap.to(block.querySelector(".quick-links-group"), {borderColor: isActive ? 'transparent' : '#DDDDDD'})
        },
      })
      return () => gsap.to(block.querySelector(".quick-links-group"), {borderColor: '#DDDDDD'})
    }
  })

  // gsap.timeline()
  //   .to(block, {
  //     background: '#FFCF70',
  //     width: '100vw',
  //     maxWidth: '100vw',
  //     left: 0
  //   })


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


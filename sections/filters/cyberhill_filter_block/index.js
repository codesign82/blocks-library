import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {ScrollTrigger} from "gsap/ScrollTrigger";
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/cyberhill';

const blockScript = async (container = document) => {
  const block = container.querySelector('.filter_block');
  
  // add block code here
  
  let selector = block.querySelector('.selector');
  let linkItems = block.querySelectorAll('.link-item');
  let path = window.location.pathname;
  let page = path.split("/");
  selector.innerText = page [page.length - 2];
  console.log();
  if (page [page.length - 2] === "knowledge-portal") {
    selector.innerText = "All Posts";
  }
  for (let linkItem of linkItems) {
    linkItem.addEventListener('click', function (e) {
      if (linkItem.classList.contains('active')) return;
      for (let i = 0; i < linkItems.length; i++) {
        linkItems[i].classList.remove('active');
      }
      linkItem.classList.add('active');
    })
  }
  const mobileMedia = window.matchMedia('(max-width: 600px)');
  const dropMenu = block.querySelector('#drop-menu');
  const filterList = block.querySelector('.filter-list');
  dropMenu.addEventListener('click', function () {
    if (!mobileMedia.matches) return;
    const isOpened = dropMenu?.classList.toggle('active');
    if (!isOpened) {
      gsap.to(filterList, {height: 0});
    } else {
      gsap.to(filterList, {height: 'auto'});
    }
  });
  
  ScrollTrigger.create({
    trigger: block,
    endTrigger: 'footer',
    pin: dropMenu,
    pinSpacing: false,
    start: () => `top ${window.innerWidth >= 600
        ? ~~(8 * parseFloat(document.documentElement.style.fontSize) - 2)
        : ~~(7 * parseFloat(document.documentElement.style.fontSize) - 2)}`,
    end: 'bottom bottom',
    onToggle({isActive}) {
      gsap.to(dropMenu, {background: isActive ? 'rgba(13, 36, 63,.9)' : 'rgba(13, 36, 63,1)'})
    }
  })
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


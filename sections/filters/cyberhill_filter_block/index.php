<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'filter_block';
$className = 'filter_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/filter_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$int_nav = get_option('options_blog_taxonomies');

$cover_options = get_field('hero_cover_image', 'options');
$all_posts_link = get_field('all_posts_link', 'options');
?>
<!-- region CyberHill's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="nav drop-menu" id="drop-menu">
  <div class="selector paragraph-14">All Posts</div>
  <svg class="down-arrow" viewBox="0 0 10 6" fill="none">
    <path d="M5 6L0 0.999995L0.7 0.299995L5 4.6L9.3 0.299995L10 0.999995L5 6Z" fill="white"/>
  </svg>
  <ul class="reset-ul filter-list">
    <li class="link-item border-green">
      <a href="<?= $all_posts_link['url'] ?>" class="paragraph-14"><?= $all_posts_link['title'] ?></a>
    </li>
    <?php
    foreach ($int_nav as $nav_item) {
      ?>
      <li class="link-item border-green">
        <a href="<?= get_category_link($nav_item); ?>" class="paragraph-14"><?= get_cat_name($nav_item); ?></a>
      </li>
      <?php
    }
    ?>
  </ul>
</div>
<div class="container">
  <div class="content">
    <div class="search">
      <input type="search" onkeyup="portal_filter();" placeholder="What are you looking for?"/>
      <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M20.7499 19.6894L15.0859 14.0254C16.4469 12.3914 17.1256 10.2956 16.9808 8.17394C16.8359 6.05231 15.8787 4.06819 14.3082 2.63433C12.7378 1.20048 10.675 0.427284 8.54894 0.475596C6.42292 0.523908 4.39736 1.39001 2.89365 2.89372C1.38993 4.39743 0.523836 6.42299 0.475524 8.54901C0.427212 10.675 1.20041 12.7378 2.63426 14.3083C4.06812 15.8788 6.05224 16.836 8.17387 16.9808C10.2955 17.1257 12.3913 16.447 14.0253 15.0859L19.6894 20.75L20.7499 19.6894ZM1.99993 8.75001C1.99993 7.41498 2.39581 6.10994 3.13751 4.99991C3.87921 3.88987 4.93342 3.02471 6.16682 2.51382C7.40022 2.00293 8.75742 1.86925 10.0668 2.12971C11.3762 2.39016 12.5789 3.03303 13.5229 3.97703C14.4669 4.92104 15.1098 6.12377 15.3702 7.43315C15.6307 8.74252 15.497 10.0997 14.9861 11.3331C14.4752 12.5665 13.6101 13.6207 12.5 14.3624C11.39 15.1041 10.085 15.5 8.74993 15.5C6.96033 15.498 5.2446 14.7862 3.97916 13.5208C2.71371 12.2553 2.00192 10.5396 1.99993 8.75001Z" fill="#1D4070"/>
      </svg>

    </div>
    <div class="f-s">
      <div class="sort">
        <div class="paragraph-12">
          <?=__('SORT BY','cyberhill') ?>
        </div>
        <div class="select-wrapper">
          <select onchange="portal_filter(this.value);" class="paragraph-14">
            <option value="DESC"><?=__('Most Recent','cyberhill') ?></option>
            <option value="ASC"><?=__('Old to new','cyberhill') ?></option>
          </select>
          <div class="arrow-down">
            <svg width="10" height="6" viewBox="0 0 10 6" fill="none">
              <path d="M5 5.99999L0 0.999988L0.7 0.299988L5 4.59999L9.3 0.299988L10 0.999988L5 5.99999Z" fill="Black"/>
            </svg>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>


<!-- endregion CyberHill's Block -->

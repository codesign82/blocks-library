<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'careers';
$className = 'careers';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/careers/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');

$query_options = get_field('query_options');
$order = @$query_options['order'];
$posts_per_page = @$query_options['number_of_posts'];
$args = array(
    'post_type' => 'careers',
    'posts_per_page' => 4,
    'order' => $order,
    'post_status' => 'publish'
);
$the_query = new WP_Query($args);
$have_posts = $the_query->have_posts();

$terms = get_terms(array(
    'taxonomy' => 'careers_type',
    'hide_empty' => true,
));

?>
<!-- region samrt_dv Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <?php if ($title) { ?>
        <div class="headline-2 title word-up"><?= $title ?></div>
    <?php } ?>
    <div class="filter-content-wrapper" id="filter-content-wrapper">
        <div class="selector"><?= __('Select Category', 'smart_dv') ?></div>
        <svg class="down-arrow" width="26" height="13" viewBox="0 0 26 13"
             fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.761324 1.3014L12.7508 12.2329L24.7402 1.3014"
                  stroke="#BFD730"/>
        </svg>
        <?php if (!empty($terms) && is_array($terms)) { ?>
            <ul class="filter-list">
                <?php foreach ($terms as $term) { ?>
                    <li class="link-item tab Sales" data-slug="<?= $term->slug ?>">
                        <?= $term->name ?>
                    </li>
                <?php } ?>
            </ul>
        <?php } ?>
    </div>
    <div class="circles">
        <div class="circle"></div>
        <div class="line"></div>
        <div class="circle"></div>
    </div>
    <?php if ($have_posts) { ?>
        <div class="cards row row-cols-2-md row-cols-1" posts-container>
            <?php while ($the_query->have_posts()) {
                $the_query->the_post();
                get_template_part("template-parts/career-card", '', array('post_id' => get_the_ID()));
                ?>
            <?php } ?>

        </div>
    <?php } ?>


    <?php $args['paged'] = 2; ?>
    <div class="no-posts headline-2 <?= $have_posts ? '' : 'active' ?>">
        <?= __('No Posts Here :(', 'smart_dv') ?></div>
    <?php $args['paged'] = 2 ?>
    <div
            class="load-more-wrapper load-more-resources iv-st-from-bottom <?= $the_query->max_num_pages <= 1 ? "hidden" : "" ?>"
            data-args='<?= json_encode($args) ?>'
            data-template="template-parts/career-card">
        <a aria-label="Load More Posts"
           class="cta-link load-more-btn"><?= __('Load More', 'smart_dv') ?>
            <svg width="10" height="10" viewBox="0 0 10 10" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
                <line x1="5" y1="10" x2="5" y2="5.96046e-07" stroke="#BFD730"
                      stroke-width="2"/>
                <line x1="10" y1="5" x2="5.96046e-07" y2="5" stroke="#BFD730"
                      stroke-width="2"/>
            </svg>
        </a>
        <div class="loader">
            <div class="loader-ball"></div>
            <div class="loader-ball"></div>
            <div class="loader-ball"></div>
        </div>
    </div>

</div>
</section>

<!-- endregion samrt_dv Block -->

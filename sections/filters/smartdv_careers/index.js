import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {gsap} from "gsap";
import {requestBodyGenerator} from "../../../scripts/functions/requestBodyGenerator";
import {stringToHTML} from "../../../scripts/functions/stringToHTML";


import '../../../scripts/sitesSizer/propellerhealth';



const blockScript = async (container = document) => {
    const block = container.querySelector('.careers');


    const mobileMedia = window.matchMedia('(max-width: 600px)');
    const dropMenu = block.querySelector('#filter-content-wrapper');
    const selector = block.querySelector('.selector');
    const filterList = block.querySelector('.filter-list');
    const tabs = block.querySelectorAll('.tab');

    dropMenu.addEventListener('click', function (e) {
        if (!mobileMedia.matches) return;
        const isOpened = dropMenu.classList.toggle('active');
        if (!isOpened) {
            gsap.to(filterList, {height: 0});
        } else {
            gsap.to(filterList, {height: 'auto'});

        }
    });


    ////////////////////////////////////////////////////////


    const postsContainer = block.querySelector('[posts-container]');
    const loadMore = block.querySelector('.load-more-wrapper');
    const noPosts = block.querySelector('.no-posts');
    const initialLoadMoreArgs = loadMore && JSON.parse(loadMore.dataset.args);
    const initialHasMore = !loadMore?.classList.contains('hidden');
    const initialPostsString = postsContainer?.innerHTML;
    let loadMoreStatus = !loadMore.classList.contains('hidden');

    let filterValue = "";
    let currentActiveTab;


    const getAllSelectsQuery = () => {
        const tax_query = {};
        let hasTax = false;

        if (filterValue) {
            hasTax = true;
            tax_query[0] = {
                taxonomy: 'careers_type',
                terms: [filterValue],
                field: 'slug',
                operator: 'IN'
            }
        }


        hasTax && (tax_query.relation = 'AND');
        return hasTax ? tax_query : null;
    }


    const categoryFilterChangeHandler = () => {
        const tax_query = getAllSelectsQuery();
        loadMore.dataset.args = JSON.stringify({
            ...initialLoadMoreArgs,
            tax_query: tax_query || [],
            paged: (!tax_query) ? 2 : 1,
        });
        postsContainer.innerHTML = '';
        if (!tax_query) {
            loadMore.classList.toggle('hidden', !initialHasMore);
            const posts = stringToHTML(initialPostsString);
            for (let post of posts) {
                postsContainer.appendChild(post);

            }
            animations(posts);
            imageLazyLoading(posts);
            return;
        }

        loadMore.click();
    };


    if (loadMore) {
        loadMore.addEventListener('click', async () => {
            if (loadMore.classList.contains('loading')) return;
            loadMore.classList.add('loading');
            loadMore.classList.remove('hidden');
            noPosts.classList.remove('active');

            const response = await fetch(theme_ajax_object.ajax_url, {
                method: 'POST',
                body: requestBodyGenerator(loadMore),
            });

            loadMore.classList.remove('loading');
            const hasMorePages = !!response.headers.get('X-WP-Has-More-Pages');
            const totalPages = +response.headers.get('X-WP-Total-Pages');


            noPosts.classList.toggle('active', totalPages === 0);
            loadMore.classList.toggle('hidden', !hasMorePages);
            loadMoreStatus = hasMorePages;
            const htmlString = await response.json();
            const oldArgs = JSON.parse(loadMore.dataset.args);
            oldArgs.paged++;
            loadMore.dataset.args = JSON.stringify(oldArgs);
            const posts = stringToHTML(htmlString.data);
            for (let post of posts) {
                postsContainer.appendChild(post);
            }
            animations(posts);
            imageLazyLoading(posts);
        })
    }


    tabs.forEach((tab, index) => {
        const termSlug = tab.dataset.slug;

        tab.addEventListener("click", () => {
            if (currentActiveTab === index) {

                tab.classList.remove("selected");
                currentActiveTab = '';
                // filterTitle.textContent = "Select Industry";
                selector.textContent = "Select Category";

            } else {
                tab.classList.add("selected");
                selector.textContent = termSlug;
                tabs[currentActiveTab]?.classList.remove("selected");
                currentActiveTab = index;
                // filterTitle.textContent = termSlug;
            }
            filterValue = filterValue === termSlug ? "" : termSlug;
            categoryFilterChangeHandler();
        })

    })


    animations(block);
    imageLazyLoading(block);

};
windowOnLoad(blockScript);



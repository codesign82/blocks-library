<?php
$post_id = @$args['post_id'] ?: get_the_ID();
$locations = get_field('locations', $post_id);
$career_type = get_field('career_type', $post_id);
$title = get_the_title(get_the_ID());
$post_permalink = get_the_permalink(get_the_ID());


?>
<a class="account-card col iv-st-from-bottom" href="<?= $post_permalink ?>">
  <div class="content-card">
    <?php if ($title) { ?>
      <div class="headline-4 sub-title"><?= $title ?></div>
    <?php } ?>
    <?php if ($locations) { ?>
      <div class="paragraph disc">
        <?= $locations ?>
      </div>
    <?php } ?>
    <div class="cta-link last_link" ><?= __('Learn More', 'smart_dv') ?>
      <svg width="23" height="12" viewBox="0 0 23 12" fill="none"
           xmlns="http://www.w3.org/2000/svg">
        <path
          d="M23 5.90698L13 0.13348V11.6805L23 5.90698ZM0 6.90698H14V4.90698H0V6.90698Z"
          fill="#BFD730"/>
      </svg>
    </div>
  </div>
</a>

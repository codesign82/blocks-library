import './index.html';
import './style.scss';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/propellerhealth';
import {pageCleaningFunctions} from "../../../scripts/general/barba";
import {accordion} from "../../../scripts/general/accordion";
import {stringToHTML} from "../../../scripts/functions/stringToHTML";
import {requestBodyGenerator} from "../../../scripts/functions/requestBodyGenerator";
import {debouncedScrollTriggerRefresh} from "../../../scripts/functions/imageLazyLoading";
import {debounce} from "lodash/function";
import "select2";
import $ from "jquery";
import {gsap} from 'gsap';
import {ScrollTrigger} from "gsap/ScrollTrigger";
import Swiper, {Pagination} from "swiper";

gsap.registerPlugin(ScrollTrigger)

const blockScript = async (container = document) => {
  const block = container.querySelector('.companies_block');

  // add block code here
  const customModal = document.querySelector('#custom-modal');
  const customModalInner = customModal && customModal.querySelector('.custom-modal-inner');
  const customModalContent = customModal && customModal.querySelector('.custom-modal-content');

  function setAccordions(accordions, companies) {
    accordion(accordions);
    for (let accordion of accordions) {
      const addonCompany = accordion.querySelector('.addon-company-target');
      addonCompany.addEventListener('click', async (e) => {
        e.preventDefault();
        customModalInner.classList.add('modal-loading');
        customModalContent.innerHTML = companies[addonCompany.dataset.addonTarget];
        const accordions = [...customModalContent.querySelectorAll('.modal-accordion')];
        const cardSwiper = customModalContent.querySelector('.swiper');
        const slides = cardSwiper && cardSwiper.querySelectorAll('.swiper-slide');
        if (cardSwiper) {
          const swiperChecker = slides.length === 2 ? {allowTouchMove: false} : {
            pagination: {
              el: customModalContent.querySelector('.swiper-pagination'),
              clickable: true
            },
          };
          new Swiper(cardSwiper, {
            slidesPerView: 2,
            spaceBetween: 25,
            modules: [Pagination],
            ...swiperChecker,
          });
        }
        if (accordions.length > 0) {
          // request addons templates
          const data = new FormData();
          data.append('args', JSON.stringify({
            'post_type': 'companies',
            'posts_per_page': -1,
            'post__in': accordions.map(accordion => accordion.dataset.companyId)
          }));
          data.append('template', 'template-loops/company-card-template-only');
          data.append('_ajax_nonce', theme_ajax_object._ajax_nonce);
          data.append('action', 'company_card');
          const response = await (await fetch(theme_ajax_object.ajax_url, {
            method: 'POST',
            body: data,
          })).json();
          //endregion addons templates
          setAccordions(accordions, response.data);
        }
        customModalInner.classList.remove('modal-loading');
      })
    }
  }


  function showModal(companyCards) {
    for (let companyCard of companyCards) {
      const button = companyCard.querySelector('.show-popup')
      button?.addEventListener('click', async () => {
        const modalTemplate = button.closest('.company-card')?.querySelector('.modal-template');
        const modalType = button.dataset.modalType;
        customModal.classList.add('custom-modal-' + modalType);
        let clone = modalTemplate.content.cloneNode(true);
        customModalContent.appendChild(clone);
        button.blur();
        const accordions = [...customModalContent.querySelectorAll('.modal-accordion')];
        const swiper = customModalContent.querySelector('.swiper');
        const slides = customModalContent.querySelectorAll('.swiper-slide');
        const swiperChecker = slides.length === 2 ? {allowTouchMove: false} : {
          pagination: {
            el: customModalContent.querySelector('.swiper-pagination'),
            clickable: true
          },
        };
        if (swiper) {
          new Swiper(swiper, {
            slidesPerView: 2,
            spaceBetween: 25,
            modules: [Pagination],
            ...swiperChecker,
          });
        }
        // region prevent page scroll
        document.documentElement.classList.add('modal-opened')
        const scrollEnabled = new Event('scroll-disabled');
        document.dispatchEvent(scrollEnabled);
        // endregion prevent page scroll
        customModal.classList.add('modal-active');
        if (accordions.length > 0) {
          // request addons templates
          const data = new FormData();
          data.append('args', JSON.stringify({
            'post_type': 'companies',
            'posts_per_page': -1,
            'post__in': accordions.map(accordion => accordion.dataset.companyId)
          }));
          data.append('template', 'template-loops/company-card-template-only');
          data.append('_ajax_nonce', theme_ajax_object._ajax_nonce);
          data.append('action', 'company_card');
          const response = await (await fetch(theme_ajax_object.ajax_url, {
            method: 'POST',
            body: data,
          })).json();
          //endregion addons templates
          setAccordions(accordions, response.data);
        }
      })
    }
  }

  showModal(block.querySelectorAll('.company-card'));

  // region filter
  const postsContainer = block.querySelector('[posts-container]');
  const loadMore = block.querySelector('.load-more-wrapper');
  const noPosts = block.querySelector('.no-posts');
  const selects = block.querySelectorAll('select.general-select');
  const initialLoadMoreArgs = loadMore && JSON.parse(loadMore.dataset.args);
  const initialHasMore = !loadMore?.classList.contains('hidden');
  const initialPostsString = postsContainer?.innerHTML;
  let loadMoreStatus = !loadMore.classList.contains('hidden');
  const search = block.querySelector('.companies-search');
  const getAllSelectsQuery = () => {
    const tax_query = {};
    let hasTax = false;
    for (let i = 0; i < selects.length; i++) {
      const select = selects[i];
      if (select.value !== 'initial') {
        hasTax = true;
        tax_query[i] = {
          taxonomy: select.dataset.taxonomy,
          terms: [String(select.value)],
          field: 'slug',
        }
      }
    }
    hasTax && (tax_query.relation = 'AND');
    return hasTax? tax_query:null;

  }
  const categoryFilterChangeHandler = (event) => {
    const tax_query = getAllSelectsQuery();
    loadMore.dataset.args = JSON.stringify({
      ...initialLoadMoreArgs,
      tax_query,
      digihive_title: search.value.trim(),
      paged: (!tax_query && !search.value.trim()) ? 2 : 1
    });
    postsContainer.innerHTML = '';
    if (!tax_query && !search.value.trim()) {
      loadMore.classList.toggle('hidden', !initialHasMore);
      const posts = stringToHTML(initialPostsString);
      for (let post of posts) {
        postsContainer.appendChild(post);
        // const accordions = post.querySelectorAll('.modal-accordion');
        // if (accordions.length > 0) {
        //   accordion(accordions);
        // }
      }
      showModal(posts)
      animations(posts);
      imageLazyLoading(posts);
      debouncedScrollTriggerRefresh();
      setTimeout(scrollHandler, 100)
      return;
    }

    loadMore.click();
  };

  if (loadMore) {
    loadMore.addEventListener('click', async () => {
      if (loadMore.classList.contains('loading')) return;
      loadMore.classList.add('loading');
      loadMore.classList.remove('hidden');
      noPosts.classList.remove('active');

      const response = await fetch(theme_ajax_object.ajax_url, {
        method: 'POST',
        body: requestBodyGenerator(loadMore),
      })

      loadMore.classList.remove('loading')
      const hasMorePages = !!response.headers.get('X-WP-Has-More-Pages');
      const totalPages = +response.headers.get('X-WP-Total-Pages');
      noPosts.classList.toggle('active', totalPages === 0)
      loadMore.classList.toggle('hidden', !hasMorePages);
      loadMoreStatus = hasMorePages;
      const htmlString = await response.json();
      const oldArgs = JSON.parse(loadMore.dataset.args);
      oldArgs.paged++;
      loadMore.dataset.args = JSON.stringify(oldArgs);

      const posts = stringToHTML(htmlString.data);
      for (let post of posts) {
        postsContainer.appendChild(post);
      }
      showModal(posts)
      animations(posts);
      imageLazyLoading(posts);
      debouncedScrollTriggerRefresh();
      setTimeout(scrollHandler, 100)
    })
  }

  for (let select of selects) {
    // region select2
    const placeholder = select.dataset.placeholder;
    $(select).select2({
      placeholder: placeholder,
      width: '100%',
      minimumResultsForSearch: -1
    });
    $(select).on("select2:selecting", function (e) {
      if (loadMore.classList.contains('loading')) {
        e.preventDefault();
      }
    })
    $(select).on('select2:select', function (e) {
      e.currentTarget.dispatchEvent(new Event("change"));
    });
    // endregion select2
    select.addEventListener('change', categoryFilterChangeHandler)
  }


  // endregion filter

  // region search


  const searchDebounce = ()=>{
    categoryFilterChangeHandler();
  }
  search.addEventListener('input',(event) => {
    if (loadMore.classList.contains('loading')) {
      event.preventDefault();
      return;
    }
    searchDebounce();

  })
  // endregion search

  const debouncedScrollHandler = debounce(() => !loadMore.matches('.loading, .hidden') && loadMore?.click(), 300, {
    leading: true,
    maxWait: 500
  });
  const scrollHandler = () => {
    0 >= (block.getBoundingClientRect().bottom - window.innerHeight * (window.innerWidth <= 600 ? 5 : 3)) && debouncedScrollHandler()
  }
  window.addEventListener('scroll', scrollHandler)
  pageCleaningFunctions.push(() => window.removeEventListener('scroll', scrollHandler))


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


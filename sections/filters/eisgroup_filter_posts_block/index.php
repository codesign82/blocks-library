<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'eis_resources_block';
$className = 'eis_resources_block';
if ( ! empty( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
    $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/eis_resources_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title       = get_field( 'title' );
$description = get_field( 'description' );
?>
<!-- region dh-updated-theme's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<?php
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

$args       = [
    'paged'          => $paged,
    'post_type'      => 'eis-resources',
    'posts_per_page' => 10,
    'post_status'    => 'publish',
    'orderby'        => array( 'date' => 'DESC' )
];
$the_query  = new WP_Query( $args );
$have_posts = $the_query->have_posts();
?>

<div class="container">
    <div class="search-part">
        <div class="line"></div>
        <div class="search-part-content">
            <div class="sort-by-wrapper">
                <div class="sort-by"><?= __( 'Sort by:', 'eisgroup' ) ?></div>
                <select class="sort-by-result" value="ASC">
                    <option value="DESC"><?= __( 'Newest', 'eisgroup' ) ?></option>
                    <option value="ASC"><?= __( 'Oldest', 'eisgroup' ) ?></option>
                </select>
            </div>
            <form class="search-wrapper">
                <input class="search-input" id="search" type="search" placeholder="<?= __( 'Search here...', 'eisgroup' ) ?>">
                <label for="search">
                    <svg class="search-icon" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.75 14.7188L11.5625 10.5312C12.4688 9.4375 12.9688 8.03125 12.9688 6.5C12.9688 2.9375 10.0312 0 6.46875 0C2.875 0 0 2.9375 0 6.5C0 10.0938 2.90625 13 6.46875 13C7.96875 13 9.375 12.5 10.5 11.5938L14.6875 15.7812C14.8438 15.9375 15.0312 16 15.25 16C15.4375 16 15.625 15.9375 15.75 15.7812C16.0625 15.5 16.0625 15.0312 15.75 14.7188ZM1.5 6.5C1.5 3.75 3.71875 1.5 6.5 1.5C9.25 1.5 11.5 3.75 11.5 6.5C11.5 9.28125 9.25 11.5 6.5 11.5C3.71875 11.5 1.5 9.28125 1.5 6.5Z"
                              fill="#222222"/>
                    </svg>
                </label>
            </form>
        </div>
    </div>
    <div class="cards-filter">
        <div class="checkbox-wrapper">
            <h5 class="paragraph filter-title">
                <?= __( 'Filter Resources', 'eisgroup' ) ?>

                <svg class="show-in-mobile" width="15" height="8" viewBox="0 0 15 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M14.3875 1.69618L8.04196 7.66602C7.83323 7.87476 7.58274 8 7.33226 8C7.12353 8 6.87304 7.87476 6.66431 7.70777L0.318744 1.73793C-0.0987276 1.36221 -0.0987275 0.735998 0.276997 0.318527C0.652721 -0.0989449 1.27893 -0.0989449 1.6964 0.27678L7.37401 5.62041L13.0099 0.276781C13.4273 -0.0989438 14.0536 -0.0989438 14.4293 0.318528C14.805 0.694252 14.805 1.32046 14.3875 1.69618Z" fill="#222222"/>
                </svg>
            </h5>
            <div class="collapse-in-mobile">
                <h6 class="paragraph paragraph-s-paragraph filter-subtitle">
                    <?= __( 'Check multiple boxes below to narrow resources.', 'eisgroup' ) ?>
                </h6>
                <form>
                    <?php
                    $taxonomies = get_object_taxonomies( 'eis-resources', 'objects' );
                    foreach ( $taxonomies as $tax ) {
                        $types = get_terms( array(
                            'taxonomy'   => $tax->name,
                            'hide_empty' => true
                        ) );
                        if ( ! empty( $types ) && is_array( $types ) ) { ?>
                            <div class="category-wrapper" data-taxonomy="<?= $tax->name ?>">
                                <div class="form-title active-category-title">
                                    <?= $tax->label ?>
                                    <svg width="15" height="8" viewBox="0 0 15 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M14.3875 1.69618L8.04196 7.66602C7.83323 7.87476 7.58274 8 7.33226 8C7.12353 8 6.87304 7.87476 6.66431 7.70777L0.318744 1.73793C-0.0987276 1.36221 -0.0987275 0.735998 0.276997 0.318527C0.652721 -0.0989449 1.27893 -0.0989449 1.6964 0.27678L7.37401 5.62041L13.0099 0.276781C13.4273 -0.0989438 14.0536 -0.0989438 14.4293 0.318528C14.805 0.694252 14.805 1.32046 14.3875 1.69618Z" fill="#222222"/>
                                    </svg>
                                </div>
                                <div class="form-menu" style="height: auto">
                                    <?php foreach ( $types as $term ) { ?>
                                        <div class="option-wrapper">
                                            <input type="checkbox" class="option-input" name='<?= $tax->name ?>[]' id="<?= $term->slug ?>" value="<?= $term->slug ?>"/>
                                            <label for="<?= $term->slug ?>"><?= $term->name; ?></label>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php }
                    }
                    ?>
                </form>
            </div>
        </div>
        <div class="posts">
            <div posts-container>
                <?php if ( $the_query->have_posts() ): while ( $the_query->have_posts() ):$the_query->the_post();
                    $post_id   = get_the_id();
                    $args_card = array(
                        'post_id' => $post_id,
                    );
                    ?>
                    <?= get_template_part( 'template-loops/resource-card-block', '', $args_card ) ?>
                <?php endwhile;endif; ?>
                <?php $args['paged']  = $args['paged'] + 1;
                $args['post__not_in'] = null;
                ?>
            </div>
            <div class="no-posts headline-2 <?= $have_posts ? '' : 'active' ?>">
                <?= __( 'No Posts Here', 'eisgroup' ) ?>
            </div>
            <div class="load-more-wrapper <?= $the_query->max_num_pages <= 1 ? "hidden" : "" ?>"
                 data-args='<?= json_encode( $args ) ?>'
                 data-template="template-loops/resource-card-block"
            >
                <div class="apply-btn  load-more-btn">
                    <?= __( 'load more', 'eisgroup' ) ?>
                </div>
                <div class="loader">
                    <div class="loader-ball"></div>
                    <div class="loader-ball"></div>
                    <div class="loader-ball"></div>
                </div>
            </div>
        </div>
    </div>
    <!--      <div class="pagination-numbers">-->
    <!--        <div class="left-arrow">-->
    <!--          <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">-->
    <!--            <path opacity="0.2" d="M17.25 11.25H8.53125L11.7656 8.0625C12.0469 7.78125 12.0469 7.26562 11.7656 6.98438C11.4844 6.70312 10.9688 6.70312 10.6875 6.98438L6.1875 11.4844C6.04688 11.625 6 11.8125 6 12C6 12.2344 6.04688 12.4219 6.1875 12.5625L10.6875 17.0625C10.9688 17.3438 11.4844 17.3438 11.7656 17.0625C12.0469 16.7812 12.0469 16.2656 11.7656 15.9844L8.53125 12.75H17.25C17.625 12.75 18 12.4219 18 12C18 11.625 17.625 11.25 17.25 11.25ZM12 0C5.34375 0 0 5.39062 0 12C0 18.6562 5.34375 24 12 24C18.6094 24 24 18.6562 24 12C24 5.39062 18.6094 0 12 0ZM12 22.5C6.1875 22.5 1.5 17.8125 1.5 12C1.5 6.23438 6.1875 1.5 12 1.5C17.7656 1.5 22.5 6.23438 22.5 12C22.5 17.8125 17.7656 22.5 12 22.5Z" fill="#33657E"/>-->
    <!--          </svg>-->
    <!--        </div>-->
    <!--        -->
    <!---->
    <!--        <div class="numbers">-->
    <!--          <span class="number">-->
    <!--            1-->
    <!--          </span>-->
    <!--          <span class="number">-->
    <!--            1-->
    <!--          </span>-->
    <!--          <span class="number">-->
    <!--            1-->
    <!--          </span>-->
    <!--          <span class="number">-->
    <!--            1-->
    <!--          </span>-->
    <!--          <span class="number">-->
    <!--          ........-->
    <!--          </span>-->
    <!--          -->
    <!--        </div>-->
    <!--        <div class="right-arrow">-->
    <!--          <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">-->
    <!--            <path d="M13.2656 6.98438C12.9844 6.70312 12.4688 6.70312 12.1875 6.98438C11.9062 7.26562 11.9062 7.78125 12.1875 8.0625L15.4219 11.25H6.75C6.32812 11.25 6 11.625 6 12C6 12.4219 6.32812 12.75 6.75 12.75H15.4219L12.1875 15.9844C11.9062 16.2656 11.9062 16.7812 12.1875 17.0625C12.4688 17.3438 12.9844 17.3438 13.2656 17.0625L17.7656 12.5625C17.9062 12.4219 18 12.2344 18 12C18 11.8125 17.9062 11.625 17.7656 11.4844L13.2656 6.98438ZM12 0C5.34375 0 0 5.39062 0 12C0 18.6562 5.34375 24 12 24C18.6094 24 24 18.6562 24 12C24 5.39062 18.6094 0 12 0ZM12 22.5C6.1875 22.5 1.5 17.8125 1.5 12C1.5 6.23438 6.1875 1.5 12 1.5C17.7656 1.5 22.5 6.23438 22.5 12C22.5 17.8125 17.7656 22.5 12 22.5Z" fill="#33657E"/>-->
    <!--          </svg>-->
    <!---->
    <!--        </div>-->
    <!---->
    <!--      </div>-->
</div>
</section>


<!-- endregion dh-updated-theme's Block -->

import './index.html';
import './style.scss';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {ScrollTrigger} from "gsap/ScrollTrigger";
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/propellerhealth';
import {stringToHTML} from "../../../scripts/functions/stringToHTML";


gsap.registerPlugin(ScrollTrigger)

const blockScript = async (container = document) => {
  const block = container.querySelector('.eis_resources_block');
  const initDateToPost = (post) => {
    if (!post.classList.contains("has-event")) return null;
    const postDate = new Date(post.querySelector(".event-date").dataset.startDate);
    post.querySelector(".date-day").textContent = postDate.toLocaleDateString([], {
      day: 'numeric',
    });
    post.querySelector(".date-month").textContent = postDate.toLocaleDateString([], {
      month: 'long',
    });
  }

  const addButtonToCard = (post) => {
    const termsWrapper = post.querySelector(".info-wrapper");
    const plusIcon = post.querySelector(".info-wrapper .icon");
    const wrapperScrollHeight = termsWrapper.scrollHeight;
    //height of one line = 48
    if (wrapperScrollHeight > 48) {
      plusIcon.classList.add("active");
      plusIcon.addEventListener("click", () => {
        //margin bottom for wrapper = 17
        termsWrapper.style.height = `${wrapperScrollHeight + 17}px`;
        plusIcon.classList.remove("active");
      })
    }
  }

  block.querySelectorAll(".has-event").forEach((post) => {
    initDateToPost(post);
  })

  block.querySelectorAll(".resource_card_block").forEach((post) => {
    addButtonToCard(post)
  })

  const players = {};
  const resourceCards = block.querySelectorAll(".resource_card_block");
  const mobileCollapse = block.querySelector(".filter-title")
  const mobileMenu = block.querySelector(".collapse-in-mobile")
  const searchPart = block.querySelector(".search-part")

  const categories = block.querySelectorAll('.category-wrapper');

  categories.forEach((category) => {
    const catTitle = category.querySelector('.form-title');
    const catBody = category.querySelector('.form-menu');

    catTitle?.addEventListener('click', () => {
      const isOpened = catTitle?.classList.toggle('active-category-title');
      if (!isOpened) {
        gsap.to(catBody, {height: 0, ease: "linear"});
      } else {
        gsap.to(catBody, {height: 'auto', ease: "linear"});
      }
    });
  });

  ScrollTrigger.matchMedia({
    "(max-width: 600px)": function () {
      mobileCollapse?.addEventListener("click", () => {
        const isOpenedMenu = mobileCollapse?.classList.toggle('mobile-collapse');

        if (!isOpenedMenu) {
          gsap.to(mobileMenu, {height: 0, ease: "linear"});
          gsap.to(searchPart, {height: 'auto'});
        } else {
          gsap.to(mobileMenu, {height: 'auto', ease: "linear"});
          gsap.to(searchPart, {height: 0});
        }
      })
    }
  });


  const postsContainer = block.querySelector('[posts-container]');
  const loadMore = block.querySelector('.load-more-wrapper');
  const noPosts = block.querySelector('.no-posts');
  const initialLoadMoreArgs = loadMore && JSON.parse(loadMore.dataset.args);
  const initialHasMore = !loadMore?.classList.contains('hidden');
  const initialPostsString = postsContainer?.innerHTML;
  let loadMoreStatus = !loadMore.classList.contains('hidden');
  const filters = block.querySelectorAll(".category-wrapper");
  const searchInput = block.querySelector(".search-input");
  const sortSelect = block.querySelector(".sort-by-result");
  let isSearchedBefore = false;

  const getAllSelectsQuery = () => {
    const tax_query = {};
    let hasTax = false;
    filters.forEach((category, index) => {
      const values = []
      category.querySelectorAll(".option-input").forEach((option) => {
        if (option.checked) {
          hasTax = true;
          values.push(String(option.value));
        }
      });
      if (values.length) {
        tax_query[index] = {
          taxonomy: category.dataset.taxonomy,
          terms: values,
          field: 'slug',
          operator: 'IN'
        }
      }
    });
    hasTax && (tax_query.relation = 'AND');
    return hasTax ? tax_query : null;
  }
  const categoryFilterChangeHandler = () => {
    const tax_query = getAllSelectsQuery();
    loadMore.dataset.args = JSON.stringify({
      ...initialLoadMoreArgs,
      tax_query: tax_query || [],
      s: searchInput.value.trim(),
      orderby: {'date': sortSelect.value},
      paged: (!tax_query && !searchInput.value.trim() && sortSelect.value === "DESC") ? 2 : 1,
    });
    postsContainer.innerHTML = '';
    if (!tax_query && !searchInput.value.trim() && sortSelect.value === "DESC") {
      loadMore.classList.toggle('hidden', !initialHasMore);
      const posts = stringToHTML(initialPostsString);
      for (let post of posts) {
        postsContainer.appendChild(post);
        initDateToPost(post);
        addButtonToCard(post);
      }
      animations(posts);
      imageLazyLoading(posts);
      return;
    }

    loadMore.click();
  };
  if (loadMore) {
    loadMore.addEventListener('click', async () => {
      if (loadMore.classList.contains('loading')) return;
      loadMore.classList.add('loading');
      loadMore.classList.remove('hidden');
      noPosts.classList.remove('active');

      const response = await fetch(theme_ajax_object.ajax_url, {
        method: 'POST',
        body: requestBodyGenerator(loadMore),
      });
      loadMore.classList.remove('loading');
      const hasMorePages = !!response.headers.get('x-wp-has-more-pages');
      console.log("hasMorePages", response.headers.get('x-wp-has-more-pages'));
      const totalPages = +response.headers.get('x-wp-total-pages');
      console.log("totalPages", response.headers.get('x-wp-total-pages'));
      console.log("response.headers", response.headers)
      noPosts.classList.toggle('active', totalPages === 0);
      loadMore.classList.toggle('hidden', !hasMorePages);
      loadMoreStatus = hasMorePages;
      const htmlString = await response.json();
      const oldArgs = JSON.parse(loadMore.dataset.args);
      oldArgs.paged++;
      loadMore.dataset.args = JSON.stringify(oldArgs);
      const posts = stringToHTML(htmlString.data);
      for (let post of posts) {
        postsContainer.appendChild(post);
        initDateToPost(post);
        addButtonToCard(post);
      }
      animations(posts);
      imageLazyLoading(posts);
    })
  }

  categoryFilterChangeHandler();

  block.querySelectorAll(".option-input").forEach((option) => {
    option.addEventListener("change", categoryFilterChangeHandler);
  });

  sortSelect.addEventListener("change", categoryFilterChangeHandler);

  const handleSearch = () => {
    if (!loadMore.classList.contains('loading')) {
      if (searchInput.value) {
        isSearchedBefore = true;
        categoryFilterChangeHandler();
      } else if (!searchInput.value && isSearchedBefore) {
        categoryFilterChangeHandler();
        isSearchedBefore = false;
      }
    }
  }

  block.querySelector(".search-wrapper").addEventListener("submit", (e) => {
    e.preventDefault();
    handleSearch();
  });

  searchInput.addEventListener("blur", () => {
    handleSearch();
  });
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);

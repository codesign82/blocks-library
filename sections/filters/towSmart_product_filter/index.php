<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'product_filter';
$className = 'product_filter';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/product_filter/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region TowSmart's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="filter">

    <div class="filter-title headline-5 ">
      Towing Accessories | Ball Mount Bars (10)
    </div>
    <div class="btn btn-filter">Filter</div>
    <div class="sidebar">
      <div class="header">
        <div class="header-nav">
          <svg class="close" width="12" height="12" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M13.4099 12L17.7099 7.71C17.8982 7.5217 18.004 7.2663 18.004 7C18.004 6.7337 17.8982 6.47831 17.7099 6.29C17.5216 6.1017 17.2662 5.99591 16.9999 5.99591C16.7336 5.99591 16.4782 6.1017 16.2899 6.29L11.9999 10.59L7.70994 6.29C7.52164 6.1017 7.26624 5.99591 6.99994 5.99591C6.73364 5.99591 6.47824 6.1017 6.28994 6.29C6.10164 6.47831 5.99585 6.7337 5.99585 7C5.99585 7.2663 6.10164 7.5217 6.28994 7.71L10.5899 12L6.28994 16.29C6.19621 16.383 6.12182 16.4936 6.07105 16.6154C6.02028 16.7373 5.99414 16.868 5.99414 17C5.99414 17.132 6.02028 17.2627 6.07105 17.3846C6.12182 17.5064 6.19621 17.617 6.28994 17.71C6.3829 17.8037 6.4935 17.8781 6.61536 17.9289C6.73722 17.9797 6.86793 18.0058 6.99994 18.0058C7.13195 18.0058 7.26266 17.9797 7.38452 17.9289C7.50638 17.8781 7.61698 17.8037 7.70994 17.71L11.9999 13.41L16.2899 17.71C16.3829 17.8037 16.4935 17.8781 16.6154 17.9289C16.7372 17.9797 16.8679 18.0058 16.9999 18.0058C17.132 18.0058 17.2627 17.9797 17.3845 17.9289C17.5064 17.8781 17.617 17.8037 17.7099 17.71C17.8037 17.617 17.8781 17.5064 17.9288 17.3846C17.9796 17.2627 18.0057 17.132 18.0057 17C18.0057 16.868 17.9796 16.7373 17.9288 16.6154C17.8781 16.4936 17.8037 16.383 17.7099 16.29L13.4099 12Z" fill="white"/>
          </svg>
          <div class="head-filter">Filter</div>
          <div class="clear">Clear</div>
        </div>
      </div>
      <ul class="all-product menu-item">
        <div class="head">
          <div class="head-5">Cateory</div>
          <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M17.0002 13.5C17.0009 13.6316 16.9757 13.7621 16.9259 13.8839C16.8762 14.0057 16.8029 14.1166 16.7102 14.21C16.6172 14.3037 16.5066 14.3781 16.3848 14.4289C16.2629 14.4797 16.1322 14.5058 16.0002 14.5058C15.8682 14.5058 15.7375 14.4797 15.6156 14.4289C15.4937 14.3781 15.3831 14.3037 15.2902 14.21L12.0002 10.9L8.69017 14.08C8.50281 14.2663 8.24936 14.3708 7.98517 14.3708C7.72099 14.3708 7.46754 14.2663 7.28017 14.08C7.18644 13.987 7.11205 13.8764 7.06128 13.7546C7.01051 13.6327 6.98438 13.502 6.98438 13.37C6.98438 13.238 7.01051 13.1073 7.06128 12.9854C7.11205 12.8636 7.18644 12.753 7.28017 12.66L11.2802 8.8C11.4671 8.61677 11.7184 8.51414 11.9802 8.51414C12.2419 8.51414 12.4932 8.61677 12.6802 8.8L16.6802 12.8C16.7771 12.8898 16.8553 12.9978 16.9102 13.118C16.9651 13.2381 16.9957 13.368 17.0002 13.5Z" fill="white"/>
          </svg>
        </div>
        <ul class="sub-menu">
          <li class="spacer"></li>
          <li class="menu">
            <a href="#" class="paragraph paragraph-14 title">All Products</a>
            <ul class="sub-menu1">
              <li class="spacer"></li>
              <li class="paragraph paragraph-14 item">Ball Mount Bars (10)</li>
              <li class="paragraph paragraph-14 item">Hitch Balls (13)</li>
              <li class="paragraph paragraph-14 item">Receiver Pins and Locks (23)</li>
              <li class="paragraph paragraph-14 item">Heavy Duty Accessories (11)</li>
              <li class="paragraph paragraph-14 item">Utility and Convenience Accessories (10)</li>
            </ul>
          </li>
          <li class="menu menu-item-has-children">
            <a href="#" class="paragraph paragraph-14 title">Custom Hitches</a>
            <ul class="sub-menu1">
              <li class="spacer"></li>
              <li class="paragraph paragraph-14 item">Ball Mount Bars (10)</li>
              <li class="paragraph paragraph-14 item">Hitch Balls (13)</li>
              <li class="paragraph paragraph-14 item">Receiver Pins and Locks (23)</li>
              <li class="paragraph paragraph-14 item">Heavy Duty Accessories (11)</li>
              <li class="paragraph paragraph-14 item">Utility and Convenience Accessories (10)</li>
            </ul>
          </li>

          <li class="menu menu-item-has-children">
            <a href="#" class="paragraph paragraph-14 title">Towing Accessories (48)</a>
            <ul class="sub-menu1">
              <li class="spacer"></li>
              <li class="paragraph paragraph-14 item">Ball Mount Bars (10)</li>
              <li class="paragraph paragraph-14 item">Hitch Balls (13)</li>
              <li class="paragraph paragraph-14 item">Receiver Pins and Locks (23)</li>
              <li class="paragraph paragraph-14 item">Heavy Duty Accessories (11)</li>
              <li class="paragraph paragraph-14 item">Utility and Convenience Accessories (10)</li>
            </ul>
          </li>
          <li class="menu menu-item-has-children">
            <a href="#" class="paragraph paragraph-14 title">Trailer Accessories (17)</a>
            <ul class="sub-menu1">
              <li class="spacer"></li>
              <li class="paragraph paragraph-14 item">Ball Mount Bars (10)</li>
              <li class="paragraph paragraph-14 item">Hitch Balls (13)</li>
              <li class="paragraph paragraph-14 item">Receiver Pins and Locks (23)</li>
              <li class="paragraph paragraph-14 item">Heavy Duty Accessories (11)</li>
              <li class="paragraph paragraph-14 item">Utility and Convenience Accessories (10)</li>
            </ul>
          </li>
        </ul>
      </ul>
      <ul class="product-class menu-item">
        <div class="head">
          <div class="head-5">Product Class</div>
          <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M17.0002 13.5C17.0009 13.6316 16.9757 13.7621 16.9259 13.8839C16.8762 14.0057 16.8029 14.1166 16.7102 14.21C16.6172 14.3037 16.5066 14.3781 16.3848 14.4289C16.2629 14.4797 16.1322 14.5058 16.0002 14.5058C15.8682 14.5058 15.7375 14.4797 15.6156 14.4289C15.4937 14.3781 15.3831 14.3037 15.2902 14.21L12.0002 10.9L8.69017 14.08C8.50281 14.2663 8.24936 14.3708 7.98517 14.3708C7.72099 14.3708 7.46754 14.2663 7.28017 14.08C7.18644 13.987 7.11205 13.8764 7.06128 13.7546C7.01051 13.6327 6.98438 13.502 6.98438 13.37C6.98438 13.238 7.01051 13.1073 7.06128 12.9854C7.11205 12.8636 7.18644 12.753 7.28017 12.66L11.2802 8.8C11.4671 8.61677 11.7184 8.51414 11.9802 8.51414C12.2419 8.51414 12.4932 8.61677 12.6802 8.8L16.6802 12.8C16.7771 12.8898 16.8553 12.9978 16.9102 13.118C16.9651 13.2381 16.9957 13.368 17.0002 13.5Z" fill="white"/>
          </svg>
        </div>

        <ul class="sub-menu">
          <div class="product-class-title">
            <div class="paragraph paragraph-14 par-14">Filter by Product Class</div>
            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M8.00001 1.3335C6.68147 1.3335 5.39254 1.72449 4.29621 2.45703C3.19988 3.18957 2.3454 4.23077 1.84082 5.44894C1.33623 6.66711 1.20421 8.00756 1.46144 9.30077C1.71868 10.594 2.35362 11.7819 3.28597 12.7142C4.21832 13.6466 5.4062 14.2815 6.69941 14.5387C7.99262 14.796 9.33306 14.6639 10.5512 14.1594C11.7694 13.6548 12.8106 12.8003 13.5431 11.704C14.2757 10.6076 14.6667 9.31871 14.6667 8.00016C14.6667 7.12468 14.4942 6.25778 14.1592 5.44894C13.8242 4.6401 13.3331 3.90517 12.7141 3.28612C12.095 2.66706 11.3601 2.176 10.5512 1.84097C9.7424 1.50593 8.87549 1.3335 8.00001 1.3335ZM8.66668 10.6668C8.66668 10.8436 8.59644 11.0132 8.47142 11.1382C8.34639 11.2633 8.17682 11.3335 8.00001 11.3335C7.8232 11.3335 7.65363 11.2633 7.52861 11.1382C7.40358 11.0132 7.33335 10.8436 7.33335 10.6668V7.3335C7.33335 7.15669 7.40358 6.98712 7.52861 6.86209C7.65363 6.73707 7.8232 6.66683 8.00001 6.66683C8.17682 6.66683 8.34639 6.73707 8.47142 6.86209C8.59644 6.98712 8.66668 7.15669 8.66668 7.3335V10.6668ZM8.00001 6.00016C7.86816 6.00016 7.73927 5.96106 7.62963 5.88781C7.52 5.81456 7.43455 5.71044 7.38409 5.58862C7.33363 5.4668 7.32043 5.33276 7.34616 5.20344C7.37188 5.07412 7.43537 4.95533 7.52861 4.86209C7.62184 4.76886 7.74063 4.70536 7.86995 4.67964C7.99927 4.65392 8.13332 4.66712 8.25514 4.71758C8.37695 4.76803 8.48107 4.85348 8.55433 4.96312C8.62758 5.07275 8.66668 5.20164 8.66668 5.3335C8.66668 5.51031 8.59644 5.67988 8.47142 5.8049C8.34639 5.92992 8.17682 6.00016 8.00001 6.00016Z" fill="white" fill-opacity="0.5"/>
            </svg>
          </div>
          <li>
            <label class="product-classes">
              <input type="checkbox">
              <span class="design-checkbox"></span>
              <span class="paragraph category-text">Class I  (12)</span>
            </label>

          </li>

          <li>
            <label class="product-classes">
              <input type="checkbox">
              <span class="design-checkbox"></span>
              <span class="paragraph category-text">Class II  (13)</span>
            </label>
          </li>

          <li>

            <label class="product-classes">
              <input type="checkbox">
              <span class="design-checkbox"></span>
              <span class="paragraph category-text">Class III  (16)</span>
            </label>
          </li>

          <li>
            <label class="product-classes">
              <input type="checkbox">
              <span class="design-checkbox"></span>
              <span class="paragraph category-text">Class IV  (24)</span>
            </label>
          </li>

          <li>
            <label class="product-classes">
              <input type="checkbox">
              <span class="design-checkbox"></span>
              <span class="paragraph category-text">Class V  (11)</span>
            </label>
          </li>

          <li>
            <label class="product-classes">
              <input type="checkbox">
              <span class="design-checkbox"></span>
              <span class="paragraph category-text">Unspecified  (17)</span>
            </label>

          </li>
        </ul>
      </ul>
      <ul class="retailer menu-item">
        <div class="head">
          <div class="head-5">Retailer</div>
          <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M17.0002 13.5C17.0009 13.6316 16.9757 13.7621 16.9259 13.8839C16.8762 14.0057 16.8029 14.1166 16.7102 14.21C16.6172 14.3037 16.5066 14.3781 16.3848 14.4289C16.2629 14.4797 16.1322 14.5058 16.0002 14.5058C15.8682 14.5058 15.7375 14.4797 15.6156 14.4289C15.4937 14.3781 15.3831 14.3037 15.2902 14.21L12.0002 10.9L8.69017 14.08C8.50281 14.2663 8.24936 14.3708 7.98517 14.3708C7.72099 14.3708 7.46754 14.2663 7.28017 14.08C7.18644 13.987 7.11205 13.8764 7.06128 13.7546C7.01051 13.6327 6.98438 13.502 6.98438 13.37C6.98438 13.238 7.01051 13.1073 7.06128 12.9854C7.11205 12.8636 7.18644 12.753 7.28017 12.66L11.2802 8.8C11.4671 8.61677 11.7184 8.51414 11.9802 8.51414C12.2419 8.51414 12.4932 8.61677 12.6802 8.8L16.6802 12.8C16.7771 12.8898 16.8553 12.9978 16.9102 13.118C16.9651 13.2381 16.9957 13.368 17.0002 13.5Z" fill="white"/>
          </svg>
        </div>

        <ul class="sub-menu">
          <div class="retailer-title">
            <div class="paragraph paragraph-14 par-14">Filter by Retailer</div>
          </div>
          <li>
            <label class="product-classes">
              <input type="checkbox">
              <span class="design-checkbox"></span>
              <span class="paragraph category-text">AutoZone (11)</span>
            </label>

          </li>
          <li>
            <label class="product-classes">
              <input type="checkbox">
              <span class="design-checkbox"></span>
              <span class="paragraph category-text">The Home Depot (24)</span>

            </label>
          </li>

          <li>
            <label class="product-classes">
              <input type="checkbox">
              <span class="design-checkbox"></span>
              <span class="paragraph category-text">Gander Mountain  (16)</span>

            </label >
          </li>

          <li>
            <label class="product-classes">
              <input type="checkbox">
              <span class="design-checkbox"></span>
              <span class="paragraph category-text">Meijer  (14)</span>
            </label>
          </li>

          <li>
            <label class="product-classes">
              <input type="checkbox">
              <span class="design-checkbox"></span>
              <span class="paragraph category-text">Menards  (27)</span>
            </label>
          </li>
        </ul>
      </ul>
      <a href="#" class="btn btn-white btn-see-products">See products (10)</a>
    </div>
  </div>
</div>
</section>


<!-- endregion TowSmart's Block -->

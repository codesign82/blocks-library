import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/towSmart';

const blockScript = async (container = document) => {
  const block = container.querySelector('.product_filter');
  
  // add block code here
  // region open sub menu1 in responsive
  const topLinks = block.querySelectorAll('.menu-item-has-children');
  topLinks.forEach((menuItem) => {
    const menuItemBody = menuItem.querySelector('.sub-menu1');
    menuItem?.addEventListener('click', (e) => {
      if (!menuItemBody || e.target.closest('.sub-menu1')) return;
      const isOpened = menuItem?.classList.toggle('active-state');
      if (!isOpened) {
        gsap.to(menuItemBody, {height: 0});
      } else {
        gsap.to(Array.from(topLinks).map(otherMenuItem => {
          const otherMenuItemBody = otherMenuItem.querySelector('.sub-menu1');
          const link = otherMenuItem.querySelector('.title');
          if (otherMenuItemBody && menuItem !== otherMenuItem) {
            otherMenuItem?.classList.remove('active-state');
            gsap.set(otherMenuItem, {zIndex: 1});
          }
          return otherMenuItemBody;
        }), {height: 0});
        gsap.set(menuItem, {zIndex: 2});
        gsap.to(menuItemBody, {height: 'auto'});
      }
    });
  });
// endregion open sub menu1 in responsive
  const sub_links = block.querySelectorAll('.filter-category');
  const sub_links_parent = block.querySelectorAll('.menu.filter-category');
  const sub_links_children = block.querySelectorAll('.item.filter-subcategory');
  const subMenus = block.querySelectorAll('.menu-item-has-children .sub-menu1');
  const shopPageTitle = document.querySelector('.shop-page-active-category-title');
  const shopPageTitleText = shopPageTitle.querySelector('.active-text');
  const shopPageTitleNumber = shopPageTitle.querySelector('.span-number');
  for (let subLink of sub_links) {
    subLink.addEventListener('click', function (e) {
      e.stopPropagation();
      // sidebar_popup.classList.remove('sidebar-active');
      let categoryName = subLink.querySelector('.category-filter-label .category-name-text').textContent;
      // let categoryProductNumber = subLink.querySelector('.category-filter-label .category-name-text').dataset.productsCount;
      shopPageTitleText.textContent = categoryName;
      shopPageTitleNumber.textContent = '( ' + 0 + ' )';
      const isSubCategory = subLink.classList.contains('filter-subcategory') ? sub_links_children : sub_links_parent;
      for (let i = 0; i < isSubCategory.length; i++) {
        isSubCategory[i].classList.remove('active');
      }
      if (subLink.classList.contains('filter-subcategory')) {
        let parentCategory = subLink.closest('.menu.filter-category');
        parentCategory.classList.add('active');
        let parentCategoryName = parentCategory.querySelector('.category-filter-label .category-name-text').textContent;
        shopPageTitleText.textContent = parentCategoryName + ' | ' + categoryName;
      }
      if (!subLink.classList.contains('menu-item-has-children') && !subLink.classList.contains('filter-subcategory')) {
        gsap.to(subMenus, {height: 0});
        topLinks.forEach((elm) => {
          elm.classList.remove('active-state');
        })
      }
      subLink.classList.add('active');
    })
  }
  
  // region open sub menu in responsive
  const filter_links = block.querySelectorAll('.menu-item');
  const mobileMedia = window.matchMedia('(max-width: 992px)');
  filter_links.forEach((menuItem) => {
    const menuItemBody = menuItem.querySelector('.sub-menu');
    menuItem?.addEventListener('click', (e) => {
      if (!menuItemBody || !mobileMedia.matches || e.target.closest('.sub-menu')) return;
      const isOpened = menuItem?.classList.toggle('active-link');
      if (!isOpened) {
        gsap.to(menuItemBody, {height: 0});
      } else {
        gsap.to(Array.from(filter_links).map(otherMenuItem => {
          const otherMenuItemBody = otherMenuItem.querySelector('.sub-menu');
          if (otherMenuItemBody && menuItem !== otherMenuItem) {
            otherMenuItem?.classList.remove('active-link');
            gsap.set(otherMenuItem, {zIndex: 1});
          }
          return otherMenuItemBody;
        }), {height: 0});
        gsap.set(menuItem, {zIndex: 2});
        gsap.to(menuItemBody, {height: 'auto'});
      }
    });
  });
// endregion open sub menu in responsive
  
  
  const btnShowFilter = document.querySelector('.btn-show-filter');
  const sidebar_popup = block.querySelector('.sidebar')
  btnShowFilter?.addEventListener('click', function () {
    sidebar_popup.classList.add('sidebar-active');
    document.documentElement.classList.add('modal-opened');
  });
  
  const closeFilter = () => {
    sidebar_popup.classList.remove('sidebar-active');
    document.documentElement.classList.remove('modal-opened');
  }
  
  const close = block.querySelector('.close');
  close.addEventListener('click', closeFilter);
  
  const seeProducts = block.querySelector('.btn-see-products');
  seeProducts.addEventListener('click', closeFilter);
  
  const clearFilter = block.querySelector('.clear-filter');
  const sidebar = block.querySelector('.sidebar');
  clearFilter.addEventListener('click', () => {
    const activeFilters = sidebar.querySelectorAll('.active');
    if (activeFilters.length > 0) {
      for (let activeFilter of activeFilters) {
        activeFilter.classList.remove('active');
      }
    }
    const activeSubMenu = sidebar.querySelector('.active-link .sub-menu');
    const activeSubMenu1 = sidebar.querySelector('.active-state .sub-menu1');
    if (activeSubMenu) {
      gsap.to(activeSubMenu, {
        height: 0
      })
      sidebar.querySelector('.active-link')?.classList.remove('active-link');
    }
    if (activeSubMenu1) {
      gsap.to(activeSubMenu1, {
        height: 0
      })
      sidebar.querySelector('.active-state')?.classList.remove('active-state');
    }
    // region clear button functionality
    for (const filterButton of document.querySelectorAll('.product-filter-button')) {
      filterButton.checked = false;
    }
    document.querySelector('[data-taxonomy-name="all-products"]').click();
    // endregion clear button functionality
  })
  
  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);




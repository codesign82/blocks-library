<?php
// Create id attribute allowing for custom "anchor" value.
if (!is_admin()) {
    $id = $block['id'];
    if (!empty($block['anchor'])) {
        $id = $block['anchor'];
    }

// Create class attribute allowing for custom "className" and "align" values.
    $dataClass = 'recruiting_wire_block';
    $className = 'recruiting_wire_block';
    if (!empty($block['className'])) {
        $className .= ' ' . $block['className'];
    }
    if (!empty($block['align'])) {
        $className .= ' align' . $block['align'];
    }
    if (get_field('is_screenshot')) :
        /* Render screenshot for example */
        echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/recruiting_wire_block/screenshot.png" >';

        return;
    endif;

    /****************************
     *     Custom ACF Meta      *
     ****************************/


    $has_container = get_field('has_container');
    $has_container = $has_container ? 'container' : '';
    $main_title = get_field('main_title');
    $left_title = get_field('left_title');
    $right_title = get_field('right_title');

    $left_group = get_field('left_group');
    $right_group = get_field('right_group');
    $left_section = get_field('left_section');
    $right_section = get_field('right_section');
    $left_number_of_posts = get_field('left_number_of_posts');
    $right_number_of_posts = get_field('right_number_of_posts');

    $has_border = get_field('has_border');
    ?>
    <!-- region advisorhub's Block -->
    <?php general_settings_for_blocks($id, $className, $dataClass); ?>
    <div class="<?= $has_container ?>">
        <div class="recruiting-wire">
            <?php if ($main_title) { ?>
                <div class="main-title headline-1"><?= $main_title; ?></div>
            <?php } ?>
            <div class="block-wrapper <?= $has_border ? 'has-border' : ''?>">
                <div class="winner items-wrapper">
                    <div class="headline-4 winner-title"><?= $left_group['left_title']; ?></div>
                    <?php
                    $types = ($left_group['left_section'] === 'win' ? 'win' : 'loss');
                    echo do_shortcode('[recruiting_write type=' . $types . ' max=' . $left_group['left_number_of_posts'] . ']') ?>
                </div>
                <div class="line"></div>
                <div class="loser items-wrapper">
                    <div class="headline-4 winner-title"><?= $right_group['right_title']; ?></div>
                    <?php
                    $types = ($right_group['right_section'] === 'win' ? 'win' : 'loss');
                    echo do_shortcode('[recruiting_write type=' . $types . ' max= ' . $right_group['right_number_of_posts'] . ']') ?>


                </div>
            </div>
        </div>
    </div>
    </section>

    <!-- endregion advisorhub's Block -->
<?php } ?>

<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'cta_columns';
$className = 'cta_columns';
$enable_dots_background = get_field('enable_dots_background');
$versions = get_field('versions');
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if ($versions) {
    $className .= " version-4";
}
if (!$enable_dots_background) {
    $className .= '  gery-bg';
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/cta_columns/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$description = get_field('description');
$image = get_field('image');
$left_title = get_field('left_title');
$right_title = get_field('right_title');
$right_description = get_field('right_description');
$left_description = get_field('left_description');
$phone = get_field('phone', 'options');
$hours_of_operation = get_field('hours_of_operation', 'options');
$email = get_field('email', 'options');
$contact_us = get_field('cta_link', 'options');
$left_title_large_or_small = get_field('left_title_large_or_small');

?>
<!-- region propellerhealth Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<?php if ($enable_dots_background) { ?>
    <picture class="dots-bg cover-background"></picture>
<?php } ?>
<div class="container <?= $versions === 'version-2' ? 'version-2' : '' ?>">
    <div class="columns <?= $versions === 'version-2' ? 'has-img' : '' ?>">
        <?php if ($versions === 'version-1') { ?>
            <div class='left-column'>
                <div class="left-content iv-st-from-bottom">
                    <?php if ($left_title) { ?>
                        <?php $left_title_large_or_small = $left_title_large_or_small ? ' headline-3 ' : ' headline-1 ' ?>
                        <div
                                class="<?= $left_title_large_or_small ?> left-title f-68 wysiwyg-block"><?= $left_title ?></div>
                    <?php } ?>
                    <?php if ($left_description) { ?>
                        <div class="paragraph left-text wysiwyg-block"><?= $left_description ?></div>
                    <?php } ?>
                    <?php if ($phone || $email || $hours_of_operation) { ?>
                        <div class="left-info">
                            <div class="phone-and-time">
                                <?php if ($phone) { ?>
                                    <a href="tel:<?= $phone ?>"
                                       class="paragraph phone"><?= $phone ?></a>
                                <?php } ?>
                                <?php if ($hours_of_operation) { ?>
                                    <div class="paragraph time"><?= $hours_of_operation ?></div>
                                <?php } ?>
                            </div>
                            <?php if ($email) { ?>
                                <a href="mailto:<?= $email ?>"
                                   class="paragraph email "><?= $email ?></a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php if ($contact_us) { ?>
                        <a href="<?= $contact_us['url'] ?>"
                           target="<?= $contact_us['target'] ?>" class="cta-link">
                            <?= $contact_us['title'] ?>
                            <svg class="arrow-right" viewBox="243.81 243.737 15.017 12.535"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path class="arrow-head"
                                      d="M 251.977 256.242 L 257.977 250.742 L 258.781 250.005 L 257.977 249.268 L 251.977 243.768 L 250.625 245.242 L 255.821 250.005 L 250.625 254.768 L 251.977 256.242 Z"
                                      style="fill: rgb(0, 117, 227);"></path>
                                <path class="arrow-line"
                                      d="M 243.857 249.004 L 256.857 249.004 L 256.857 251.004 L 243.857 251.004 L 243.857 249.004 Z"
                                      style="fill: rgb(0, 117, 227);"></path>
                            </svg>
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class='right-column'>
                <?php if ($right_title) { ?>
                    <div
                            class="headline-3 right-title iv-st-from-bottom"><?= $right_title ?></div>
                <?php } ?>
                <?php if (have_rows('button_repeater')) { ?>
                    <?php while (have_rows('button_repeater')) {
                        the_row();
                        $audience = get_sub_field('audience');
                        $button_description = get_sub_field('button_description');
                        $link = get_sub_field('link');
                        ?>
                        <?php if ($link) { ?>
                            <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>" role="button" class="cta-button iv-st-from-bottom">
              <span class="text-and-link ">
                <?php if ($audience) { ?>
                    <div class="headline-3 btn-audience"> <?= $audience ?> </div>
                <?php } ?>

                  <span
                          class="cta-link"> <?= $link['title'] ?>
                    <svg class="arrow-right" viewBox="243.81 243.737 15.017 12.535" xmlns="http://www.w3.org/2000/svg">
                      <path class="arrow-head" d="M 251.977 256.242 L 257.977 250.742 L 258.781 250.005 L 257.977 249.268 L 251.977 243.768 L 250.625 245.242 L 255.821 250.005 L 250.625 254.768 L 251.977 256.242 Z" style="fill: #C6DFF8;"></path>
                      <path class="arrow-line" d="M 243.857 249.004 L 256.857 249.004 L 256.857 251.004 L 243.857 251.004 L 243.857 249.004 Z" style="fill: #C6DFF8;"></path>
                    </svg>
                  </span>

              </span>
                                <?php if ($button_description) { ?>
                                    <span class="paragraph btn-description "
                                          role="presentation"><?= $button_description ?></span>
                                <?php } ?>
                            </a>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
        <?php } ?>

        <?php if ($versions === 'version-2') { ?>
            <div class='left-column '>
                <?php if ($image) { ?>
                    <picture class="aspect-ratio iv-st-from-bottom">
                        <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>"/>
                    </picture>
                <?php } ?>

            </div>
            <div class='right-column'>
                <?php if ($right_title) { ?>
                    <div
                            class="headline-3 right-title iv-st-from-bottom"><?= $right_title ?></div>
                <?php } ?>
                <?php if ($right_description) { ?>
                    <div
                            class="right-description iv-st-from-bottom"><?= $right_description ?></div>
                <?php } ?>
                <?php if (have_rows('button_repeater')) { ?>
                    <?php while (have_rows('button_repeater')) {
                        the_row();
                        $audience = get_sub_field('audience');
                        $button_description = get_sub_field('button_description');
                        $link = get_sub_field('link');
                        ?>
                        <?php if ($link) { ?>
                            <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>" role="button" class="cta-button iv-st-from-bottom">
              <span class="text-and-link ">
                <?php if ($audience) { ?>
                    <div class="headline-3 btn-audience"> <?= $audience ?> </div>
                <?php } ?>

                  <span
                          class="cta-link"> <?= $link['title'] ?>
                    <svg class="arrow-right" viewBox="243.81 243.737 15.017 12.535" xmlns="http://www.w3.org/2000/svg">
                      <path class="arrow-head" d="M 251.977 256.242 L 257.977 250.742 L 258.781 250.005 L 257.977 249.268 L 251.977 243.768 L 250.625 245.242 L 255.821 250.005 L 250.625 254.768 L 251.977 256.242 Z" style="fill: #C6DFF8;"></path>
                      <path class="arrow-line" d="M 243.857 249.004 L 256.857 249.004 L 256.857 251.004 L 243.857 251.004 L 243.857 249.004 Z" style="fill: #C6DFF8;"></path>
                    </svg>
                  </span>

              </span>
                                <?php if ($button_description) { ?>
                                    <span class="paragraph btn-description "
                                          role="presentation"><?= $button_description ?></span>
                                <?php } ?>
                            </a>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
        <?php } ?>

        <?php if ($versions === 'version-3') { ?>
            <div class='left-column '>
                <div class='left-content iv-st-from-bottom'>
                    <?php if ($left_title) { ?>
                        <?php $left_title_large_or_small = $left_title_large_or_small ? ' headline-3 ' : ' headline-1 ' ?>
                        <div
                                class="<?= $left_title_large_or_small ?> left-title"><?= $left_title ?></div>
                    <?php } ?>
                    <?php if ($left_description) { ?>
                        <div class="paragraph left-text"><?= $left_description ?></div>
                    <?php } ?>
                </div>


            </div>
            <div class='right-column'>
                <?php if (have_rows('button_repeater')) { ?>
                    <?php while (have_rows('button_repeater')) {
                        the_row();
                        $audience = get_sub_field('audience');
                        $button_description = get_sub_field('button_description');
                        $link = get_sub_field('link');
                        ?>
                        <?php if ($link) { ?>
                            <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>" role="button" class="cta-button  iv-st-from-bottom">
              <span class="text-and-link ">
                <?php if ($audience) { ?>
                    <div class="headline-3 btn-audience"> <?= $audience ?> </div>
                <?php } ?>
                  <span
                          class="cta-link"> <?= $link['title'] ?>
                    <svg class="arrow-right" viewBox="243.81 243.737 15.017 12.535" xmlns="http://www.w3.org/2000/svg">
                      <path class="arrow-head" d="M 251.977 256.242 L 257.977 250.742 L 258.781 250.005 L 257.977 249.268 L 251.977 243.768 L 250.625 245.242 L 255.821 250.005 L 250.625 254.768 L 251.977 256.242 Z" style="fill: #C6DFF8;"></path>
                      <path class="arrow-line" d="M 243.857 249.004 L 256.857 249.004 L 256.857 251.004 L 243.857 251.004 L 243.857 249.004 Z" style="fill: #C6DFF8;"></path>
                    </svg>
                  </span>
              </span>
                                <?php if ($button_description) { ?>
                                    <span class="paragraph btn-description "
                                          role="presentation"><?= $button_description ?></span>
                                <?php } ?>
                            </a>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
        <?php } ?>

        <?php if ($versions === 'version-4') { ?>
            <div class='left-column '>
                <div class='left-content iv-st-from-bottom'>
                    <?php if ($left_title) { ?>
                        <?php $left_title_large_or_small = $left_title_large_or_small ? ' headline-3 ' : ' headline-1  f-68 ' ?>
                        <div
                                class="<?= $left_title_large_or_small ?> left-title "><?= $left_title ?></div>
                    <?php } ?>
                    <?php if ($left_description) { ?>
                        <div class="paragraph left-text"><?= $left_description ?></div>
                    <?php } ?>
                </div>


            </div>
            <div class='right-column'>
                <?php if (have_rows('button_repeater')) { ?>
                    <?php while (have_rows('button_repeater')) {
                        the_row();
                        $link = get_sub_field('link');
                        ?>
                        <a role="button" href="<?= $link['url'] ?>" target="<?= $link['target'] ?>"
                           class="cta-button iv-st-from-bottom large-width ">
                            <div class="text-and-link  ">
                                <?php if ($link) { ?>
                                    <div
                                            class="headline-3 btn-audience"> <?= $link['title'] ?> </div>
                                <?php } ?>
                            </div>
                        </a>
                    <?php } ?>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>
</section>


<!-- endregion propellerhealth Block -->

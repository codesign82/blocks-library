<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'open_positions_block';
$className = 'open_positions_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/open_positions_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/

?>
<!-- region propellerhealth Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">

    <?php if (have_rows('position')) { ?>
        <?php while (have_rows('position')) {
            the_row();
            $title = get_sub_field('title');
            $description = get_sub_field('description');
            $location = get_sub_field('location');
            $cta_link = get_sub_field('cta_link');
            ?>
            <div class="job-row iv-st-from-bottom">
                <div class="job_and_location ">
                    <?php if ($title) { ?>
                        <div class="title  headline-4 fw-extra-bold "><?= $title ?></div>
                    <?php } ?>
                    <?php if ($location) { ?>
                        <div class="location headline-5  fw-bold  "><?= $location ?></div>
                    <?php } ?>
                </div>
                <?php if ($description) { ?>
                    <div class="desc paragraph paragraph-18 "><?= $description ?></div>
                <?php } ?>
                <?php if ($cta_link) { ?>
                    <a role="button" aria-pressed="true"
                       target="<?= $cta_link['target'] ?>" href="<?= $cta_link['url'] ?>"
                       class="cta-link link "><?= $cta_link['title'] ?>
                        <svg class='arrow-right' viewBox='243.81 243.737 15.017 12.535'
                             xmlns='http://www.w3.org/2000/svg'>
                            <path class='arrow-head'
                                  d='M 251.977 256.242 L 257.977 250.742 L 258.781 250.005 L 257.977 249.268 L 251.977 243.768 L 250.625 245.242 L 255.821 250.005 L 250.625 254.768 L 251.977 256.242 Z'
                                  style='fill: rgb(0, 117, 227);'></path>
                            <path class='arrow-line'
                                  d='M 243.857 249.004 L 256.857 249.004 L 256.857 251.004 L 243.857 251.004 L 243.857 249.004 Z'
                                  style='fill: rgb(0, 117, 227);'></path>
                        </svg>

                    </a>
                <?php } ?>
            </div>
        <?php } ?>
    <?php } ?>


</div>
</section>


<!-- endregion propellerhealth Block -->

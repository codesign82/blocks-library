import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";

import '../../../scripts/sitesSizer/cyberhill';

const blockScript = async (container = document) => {
  const block = container.querySelector('.contact_block');
  
  // add block code here
  const middleMountain = block.querySelector('#middle-mount');
  const rightMountain = block.querySelector('#right-mount');
  const leftMountain = block.querySelector('#left-mount');
  const tl = gsap.timeline({repeat: -1, yoyo: true, defaults: {duration: 2}})
  tl
      .to(middleMountain, {y: 20}, 'time')
      .to(rightMountain, {y: 30}, 'time')
      .to(leftMountain, {y: 10}, 'time')
  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



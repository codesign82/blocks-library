<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'contact_block';
$className = 'contact_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/contact_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/

$consultation_intro_text = get_field('consultation_intro_text');
$consultation_button = get_field('consultation_button');
$contact_intro_text = get_field('contact_intro_text');
$contact_button = get_field('contact_button');
?>
<!-- region CyberHill's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="two-card ">
  <div class="white-card">
    <?php if ($consultation_intro_text) { ?>
      <h3 class="headline-3"><?= $consultation_intro_text ?></h3>
    <?php } ?>
    <?php if ($consultation_button) { ?>
      <a href="<?= $consultation_button['url'] ?>" target="<?= $consultation_button['target'] ?>" class="primary-button primary-button-with-arrow  primary-button-small-button" id="left-cta">
        <?= $consultation_button['title'] ?>
        <svg class="arrow" viewBox="0 0 18 16" fill="none"
             xmlns="http://www.w3.org/2000/svg">
          <path
            d="M10.5 0.5L9.4275 1.54475L15.1125 7.25H0V8.75H15.1125L9.4275 14.4298L10.5 15.5L18 8L10.5 0.5Z"
            fill="#1D4070"></path>
        </svg>
      </a>
    <?php } ?>
  </div>
  <div class="blue-card">
    <?php if ($contact_intro_text) { ?>
      <h3 class="headline-3"><?= $contact_intro_text ?></h3>
    <?php } ?>
    <?php if ($contact_button) { ?>
      <a class="primary-button" href="<?= $contact_button['url'] ?>" target="<?= $contact_button['target'] ?>" id="contact-us"><?= $contact_button['title'] ?></a>
    <?php } ?>
  </div>
</div>
</section>
<!-- endregion CyberHill's Block -->

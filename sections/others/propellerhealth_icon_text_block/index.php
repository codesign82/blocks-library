<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$choose_your_theme = get_field('choose_your_theme');
$number_of_cards_in_row = get_field('number_of_cards_in_row');


$dataClass = 'icons_text_block';
$className = 'icons_text_block';
$className .= ' ' . $choose_your_theme;
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}

if ($number_of_cards_in_row === '4') {
    $className .= ' ' . 'col-number-4';
}
if ($choose_your_theme === 'theme-2') {
    $number_of_cards_in_row = '2';
}

if ($choose_your_theme === 'theme-3') {
    $number_of_cards_in_row = '2';
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/icons_text_block/screenshot.png" >';

    return;
endif;


/****************************
 *     Custom ACF Meta      *
 ****************************/
$cta_button = get_field('cta_button');
$title = get_field('title');
?>
<!-- region propellerhealth Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class='container'>
    <div class='main-content'>
        <?php if ($title) { ?>
            <div class='title headline-2 iv-st-from-bottom'><?= $title?></div>
        <?php } ?>
        <?php if (have_rows('card')) { ?>
            <div class='icons-wrapper row <?php if (!$choose_your_theme === '3') { ?>row-cols-1 row-cols-md-3 row-cols-lg-<?= $number_of_cards_in_row ?>
      <?php } else { ?>row-cols-1 row-cols-md-3 row-cols-lg-<?= $number_of_cards_in_row ?>
      <?php } ?>
'>
                <?php while (have_rows('card')) {
                    the_row();
                    $icon = get_sub_field('icon');
                    $card_title = get_sub_field('card_title');
                    $description = get_sub_field('description');
                    $sub_title = get_sub_field('sub_title');
                    ?>
                    <div class="card-wrapper col">
                        <div class='card iv-st-from-bottom'>
                            <?php if ($icon) { ?>
                                <div class='svg-wrapper moving-shape'><?= $icon ?></div>
                            <?php } ?>
                            <?php if ($sub_title) { ?>
                                <div
                                        class='card-sub-title headline-5 fw-bold '><?= $sub_title ?></div>
                            <?php } ?>
                            <?php if ($card_title) { ?>
                                <div
                                        class='card-title headline-4 fw-bold'><?= $card_title ?></div>
                            <?php } ?>
                            <?php if ($description) { ?>
                                <div class='description paragraph'><?= $description ?></div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
    <?php if ($cta_button) { ?>
        <a href='<?= $cta_button['url'] ?>'
           target="<?= $cta_button['target'] ?>"
           class='cta-button btn iv-st-from-bottom'><?= $cta_button['title'] ?></a>
    <?php } ?>
</div>
</section>


<!-- endregion propellerhealth Block -->

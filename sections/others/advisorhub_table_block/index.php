<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'table_block';
$className = 'table_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/table_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/

$recruiting_table_wire = get_field('recruiting_table_wire');
$show_posts_manually_or_automatically = get_field('show_posts_manually_or_automatically');
$number_of_rows = get_field('number_of_rows');

$has_container = get_field('has_container');
$has_container = $has_container ? 'container' : '';

?>
<!-- region advisorhub's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<div class="<?= $has_container; ?>">

    <table class="main-table" data-page-length="<?= $number_of_rows ?>">
        <thead>
        <tr class="table-header has-arrow">
            <th>MOVE DATE</th>
            <th>NAME</th>
            <th>AUM</th>
            <th>OLD FIRM</th>
            <th>NEW FIRM</th>
        </tr>
        </thead>
        <?php if ($show_posts_manually_or_automatically === 'manually') { ?>
            <?php if ($recruiting_table_wire) :
                ?>
                <tbody>
                <?php foreach ($recruiting_table_wire as $post):
                    $move_date = get_field('move_date', $post);
                    $aum = get_field('aum', $post);
                    $old_firm = get_field('old_firm', $post);
                    $new_firm = get_field('new_firm', $post);
                    // Setup this post for WP functions (variable must be named $post).
                    setup_postdata($post); ?>
                    <tr class="table-body" ">

                    <td data-label="Move Date"><?= $move_date ?></td>
                    <td data-label="Name"><?= get_the_title($post) ?></td>
                    <td data-label="AUM"><?= $aum ?></td>
                    <td data-label="Old Firm"><?= $old_firm ?></td>
                    <td data-label="New Firm"><?= $new_firm ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
                <?php
                // Reset the global post object so that the rest of the page works correctly.
                wp_reset_postdata(); ?>
            <?php endif; ?>
        <?php }
        elseif ($show_posts_manually_or_automatically === 'automatically') { ?>
            <?php
            $query_options_for_cpt = get_field('query_options_for_cpt');
            $number_of_posts = @$query_options_for_cpt['number_of_posts'];
            $order = @$query_options_for_cpt['order'];
            $args = array(
                'post_type' => 'recruiting_moves',
                'posts_per_page' => $number_of_posts ?: 8,
                'order' => $order,
                'post_status' => 'publish',
            );
            // The Query
            $the_query = new WP_Query($args);
            // The Loop
            if ($the_query->have_posts()) { ?>
                <tbody>
                <?php while ($the_query->have_posts()) {
                    $the_query->the_post();
                    $move_date = get_field('move_date', get_the_ID());
                    $aum = get_field('aum', get_the_ID());
                    $old_firm = get_field('old_firm', get_the_ID());
                    $new_firm = get_field('new_firm', get_the_ID()); ?>
                    <tr class="table-body">
                        <td data-label="Move Date"><?= $move_date; ?></td>
                        <td data-label="Name"><?php the_title(); ?></td>
                        <td data-label="AUM"><?= $aum ?></td>
                        <td data-label="Old Firm"><?= $old_firm ?></td>
                        <td data-label="New Firm"><?= $new_firm ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            <?php }
            /* Restore original Post Data */
            wp_reset_postdata(); ?>
        <?php } ?>
    </table>
</div>
</section>


<!-- endregion advisorhub's Block -->

<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'double_card_cta_block';
$className = 'double_card_cta_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/double_card_cta_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$cta_button = get_field('cta_button');
$image = get_field('image');
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="content">
        <div class="left-content">
            <?php if ($title) { ?>
                <h2 move-to-here class="title headline-3 iv-st-from-bottom">
                    <?= $title ?>
                </h2>
            <?php } ?>
            <?php if ($description) { ?>
                <div class="description paragraph iv-st-from-bottom">
                    <?= $description ?>
                </div>
            <?php } ?>
            <?php if ($cta_button) { ?>
                <div class="iv-st-from-bottom">
                    <a class="btn " href="<?= $cta_button['url'] ?>"
                       target="<?= $cta_button['target'] ?>"><?= $cta_button['title'] ?></a>
                </div>
            <?php } ?>
        </div>
        <div class="right-content">
            <?php if (have_rows('buttons')) { ?>
                <div class="links-group ">
                    <?php while (have_rows('buttons')) {
                        the_row();
                        $link = get_sub_field('link');
                        ?>
                        <a class="link custom-link iv-st-from-bottom" href="<?= $link['url'] ?>"> <?= $link['title'] ?>
                            <svg class='arrow' viewBox='0 0 29.5 22.1' role="application">
                                <path class='arrow-head'
                                      d='M29.1,12.1a1.7,1.7,0,0,0,0-2.1L19.5.4a1.7,1.7,0,0,0-2.1,0,1.6,1.6,0,0,0,0,2.2L25.9,11l-8.5,8.5a1.6,1.6,0,0,0,0,2.2,1.7,1.7,0,0,0,2.1,0Z'
                                      fill='#f90'/>
                                <path class='arrow-line' d='M0,12.5H28v-3H0Z' fill='#f90'/>
                            </svg>
                        </a>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php if ($image) { ?>
                <picture class="img-wrapper aspect-ratio">
                    <?= get_acf_image($image, 'img-730-800') ?>
                </picture>
            <?php } ?>
        </div>

    </div>
</div>
</section>

<!-- endregion headversity's Block -->

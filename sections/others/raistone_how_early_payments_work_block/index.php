<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
$light_mode = get_field('light_mode');
// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'how_early_payments_work_block';
$className = $light_mode ? 'how_early_payments_work_block light' : 'how_early_payments_work_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/how_early_payments_work_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$section_title = get_field('section_title');
$section_description = get_field('section_description');
$section_sub_description = get_field('section_sub_description');
$bottom_text = get_field('bottom_text');
$first_timeline_button_text = get_field('first_timeline_button_text');
$second_timeline_button_text = get_field('second_timeline_button_text');
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<div class="container">

    <div class="top-content">
        <?php if ($section_title) { ?>
            <h3 class="headline-2 main-title iv-st-from-bottom"><?= $section_title ?></h3>
        <?php } ?>
        <?php if ($section_description) { ?>
            <div class="paragraph paragraph-normal-paragraph iv-st-from-bottom">
                <?= $section_description ?>
            </div>
        <?php } ?>
    </div>
    <?php if ($first_timeline_button_text || $second_timeline_button_text) { ?>
        <ul class="toggle-action">
            <?php if ($first_timeline_button_text) { ?>
                <li class="headline-6 toggle-btn tab-1 active-btn"><?= $first_timeline_button_text ?></li>
            <?php } ?>
            <?php if ($second_timeline_button_text) { ?>
                <li class="headline-6 toggle-btn tab-2"><?= $second_timeline_button_text ?></li>
            <?php } ?>
        </ul>
    <?php } ?>
    <?php if ($section_sub_description) { ?>
        <div class="paragraph paragraph-normal-paragraph description iv-st-from-bottom">
            <?= $section_sub_description ?>
        </div>
    <?php } ?>
    <?php if (have_rows('steps')) { ?>
        <div class="steps-wrapper tab-content tab-content-1 tab-content-active">
            <div class="animated-line-border main">
                <svg class="dotted-line" preserveAspectRatio="xMidYMin slice">
                    <defs>
                        <clipPath class="animated-line-border-clip-path" id="animated-line-border-clip-path">
                            <rect x="0" y="0" width="100%" height="100%"></rect>
                        </clipPath>
                    </defs>
                    <line x1="50%" x2="50%" y1="0" y2="100%" clip-path="url(#animated-line-border-clip-path)"/>
                </svg>
            </div>
            <?php while (have_rows('steps')) {
                the_row();
                $icon = get_sub_field('icon');
                $color = get_sub_field('color');
                $button_text = get_sub_field('button_text');
                $text_color = get_sub_field('text_color');
                $step_title = get_sub_field('step_title');
                $has_sub_steps = get_sub_field('has_sub_steps');
                ?>
                <div style="color:<?= $color ?>" class="step <?= $has_sub_steps ? '' : 'no-list' ?>">
                    <div class="icon-title-wrapper">
                        <div class="icon">
                            <img src="<?= @$icon['url'] ?>" alt="<?= @$icon['alt'] ?>">
                        </div>
                        <?php if ($button_text) { ?>
                            <h4 class="step-title">
                                <span style="color: <?= $text_color ?>"><?= $button_text ?></span>
                            </h4>
                        <?php } ?>
                    </div>
                    <div class="content-wrapper">
                        <div class="time-wrapper">
                            <h5 class="step-time">
                                <svg class="dotted-line" preserveAspectRatio="xMidYMin slice">
                                    <line x1="0" x2="100%" y1="50%" y2="50%"/>
                                </svg>
                                <span class="date-content"><?= $step_title ?></span>
                            </h5>
                            <div class="animated-line-border"></div>
                        </div>
                        <?php if ($has_sub_steps) { ?>
                            <div class="list-wrapper">
                                <svg class="dotted-line inner">
                                    <line x1="0" x2="99%" y1="2" y2="2"/>
                                    <line x1="2" x2="2" y1="0" y2="99%"/>
                                    <line class="hide-small" x1="0" x2="99%" y1="99%" y2="99%"/>
                                    <line class="hide-large" x1="99%" x2="99%" y1="0" y2="99%"/>
                                </svg>
                                <ul class="info-wrapper">
                                    <?php
                                    if (have_rows('the_sub_steps')):
                                        while (have_rows('the_sub_steps')) : the_row();
                                            $text = get_sub_field('text'); ?>
                                            <li class="paragraph info paragraph-l-paragraph">
                                                <?= $text ?>
                                            </li>
                                        <?php endwhile; endif; ?>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
    <?php if (have_rows('second_steps')) { ?>
        <div class="steps-wrapper tab-content tab-content-2">
            <div class="animated-line-border main">
                <svg class="dotted-line" preserveAspectRatio="xMidYMin slice">
                    <defs>
                        <clipPath class="animated-line-border-clip-path" id="animated-line-border-clip-path2">
                            <rect x="0" y="0" width="100%" height="100%"></rect>
                        </clipPath>
                    </defs>
                    <line x1="50%" x2="50%" y1="0" y2="100%" clip-path="url(#animated-line-border-clip-path2)"/>
                </svg>
            </div>
            <?php while (have_rows('second_steps')) {
                the_row();
                $icon = get_sub_field('icon');
                $color = get_sub_field('color');
                $button_text = get_sub_field('button_text');
                $text_color = get_sub_field('text_color');
                $step_title = get_sub_field('step_title');
                $has_sub_steps = get_sub_field('has_sub_steps');
                ?>
                <div style="color:<?= $color ?>" class="step <?= $has_sub_steps ? '' : 'no-list' ?>">
                    <div class="icon-title-wrapper">
                        <div class="icon">
                            <img src="<?= @$icon['url'] ?>" alt="<?= @$icon['alt'] ?>">
                        </div>
                        <?php if ($button_text) { ?>
                            <h4 class="step-title">
                                <span style="color: <?= $text_color ?>"><?= $button_text ?></span>
                            </h4>
                        <?php } ?>
                    </div>
                    <div class="content-wrapper">
                        <div class="time-wrapper">
                            <h5 class="step-time">
                                <svg class="dotted-line" preserveAspectRatio="xMidYMin slice">
                                    <line x1="0" x2="100%" y1="50%" y2="50%"/>
                                </svg>
                                <span class="date-content"><?= $step_title ?></span>
                            </h5>
                            <div class="animated-line-border"></div>
                        </div>
                        <?php if ($has_sub_steps) { ?>
                            <div class="list-wrapper">
                                <svg class="dotted-line inner">
                                    <line x1="0" x2="99%" y1="2" y2="2"/>
                                    <line x1="2" x2="2" y1="0" y2="99%"/>
                                    <line class="hide-small" x1="0" x2="99%" y1="99%" y2="99%"/>
                                    <line class="hide-large" x1="99%" x2="99%" y1="0" y2="99%"/>
                                </svg>
                                <ul class="info-wrapper">
                                    <?php
                                    if (have_rows('the_sub_steps')):
                                        while (have_rows('the_sub_steps')) : the_row();
                                            $text = get_sub_field('text'); ?>
                                            <li class="paragraph info paragraph-l-paragraph">
                                                <?= $text ?>
                                            </li>
                                        <?php endwhile; endif; ?>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
    <?php if ($bottom_text) { ?>
        <div class="separator"></div>
        <div class="bottom-content-text paragraph paragraph-normal-paragraph description iv-st-from-bottom">
            <?= $bottom_text ?>
        </div>
    <?php } ?>
</div>

</section>


<!-- endregion raistone's Block -->

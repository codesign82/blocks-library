import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/raistone';

import {gsap} from "gsap"
import {ScrollTrigger} from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger)

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const blocks = container.querySelectorAll('.how_early_payments_work_block');

  for (let block of blocks) {


  const activeContent = block.querySelector(".tab-content-active");

  function runAnimation(activeContent) {
    let mainDottedLine = activeContent.querySelector('.animated-line-border-clip-path rect');
    // gsap.set(mainDottedLine, {attr: {height: '0%'}});
    gsap.fromTo(mainDottedLine, {
      attr: {height: '0%'},
    }, {
      attr: {height: '100%'},
      ease: 'linear',
      scrollTrigger: {
        trigger: mainDottedLine,
        endTrigger: activeContent,
        start: 'top center',
        end: 'bottom center',
        scrub: 0.5,
      }
    })
    for (let step of activeContent.querySelectorAll('.step')) {
      const icon = step.querySelector(".icon");
      const stepTitle = step.querySelector(".step-title");

      const stepTimeWrapper = step.querySelector(".step-time");
      const stepHorizontalLine = stepTimeWrapper.querySelector(".dotted-line");
      const stepListLine = step.querySelector(".dotted-line.inner");
      const infoList = step.querySelectorAll(".info");
      const stepAnimation = gsap.timeline({
        scrollTrigger: {
          trigger: icon,
          start: 'center center',
          toggleActions: 'play none none reverse'
        }
      })
          .set(step, {opacity: 1})

          .fromTo(icon, {scale: 0}, {
            scale: 1, ease: 'back.out(2)'
          })
          .fromTo(stepTimeWrapper, {opacity: 0, x: 50}, {
            opacity: 1,
            x: 0
          }, '<50%')
          .fromTo(stepTitle, {opacity: 0, x: -30}, {
            opacity: 1, x: 0
          }, '<')

      if (infoList.length) {
        stepAnimation
            .fromTo(stepHorizontalLine, {
              scaleX: 0,
              transformOrigin: 'left'
            }, {scaleX: 1, transformOrigin: 'left'}, '<50%')
            .fromTo(stepListLine, {opacity: 0, x: 50}, {
              opacity: 1, x: 0
            }, '<25%')
            .fromTo(infoList, {opacity: 0, x: 50}, {
              opacity: 1, x: 0, stagger: 0.05
            }, '<25%');
      }
      // stepAnimation.from(stepTitle, {opacity: 0, x: -30}, '.75')
    }
  }

  runAnimation(activeContent);

  // toggle buttons
  const tab1 = block.querySelector('.tab-1');
  const tab2 = block.querySelector('.tab-2');
  const tabContent1 = block.querySelector('.tab-content-1');
  const tabContent2 = block.querySelector('.tab-content-2');

  function tabController(activeTab) {
    if (activeTab === 1) {
      tab2?.classList.remove('active-btn');
      tab1?.classList.add('active-btn');
      tabContent2?.classList.remove('tab-content-active');
      tabContent1?.classList.add('tab-content-active');
    } else {
      tab1?.classList.remove('active-btn');
      tab2?.classList.add('active-btn');
      tabContent1?.classList.remove('tab-content-active');
      tabContent2?.classList.add('tab-content-active');
    }
    runAnimation(block.querySelector(".tab-content-active"));
  }

  tab1?.addEventListener('click', () => tabController(1))
  tab2?.addEventListener('click', () => tabController(2))


  animations(block);
  imageLazyLoading(block);}
};
windowOnLoad(blockScript);


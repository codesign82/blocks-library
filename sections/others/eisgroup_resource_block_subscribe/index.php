<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'resource_block_subscribe';
$className = 'resource_block_subscribe';
if ( ! empty( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
    $className .= ' align' . $block['align'];

}
if ( get_field( 'is_screenshot' ) ) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/resource_block_subscribe/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title        = get_field( 'title', 'options' );
$description  = get_field( 'description', 'options' );
$form_wrapper = get_field( 'form_wrapper', 'options' );
$svg_uid      = uniqid();
?>
<!-- region dh-updated-theme's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
    <div class="subscribe-wrapper">
        <div class="left-content">
            <?php if ( $title ) { ?>
                <h2 class="headline-1 left-title"><?= $title ?></h2>
            <?php } ?>
            <?php if ( $description ) { ?>
                <div class="headline-3 left-description">
                    <?= $description ?>
                </div>
            <?php } ?>
        </div>
        <div class="right-content">
            <div class="shape">
                <svg class="remove-in-small" fill="none" height="542" viewBox="0 0 621 542" width="621" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <mask id="<?= $svg_uid ?>" height="542" maskUnits="userSpaceOnUse" width="621" x="0" y="0">
                        <path d="m0 0h621v542h-621z" fill="#fff"/>
                    </mask>
                    <g mask="url(#<?= $svg_uid ?>)" opacity=".4">
                        <path d="m1087.38 1386.59h199.28l263.71-423.543-110.97-189.701h-256.56l-243.428 381.784zm-441.514-613.244h-255.362l-112.168 189.701 263.715 423.543h495.209zm1201.634-196.859 477.31 808.913 1.19 1.19h-479.7l-1.19-1.19-243.43-414.001zm-270.88 353.154-639.595-1085.709 238.655-405.65 646.76 1096.447zm749.38 496.319h-1213.56l260.13 408.04h714.77zm-1184.92-2007.96h-478.509l-505.949 856.636h478.504zm-1007.13 897.201-647.95 1097.639 239.849 409.23 887.798-1506.869zm927.18 1110.759 260.13 408.04h-1557.226l239.84863-408.04zm354.4-691.986-239.85-406.842h-522.655l-239.848 406.842z"
                              fill="#185a7d"/>
                    </g>
                </svg>


                <svg class="show-in-small" width="350" height="445" viewBox="0 0 350 445" fill="none" xmlns="http://www.w3.org/2000/svg">

                    <path opacity="0.4"
                          d="M678.242 1080.07H819.14L1005.6 780.998L927.133 647.045H745.738L573.624 916.635L678.242 1080.07ZM366.074 647.045H185.523L106.215 780.998L292.672 1080.07H642.807L366.074 647.045ZM1215.68 508.038L1553.16 1079.23L1554 1080.07H1214.83L1213.99 1079.23L1041.88 786.895L1215.68 508.038ZM1024.16 757.408L571.936 -9.23856L740.676 -295.678L1197.96 478.551L1024.16 757.408ZM1554 1107.88H695.96L879.885 1396H1385.26L1554 1107.88ZM716.208 -310H377.886L20.1581 294.893H358.481L716.208 -310ZM4.12787 323.537L-454 1098.61L-284.417 1387.58L343.294 323.537H4.12787ZM659.681 1107.88L843.607 1396H-257.418L-87.8351 1107.88H659.681ZM910.259 619.243L740.676 331.961H371.136L201.553 619.243H910.259Z"
                          fill="#185A7D"/>
                </svg>

            </div>
            <?php if ( $form_wrapper ) { ?>
                <div class="form-wrapper">
                    <?php if ( $form_wrapper['form_title'] ) { ?>
                        <h5 class="headline-4 form-title"><?= $form_wrapper['form_title'] ?></h5>
                    <?php } ?>
                    <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/embed/v2.js"></script>
                    <script>
                        hbspt.forms.create({
                            region: "na1",
                            portalId: "16059",
                            css: " ",
                            formId: "<?= $form_wrapper['form_id'] ?>",
                            onFormReady: () => {
                                const formInputs = document.querySelectorAll(".resource_block_subscribe .hs-fieldtype-text input");
                                const addFocusBlurToInputs = (el) => {
                                    if (!el) return;
                                    el.addEventListener("focus", () => {
                                        el.parentElement.parentElement.classList.add("focused");
                                    })
                                    el.addEventListener("blur", () => {
                                        if (!el.value) {
                                            el.parentElement.parentElement.classList.remove("focused");
                                        }
                                    })
                                }
                                formInputs.forEach(el => {
                                    if (el.value) {
                                        el.parentElement.parentElement.classList.add("focused");
                                    }
                                    addFocusBlurToInputs(el);
                                })
                            }
                        });
                    </script>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
</section>


<!-- endregion dh-updated-theme's Block -->

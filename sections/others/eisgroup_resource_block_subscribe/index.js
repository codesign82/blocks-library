import './index.html';
import './style.scss';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/propellerhealth';
import $ from "jquery";

const blockScript = async (container = document) => {
  const block = container.querySelector('.resource_block_subscribe');
  
  // add block code here
  const formInputs = block.querySelectorAll(".ginput_container input");

  const addFocusBlurToInputs = (el) => {
    el.addEventListener("focus", () => {
      el.parentElement.parentElement.classList.add("focused");
    })
    el.addEventListener("blur", () => {
      if (!el.value) {
        el.parentElement.parentElement.classList.remove("focused");
      }
    })
  }

  formInputs.forEach(el => {
    if (el.value) {
      el.parentElement.parentElement.classList.add("focused");
    }
    addFocusBlurToInputs(el);
  })

  if (document.querySelector('form[id *="gform"]')) {
    $(document).on('gform_post_render', function () {
      block.querySelectorAll(".ginput_container input").forEach(el => {
        addFocusBlurToInputs(el);
      })
    });
  }


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



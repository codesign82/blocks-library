import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/towSmart';
gsap.registerPlugin('ScrollTrigger');

const blockScript = async (container = document) => {
  const block = container.querySelectorAll('.product_types_block');
  
  const productCircle = block.querySelectorAll(".products-types-circle")
  const productBackground = block.querySelector(".product-background")
  const title = block.querySelector(".text-head")
  const btn = block.querySelector(".btn-arrow")
  
  const bigCar = gsap.timeline({
    defaults: {
      ease: "power3"
    },
    scrollTrigger: {
      trigger: block,
      start: "center bottom",
      end: "center top",
    },
  })
      .from(title, {
        opacity: 0,
        y: 10
      })
      .from(productBackground, {
        opacity: 0,
        ease: "power2",
        duration: 1
      }, "<1")
      .from(productCircle, {
        opacity: 0,
        ease: "power2",
        duration: 1
      }, "<")
      .to(productCircle, {
        background: "rgba(255, 255, 255, 0.2)",
        scale: 1.2,
        stagger: 1
      })
      .to(productCircle, {
        background: "rgba(255, 255, 255, 0.1)",
        scale: 1,
        stagger: 1,
        clearProps: true,
      }, "<33%")
      
      .from(btn, {
        opacity: 0,
      })
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);




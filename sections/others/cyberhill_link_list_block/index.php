<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'link_list_block';
$className = 'link_list_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/link_list_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_logo = get_field('main_logo');
$title = get_field('title');
$sub_title = get_field('sub_title');
?>
<!-- region CyberHill's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <?php if ($title) { ?>
    <div class="title-with-shape title-with-shape-blue iv-st-from-bottom">
      <?= $title ?>
    </div>
  <?php } ?>
  <?php if ($sub_title) { ?>
    <div class="headline-4 iv-st-from-bottom"><?= $sub_title ?></div>
  <?php } ?>

  <div class="content-wrapper">

    <?php
    if (have_rows('links')) {
      ?>
      <?php
      while (have_rows('links')) {
        the_row();
        $link = get_sub_field('link');
        $text = get_sub_field('text');
        ?>
        <?php if ($link) { ?>
          <a class="link-wrapper iv-st-from-bottom" href="<?= $link['url'] ?>" target="<?= $link['target'] ?>">
            <h2 class="headline-2"><?= $link['title'] ?></h2>
            <div class="more-info-content">
              <p class="paragraph paragraph-12"><?= $text ?></p>
              <div class="primary-button-only-arrow">
                <svg class="arrow" width="25" height="15" viewBox="0 0 25 15" fill="none">

                  <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M16.2524 1.29418L21.9363 6.89262L0.515991 6.89262V8.01564L22.0294 8.01564L16.2524 13.7058L17.0583 14.5L23.359 8.29398L23.366 8.3009L24.1719 7.50671L24.1651 7.49999L24.1719 7.49327L23.366 6.69907L23.359 6.706L17.0583 0.499981L16.2524 1.29418Z"
                        fill="#82FA81"/>
                </svg>
              </div>
            </div>
          </a>
        <?php } ?>
        <?php
      } ?>
      <?php
    }
    ?>
  </div>
</div>
</section>


<!-- endregion CyberHill's Block -->

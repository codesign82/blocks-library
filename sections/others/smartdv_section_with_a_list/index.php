<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'section_with_a_list';
$className = 'section_with_a_list';
$dark_background = get_field('dark_background');
if ($dark_background) {
    $className .= ' dark-background';
}

if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/section_with_a_list/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$small_icon = get_field('small_icon');
$title = get_field('title');
$description = get_field('description');

?>
<!-- region samrt_dv Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="section-list">
        <?php if ($small_icon) { ?>
            <div class="small-icon iv-st-from-bottom">
                <?= $small_icon ?>
            </div>
        <?php } ?>
        <?php if ($title) { ?>
            <div class="headline-3 section-title word-up"><?= $title ?></div>
        <?php } ?>
        <div class="description-wrapper iv-st-from-bottom">
            <?php if ($description) { ?>
                <div class="description paragraph">
                    <?= $description ?>
                </div>
            <?php } ?>
        </div>


    </div>
</div>
</section>


<!-- endregion samrt_dv Block -->

import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";

import '../../../scripts/sitesSizer/propellerhealth';



const blockScript = async (container = document) => {
  const block = container.querySelector('.section_with_a_list');


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";

import '../../../scripts/sitesSizer/propellerhealth';

import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger)


const blockScript = async (container = document) => {
  const block = container.querySelector('.icon_with_a_text_block');


  const pinElement = block.querySelector(".pin-element")
  const boxShapes = block.querySelector(".box-shapes-wrapper")
  const illustration = block.querySelector(".icon")
  const button = block.querySelector(".cta-button")
  const textWrapper = block.querySelector(".text-wrapper")
  const textContent = block.querySelectorAll(".text-wrapper >*, .icon")

  if (!block.classList.contains("version-2")) {
    gsap.timeline({
      scrollTrigger: {
        trigger: pinElement,
        scrub: .5,
        // pin: true,
        start: "top 70%",
        end: "50% center",
      }
    })
        .to(boxShapes, {
          transform: "unset"
        })
        .from(textWrapper, {
          y: -illustration.clientHeight
        }, "<")
        .to(illustration, {
          opacity: 1,
          duration: .2
        })
        .from(button, {
          opacity: 0,
          duration: .2
        }, "<")
        .set("", {}, "+=1")
  } else {
    gsap.to(boxShapes, {
      transform: "unset",
      scrollTrigger: {
        trigger: block,
        scrub: .5,
        end: "bottom 80%"
      }
    })
    for (let i = 0; i < textContent.length; i++) {
      textContent[i].classList.add("iv-st-from-bottom")
    }
  }


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



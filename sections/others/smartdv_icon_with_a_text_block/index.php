<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'icon_with_a_text_block';
$className = 'icon_with_a_text_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
$vertically_or_horizontally = get_field('vertically_or_horizontally');

if ($vertically_or_horizontally) {
    $className .= ' version-2';
}

if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/icon_with_a_text_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$icon = get_field('icon');
$title = get_field('title');
$description = get_field('description');
$link = get_field('link');
$small_text = get_field('small_text');
?>
<!-- region samrt_dv Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="pin-element">
    <div class="box-wrapper">
        <div class="box-shapes-wrapper">
            <div class="horizontal-style box-shape right"></div>
            <div class="horizontal-style box-shape left"></div>
            <div class="vertical-style box-shape top"></div>
            <div class="vertical-style box-shape bottom"></div>
        </div>
        <div class="content-wrapper">
            <div class="container">
                <div class="icon-and-text-wrapper ">
                    <?php if ($icon) { ?>
                        <div class="icon">
                            <?= $icon ?>
                        </div>
                    <?php } ?>
                    <div class="text-wrapper">
                        <?php if ($small_text) { ?>
                            <div class="small-text">
                                <?= $small_text ?>
                            </div>
                        <?php } ?>
                        <?php if ($title) { ?>
                            <div class="headline-2 title"><?= $title ?></div>
                        <?php } ?>
                        <?php if ($description) { ?>
                            <div class="paragraph description">
                                <?= $description ?>
                            </div>
                        <?php } ?>
                        <?php if ($link) { ?>
                            <?php if ($vertically_or_horizontally) { ?>
                                <a class="cta-link last_link"
                                   href="<?= $link['url'] ?>"><?= $link['title'] ?>
                                    <svg width="23" height="12" viewBox="0 0 23 12" fill="none">
                                        <path d="M23 6L13 0.226497V11.7735L23 6ZM0 7H14V5H0V7Z"
                                              fill="#BFD730"/>
                                    </svg>
                                </a>
                            <?php } else { ?>
                                <a class="cta-button" href="<?= $link['url'] ?>"
                                   target="<?= $link['target'] ?>">
                                    <?= $link['title'] ?></a>
                            <?php } ?>
                        <?php } ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</section>


<!-- endregion samrt_dv Block -->

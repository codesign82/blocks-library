<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'second_content_link_block';
$className = 'second_content_link_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/takeoff-code/gutenberg-blocks/content_and_media_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$link = get_field('link');
?>
<!-- region senet's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<div class="container">
    <div class="cards">
        <div class="card-title-description">
            <div class="left-title">
                <?php if ($title) { ?>
                    <h2 class="headline-2 title-card"><?= $title ?></h2>
                <?php } ?>
            </div>
            <?php if ($description) { ?>
                <div class="right_description paragraph description-card">
                    <?= $description ?>
                </div>
            <?php } ?>
        </div>
        <div class="link">
            <?php if ($link) {   ?>
                <a class="cta-link  first-link" href="<?= $link['url'] ?>" target="<?= $link['target'] ?>"><?= $link['title'] ?>
                    <svg width="22" height="11" viewBox="0 0 22 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.3292 3.54817H15.4542V3.42317V1.17424C15.4542 0.24156 16.5818 -0.225521 17.2413 0.433966L21.4434 4.63607L21.5318 4.54768L21.4434 4.63607C21.8522 5.04487 21.8523 5.70771 21.4434 6.11656L17.2413 10.3187C16.5818 10.9782 15.4542 10.5111 15.4542 9.57839V7.32942V7.20442H15.3292H0.585938C0.331389 7.20442 0.125 6.99803 0.125 6.74348V4.0091C0.125 3.75455 0.331389 3.54817 0.585938 3.54817H15.3292Z" fill="white" stroke="#FF5900" stroke-width="0.25"></path>
                    </svg>
                </a>
            <?php } ?>
        </div>
    </div>
</div>
</section>

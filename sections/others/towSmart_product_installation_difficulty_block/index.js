import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/towSmart';

const blockScript = async (container = document) => {
  const block = container.querySelector('.product_installation_difficulty_block');
  
  
  const difficultyNumber = block.querySelector('.progress-wrapper').dataset.difficulty;
  const progress = block.querySelectorAll('.progress');
  const angle = block.querySelector('.pointer');
  const angleParent = progress[difficultyNumber - 1];
  angleParent.appendChild(angle);
  for (let i = 0; i < difficultyNumber; i++) {
    progress[i].classList.add('active');
    
  }
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);




import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import Swiper from "swiper";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {ScrollTrigger} from "gsap/ScrollTrigger";
import {createImageRevealTimeline} from "../../../scripts/general/animations/imagesRevealAnimation";
// import '../../../scripts/sitesSizer/wrap';
gsap.registerPlugin(ScrollTrigger);


const blockScript = async (container = document) => {
  const block = container.querySelector('.how_it_works_block');
  
  
  const steps = block.querySelectorAll('.step');
  
  const height = steps.length * 1000;
  
  for (let step of steps) {
    const image = step.querySelector('.left-content picture');
    const number = step.querySelector('.clip-steps-number');
    const paragraph = step.querySelector('.clip-paragraph');
    const btn = step.querySelector('.clip-btn');
    step.tl = gsap.timeline({paused: true})
        .addLabel('start')
        .add(createImageRevealTimeline(image, 'left'), 'start')
        .fromTo([number, paragraph, btn], {
          clipPath: 'polygon(0% 100%, 100% 100%, 100% 100%, 0% 100%)',
        }, {
          clipPath: 'polygon(0% 0%, 100% 0%, 100% 100%, 0% 100%)',
        }, 'start')
        .fromTo([number.children[0], paragraph.children[0], btn.children[0]], {
          yPercent: 100,
        }, {
          yPercent: 0,
        }, 'start')
    
    
  }
  let currentStep = -1;
  
  const images = [];
  for (const image of block.querySelectorAll('img')) {
    const p = new Promise(resolve => {
      if (image.complete && image.naturalHeight !== 0) {
        resolve();
      } else {
        image.addEventListener('load', resolve)
      }
    })
  }
  ScrollTrigger.create({
    trigger: block,
    start: 'center center',
    end: '+=' + height,
    pin: true,
    pinSpacing: true,
    // pinType:'transform',
    anticipatePin: 1,
    async onUpdate(self) {
      const nextStep = ~~(self.progress * 0.9999999 * steps.length);
      if (nextStep === currentStep) return
      
      await steps[currentStep]?.tl.reverse();
      setTimeout(() => {
        steps[currentStep]?.classList.remove('active');
        currentStep = nextStep;
        
        steps[currentStep]?.classList.add('active');
        steps[currentStep]?.tl.play();
      }, 200)
    }
  });
  
  
  animations(block);
  imageLazyLoading(block);
  await Promise.all(images)
  ScrollTrigger.refresh();
};
windowOnLoad(blockScript);




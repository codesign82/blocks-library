<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'how_it_works_block';
$className = 'how_it_works_block';
$className .= ' noise white-section';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() .
       '/template-parts/blocks/how_it_works_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title    = get_field( 'title' );
$subtitle = get_field( 'subtitle' );
?>
<!-- region PopRev's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <div class="background"></div>
  <div class="steps-text general-margin">
    <?php if ( $title ) { ?>
      <h2 class="headline-2 word-up"><?= $title ?></h2>
    <?php } ?>
    <?php if ( $subtitle ) { ?>
      <h4 class="sub-title iv-st-from-bottom-f"><?= count( get_field( 'slides' ) ) ?> <?= $subtitle ?></h4>
    <?php } ?>
  </div>
  <?php
  $index = 1;
  while ( have_rows( 'slides' ) ) {
    the_row();
    $image      = get_sub_field( 'image' );
    $text       = get_sub_field( 'text' );
    $cta_button = get_sub_field( 'cta_button' );
    ?>
    <div class="content-wrapper step <?= $index === 1 ? 'active' : '' ?>">
      <div class="left-content general-margin">
        <?php if ( $image ) { ?>
          <picture>
            <img
              src="<?= $image['url'] ?>"
              alt=" <?= $image['alt'] ?>">
          </picture>
        <?php } ?>
      </div>
      <div class="right-content">
        <div class="clip-steps-number">
          <div class="steps-number"><?= $index ++ ?></div>
        </div>

        <div>
          <?php if ( $text ) { ?>
            <div class="clip-paragraph">
              <div class="paragraph"> <?= $text ?></div>
            </div>
          <?php } ?>
          <?php if ( $cta_button ) { ?>
            <div class="clip-btn">
              <a class="btn iv-st-from-bottom glitch" data-hover
                 href="<?= $cta_button['url'] ?>"
                 target="<?= $cta_button['target'] ?>"><?= $cta_button['title'] ?></a>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  <?php } ?>
</div>
</section>

<!-- endregion PopRev's Block -->

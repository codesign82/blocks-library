import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/raistone';

import {gsap} from "gsap";
import DrawSVGPlugin from "gsap/DrawSVGPlugin";
import {ScrollTrigger} from "gsap/ScrollTrigger";
import {random} from "gsap/gsap-core";
import $ from "jquery"

gsap.registerPlugin(DrawSVGPlugin, ScrollTrigger)
/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.get_started_block');


  const chartDots = block.querySelector("#chart-dots")
  const chart = block.querySelector("#chart")
  const priceBtn = block.querySelectorAll(".price-btn")
  const continueBtn = block.querySelector(".gform_next_button")
  const stepOne = block.querySelector(".step-one")
  const stepTwo = block.querySelector(".step-two")
  const shapeLines = block.querySelectorAll("#lines-group path")
  const shapeSmallLines = block.querySelectorAll("#lines-group line")
  const blueBoxes = block.querySelectorAll(".blue-box")
  const imgWrapper = block.querySelectorAll(".image-wrapper")

  // When Enter Block Animation

  const lineAnimation = gsap.timeline({
    scrollTrigger: {
      trigger: block,
      start: "top 60%",
    }
  })
      .from(imgWrapper, {
        opacity: 0,
        transformOrigin: "left bottom"
      })
      .from(shapeLines, {
        drawSVG: 0,
        stagger: .1
      })
      .from(chart, {
        drawSVG: "0%",
        duration: 3,
        ease: "power3.inOut",
      }, 0)

      .from(shapeSmallLines, {
        opacity: 0,
        drawSVG: "0%",
        duration: 3,
        ease: "power3.inOut",
        stagger: .1
      }, 0)

      .from(chartDots, {
        xPercent: -100,
        duration: 2,
        ease: "power1"
      }, 0)

      .from(blueBoxes, {
        opacity: 0,
        yPercent: 10,
        xPercent: 10,
        stagger: .2,
        ease: "power3.inOut",
      }, 0)


  // infinite animation


  let isPriceSelected = false;
  for (let priceBtnElement of priceBtn) {
    priceBtnElement.addEventListener('click', () => {
      [...priceBtnElement.closest('.step-one').children].forEach(sib => sib.classList.remove('active'))
      priceBtnElement.classList.add('active')
      isPriceSelected = true
    })
  }
  // continueBtn?.addEventListener("click", () => {
  //
  //   gsap.to('.gform_wrapper .gform_page', {
  //     opacity: 0,
  //     xPercent: -50,
  //     delay: .5
  //   })
  // })
  $(document).on('gform_page_loaded', function (event, form_id, current_page) {
    console.log(event, form_id, current_page);
    if (current_page !== '2') return;
    const steps = block.querySelectorAll('.gform_wrapper .gform_page');
    steps[0].classList.add('active', 'absolute')
    steps[1].classList.add('active')
    gsap.set(steps[1], {opacity: 0, xPercent: 50})
    gsap.timeline().to(steps[0], {
      opacity: 0,
      xPercent: -50,
    }).to(steps[1], {
      opacity: 1,
      xPercent: 0,
    }, '<50%').then(() => steps[0].classList.remove('active'))
  });


  // var gf_active_page = 0;
  // jQuery('.gform_page').addClass('active');

  // function setPage(current_page, form_id) {
  //   setTimeout(function () {
  //     jQuery('.gform_wrapper .gform_page').removeClass('active');
  //     jQuery('.gform_wrapper #gform_page_' + form_id + '_' + current_page).addClass('animated fadeInUp active').delay(1000).addClass('active');
  //     gf_active_page = current_page;
  //   }, 100);
  // }

  $(document).on('gform_page_loaded', function (event, form_id, current_page) {

    // if (gf_active_page != current_page) {
    //   setPage(current_page, form_id);
    // } else {
    //   jQuery('.gform_wrapper #gform_page_' + form_id + '_' + current_page).addClass('active');
    //
    // }

  });


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


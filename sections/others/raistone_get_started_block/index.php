<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'get_started_block';
$className = 'get_started_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/get_started_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$image = get_field('image');
$cta_button = get_field('cta_button');
$form = get_field('form');
$question_text = get_field('question_text');
$email_text = get_field('email_text');
$email_link = get_field('email_link');
$call_text = get_field('call_text');
$phone = get_field('phone');
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<picture class="ellipse-back-ground">
    <img
            src="<?= get_template_directory_uri() . '/front-end/src/images/ellipse.png' ?>"
            alt="">
</picture>
<div class="container">
    <div class="get-started-wrapper">
        <div class="left-content">
            <?php if ($title) { ?>
                <h5 class="headline-5 title"><?= $title ?></h5>
            <?php } ?>
            <?php if ($description) { ?>
                <h2 class="headline-2 description"><?= $description ?></h2>
            <?php } ?>
            <div class="shape-wrapper">
                <svg id="lines-chart-svg" preserveAspectRatio="none"
                     viewBox="0 0 461.75 384.63">
                    <defs>
                        <clipPath id="clip-path">
                            <rect x="113.58" y="130.62" width="292.8" height="50.77"
                                  fill="none"/>
                        </clipPath>
                    </defs>
                    <g id="blue-box">
                        <path class="blue-box" d="M356.68,250.72h31v30.89h-31Z" fill="#9cdae0"/>
                        <path
                                d="M362.48,290.36H393.1a1.39,1.39,0,0,0,.95-.38,1.33,1.33,0,0,0,.41-.92V258.48a1.36,1.36,0,0,0-.39-1,1.41,1.41,0,0,0-1-.4H362.48a1.39,1.39,0,0,0-1,.4,1.36,1.36,0,0,0-.39,1v30.73a1.25,1.25,0,0,0,.45.84A1.24,1.24,0,0,0,362.48,290.36Zm29.27-2.66H364V259.83H392Z"
                                fill="#fff"/>
                    </g>
                    <g id="blue-box-2" data-name="blue-box">
                        <path class="blue-box" d="M375.44,328.55h31v30.89h-31Z" fill="#9cdae0"/>
                        <path
                                d="M362.48,352.62H393.1a1.41,1.41,0,0,0,1-.4,1.38,1.38,0,0,0,.39-1V320.73a1.38,1.38,0,0,0-.39-1,1.4,1.4,0,0,0-1-.39H362.48a1.14,1.14,0,0,0-.52.09,1.13,1.13,0,0,0-.45.29,1.32,1.32,0,0,0-.3.44,1.37,1.37,0,0,0-.08.53v30.53a1.35,1.35,0,0,0,1.35,1.36Zm29.27-2.71H364V322H392Z"
                                fill="#fff"/>
                    </g>
                    <g id="lines-group">
                        <path id="line-cover"
                              d="M435,216.35l-1.68,130a35.74,35.74,0,0,1-35.74,35.27H261.65"
                              fill="none" stroke="#fff"
                              stroke-linecap="round" stroke-linejoin="round"
                              stroke-width="6"/>
                        <path id="line-cover-2" data-name="line-cover"
                              d="M433,109.87l-1.68-71.6A35.74,35.74,0,0,0,395.59,3H3"
                              fill="none" stroke="#fff" stroke-linecap="round"
                              stroke-linejoin="round" stroke-width="6"/>
                        <line x1="236.03" y1="35.82" x2="325.31" y2="35.82" fill="none"
                              stroke="#fff" stroke-linecap="round"
                              stroke-linejoin="round" stroke-width="6"/>
                        <line x1="236.03" y1="56.19" x2="380.94" y2="56.19" fill="none"
                              stroke="#fff" stroke-linecap="round"
                              stroke-linejoin="round" stroke-width="6"/>
                        <line x1="356.74" y1="35.82" x2="380.86" y2="35.82" fill="none"
                              stroke="#fff" stroke-linecap="round"
                              stroke-linejoin="round" stroke-width="6"/>
                        <line x1="313.55" y1="76.33" x2="380.86" y2="76.33" fill="none"
                              stroke="#fff" stroke-linecap="round"
                              stroke-linejoin="round" stroke-width="6"/>
                    </g>
                    <g id="chart-group">
                        <polyline id="chart"
                                  points="142.09 187.75 203.68 126.17 290.53 210.11 381.13 120.35"
                                  fill="none" stroke="#fff"
                                  stroke-linecap="round" stroke-linejoin="round"
                                  stroke-width="6"/>
                        <g clip-path="url(#clip-path)">
                            <g id="chart-dots">
                                <line x1="64.55" y1="160.25" x2="73.05" y2="160.25"
                                      fill="none" stroke="#fff" stroke-linecap="round"
                                      stroke-linejoin="round" stroke-width="6"/>
                                <line x1="92.39" y1="160.25" x2="440.58" y2="160.25"
                                      fill="none" stroke-dasharray="16.44 19.34"
                                      stroke-linecap="round" stroke-linejoin="round"
                                      stroke-width="6" stroke="#fff"/>
                                <line x1="450.25" y1="160.25" x2="458.75" y2="160.25"
                                      fill="none" stroke="#fff" stroke-linecap="round"
                                      stroke-linejoin="round" stroke-width="6"/>
                            </g>
                        </g>
                    </g>
                </svg>
                <?php if ($image){ ?>
                <div class="image-wrapper">
                    <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                </div>
            </div>
        <?php } ?>
        </div>
        <div class="right-content">
            <?php if ($form) { ?>
                <div class="step-one step">
                    <?php echo do_shortcode('[gravityform id="' . $form . '" ajax="true" title="false" description="false"]'); ?>
                </div>
            <?php } ?>
            <div class="step-three step">
                <div class="paragraph step-description">

                </div>
            </div>
        </div>
    </div>
</div>
</section>


<!-- endregion raistone's Block -->


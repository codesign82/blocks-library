import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/propellerhealth';


import {gsap} from "gsap";

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
    const blocks = container.querySelectorAll('.hover_card_ctas_block');

    for (let block of blocks) {


        const taps = block.querySelectorAll('.tap');
        const tapImg = block.querySelectorAll('.tap-img img');
        const tapVideo = block.querySelector('.modal video.video');
        const playerIcon = block.querySelector(".player-icon-wrapper");
        const rightVideo = block.querySelector(".right-video");
        const rightContent = block.querySelector(".tap-img");
        let currentActiveTap = 0;
        let currentActiveImg = 0;
        let isActiveTapIncludeVideo;

        const hoverOnTapImage = () => {
            if (isActiveTapIncludeVideo) {
                rightVideo.classList.add("active");
                rightVideo.play();
            }
        }

        const removeHoverOnTapImage = () => {
            if (isActiveTapIncludeVideo) {
                rightVideo.classList.remove("active");
                rightVideo.pause();
                rightVideo.currentTime = 0;
            }
        }


        rightContent.addEventListener("mouseenter", hoverOnTapImage);
        rightContent.addEventListener("mouseleave", removeHoverOnTapImage);

        if (taps[currentActiveTap].dataset.srcType === 'video') {
            rightVideo.src = taps[currentActiveTap].dataset.videoSrc;
            isActiveTapIncludeVideo = true;
        }


        taps.forEach((tap, index) => {
            tap.addEventListener('mouseenter', () => {
                if (currentActiveTap === index) {
                    return null
                }
                const nextActiveImg = (currentActiveImg + 1) % 2;
                const srcType = tap.dataset.srcType;
                taps[currentActiveTap].classList.remove('tap-active');
                currentActiveTap = index;
                tap.classList.add('tap-active');

                tapImg[nextActiveImg].src = tap.dataset.imgSrc;
                tapImg[nextActiveImg].style.zIndex = 1;
                tapImg[nextActiveImg].style.opacity = 1;

                tapImg[currentActiveImg].style.zIndex = 0;
                tapImg[currentActiveImg].style.opacity = 0;
                tapVideo.src = tap.dataset.videoSrc;

                isActiveTapIncludeVideo = srcType === 'video';

                if (srcType === 'video') {
                    playerIcon.classList.add('player-icon-active');
                    rightVideo.src = tap.dataset.videoSrc;
                } else {
                    playerIcon.classList.remove('player-icon-active');
                }
                currentActiveImg = nextActiveImg;
            })

        })


        const video = block.querySelector(".video");
        const modal = block.querySelector(".modal");
        const exit = block.querySelector(".exit");


        const videoToggling = () => {
            if (isActiveTapIncludeVideo) {
                modal.classList.add("show")
                document.documentElement.classList.add('modal-opened');
                video?.play();
            }
        }

        rightContent?.addEventListener("click", videoToggling);

        exit?.addEventListener("click", hide);
        modal?.addEventListener("click", hide);

        function hide(e) {
            if (e.target.closest('.modal-content') && !e.target.closest('.exit')) return;
            modal?.classList.remove("show")
            video?.pause()
            document.documentElement.classList.remove('modal-opened');

        }


        animations(block);
        imageLazyLoading(block);
    }

};
windowOnLoad(blockScript);


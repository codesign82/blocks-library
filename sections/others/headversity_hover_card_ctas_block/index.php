<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'hover_card_ctas_block';
$className = 'hover_card_ctas_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/hover_card_ctas_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$left_title = get_field('left_title');
$left_description = get_field('left_description');
$cta = get_field('cta');
$cards_repeater = @get_field('cards_repeater')[0];
$f_image_or_video = @$cards_repeater['image_or_video'];
$first_image = @$cards_repeater['image'];
$first_video = @$cards_repeater['video_url'];
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="training-wrapper">
        <div class="left-content">
            <?php if ($left_title) { ?>
                <h3 move-to-here class="headline-3 title iv-st-from-bottom"><?= $left_title ?></h3>
            <?php } ?>
            <?php if ($left_description) { ?>
                <div class="paragraph m-paragraph iv-st-from-bottom description "><?= $left_description ?></div>
            <?php } ?>
            <?php if ($cta) { ?>
                <div class="iv-st-from-bottom button">
                    <a href="<?= $cta['url'] ?>" class="btn"><?= $cta['title'] ?></a>
                </div>
            <?php } ?>
            <?php if (have_rows('cards_repeater')) { ?>
            <div class="taps iv-st-from-bottom">
                <?php while (have_rows('cards_repeater')) {
                    the_row();
                    $icon = get_sub_field('icon');
                    $title = get_sub_field('title');
                    $link = get_sub_field('link');
                    $description = get_sub_field('description');
                    $image_or_video = get_sub_field('image_or_video');
                    $image = get_sub_field('image');
                    $video_url = get_sub_field('video_url');
                    $active_state = get_row_index() === 1 ? 'tap-active' : '';
                    ?>

                    <div class="tap <?= $active_state ?> " data-src-type="<?= $image_or_video ?>"
                         data-img-src="<?= @$image['url'] ?>" data-video-src="<?= $video_url ?>">
                        <?php if ($icon) { ?>
                            <div class="left-tap">
                                <?= $icon ?>
                            </div>
                        <?php } ?>
                        <div class="right-tap">
                            <?php if ($title) { ?>
                                <h5 class="headline-5 tap-title"><?= $title ?></h5>
                            <?php } ?>
                            <?php if ($description) { ?>
                                <div class="paragraph tap-description <?= $icon ? "has-icon" : "" ?>"><?= $description ?>
                                </div>
                            <?php } ?>
                        </div>
                        <?php if ($link) { ?>
                            <a class="arrow <?= $icon ? "has-icon" : "" ?>" href="<?= $link ?>">
                                <svg width="55" height="38" viewBox="0 0 55 38" fill="none"
                                     xmlns="http://www.w3.org/2000/svg" role="application">
                                    <path d="M53.7678 20.7678C54.7441 19.7915 54.7441 18.2085 53.7678 17.2322L37.8579 1.32233C36.8816 0.346023 35.2986 0.346023 34.3223 1.32233C33.346 2.29864 33.346 3.88156 34.3223 4.85787L48.4645 19L34.3223 33.1421C33.346 34.1184 33.346 35.7014 34.3223 36.6777C35.2986 37.654 36.8816 37.654 37.8579 36.6777L53.7678 20.7678ZM-2.18557e-07 21.5L52 21.5L52 16.5L2.18557e-07 16.5L-2.18557e-07 21.5Z"
                                          fill="#FF9900"/>
                                </svg>
                            </a>
                        <?php } ?>
                    </div>

                <?php } ?>
                <?php } ?>
            </div>
        </div>
        <div class="right-content">
            <?php if ($left_title) { ?>

                <h3 class="headline-3 title iv-st-from-bottom training-text"><?= $left_title ?></h3>
            <?php } ?>
            <?php if ($left_description) { ?>
                <div class="paragraph m-paragraph iv-st-from-bottom description top-description"><?= $left_description ?></div>
            <?php } ?>
            <?php if ($cta) { ?>
                <div class="iv-st-from-bottom left-btn">
                    <a href="<?= $cta['url'] ?>" class="btn"><?= $cta['title'] ?></a>
                </div>
            <?php } ?>
            <div class="tap-img aspect-ratio iv-st-from-bottom">
                <img src="<?= $first_image['url'] ?>" alt="<?= $first_image['alt'] ?>">
                <?= get_acf_image($first_image, 'img-730-800') ?>
                <?= get_acf_image($first_image, 'img-730-800', ['second-img']) ?>
                <video
                        type="video/mp4" playsinline muted class="right-video">
                </video>
                <button aria-label="Open Video Modal"
                        class="player-icon-wrapper <?= $f_image_or_video === 'video' ? 'player-icon-active' : '' ?>">
                    <svg class="player-icon" width="66" height="68" viewBox="0 0 66 68"
                         fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M62.418 31.2094L15.6039 58.2375V4.18127L62.418 31.2094Z"
                              fill="#FF9900"/>
                        <path d="M20.1859 11.0295L64 36.3256L20.1859 61.6217V11.0295Z"
                              stroke="#0A1B57" stroke-width="2"/>
                    </svg>
                </button>
            </div>
        </div>
        <div class="modal" aria-modal="true" role="dialog">
            <div class="modal-content">
                <div class="modal-video aspect-ratio">
                    <video
                            data-src="<?= $first_video ?>"
                            type="video/mp4" playsinline controls
                            class="video">
                    </video>
                </div>
            </div>
            <svg class="exit" width="39" height="39" viewBox="0 0 39 39" fill="none" role="application"
                 xmlns="http://www.w3.org/2000/svg">
                <line x1="9.90039" y1="28.2841" x2="28.2852" y2="9.8993"
                      stroke="#FF9900" stroke-width="3" stroke-linecap="round"/>
                <path d="M28.9922 28.2841L10.6074 9.89928" stroke="#FF9900"
                      stroke-width="3" stroke-linecap="round"/>
            </svg>
        </div>
    </div>
</div>
</section>


<!-- endregion headversity's Block -->

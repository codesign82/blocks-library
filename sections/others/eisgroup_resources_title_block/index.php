<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'resources_title_block';
$className = 'resources_title_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/resources_title_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region dh-updated-theme's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="text-wrapper">
        <?php if ($title) { ?>
            <h2 class="headline-1 title"><?= $title ?></h2>
        <?php } ?>
        <?php if ($description) { ?>
            <div class="headline-3 description">
                <?= $description ?>
            </div>
        <?php } ?>
    </div>
</div>
</section>


<!-- endregion dh-updated-theme's Block -->

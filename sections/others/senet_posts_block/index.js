import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import {swiperActivator} from "../../../scripts/general/swiper-activator"
import {Swiper,Navigation} from "swiper";
import '../../../scripts/sitesSizer/propellerhealth';

const blockScript = async (container = document) => {
  const block = container.querySelector('.posts_block');

  let postSwiper;
  swiperActivator('(max-width:991px)', () => {
    postSwiper = new Swiper(block.querySelector('.mySwiper'), {
      slidesPerView: 1,
      spaceBetween: 50,
      observer: true,
      observeParents: true,
      navigation: {
        nextEl: block.querySelector(".swiper-button-next"),
        prevEl: block.querySelector(".swiper-button-prev"),
      },
      breakpoints: {
        600:
            {
              slidesPerView: 2,
              spaceBetween: 20,
            },
      },

    });
  }, () => postSwiper?.destroy?.())
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



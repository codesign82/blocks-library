<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'posts_block';
$className = 'posts_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/takeoff-code/gutenberg-blocks/posts_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$select_your_posts = get_field('select_your_posts');
$type = get_field('type');

?>
<!-- region senet's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="latest-news-content-wrapper">
        <?php if ($title) { ?>
            <h2 class="headline-2 main-title "><?= $title ?></h2>
        <?php } ?>
        <div class="swiper mySwiper">
            <div class="swiper-wrapper">
                <?php if ($type === "relationship") { ?>
                    <?php $posts = get_field('posts'); ?>
                    <?php foreach ($posts as $post):
                        // Setup this post for WP functions (variable must be named $post).
                        setup_postdata($post);
                        get_template_part("takeoff-code/gutenberg-blocks/post-card", '', array(
                            'post_id' => $post,
                        ));
                        ?>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                <?php } else { ?>
                    <?php if (have_rows('posts_repeater')) {
                        ?>
                        <?php while (have_rows('posts_repeater')) {
                            the_row();
                            $title = get_sub_field('title');
                            $description = get_sub_field('description');
                            $cta = get_sub_field('cta');
                            $category = get_sub_field('category');
                            $image = get_sub_field('image');
                            ?>
                            <div class="swiper-slide ">
                                <div class="horizontal-card">
                                    <?php if ($image && !empty($image)) { ?>
                                        <div class="post-image-wrapper" aria-label="post image">
                                            <img src="<?= $image["url"] ?>" alt="<?= $image["alt"] ?>">
                                        </div>
                                    <?php } ?>
                                    <div style="display: flex;flex-direction: column;grid-row:3/4;">
                                        <?php if ($category) { ?>
                                            <div class="category-wrapper">
                                                <p class="label"><?= $category ?></p>
                                            </div>
                                        <?php } ?>
                                        <?php if ($title) { ?>
                                            <p class="post-title paragraph no-hover" aria-label="post title"><?= $title ?></p>
                                        <?php } ?>
                                        <?php if ($cta && !empty($cta)) { ?>
                                            <a href="<?= $cta['url'] ?>" target="<?= $cta['target'] ?>" class='cta-link'>
                                                <?= $cta['title'] ?>
                                                <svg class='arrow' width='22' height='11' viewBox='0 0 22 11' fill='none'
                                                     xmlns='http://www.w3.org/2000/svg'>
                                                    <path d='M15.3292 3.54817H15.4542V3.42317V1.17424C15.4542 0.24156 16.5818 -0.225521 17.2413 0.433966L21.4434 4.63607L21.5318 4.54768L21.4434 4.63607C21.8522 5.04487 21.8523 5.70771 21.4434 6.11656L17.2413 10.3187C16.5818 10.9782 15.4542 10.5111 15.4542 9.57839V7.32942V7.20442H15.3292H0.585938C0.331389 7.20442 0.125 6.99803 0.125 6.74348V4.0091C0.125 3.75455 0.331389 3.54817 0.585938 3.54817H15.3292Z'
                                                          fill='#FF5900' stroke='#FF5900' stroke-width='0.25'/>
                                                </svg>
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
        <div class="swiper-arrows-container">
            <div class="swiper-button swiper-button-prev arrow-left" aria-label="slider">
                <svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="25" cy="25" r="24.5" stroke="#FF5900"/>
                    <line x1="0.5" y1="-0.5" x2="17.9615" y2="-0.5" transform="matrix(-1 0 0 1 33.9287 25.6733)" stroke="#184485" stroke-opacity="0.8" stroke-linecap="round"/>
                    <path d="M21.2364 31.7404L15.1787 25.2452L21.2364 18.75" stroke="#184485" stroke-opacity="0.8" stroke-linecap="round"/>
                </svg>

            </div>
            <div class="swiper-button swiper-button-next arrow-right" aria-label="slider">
                <svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="25" cy="25" r="24.5" stroke="#FF5900"/>
                    <line x1="15.6787" y1="25.1733" x2="33.1403" y2="25.1733" stroke="#184485" stroke-opacity="0.8" stroke-linecap="round"/>
                    <path d="M27.871 31.7404L33.9287 25.2452L27.871 18.75" stroke="#184485" stroke-opacity="0.8" stroke-linecap="round"/>
                </svg>

            </div>
        </div>
    </div>
</div>
</section>

<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'benefits_and_perks_block';
$className = 'benefits_and_perks_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/benefits_and_perks_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field( 'title' );
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <?php if ( $title ) { ?>
    <h2 class="headline-2"><?= $title ?></h2>
  <?php } ?>
  <?php if ( have_rows( 'benefits_plus_perks' ) ) { ?>
    <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
      <?php while ( have_rows( 'benefits_plus_perks' ) ) {
        the_row();
        $image = get_sub_field( 'image' );
        $text  = get_sub_field( 'text' ); ?>
        <div class="col">
          <div class="benefits_and_perks_group box-shadow">
            <?php if ( $image ) { ?>
              <picture class="benefits_and_perks_img">
                <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
              </picture>
            <?php } ?>
            <?php if ( $text ) { ?>
              <p class="paragraph"><?= $text ?></p>
            <?php } ?>
          </div>
        </div>
      <?php } ?>
    </div>
  <?php } ?>
</div>
</section>


<!-- endregion ZineOne's Block -->

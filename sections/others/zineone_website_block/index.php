<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'website_block';
$className = 'website_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/website_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$statistics = get_field('statistics');
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">

  <div class="website-img remove-in-small-screen">
    <svg lazyloaded-svg="website-block-desktop" viewBox="0 0 1024 640"></svg>
  </div>
  <div class="website-img remove-from-medium">
    <svg lazyloaded-svg="website-block-mobile" viewBox="0 0 366 1006"></svg>
  </div>
  <?php if (have_rows('statistics')) { ?>
    <div class="website-list">
      <?php while (have_rows('statistics')) {
        the_row();
        $number = get_sub_field('number');
        $title = get_sub_field('title');
        ?>
        <div class="website-text iv-st-from-bottom">
          <h4 class="headline-3"><?=$number ?></h4>
          <p class="paragraph"><?=$title ?></p>
        </div>
      <?php } ?>
    </div>
  <?php } ?>
</div>
</section>


<!-- endregion ZineOne's Block -->

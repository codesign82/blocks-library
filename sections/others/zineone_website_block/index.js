import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import DrawSVGPlugin from "gsap/DrawSVGPlugin";
import {ScrollTrigger} from "gsap/ScrollTrigger";
import '../../../scripts/sitesSizer/zineone';
gsap.registerPlugin(DrawSVGPlugin, ScrollTrigger)


/**
 *
 * @param container {Document}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
const block = container.querySelector('.website_block');
  
  const desktopMedia = window.matchMedia('(min-width: 600px)');
  
  
  if (desktopMedia.matches) {
    
    const move = gsap.timeline({
      repeat: -1,
      repeatDelay: 2,
      scrollTrigger: {
        trigger:block,
        start:"top 50%"
      }
    })
    
  }
  
  gsap.from(block.querySelectorAll(".counter"), {
    textContent:0,
    duration:3,
    scrollTrigger: {
      trigger:block,
      start:"top 90%"
    },
    snap: { textContent: 1 }
  })
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);

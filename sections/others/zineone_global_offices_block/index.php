<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'global_offices_block';
$className = 'global_offices_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/global_offices_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title       = get_field( 'title' );
$description = get_field( 'description' );
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <?php if ( $title ) { ?>
    <h2 class="headline-2 text1"><?= $title ?></h2>
  <?php } ?>
  <?php if ( have_rows( 'global_offices' ) ) { ?>
    <div class="about-global-offices">
      <div class="row row-cols-1 row-cols-md-2 row-cols-lg-2">
        <?php while ( have_rows( 'global_offices' ) ) {
          the_row();
          $title                    = get_sub_field( 'title' );
          $subtitle                 = get_sub_field( 'subtitle' );
          $address                  = get_sub_field( 'address' );
          $address_and_border_color = get_sub_field( 'address_and_border_color' );
          $description              = get_sub_field( 'description' ); ?>
          <div class="col col-style-<?= get_row_index(); ?>">
            <div class="global-offices-head">
              <?php if ( $title ) { ?>
                <p class="paragraph title"><?= $title ?></p>
              <?php } ?>
              <?php if ( $subtitle ) { ?>
                <h2 class="headline-3 subtitle"><?= $subtitle ?></h2>
              <?php } ?>
              <?php if ( $address ) { ?>
                <div class="paragraph address"><?= $address ?></div>
              <?php } ?>
              <?php if ( $description ) { ?>
                <div class="paragraph description"><?= $description ?></div>
              <?php } ?>
            </div>
            <?php if ( $address_and_border_color ) { ?>
              <style>
                .col-style-<?= get_row_index()?> .global-offices-head {
                  border-left-color: <?=$address_and_border_color?> !important;
                }

                .col-style-<?= get_row_index()?> .address {
                  color: <?=$address_and_border_color?> !important;
                }
              </style>
            <?php } ?>
          </div>
        <?php } ?>
      </div>
    </div>
  <?php } ?>

  <?php if ( have_rows( 'global_offices_information' ) ) { ?>
    <div class="globally-offices-text">
      <?php while ( have_rows( 'global_offices_information' ) ) {
        the_row();
        $title       = get_sub_field( 'title' );
        $value       = get_sub_field( 'value' );
        $value_color = get_sub_field( 'value_color' ); ?>
        <div class="leader-office">
          <?php if ( $title ) { ?>
            <p class="paragraph"><?= $title ?></p>
          <?php } ?>
          <?php if ( $value ) { ?>
            <h3 class="headline-2 leader-office-value" <?= $value_color ? 'style="color:' . $value_color . ';"' : '' ?>><?= $value ?></h3>
          <?php } ?>
        </div>
        <?php
        if ( get_row_index() % 3 == 0 ) { ?>
          <div class="w-100"></div>
        <?php } ?>
      <?php } ?>
    </div>
  <?php } ?>
</div>
</section>


<!-- endregion ZineOne's Block -->

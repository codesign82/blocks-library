import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import $ from 'jquery';
import 'select2';
import '../../../scripts/sitesSizer/towSmart';

const blockScript = async (container = document) => {
  const block = container.querySelector('.configure_your_vehicle_block');
  
  const configureYourVehicle = block.querySelector('.configure-your-vehicle');
  const selects = block.querySelectorAll('.configure-your-vehicle-select');
  const confirmYourSelection = block.querySelector('.confirm-your-selection');
  const modalYearValue = block.querySelector('.modal-year-value');
  const vehicleBrandValue = block.querySelector('.vehicle-brand-value');
  const vehicleModelValue = block.querySelector('.vehicle-model-value');
  const resetSelectValues = block.querySelector('.reset-select-values');
  const editSelectValues = block.querySelector('.edit-select-values');
  const resultsWrapper = block.querySelector('.results-wrapper');
  const blockSpacer = block.querySelector('.block-spacer');
  const mobileMedia = window.matchMedia('(max-width: 599.98px)');
  let selectWidth = mobileMedia.matches ? {width: '110%'} : {width: '120%'};
  let selectValues = {};
  
  
  function resultToggler(show) {
    if (show) {
      gsap.to(blockSpacer, {height: 0})
      gsap.to(resultsWrapper, {height: 'auto'})
      gsap.to(configureYourVehicle, {height: 0, opacity: 0})
    } else {
      gsap.to(blockSpacer, {height: '3rem'})
      gsap.to(resultsWrapper, {height: 0})
      gsap.to(configureYourVehicle, {height: 'auto', opacity: 1})
    }
  }
  
  // region open select on parent click
  for (let selectParent of selects) {
    const selectElement = selectParent.querySelector('select');
    const clearSelect = selectParent.querySelector('.clear-select-value');
    const placeholder = selectElement.dataset.placeholder;
    $(selectElement).select2({
      placeholder: placeholder,
      // dropdownAutoWidth: true,
      // dropdownParent: $(selectParent),
      ...selectWidth,
    });
    $(selectElement).on('select2:open', function (e) {
      document.querySelector('.select2-search__field').focus();
      // click on parent if document presses tab
      selectParent.click();
      // click on parent if document presses tab
      let $select = $(this), $select2 = $select.data('select2'),
          $dropdown = $select2.dropdown.$dropdown || $select2.selection.$dropdown,
          $search = $select2.dropdown.$search || $select2.selection.$search,
          data = $select.select2('data');
      // Above dropdown
      if ($dropdown.hasClass('select2-dropdown--above')) {
        $dropdown.append($search.parents('.select2-search--dropdown').detach());
      }
      // Placeholder
      $search.attr('placeholder', (data[0].text !== '' ? data[0].text : $select.data('placeholder')));
      // focus search input
    });
    $(selectElement).on('select2:close', function (e) {
      selectParent.classList.remove('select-active');
      let prevSibling = selectParent.previousElementSibling;
      if (prevSibling) {
        prevSibling.classList.remove('select-remove-border');
      }
    });
    // $(selectElement).on('select2:focus', function (evt) {
    //   console.log('foucsed')
    //   $(this).select2('open');
    // });
    //region check if select2 value change show clear button
    $(selectElement).on('select2:select', function (e) {
      $(clearSelect).addClass('clear-select-active');
      $(selectParent).removeClass('select-active');
      selectValues[selectElement.dataset.name + 'Vehicle'] = $(selectElement).select2('data')[0].text;
      // selectValues.push($(selectElement).select2('data')[0].text);
      if (Object.keys(selectValues).length === 3) {
        $(confirmYourSelection).removeClass('btn-not-allowed').addClass('btn-allowed');
        $(confirmYourSelection).removeAttr('disabled');
      }
    });
    //endregion check if select2 value change show clear button
    //region clear select value
    $(clearSelect).on('click', function () {
      $(selectElement).val("").trigger('change');
      $(clearSelect).removeClass('clear-select-active');
      delete selectValues[selectElement.dataset.name];
      $(confirmYourSelection).addClass('btn-not-allowed').removeClass('btn-allowed');
      $(confirmYourSelection).attr('disabled', 'true');
    });
    //endregion clear select value
    // region open select on click parent and remove active from siblings
    selectParent?.addEventListener('click', (e) => {
      if (!e.target.classList.contains('confirm-your-selection')) {
        $(selectParent).addClass('select-active').siblings().removeClass('select-active');
        $(selectElement).select2('open');
        // focus search input
        document.querySelector('.select2-search__field').focus();
      }
      let prevSibling = selectParent.previousElementSibling;
      if (prevSibling) {
        prevSibling.classList.add('select-remove-border');
      }
    })
    // endregion open select on click parent and remove active from siblings
  }
  // endregion open select on parent click
  
  // region get result
  confirmYourSelection?.addEventListener('click', () => {
    modalYearValue.innerHTML = selectValues.yearVehicle;
    vehicleBrandValue.innerHTML = selectValues.makeVehicle;
    vehicleModelValue.innerHTML = selectValues.modelVehicle;
    resultToggler(true);
  })
  // endregion get result
  document.addEventListener('hitch_filter_show', () => {
    resultToggler(true);
  })
  // region reset select values
  resetSelectValues?.addEventListener('click', () => {
    $('.configure-your-vehicle-select select').val("").trigger('change');
    $('.configure-your-vehicle-select .clear-select-value').removeClass('clear-select-active');
    $(confirmYourSelection).addClass('btn-not-allowed').removeClass('btn-allowed');
    $(confirmYourSelection).attr('disabled', 'true');
    selectValues = {};
    resultToggler();
  })
  // endregion reset select values
  
  // region edit select values
  editSelectValues?.addEventListener('click', () => {
    resultToggler();
  })
  // endregion edit select values
  
  $(block).on('focus', '.select2-selection', function (e) {
    $(this).closest(".select2-container").siblings('select:enabled').select2('open');
  });
  
  //region clear select values
  if (document.readyState === 'complete') {
    $('.configure_your_vehicle_block select').val("").trigger('change');
  }
  //endregion  clear select values
  
  
  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);




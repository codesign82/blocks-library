import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {debounce} from "../../../scripts/functions/debounce";

import '../../../scripts/sitesSizer/propellerhealth';

import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger)


const blockScript = async (container = document) => {
    const blocks = container.querySelectorAll('.section_with_centered_text');

    for (let block of blocks) {

        const desktopSlopValue = 8.333334;
        const tabletSlopValue = 5.04;
        const mobileSlopValue = (parseFloat(window.getComputedStyle(block.querySelector(".container"), null).getPropertyValue('padding-left')) / window.innerWidth) * 100;
        ;
        let pathSlopValue = desktopSlopValue;
        const fixPaths = debounce(() => {
            pathSlopValue = window.innerWidth >= 992
                ? desktopSlopValue
                : window.innerWidth >= 600
                    ? tabletSlopValue
                    : mobileSlopValue;
            const path = block.querySelector('.bottom-background path')
            path.setAttribute('d', `M 0 100 L ${pathSlopValue} 0 L ${100 - pathSlopValue} 0 L 100 100 Z`);
        }, 300);

        fixPaths();
        window.addEventListener('resize', fixPaths)

        const content = block.querySelector(".content")
        if (!block.classList.contains("no-animation")) {
            gsap.from(content, {
                rotateX: 65,
                transformOrigin: "bottom",
                scrollTrigger: {
                    trigger: block,
                    scrub: .5,
                    end: "center center",
                }
            })
        }

        animations(block);
        imageLazyLoading(block);
    }
};
windowOnLoad(blockScript);



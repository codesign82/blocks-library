<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'section_with_centered_text';
$className = 'section_with_centered_text';
$orientation = get_field('orientation');
$list_or_text = get_field('list_or_text');
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if ($list_or_text) {
    $className .= ' list';
}
if ($orientation) {
    $className .= ' centered';
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/section_with_centered_text/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$text = get_field('text');
$form = get_field('form');
$cta_button = get_field('cta_button');
$has_form = get_field('has_form');
$list_or_text = get_field('list_or_text');
$list_items = get_field('list_items');
$right_items = get_field('right_items');
?>
<!-- region samrt_dv Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="content">
        <?php if ($title) { ?>
            <div class="headline-2 main-title"><?= $title ?></div>
        <?php } ?>
        <?php if ($list_or_text === 'list') {

            ?>
            <div class="list-wrapper">
                <div class="left-content">
                    <div class="paragraph description">
                        <?= $list_items['left_items'] ?>
                    </div>
                </div>
                <?php if (have_rows('list_items')) { ?>
                    <?php while (have_rows('list_items')) {
                        the_row();
                        ?>


                        <?php if (have_rows('right_items')) {

                            ?>

                            <div class="right-content ">
                                <?php while (have_rows('right_items')) {
                                    the_row();
                                    $item = get_sub_field('item');
                                    ?>

                                    <div class="paragraph description-list">
                                        <?= $item ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>


        <?php } else { ?>
            <div class="paragraph description">
                <?= $text ?>
            </div>

        <?php } ?>
        <?php if ($has_form) { ?>
            <?php echo do_shortcode('[gravityform id="' . $form . '" ajax="true" title="false" description="false"]'); ?>
        <?php } ?>
        <?php if ($cta_button) { ?>
            <a href="<?= $cta_button['url'] ?>" class="cta-link">
                <?= $cta_button['title'] ?>
                <svg width="23" height="12" viewBox="0 0 23 12" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path d="M23 6L13 0.226497V11.7735L23 6ZM0 7H14V5H0V7Z" fill="#BFD730"/>
                </svg>

            </a>
        <?php } ?>

    </div>
</div>
<div class="bottom-background">
    <svg preserveAspectRatio="none" viewBox="0 0 100 100">
        <path d="M -12.4 20 L 9.3 0 L 90.7 0 L 112.4 20 L 0 20 Z"></path>
    </svg>
</div>
</section>


<!-- endregion samrt_dv Block -->

import './index.html';
import './style.scss';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/propellerhealth';

const blockScript = async (container = document) => {
  const block = container.querySelector('.content_link_block');
  
  // add block code here
  const formBtn = block.querySelector(".cta-btn");
  if (formBtn) {
    const getInTouchModal = document.querySelector("#getInTouchModal");
    formBtn.addEventListener("click", () => {
      getInTouchModal.style.display = "block";
      preventPageScroll();
    })
  }

  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'supplier_centric_case_block';
$className = 'supplier_centric_case_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/supplier_centric_case_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region raistone's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="supplier-centric-case-group">
        <?php if ($title) { ?>
            <h2 class="headline-2 supplier-title iv-st-from-bottom"><?= $title ?></h2>
        <?php } ?>
        <?php if ($description) { ?>
            <div class=" paragraph paragraph-xl-paragraph supplier-description iv-st-from-bottom"><?= $description ?></div>
        <?php } ?>
        <?php
        $case_study = get_field('case_study');
        if ($case_study): ?>
            <?php get_template_part('template-parts/case-study', '', array('post_id' => $case_study)); ?>
        <?php endif; ?>
        <?php
        $name = get_the_title($case_study);
        $link = get_the_permalink($case_study);
        ?>

        <a href="<?= $link ?>" class="btn-arrow btn-arrow-white read-arrow iv-st-from-bottom">Read
            <?= $name ?>’s experience
            <svg width="9" height="14" viewBox="0 0 9 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M1.03906 13L7.03906 7L1.03906 1" stroke="#0A0A0A" stroke-width="2" stroke-linecap="round"
                      stroke-linejoin="round"/>
            </svg>
        </a>
    </div>
</div>
</section>


<!-- endregion raistone's Block -->

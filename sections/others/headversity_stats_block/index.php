<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'stats_block';
$className = 'stats_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/stats_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$description = get_field('description');
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class='container'>
    <div class='content'>
        <div class='card iv-st-from-bottom'>
            <?php if (have_rows('stats_repeater')) {
                while (have_rows('stats_repeater')) {
                    the_row();
                    $prefix = get_sub_field('prefix');
                    $suffix = get_sub_field('suffix');
                    $number = get_sub_field('number');
                    $description = get_sub_field('description');
                    ?>
                    <div class='card_stats '>

                        <div class='number headline-1'><?= $prefix ?><span
                                    data-number="<?= $number ?>">0</span><?= $suffix ?></div>
                        <div class='description headline-5'><?= $description ?></div>
                    </div>
                    <hr>
                <?php }
            } ?>
        </div>
    </div>
</div>
</section>


<!-- endregion headversity's Block -->

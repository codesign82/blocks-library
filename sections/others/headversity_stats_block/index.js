import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/propellerhealth';


import {gsap} from "gsap";

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.stats_block');


  // add block code here
  const numEl = block.querySelectorAll(".number span");
  gsap.fromTo(
      numEl,
      {innerHTML: 0},
      {
        innerHTML: (_, element) => +element.dataset.number,
        duration: 3,
        ease: "power2.out",
        modifiers: {
          innerHTML: (value, target) =>
              value.toFixed?.(target.dataset.toFixed ?? 0),
        },
        stagger: 0.2,
        immediateRender: true,
        scrollTrigger: {
          trigger: block
        }
      },
  )

  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


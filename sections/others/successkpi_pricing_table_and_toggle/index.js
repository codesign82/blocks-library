import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import '../../../scripts/sitesSizer/propellerhealth';
import SwiperCore, {Swiper, Pagination} from "swiper";

const blockScript = async (container = document) => {
  const block = container.querySelector('.pricing_table_and_toggle');
  
  // add block code here


  let price_month = block.querySelectorAll('.price-month');
  let price_text_month = block.querySelectorAll('.price-text-month');
  let price_minute = block.querySelectorAll('.price-minute');
  let price_text_minute = block.querySelectorAll('.price-text-minute');
  let toggle_input= block.querySelector('#switch');
  let perMonth = block.querySelector('.per-months')
  let perMinute = block.querySelector('.per-minute');

  function toggleCheckbox(){
    if (toggle_input.checked === true){

      perMinute.classList.add('per-active');
      perMonth.classList.remove('per-active');
      price_minute.forEach( per_price =>{
        per_price.style.display = "block";
      });
      price_month.forEach( per_price =>{
        per_price.style.display = "none";
      });

      price_text_minute.forEach( per_text =>{
        per_text .style.display = "block";

      });

      price_text_month.forEach( per_text =>{
        per_text .style.display = "none";
      });

    } else {

      perMonth.classList.add('per-active');
      perMinute.classList.remove('per-active');
      price_text_month.forEach( per_text =>{
        per_text .style.display = "block";
      });

      price_text_minute.forEach( per_text =>{
        per_text .style.display = "none";
      });

      price_month.forEach( per_price =>{
        per_price.style.display = "block";
      });

      price_minute.forEach( per_price =>{
        per_price.style.display = "none";
      });

    };
  }

  toggle_input.addEventListener('change', function (e){
    // console.log(toggle_input.checked)
    toggleCheckbox();

  });


  perMonth.addEventListener('click', function (e){
    perMonth.classList.add('per-active');
    perMinute.classList.remove('per-active');
    toggle_input.checked = false;
    toggleCheckbox();

  });


  perMinute.addEventListener('click', function (e){
    perMinute.classList.add('per-active');
    perMonth.classList.remove('per-active');
    toggle_input.checked = true;
    toggleCheckbox();

  });


  SwiperCore.use([Pagination]);
  let swiper = new Swiper(block.querySelector('.mySwiper'), {
    slidesPerView: 1,
    pagination: {
      el: block.querySelector(".swiper-pagination"),
      clickable: true,
    },
    breakpoints: {
      600: {
        slidesPerView: 2,
        spaceBetween: 16,
      },
    }

  });



  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



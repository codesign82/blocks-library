<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'pricing_table_and_toggle';
$className = 'pricing_table_and_toggle';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/pricing_table_and_toggle/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$table_title = get_field('table_title');
$manually_or_automatically = get_field('manually_or_automatically');
$plans = get_field('plans');
$terms = get_terms(array(
    'taxonomy' => 'features',
    'hide_empty' => true,
));

?>
<!-- region successkpi's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="price-plans">
        <div class="choose-plan">
            <div class="small-title per-months per-active"><?= __('Per Month ', 'successkpi') ?></div>
            <div class="switch-container">
                <input type="checkbox" id="switch">
                <label for="switch" class="switch-label">
                    <div class="switch-rail">
                        <div class="switch-slider"></div>
                    </div>
                </label>
            </div>
            <div class="small-title per-minute"><?= __('Per Minute ', 'successkpi') ?></div>
        </div>
        <?php if ($manually_or_automatically) { ?>

            <div class="row row-cols-1 row-cols-md-2 row-cols-lg-4 iv-st-from-bottom">
                <?php if ($plans) { ?>
                    <?php foreach ($plans as $post) {
                        setup_postdata($post);
                        get_template_part("template-parts/plan-card", '', array(
                            'post_id' => $post,
                        ));
                    } ?>
                <?php } ?>
                <?php wp_reset_postdata(); ?>
            </div>
        <?php } else {
            $query_options = get_field('query_options');
            $number_of_posts = @$query_options['number_of_posts'];
            $post_type = 'plans';
            $order = @$query_options['order'];
            $args = array(
                'post_type' => 'plans',
                'posts_per_page' => $number_of_posts,
                'order' => $order,
                'post_status' => 'publish',
            );
            $the_query = new WP_Query($args); ?>
            <div class="row row-cols-md-2 row-cols-lg-4 iv-st-from-bottom">
                <?php
                // The Query
                // The Loop
                if ($the_query->have_posts()) { ?>
                    <?php while ($the_query->have_posts()) {
                        $the_query->the_post();
                        get_template_part('template-parts/plan-card', '',
                            array('post_id' => get_the_id()));
                        ?>
                    <?php } ?>
                <?php }
                wp_reset_postdata(); ?>
            </div>
        <?php } ?>


        <div class="categories-table iv-st-from-bottom">
            <?php if ($table_title) { ?>
                <div class="headline-2 table-title iv-st-from-bottom"><?= $table_title ?></div>
            <?php } ?>

            <table class="cat-table">
                <?php if ($manually_or_automatically) {
                    if ($plans) { ?>
                        <tr class="iv-st-from-bottom">
                            <td>&nbsp;</td>
                            <?php foreach ($plans as $post) {
                                $title = get_the_title($post);
                                $price = get_field('price', $post);
                                $price_for_minute = get_field('price_for_minute' , $post);
                                $price_t = get_field('price_transcription', $post);
                                $add_plus_transcription = get_field('add_plus_transcription', $post);
                                ?>
                                <th>
                                    <div class="small-title th-title "><?= $title ?></div>
                                    <div class="head-text price-text-month"><?= $price . ' ' . __('per agent/month', 'successkpi') ?></div>
                                    <div class="head-text price-text-minute"><?= $price_for_minute . ' ' . __('per agent/minute', 'successkpi') ?></div>
                                    <?php if($add_plus_transcription){ ?>
                                        <div  class="head-text-2"><?= __("Plus $$price_t/minute of transcription",'successkpi') ?></div>
                                    <?php } ?>
                                </th>
                            <?php } ?> </tr> <?php }
                } else { ?>
                    <?php
                    $query_options = get_field('query_options');
                    $number_of_posts = @$query_options['number_of_posts'];
                    $post_type = 'plans';
                    $order = @$query_options['order'];
                    $args = array(
                        'post_type' => $post_type,
                        'posts_per_page' => $number_of_posts,
                        'order' => $order,
                        'post_status' => 'publish',
                    );
                    $the_query = new WP_Query($args);
                    // The Loop
                    $have_posts = $the_query->have_posts();
                    if ($have_posts) { ?>
                        <tr class="iv-st-from-bottom">
                            <td>&nbsp;</td>
                            <?php while ($the_query->have_posts()) {
                                $the_query->the_post();
                                $post_id = get_the_ID();
                                $title = get_the_title($post_id);
                                $price = get_field('price', $post_id);
                                $price_for_minute = get_field('price_for_minute' , $post_id);
                                $price_t = get_field('price_transcription', $post_id);
                                $add_plus_transcription = get_field('add_plus_transcription', $post_id);
                                ?>
                                <th>
                                    <div class="small-title th-title"><?= $title ?></div>
                                    <div class="head-text price-text-month"><?= $price . ' ' . __('per agent/month', 'successkpi') ?></div>
                                    <div class="head-text price-text-minute"><?= $price_for_minute . ' ' . __('per agent/minute', 'successkpi') ?></div>
                                    <?php if($add_plus_transcription){ ?>
                                        <div class="head-text-2"><?= __("Plus $$price_t/minute of transcription",'successkpi') ?></div>
                                    <?php } ?>
                                </th>
                            <?php } ?>
                        </tr>
                    <?php }
                } ?>


                <?php foreach ($terms as $term) { ?>
                    <tr class="iv-st-from-bottom">
                        <th class="left-head"><?= $term->name ?></th>
                        <?php if ($manually_or_automatically) {
                            //printf('Plus $%f/minute of transcription', $t_p)
                            if ($plans) {
                                foreach ($plans as $post) { ?>
                                    <td>
                                        <?php if (has_term($term, 'features', $post)) { ?>
                                            <svg class="true-svg" width="14" height="11"
                                                 viewBox="0 0 14 11" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M14 1.5C14 1.78125 13.875 2.03125 13.6875 2.21875L5.6875 10.2188C5.5 10.4062 5.25 10.5 5 10.5C4.71875 10.5 4.46875 10.4062 4.28125 10.2188L0.28125 6.21875C0.09375 6.03125 0 5.78125 0 5.5C0 4.9375 0.4375 4.5 1 4.5C1.25 4.5 1.5 4.625 1.6875 4.8125L5 8.09375L12.2812 0.8125C12.4688 0.625 12.7188 0.5 13 0.5C13.5312 0.5 14 0.9375 14 1.5Z"
                                                        fill="#0D4CCD"/>
                                            </svg>
                                        <?php } else { ?>
                                            <svg class="false-svg" width="21" height="4"
                                                 viewBox="0 0 21 4"
                                                 fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M18.75 3.5H2.25C1.40625 3.5 0.75 2.84375 0.75 2C0.75 1.20312 1.40625 0.5 2.25 0.5H18.75C19.5469 0.5 20.25 1.20312 20.25 2C20.25 2.84375 19.5469 3.5 18.75 3.5Z"
                                                        fill="#555555"/>
                                            </svg>
                                        <?php } ?>
                                    </td>
                                <?php }
                            }
                        } else { ?>
                            <?php
                            $query_options = get_field('query_options');
                            $number_of_posts = @$query_options['number_of_posts'];
                            $post_type = 'plans';
                            $order = @$query_options['order'];
                            $args = array(
                                'post_type' => $post_type,
                                'posts_per_page' => $number_of_posts,
                                'order' => $order,
                                'post_status' => 'publish',
                            );
                            $the_query = new WP_Query($args);
                            // The Loop
                            $have_posts = $the_query->have_posts();
                            if ($have_posts) {
                                while ($the_query->have_posts()) {
                                    $the_query->the_post();
                                    $post_id = get_the_ID();
                                    ?>
                                    <td>
                                        <?php if (has_term($term, 'features')) { ?>
                                            <svg class="true-svg" width="14" height="11"
                                                 viewBox="0 0 14 11" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M14 1.5C14 1.78125 13.875 2.03125 13.6875 2.21875L5.6875 10.2188C5.5 10.4062 5.25 10.5 5 10.5C4.71875 10.5 4.46875 10.4062 4.28125 10.2188L0.28125 6.21875C0.09375 6.03125 0 5.78125 0 5.5C0 4.9375 0.4375 4.5 1 4.5C1.25 4.5 1.5 4.625 1.6875 4.8125L5 8.09375L12.2812 0.8125C12.4688 0.625 12.7188 0.5 13 0.5C13.5312 0.5 14 0.9375 14 1.5Z"
                                                        fill="#0D4CCD"/>
                                            </svg>
                                        <?php } else { ?>
                                            <svg class="false-svg" width="21" height="4"
                                                 viewBox="0 0 21 4"
                                                 fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M18.75 3.5H2.25C1.40625 3.5 0.75 2.84375 0.75 2C0.75 1.20312 1.40625 0.5 2.25 0.5H18.75C19.5469 0.5 20.25 1.20312 20.25 2C20.25 2.84375 19.5469 3.5 18.75 3.5Z"
                                                        fill="#555555"/>
                                            </svg>
                                        <?php } ?>
                                    </td>
                                <?php }
                            } ?>
                        <?php } ?>
                    </tr>
                <?php } ?>

            </table>

            <div class="swiper mySwiper">
                <?php if ($manually_or_automatically) {
                    if ($plans) { ?>
                        <div class="swiper-wrapper iv-st-from-bottom">
                            <?php foreach ($plans as $post) {
                                $title = get_the_title($post);
                                $price = get_field('price', $post);
                                $price_for_minute = get_field('price_for_minute' , $post);
                                $price_t = get_field('price_transcription', $post);
                                $add_plus_transcription = get_field('add_plus_transcription', $post);
                                ?>
                                <div class="swiper-slide">
                                    <div class="small-title th-title "><?= $title ?></div>
                                    <div class="text-wrapper">
                                        <div class="head-text price-text-month"><?= $price . ' ' . __('per agent/month', 'successkpi') ?></div>
                                        <div class="head-text price-text-minute"><?= $price_for_minute . ' ' . __('per agent/minute', 'successkpi') ?></div>
                                        <?php  if($add_plus_transcription){?>
                                            <div  class="head-text-2"><?=  __("Plus $$price_t/minute of transcription",'successkpi') ?></div>
                                        <?php } ?>
                                    </div>
                                    <?php foreach ($terms as $term) { ?>
                                        <div class="dodo">
                                            <div class="left-head"><?= $term->name ?></div>
                                            <?php if (has_term($term, 'features', $post)) { ?>
                                                <svg class="true-svg" width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M21.5 2C21.5 2.42188 21.3125 2.79688 21.0312 3.07812L9.03125 15.0781C8.75 15.3594 8.375 15.5 8 15.5C7.57812 15.5 7.20312 15.3594 6.92188 15.0781L0.921875 9.07812C0.640625 8.79688 0.5 8.42188 0.5 8C0.5 7.15625 1.15625 6.5 2 6.5C2.375 6.5 2.75 6.6875 3.03125 6.96875L8 11.8906L18.9219 0.96875C19.2031 0.6875 19.5781 0.5 20 0.5C20.7969 0.5 21.5 1.15625 21.5 2Z" fill="#0D4CCD"/>
                                                </svg>
                                            <?php } else { ?>
                                                <svg class="false-svg" width="21" height="4"
                                                     viewBox="0 0 21 4"
                                                     fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                            d="M18.75 3.5H2.25C1.40625 3.5 0.75 2.84375 0.75 2C0.75 1.20312 1.40625 0.5 2.25 0.5H18.75C19.5469 0.5 20.25 1.20312 20.25 2C20.25 2.84375 19.5469 3.5 18.75 3.5Z"
                                                            fill="#555555"/>
                                                </svg>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?> </div>
                        <div class="swiper-pagination"></div>
                    <?php }} else { ?>
                    <?php
                    $query_options = get_field('query_options');
                    $number_of_posts = @$query_options['number_of_posts'];
                    $post_type = 'plans';
                    $order = @$query_options['order'];
                    $args = array(
                        'post_type' => $post_type,
                        'posts_per_page' => $number_of_posts,
                        'order' => $order,
                        'post_status' => 'publish',
                    );
                    $the_query = new WP_Query($args);
                    // The Loop
                    $have_posts = $the_query->have_posts();
                    if ($have_posts) { ?>
                        <div class="swiper-wrapper iv-st-from-bottom">

                            <?php while ($the_query->have_posts()) {
                                $the_query->the_post();
                                $post_id = get_the_ID();
                                $title = get_the_title($post_id);
                                $price = get_field('price', $post_id);
                                $price_for_minute = get_field('price_for_minute' , $post_id);
                                $price_t = get_field('price_transcription', $post_id);
                                $add_plus_transcription =get_field('add_plus_transcription', $post_id);
                                ?>
                                <div class="swiper-slide">
                                    <div class="small-title th-title"><?= $title ?></div>
                                    <div class="text-wrapper">
                                        <div class="head-text price-text-month"><?= $price . ' ' . __('per agent/month', 'successkpi') ?></div>
                                        <div class="head-text price-text-minute"><?= $price_for_minute . ' ' . __('per agent/minute', 'successkpi') ?></div>
                                        <?php  if($add_plus_transcription){?>
                                            <div class="head-text-2"><?=  __("Plus $$price_t/minute of transcription",'successkpi') ?></div>
                                        <?php }?>
                                    </div>
                                    <?php foreach ($terms as $term) { ?>
                                        <div class="dodo">
                                            <div class="left-head"><?= $term->name ?></div>
                                            <?php if (has_term($term, 'features', $post)) { ?>
                                                <svg class="true-svg" width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M21.5 2C21.5 2.42188 21.3125 2.79688 21.0312 3.07812L9.03125 15.0781C8.75 15.3594 8.375 15.5 8 15.5C7.57812 15.5 7.20312 15.3594 6.92188 15.0781L0.921875 9.07812C0.640625 8.79688 0.5 8.42188 0.5 8C0.5 7.15625 1.15625 6.5 2 6.5C2.375 6.5 2.75 6.6875 3.03125 6.96875L8 11.8906L18.9219 0.96875C19.2031 0.6875 19.5781 0.5 20 0.5C20.7969 0.5 21.5 1.15625 21.5 2Z" fill="#0D4CCD"/>
                                                </svg>
                                            <?php } else { ?>
                                                <svg class="false-svg" width="21" height="4"
                                                     viewBox="0 0 21 4"
                                                     fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                            d="M18.75 3.5H2.25C1.40625 3.5 0.75 2.84375 0.75 2C0.75 1.20312 1.40625 0.5 2.25 0.5H18.75C19.5469 0.5 20.25 1.20312 20.25 2C20.25 2.84375 19.5469 3.5 18.75 3.5Z"
                                                            fill="#555555"/>
                                                </svg>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="swiper-pagination"></div>
                    <?php }
                } ?>
            </div>


        </div>
    </div>
</div>
</section>


<!-- endregion successkpi's Block -->

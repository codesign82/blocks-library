import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {numberWithCommas} from "../../../scripts/functions/numberWithCommas";
import '../../../scripts/sitesSizer/wrap'

const blockScript = async (container = document) => {
  
  const blocks = container.querySelectorAll('.credit_score_calculator_block');
  
  for (let block of blocks) {
    const result = block.querySelector('.rent-result .total-amount span.value')
    result.amount = 0;
    const comma = block.querySelector('.comma');
    comma.textContent = numberWithCommas(+comma.textContent);
  
  
    const subCTAText = block.querySelector('.tiny-text')
    const thresholds = JSON.parse(subCTAText.dataset.thresholds).sort((a, b) => b.number - a.number)
    const updateSubCTAText = () => {
      for (const threshold of thresholds) {
        if (result.amount >= threshold.number) {
          subCTAText.textContent = threshold.text;
          break;
        }
      }
    }
  
  
    const rentCalculator = {el: block.querySelector('.rent-calculator')}
    rentCalculator.value = rentCalculator.el.querySelector('span.value');
    rentCalculator.input = rentCalculator.el.querySelector('input');
    rentCalculator.slider = rentCalculator.el.querySelector('.progress');
    rentCalculator.inputCallback = () => {
      const width = (rentCalculator.input.value - rentCalculator.input.min) * 100 / (rentCalculator.input.max - rentCalculator.input.min)
      rentCalculator.slider.style.width = `${width}%`
      rentCalculator.value.textContent = numberWithCommas(rentCalculator.input.value);
      result.amount = rentCalculator.input.value * monthsInUnit.input.value;
      result.textContent = numberWithCommas(result.amount)
      updateSubCTAText();
    };
  
    const monthsInUnit = {el: block.querySelector('.months-in-unit')}
    monthsInUnit.value = monthsInUnit.el.querySelector('span.value');
    monthsInUnit.input = monthsInUnit.el.querySelector('input');
    monthsInUnit.slider = monthsInUnit.el.querySelector('.progress');
    monthsInUnit.inputCallback = () => {
      const width = (monthsInUnit.input.value - monthsInUnit.input.min) * 100 / (monthsInUnit.input.max - monthsInUnit.input.min)
      monthsInUnit.slider.style.width = `${width}%`
      monthsInUnit.value.textContent = monthsInUnit.input.value;
      result.amount = rentCalculator.input.value * monthsInUnit.input.value;
      result.textContent = numberWithCommas(result.amount)
      updateSubCTAText();
    };
  
  
    rentCalculator.inputCallback();
    monthsInUnit.inputCallback();
    rentCalculator.input.addEventListener('input', rentCalculator.inputCallback)
    monthsInUnit.input.addEventListener('input', monthsInUnit.inputCallback)
  
    animations(block);
    imageLazyLoading(block);
  }
};
windowOnLoad(blockScript);




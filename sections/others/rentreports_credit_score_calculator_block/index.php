<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'credit_score_calculator_block';
$className = 'credit_score_calculator_block';
if ( ! empty( $block['className'] ) ) {
	$className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
	$className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
	/* Render screenshot for example */
	echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/credit_score_calculator_block/screenshot.png" >';
	
	return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title          = get_field( 'title' );
$description    = get_field( 'description' );
$rent_amount    = get_field( 'rent_amount' );
$months_in_unit = get_field( 'months_in_unit' );
$info_box       = get_field( 'info_box' );
$thresholds     = get_field( 'thresholds' );
$thresholds[]   = [ 'number' => 0, 'text' => $info_box['bottom_text'] ];
?>
<!-- region RentReporters's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <div class="credit-score-wrapper">
    <div class="rent-calculator">
      <h2 class="headline-1 iv-st-from-bottom-f"><?= $title ?></h2>
      <div class="paragraph description iv-st-from-bottom-f"><?= $description ?></div>
      <div class="rent-amount rent-calc iv-st-from-bottom-f">
        <h6 class="paragraph size16 rent-calc-title"><?= $rent_amount['title'] ?></h6>
        <h3 class="headline-3"><?= $rent_amount['prefix'] ?>
          <span class="value"><?= $rent_amount['minimum_number'] ?></span>/<?= $rent_amount['suffix'] ?></h3>
        <div class="calculator">
          <div class="progress-line"></div>
          <div class="progress">
            <div class="progress-circle"></div>
          </div>
          <input type="range" min="<?= $rent_amount['minimum_number'] ?>" max="<?= $rent_amount['maximum_number'] ?>" step="1" value="<?= $rent_amount['minimum_number'] ?>">
          <div class="rent-range">
            <h4 class="headline-4"><?= $rent_amount['prefix'] ?><?= $rent_amount['minimum_number'] ?></h4>
            <h4 class="headline-4">
				<?= $rent_amount['prefix'] ?><span class="comma"><?= $rent_amount['maximum_number'] ?></span>
            </h4>
          </div>
        </div>
      </div>
      <div class="months-in-unit rent-calc iv-st-from-bottom-f">
        <h6 class="paragraph size16 rent-calc-title"><?= $months_in_unit['title'] ?></h6>
        <h3 class="headline-3">
          <span class="value"><?= $months_in_unit['minimum_number'] ?></span> <?= $months_in_unit['suffix'] ?></h3>
        <div class="calculator">
          <div class="progress-line"></div>
          <div class="progress">
            <div class="progress-circle"></div>
          </div>
          <input type="range" min="<?= $months_in_unit['minimum_number'] * 12 ?>" max="<?= $months_in_unit['maximum_number'] * 12 ?>" step="1" value="<?= $months_in_unit['minimum_number'] * 12 ?>">
          <div class="rent-range">
            <h4 class="headline-4"><?= $months_in_unit['minimum_number'] ?> <?= __( 'years', 'rent-reporters-theme' ) ?></h4>
            <h4 class="headline-4"><?= $months_in_unit['maximum_number'] ?> <?= __( 'years', 'rent-reporters-theme' ) ?></h4>
          </div>
        </div>
      </div>
    </div>
    <div class="rent-result">
      <div class="rent-result-card card-border iv-st-from-bottom-f bottom-border">
        <h5 class="rent-result-title"><?= $info_box['title'] ?></h5>
        <h4 class="headline-2 total-amount"><?= $rent_amount['prefix'] ?>
          <span class="value"><?= $rent_amount['minimum_number'] ?></span></h4>
        <p class="paragraph size16"><?= $info_box['sub_title'] ?></p>
		  <?php if ( $info_box['link'] ) { ?>
            <a href="<?= $info_box['link']['url'] ?>" class="link has-arrow" target="<?= $info_box['link']['target'] ?>">
				<?= $info_box['link']['title'] ?>
              <svg class="arrow" width="17" height="11" viewBox="0 0 17 11">
                <path fill="currentColor"
                      d="M10.761.325l-.703.704a.437.437 0 0 0 .035.597l2.813 2.707H.812a.427.427 0 0 0-.422.422v.985c0 .246.176.421.422.421h12.094l-2.813 2.743a.437.437 0 0 0-.035.597l.703.703c.176.141.422.141.598 0l4.64-4.64a.476.476 0 0 0 0-.598L11.36.326a.476.476 0 0 0-.598 0z"/>
              </svg>
            </a>
		  <?php } ?>
      </div>
      <h6 data-thresholds='<?= json_encode( $thresholds ) ?>' class="tiny-text">
		  <?= $info_box['bottom_text'] ?>
      </h6>
    </div>
  </div>
</div>
</section>


<!-- endregion RentReporters's Block -->

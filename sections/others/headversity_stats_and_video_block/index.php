<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'stats_and_video_block';
$className = 'stats_and_video_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/stats_and_video_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$text_section = get_field('text_section');
$card = get_field('card');
$video = get_field('video');
// Full Video
$video_type = @$video['video_type'];
$video_url = @$video['video_url'];
$video_file = @$video['video_file'];
$video_youtube = @$video['youtube_url'];
$video_final = '';
if ($video_type === 'youtube') {
    $video_final = $video_youtube;
}
// DO Not Delete Video Type For JS
$video_type_final = $video_type === 'youtube' ? 'youtube' : 'video';
$poster_video_image = get_field('poster_video_image');
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<?php if ($video) { ?>
    <div class="modal" aria-modal="true" role="dialog">
        <div class="modal-content" aria-modal="true" role="dialog">
            <div class="modal-video aspect-ratio">
                <?php if ($video_type === 'youtube') {
                    ?>
                    <div class="the-video" id="youtube-video"
                         data-src="<?= generateVideoEmbedUrl($video_youtube) ?>"></div>
                    <?php
                } else {
                    ?>
                    <video class="video" data-src="<?= $video_type === 'url' ? $video_url : $video_file ?>"
                           type="video/mp4" playsinline controls></video>
                    <?php
                } ?>
            </div>
        </div>
        <svg class="exit" width="39" height="39" viewBox="0 0 39 39" fill="none"
             xmlns="http://www.w3.org/2000/svg" role="application">
            <line x1="9.90039" y1="28.2841" x2="28.2852" y2="9.8993"
                  stroke="#FF9900" stroke-width="3" stroke-linecap="round"/>
            <path d="M28.9922 28.2841L10.6074 9.89928" stroke="#FF9900"
                  stroke-width="3" stroke-linecap="round"/>
        </svg>

    </div>
<?php } ?>
<div class="block-content">
    <div class="container ">
        <?php if ($text_section) { ?>
            <div class="text-section">
                <?php if ($text_section['title']) { ?>
                    <h2 move-to-here class="title headline-3 iv-st-from-bottom"><?= $text_section['title'] ?></h2>
                <?php } ?>
                <?php if ($text_section['description']) { ?>
                    <div
                            class="description paragraph paragraph-m-paragraph iv-st-from-bottom">
                        <?= $text_section['description'] ?>
                    </div>
                <?php } ?>
                <?php if ($text_section['cta_button']) { ?>
                    <div class="iv-st-from-bottom">
                        <a class="btn"
                           href="<?= $text_section['cta_button']["url"] ?>"><?= $text_section['cta_button']["title"] ?></a>
                    </div>
                <?php } ?>

            </div>
        <?php } ?>
    </div>
    <?php if ($poster_video_image) { ?>
        <div class="video-section">
            <div class="right-video">
                <div class="video-wrapper aspect-ratio" data-video-type="<?= $video_type ?>">
                    <?php if ($video_type === 'youtube') {
                        ?>
                        <div class="the-video" id="right-youtube-video"
                             data-src="<?= generateVideoEmbedUrl($video_youtube) ?>"></div>
                        <?php
                    } else {
                        ?>
                        <video class="video" muted data-src="<?= $video_type === 'url' ? $video_url : $video_file ?>"
                               type="video/mp4" playsinline></video>
                        <?php
                    } ?>
                </div>
            </div>

            <div class="video-wrapper aspect-ratio" tabindex="0" data-video-type="<?= $video_type_final ?>"
                 aria-label="<?= __('open video', 'headversity') ?>" role="button">
                <picture>
                    <?= get_acf_image($poster_video_image, 'img-613-384') ?>
                </picture>
            </div>

            <button class="player-icon-wrapper" aria-label="<?= __('play video', 'headversity') ?>">
                <svg class="player-icon" width="66" height="68" viewBox="0 0 66 68"
                     fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path d="M62.418 31.2094L15.6039 58.2375V4.18127L62.418 31.2094Z"
                          fill="#FF9900"/>
                    <path d="M20.1859 11.0295L64 36.3256L20.1859 61.6217V11.0295Z"
                          stroke="#0A1B57" stroke-width="2"/>
                </svg>
            </button>
        </div>
    <?php } ?>
    <!--      region percentage card-->
    <?php if (have_rows('card')) { ?>
        <div class="percentage-card iv-st-from-bottom">
            <?php while (have_rows('card')) {
                the_row();
                $percentage_value = get_sub_field('percentage_value');
                $percentage_name = get_sub_field('percentage_name');
                $prefix = get_sub_field('prefix');
                $suffix = get_sub_field('suffix');
                ?>
                <div class="rate-field iv-st-from-bottom">
                    <?php if ($percentage_value) { ?>
                        <div class="percentage-value headline-1">
                            <?= $prefix ?><span
                                    data-number='<?= $percentage_value ?>'>0</span><?= $suffix ?>
                        </div>
                    <?php } ?>
                    <?php if ($percentage_name) { ?>
                        <div
                                class="percentage-name headline-5  iv-st-from-bottom"><?= $percentage_name ?></div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
    <!--      endregion percentage card-->

</div>
</section>


<!-- endregion headversity's Block -->

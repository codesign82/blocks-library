import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/propellerhealth';

import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";
import {youtubePlayer} from "../../../scripts/general/youtube-player";

gsap.registerPlugin(ScrollTrigger)

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.stats_and_video_block');


  const videoWrapper = block.querySelector(".video-wrapper");
  const video = block.querySelector(".modal-video video");
  const modal = block.querySelector(".modal");
  const exit = block.querySelector(".exit");
  const videoSection = block.querySelector(".video-section");
  const rightVideo = block.querySelector(".right-video");
  const rightVideoEl = block.querySelector(".right-video video");
  const videoType = videoWrapper.dataset.videoType;

  let modalYoutubePlayer, rightYoutubePlayer;
  const modalYoutubeElement = block.querySelector("#youtube-video");
  const rightYoutubeElement = block.querySelector("#right-youtube-video");

  if (videoType === "youtube") {
    modalYoutubePlayer = youtubePlayer(modalYoutubeElement, modalYoutubeElement.dataset.src);
    rightYoutubePlayer = youtubePlayer(
        rightYoutubeElement,
        rightYoutubeElement.dataset.src,
        {controls: false}
    );
    rightYoutubePlayer.mute();
  }

  const playVideo = (videoEl, ytEl) => {
    if (videoType === "youtube") {
      ytEl.play();
    } else {
      videoEl.play();
    }
  }

  const pauseVide = (videoEl, ytEl) => {
    if (videoType === "youtube") {
      ytEl.pause();
      ytEl.seek(0)
    } else {
      videoEl.pause();
      videoEl.currentTime = 0;
    }
  }

  videoSection.addEventListener("mouseenter", () => {
    rightVideo.classList.add("active");
    playVideo(rightVideoEl, rightYoutubePlayer);
  })

  videoSection.addEventListener("mouseleave", () => {
    rightVideo.classList.remove("active");
    pauseVide(rightVideoEl, rightYoutubePlayer);
  })


  videoWrapper?.addEventListener("click", (e) => {
    modal.classList.add("show");
    playVideo(video, modalYoutubePlayer)
    document.documentElement.classList.add('modal-opened');
  });

  exit?.addEventListener("click", hide);
  modal?.addEventListener("click", hide);

  window.addEventListener('keydown', function (event) {
    if (event.key === 'Escape') {
      hide(event);
    }
  })

  function hide(e) {
    if (e.target.closest('.modal-content') && !e.target.closest('.exit')) return;
    modal.classList.remove("show");
    pauseVide(video, modalYoutubePlayer);
    document.documentElement.classList.remove('modal-opened');
  }

  const numEl = block.querySelectorAll(".percentage-value span");
  ScrollTrigger.batch(numEl, {
    onEnter: batch => {
      gsap.fromTo(batch, {innerHTML: 0}
          , {
            innerHTML: (_, element) => +element.dataset.number,
            duration: 3,
            ease: "power2.out",
            modifiers: {
              innerHTML: (value, target) =>
                  value.toFixed?.(target.dataset.toFixed ?? 0),
            },
            stagger: 0.2,
            immediateRender: true,
            clearProps: 'transform',
          });
    },
    start: 'top 80%',
    once: true,
  });


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


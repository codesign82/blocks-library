import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";
import '../../../scripts/sitesSizer/propellerhealth';


gsap.registerPlugin(ScrollTrigger)


const blockScript = async (container = document) => {
  const block = container.querySelector('.content_and_list_columns');
  
  // add block code here
  const list = block.querySelectorAll(".list-item")
  gsap.set(list, {
    opacity: 0,
    yPercent: 50
  })

  for (let i = 0; i < list.length; i++) {
    const tl = gsap.timeline({
      scrollTrigger: {
        trigger: list[i],
        start: "0% 70%"
      }
    })
    tl.to(list[i], {
      opacity: 1,
      yPercent: 0,
    })
    tl.call(() => {
      list[i].classList.add('active')
    },[],"<50%")
  }
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);



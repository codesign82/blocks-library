<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'content_and_list_columns';
$className = 'content_and_list_columns';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/content_and_list_columns/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$main_description = get_field('main_description');
$content_type = get_field('content_type');
$content_description = get_field('description');
$cta_link = get_field('cta_link');


?>
<!-- region propellerhealth Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class='container'>
    <div class='main-content'>
        <div class='left-content'>
            <?php if ($main_description) { ?>
                <div class='main-desc   headline-3 iv-st-from-bottom '>
                    <?=$main_description?>
                </div>
            <?php } ?>
            <?php if ($cta_link) { ?>
                <a role="button" class="cta-button iv-st-from-bottom"
                   target="<?= $cta_link['target'] ?>"
                   href='<?= $cta_link['url'] ?>'><?= $cta_link['title'] ?></a>
            <?php } ?>
        </div>
        <div class='right-content'>
            <?php if ($content_type === 'text_list') { ?>
                <?php if (have_rows('text_list')) { ?>
                    <div class='list'>
                        <?php while (have_rows('text_list')) {
                            the_row();
                            $list_item = get_sub_field('list_item');
                            ?>
                            <div class='list-item headline-4 fw-bold  '>
                                <?=$list_item?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <div class='description paragraph wysiwyg-block iv-st-from-bottom '>
                    <?=$content_description?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
</section>


<!-- endregion propellerhealth Block -->

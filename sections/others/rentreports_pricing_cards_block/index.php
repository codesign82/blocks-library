<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'pricing_cards_block';
$className = 'pricing_cards_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/pricing_cards_block/screenshot.png" >';
  
  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$pricing_title = get_field('pricing_title');
$pricing_description = get_field('pricing_description');
$choose_plan_title = get_field('choose_plan_title');
?>
<!-- region RentReporters's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="pricing-cards-wrapper">
    <div class="pricing-cards-title">
      <?php if ($pricing_title) { ?>
          <h2 class="headline-1 iv-st-from-bottom-f"><?= $pricing_title ?></h2>
      <?php } ?>
      <?php if ($pricing_description) { ?>
          <p class="paragraph iv-st-from-bottom-f"><?= $pricing_description ?></p>
      <?php } ?>
    </div>
    <div class="choose-your-plan">
      <?php if ($choose_plan_title) { ?>
        <h6 class="choose-play-title iv-st-from-bottom-f"><?= $choose_plan_title ?></h6>
      <?php } ?>
      <div class="row row-cols-1 row-cols-md-2">
        <?php
        if (have_rows('pricing_cards')) {
          while (have_rows('pricing_cards')) {
            the_row();
            $is_best_value_card = get_sub_field('is_best_value_card');
            $is_best_value_card = $is_best_value_card ? 'best-pricing' : '';
            $color = get_sub_field('color');
            $title = get_sub_field('title');
            $price = get_sub_field('price');
            $sub_price = get_sub_field('sub_price');
            $cta_button = get_sub_field('cta_button');
            $view_items_switcher_mobile = get_sub_field('view_items_switcher_mobile');
            ?>
            <div class="col">
              <div class="pricing-card box-shadow iv-st-from-bottom <?= $is_best_value_card ?>"
                  <?php if ($color) { ?> style="color: <?= $color ?>" <?php } ?> >
                    <div class="best-value iv-st-from-top-f"><span><?= __( 'best value', 'rent-reporters-theme' ) ?></span></div>
                <?php if ($title) { ?>
                        <h2 class="headline-2 title iv-st-from-top-f"><?= $title ?></h2>
                <?php } ?>
                <?php if ($price) { ?>
                        <h5 class="price headline-5 iv-st-from-top-f"><?= $price ?></h5>
                <?php } ?>
                <?php if ($sub_price) { ?>
                        <p class="paragraph size16 sub-price iv-st-from-top-f"><?= $sub_price ?></p>
                <?php } ?>
                <?php if ($cta_button) { ?>
                        <a href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>" class="btn enroll-now iv-st-from-top-f"><?= $cta_button['title'] ?></a>
                <?php } ?>
                <?php if ($view_items_switcher_mobile) { ?>
                        <div class="view-features iv-st-from-top-f"><?= $view_items_switcher_mobile ?></div>
                <?php } ?>
                <div class="items">
                  <?php
                  if (have_rows('items')) {
                    while (have_rows('items')) {
                      the_row();
                      $item_title = get_sub_field('item_title');
                      $item_description = get_sub_field('item_description');
                      ?>
                              <div class="item iv-st-from-bottom-f">
                        <?php if ($item_title) { ?>
                                    <h6 class="item-title iv-st-from-bottom-f"><?= $item_title ?></h6>
                        <?php } ?>
                        <?php if ($item_description) { ?>
                          <div class="item-description">
                            <div class="tooltip"><?= $item_description ?></div>
                            <svg width="21" height="21" viewBox="0 0 21 21">
                              <path fill="#82b378"
                                    d="M20.674 10.122c0 5.619-4.594 10.172-10.172 10.172A10.17 10.17 0 0 1 .33 10.122C.33 4.544 4.883-.05 10.502-.05c5.578 0 10.172 4.594 10.172 10.172zM8.779 6.184c0 .985.739 1.723 1.723 1.723.943 0 1.723-.738 1.723-1.723 0-.943-.78-1.722-1.723-1.722-.984 0-1.723.779-1.723 1.722zm4.02 7.711c0-.246-.246-.492-.492-.492h-.493V9.302c0-.247-.246-.493-.492-.493H8.697a.499.499 0 0 0-.492.493v.984c0 .287.205.492.492.492h.492v2.625h-.492a.499.499 0 0 0-.492.492v.985c0 .287.205.492.492.492h3.61a.499.499 0 0 0 .492-.492z"/>
                            </svg>
                          </div>
                        <?php } ?>
                      </div>
                      <?php
                    }
                  }
                  ?>
                </div>
              </div>
            </div>
            <?php
          }
        }
        ?>
      </div>
    </div>
  </div>
</div>
</section>

<!-- endregion RentReporters's Block -->

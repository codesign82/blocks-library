import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {allowPageScroll, preventPageScroll} from "../../../scripts/functions/prevent_allowPageScroll";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/wrap'

const blockScript = async (container = document) => {
  
  const blocks = container.querySelectorAll('.pricing_cards_block');
  for (let block of blocks) {
    const features = block.querySelectorAll('.view-features');
    features.forEach((feature) => {
      const featureBody = feature.parentElement.querySelector('.items');
      feature?.addEventListener('click', (e) => {
        const isOpened = feature?.classList.toggle('active');
        if (!isOpened) {
          gsap.set(featureBody, {overflow: 'hidden'});
          gsap.to(featureBody, {height: 0});
        } else {
          gsap.set(featureBody, {overflow: 'visible'});
          gsap.to(featureBody, {height: 'auto'});
        }
      });
    });
    
    animations(block);
    imageLazyLoading(block);
  }
};
windowOnLoad(blockScript);




import './index.html';
import './style.scss';
import {debounce} from 'lodash';
import {gsap} from "gsap";
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import '../../../scripts/sitesSizer/towSmart';
import Swiper, {Pagination,Navigation} from "swiper";

Swiper.use([Pagination,Navigation])

const blockScript = async (container = document) => {
  const block = container.querySelector('.how_to_tow_smart_block');
  
  let swiper = new Swiper(block.querySelector('.swiper-container'), {
    pagination: {
      el: block.querySelector('.swiper-pagination'),
      clickable: true,
      
    },
    navigation: {
      nextEl: block.querySelector('.swiper-button-next'),
      prevEl: block.querySelector('.swiper-button-prev'),
    },
    slidesPerView: '1',
    spaceBetween: 0,
    allowTouchMove: true,
    observer: true,
    observeParents: true
  });
  
  const video = block.querySelector(".video-play");
  const circlePlayButton = block.querySelector(".play-btn");
  const videoImg = block.querySelector(".video-wrapper-img");
  
  function togglePlay() {
    if (video.paused || video.ended) {
      video.play();
    } else {
      video.pause();
    }
  }
  
  circlePlayButton?.addEventListener("click", togglePlay);
  video?.addEventListener("playing", function () {
    circlePlayButton.classList.add('active');
    videoImg.classList.add('active');
  });
  video?.addEventListener("pause", function () {
    circlePlayButton.classList.toggle('active');
  });
  
  
  
  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);




<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'stay_updated_block';
$className = 'stay_updated_block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/stay_updated_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$social_link_image = get_field('social_link_image');
$cards = get_field('cards');

?>
<!-- region advisorhub's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="title-section">
        <?php if ($title) { ?>
            <H1 class="headline-5"><?= $title; ?></H1>
        <?php } ?>
        <?php if ($description) { ?>
            <h3 class="description">
                <?= $description; ?>
            </h3>
        <?php } ?>
        <div class="svg-wrapper">
            <?php if (have_rows('social_icon_or_image')) {
                while (have_rows('social_icon_or_image')) {
                    the_row();
                    $image_or_svg = get_sub_field('image_or_svg');
                    $image = get_sub_field('image');
                    $svg = get_sub_field('svg');
                    $social_link = get_sub_field('social_link');
                    ?>
                    <?php if ($image_or_svg) { ?>
                        <?php if ($social_link) { ?>
                            <a class="social" href="<?= $social_link['url'] ?>"
                               target="<?= $social_link['target'] ?>">
                                <img class="social-img" src="<?= $image['url'] ?>"
                                     alt="<?= $image['alt'] ?>">
                            </a>
                        <?php } ?>
                    <?php } else { ?>
                        <?php if ($social_link) { ?>
                            <a class="social" href="<?= $social_link['url'] ?>"
                               target="<?= $social_link['target'] ?>">
                                <?= $svg ?>
                            </a>
                        <?php } ?>
                    <?php } ?>
                <?php }
            } ?>
        </div>
    </div>
    <div class=" row row-cols-1 row-cols-md-3">
        <?php if (have_rows('cards')) {
            while (have_rows('cards')) {
                the_row();
                $image = get_sub_field('image');
                ?>
                <div class="col">
                    <div class="card">
                        <picture class="aspect-ratio">
                            <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                        </picture>
                        <div class="magnifier-wrapper">
                            <a href="#" target="_blank">
                                <svg class="magnifier" width="18" height="18"
                                     viewBox="0 0 18 18"
                                     fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                            d="M17.3348 14.7172L13.5096 10.892C14.3703 9.61695 14.8165 8.02312 14.5934 6.30178C14.179 3.36912 11.7883 1.01025 8.88748 0.627731C4.55225 0.0858274 0.886433 3.75164 1.42834 8.08687C1.81086 10.9876 4.16973 13.3784 7.10238 13.7928C8.82372 14.0159 10.4176 13.5697 11.7245 12.709L15.5178 16.5342C16.0279 17.0123 16.8248 17.0123 17.3348 16.5342C17.8129 16.0242 17.8129 15.2272 17.3348 14.7172ZM3.91472 7.19433C3.91472 4.96296 5.73169 3.11411 7.99493 3.11411C10.2263 3.11411 12.0751 4.96296 12.0751 7.19433C12.0751 9.45757 10.2263 11.2745 7.99493 11.2745C5.73169 11.2745 3.91472 9.45757 3.91472 7.19433Z"
                                            fill="#AAB8BF"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            <?php }
        } ?>
    </div>
</div>
</section>


<!--endregion advisorhub's Block -->

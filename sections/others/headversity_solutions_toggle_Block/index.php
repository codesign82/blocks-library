<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'solutions_toggle_Block';
$className = 'solutions_toggle_Block';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/solutions_toggle_Block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$block_title = get_field('block_title');
$left_tap = get_field('left_tap');
$right_tap = get_field('right_tap');
$contents = get_field('contents');
$first_row = $contents[0];
$second_row = $contents[1];
$first_description = $first_row['block_description'];
$first_achievement = $first_row['achievement'];
$second_description = $second_row['block_description'];
$second_achievement = $second_row['achievement'];
?>
<!-- region headversity's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
    <div class="block-content">
        <?php if ($block_title) { ?>
            <h2 move-to-here
                class="block-title headline-3 iv-st-from-bottom"><?= $block_title ?></h2>
        <?php } ?>
        <div class="taps-wrapper">
            <!--  left Tap   -->
            <?php if ($left_tap) { ?>
                <div class="tap left-tap active-tap iv-st-from-bottom">
                    <div class="svg-wrapper">
                        <svg width="72" height="72" viewBox="0 0 72 72" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <circle cx="36" cy="36" r="34.5" transform="rotate(90 36 36)"
                                    stroke="#005DB3" stroke-width="3"/>
                            <path d="M38.5 50L49.875 36L38.5 22" stroke="#005DB3"
                                  stroke-width="3" stroke-linecap="round"/>
                            <path d="M28 50L39.375 36L28 22" stroke="#005DB3"
                                  stroke-width="3" stroke-linecap="round"/>
                        </svg>
                    </div>
                    <div class="text-wrapper">
                        <?php if ($left_tap['tap_title']) { ?>
                            <div
                                    class="tap-title headline-5"><?= $left_tap['tap_title'] ?></div>
                        <?php }
                        if ($left_tap['tap_description']) { ?>
                            <div
                                    class="tap-description paragraph"><?= $left_tap['tap_description'] ?>
                            </div>
                        <?php } ?>
                    </div>
                    <!--            text of small tap-->
                    <div class="text-tap-part small-active " style="height: auto">
                        <?php if ($first_description) { ?>
                            <div class="description paragraph"><?= $first_description ?>
                            </div>
                        <?php } ?>
                        <div class="achievements-wrapper">
                            <?php if ($first_achievement) { ?>
                                <?php foreach ($first_achievement as $card) { ?>
                                    <div class="achievement">
                                        <svg class="checked-svg" width="26" height="24"
                                             viewBox="0 0 26 24" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path d="M2 11.5L10.36 21L24 2" stroke="#005DB3"
                                                  stroke-width="3" stroke-linecap="round"/>
                                        </svg>

                                        <div
                                                class="heading headline-6"><?= $card['achievement_name'] ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                            } ?>

                        </div>
                    </div>
                </div>
            <?php } ?>
            <!--  Right Tap   -->
            <?php if ($right_tap) { ?>
                <div class="tap right-tap iv-st-from-bottom">
                    <div class="svg-wrapper">
                        <svg width="72" height="72" viewBox="0 0 72 72" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <circle cx="36" cy="36" r="34.5" transform="rotate(90 36 36)"
                                    stroke="#005DB3" stroke-width="3"/>
                            <path d="M38.5 50L49.875 36L38.5 22" stroke="#005DB3"
                                  stroke-width="3" stroke-linecap="round"/>
                            <path d="M28 50L39.375 36L28 22" stroke="#005DB3"
                                  stroke-width="3" stroke-linecap="round"/>
                        </svg>
                    </div>
                    <div class="text-wrapper">
                        <?php if ($right_tap['tap_title']) { ?>
                            <div
                                    class="tap-title headline-5 "><?= $right_tap['tap_title'] ?></div>
                        <?php }
                        if ($right_tap['tap_description']) { ?>
                            <div
                                    class="tap-description paragraph "><?= $right_tap['tap_description'] ?>
                            </div>
                        <?php } ?>

                    </div>
                    <!--            text of small tap-->
                    <div class="text-tap-part">
                        <?php if ($second_description) { ?>
                            <div class="description paragraph"><?= $second_description ?>
                            </div>
                        <?php } ?>
                        <div class="achievements-wrapper">
                            <?php if ($second_achievement) { ?>
                                <?php foreach ($second_achievement as $card) { ?>
                                    <div class="achievement">
                                        <svg class="checked-svg" width="26" height="24"
                                             viewBox="0 0 26 24" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path d="M2 11.5L10.36 21L24 2" stroke="#005DB3"
                                                  stroke-width="3" stroke-linecap="round"/>
                                        </svg>
                                        <div
                                                class="heading headline-6"><?= $card['achievement_name'] ?>
                                        </div>
                                    </div>
                                <?php }
                            } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
        <!--    Content Section   -->
        <div class="bottom-section">
            <?php if (have_rows('contents')) {
                while (have_rows('contents')) {
                    the_row();
                    $block_description = get_sub_field('block_description');
                    $achievement = get_sub_field('achievement');
                    $image_section = get_sub_field('image_section');
                    $orbs_color = get_sub_field('orbs_color');
                    $rounded_left = @$image_section['is_rounded_left'];
                    $rounded_right = @$image_section['is_rounded_right'];
                    ?>
                    <div class="content-section-holder ">
                        <div class="content-section ">
                            <div class="text-part iv-st-from-bottom">
                                <?php if ($block_description) { ?>
                                    <div class="description paragraph">
                                        <?= $block_description ?>
                                    </div>
                                <?php } ?>
                                <div class="achievements-wrapper">
                                    <?php if (have_rows('achievement')) {
                                        while (have_rows('achievement')) {
                                            the_row();
                                            $achievement_name = get_sub_field('achievement_name');
                                            ?>
                                            <div class="achievement">
                                                <svg class="checked-svg" width="26" height="24"
                                                     viewBox="0 0 26 24" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M2 11.5L10.36 21L24 2" stroke="#005DB3"
                                                          stroke-width="3" stroke-linecap="round"/>
                                                </svg>
                                                <div class="heading headline-6"><?= $achievement_name ?>
                                                </div>
                                            </div>
                                        <?php }
                                    } ?>
                                </div>
                            </div>
                            <?php if ($image_section) { ?>
                                <div class="images-part iv-st-from-bottom">
                                    <div class="mobile-wrapper">
                                        <?php if ($image_section['main_image']) { ?>
                                            <picture class=" mobile aspect-ratio">
                                                <?= get_acf_image($image_section['main_image'], 'img-235-500') ?>
                                            </picture>
                                        <?php } ?>

                                    </div>
                                    <?php if ($image_section['left_image']) { ?>
                                        <picture
                                                class="small-card pic-2 <?= $rounded_left ? "rounded" : "" ?>">
                                            <?php if ($rounded_left) { ?>
                                                <?= get_acf_image($image_section['left_image'], 'img-173-173') ?>
                                            <?php } else { ?>
                                                <?= get_acf_image($image_section['left_image'], 'img-205-84') ?>
                                            <?php } ?>
                                        </picture>
                                    <?php }
                                    if ($image_section['right_image']) { ?>
                                        <picture
                                                class="small-card pic-3 <?= $rounded_right ? "rounded" : "" ?>">
                                            <?php if ($rounded_left) { ?>
                                                <?= get_acf_image($image_section['right_image'], 'img-173-173') ?>
                                            <?php } else { ?>
                                                <?= get_acf_image($image_section['right_image'], 'img-205-84') ?>
                                            <?php } ?>
                                        </picture>
                                    <?php } ?>
                                    <?php if ($image_section['main_image']) { ?>
                                        <div class="yellow-ring"></div>
                                    <?php } ?>
                                    <div class="ellipse ellipse-<?= $orbs_color ?> orbs-30"
                                         style="background: radial-gradient(50% 50% at 50% 50%, <?= $orbs_color ?> 0%, rgba(255, 255, 255, 0) 100%);"
                                    ></div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php }
            } ?>
        </div>
    </div>
</div>
</section>

import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../../scripts/functions/imageLazyLoading';
import {animations} from '../../../scripts/general/animations';
import {windowOnLoad} from "../../../scripts/windowOnLoad";
import '../../../scripts/sitesSizer/propellerhealth';


import {gsap} from "gsap";

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const blockScript = async (container = document) => {
  const block = container.querySelector('.solutions_toggle_Block');

  const contentSectionHolder = block.querySelectorAll(".content-section-holder");
  const taps = block.querySelectorAll(".tap");
  const cin = block.querySelectorAll(".text-tap-part")


  for (let i = 0; i < taps.length; i++) {
    contentSectionHolder[0].classList.add('appeared')
    const tap = taps[i];
    tap.addEventListener('click', () => {
      if (tap.classList.contains('active-tap')) return;
      for (let x = 0; x < taps.length; x++) {

        contentSectionHolder[x].classList.remove("appeared");
        taps[x].classList.remove("active-tap");
      }
      contentSectionHolder[i].classList.add("appeared");
      tap.classList.add("active-tap");

      const text = tap.querySelector('.text-tap-part');
      const isOpened = text.classList.toggle('small-active');
      if (!isOpened) {
        gsap.to(text, {height: 0});
      } else {

        gsap.to(Array.from(taps).map(otherMenuItem => {
          const otherMenuItemBody = otherMenuItem.querySelector('.text-tap-part');
          if (otherMenuItemBody && text !== otherMenuItem.querySelector('.text-tap-part')) {
            otherMenuItemBody?.classList.remove('small-active');
            gsap.set(otherMenuItem, {zIndex: 1});
          }
          return otherMenuItemBody;
        }), {height: 0});
        gsap.set(text, {zIndex: 2});
        gsap.to(text, {height: 'auto'});
      }

    })
  }


  animations(block);
  imageLazyLoading(block);
};
windowOnLoad(blockScript);


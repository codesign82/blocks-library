<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$background_color = get_field( 'background_color' );
$background_color = $background_color ? ' white ' : '  ';
$dataClass        = 'text_columns_link_block';
$className        = 'text_columns_link_block';
$className        .= $background_color;
if ( ! empty( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
    $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
    /* Render screenshot for example */
    echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/text_columns_link_block/screenshot.png" >';

    return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field( 'title' );
$text = get_field( 'text' );
$cta_link = get_field( 'cta_link' );
$cta_button = get_field( 'cta_button' );
$enable_dots_background = get_field( 'enable_dots_background' );
$have_svg_animation = get_field('have_svg_animation');
$title_style = get_field('title_style');
$text_style = get_field('text_style');
$position_content = get_field('position_content');
?>
<!--   region propellerhealth Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<?php if ( $enable_dots_background ) { ?>
    <picture class="dots-bg cover-background"></picture>
<?php } ?>
<div class="container">
    <div class="text-columns-link-cards <?=$position_content ? ' center ' : ' ' ?>">
        <?php if ( $title ) { ?>
            <div class="text-columns-link-left-title <?=$title_style?> iv-st-from-bottom">
                <?= $title ?>
            </div>
        <?php } ?>
        <div class="text-columns-link-right iv-st-from-bottom">
            <?php if ( $text ) { ?>
                <div class="text-columns-link-right-description <?=$text_style?>">
                    <?= $text ?>
                </div>
            <?php } ?>
            <?php if ( $cta_link ) { ?>
                <a href="<?= $cta_link['url'] ?>"
                   target="<?= $cta_link['target'] ?>"
                   class="cta-link text-columns-link"><?= $cta_link['title'] ?>
                    <svg class="arrow-right" viewBox="243.81 243.737 15.017 12.535"
                         xmlns="http://www.w3.org/2000/svg">
                        <path class="arrow-head"
                              d="M 251.977 256.242 L 257.977 250.742 L 258.781 250.005 L 257.977 249.268 L 251.977 243.768 L 250.625 245.242 L 255.821 250.005 L 250.625 254.768 L 251.977 256.242 Z"
                              style="fill: #C6DFF8;"></path>
                        <path class="arrow-line"
                              d="M 243.857 249.004 L 256.857 249.004 L 256.857 251.004 L 243.857 251.004 L 243.857 249.004 Z"
                              style="fill: #C6DFF8;"></path>
                    </svg>
                </a>
            <?php } ?>
            <?php if ( $cta_button ) { ?>
                <a href="<?= $cta_button['url'] ?>"
                   target="<?= $cta_button['target'] ?>"
                   class="white-cta cta-button  text-columns-link-cta-btn"><?= $cta_button['title'] ?>
                </a>
            <?php } ?>
        </div>
        <?php if ($have_svg_animation) { ?>
            <svg class="dots-animation-i-e site-dots" fill="none">
                <path class="dot-line-animation" d="" stroke="#90E2C8" stroke-width="3" stroke-linecap="round" stroke-dasharray="1 10 1 10"/>
            </svg>
        <?php } ?>
    </div>
</div>
</section>


<!-- endregion propellerhealth Block -->

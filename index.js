const repoUrl='https://bitbucket.org/codesign82/blocks-library/src/main/';

//Change body Overflow
const handleBodyOverflow = (hidden = true) => {
  document.body.style.overflow = hidden ? 'hidden' : 'auto';
}

//Masonry Layout
const cardsWrapper = document.querySelector('.grid');
const styleLayoutMasonry = () => {
  const iso = new Isotope(cardsWrapper, {
    itemSelector: '.grid-item',
    percentPosition: true,
    horizontalOrder: true,
    masonry: {
      columnWidth: '.grid-sizer',
    }
  });
  
  imagesLoaded(cardsWrapper).on('progress', function () {
    // layout Isotope after each image loads
    iso.layout();
  });
}

//Modal
let screenshots = [];
let modal = document.getElementById("animationModal");
const iframes = document.querySelectorAll(".iframe_wrapper");
let currentIframe = 0;

const animationIframe = document.querySelector(".iframe_wrapper");

const openModal = () => {
  screenshots = document.querySelectorAll(`.animation_screenshot`);
  modal.classList.add("showModal");
  handleBodyOverflow();
  document.body.style.height = "100vh";
  header.style.zIndex = '0';
};

const closeModal = () => {
  modal.classList.remove("showModal");
  handleBodyOverflow(false);
  document.body.style.height = "unset";
  currentIframe = 0;
  header.style.zIndex = '99';
};

document.querySelector(".closeBtn").addEventListener("click", closeModal);


//Show Animation On Modal
const modalLoader = document.querySelector('.modal_loader');
const pageBtn = document.querySelector('.originalPageBtn');
const codeBtn = document.querySelector('.sourceCodeBtn');
const viewAnimationOnModal = (el) => {
  openModal();
  modalLoader.style.display = 'flex';
  const nextIframe = (1 + currentIframe) % 2;
  pageBtn.href = el.dataset.target;
  codeBtn.href = el.dataset.codeUrl;
  iframes[currentIframe].src = el.dataset.iFrameSrc;
  iframes[currentIframe].onload = () => {
    modalLoader.style.display = 'none'
  };
  iframes[currentIframe].style.zIndex = "0";
  iframes[currentIframe].style.opacity = "1";
  iframes[nextIframe].style.zIndex = "-1";
  iframes[nextIframe].style.opacity = "0";
  currentIframe = nextIframe;
};


//Slider
let activeSlideIndex = 0;
const showNextSlide = () => {
  (activeSlideIndex === screenshots.length - 1) ? (activeSlideIndex = 0) : activeSlideIndex++;
  viewAnimationOnModal(screenshots[activeSlideIndex].parentNode);
}
document.querySelector(".nextBtn").addEventListener('click', showNextSlide);

const showPrevSlide = () => {
  (activeSlideIndex === 0) ? (activeSlideIndex = screenshots.length - 1) : activeSlideIndex--;
  viewAnimationOnModal(screenshots[activeSlideIndex].parentNode);
}
document.querySelector(".prevBtn").addEventListener('click', showPrevSlide);


//Check is user admin
const urlSearchParams = new URLSearchParams(window.location.search);
if (urlSearchParams.has('admin')) {
  codeBtn.style.display = 'block';
}


//Handle Switch Dark Light Mode
const switchModeBtn = document.getElementById('switchMode');
const htmlEl = document.documentElement;
const modeInStorage = localStorage.getItem('mode');

if (modeInStorage === 'dark') {
  htmlEl.setAttribute('data-theme', modeInStorage);
  switchModeBtn.checked = true;
}

switchModeBtn.addEventListener('change', (e) => {
  if (e.target.checked) {
    htmlEl.setAttribute('data-theme', 'dark');
    localStorage.setItem('mode', 'dark');
  } else {
    htmlEl.setAttribute('data-theme', 'light');
    localStorage.setItem('mode', 'light');
  }
});


//Sticky Header
const mainSection = document.querySelector('main')
let isHeaderSticky = false;
let lastY = 0;
const header = document.querySelector('.header');
const headerHeight = header.getBoundingClientRect().height;

const handlePagePaddingTop = (value = null) => {
  mainSection.style.paddingTop = value ? `${value}px` : `${headerHeight}px`;
}
handlePagePaddingTop();

const onScroll = () => {
  isHeaderSticky = (lastY <= mainSection.scrollTop);
  lastY = mainSection.scrollTop;
  header.style.top = isHeaderSticky ? `-${headerHeight}px` : '0px';
  isHeaderSticky && handlePagePaddingTop(0);
}
mainSection.addEventListener("scroll", onScroll);


// Handle Mobile Alert
const mobileAlertSection = document.querySelector('.mobileAlert');
const handleMobileAlertView = () => {
  if (window.innerWidth <= 991) {
    mobileAlertSection.style.display = 'block'
  } else {
    mobileAlertSection.style.display = 'none'
  }
}
handleMobileAlertView()
window.addEventListener('resize', handleMobileAlertView);


//Convert a template string into HTML DOM nodes
const stringToHTML = (str) => {
  const parser = new DOMParser();
  const doc = parser.parseFromString(str, 'text/html');
  return [...doc.body.children];
};


//List Data In HTML
const renderSidebarList = (sections) => {
  console.log(sections);
  // sections.push({name:"All",readableName:"All"})
  document.getElementById("All").addEventListener("click", () => {
    renderSection(sections)
  })
  renderSection(sections)
  for (const section of sections) {
    const str = `
                <div class="sideBar_link" id=${section.name}>
                    <div class="sideBar_link_icon">
                      ${section.icon} </div>
                    <p class="headline-3-light">${section.readableName}</p>
                </div>
        `;
    /**
     * {HTMLElement}
     */
    const sideBarLink = stringToHTML(str)[0];
    sideBarLink.sectionData = section;
    sideBarLink.addEventListener('click', () => renderSection([section], sideBarLink));
    document.querySelector('.sideBar_links').append(sideBarLink);
  }
}

let activeLink;
const loader = document.querySelector(".loaderCont");

const renderSection = (sections, activeSideBarLink) => {
  activeSideBarLink = activeSideBarLink ? activeSideBarLink : document.querySelector('.sideBar_link');
  activeSideBarLink.classList.add("active_sideBar_link");
  activeLink && activeLink.classList.remove("active_sideBar_link");
  activeLink = activeSideBarLink;
  
  for (let elementNodeListOfElement of cardsWrapper.querySelectorAll('.grid-item')) {
    elementNodeListOfElement.remove();
  }
  for (const section of sections) {
    if (section.renderedComponents) {
      for (let renderedComponent of section.renderedComponents) {
        cardsWrapper.append(renderedComponent);
      }
    } else {
      let finalHTMLString = ``
      for (let component of section.components) {
        finalHTMLString += `
            <div class="grid-item" id="${component.name}" data-target="${component.siteUrl}" data-i-frame-src="${component.iFrameSrc}" data-code-url="${repoUrl}sections/${section.name}/${component.name}">
                <div class="animation_screenshot">
                    <img src="${component.mediaUrl || `./sections/${section.name}/${component.name}/screenshot.png`}" alt="${component.readableName}" class="screenshotImg"/>
                    <div class="animation_screenshot_overlay">
                        <p class="headline-3-light">Click to view</p>
                    </div>
                </div>
            </div>
            `
      }
      section.renderedComponents = stringToHTML(finalHTMLString);
      loader.style.height = `${window.innerHeight - headerHeight}px`;
      loader.style.display = 'flex'
      mainSection.style.overflow = 'hidden';
      
      for (let renderedComponent of section.renderedComponents) {
        renderedComponent.addEventListener("click", () => {
          viewAnimationOnModal(renderedComponent);
        })
        cardsWrapper.append(renderedComponent);
      }
      const promises = [...document.querySelectorAll(".screenshotImg")].map(el => new Promise((resolve, reject) => {
        el.onload = resolve
        el.onerror = reject
      }));
      Promise.allSettled(promises).then(() => {
        loader.style.display = 'none';
        mainSection.style.overflow = 'auto';
        styleLayoutMasonry();
      });
    }
  }
  
  styleLayoutMasonry();
}
const renderSearchedElements = (renderElements, section) => {
  let finalHTMLString = ``
  for (let component of renderElements) {
    finalHTMLString += `
            <div class="grid-item" id="${component.name}" data-target="${component.siteUrl}" data-i-frame-src="${component.iFrameSrc}">
                <div class="animation_screenshot">
                    <img src="${component.mediaUrl}" alt="${component.readableName}" class="screenshotImg"/>
                    <div class="animation_screenshot_overlay">
                        <p class="headline-3-light">Click to view</p>
                    </div>
                </div>
            </div>
            `
  }
  section.renderedComponents = stringToHTML(finalHTMLString);
  for (let i = 0; i < section.renderedComponents.length; i++) {
    
    section.renderedComponents[i].addEventListener("click", () => {
      viewAnimationOnModal(section.renderedComponents[i]);
    })
  }
  
  
  renderSection([section]);
}
//Load Data From JSON File
const getData = async () => {
  const response = await fetch('./main-data.json');
  return await response.json();
}
window.addEventListener('load', async () => {
  const sections = await getData();
  let tagsElements = []
  for (let section of sections) {
    if (!section.components) continue;
    for (let component of section.components) {
      if (!component?.tags) continue;
      for (let i = 0; i < component.tags.length; i++) {
        tagsElements.push({id: i, text: component.tags[i]})

      }
    }
  }

  let data = $.map(tagsElements, function (obj) {
    obj.id = obj.text; // replace pk with your identifier
    return obj;
  });
  $('.js-example-basic-single').select2({
    data: data,
    placeholder: "Search",
    allowClear: true
  });

  $('.js-example-basic-single').on('change', function (e) {
    console.log('66666666666666')
    let values = $(".js-example-basic-single").val();
    let renElements = []
    for (let i = 0; i < sections.length; i++) {
      for (let j = 0; j <= sections[i].components.length; j++) {
        if (sections[i]?.components[j]?.tags.some(item => values.includes(item))) {
          // console.log(sections[i]?.components[j])
          renElements.push(sections[i].components[j]);
          renderSearchedElements(renElements, sections[i]);
        } else if (e.target.value === "") {
          renElements = [...sections[i].components];
          renderSearchedElements(renElements, sections[i]);
        }
      }
      renElements = []
    }
  });
  renderSidebarList(sections);
})

//Handle Preloader
async function onLoad() {
  if (document.readyState === 'complete') {
    document.querySelector(".preloader").style.display = "none";
  }
}

onLoad();
document.onreadystatechange = function () {
  onLoad();
};

module.exports = function (plop) {
  plop.addHelper("upperCase", text => text.toUpperCase());
  const pascalCase = (s) =>
    s.replace(/\w+/g, function (w) {
      return w[0].toUpperCase() + w.slice(1);
    });
  const files = {
    frontEnd: {
      html: './plop-templates/front-end-templates/html-template.html',
      js: './plop-templates/front-end-templates/index.js',
      style: './plop-templates/front-end-templates/style.scss',
    },
    backEnd: {
      php: './plop-templates/back-end-templates/index.php',
      appendACFBlock: './plop-templates/back-end-templates/appendACFBlock.php',

    },
  };

  const createJs = {
    type: "add",
    path: "./src/blocks/{{snakeCase name}}/index.js",
    templateFile: files.frontEnd.js,
  };

  const createStyle = {
    type: "add",
    path: "./src/blocks/{{snakeCase name}}/style.scss",
    templateFile: files.frontEnd.style,
  };

  const createHtml = {
    type: "add",
    path: "./src/blocks/{{snakeCase name}}/index.html",
    templateFile: files.frontEnd.html,
  };

  const createPhp = {
    type: "add",
    path: "../template-parts/blocks/{{snakeCase name}}/index.php",
    templateFile: files.backEnd.php,
  };

  const createFrontEndBlockFiles = {
    type: "addMany",
    destination: "./src/blocks/{{snakeCase name}}",
    base: `plop-templates/front-end-templates`,
    templateFiles: `plop-templates/front-end-templates/*.hbs`
  }


  const registerACFBlock = {
    type: "append",
    pattern: /.*(?=\/\* -- blocks will be registered above -- \*\/)/,
    path: "../functions.php",
    templateFile: files.backEnd.appendACFBlock,
    data:{name:'', domain:'cyberHill'}
  };

  const createStyledComp = {
    type: "append",
    pattern: '.*(?=\/\* -- blocks will be registered above -- \*\/)',
    path: "../template-parts/blocks/{{snakeCase name}}/index.php",
    templateFile: files.backEnd.php,
    data:{name:'', domain:'cyberHill'}
  };


  /* Input Options */
  const getBlockName = {
    type: "input",
    name: "name",
    message: "What is the block name?",
    validate: function (value) {
      if (/.+/.test(value)) {
        return true;
      }
      return "name is required";
    },
  };

  const getOldBlockName = {
    type: "input",
    name: "oldName",
    message: "What is the old block name?",
    validate: function (value) {
      if (/.+/.test(value)) {
        return true;
      }
      return "name is required";
    },
  };
  const getNewBlockName = {
    type: "input",
    name: "newName",
    message: "What is the new block name?",
    validate: function (value) {
      if (/.+/.test(value)) {
        return true;
      }
      return "name is required";
    },
  };


  /* Generators */
  plop.setGenerator("cb", {
    description: "Create Block",
    prompts: [getBlockName],
    actions: [
      createFrontEndBlockFiles,
      createPhp,
      registerACFBlock,
    ],
  });

  plop.setGenerator("eb", {
    description: "Edit Block Name",
    prompts: [getOldBlockName, getNewBlockName],
    actions: [
      createStyledComp,
    ]
  });
};

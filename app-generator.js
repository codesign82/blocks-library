const fs = require('fs');
const {debounce} = require('lodash');


// const exclude = /(\.idea)|(main-data\.json)|(node_modules)/g;
const generator = debounce(() => {
    const sections = fs.readdirSync('./sections');
    let sectionsObjArray = [];
    for (const section of sections) {
        //for mac issue
        if (section === ".DS_Store") continue;

        const blocks = fs.readdirSync(`./sections/${section}`)
        let sectionBlocksObjArray = []

        for (const block of blocks) {
            //for mac issue
            if (block === ".DS_Store") continue;

            fs.readFile(`./sections/${section}/${block}/data.json`, (err, data) => {
                if (err) {
                    console.log("has error", err)
                } else {
                    const blockJSON = data.toString();
                    const blockObj = JSON.parse(blockJSON);
                    blockObj.iFrameSrc = `./sections/${section}/${block}/bundle.html`
                    sectionBlocksObjArray.push(blockObj);
                }
            })
            let blockJSON = '{}';
            let blockObj = {};
            try {
                blockJSON = fs.readFileSync(`./sections/${section}/${block}/data.json`).toString();
                blockObj = JSON.parse(blockJSON);
                blockObj.iFrameSrc = `./sections/${section}/${block}/bundle.html`
            } catch (e) {
                console.log(e);
            }

            sectionBlocksObjArray.push(blockObj);
        }

        let sectionObj = {
            name: `${section}`,
            readableName: `${section.replace("_", " ")}`,
            "icon": `<svg xmlns="http://www.w3.org/2000/svg" width="24" height="23" viewBox="0 0 24 23">
                            <g>
                                <g>
                                    <path fill="currentColor"
                                          d="M16.117 1.957a4.083 4.083 0 1 1-1.197 2.886 4.088 4.088 0 0 1 1.197-2.886zm-.03 2.886a2.92 2.92 0 0 0 4.537 2.425 2.917 2.917 0 1 0-4.537-2.425zM3.837 14.76a7.596 7.596 0 0 1 7.113 4.953l1.088-3.99a25.393 25.393 0 0 1 2.917-6.696c.284.273.594.516.927.727a24.268 24.268 0 0 0-2.719 6.276L11.4 22.497a.584.584 0 0 1-1.147-.154 6.424 6.424 0 0 0-6.416-6.416H1.503a.583.583 0 0 1 0-1.167z"/>
                                </g>
                            </g>
                        </svg>`,
            components: sectionBlocksObjArray
        }

        sectionsObjArray.push(sectionObj);
    }
    const now = new Date();
    fs.writeFile("./main-data.json", JSON.stringify(sectionsObjArray), function (err) {
        if (err) throw err;
        console.log('\x1b[32m', `********************* DONE SUCCESSFULLY (${now.getHours().toString().padStart(2, '0')}:${now.getMinutes()}:${now.getSeconds()}) *********************`, '\x1b[0m');
        console.log('\x1b[31m', 'watching files...', '\x1b[0m');
    });
}, 1000)


console.log('\x1b[31m', 'watching files...', '\x1b[0m');

generator();
fs.watch('./', {recursive: true}, function (event, filename) {
    filename?.match(/sections.*data.json$/) && generator()
});

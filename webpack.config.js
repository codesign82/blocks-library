const path = require('path');
// const HtmlWebpackPlugin = require("html-webpack-plugin");
// const HtmlInlineScriptPlugin = require('html-inline-script-webpack-plugin');

const glob = require("glob");
const files = glob.sync("./sections/**/*.js").reduce((acc, current) => {
  if (!current.match(/bundle.js|_ignore_/)) {
    acc[current.replace('./', '').replace('index.js', 'bundle')] = current;
  }
  return acc
}, {});
module.exports = function (env, argv) {
  return {
    entry: files,
    output: {
      filename: '[name].js',
      path: path.resolve(__dirname, './'),
      publicPath: './',
    },
    // mode:argv.mode||'development',
    // resolve: {
    //   roots: [path.resolve(__dirname)]
    // },
    watch: true,
    module: {
      rules: [
        {
          test: /\.html$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                
                name: '[path]bundle.html'
              }
            },
            'extract-loader',
            {
              loader: 'html-loader', options: {
                esModule: false,
                sources: {
                  list: [
                    {
                      // Tag name
                      tag: 'img',
                      // Attribute name
                      attribute: 'src',
                      // Type of processing, can be `src` or `scrset`
                      type: 'src',
                    },
                    {
                      // Tag name
                      tag: 'img',
                      // Attribute name
                      attribute: 'srcset',
                      // Type of processing, can be `src` or `scrset`
                      type: 'srcset',
                    },
                    {
                      tag: 'img',
                      attribute: 'data-src',
                      type: 'src',
                    },
                    {
                      tag: 'img',
                      attribute: 'data-srcset',
                      type: 'srcset',
                    },
                  ],
                  // urlFilter: (attribute, value, resourcePath) => {
                  //   return !/style.css$/.test(value) && !/main.css$/.test(value) && !/main.js$/.test(value);
                  // },
                },
              },
            }],
        },
        {
          test: /\.jsx?$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [['@babel/env', {
                "useBuiltIns": "usage",
                "corejs": 3
              }]],
              plugins: [
                '@babel/plugin-proposal-class-properties',
                '@babel/plugin-proposal-object-rest-spread',
                '@babel/plugin-proposal-export-default-from',
                '@babel/plugin-proposal-export-namespace-from',
              ],
            },
          },
        },
        {
          test: /\.s?css$/,
          use: [
            'style-loader',
            {
              loader: 'css-loader',
              options: {importLoaders: 1, sourceMap: true}
            },
            {loader: 'postcss-loader', options: {}},
            'resolve-url-loader',
            {loader: 'sass-loader', options: {sourceMap: true}},
          ],
        },
        {
          test: /\.(woff(2)?|ttf|eot|otf)(\?v=\d+\.\d+\.\d+)?$/,
          use: [{
            loader: 'file-loader',
            options: {
              name: '[folder]/[name].[ext]',
            },
          }],
        },
        {
          test: /\.(png|svg|jpg|gif)$/,
          use: [{
            loader: 'file-loader',
            options: {
              publicPath: function (url) {
                return './' + url;
              },
              name: '[path][name].[ext]',
              context: path.resolve(__dirname, './sections'),
            },
          }],
        },
      ],
    },
    optimization: {
      sideEffects: false,
    },
    devtool: argv.mode === 'production' ? 'source-map' : 'eval',
    devServer: {
      port: 8080,
    },
  };
};

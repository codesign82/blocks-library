import {gsap} from "gsap";

export const windowOnLoad = (callback) => {
  let loaded = false;
  
  async function onLoad() {
    gsap.config({
      nullTargetWarn: false,
    });
    if (document.readyState === 'complete' && !loaded) {
      loaded = true;
      await callback?.();
      document.body.classList.add('loaded');
    }
  }
  
  onLoad();
  
  document.onreadystatechange = function () {
    onLoad();
  };
}

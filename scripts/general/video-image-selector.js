export function videoImageSelector(container = document) {
  let videoWrappers = container.querySelectorAll('.video-image-selector');

  for (const videoWrapper of videoWrappers) {
    if (videoWrapper.classList.contains('has-external-video')) {
      const videoPoster = videoWrapper.querySelector('.video-poster');
      // const videoPlayButton = videoWrapper.querySelector('.video-play-button');
      const videoIframe = videoWrapper.querySelector('iframe');
      if (videoIframe.src.includes('youtube')) {
        videoPoster?.addEventListener('click', function () {
          videoPoster.style.transition = '0.8s';
          videoPoster.style.opacity = 0;
          videoPoster.style.visibility = 'hidden';
          videoPoster.classList.add('active-vimeo-video');
          // videoPlayButton.classList.add('playing');
          videoIframe.src = videoIframe.src + '&autoplay=1';
        })
      } else if (videoIframe.src.includes('vimeo')) {
        const vimeoScriptTag = document.createElement('script');
        vimeoScriptTag.src = "https://player.vimeo.com/api/player.js";
        const firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(vimeoScriptTag, firstScriptTag);

        vimeoScriptTag.addEventListener('load', function () {
          const player = new Vimeo.Player(videoIframe);
          videoPoster?.addEventListener('click', function () {
            videoPoster.style.transition = '0.8s';
            videoPoster.style.opacity = 0;
            videoPoster.style.visibility = 'hidden';
            videoPoster.classList.add('active-vimeo-video');
            // videoPlayButton.classList.add('playing');
            player.play();
          })
        });
      }
    }
  }
}

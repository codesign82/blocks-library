import gsap from "gsap";
import {getElementsForAnimation} from "../functions/getElementsForAnimation";



export function accordion(accordions) {
  if (accordions.length === 0) return;
  accordions.forEach((accordion) => {
    const accordionHead = accordion.querySelector('.accordion-head');
    const accordionBody = accordion.querySelector('.accordion-body');
    if (!accordionBody) {
      gsap.set(accordionHead, {
        cursor: "auto"
      })
    }
    accordionHead?.addEventListener('click', (e) => {
      if (!accordionBody) {
        return;
      }
      const isOpened = accordion?.classList.toggle('accordion-active');
      if (!isOpened) {
        gsap.to(accordionBody, {height: 0});
      } else {
        gsap.to(Array.from(accordions).map(otherAccordion => {
          const otherAccordionBody = otherAccordion.querySelector('.accordion-body');
          if (otherAccordionBody && accordion !== otherAccordion) {
            otherAccordion?.classList.remove('accordion-active');
            gsap.set(otherAccordion, {zIndex: 1});
          }
          return otherAccordionBody;
        }), {height: 0});
        gsap.set(accordion, {zIndex: 2});
        gsap.to(accordionBody, {height: 'auto'});
      }
    });
  });
}



// Old Accordion Function For Faqs Only


// export default function accordion(block) {
//
//   const faqs = getElementsForAnimation(block, '.faq');
//   if (faqs.length === 0) return;
//   faqs.forEach((faq) => {
//     const faqHead = faq.querySelector('.faq-head');
//     const faqBody = faq.querySelector('.faq-body');
//     faqHead?.addEventListener('click', (e) => {
//       if (!faqBody) {
//         return;
//       }
//       const isOpened = faq?.classList.toggle('faq-active');
//       if (!isOpened) {
//         gsap.to(faqBody, {height: 0});
//       } else {
//         gsap.to(Array.from(faqs).map(otherFaq => {
//           const otherFaqBody = otherFaq.querySelector('.faq-body');
//           if (otherFaqBody && faq !== otherFaq) {
//             otherFaq?.classList.remove('faq-active');
//             gsap.set(otherFaq, {zIndex: 1});
//           }
//           return otherFaqBody;
//         }), {height: 0});
//         gsap.set(faq, {zIndex: 2});
//         gsap.to(faqBody, {height: 'auto'});
//       }
//     });
//   });
// }

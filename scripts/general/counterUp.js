import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger"

gsap.registerPlugin([ScrollTrigger]);

export default function counterUp(counterVal) {
  let Cont = {val: counterVal.innerHTML},
    NewVal = parseInt(counterVal.dataset.countTo);

  gsap.to(Cont, {
    val: NewVal,
    roundProps: "val",
    ease: 'none',
    duration: 2,
    onUpdate: function () {
      counterVal.innerHTML = Cont.val
    },
    scrollTrigger: {
      trigger: counterVal,
      start: 'top 50%',
      end: 'bottom bottom',
      pin: false,
    }
  });
}

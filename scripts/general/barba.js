import barbajs from '@barba/core';
import {gsap} from 'gsap';

import ScrollTrigger from 'gsap/ScrollTrigger';

let popState = {'status': false};
window.addEventListener('popstate', () => {
  popState.status = true;
});
export const pageCleaningFunctions = [];
const crossFade = {
  name: 'bottom-overlay-color-transition',
  leave(data) {
    const cover = document.querySelector('.page-transition');
    return gsap.timeline()
        .to(cover, {opacity: 1, duration: .7})
    
  },
  enter(data) {
    gsap.set(data.next.container, {opacity: 0});
    gsap.set(data.current.container, {zIndex: -1, position: 'absolute'});
    const cover = document.querySelector('.page-transition');
    return gsap.timeline()
        .set(data.next.container, {opacity: 1})
        .set(data.current.container, {opacity: 0})
        .to(cover, {opacity: 0, duration: .7, ease: 'power2.in'}, '+=.2')
  },
  afterLeave(data) {
    let regexp = /\<body.*\sclass=["'](.+?)["'].*\>/gi,
        match = regexp.exec(data.next.html);
    if (!match || !match[1]) {
      // If there is no <body/> classes, remove it
      document.body.setAttribute("class", "loaded");
    } else {
      // Set the new body class
      document.body.setAttribute("class", `${match[1]} loaded`);
    }
  }
};
export const barba = (reInvokableFunction) => {
  gsap.registerPlugin(ScrollTrigger);
  if (document.querySelector('[data-barba]')) {
    barbajs.init({
      transitions: [crossFade],
      prefetchIgnore: true,
      timeout: 0,
      prevent: ({el}) => el.classList?.contains('ab-item'),
    });
    
    barbajs.hooks.afterLeave(() => {
      document.querySelector('.keep-scroll').classList.remove('keep-scroll-active');
      
      window.dispatchEvent(new Event('will-leave'));
      for (let scrollTrigger of ScrollTrigger.getAll()) {
        scrollTrigger.kill();
      }
      while (pageCleaningFunctions.length) {
        pageCleaningFunctions.pop()();
      }
      if (popState.status) {
        popState.status = false
      } else {
        document.querySelector('header').classList.remove('free')
        window.dispatchEvent(new Event('reset-position'))
        window.scrollTo(0, 0)
      }
      ScrollTrigger.clearScrollMemory();
      ScrollTrigger.update();
    });
    
    barbajs.hooks.beforeEnter(data => {
      reInvokableFunction(data.next.container);
    });
    
  } else {
    console.log('no barbajs container');
  }
}

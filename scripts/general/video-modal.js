import {preventPageScroll} from "../functions/prevent_allowPageScroll";

export function videoModal(container = document) {
  let buttons = container.querySelectorAll('.open-modal-button');
  const customModalEl = document.getElementById('custom-modal');

  buttons.forEach((button) => {
    button?.addEventListener('click', function (e) {
      const modalContent = container.querySelector('.custom-modal-wrap');
      const videoUrl = button.dataset.iframeSrc;
      // toggler function
      preventPageScroll();
      customModalEl?.classList.add('active');
      modalContent.querySelector('iframe').src = videoUrl;
    });
  });

}

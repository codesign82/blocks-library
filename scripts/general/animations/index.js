import {parallaxAnimation} from './parallaxAnimation';
import {imageRevealAnimation} from './imagesRevealAnimation';
import {wordsUpAnimation} from './wordsUpAnimation';
import {linesUpAnimation} from './linesUpAnimation';
import {inViewAnimations} from './InViewAnimations';
import {realLinesUpAnimation} from './realLinesUpAnimation';
import {spriteSheetAnimation} from "./spriteSheetAnimation";
import {gsap} from 'gsap';
import ScrollTrigger from 'gsap/ScrollTrigger';
import {charactersUpAnimation} from "./charactersUpAnimation";
import {shapesAnimation} from "./shapesAnimation";
import {drawDottedLineAnimation} from "./drawDottedLineAnimation";

gsap.registerPlugin(ScrollTrigger)


export function animations(container = document) {
  imageRevealAnimation(container);
  wordsUpAnimation(container);
  linesUpAnimation(container);
  inViewAnimations(container);
  spriteSheetAnimation(container);
  parallaxAnimation(container);
  realLinesUpAnimation(container);
  charactersUpAnimation(container);
  shapesAnimation(container);
  drawDottedLineAnimation(container);
}

import {gsap} from 'gsap';
import {ScrollTrigger} from 'gsap/ScrollTrigger';
import {getElementsForAnimation} from '../../functions/getElementsForAnimation';

gsap.registerPlugin(ScrollTrigger);

/*
 * =============================================
 * FUNCTIONS
 * =============================================
 */

/*
 * generates a random number in the [min, max) interval
 * max: upper boundary for generated number;
 *      defaults to 1
 * min: lower boundary for generated number;
 *      defaults to 0
 * _int: flag specifying if generated number
 *       should be rounded to the nearest integer
 *       falsy by default
 */
const rand = function (_max, _min, _int) {
  const max = (_max === 0 || _max) ? _max : 1,
      min = _min || 0,
      gen = min + (max - min) * Math.random();

  return (_int) ? Math.round(gen) : gen;
};

const confettis = [];

/* Load up some confetti! */
const loadConfetti = function () {
  const large_green = new Image;
  large_green.src = `
 data:image/svg+xml,%3Csvg width='32.25' height='26.25' viewBox='0 0 43 35' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M17.0232 29.0282C17.0232 29.0282 9.88664 23.7404 8.04753 15.8381C6.67819 9.97143 10.1115 2.61358 10.3914 2.28002L0.831028 0.411133C0.831028 0.411133 -2.0026 12.8655 2.79508 21.1455C4.26436 23.6815 8.5273 27.1789 11.271 28.4837C14.2895 29.921 17.0232 29.0282 17.0232 29.0282Z' fill='%236BB7BF'/%3E%3Cpath d='M33.6401 23.3141L42.3408 33.085C42.3408 33.085 17.7528 39.4959 3.69458 22.4214C3.69458 22.4214 24.3945 29.2934 33.6401 23.3141Z' fill='%2386D1CD'/%3E%3C/svg%3E%0A
`;
  confettis.push(large_green);

  const large_blue = new Image;
  large_blue.src = `data:image/svg+xml,%3Csvg width='59.28' height='66.75' viewBox='0 0 104 89' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M73.724 34.0602C73.724 34.0602 66.9977 67.7939 31.4116 61.5228L21.5941 84.9063C21.5941 84.9063 42.487 89.9456 69.4083 52.7864C87.0309 28.4786 73.724 34.0602 73.724 34.0602Z' fill='%235685D8'/%3E%3Cpath d='M80.4136 0.914403C80.4136 0.914403 96.9213 29.0781 55.1704 69.3735C55.1704 69.3735 69.3854 52.7993 62.0371 20.7448L80.4136 0.914403Z' fill='%23739AF0'/%3E%3C/svg%3E%0A`;
  confettis.push(large_blue);

  const small_green = new Image;
  small_green.src = `
  data:image/svg+xml,%3Csvg width='30' height='24.75' viewBox='0 0 40 33' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M0 23.99C0 23.99 17.2 13.13 19.01 0C19.01 0 35.76 0.909997 39.38 6.34C39.38 6.34 31.69 28.06 22.63 32.14C22.63 32.14 2.26 33.5 0 23.99Z' fill='%2386D1CD'/%3E%3C/svg%3E%0A
  `;
  confettis.push(small_green);

  const small_orange = new Image;
  small_orange.src = `
  data:image/svg+xml,%3Csvg width='28.5' height='23.25' viewBox='0 0 38 31' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M0.271484 22.39C0.271484 22.39 16.3215 12.25 18.0115 0C18.0115 0 33.6415 0.84 37.0215 5.91C37.0215 5.91 29.8415 26.18 21.3915 29.99C21.3915 29.99 2.38148 31.26 0.271484 22.39Z' fill='%23ED7D2B'/%3E%3C/svg%3E
  `;
  confettis.push(small_orange);


};


export function confettiClick(container = document) {
  const canvases = getElementsForAnimation(container, 'canvas.confetti-click');
  for (const canvas of canvases) {


    /*
     * =============================================
     * GLOBALS
     * =============================================
     */
    const N_PARTICLES = 25;

    let w /* canvas width */;
    let h /* canvas height */;
    let particles = [];
    let source = {} /* particle fountain source */;
    const ctx = canvas.getContext('2d') /* get canvas context */;
    let t = 0;
    let req_id = null;

    /*
     * =============================================
     * OBJECTS USED
     * =============================================
     */
    const Particle = function (i) {
      let confetti /* current confetti piece */,
          pos /* current particle position */,
          v /* current particle velocity */,
          a /* current particle acceleration */,
          c_angle /* confetti particle angle */,
          angle_v /* angle velocity */

      /* active = was already shot up, but hasn't landed yet */
      this.active = false;

      /*
       * make particle active and give it a velocity so that
       * it can start moving
       */
      this.shoot = function () {
        let angle, val;
        this.active = true;

        /* choose our confetti */
        confetti = confettis[Math.floor(Math.random() * confettis.length)];

        /* position it at the fountain source,
         * but a bit lower, depending on its radius
         */
        pos = {'x': source.x + rand(-10, 10), 'y': source.y};

        /*
         * give it an acceleration considering gravity
         * and uniform friction (depending on its radius)
         */
        a = {'x': 0, 'y': .4};

        /* generate a random angle at which it shoots up */
        angle = rand(Math.PI / 8, -Math.PI / 8) - Math.PI / 2;

        /* Set up our confetti particle angle */
        c_angle = 0;
        angle_v = rand(-10, 10);

        /* generate random velocity absolute value in that direction */
        val = rand(h / 30, h / 60);

        /* compute initial velocity components */
        v = {
          'x': val * Math.cos(angle),
          'y': val * Math.sin(angle)
        };

      };

      /*
       * particle is in motion, update its velocity and position
       */
      this.motionUpdate = function () {
        /*
         * velocity_incr = acceleration * time_incr
         * position_incr = velocity * time_incr
         * but time_incr = 1 in our case
         * (see the t++ line in drawOnCanvas)
         * so compute new velocity and position components
         * based on this
         */
        v.x += a.x;
        v.y += a.y;
        pos.x += Math.round(v.x);
        pos.y += Math.round(v.y);
        c_angle += angle_v;

        /* if it has landed = it's below canvas bottom edge */
        if (pos.y > h | pos.x < 0 | pos.x > w) {
          /* reset position to the initial one */
          pos = {'x': source.x, 'y': source.y};
          /* ... and make this particle inactive */
          this.active = 'done';
        }
      };

      this.draw = function (ctx) {
        ctx.save();

        ctx.translate(pos.x, pos.y);
        ctx.rotate(c_angle * Math.PI / 180);

        ctx.drawImage(confetti, -(confetti.width / 2), -(confetti.height / 2));

        ctx.restore();

        /* update its velocity and position */
        this.motionUpdate();
      };
    }


    /*
     * =============================================
     * FUNCTIONS
     * =============================================
     */


    /*
     * initializes a bunch of basic stuff
     * like canvas dimensions,
     * default particle source,
     * particle array...
     */
    const initCanvas = function (_canvas) {
      const s = getComputedStyle(_canvas);
      // console.log(s);
      /* stop animation if any got started */
      if (req_id) {
        particles = [];
        cancelAnimationFrame(req_id);
        req_id = null;
        t = 0;
      }

      /*
       * set canvas width & height
       * don't forget to also set the width & height attributes
       * of the canvas element, not just the w & h variables
       */
      w = _canvas.width = ~~s.width.split('px')[0];
      h = _canvas.height = ~~s.height.split('px')[0];
      /* set an initial source for particle fountain */
      source = {x: w / 2, y: h * .75};

      /* create particles and add them to the particle array */
      for (let i = 0; i < N_PARTICLES; i++) {
        particles.push(new Particle(i));
      }

      drawOnCanvas();
    };


    /*
     * goes through the particle array and
     * may call a particle's draw function
     */
    const drawOnCanvas = function () {
      ctx.clearRect(0, 0, w, h);
      const toBeRemoved = {}

      /* go through each particle in the particle array */
      for (let i = 0; i < particles.length; i++) {
        if (particles[i]?.active === true) {// if it's active
          particles[i].draw(ctx); // draw it on canvas
        } else if (particles[i]?.active === false) {
          particles[i].shoot(ctx); // try to make it shoot up
        } else {
          toBeRemoved[i] = true;
        }
      }
      particles = particles.filter((_, i) => !toBeRemoved[i])

      t++; /* time increment */

      req_id = requestAnimationFrame(drawOnCanvas);
    };


    /*
     * =============================================
     * START IT ALL
     * =============================================
     */

    /* Pull in our confetti */
    loadConfetti();

    /*
     * inside the setTimeout so that
     * the dimensions do get set via CSS before calling
     * the initCanvas function
     */
    setTimeout(function () {
      initCanvas(canvas);

      /* set new canvas dimensions on viewport resize */
      addEventListener('resize', () => initCanvas(canvas), false);

      canvas.addEventListener('customClick', function (e) {
        ctx.clearRect(0, 0, w, h);

        /* move x coordinate of particle fountain source */
        source.x = e.detail.clientX;
        source.y = e.detail.clientY;
        for (let i = 0; i < N_PARTICLES; i++) {
          particles.push(new Particle(i));
        }
      }, false);
      canvas.addEventListener('scrollIntoView', function () {
        ctx.clearRect(0, 0, w, h);

        /* move x coordinate of particle fountain source */
        source = {x: w / 2, y: h * .75};
        for (let i = 0; i < N_PARTICLES; i++) {
          particles.push(new Particle(i));
        }
      }, false);
    }, 500);
  }
}


const a = () => {
  gsap.set("#container", {perspective: 600})
  gsap.set("img", {xPercent: "-50%", yPercent: "-50%"})

  const total = 30;
  const container = document.getElementById("container"), w = window.innerWidth, h = window.innerHeight;

  for (let i = 0; i < total; i++) {
    const Div = document.createElement('div');
    Div.classList.add('dot', `dot-${~~(Math.random() * 4)}`)
    gsap.set(Div, {x: R(0, w), y: R(-200, -150), z: R(-200, 200)});
    container.appendChild(Div);
    animm(Div);
  }

  function animm(elm) {
    gsap.to(elm, {duration: R(6, 15), y: h + 100, ease: 'linear', repeat: -1, delay: -15});
    gsap.to(elm, {duration: R(4, 8), x: '+=100', rotationZ: R(0, 180), repeat: -1, yoyo: true, ease: 'sine.inOut'});
    gsap.to(elm, {
      duration: R(2, 8),
      rotationX: R(0, 360),
      rotationY: R(0, 360),
      repeat: -1,
      yoyo: true,
      ease: 'sine.inOut',
      delay: -5
    });
  }

  function R(min, max) {
    return min + Math.random() * (max - min)
  }
}

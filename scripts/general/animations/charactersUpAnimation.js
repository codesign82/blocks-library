import {gsap} from 'gsap';
import {ScrollTrigger} from 'gsap/ScrollTrigger';
import {SplitText} from 'gsap/SplitText';
import {getElementsForAnimation} from '../../functions/getElementsForAnimation';

gsap.registerPlugin(ScrollTrigger);
gsap.registerPlugin(SplitText);

export function charactersUpAnimation(container = document, selector = '.char-up') {
  const charsToSplit = getElementsForAnimation(container, selector);
  if (!charsToSplit) return;
  // new SplitText(charsToSplit, {
  //   type: 'chars',
  //   charsClass: 'char-overflow',
  // });
  let splitWords = new SplitText(charsToSplit, {
    type: 'chars',
    charsClass: 'child-char',
  });
  gsap.set(splitWords.chars, {yPercent: 100, opacity: 0});
  ScrollTrigger.batch(splitWords.chars, {
    onEnter: batch => gsap.timeline()
      .to(batch, {yPercent: 0, opacity: 1, duration: 0.35, stagger: 0.05}),
    onEnterBack: batch => gsap.timeline()
      .to(batch, {yPercent: 0, opacity: 1, duration: 0.35, stagger: 0.05}),
    start: 'top 50%',
    batchMax: 200,
    once: true,
  });
  gsap.set(charsToSplit, {autoAlpha: 1});
}

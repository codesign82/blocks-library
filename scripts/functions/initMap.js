import mapboxgl from 'mapbox-gl';
import {cellToLatLng} from "h3-js";
import MapboxGeocoder from "@mapbox/mapbox-gl-geocoder";
import {allowPageScroll, preventPageScroll} from "./prevent_allowPageScroll";
import countriesColorsFile from "./WorldMapUpdate_042022.txt"
import '@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css';

mapboxgl.accessToken = 'pk.eyJ1IjoibWFobW91ZC10YWhhIiwiYSI6ImNsN2Vpcm82YTAwdGMzeXBxYTN1NGVkaDUifQ.XtgGBx9IOoMS4MCBJSMcrQ';

const markerZoom = 9;

const defaultOptions = {
  center: [-71.18953488306244, 42.29365792153935],
  zoom: 4
}

const defaultActiveKeys = new Set(["active-deployments", "senet-ready", "lora-alliance-unspecified", "coming-soon"]);
const mainKeys = new Set(["senet-data", "helium-data"]);
const mapKeys = {
  "#498a49": "active-deployments",
  "#44B169": "senet-data",
  "#9945C7": "helium-data",
  "#155889": "senet-ready",
  "#ddd": "lora-alliance-unspecified",
  "#b4d5ed": "coming-soon"
};

const senetReadyHighLevel = document.querySelector("#senet-ready-high-level");

export function initMap({element, options}) {
  const mapOptions = {
    ...defaultOptions,
    ...options
  }
  
  let lastScrollTop = 0;
  let isScrollUp;
  window.addEventListener("scroll", () => {
    const st = window.pageYOffset || window.scrollY;
    isScrollUp = st < lastScrollTop;
    lastScrollTop = st <= 0 ? 0 : st;
    
    if (isScrollUp && window.scrollY !== 0) {
      element.style.pointerEvents = "none";
    } else {
      setTimeout(() => {
        element.style.pointerEvents = "unset";
      }, 300)
    }
  }, false);
  const countryInfoElement = document.querySelector(".country-info");
  
  const countriesCustomData = {};
  const countriesNavigation = {};
  const reedCountriesFile = () => {
    const rawFile = new XMLHttpRequest();
    let data;
    rawFile.open("GET", countriesColorsFile, false);
    rawFile.onreadystatechange = function () {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          data = rawFile.responseText;
          const splittedData = data.split(",\n");
          data = splittedData.length > 1 ? splittedData : data.split(",\r\n");
          data = data.reduce((acc, el) => {
            if (!el) return acc;
            const element = el.lastIndexOf(",") === el.length - 1 ? el.substring(0, el.lastIndexOf(',')) : el;
            if (!element) return acc;
            const parsingElement = JSON.parse(element);
            acc[parsingElement.color] = acc[parsingElement.color] ? [...acc[parsingElement.color], parsingElement.id] : [parsingElement.id];
            countriesCustomData[parsingElement.id] = parsingElement.customData.split("\r*")[0];
            if (parsingElement.url && !parsingElement.url.includes("senetco")) {
              countriesNavigation[parsingElement.id] = parsingElement.url;
            }
            return acc
          }, {})
        }
      }
    }
    rawFile.send(null);
    
    return data || null;
  }
  
  const markerElement = document.createElement('div');
  markerElement.className = 'marker';
  
  const markerClickedElement = document.createElement('div');
  markerClickedElement.className = 'marker';
  
  const mapKeysElements = document.querySelectorAll(".map-key-item");
  
  const map = new mapboxgl.Map({
    container: element,
    style: 'mapbox://styles/mahmoud-taha/cl7k94weg002w14lkwa93p0vg',
    center: mapOptions.center,
    zoom: mapOptions.zoom,
    projection: 'globe',
  });
  
  const getUniqueFeatures = (features) => {
    let uniqueIds = new Set();
    const currentZoom = map.getZoom();
    
    for (const feature of features) {
      const id = feature.layer.id;
      if (!uniqueIds.has(id)) {
        uniqueIds.add(id);
      }
    }
    
    if (uniqueIds.size === 0) {
      uniqueIds = defaultActiveKeys;
    }
    
    let senetReady = false;
    mapKeysElements.forEach((element) => {
      const isSenetReady = ["active-deployments", "senet-ready"].includes(element.id) && currentZoom >= 6;
      if (!(currentZoom < 6 && mainKeys.has(element.id))) {
        if (uniqueIds.has(element.id)) {
          if (isSenetReady) {
            element.classList.remove("active");
            senetReadyHighLevel.classList.add("active");
            senetReady = true;
          } else {
            element.classList.add("active");
          }
        } else {
          element.classList.remove("active");
          isSenetReady && !senetReady && senetReadyHighLevel.classList.remove("active");
        }
      } else {
        element.classList.remove("active");
        senetReadyHighLevel.classList.remove("active");
      }
    })
  }
  const rootEl = document.querySelector(':root');
  // let checkSenetCoverageInterval;
  let lastFeaturesCount = 0;
  const filterFeatures = () => {
    try {
      const features = map.queryRenderedFeatures({layers: Object.values(mapKeys)});
      if (lastFeaturesCount !== features.length) {
        getUniqueFeatures(features);
        lastFeaturesCount = features.length;
      }
      return features.length
    } catch (e) {
      return false
    }
  }
  
  map.on('style.load', () => {
    rootEl.style.setProperty('--marker-size', `${0.0068 * Math.exp(0.9089 * map.getZoom())}px`);
    
    const intervalId = setInterval(filterFeatures, 1000)
    const countriesColors = reedCountriesFile();
    Object.keys(countriesColors || {}).forEach((el, i) => {
      map.addLayer(
          {
            id: mapKeys[el],
            source: {
              type: 'vector',
              url: 'mapbox://mapbox.country-boundaries-v1',
            },
            'source-layer': 'country_boundaries',
            type: 'fill',
            paint: {
              'fill-color': el,
              'fill-opacity': 1,
            },
          },
          'country-label'
      );
      map.setPaintProperty(mapKeys[el], 'fill-opacity', [
        'interpolate',
        ['exponential', 1],
        ['zoom'],
        1,
        1,
        11,
        0.5
      ]);
      map.setFilter(mapKeys[el], [
        "all",
        [
          "in",
          ["get", "iso_3166_1"],
          ["literal", countriesColors[el]]
        ],
        [
          "==",
          ["get", "disputed"],
          "false"
        ],
        [
          "any",
          [
            "==",
            "all",
            ["get", "worldview"]
          ],
          [
            "in",
            "US",
            ["get", "worldview"]
          ]
        ]
      ]);
      
      const hoverOnCountry = (e) => {
        if (e.features) {
          countryInfoElement.textContent = `${e.features?.[0]?.properties?.name_en}: ${countriesCustomData[e.features?.[0]?.properties?.iso_3166_1]}`;
        }
      }
      
      map.on('mousemove', mapKeys[el], hoverOnCountry);
      
      map.on('touchmove', mapKeys[el], hoverOnCountry);
      
      map.on('mouseleave', mapKeys[el], (e) => {
        countryInfoElement.textContent = "";
      });
      
      map.on('touchcancel', mapKeys[el], (e) => {
        countryInfoElement.textContent = "";
      });
    });
    
    map.setPaintProperty("active-deployments", 'fill-color', [
      'interpolate',
      ['exponential', 1],
      ['zoom'],
      1,
      "#498a49",
      6,
      "#155889"
    ]);
    
    let markerOnClick, marker;
    const popup = new mapboxgl.Popup();
    
    popup.on('close', () => {
      if (markerOnClick) {
        markerOnClick.remove();
        markerOnClick = null;
        if (markerClickedElement.classList.contains("hideMarker")) {
          markerClickedElement.classList.remove("hideMarker")
        }
      }
    });
    // map.on("click", (e) => {
    //   console.log(map.queryRenderedFeatures(e.point))
    // })
    const clickOnSenetReady = (e) => {
      if ((markerOnClick || marker) || (map.getZoom() <= 7)) return;
      const features = map.queryRenderedFeatures(e.point);
      const notClickableLayers = new Set(["senet-data", "helium-data"]);
      for (const feature of features) {
        if (notClickableLayers.has(feature.layer.id)) return;
      }
      const linkTo = countriesNavigation[e?.features?.[0]?.properties?.['iso_3166_1']];
      if (linkTo) {
        window.open(linkTo, "_blank");
      } else {
        const {lat, lng} = e.lngLat;
        fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${lng},${lat}.json?limit=1&types=place%2Cregion&access_token=${mapboxgl.accessToken}`)
            .then((response) => response.json())
            .then((data) => {
                  popup.setLngLat({
                    lat: lat,
                    lng: lng
                  })
                      .setHTML(`
          <p class="coverage-info">
          Not seeing network coverage, but want to deploy an application in ${data.features?.[0]?.text || data.features?.[0]?.place_name?.split(",")?.[0]}?
          </p>
          <button class="cta-link contact-us-btn">
            Contact Us
          <svg width="22" height="11" viewBox="0 0 22 11" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M15.3292 3.54817H15.4542V3.42317V1.17424C15.4542 0.24156 16.5818 -0.225521 17.2413 0.433966L21.4434 4.63607L21.5318 4.54768L21.4434 4.63607C21.8522 5.04487 21.8523 5.70771 21.4434 6.11656L17.2413 10.3187C16.5818 10.9782 15.4542 10.5111 15.4542 9.57839V7.32942V7.20442H15.3292H0.585938C0.331389 7.20442 0.125 6.99803 0.125 6.74348V4.0091C0.125 3.75455 0.331389 3.54817 0.585938 3.54817H15.3292Z" fill="#FF5900" stroke="#FF5900" stroke-width="0.25"/>
</svg>
          </button>
          `).addTo(map);
                  document.querySelector(".contact-us-btn").addEventListener("click", () => {
                    document.querySelector("#getInTouchModal").style.display = "block";
                    preventPageScroll();
                  })
                }
            )
      }
    };
    
    map.on("click", "active-deployments", clickOnSenetReady);
    map.on("click", "senet-ready", clickOnSenetReady);
    map.on('click', 'senet-data', (e) => {
      if (map.getZoom() > 7) {
        const [lat, lng] = cellToLatLng(e.features[0].properties.h3Index);
        const strength = e.features[0].properties.value;
        fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${lng},${lat}.json?limit=1&types=place%2Cregion&access_token=${mapboxgl.accessToken}`)
            .then((response) => response.json())
            .then((data) => {
              popup.setLngLat({
                lat: lat,
                lng: lng
              })
                  .setHTML(`
          <h3>${data.features?.[0]?.text || data.features?.[0]?.place_name?.split(",")?.[0] || "Senet Coverage"}</h3>
          <p class="map-info">Lattitude: ${+lat.toFixed(4)}</p>
          <p class="map-info">Longitude: ${+lng.toFixed(4)}</p>
          <p class="map-info">Path Quality: <span class="blurry-text">High</span></p>
          <p class="map-info">Helium Extended:<span class="blurry-text">154</span> Hotspots</p>
          <button class="cta-link form-link">
          Access Coverage Information
          <svg width="22" height="11" viewBox="0 0 22 11" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M15.3292 3.54817H15.4542V3.42317V1.17424C15.4542 0.24156 16.5818 -0.225521 17.2413 0.433966L21.4434 4.63607L21.5318 4.54768L21.4434 4.63607C21.8522 5.04487 21.8523 5.70771 21.4434 6.11656L17.2413 10.3187C16.5818 10.9782 15.4542 10.5111 15.4542 9.57839V7.32942V7.20442H15.3292H0.585938C0.331389 7.20442 0.125 6.99803 0.125 6.74348V4.0091C0.125 3.75455 0.331389 3.54817 0.585938 3.54817H15.3292Z" fill="#FF5900" stroke="#FF5900" stroke-width="0.25"/>
</svg>
          </button>
          `).addTo(map);
              
              markerOnClick = new mapboxgl.Marker(markerClickedElement).setLngLat([lng, lat]).addTo(map);
              
              document.querySelector(".form-link").addEventListener("click", () => {
                modal.style.display = "block";
                preventPageScroll();
              })
            });
      }
    });
    const hoverOnSenet = (e) => {
      const currentZoom = map.getZoom();
      if (currentZoom > 7) {
        map.getCanvas().style.cursor = 'pointer';
        const langLat = cellToLatLng(e.features[0].properties.h3Index);
        marker = new mapboxgl.Marker(markerElement).setLngLat(langLat.reverse()).addTo(map);
      }
    }
    const hoverLeaveOnSenet = () => {
      map.getCanvas().style.cursor = '';
      if (marker) {
        marker.remove()
        marker = null;
      }
    }
    const moveEnd = () => {
      clearInterval(intervalId);
      filterFeatures();
    }
    map.on('mousemove', 'senet-data', hoverOnSenet);
    map.on('touchmove', 'senet-data', hoverOnSenet);
    map.on('mouseleave', 'senet-data', hoverLeaveOnSenet);
    map.on('touchcancel', 'senet-data', hoverLeaveOnSenet);
    map.on('zoom', () => {
      // exponential function to calculate the required mark size relative to the zoom level
      // it should fulfill these points
      // 10 => 60
      // 12.6 => 650
      rootEl.style.setProperty('--marker-size', `${0.0068 * Math.exp(0.9089 * map.getZoom())}px`);
    });
    map.on('moveend', moveEnd);
    map.on('touchend', moveEnd);
  });
  
  map.addControl(new mapboxgl.NavigationControl());
  
  const geocoder = new MapboxGeocoder({
    accessToken: mapboxgl.accessToken,
    mapboxgl: mapboxgl,
    placeholder: 'Search Postal Code or Location',
    marker: false,
    clearAndBlurOnEsc: true,
    flyTo: {
      zoom: 8,
    }
  })
  map.addControl(geocoder);
  
  let lastCheck = 0;
  
  geocoder.on('result', function (e) {
    // if (checkSenetCoverageInterval) {
    //   lastCheck = 0;
    // clearInterval(checkSenetCoverageInterval);
    // }
    // checkSenetCoverageInterval = setInterval(() => {
    //   checkSenetCoverage(e)
    // }, 2000);
  })
  
  // Get the modal
  const modal = document.getElementById("mapModal");
  const span = document.querySelector("#mapModal .close");
  span.onclick = function () {
    modal.style.display = "none";
    allowPageScroll();
  }
  window.addEventListener("click", (event) => {
    if (event.target === modal) {
      modal.style.display = "none";
      allowPageScroll();
    }
  })
  
  return map;
}


// const checkSenetCoverage = (e) => {
//   try {
//     const features = map.queryRenderedFeatures(e.result.center, {layers: Object.values(mapKeys)});
//     console.log(lastCheck, "hopaaaaaaaaaaaaaa", features.length, lastCheck !== features.length);
//
//     if (lastCheck !== features.length) {
//       console.log("lassssssssssst", map.queryRenderedFeatures({layers: ["senet-data"]}))
//       // getUniqueFeatures(features);
//       lastCheck = features.length;
//       console.log("features", features);
//       const places = {
//         'type': 'FeatureCollection',
//         'features': [
//           {
//             'type': 'Feature',
//             'properties': {
//               'description': "Ford's Theater",
//             },
//             'geometry': {
//               'type': 'Point',
//               'coordinates': e.result.center
//             }
//           },
//         ]
//       };
//       map.addSource('places', {
//         'type': 'geojson',
//         'data': places
//       });
//       map.addLayer({
//         'id': 'poi-labels',
//         'type': 'symbol',
//         'source': 'places',
//         "paint": {
//           "text-color": "#000",
//         },
//         'layout': {
//           'text-field': ['get', 'description'],
//           'text-variable-anchor': ['top', 'bottom', 'left', 'right'],
//           'text-radial-offset': 0.5,
//           'text-justify': 'auto',
//         }
//       });
//       // const popup = new mapboxgl.Popup();
//     }
//     return features.length
//   } catch (e) {
//     return false
//   }
// }

// window.testH3Data = (h3Data) => {
//   if (!h3Data) return null;
//   const rawFile = new XMLHttpRequest();
//   let data;
//   rawFile.open("GET", senetData, false);
//   rawFile.onreadystatechange = () => {
//     if (rawFile.readyState === 4) {
//       if (rawFile.status === 200 || rawFile.status === 0) {
//         const res = rawFile.responseText;
//         data = JSON.parse(res)?.features;
//       }
//     }
//   }
//   rawFile.send(null);
//
//   const testingData = new Set(h3Data);
//   const newData = data.filter(el => testingData.has(el.id));
//
//   return newData.map(el => ({
//     h3: el.id,
//     latLng: cellToLatLng(el.id),
//     coverage: el.properties.value === 0.3 ? "Partial" : el.properties.value === 0.7 ? "Good" : "Excellent",
//   }));
// };
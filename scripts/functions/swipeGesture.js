/**
 * @param element {HTMLElement|Document}
 * @param [callback] {function}
 * @param callbacks {{right?:function, left?:function, up?:function, down?:function }}
 */
import {debounce} from "./debounce";

export const swipeGesture = (element = document, callback = () => {
}, callbacks = {}) => {
  const {
    right: rightCallback,
    left: leftCallback,
    up: upCallback,
    down: downCallback,
  } = {
    ...{
      right: () => {
      },
      left: () => {
      },
      up: () => {
      },
      down: () => {
      },
    },
    ...callbacks,
  };

  let xDown = null;
  let yDown = null;

  function getTouches(evt) {
    return evt.touches; // jQuery
  }

  function handleTouchStart(evt) {
    const firstTouch = getTouches(evt)[0];
    xDown = firstTouch.clientX;
    yDown = firstTouch.clientY;
  }

  const handleTouchMove = (evt) => {
    if (!xDown || !yDown) {
      return;
    }
    const xUp = evt.touches[0].clientX;
    const yUp = evt.touches[0].clientY;

    const xDiff = xDown - xUp;
    const yDiff = yDown - yUp;
    if (Math.abs(xDiff) < 10 || Math.abs(yDiff) < 10)
      if (Math.abs(xDiff) > Math.abs(yDiff)) {/*most significant*/
        if (xDiff > 0) {
          /* right swipe */
          callback?.()
          rightCallback()
        } else {
          /* left swipe */
          callback?.()
          leftCallback()
        }
      } else {
        if (yDiff > 0) {
          /* down swipe */
          callback?.()
          downCallback()
        } else {
          /* up swipe */
          callback?.()
          upCallback()
        }
      }
    /* reset values */
    xDown = null;
    yDown = null;
  }


  element.addEventListener('touchstart', handleTouchStart, false);
  element.addEventListener('touchmove', handleTouchMove, false);

}

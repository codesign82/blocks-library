import Globe from "globe.gl";
import {Color} from 'three';
import countriesColorsFile from "./WorldMapUpdate_042022.txt"
import countries from "./globe-data-min.json";

const countriesNavigation = {};

const readCountriesFile = () => {
  const rawFile = new XMLHttpRequest();
  let data;
  rawFile.open("GET", countriesColorsFile, false);
  rawFile.onreadystatechange = function () {
    if (rawFile.readyState === 4) {
      if (rawFile.status === 200 || rawFile.status === 0) {
        data = rawFile.responseText;
        const splittedData = data.split(",\n");
        data = splittedData.length > 1 ? splittedData : data.split(",\r\n");
        data = data.reduce((acc, el) => {
          if (!el) return acc
          const element = el.lastIndexOf(",") === el.length - 1 ? el.substring(0, el.lastIndexOf(',')) : el;
          if (!element) return acc;
          const parsingGlobElement = JSON.parse(element);
          if (parsingGlobElement.url && !parsingGlobElement.url.includes("senetco")) {
            countriesNavigation[parsingGlobElement.id] = parsingGlobElement.url;
          }
          acc[parsingGlobElement.id] = parsingGlobElement.color;
          return acc
        }, {})
      }
    }
  }
  rawFile.send(null);
  return data || null;
}

const countriesColors = readCountriesFile();

const defaultOptions = {
  autoRotate: true,
  autoRotateSpeed: 4,
  clickable: true,
  enableZoom: true,
  enableUserInteraction: true,
  backgroundColor: "#F5F5F5",
  firstZoom: 2.5,
  animateGlobeOnInit: false,
}

export function initGlobe({globeElement, mapElement, options}) {
  if (!globeElement) return null;
  const timeToSpin = +globeElement.dataset.timeout * 1000 || 20000;
  const globeOptions = {
    ...defaultOptions,
    ...options
  }
  
  const {
    autoRotate,
    autoRotateSpeed,
    clickable,
    enableZoom,
    enableUserInteraction,
    backgroundColor,
    firstZoom,
    animateGlobeOnInit
  } = globeOptions
  
  const ringsData = [
    {
      lat: -8.134107567057697,
      lng: -53.96406049083066,
      maxR: 12,
      propagationSpeed: 2,
      repeatPeriod: 763.3263799085797,
    },
    {
      lat: 48.452270965218936,
      lng: -98.29659995842007,
      maxR: 18,
      propagationSpeed: 3,
      repeatPeriod: 763.3263799085797,
    },
    {
      lat: 22.961766775637813,
      lng: 79.21300007685696,
      maxR: 12,
      propagationSpeed: 2,
      repeatPeriod: 763.3263799085797,
    },
    {
      lat: -24.907145040330605,
      lng: 138.1832435848409,
      maxR: 12,
      propagationSpeed: 2,
      repeatPeriod: 763.3263799085797,
    },
    {
      lat: 50.77234594966383,
      lng: 2.540401850382608,
      maxR: 6,
      propagationSpeed: 1,
      repeatPeriod: 900,
    },
    {
      lat: 36.83747836766468,
      lng: 140.33868160722218,
      maxR: 4,
      propagationSpeed: 1,
      repeatPeriod: 900,
    },
    {
      lat: 7.714318777107451,
      lng: -5.767652859200268,
      maxR: 4,
      propagationSpeed: 1,
      repeatPeriod: 900,
    },
    {
      lat: -28.494549981207314,
      lng: 24.2161010927434,
      maxR: 4,
      propagationSpeed: 1,
      repeatPeriod: 900,
    },
    {
      lat: 45.770825265265316,
      lng: 25.66975248644526,
      maxR: 3,
      propagationSpeed: 0.75,
      repeatPeriod: 900,
    },
  ];
  
  const world = Globe({
    waitForGlobeReady: false,
  })(globeElement)
      .ringsData(ringsData)
      .ringColor(() => "#FF5900")
      .ringMaxRadius('maxR')
      .ringPropagationSpeed('propagationSpeed')
      .ringRepeatPeriod('repeatPeriod')
      .hexPolygonsData(countries.features)
      .hexPolygonResolution(3)
      .hexPolygonMargin(0.3)
      .hexPolygonColor((e) => countriesColors[e?.properties.ISO_A2] || "rgba(221, 221, 221, 1)")
      .backgroundColor(backgroundColor)
      .width([globeElement.clientWidth])
      .height([globeElement.clientHeight])
      .pointOfView({
        lat: 8.725924859623788,
        lng: -83.03979613238917,
        altitude: animateGlobeOnInit ? 0 : firstZoom
      })
      .polygonsData(countries.features)
      .polygonCapColor(() => 'rgba(0, 0, 0, 0)')
      .polygonSideColor(() => 'rgba(0, 0, 0, 0)')
  
  world.controls().autoRotate = animateGlobeOnInit ? false : autoRotate;
  world.controls().autoRotateSpeed = animateGlobeOnInit ? 0 : autoRotateSpeed;
  world.controls().enableZoom = animateGlobeOnInit ? false : enableZoom;
  world.controls().enabled = animateGlobeOnInit ? false : enableUserInteraction;
  const globeMaterial = world.globeMaterial();
  globeMaterial.color = new Color(`rgba(245, 245, 245, ${animateGlobeOnInit ? 0 : 1})`);
  globeMaterial.emissiveIntensity = 0.1;
  globeMaterial.shininess = 0.7;
  world.globeMaterial().opacity = animateGlobeOnInit ? 0 : 1;
  
  
  let isAnimationGlobeFinished = false;
  setTimeout(() => globeElement.parentNode?.classList.add('loaded'), 1000)
  if (animateGlobeOnInit) {
    let currentOpacity = 0.5;
    const animateOpacity = () => {
      if (world.globeMaterial().opacity !== 1) {
        world.globeMaterial().opacity = currentOpacity;
        currentOpacity += 0.5;
      }
    }
    
    setTimeout(() => {
      world.pointOfView({altitude: firstZoom}, 3000);
      setTimeout(() => {
        isAnimationGlobeFinished = true;
      }, 3000)
    }, 1000)
    
    setTimeout(() => {
      world.controls().autoRotate = autoRotate;
      world.controls().autoRotateSpeed = autoRotateSpeed;
      world.controls().enableZoom = enableZoom;
      world.controls().enabled = enableUserInteraction;
      setInterval(animateOpacity, 500);
    }, 4000)
  }
  
  if (clickable && mapElement) {
    const initMapFile = import('./initMap');
    const animateToMap = (lng, lat) => {
      const MAP_CENTER = {lat: lat, lng: lng, altitude: 0.1};
      world.pointOfView(MAP_CENTER, 3000);
      setTimeout(() => {
        globeElement.classList.add("hide");
        mapElement.classList.add("visible");
        initMapFile.then(({initMap}) => initMap({
          element: mapElement,
          options: {
            center: [lng, lat],
          }
        }));
      }, 2000)
    }
    
    const noInteract = setTimeout(() => {
      animateToMap(-99.5118087059997, 38.807455912438);
    }, timeToSpin);
    
    let interact = false;
    world.controls()._listeners.start[0] = () => {
      clearTimeout(noInteract);
      interact = true;
    };
    
    world.onZoom(({...e}) => {
      if (e.altitude.toFixed(2) !== firstZoom.toFixed(2) && isAnimationGlobeFinished && !interact) {
        clearTimeout(noInteract);
        interact = true;
      }
    })
    
    world.onPolygonClick(({properties}, _, {lat, lng}) => {
      clearTimeout(noInteract);
      const linkTo = countriesNavigation[properties?.ISO_A2];
      if (linkTo) {
        window.open(linkTo, "_blank")
      } else {
        animateToMap(lng, lat);
      }
    })
        .onGlobeClick((lngLat) => {
          clearTimeout(noInteract);
          animateToMap(lngLat.lng, lngLat.lat);
        })
    
    globeElement.addEventListener("mousemove", (event) => {
      let offsetX = event.offsetX;
      let offsetY = event.offsetY;
      const globeCoords = world.toGlobeCoords(offsetX, offsetY)
      world.controls().autoRotate = !globeCoords;
      globeElement.style.cursor = globeCoords ? "pointer" : "unset";
    });
  }
  
  window.addEventListener('resize', () => {
    world.width([globeElement.clientWidth])
    world.height([globeElement.clientHeight])
  });
  
  return world;
}
export const loadedPages = [];

export function initLinksToPreloadAssets(container) {
  for (let link of container.querySelectorAll('a')) {
    link.addEventListener('mouseenter', () => preloadAssetsOfLink(link.href), {passive: true})
    link.addEventListener('touchstart', () => preloadAssetsOfLink(link.href), {passive: true})
  }
}

export function preloadAssetsOfLink(url) {
  try {
  
  if (loadedPages.includes(url)) return;
  loadedPages.push(url);
  fetch(url).then(res => res.text()).then(text => {
    const doc = (new DOMParser()).parseFromString(text, 'text/html');
    for (let img of doc.querySelectorAll('img,video')) {
      const src = img.src || img.getElementsByTagName("source")?.[0]?.src
      src && fetch(src)
    }
  }).catch(console.log)
  }
  catch (e) {
    console.log(e);
  }
}
